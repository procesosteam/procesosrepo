using System;
using System.Collections.Generic;

namespace ProcesoPP.Model
{
	public class TipoServicio
	{
		#region Private Properties
		
		private int _IdTipoServicio;
		private int _IdEspecificaciones;
		private string _Descripcion;
        private List<EspecificacionTecnica> _oEspecificacion;
		#endregion
		
		#region Constructors
		
		public TipoServicio()
		{
            oEspecificacion = new List<EspecificacionTecnica>();
		}
		
		public TipoServicio(int IdTipoServicio, int IdEspecificaciones, string Descripcion)
		{
			_IdTipoServicio = IdTipoServicio;
			_IdEspecificaciones = IdEspecificaciones;
			_Descripcion = Descripcion;
		}

		#endregion
		
		#region Properties
		
		public int IdTipoServicio
		{
			get	{ return _IdTipoServicio; }
			set	{ _IdTipoServicio = value; }
		}
		
		public string Descripcion
		{
			get	{ return _Descripcion; }
			set	{ _Descripcion = value; }
		}

        public List<EspecificacionTecnica> oEspecificacion
        {
            get { return _oEspecificacion; }
            set { _oEspecificacion = value; }
        }

        public string CodTango { get; set; }

        public string Abreviacion { get; set; }
        public bool RequierePersonal { get; set; }        
        public int idSector { get; set; } 
        public bool RequiereMateriales { get; set; }
        public bool RequiereCuadrilla { get; set; }
        public bool MasEquipo { get; set; }
        public bool RequiereTraslado { get; set; }
        public bool RequiereKms { get; set; }
        public bool RequiereKmsTaco { get; set; }
        public bool RequiereChofer { get; set; }
        public bool RequiereEquipo { get; set; }

		#endregion
	}
}
