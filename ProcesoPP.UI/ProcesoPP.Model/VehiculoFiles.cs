
///////////////////////////////////////////////////////////////////////////
//
// Generated by MyGeneration Version # (1.3.0.9) 
//
///////////////////////////////////////////////////////////////////////////

using System;


namespace ProcesoPP.Model
{
	public class VehiculoFiles
	{
		#region Private Properties
		
		private int _IdVehiculoFiles;
		private int _IdVehiculo;
		private string _Nombre;
		private string _Ruta;
		
		#endregion
		
		#region Constructors
		
		public VehiculoFiles()
		{

		}
		
		public VehiculoFiles(int IdVehiculoFiles, int IdVehiculo, string Nombre, string Ruta)
		{
			_IdVehiculoFiles = IdVehiculoFiles;
			_IdVehiculo = IdVehiculo;
			_Nombre = Nombre;
			_Ruta = Ruta;
		}

		#endregion
		
		#region Properties
		
		public int IdVehiculoFiles
		{
			get	{ return _IdVehiculoFiles; }
			set	{ _IdVehiculoFiles = value; }
		}
		
		public int IdVehiculo
		{
			get	{ return _IdVehiculo; }
			set	{ _IdVehiculo = value; }
		}
		
		public string Nombre
		{
			get	{ return _Nombre; }
			set	{ _Nombre = value; }
		}
		
		public string Ruta
		{
			get	{ return _Ruta; }
			set	{ _Ruta = value; }
		}
		
		#endregion
	}
}
