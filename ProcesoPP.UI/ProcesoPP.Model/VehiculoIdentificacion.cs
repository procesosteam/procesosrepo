
using System;


namespace ProcesoPP.Model
{
	public class VehiculoIdentificacion
	{
		#region Private Properties
		
		private int _IdIdentificacion;
		private int _IdVehiculo;
		private int _IdCodificacion;
		private string _NroTacografo;
		private bool _Alta;
		private bool _Baja;
        private string _NroCodificacion;

		#endregion
		
		#region Constructors
		
		public VehiculoIdentificacion()
		{

		}
		
		public VehiculoIdentificacion(int IdIdentificacion, int IdVehiculo, int IdCodificacion, string NroTacografo, bool Alta, bool Baja, string NroCodificacion)
		{
			_IdIdentificacion = IdIdentificacion;
			_IdVehiculo = IdVehiculo;
			_IdCodificacion = IdCodificacion;
			_NroTacografo = NroTacografo;
			_Alta = Alta;
			_Baja = Baja;
            _NroCodificacion = NroCodificacion;
		}

		#endregion
		
		#region Properties
		
		public int IdIdentificacion
		{
			get	{ return _IdIdentificacion; }
			set	{ _IdIdentificacion = value; }
		}
		
		public int IdVehiculo
		{
			get	{ return _IdVehiculo; }
			set	{ _IdVehiculo = value; }
		}
		
		public int IdCodificacion
		{
			get	{ return _IdCodificacion; }
			set	{ _IdCodificacion = value; }
		}
		
		public string NroTacografo
		{
			get	{ return _NroTacografo; }
			set	{ _NroTacografo = value; }
		}

        public string NroCodificacion
        {
            get { return _NroCodificacion; }
            set { _NroCodificacion = value; }
        }

		public bool Alta
		{
			get	{ return _Alta; }
			set	{ _Alta = value; }
		}
		
		public bool Baja
		{
			get	{ return _Baja; }
			set	{ _Baja = value; }
		}

        private Codificacion _codificacion;
        public Codificacion oCodificacion
        {
            get { return _codificacion; }
            set { _codificacion = value; }
        }        
		
		#endregion
	}
}
