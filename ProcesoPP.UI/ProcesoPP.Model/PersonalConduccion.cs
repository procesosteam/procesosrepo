using System;


namespace ProcesoPP.Model
{
	public class PersonalConduccion
	{
		#region Private Properties
		
		private int _IdPersonalConduccion;
		private bool _ManejaEmpresa;
		private DateTime _VtoCarnet;
		private bool _Psicofisico;
		private DateTime _VtoPsicofisico;
		private bool _CargarGenerales;
		private DateTime _VtoCargasGenerales;
		private bool _CargarPeligrosas;
		private DateTime _VtoCargasPeligrosas;
		private bool _ManejoDefensivo;
		private DateTime _VtoManejoDefensivo;
		private string _Empresa;
		private string _CertCNRT;
        private int _IdCategoria;
        private bool _CarnetPsicofisico;
        private DateTime _VencimientoCarnet;
		
		#endregion
		
		#region Constructors
		
		public PersonalConduccion()
		{

		}
		
		public PersonalConduccion(int IdPersonalConduccion, bool ManejaEmpresa, DateTime VtoCarnet, bool Psicofisico, DateTime VtoPsicofisico, bool CargarGenerales, DateTime VtoCargasGenerales, bool CargarPeligrosas, DateTime VtoCargasPeligrosas, bool ManejoDefensivo, DateTime VtoManejoDefensivo, string Empresa, string CertCNRT, int IdCategoria, bool Carnet, DateTime vencimientoCarnet)
		{
			_IdPersonalConduccion = IdPersonalConduccion;
			_ManejaEmpresa = ManejaEmpresa;
			_VtoCarnet = VtoCarnet;
			_Psicofisico = Psicofisico;
			_VtoPsicofisico = VtoPsicofisico;
			_CargarGenerales = CargarGenerales;
			_VtoCargasGenerales = VtoCargasGenerales;
			_CargarPeligrosas = CargarPeligrosas;
			_VtoCargasPeligrosas = VtoCargasPeligrosas;
			_ManejoDefensivo = ManejoDefensivo;
			_VtoManejoDefensivo = VtoManejoDefensivo;
			_Empresa = Empresa;
			_CertCNRT = CertCNRT;
            _IdCategoria = IdCategoria;
            _CarnetPsicofisico = Carnet;
            _VencimientoCarnet = vencimientoCarnet;
		}

		#endregion
		
		#region Properties
		
		public int IdPersonalConduccion
		{
			get	{ return _IdPersonalConduccion; }
			set	{ _IdPersonalConduccion = value; }
		}
		
		public bool ManejaEmpresa
		{
			get	{ return _ManejaEmpresa; }
			set	{ _ManejaEmpresa = value; }
		}
		
		public DateTime VtoCarnet
		{
			get	{ return _VtoCarnet; }
			set	{ _VtoCarnet = value; }
		}
		
		public bool Psicofisico
		{
			get	{ return _Psicofisico; }
			set	{ _Psicofisico = value; }
		}
		
		public DateTime VtoPsicofisico
		{
			get	{ return _VtoPsicofisico; }
			set	{ _VtoPsicofisico = value; }
		}
		
		public bool CargarGenerales
		{
			get	{ return _CargarGenerales; }
			set	{ _CargarGenerales = value; }
		}
		
		public DateTime VtoCargasGenerales
		{
			get	{ return _VtoCargasGenerales; }
			set	{ _VtoCargasGenerales = value; }
		}
		
		public bool CargarPeligrosas
		{
			get	{ return _CargarPeligrosas; }
			set	{ _CargarPeligrosas = value; }
		}
		
		public DateTime VtoCargasPeligrosas
		{
			get	{ return _VtoCargasPeligrosas; }
			set	{ _VtoCargasPeligrosas = value; }
		}
		
		public bool ManejoDefensivo
		{
			get	{ return _ManejoDefensivo; }
			set	{ _ManejoDefensivo = value; }
		}
		
		public DateTime VtoManejoDefensivo
		{
			get	{ return _VtoManejoDefensivo; }
			set	{ _VtoManejoDefensivo = value; }
		}
		
		public string Empresa
		{
			get	{ return _Empresa; }
			set	{ _Empresa = value; }
		}
		
		public string CertCNRT
		{
			get	{ return _CertCNRT; }
			set	{ _CertCNRT = value; }
		}

        public int IdCategoria
        {
            get { return _IdCategoria; }
            set { _IdCategoria = value; }
        }

        public bool CarnetPsicofisico
        {
            get { return _CarnetPsicofisico; }
            set { _CarnetPsicofisico = value; }
        }

        public DateTime VencimientoCarnet
        {
            get { return _VencimientoCarnet; }
            set { _VencimientoCarnet = value; }
        }

		#endregion
	}
}
