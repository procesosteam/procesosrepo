

using System;

namespace ProcesoPP.Model
{
	public class Solicitante
	{
		#region Private Properties
		
		private int _IdSolicitante;
		private string _Nombre;
		private string _Apellido;
		private int _IdSector;
		private int _IdEmpresa;
		private string _Telefono;
        private Sector _oSector;
        private Empresa _oEmpresa;
		
		#endregion
		
		#region Constructors
		
		public Solicitante()
		{

		}
		
		public Solicitante(int IdSolicitante, string Nombre, string Apellido, int IdSector, int IdEmpresa, string Telefono)
		{
			_IdSolicitante = IdSolicitante;
			_Nombre = Nombre;
			_Apellido = Apellido;
			_IdSector = IdSector;
			_IdEmpresa = IdEmpresa;
			_Telefono = Telefono;
		}

		#endregion
		
		#region Properties
		
		public int IdSolicitante
		{
			get	{ return _IdSolicitante; }
			set	{ _IdSolicitante = value; }
		}
		
		public string Nombre
		{
			get	{ return _Nombre; }
			set	{ _Nombre = value; }
		}
		
		public string Apellido
		{
			get	{ return _Apellido; }
			set	{ _Apellido = value; }
		}
		
		public int IdSector
		{
			get	{ return _IdSector; }
			set	{ _IdSector = value; }
		}
		
		public int IdEmpresa
		{
			get	{ return _IdEmpresa; }
			set	{ _IdEmpresa = value; }
		}
		
		public string Telefono
		{
			get	{ return _Telefono; }
			set	{ _Telefono = value; }
		}

        public Sector oSector
        {
            get { return _oSector; }
            set { _oSector = value; }
        }

        public Empresa oEmpresa { get; set; }

		#endregion
	}
}
