
using System;


namespace ProcesoPP.Model
{
	public class Banco
	{
		#region Private Properties
		
		private int _IdBanco;
		private string _Descripcion;
		
		#endregion
		
		#region Constructors
		
		public Banco()
		{

		}
		
		public Banco(int IdBanco, string Descripcion)
		{
			_IdBanco = IdBanco;
			_Descripcion = Descripcion;
		}

		#endregion
		
		#region Properties
		
		public int IdBanco
		{
			get	{ return _IdBanco; }
			set	{ _IdBanco = value; }
		}
		
		public string Descripcion
		{
			get	{ return _Descripcion; }
			set	{ _Descripcion = value; }
		}
		
		#endregion
	}
}
