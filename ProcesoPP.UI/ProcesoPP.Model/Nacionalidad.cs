
///////////////////////////////////////////////////////////////////////////
//
// Generated by MyGeneration Version # (1.3.0.9) 
//
///////////////////////////////////////////////////////////////////////////

using System;


namespace ProcesoPP.Model
{
	public class Nacionalidad
	{
		#region Private Properties
		
		private int _IdNacionalidad;
		private string _Descripcion;
		
		#endregion
		
		#region Constructors
		
		public Nacionalidad()
		{

		}
		
		public Nacionalidad(int IdNacionalidad, string Descripcion)
		{
			_IdNacionalidad = IdNacionalidad;
			_Descripcion = Descripcion;
		}

		#endregion
		
		#region Properties
		
		public int IdNacionalidad
		{
			get	{ return _IdNacionalidad; }
			set	{ _IdNacionalidad = value; }
		}
		
		public string Descripcion
		{
			get	{ return _Descripcion; }
			set	{ _Descripcion = value; }
		}
		
		#endregion
	}
}
