
using System;

namespace ProcesoPP.Model
{
	public class AcuerdoConceptoCosto
	{
		#region Private Properties
		
		private int _IdAcuerdoConcepto;
		private string _Concepto;
		private decimal _Costo;
		private int _IdAcuerdoServicio;
        private int _IdEspecificacion;
        private EspecificacionTecnica _oEspecificacion;

		#endregion
		
		#region Constructors
		
		public AcuerdoConceptoCosto()
		{

		}

        public AcuerdoConceptoCosto(int IdAcuerdoConcepto, string Concepto, decimal Costo, int IdAcuerdoServicio, int IdEspecificacion, EspecificacionTecnica oespecificacion)
		{
			_IdAcuerdoConcepto = IdAcuerdoConcepto;
			_Concepto = Concepto;
			_Costo = Costo;
			_IdAcuerdoServicio = IdAcuerdoServicio;
            _IdEspecificacion = IdEspecificacion;
            _oEspecificacion = oespecificacion;
		}

		#endregion
		
		#region Properties
		
		public int IdAcuerdoConcepto
		{
			get	{ return _IdAcuerdoConcepto; }
			set	{ _IdAcuerdoConcepto = value; }
		}
		
		public string Concepto
		{
			get	{ return _Concepto; }
			set	{ _Concepto = value; }
		}
		
		public decimal Costo
		{
			get	{ return _Costo; }
			set	{ _Costo = value; }
		}
		
		public int IdAcuerdoServicio
		{
			get	{ return _IdAcuerdoServicio; }
			set	{ _IdAcuerdoServicio = value; }
		}
        
        public int IdEspecificacion
        {
            get { return _IdEspecificacion; }
            set { _IdEspecificacion = value; }
        }

        public EspecificacionTecnica oEspecificacion
        {
            get { return _oEspecificacion; }
            set { _oEspecificacion = value; }
        }

		#endregion
	}
}
