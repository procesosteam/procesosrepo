
using System;
using System.Collections.Generic;
using System.Data;


namespace ProcesoPP.Model
{
	public class ParteEntrega
	{
		#region Private Properties
		
		private int _IdParteEntrega;
		private DateTime _Fecha;
        private DateTime _HoraSalida;
        private DateTime _HoraLlegada;
		private int _IdOrdenTrabajo;
        private string _NroParte;
        private bool _Entrega;
        private string _Observaciones;
        private OrdenTrabajo _oOrdenTrabajo;

		#endregion
		
		#region Constructors
		
		public ParteEntrega()
		{

		}

        public ParteEntrega(int IdParteEntrega, DateTime Fecha, DateTime HoraSalida, DateTime HoraLlegada, int IdOrdenTrabajo)
		{
			_IdParteEntrega = IdParteEntrega;
			_Fecha = Fecha;
			_HoraSalida = HoraSalida;
			_HoraLlegada = HoraLlegada;
			_IdOrdenTrabajo = IdOrdenTrabajo;
		}

		#endregion
		
		#region Properties

        public int IdParteEntrega
		{
			get	{ return _IdParteEntrega; }
			set	{ _IdParteEntrega = value; }
		}

        public DateTime Fecha
		{
			get	{ return _Fecha; }
			set	{ _Fecha = value; }
		}

        public DateTime HoraSalida
		{
			get	{ return _HoraSalida; }
			set	{ _HoraSalida = value; }
		}

        public DateTime HoraLlegada
		{
			get	{ return _HoraLlegada; }
			set	{ _HoraLlegada = value; }
		}

        public int IdOrdenTrabajo
		{
			get	{ return _IdOrdenTrabajo; }
			set	{ _IdOrdenTrabajo = value; }
		}

        public string NroParteEntrega
        {
            get { return _NroParte; }
            set { _NroParte = value; }
        }

        public bool Entrega
        {
            get { return _Entrega; }
            set { _Entrega = value; }
        }

        public string Adicionales
        {
            get { return _Observaciones; }
            set { _Observaciones = value; }
        }

        public bool Conexion { get; set; }
        public bool Desconexion { get; set; }
        public string Observacion { get;set; }
        public string nroRemito { get; set; }
        public bool Mensual { get; set; }

        public OrdenTrabajo oOrdenTrabajo
        {
            get { return _oOrdenTrabajo; }
            set { _oOrdenTrabajo = value; }
        }

        public DataTable oVerificaParte{ get; set; }

		#endregion
	}
}
