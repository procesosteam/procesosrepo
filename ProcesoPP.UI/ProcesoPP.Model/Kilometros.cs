﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProcesoPP.Model
{
    public class Kilometros
    {
        #region Constructors
		
		public Kilometros()
		{
            oLugarHasta = new Lugar();
            oLugarDesde = new Lugar();
		}

        public Kilometros(int _idKilometros, Lugar _oDesde, Lugar _oHasta, decimal _kms)
		{
            idKms = _idKilometros;
			oLugarDesde = _oDesde;
            oLugarHasta = _oHasta;
            Kms = _kms;
		}

		#endregion
		
		#region Properties

        public int idKms { get; set; }
        public Lugar oLugarDesde{ get; set; }
        public Lugar oLugarHasta { get; set; }
        public decimal Kms { get; set; }
        public bool KmsEstablecidos { get; set; }
        #endregion
    }
}
