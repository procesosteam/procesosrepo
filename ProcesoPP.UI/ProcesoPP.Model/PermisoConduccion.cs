using System;


namespace ProcesoPP.Model
{
	public class PermisoConduccion
	{
		#region Private Properties
		
		private int _IdPermiso;
		private string _Descripcion;
        private int _IdPersonalACargo;
        private int _IdPersonal;
        private DateTime _Fecha;
        private DateTime _FechaOriginal;

		#endregion
		
		#region Constructors
		
		public PermisoConduccion()
		{

		}

        public PermisoConduccion(int IdPermiso, string Descripcion, int IdPersonalACargo, int IdPersonal, DateTime Fecha, DateTime FechaOriginal)
		{
			_IdPermiso = IdPermiso;
			_Descripcion = Descripcion;
            _IdPersonal = IdPersonal;
            _IdPersonalACargo = IdPersonalACargo;
            _Fecha = Fecha;
            _FechaOriginal = FechaOriginal;
		}

		#endregion
		
		#region Properties
		
		public int IdPermiso
		{
			get	{ return _IdPermiso; }
			set	{ _IdPermiso = value; }
		}
		
		public string Descripcion
		{
			get	{ return _Descripcion; }
			set	{ _Descripcion = value; }
		}

        public int IdPersonalACargo
        {
            get { return _IdPersonalACargo; }
            set { _IdPersonalACargo = value; }
        }

        public int IdPersonal
        {
            get { return _IdPersonal; }
            set { _IdPersonal = value; }
        }

        public DateTime Fecha
        {
            get { return _Fecha; }
            set { _Fecha = value; }
        }

        public DateTime FechaOriginal
        {
            get { return _FechaOriginal; }
            set { _FechaOriginal = value; }
        }

		#endregion
	}
}
