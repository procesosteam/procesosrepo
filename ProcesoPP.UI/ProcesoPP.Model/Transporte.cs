

using System;

namespace ProcesoPP.Model
{
	public class Transporte
	{
		#region Private Properties
		
		private int _IdTransporte;
		private string _Descripcion;
		private string _Marca;
		private string _Modelo;
		private int _Anio;
		private string _ItemVerificacion;
		private string _Patente;
		
		#endregion
		
		#region Constructors
		
		public Transporte()
		{

		}
		
		public Transporte(int IdTransporte, string Descripcion, string Marca, string Modelo, int Anio, string ItemVerificacion, string Patente)
		{
			_IdTransporte = IdTransporte;
			_Descripcion = Descripcion;
			_Marca = Marca;
			_Modelo = Modelo;
			_Anio = Anio;
			_ItemVerificacion = ItemVerificacion;
			_Patente = Patente;
		}

		#endregion
		
		#region Properties
		
		public int IdTransporte
		{
			get	{ return _IdTransporte; }
			set	{ _IdTransporte = value; }
		}
		
		public string Descripcion
		{
			get	{ return _Descripcion; }
			set	{ _Descripcion = value; }
		}
		
		public string Marca
		{
			get	{ return _Marca; }
			set	{ _Marca = value; }
		}
		
		public string Modelo
		{
			get	{ return _Modelo; }
			set	{ _Modelo = value; }
		}
		
		public int Anio
		{
			get	{ return _Anio; }
			set	{ _Anio = value; }
		}
		
		public string ItemVerificacion
		{
			get	{ return _ItemVerificacion; }
			set	{ _ItemVerificacion = value; }
		}
		
		public string Patente
		{
			get	{ return _Patente; }
			set	{ _Patente = value; }
		}
		
		#endregion
	}
}
