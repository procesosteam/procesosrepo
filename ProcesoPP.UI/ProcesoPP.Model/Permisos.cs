﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProcesoPP.Model
{
    [Serializable]
    public class Permisos
    {
        public int idPermiso { get; set; }
        public Modulo oModulo { get; set; }
        public TipoUsuario oTipoUsuario { get; set; }
        public bool Lectura { get; set; }
        public bool Escritura { get; set; }
    }
    [Serializable]
    public class Modulo
    {
        public int idModulo{ get; set; }
        public string  Descripcion{ get; set; }
    }
    [Serializable]
    public class TipoUsuario
    {
        public int idTipoUsuario{ get; set; }
        public string Descripcion { get; set; }
    }


}
