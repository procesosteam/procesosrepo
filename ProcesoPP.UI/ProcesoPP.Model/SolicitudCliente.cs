

using System;
using System.Collections.Generic;

namespace ProcesoPP.Model
{
	public class SolicitudCliente
	{
		#region Private Properties
		
		private int _IdSolicitud;
        private int _NroSolicitud;
		private DateTime _Fecha;
		private DateTime _Hora;
		private DateTime _FechaAlta;
		private int _IdUsuario;
		private int _IdTipoServicio;
        private List<EspecificacionTecnica> _oListEspecificaciones;
        private List<SolicitudClienteTipoServicio> _oListSCTipoServicio;
		private int _IdMovimiento;
		private int _IdSolicitante;
		private int _IdCondicion;
		private string _NroPedidoCliente;
        private Movimiento _oMovimiento;
        private Solicitante _oSolicitante;
        private Condicion _oCondicion;
        private TipoServicio _oTipoServicio;
        private bool _baja;
        private bool _Entrega;
        private int _IdEspecificaciones;

		#endregion
		
		#region Constructors
		
		public SolicitudCliente()
		{
         

		}
		
		public SolicitudCliente(int IdSolicitud, DateTime Fecha, DateTime Hora, DateTime FechaAlta, int IdUsuario, int IdTipoServicio, List<EspecificacionTecnica> oListEsp, int IdMovimiento, int IdSolicitante, int IdCondicion, string NroPedidoCliente)
		{
			_IdSolicitud = IdSolicitud;
			_Fecha = Fecha;
			_Hora = Hora;
			_FechaAlta = FechaAlta;
			_IdUsuario = IdUsuario;
			_IdTipoServicio = IdTipoServicio;
			_oListEspecificaciones = oListEsp;
			_IdMovimiento = IdMovimiento;
			_IdSolicitante = IdSolicitante;
			_IdCondicion = IdCondicion;
			_NroPedidoCliente = NroPedidoCliente;
		}

		#endregion
		
		#region Properties

        public int NroSolicitud
        {
            get { return _NroSolicitud; }
            set { _NroSolicitud = value; }
        }

		public int IdSolicitud
		{
			get	{ return _IdSolicitud; }
			set	{ _IdSolicitud = value; }
		}
		
		public DateTime Fecha
		{
			get	{ return _Fecha; }
			set	{ _Fecha = value; }
		}
		
		public DateTime Hora
		{
			get	{ return _Hora; }
			set	{ _Hora = value; }
		}
		
		public DateTime FechaAlta
		{
			get	{ return _FechaAlta; }
			set	{ _FechaAlta = value; }
		}
		
		public int IdUsuario
		{
			get	{ return _IdUsuario; }
			set	{ _IdUsuario = value; }
		}
		
		public int IdTipoServicio
		{
			get	{ return _IdTipoServicio; }
			set	{ _IdTipoServicio = value; }
		}

        public TipoServicio oTipoServicio
        {
            get { return _oTipoServicio; }
            set { _oTipoServicio = value; }
        }

        public int IdEspecificaciones
        {
            get { return _IdEspecificaciones; }
            set { _IdEspecificaciones = value; }
        }

        public List<EspecificacionTecnica> oListEspecificaciones
		{
			get	{ return _oListEspecificaciones; }
            set { _oListEspecificaciones = value; }
		}
        		
		public int IdMovimiento
		{
			get	{ return _IdMovimiento; }
			set	{ _IdMovimiento = value; }
		}
		
		public int IdSolicitante
		{
			get	{ return _IdSolicitante; }
			set	{ _IdSolicitante = value; }
		}
		
		public int IdCondicion
		{
			get	{ return _IdCondicion; }
			set	{ _IdCondicion = value; }
		}
		
		public string NroPedidoCliente
		{
			get	{ return _NroPedidoCliente; }
			set	{ _NroPedidoCliente = value; }
		}

        public Movimiento oMovimiento
        {
            get { return _oMovimiento; }
            set { _oMovimiento = value; }
        }

        public Condicion oCondicion
        {
            get { return _oCondicion; }
            set { _oCondicion = value; }
        }

        public bool Baja
        {
            get { return _baja; }
            set { _baja = value; }
        }

        public bool Entrega
        {
            get { return _Entrega; }
            set { _Entrega = value; }
        }

        public int IdSector { get; set; }
        public Sector oSector { get; set; }
        public Solicitante oSolicitante { get; set; }
        public string ServicioAux{ get; set; }
        public DateTime FechaEstimada { get; set; }
        public bool nuevoMovimiento { get; set; }
        public string letra { get; set; }
        public string fechaRegreso { get; set; }
        public int idSolicitudOrig { get; set; }

        #endregion

    }
}
