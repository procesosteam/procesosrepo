
using System;

namespace ProcesoPP.Model
{
	public class VerificaParte
	{
		#region Private Properties
		
		private int _IdVerifica;
		private int _IdParte;
        private decimal _Cantidad;
		
		#endregion
		
		#region Constructors
		
		public VerificaParte()
		{

		}
		
		public VerificaParte(int IdVerifica, int IdParte, decimal cantidad)
		{
			_IdVerifica = IdVerifica;
			_IdParte = IdParte;
            _Cantidad = cantidad;
		}

		#endregion
		
		#region Properties
		
		public int IdVerifica
		{
			get	{ return _IdVerifica; }
			set	{ _IdVerifica = value; }
		}
		
		public int IdParte
		{
			get	{ return _IdParte; }
			set	{ _IdParte = value; }
		}

        public decimal Cantidad
        {
            get { return _Cantidad; }
            set { _Cantidad = value; }
        }
		
		#endregion
	}
}
