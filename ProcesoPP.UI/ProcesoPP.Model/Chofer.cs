

using System;


namespace ProcesoPP.Model
{
	public class Chofer
	{
		#region Private Properties
		
		private int _IdChofer;
		private string _Nombre;
		private string _Apellido;
		private string _Direccion;
		private string _Telefono;
		private string _DNI;
		
		#endregion
		
		#region Constructors
		
		public Chofer()
		{

		}
		
		public Chofer(int IdChofer, string Nombre, string Apellido, string Direccion, string Telefono, string DNI)
		{
			_IdChofer = IdChofer;
			_Nombre = Nombre;
			_Apellido = Apellido;
			_Direccion = Direccion;
			_Telefono = Telefono;
			_DNI = DNI;
		}

		#endregion
		
		#region Properties
		
		public int IdChofer
		{
			get	{ return _IdChofer; }
			set	{ _IdChofer = value; }
		}
		
		public string Nombre
		{
			get	{ return _Nombre; }
			set	{ _Nombre = value; }
		}
		
		public string Apellido
		{
			get	{ return _Apellido; }
			set	{ _Apellido = value; }
		}
		
		public string Direccion
		{
			get	{ return _Direccion; }
			set	{ _Direccion = value; }
		}
		
		public string Telefono
		{
			get	{ return _Telefono; }
			set	{ _Telefono = value; }
		}
		
		public string DNI
		{
			get	{ return _DNI; }
			set	{ _DNI = value; }
		}
		
		#endregion
	}
}
