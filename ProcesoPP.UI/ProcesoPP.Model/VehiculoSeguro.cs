
using System;


namespace ProcesoPP.Model
{
	public class VehiculoSeguro
	{
		#region Private Properties
		
		private int _IdSeguro;
		private int _IdVehiculo;
		private bool _CompaniaAseg;
		private string _NroPoliza;
		private DateTime _VigenciaDesde;
		private DateTime _VigenciaHasta;
		
		#endregion
		
		#region Constructors
		
		public VehiculoSeguro()
		{

		}
		
		public VehiculoSeguro(int IdSeguro, int IdVehiculo, bool CompaniaAseg, string NroPoliza, DateTime VigenciaDesde, DateTime VigenciaHasta)
		{
			_IdSeguro = IdSeguro;
			_IdVehiculo = IdVehiculo;
			_CompaniaAseg = CompaniaAseg;
			_NroPoliza = NroPoliza;
			_VigenciaDesde = VigenciaDesde;
			_VigenciaHasta = VigenciaHasta;
		}

		#endregion
		
		#region Properties
		
		public int IdSeguro
		{
			get	{ return _IdSeguro; }
			set	{ _IdSeguro = value; }
		}
		
		public int IdVehiculo
		{
			get	{ return _IdVehiculo; }
			set	{ _IdVehiculo = value; }
		}
		
		public bool CompaniaAseg
		{
			get	{ return _CompaniaAseg; }
			set	{ _CompaniaAseg = value; }
		}
		
		public string NroPoliza
		{
			get	{ return _NroPoliza; }
			set	{ _NroPoliza = value; }
		}
		
		public DateTime VigenciaDesde
		{
			get	{ return _VigenciaDesde; }
			set	{ _VigenciaDesde = value; }
		}
		
		public DateTime VigenciaHasta
		{
			get	{ return _VigenciaHasta; }
			set	{ _VigenciaHasta = value; }
		}
		
		#endregion
	}
}
