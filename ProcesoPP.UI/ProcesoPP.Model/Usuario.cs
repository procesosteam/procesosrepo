
using System;

namespace ProcesoPP.Model
{
	public class Usuario
	{
		#region Private Properties
		
		private int _IdUsuario;
		private string _Nombre;
		private string _Apellido;
		private string _Telefono;
		private string _Direccion;
		private string _Clave;
        private string _UserName;
        private string _mail;

		#endregion
		
		#region Constructors
		
		public Usuario()
		{

		}
		
		public Usuario(int IdUsuario, string Nombre, string Apellido, string Telefono, string Direccion, string Clave, string UserName)
		{
			_IdUsuario = IdUsuario;
			_Nombre = Nombre;
			_Apellido = Apellido;
			_Telefono = Telefono;
			_Direccion = Direccion;
			_Clave = Clave;
            _UserName = UserName;
		}

		#endregion
		
		#region Properties
		
		public int IdUsuario
		{
			get	{ return _IdUsuario; }
			set	{ _IdUsuario = value; }
		}
		
		public string Nombre
		{
			get	{ return _Nombre; }
			set	{ _Nombre = value; }
		}
		
		public string Apellido
		{
			get	{ return _Apellido; }
			set	{ _Apellido = value; }
		}
		
		public string Telefono
		{
			get	{ return _Telefono; }
			set	{ _Telefono = value; }
		}
		
		public string Direccion
		{
			get	{ return _Direccion; }
			set	{ _Direccion = value; }
		}
		
		public string Clave
		{
			get	{ return _Clave; }
			set	{ _Clave = value; }
		}

        public string UserName
        {
            get { return _UserName; }
            set { _UserName = value; }
        }

        public TipoUsuario oTipoUsuario{ get; set; }

        public string Mail
        {
            get { return _mail; }
            set { _mail = value; }
        }
		
		#endregion
              
    }
}
