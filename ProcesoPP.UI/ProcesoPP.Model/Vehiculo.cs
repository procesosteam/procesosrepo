
using System;


namespace ProcesoPP.Model
{
	public class Vehiculo
	{
		#region Private Properties
		
		private int _IdVehiculo;
		private int _IdMarca;
		private string _Modelo;
		private int _Anio;
		private string _Patente;
		private int _IdEmpresa;
		private int _IdSector;
		private DateTime _FechaAltaEmpresa;
		private DateTime _FechaBajaEmpresa;
		private int _IdUsuario;
		private DateTime _FechaAlta;
		private string _Titular;
		private string _PotenciaMotor;
		private string _Color;
		private string _NroMotor;
		private string _NroChasis;
        private bool _Alta;
        private bool _Baja;
        private bool _EsEquipo;
        private VehiculoIdentificacion _vehiculoId;
		#endregion
		
		#region Constructors
		
		public Vehiculo()
		{
            _vehiculoId = new VehiculoIdentificacion();
		}
		
		public Vehiculo(int IdVehiculo, int IdMarca, string Modelo, int Anio, string Patente, int IdEmpresa, int IdSector, DateTime FechaAltaEmpresa, DateTime FechaBajaEmpresa, int IdUsuario, DateTime FechaAlta, string Titular, string PotenciaMotor, string Color, string NroMotor, string NroChasis, bool Alta, bool Baja,bool esEquipo)
		{
			_IdVehiculo = IdVehiculo;
			_IdMarca = IdMarca;
			_Modelo = Modelo;
			_Anio = Anio;
			_Patente = Patente;
			_IdEmpresa = IdEmpresa;
			_IdSector = IdSector;
			_FechaAltaEmpresa = FechaAltaEmpresa;
			_FechaBajaEmpresa = FechaBajaEmpresa;
			_IdUsuario = IdUsuario;
			_FechaAlta = FechaAlta;
			_Titular = Titular;
			_PotenciaMotor = PotenciaMotor;
			_Color = Color;
			_NroMotor = NroMotor;
			_NroChasis = NroChasis;
            _Alta = Alta;
            _Baja = Baja;
            _EsEquipo = esEquipo;
		}

		#endregion
		
		#region Properties
		
		public int IdVehiculo
		{
			get	{ return _IdVehiculo; }
			set	{ _IdVehiculo = value; }
		}
		
		public int IdMarca
		{
			get	{ return _IdMarca; }
			set	{ _IdMarca = value; }
		}
		
		public string Modelo
		{
			get	{ return _Modelo; }
			set	{ _Modelo = value; }
		}
		
		public int Anio
		{
			get	{ return _Anio; }
			set	{ _Anio = value; }
		}
		
		public string Patente
		{
			get	{ return _Patente; }
			set	{ _Patente = value; }
		}
		
		public int IdEmpresa
		{
			get	{ return _IdEmpresa; }
			set	{ _IdEmpresa = value; }
		}
		
		public int IdSector
		{
			get	{ return _IdSector; }
			set	{ _IdSector = value; }
		}
		
		public DateTime FechaAltaEmpresa
		{
			get	{ return _FechaAltaEmpresa; }
			set	{ _FechaAltaEmpresa = value; }
		}
		
		public DateTime FechaBajaEmpresa
		{
			get	{ return _FechaBajaEmpresa; }
			set	{ _FechaBajaEmpresa = value; }
		}
		
		public int IdUsuario
		{
			get	{ return _IdUsuario; }
			set	{ _IdUsuario = value; }
		}
		
		public DateTime FechaAlta
		{
			get	{ return _FechaAlta; }
			set	{ _FechaAlta = value; }
		}
		
		public string Titular
		{
			get	{ return _Titular; }
			set	{ _Titular = value; }
		}
		
		public string PotenciaMotor
		{
			get	{ return _PotenciaMotor; }
			set	{ _PotenciaMotor = value; }
		}
		
		public string Color
		{
			get	{ return _Color; }
			set	{ _Color = value; }
		}
		
		public string NroMotor
		{
			get	{ return _NroMotor; }
			set	{ _NroMotor = value; }
		}
		
		public string NroChasis
		{
			get	{ return _NroChasis; }
			set	{ _NroChasis = value; }
		}
        
        public bool Alta
        {
            get { return _Alta;}
            set { _Alta = value; }
        }

        public bool Baja
        {
            get { return _Baja; }
            set { _Baja = value; }
        }

        public bool EsEquipo
        {
            get { return _EsEquipo; }
            set { _EsEquipo = value; }
        }

        public VehiculoIdentificacion oVehiculoIdentificacion
        {
            get { return _vehiculoId; }
            set { _vehiculoId = value; }
        }
        
        #endregion

        public string Descripcion 
        {
            get { return oVehiculoIdentificacion.oCodificacion.Codigo + ' ' + oVehiculoIdentificacion.NroCodificacion;} 
        }
    }
}
