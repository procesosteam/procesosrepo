using System;


namespace ProcesoPP.Model
{
	public class PersonalDatosLaborales
	{
		#region Private Properties
		
		private int _IdPersonalDatosLaborales;
		private int _IdEmpresa;
		private DateTime _FechaAlta;
		private int _IdRelacion;
		private int _IdModalidad;
		private int _IdFuncion;
		private int _IdSector;
		private int _IdDiagrama;
		private int _IdBanco;
        private string _CBU;
        
		#endregion
		
		#region Constructors
		
		public PersonalDatosLaborales()
		{

		}
		
		public PersonalDatosLaborales(int IdPersonalDatosLaborales, int IdEmpresa, DateTime FechaAlta, int IdRelacion, int IdModalidad, int IdFuncion, int IdSector, int IdDiagrama, int IdBanco, string CBU)
		{
			_IdPersonalDatosLaborales = IdPersonalDatosLaborales;
			_IdEmpresa = IdEmpresa;
			_FechaAlta = FechaAlta;
			_IdRelacion = IdRelacion;
			_IdModalidad = IdModalidad;
			_IdFuncion = IdFuncion;
			_IdSector = IdSector;
			_IdDiagrama = IdDiagrama;
			_IdBanco = IdBanco;
            _CBU = CBU;
           
		}

		#endregion
		
		#region Properties
		
		public int IdPersonalDatosLaborales
		{
			get	{ return _IdPersonalDatosLaborales; }
			set	{ _IdPersonalDatosLaborales = value; }
		}
		
		public int IdEmpresa
		{
			get	{ return _IdEmpresa; }
			set	{ _IdEmpresa = value; }
		}
		
		public DateTime FechaAlta
		{
			get	{ return _FechaAlta; }
			set	{ _FechaAlta = value; }
		}
		
		public int IdRelacion
		{
			get	{ return _IdRelacion; }
			set	{ _IdRelacion = value; }
		}
		
		public int IdModalidad
		{
			get	{ return _IdModalidad; }
			set	{ _IdModalidad = value; }
		}
		
		public int IdFuncion
		{
			get	{ return _IdFuncion; }
			set	{ _IdFuncion = value; }
		}
		
		public int IdSector
		{
			get	{ return _IdSector; }
			set	{ _IdSector = value; }
		}
		
		public int IdDiagrama
		{
			get	{ return _IdDiagrama; }
			set	{ _IdDiagrama = value; }
		}
		
		public int IdBanco
		{
			get	{ return _IdBanco; }
			set	{ _IdBanco = value; }
		}

        public string CBU
        {
            get { return _CBU; }
            set { _CBU = value; }
        }

        

		#endregion
	}
}
