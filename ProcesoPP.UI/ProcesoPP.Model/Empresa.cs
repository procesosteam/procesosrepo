
using System;
using System.ComponentModel;

namespace ProcesoPP.Model
{
	public class Empresa
	{
		#region Private Properties
		
		private int _IdEmpresa;
		private string _Descripcion;
        private string _RazonSocial;
        private string _Domicilio;
        private string _idCondIVA;
        private string _CUIT;
        private bool _Interna;
		#endregion
		
		#region Constructors
		
		public Empresa()
		{

		}
		
		public Empresa(int IdEmpresa, string Descripcion)
		{
			_IdEmpresa = IdEmpresa;
			_Descripcion = Descripcion;
		}

		#endregion
		
		#region Properties
		
		public int IdEmpresa
		{
			get	{ return _IdEmpresa; }
			set	{ _IdEmpresa = value; }
		}
		
		public string Descripcion
		{
			get	{ return _Descripcion; }
			set	{ _Descripcion = value; }
		}

        public string RazonSocial
        {
            get { return _RazonSocial; }
            set { _RazonSocial = value; }
        }

        public string Domicilio
        {
            get { return _Domicilio; }
            set { _Domicilio= value; }
        }

        public string CondIVA
        {
            get { return _idCondIVA; }
            set { _idCondIVA = value; }
        }

        public bool Interna
        {
            get { return _Interna; }
            set { _Interna = value; }
        }

        public string CUIT
        {
            get { return _CUIT; }
            set { _CUIT = value; }
        }

        public string CodTango { get; set; }

        public bool Baja { get; set; }
        
        public enum IdCondicionIVA
        {
            [Description("Resp. Inscripto")]RI,
            [Description("Monotributista")]Monotributista,
            [Description("Exento")]Exento,
            [Description("Cons. Final")]CF,
            [Description("No Responsable")]NRI
        };

		#endregion
	}
}
