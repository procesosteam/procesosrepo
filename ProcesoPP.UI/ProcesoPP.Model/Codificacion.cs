
using System;


namespace ProcesoPP.Model
{
	public class Codificacion
	{
		#region Private Properties
		
		private int _IdCodificacion;
		private string _Descripcion;
        private string _Codigo;
        private bool _Equipo;

		#endregion
		
		#region Constructors
		
		public Codificacion()
		{

		}
		
		public Codificacion(int IdCodificacion, string Descripcion, string Codigo, bool Equipo)
		{
			_IdCodificacion = IdCodificacion;
			_Descripcion = Descripcion;
            _Codigo = Codigo;
            _Equipo = Equipo;
		}

		#endregion
		
		#region Properties
		
		public int IdCodificacion
		{
			get	{ return _IdCodificacion; }
			set	{ _IdCodificacion = value; }
		}
		
		public string Descripcion
		{
			get	{ return _Descripcion; }
			set	{ _Descripcion = value; }
		}

        public string Codigo
        {
            get { return _Codigo; }
            set { _Codigo = value;}
        }

        public bool Equipo
        {
            get { return _Equipo; }
            set { _Equipo = value; }
        }

		#endregion
	}
}
