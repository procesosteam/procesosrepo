
using System;


namespace ProcesoPP.Model
{
	public class CategoriaCarnet
	{
		#region Private Properties
		
		private int _IdCarnet;
		private string _Descripcion;
		
		#endregion
		
		#region Constructors
		
		public CategoriaCarnet()
		{

		}
		
		public CategoriaCarnet(int IdCarnet, string Descripcion)
		{
			_IdCarnet = IdCarnet;
			_Descripcion = Descripcion;
		}

		#endregion
		
		#region Properties
		
		public int IdCarnet
		{
			get	{ return _IdCarnet; }
			set	{ _IdCarnet = value; }
		}
		
		public string Descripcion
		{
			get	{ return _Descripcion; }
			set	{ _Descripcion = value; }
		}
		
		#endregion
	}
}
