

using System;


namespace ProcesoPP.Model
{
	public class Trailer
	{
		#region Private Properties
		
		private int _IdTrailer;
		private string _Descripcion;
		private int _IdEspecificacion;
        private EspecificacionTecnica _oEspecificacion;

		#endregion
		
		#region Constructors
		
		public Trailer()
		{

		}
		
		public Trailer(int IdTrailer, string Descripcion, int IdEspecificacion)
		{
			_IdTrailer = IdTrailer;
			_Descripcion = Descripcion;
			_IdEspecificacion = IdEspecificacion;
		}

		#endregion
		
		#region Properties
		
		public int IdTrailer
		{
			get	{ return _IdTrailer; }
			set	{ _IdTrailer = value; }
		}
		
		public string Descripcion
		{
			get	{ return _Descripcion; }
			set	{ _Descripcion = value; }
		}
		
		public int IdEspecificacion
		{
			get	{ return _IdEspecificacion; }
			set	{ _IdEspecificacion = value; }
		}

        public EspecificacionTecnica oEspecificacion
        {
            get { return _oEspecificacion; }
            set { _oEspecificacion = value; }
        }
		#endregion
	}
}
