

using System;


namespace ProcesoPP
{
	public class TipoServicioEspecificacion
	{
		#region Private Properties
		
		private int _IdTipoEspecificacion;
		private int _IdTipoServicio;
		private int _IdEspecificacion;
		
		#endregion
		
		#region Constructors
		
		public TipoServicioEspecificacion()
		{

		}
		
		public TipoServicioEspecificacion(int IdTipoEspecificacion, int IdTipoServicio, int IdEspecificacion)
		{
			_IdTipoEspecificacion = IdTipoEspecificacion;
			_IdTipoServicio = IdTipoServicio;
			_IdEspecificacion = IdEspecificacion;
		}

		#endregion
		
		#region Properties
		
		public int IdTipoEspecificacion
		{
			get	{ return _IdTipoEspecificacion; }
			set	{ _IdTipoEspecificacion = value; }
		}
		
		public int IdTipoServicio
		{
			get	{ return _IdTipoServicio; }
			set	{ _IdTipoServicio = value; }
		}
		
		public int IdEspecificacion
		{
			get	{ return _IdEspecificacion; }
			set	{ _IdEspecificacion = value; }
		}
		
		#endregion
	}
}
