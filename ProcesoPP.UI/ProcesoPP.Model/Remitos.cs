﻿
using System;
using ProcesoPP.Model;
using System.Collections.Generic;


namespace ProcesoPP.Model
{
	public class Remitos
	{
		#region Private Properties
		
		private int _IdRemito;
		private int _NroRemito;
		private DateTime _Fecha;
		private ParteEntrega _IdParte;
		private int _Cantidad;
		private string _Observacion;
		private bool _Anulado;
		private bool _Impreso;
		private int _IdUsuario;
		private DateTime _FechaAlta;
        private List<RemitoDetalle> _oListDetalles;
        private string _nroOC;
        private string _nroPO;
 
		#endregion
		
		#region Constructors
		
		public Remitos()
		{
            oParte = new ParteEntrega();
            oListDetalles = new List<RemitoDetalle>();
		}
		
		public Remitos(int IdRemito, int NroRemito, DateTime Fecha, ParteEntrega IdParte, int Cantidad, string Observacion, bool Anulado, bool Impreso, int IdUsuario, DateTime FechaAlta)
		{
			_IdRemito = IdRemito;
			_NroRemito = NroRemito;
			_Fecha = Fecha;
			_IdParte = IdParte;
			_Cantidad = Cantidad;
			_Observacion = Observacion;
			_Anulado = Anulado;
			_Impreso = Impreso;
			_IdUsuario = IdUsuario;
			_FechaAlta = FechaAlta;
		}

		#endregion
		
		#region Properties
		
		public int IdRemito
		{
			get	{ return _IdRemito; }
			set	{ _IdRemito = value; }
		}
		
		public int NroRemito
		{
			get	{ return _NroRemito; }
			set	{ _NroRemito = value; }
		}
		
		public DateTime Fecha
		{
			get	{ return _Fecha; }
			set	{ _Fecha = value; }
		}
		
		public ParteEntrega oParte
		{
			get	{ return _IdParte; }
			set	{ _IdParte = value; }
		}
		
		public int Cantidad
		{
			get	{ return _Cantidad; }
			set	{ _Cantidad = value; }
		}
		
		public string Observacion
		{
			get	{ return _Observacion; }
			set	{ _Observacion = value; }
		}
		
		public bool Anulado
		{
			get	{ return _Anulado; }
			set	{ _Anulado = value; }
		}

        public DateTime FechaAnulado { get; set; }
		
		public bool Impreso
		{
			get	{ return _Impreso; }
			set	{ _Impreso = value; }
		}
		
		public int IdUsuario
		{
			get	{ return _IdUsuario; }
			set	{ _IdUsuario = value; }
		}
		
		public DateTime FechaAlta
		{
			get	{ return _FechaAlta; }
			set	{ _FechaAlta = value; }
		}
        public List<RemitoDetalle> oListDetalles
        {
            get { return _oListDetalles; }
            set { _oListDetalles = value; }
        }

        public string CCosto
        {
            get { return _nroPO; }
            set { _nroPO = value; }
        }

        public string NroOC
        {
            get { return _nroOC; }
            set { _nroOC = value; }
        }

        public string Leyenda1 { get; set; }
        public string Leyenda2 { get; set; }
        public string Leyenda3 { get; set; }
        public string Leyenda4 { get; set; }

		#endregion
	}
}
