
using System;

namespace ProcesoPP.Model
{
	public class Equipos
	{
		#region Private Properties
		
		private int _IdEquipo;
		private string _Descripcion;
		
		#endregion
		
		#region Constructors
		
		public Equipos()
		{

		}
		
		public Equipos(int IdEquipo, string Descripcion)
		{
			_IdEquipo = IdEquipo;
			_Descripcion = Descripcion;
		}

		#endregion
		
		#region Properties
		
		public int IdEquipo
		{
			get	{ return _IdEquipo; }
			set	{ _IdEquipo = value; }
		}
		
		public string Descripcion
		{
			get	{ return _Descripcion; }
			set	{ _Descripcion = value; }
		}

        public int idCliente { get; set; }

		#endregion
	}
}
