
///////////////////////////////////////////////////////////////////////////
//
// Generated by MyGeneration Version # (1.3.0.9) 
//
///////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;


namespace ProcesoPP.Model
{
	public class AcuerdoTitulo
	{
		#region Private Properties
		
		private int _IdAcuerdoTitulo;
		private string _Titulo;
		private string _Detalle;
		private int _IdAcuerdo;
		
		#endregion
		
		#region Constructors
		
		public AcuerdoTitulo()
		{

		}
		
		public AcuerdoTitulo(int IdAcuerdoTitulo, string Titulo, string Detalle, int IdAcuerdo)
		{
			_IdAcuerdoTitulo = IdAcuerdoTitulo;
			_Titulo = Titulo;
			_Detalle = Detalle;
			_IdAcuerdo = IdAcuerdo;
		}

		#endregion
		
		#region Properties
		
		public int IdAcuerdoTitulo
		{
			get	{ return _IdAcuerdoTitulo; }
			set	{ _IdAcuerdoTitulo = value; }
		}
		
		public string Titulo
		{
			get	{ return _Titulo; }
			set	{ _Titulo = value; }
		}
		
		public string Detalle
		{
			get	{ return _Detalle; }
			set	{ _Detalle = value; }
		}
		
		public int IdAcuerdo
		{
			get	{ return _IdAcuerdo; }
			set	{ _IdAcuerdo = value; }
		}
		
		#endregion
	}

    public class AcuerdoTituloObj
    {
        public AcuerdoTituloList acuerdoTitulos { get; set; }
    }

    public class AcuerdoTituloList
    {
        public List<AcuerdoTitulo> acuerdoTitulo { get; set; }
    }

}
