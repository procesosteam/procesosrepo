

using System;

namespace ProcesoPP.Model
{
	public class Movimiento
	{
		#region Private Properties
		
		private int _IdMovimiento;
		private string _Origen;
		private string _Destino;
		private int _KmsEstimados;
		private DateTime _FechaEstimada;
		
		#endregion
		
		#region Constructors
		
		public Movimiento()
		{
            oLugarDestino = new Lugar();
            oLugarOrigen = new Lugar();
		}
		
		public Movimiento(int IdMovimiento, string Origen, string Destino, int KmsEstimados, DateTime FechaEstimada, Lugar lugarDesde, Lugar lugarHasta)
		{
			_IdMovimiento = IdMovimiento;
			_Origen = Origen;
			_Destino = Destino;
			_KmsEstimados = KmsEstimados;
			_FechaEstimada = FechaEstimada;
            oLugarDestino = lugarHasta;
            oLugarOrigen = lugarDesde;
		}

		#endregion
		
		#region Properties
		
		public int IdMovimiento
		{
			get	{ return _IdMovimiento; }
			set	{ _IdMovimiento = value; }
		}
		
		public string PozoOrigen
		{
			get	{ return _Origen; }
			set	{ _Origen = value; }
		}
		
		public string PozoDestino
		{
			get	{ return _Destino; }
			set	{ _Destino = value; }
		}
		
		public int KmsEstimados
		{
			get	{ return _KmsEstimados; }
			set	{ _KmsEstimados = value; }
		}
		
		public DateTime FechaEstimada
		{
			get	{ return _FechaEstimada; }
			set	{ _FechaEstimada = value; }
		}

        public Lugar oLugarOrigen{ get; set; }
        public Lugar oLugarDestino { get; set; }

		#endregion
	}
}
