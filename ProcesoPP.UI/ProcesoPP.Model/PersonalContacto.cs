
using System;


namespace ProcesoPP.Model
{
	public class PersonalContacto
	{
		#region Private Properties
		
		private int _IdPersonalContacto;
		private string _Apellido;
		private string _Nombre;
		private string _Relacion;
		private string _Telefono;
		
		#endregion
		
		#region Constructors
		
		public PersonalContacto()
		{

		}

        public PersonalContacto(int IdPersonalContacto, string Apellido, string Nombre, string Relacion, string Telefono)
		{
			_IdPersonalContacto = IdPersonalContacto;
			_Apellido = Apellido;
			_Nombre = Nombre;
			_Relacion = Relacion;
			_Telefono = Telefono;
		}

		#endregion
		
		#region Properties
		
		public int IdPersonalContacto
		{
			get	{ return _IdPersonalContacto; }
			set	{ _IdPersonalContacto = value; }
		}
		
		public string Apellido
		{
			get	{ return _Apellido; }
			set	{ _Apellido = value; }
		}
		
		public string Nombre
		{
			get	{ return _Nombre; }
			set	{ _Nombre = value; }
		}
		
		public string Relacion
		{
			get	{ return _Relacion; }
			set	{ _Relacion = value; }
		}
		
		public string Telefono
		{
			get	{ return _Telefono; }
			set	{ _Telefono = value; }
		}
		
		#endregion
	}
}
