﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProcesoPP.Model
{
    public class Lugar
    {
        #region Private Properties
		
		private int _IdLugar;
		private string _Descripcion;
		
		#endregion
		
		#region Constructors
		
		public Lugar()
		{

		}
		
		public Lugar(int IdLugar, string Descripcion)
		{
			_IdLugar = IdLugar;
			_Descripcion = Descripcion;
		}

		#endregion
		
		#region Properties
		
		public int IdLugar
		{
			get	{ return _IdLugar; }
			set	{ _IdLugar = value; }
		}
		
		public string Descripcion
		{
			get	{ return _Descripcion; }
			set	{ _Descripcion = value; }
		}
		
		#endregion
    }
}
