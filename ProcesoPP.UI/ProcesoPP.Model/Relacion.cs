using System;


namespace ProcesoPP.Model
{
	public class Relacion
	{
		#region Private Properties
		
		private int _IdRelacion;
		private string _Descripcion;
		private bool _Baja;
		
		#endregion
		
		#region Constructors
		
		public Relacion()
		{

		}
		
		public Relacion(int IdRelacion, string Descripcion)
		{
			_IdRelacion = IdRelacion;
			_Descripcion = Descripcion;
			_Baja = false;
		}

		#endregion
		
		#region Properties
		
		public int IdRelacion
		{
			get	{ return _IdRelacion; }
			set	{ _IdRelacion = value; }
		}
		
		public string Descripcion
		{
			get	{ return _Descripcion; }
			set	{ _Descripcion = value; }
		}
		
		public bool Baja
		{
			get	{ return _Baja; }
			set	{ _Baja = value; }
		}
		
		#endregion
	}
}
