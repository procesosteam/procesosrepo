
using System;
using System.Collections.Generic;


namespace ProcesoPP.Model
{
	public class AcuerdoServicio
	{
		#region Private Properties
		
		private int _IdAcuerdoServicio;
		private int _IdAcuerdo;
		private int _IdTipoServicio;
        private TipoServicio _oTipoServicio;

        private List<AcuerdoConceptoCosto> _oListconceptos;
		#endregion
		
		#region Constructors
		
		public AcuerdoServicio()
		{
            oListConcepto = new List<AcuerdoConceptoCosto>();
		}

        public AcuerdoServicio(int IdAcuerdoServicio, int IdAcuerdo, int IdTipoServicio, List<AcuerdoConceptoCosto> oListcon, TipoServicio otipoServicio)
		{
			_IdAcuerdoServicio = IdAcuerdoServicio;
			_IdAcuerdo = IdAcuerdo;
			_IdTipoServicio = IdTipoServicio;
			_oListconceptos = oListcon;
            _oTipoServicio = otipoServicio;
		}

		#endregion
		
		#region Properties
		
		public int IdAcuerdoServicio
		{
			get	{ return _IdAcuerdoServicio; }
			set	{ _IdAcuerdoServicio = value; }
		}
		
		public int IdAcuerdo
		{
			get	{ return _IdAcuerdo; }
			set	{ _IdAcuerdo = value; }
		}
		
		public int IdTipoServicio
		{
			get	{ return _IdTipoServicio; }
			set	{ _IdTipoServicio = value; }
		}

        public TipoServicio oTipoServicio
        {
            get { return _oTipoServicio; }
            set { _oTipoServicio = value; }
        }
	
        public List<AcuerdoConceptoCosto> oListConcepto
        {
            get { return _oListconceptos; }
            set { _oListconceptos = value; }
        }

        public string requiere { get; set; }

		#endregion
	}
}
