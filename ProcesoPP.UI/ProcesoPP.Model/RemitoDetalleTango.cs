﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProcesoPP.Model
{
    public class RemitoDetalleTango
    {
        public string codTango { get; set; }

        public string Descripcion { get; set; }

        public string DescripAdic { get; set; }

        public int nRenglon { get; set; }

        public int cantidad { get; set; }

        public decimal PrecioUnit { get; set; }

        public decimal PrecioTotal { get; set; }

        public string nroRemito { get; set; }

    }
}
