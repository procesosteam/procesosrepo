﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProcesoPP.Model
{
    public class SolicitudEspecificacion
    {
        public int idEspecificacion{ get; set; }
        public int idSolicitud { get; set; }
    }
}
