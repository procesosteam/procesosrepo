
using System;
using ProcesoPP.Model;

namespace ProcesoPP
{
	public class RemitoDetalle
	{
		#region Private Properties
		
		private int _IdRemitoDetalle;
		private int _IdRemito;
		private int _Cantidad;
		private string _Detalle;
		private decimal _PrecioUnit;
		private decimal _PrecioTotal;
        private int _IdParte;
		
		#endregion
		
		#region Constructors
		
		public RemitoDetalle()
		{

		}
		
		public RemitoDetalle(int IdRemitoDetalle, int IdRemito, int Cantidad, string Detalle, decimal PrecioUnit, decimal PrecioTotal)
		{
			_IdRemitoDetalle = IdRemitoDetalle;
			_IdRemito = IdRemito;
			_Cantidad = Cantidad;
			_Detalle = Detalle;
			_PrecioUnit = PrecioUnit;
			_PrecioTotal = PrecioTotal;
		}

		#endregion
		
		#region Properties
		
		public int IdRemitoDetalle
		{
			get	{ return _IdRemitoDetalle; }
			set	{ _IdRemitoDetalle = value; }
		}
		
		public int IdRemito
		{
			get	{ return _IdRemito; }
			set	{ _IdRemito = value; }
		}
		
		public int Cantidad
		{
			get	{ return _Cantidad; }
			set	{ _Cantidad = value; }
		}
		
		public string Detalle
		{
			get	{ return _Detalle; }
			set	{ _Detalle = value; }
		}
		
		public decimal PrecioUnit
		{
			get	{ return _PrecioUnit; }
			set	{ _PrecioUnit = value; }
		}
		
		public decimal PrecioTotal
		{
			get	{ return _PrecioTotal; }
			set	{ _PrecioTotal = value; }
		}

        public int IdParte
        {
            get { return _IdParte; }
            set { _IdParte = value; }
        }

        //public Remitos oRemito { get; set; }

        //public string codTango { get; set; }

        //public string DescripAdic { get; set; }

        //public string nRenglon{ get; set; }


		#endregion
	}
}
