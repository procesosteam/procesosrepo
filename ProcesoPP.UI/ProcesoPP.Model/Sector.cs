using System;

namespace ProcesoPP.Model
{
	public class Sector
	{
		#region Private Properties
		
		private int _IdSector;
		private string _Descripcion;
        private int _IdEmpresa;
		
		#endregion
		
		#region Constructors
		
		public Sector()
		{

		}

        public Sector(int IdSector, string Descripcion, int IdEmpresa)
		{
			_IdSector = IdSector;
			_Descripcion = Descripcion;
            _IdEmpresa = IdEmpresa;
		}

		#endregion
		
		#region Properties
		
		public int IdSector
		{
			get	{ return _IdSector; }
			set	{ _IdSector = value; }
		}
		
		public string Descripcion
		{
			get	{ return _Descripcion; }
			set	{ _Descripcion = value; }
		}

        public int IdEmpresa
        {
            get { return _IdEmpresa; }
            set { _IdEmpresa = value; }
        }

        public Empresa oEmpresa{ get; set; }

		#endregion
	}
}
