
using System;

namespace ProcesoPP.Model
{
	public class Condicion
	{
		#region Private Properties
		
		private int _IdCondicion;
		private string _Descripcion;
		
		#endregion
		
		#region Constructors
		
		public Condicion()
		{

		}
		
		public Condicion(int IdCondicion, string Descripcion)
		{
			_IdCondicion = IdCondicion;
			_Descripcion = Descripcion;
		}

		#endregion
		
		#region Properties
		
		public int IdCondicion
		{
			get	{ return _IdCondicion; }
			set	{ _IdCondicion = value; }
		}
		
		public string Descripcion
		{
			get	{ return _Descripcion; }
			set	{ _Descripcion = value; }
		}


        public enum id
        {
            diario = 1,
            mensual = 2
        }

		#endregion
	}
}
