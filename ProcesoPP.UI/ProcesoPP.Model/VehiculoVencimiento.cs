
using System;


namespace ProcesoPP.Model
{
	public class VehiculoVencimiento
	{
		#region Private Properties
		
		private int _IdVencimiento;
		private int _IdVehiculo;
		private DateTime _VencimientoCedula;
		private DateTime _VencimientoPatente;
		private DateTime _VencimientoLeasing;
		private string _NroVTV;
		private DateTime _VencimientoVTV;
		private bool _RUTA;
		private string _NroExpediente;
		private DateTime _Emision;
		private DateTime _Vencimiento;
		
		#endregion
		
		#region Constructors
		
		public VehiculoVencimiento()
		{

		}
		
		public VehiculoVencimiento(int IdVencimiento, int IdVehiculo, DateTime VencimientoCedula, DateTime VencimientoPatente, DateTime VencimientoLeasing, string NroVTV, DateTime VencimientoVTV, bool RUTA, string NroExpediente, DateTime Emision, DateTime Vencimiento)
		{
			_IdVencimiento = IdVencimiento;
			_IdVehiculo = IdVehiculo;
			_VencimientoCedula = VencimientoCedula;
			_VencimientoPatente = VencimientoPatente;
			_VencimientoLeasing = VencimientoLeasing;
			_NroVTV = NroVTV;
			_VencimientoVTV = VencimientoVTV;
			_RUTA = RUTA;
			_NroExpediente = NroExpediente;
			_Emision = Emision;
			_Vencimiento = Vencimiento;
		}

		#endregion
		
		#region Properties
		
		public int IdVencimiento
		{
			get	{ return _IdVencimiento; }
			set	{ _IdVencimiento = value; }
		}
		
		public int IdVehiculo
		{
			get	{ return _IdVehiculo; }
			set	{ _IdVehiculo = value; }
		}
		
		public DateTime VencimientoCedula
		{
			get	{ return _VencimientoCedula; }
			set	{ _VencimientoCedula = value; }
		}
		
		public DateTime VencimientoPatente
		{
			get	{ return _VencimientoPatente; }
			set	{ _VencimientoPatente = value; }
		}
		
		public DateTime VencimientoLeasing
		{
			get	{ return _VencimientoLeasing; }
			set	{ _VencimientoLeasing = value; }
		}
		
		public string NroVTV
		{
			get	{ return _NroVTV; }
			set	{ _NroVTV = value; }
		}
		
		public DateTime VencimientoVTV
		{
			get	{ return _VencimientoVTV; }
			set	{ _VencimientoVTV = value; }
		}
		
		public bool RUTA
		{
			get	{ return _RUTA; }
			set	{ _RUTA = value; }
		}
		
		public string NroExpediente
		{
			get	{ return _NroExpediente; }
			set	{ _NroExpediente = value; }
		}
		
		public DateTime Emision
		{
			get	{ return _Emision; }
			set	{ _Emision = value; }
		}
		
		public DateTime Vencimiento
		{
			get	{ return _Vencimiento; }
			set	{ _Vencimiento = value; }
		}
		
		#endregion
	}
}
