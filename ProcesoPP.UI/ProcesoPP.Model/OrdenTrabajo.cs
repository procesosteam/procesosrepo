

using System;
using System.Collections.Generic;

namespace ProcesoPP.Model
{
	public class OrdenTrabajo
	{
		#region Private Properties
		
		private int _IdOrden;
		private int _IdTransporte;
		private int _IdEquipo;
		private DateTime _Horario;
		private string _Salida;
		private string _Llegada;
		private decimal _KmsRecorridos;
		private int _IdSolicitudCliente;
		private string _Controlo;
		private string _Verifico;
		private int _IdVehiculo;
        private int _IdAcompanante;
        private int _IdChofer;
        private bool _baja;
        private SolicitudCliente _oSolicitudCliente;
        private bool _Entraga;
        private Vehiculo _oTransporte;
        private Vehiculo _oVehiculo;

		#endregion
		
		#region Constructors
		
		public OrdenTrabajo()
		{
            oListOTIConcepto = new List<OrdenTrabajoConcepto>();
            oListOTIDetalle = new List<OrdenTrabajoDetalle>();
		}
		
		public OrdenTrabajo(int IdOrden, int IdTransporte, int IdEquipo, DateTime Horario, string Salida, string Llegada, decimal KmsRecorridos, int IdSolicitudCliente, string Controlo, string Verifico, int IdVehiculo)
		{
			_IdOrden = IdOrden;
			_IdTransporte = IdTransporte;
			_IdEquipo = IdEquipo;
			_Horario = Horario;
			_Salida = Salida;
			_Llegada = Llegada;
			_KmsRecorridos = KmsRecorridos;
			_IdSolicitudCliente = IdSolicitudCliente;
			_Controlo = Controlo;
			_Verifico = Verifico;
			_IdVehiculo = IdVehiculo;
		}

		#endregion
		
		#region Properties
		
		public int IdOrden
		{
			get	{ return _IdOrden; }
			set	{ _IdOrden = value; }
		}
		/// <summary>
        /// idTransporte, es el idVehiculo(2016) con el que se hace el movimiento, si el servicio lo requiere
		/// </summary>
		public int IdTransporte
		{
			get	{ return _IdTransporte; }
			set	{ _IdTransporte = value; }
		}
		/// <summary>
		/// IdEquipo, es el idCuadrilla (p/reutilizar campos que no se usaban), si el servicio lo requiere
		/// </summary>
		public int IdEquipo
		{
			get	{ return _IdEquipo; }
			set	{ _IdEquipo = value; }
		}

        public int IdAcompanante
        {
            get { return _IdAcompanante; }
            set { _IdAcompanante = value; }
        }
        
        public int IdChofer
        {
            get { return _IdChofer; }
            set { _IdChofer = value; }
        }

		public DateTime Horario
		{
			get	{ return _Horario; }
			set	{ _Horario = value; }
		}
		
		public string Salida
		{
			get	{ return _Salida; }
			set	{ _Salida = value; }
		}
		
		public string Llegada
		{
			get	{ return _Llegada; }
			set	{ _Llegada = value; }
		}
		
		public decimal KmsRecorridos
		{
			get	{ return _KmsRecorridos; }
			set	{ _KmsRecorridos = value; }
		}
		
		public int IdSolicitudCliente
		{
			get	{ return _IdSolicitudCliente; }
			set	{ _IdSolicitudCliente = value; }
		}
				
		public string Controlo
		{
			get	{ return _Controlo; }
			set	{ _Controlo = value; }
		}
		
		public string Verifico
		{
			get	{ return _Verifico; }
			set	{ _Verifico = value; }
		}
		/// <summary>
		/// idVehiculo, en la base de datos es idTrailer, que en realidad es el id de Equipo que se mueve, segun el servicio
		/// </summary>
		public int IdVehiculo
		{
			get	{ return _IdVehiculo; }
			set	{ _IdVehiculo = value; }
		}

        public bool Baja
        {
            get { return _baja; }
            set { _baja = value; }
        }

        public SolicitudCliente oSolicitudCliente
        {
            get { return _oSolicitudCliente; }
            set { _oSolicitudCliente = value; }
        }
        
        public bool Entraga
        {
            get { return _Entraga; }
            set { _Entraga = value; }
        }

        public Vehiculo oTransporte
        {
            get { return _oTransporte; }
            set { _oTransporte = value; }
        }

        public Vehiculo oVehiculo
        {
            get { return _oVehiculo; }
            set { _oVehiculo = value; }
        }

        public List<OrdenTrabajoConcepto> oListOTIConcepto { get; set; }
        public List<OrdenTrabajoDetalle> oListOTIDetalle { get; set; }

        public Personal oChofer { get; set; }
        public Personal oAcompanante { get; set; }
        public Cuadrilla oCuadrilla { get; set; }

		#endregion
	}
}
