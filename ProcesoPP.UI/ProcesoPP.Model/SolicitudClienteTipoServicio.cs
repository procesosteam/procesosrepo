﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProcesoPP.Model
{
    public class SolicitudClienteTipoServicio
    {
        #region Private Properties

        private int _IdSolicitudTipoServicio;
        private int _IdSolicitudCliente;
        private int _IdTipoServicio;
        private int _IdEspecificacion;

        #endregion

        #region Constructors

        public SolicitudClienteTipoServicio()
        {

        }

        public SolicitudClienteTipoServicio(int IdSolicitudTipoServicio, int IdSolicitudCliente, int IdTipoServicio, int IdEspecificacion)
        {
            _IdSolicitudTipoServicio = IdSolicitudTipoServicio;
            _IdSolicitudCliente = IdSolicitudCliente;
            _IdTipoServicio = IdTipoServicio;
            _IdEspecificacion = IdEspecificacion;
        }

        #endregion

        #region Properties

        public int IdSolicitudTipoServicio
        {
            get { return _IdSolicitudTipoServicio; }
            set { _IdSolicitudTipoServicio = value; }
        }

        public int IdSolicitudCliente
        {
            get { return _IdSolicitudCliente; }
            set { _IdSolicitudCliente = value; }
        }

        public int IdTipoServicio
        {
            get { return _IdTipoServicio; }
            set { _IdTipoServicio = value; }
        }

        public int IdEspecificacion
        {
            get { return _IdEspecificacion; }
            set { _IdEspecificacion = value; }
        }

        #endregion
    }
}


