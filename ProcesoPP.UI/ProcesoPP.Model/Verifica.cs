
using System;


namespace ProcesoPP.Model
{
	public class Verifica
	{
		#region Private Properties

        private int _IdVerifica;
        private string _Descripcion;
		
		#endregion
		
		#region Constructors
		
		public Verifica()
		{

		}

        public Verifica(int IdVerifica, string Descripcion)
		{
			_IdVerifica = IdVerifica;
			_Descripcion = Descripcion;
		}

		#endregion
		
		#region Properties
		
		public int IdVerifica
		{
			get	{ return _IdVerifica; }
			set	{ _IdVerifica = value; }
		}
		
		public string Descripcion
		{
			get	{ return _Descripcion; }
			set	{ _Descripcion = value; }
		}
		
		#endregion
	}
}
