﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using ProcesoPP.SqlServerLibrary;

namespace ProcesoPP.Auditoria
{
    public partial class _Default : System.Web.UI.Page
    {

        #region EVENTOS

        protected void Page_Load(object sender, EventArgs e)
        {
           
        }


        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                CargarGrilla();
            }
            catch (Exception ex)
            {
                //Script.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }


        protected void gvMovimientos_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvMovimientos.PageIndex = e.NewPageIndex;
            CargarGrilla();
        }

        #endregion

        #region METODOS

     
        private void InitScreen()
        {
            
        }

        private void CargarGrilla()
        {
            DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(),
                                                        "AuditoriaGetByFilter",
                                                        SqlImplHelper.getNullIfEmpty(txtNroSolicitud.Text),
                                                        SqlImplHelper.getNullIfEmpty(txtFecha.Text),
                                                        SqlImplHelper.getNullIfEmpty(txtFechaHasta.Text)).Tables[0];

            gvMovimientos.DataSource = dt;
            gvMovimientos.DataBind();

        }

        #endregion


    }
}
