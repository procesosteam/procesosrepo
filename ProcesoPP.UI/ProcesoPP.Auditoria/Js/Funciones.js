﻿function CambiarConfiguracionCalendario(CajaTexto, Boton) {
    var contenedor = "ctl00_ContentPlaceHolder1_";
    CajaTexto = document.getElementById(contenedor + CajaTexto);
    Calendar.setup({ inputField: CajaTexto,      // id del campo de texto
        ifFormat: "%d/%m/%Y",       // formato de la fecha, cuando se escriba en el campo de texto
        button: Boton   // el id del botón que lanzará el calendario       
    });
}

function Tildar(check) {
    check = check.getElementsByTagName('input');
    for (i = 1; i < check.length; i++) {
        if (check[0].checked) {
            check[i].checked = false;
        }
    }
}
    