﻿<%@ Page Title="Página principal" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeBehind="Default.aspx.cs" Inherits="ProcesoPP.Auditoria._Default" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">

    <div id="DivFiltro">

<div id="apertura">Reporte Movimientos</div>  
<div class="solicitud100">
    <div class="sol_tit50"><label>Fecha Desde</label></div>
    <div class="sol_campo30"><asp:TextBox CssClass="sol_campo25" runat="server" id="txtFecha" /></div>
    <div class="sol_fecha"><img id="btnFecha" alt="calendario" src="img/calendario.gif" style="width:20px; height:20px; cursor:pointer;" onmouseover="CambiarConfiguracionCalendario('txtFecha',this);"/></div>
    
   <div class="sol_fecha"></div>
   
    <div class="sol_tit50"> <label>Fecha Hasta</label></div>
    <div class="sol_campo30"><asp:TextBox CssClass="sol_campo25" runat="server" id="txtFechaHasta" /></div>
    <div class="sol_fecha"><img id="btnFechaHasta" alt="calendario" src="img/calendario.gif" style="width:20px; height:20px; cursor:pointer;" onmouseover="CambiarConfiguracionCalendario('txtFechaHasta',this);"/></div>
</div>
<div class="solicitud100sin">    
   <div class="sol_tit50"><label>N° Solicitud</label></div>
    <div class="sol_campo50"><asp:TextBox CssClass="sol_campo25" runat="server" id="txtNroSolicitud" onkeypress="return ValidarNumeroEntero(event,this);"/></div>
 
    <div class="sol_btn">
        <asp:Button runat="server" id="btnBuscar" cssclass="btn_form"
              Text="Buscar" onclick="btnBuscar_Click" />
    </div>
</div>

</div>

<asp:GridView ID="gvMovimientos" runat="server" AutoGenerateColumns="true" 
    DataMember="idCondicion" HeaderStyle-CssClass="tabla_font" 
        RowStyle-CssClass= "tabla_font2" CssClass="tabla3" 
        AllowPaging="true" PageSize="20" 
        onpageindexchanging="gvMovimientos_PageIndexChanging" PagerStyle-CssClass="tabla_font">
</asp:GridView>

</asp:Content>
