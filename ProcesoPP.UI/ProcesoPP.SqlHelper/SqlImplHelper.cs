﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;

namespace ProcesoPP.SqlServerLibrary
{
    public class SqlImplHelper
    {
        private static string _connectionString = null;
        private static SqlTransaction _transaction = null;
        private static SqlConnection _connection = null;
                
        public static string getConnectionString()
        {
            if (_connectionString == null)
            {
                _connectionString = ConfigurationManager.ConnectionStrings["CadenaConexion"].ConnectionString;
            }

            return _connectionString;
        }

        public static SqlTransaction getTransaction()
        {
            try
            {
                if (_transaction == null)
                {
                    _connection = new SqlConnection(getConnectionString());
                    _connection.Open();
                    _transaction = _connection.BeginTransaction(IsolationLevel.ReadCommitted);
                }

                return _transaction;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void commitTransaction()
        {
            try
            {
                if (_transaction != null)
                {
                    _transaction.Commit();
                    _connection.Close();
                    _connection = null;
                    _transaction = null;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void rollbackTransaction()
        {
            try
            {
                if (_transaction != null)
                {
                    _transaction.Rollback();
                    _connection.Close();
                    _connection = null;
                    _transaction = null;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /*CONEXION TANGO*/
        private static string _connStrTango = null;
        private static SqlTransaction _transTango = null;
        private static SqlConnection _connTango = null;

        public static string getConnectionStringTango()
        {
            if (_connStrTango == null)
            {
                _connStrTango = ConfigurationManager.ConnectionStrings["CadenaConexionTango"].ConnectionString;
            }

            return _connStrTango;
        }

        public static SqlTransaction getTransactionTango()
        {
            try
            {
                if (_transTango == null)
                {
                    _connTango = new SqlConnection(getConnectionStringTango());
                    _connTango.Open();
                    _transTango = _connTango.BeginTransaction(IsolationLevel.ReadCommitted);
                }

                return _transTango;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void commitTransactionTango()
        {
            try
            {
                if (_transTango != null)
                {
                    _transTango.Commit();
                    _connTango.Close();
                    _connTango = null;
                    _transTango = null;
                }
            }
            catch (Exception ex)
            {
                ex.Source = "CONEXION CON TANGO";
                throw ex;
            }
        }

        public static void rollbackTransactionTango()
        {
            try
            {
                if (_transTango != null)
                {
                    _transTango.Rollback();
                    _connTango.Close();
                    _connTango = null;
                    _transTango = null;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        
        public static DateTime getMinValueIfNull(object drValue)
        {
            return Convert.IsDBNull(drValue) || drValue.ToString().Length == 0 ? new DateTime(1, 1, 1) : Convert.ToDateTime(drValue.ToString());
        }

        public static int getCeroIfNull(object drValue)
        {
            return Convert.IsDBNull(drValue) || drValue.ToString().Length == 0 ? 0 : Convert.ToInt32(drValue.ToString());
        }

        public static decimal getCeroDecIfNull(object drValue)
        {
            return Convert.IsDBNull(drValue) || drValue.ToString().Length == 0 ? 0 : Convert.ToDecimal(drValue.ToString());
        }

        public static bool getFalseIfNull(object drValue)
        {
            return Convert.IsDBNull(drValue) || drValue.ToString().Length == 0 ? false : Convert.ToBoolean(drValue.ToString());
        }

        /// <summary>
        /// Si la fecha del parametro viene minValue, devuelve una fecha con formato 01/01/1800 para insertar en TANGO
        /// </summary>
        /// <param name="dtValue"></param>
        /// <returns></returns>
        public static DateTime getMinValueTango(DateTime dtValue)
        {
            if (dtValue == DateTime.MinValue)
            {
                return new DateTime(1800,01,01);
            }

            return dtValue;
        }

        public static object getNullIfDateMinValue(DateTime dtValue)
        {
            if (dtValue == DateTime.MinValue || dtValue == new DateTime(1900, 01, 01) || dtValue == new DateTime(1,1,1))
            {
                return null;
            }

            return dtValue;
        }

        public static object getNullIfEmpty(string sValue)
        {
            if (sValue == null || sValue.Trim().Length == 0)
            {
                return null;
            }

            return sValue;
        }

        public static object getNullIfCero(string value)
        {
            return value == "0" || value.Equals(string.Empty) ? null : value;
        }

        public static object getNullIfDate(string value)
        {
            if (value == null || value.Trim().Length == 0)
            {
                return null;
            }

            return DateTime.Parse(value);
        }
    }
}
