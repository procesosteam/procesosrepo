
using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using ProcesoPP.SqlServerLibrary;
using ProcesoPP.Model;

namespace ProcesoPP.SqlServerImpl
{
	public class SectorImpl	
	{
		#region Sector methods

		public int SectorAdd(Sector sector)
		{
			try
			{
				sector.IdSector= (int)SqlHelper.ExecuteScalar(SqlImplHelper.getConnectionString(), "SectorAdd",
														sector.Descripcion,
                                                        sector.IdEmpresa);
                SqlImplHelper.commitTransaction();
                return sector.IdSector;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		public bool SectorUpdate(Sector sector)
		{
			try
			{
				int update = SqlHelper.ExecuteNonQuery(SqlImplHelper.getConnectionString(), "SectorUpdate",
														sector.IdSector,
                                                        sector.Descripcion,
                                                        sector.IdEmpresa);
				if (update > 0)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		public bool SectorDelete(int IdSector)
		{
			try
			{
				int update = SqlHelper.ExecuteNonQuery(SqlImplHelper.getConnectionString(), "SectorDelete",
														IdSector);
				if (update > 0)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		public Sector SectorGetById(int IdSector)
		{
			try
			{
				DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(), "SectorGetById",
														IdSector).Tables[0];
				Sector NewEnt = new Sector();

				if(dt.Rows.Count > 0)
				{
					DataRow dr = dt.Rows[0];
					NewEnt.IdSector = Int32.Parse(dr["IdSector"].ToString());
					NewEnt.Descripcion = dr["Descripcion"].ToString();
                    NewEnt.IdEmpresa = dr["IdEmpresa"] == null ? 0 : int.Parse(dr["IdEmpresa"].ToString());
                    NewEnt.oEmpresa = (new EmpresaImpl()).EmpresaGetById(NewEnt.IdEmpresa);
				}
				return NewEnt;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		public List<Sector> SectorGetAll()
		{
			List<Sector> lstSector = new List<Sector>();
			try
			{
				DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(), "SectorGetAll").Tables[0];
				if (dt.Rows.Count > 0)
				{
					int colIdSector =  dt.Columns["IdSector"].Ordinal;
					int colDescripcion =  dt.Columns["Descripcion"].Ordinal;
					for (int i = 0; dt.Rows.Count > i; i++)
					{
						Sector NewEnt = new Sector();
						NewEnt.IdSector = Int32.Parse(dt.Rows[i].ItemArray[colIdSector].ToString());
						NewEnt.Descripcion = dt.Rows[i].ItemArray[colDescripcion].ToString();
                        NewEnt.IdEmpresa = int.Parse(dt.Rows[i]["IdEmpresa"].ToString());
                        NewEnt.oEmpresa = (new EmpresaImpl()).EmpresaGetById(NewEnt.IdEmpresa);
						lstSector.Add(NewEnt);
					}
				}
				return lstSector;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		#endregion


        public List<Sector> SectorGetAllByIdEmpresa(int idEmpresa)
        {
            List<Sector> lstSector = new List<Sector>();
            try
            {
                DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(), "SectorGetAllByIdEmpresa",
                                                        SqlImplHelper.getNullIfCero(idEmpresa.ToString())).Tables[0];
                if (dt.Rows.Count > 0)
                {
                    int colIdSector = dt.Columns["IdSector"].Ordinal;
                    int colDescripcion = dt.Columns["Descripcion"].Ordinal;
                    for (int i = 0; dt.Rows.Count > i; i++)
                    {
                        Sector NewEnt = new Sector();
                        NewEnt.IdSector = Int32.Parse(dt.Rows[i].ItemArray[colIdSector].ToString());
                        NewEnt.Descripcion = dt.Rows[i].ItemArray[colDescripcion].ToString();
                        NewEnt.IdEmpresa = int.Parse(dt.Rows[i]["IdEmpresa"].ToString());
                        NewEnt.oEmpresa = (new EmpresaImpl()).EmpresaGetById(NewEnt.IdEmpresa);
                        lstSector.Add(NewEnt);
                    }
                }
                return lstSector;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Sector> SectorGetAllByIdEmpresaInterna()
        {
            List<Sector> lstSector = new List<Sector>();
            try
            {
                DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(), "SectorGetAllByIdEmpresaInterna").Tables[0];
                if (dt.Rows.Count > 0)
                {
                    int colIdSector = dt.Columns["IdSector"].Ordinal;
                    int colDescripcion = dt.Columns["Descripcion"].Ordinal;
                    for (int i = 0; dt.Rows.Count > i; i++)
                    {
                        Sector NewEnt = new Sector();
                        NewEnt.IdSector = Int32.Parse(dt.Rows[i].ItemArray[colIdSector].ToString());
                        NewEnt.Descripcion = dt.Rows[i].ItemArray[colDescripcion].ToString();
                        NewEnt.IdEmpresa = int.Parse(dt.Rows[i]["IdEmpresa"].ToString());
                        lstSector.Add(NewEnt);
                    }
                }
                return lstSector;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
