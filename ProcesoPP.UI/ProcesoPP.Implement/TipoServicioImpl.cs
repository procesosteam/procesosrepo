
using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using ProcesoPP.SqlServerLibrary;
using ProcesoPP.Model;

namespace ProcesoPP.SqlServerImpl
{
	public class TipoServicioImpl	
	{
		#region TipoServicio methods

		public int TipoServicioAdd(TipoServicio tiposervicio)
		{
			try
			{
				tiposervicio.IdTipoServicio = (int)SqlHelper.ExecuteScalar(SqlImplHelper.getTransaction(), "TipoServicioAdd",														
														tiposervicio.Descripcion,
                                                        tiposervicio.CodTango,
                                                        tiposervicio.Abreviacion,
                                                        tiposervicio.RequierePersonal,
                                                        tiposervicio.idSector,
                                                        tiposervicio.RequiereKms,
                                                        tiposervicio.RequiereMateriales,
                                                        tiposervicio.RequiereCuadrilla,
                                                        tiposervicio.MasEquipo,
                                                        tiposervicio.RequiereTraslado,
                                                        tiposervicio.RequiereEquipo,
                                                        tiposervicio.RequiereKmsTaco,
                                                        tiposervicio.RequiereChofer);

                TipoServicioEspecificacionImpl oTipoServicioEspecificacionImpl = new TipoServicioEspecificacionImpl();
                foreach(EspecificacionTecnica otsEsp in tiposervicio.oEspecificacion)
                {
                    TipoServicioEspecificacion oTipoServicioEspecificacion = new TipoServicioEspecificacion();
                    oTipoServicioEspecificacion.IdTipoServicio = tiposervicio.IdTipoServicio;
                    oTipoServicioEspecificacion.IdEspecificacion = otsEsp.IdEspecificaciones;
                    oTipoServicioEspecificacionImpl.TipoServicioEspecificacionAdd(oTipoServicioEspecificacion);
                }

                SqlImplHelper.commitTransaction();
                return tiposervicio.IdTipoServicio;

			}
			catch(Exception ex)
			{
                SqlImplHelper.rollbackTransaction();
				throw ex;
			}
		}

		public bool TipoServicioUpdate(TipoServicio tiposervicio)
		{
			try
			{
				int update = SqlHelper.ExecuteNonQuery(SqlImplHelper.getTransaction(), "TipoServicioUpdate",
														tiposervicio.IdTipoServicio, 
														tiposervicio.Descripcion,
                                                        tiposervicio.CodTango,
                                                        tiposervicio.Abreviacion,
                                                        tiposervicio.RequierePersonal,
                                                        tiposervicio.idSector,
                                                        tiposervicio.RequiereKms, 
                                                        tiposervicio.RequiereMateriales,
                                                        tiposervicio.RequiereCuadrilla,
                                                        tiposervicio.MasEquipo,
                                                        tiposervicio.RequiereTraslado,
                                                        tiposervicio.RequiereEquipo,
                                                        tiposervicio.RequiereKmsTaco,
                                                        tiposervicio.RequiereChofer);

                TipoServicioEspecificacionImpl oTipoServicioEspecificacionImpl = new TipoServicioEspecificacionImpl();
                oTipoServicioEspecificacionImpl.TipoServicioEspecificacionDeleteByIdTipoServicio(tiposervicio.IdTipoServicio);
                foreach (EspecificacionTecnica otsEsp in tiposervicio.oEspecificacion)
                {
                    TipoServicioEspecificacion oTipoServicioEspecificacion = new TipoServicioEspecificacion();
                    oTipoServicioEspecificacion.IdTipoServicio = tiposervicio.IdTipoServicio;
                    oTipoServicioEspecificacion.IdEspecificacion = otsEsp.IdEspecificaciones;
                    oTipoServicioEspecificacionImpl.TipoServicioEspecificacionAdd(oTipoServicioEspecificacion);
                }

                SqlImplHelper.commitTransaction();
              
				if (update > 0)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			catch(Exception ex)
			{
                SqlImplHelper.rollbackTransaction();
				throw ex;
			}
		}

		public bool TipoServicioDelete(int IdTipoServicio)
		{
			try
			{
				int update = SqlHelper.ExecuteNonQuery(SqlImplHelper.getConnectionString(), "TipoServicioDelete",
														IdTipoServicio);
				if (update > 0)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		public TipoServicio TipoServicioGetById(int IdTipoServicio)
		{
			try
			{
				DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(), "TipoServicioGetById",
														IdTipoServicio).Tables[0];

                TipoServicio NewEnt = new TipoServicio();
				if(dt.Rows.Count > 0)
				{
					NewEnt = CargarTipoServicio(dt.Rows[0]);
                }

				return NewEnt;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

        public TipoServicio CargarTipoServicio(DataRow dr)
        {
            TipoServicio NewEnt = new TipoServicio();
            EspecificacionTecnicaImpl oEspecificacionTecnicaImpl = new EspecificacionTecnicaImpl();
            NewEnt.IdTipoServicio = Int32.Parse(dr["IdTipoServicio"].ToString());
            NewEnt.Descripcion = dr["Descripcion"].ToString();
            NewEnt.CodTango = dr["codTango"].ToString();
            NewEnt.Abreviacion = dr["Abreviacion"].ToString();
            NewEnt.idSector = SqlImplHelper.getCeroIfNull(dr["idSector"]);
            NewEnt.RequierePersonal = SqlImplHelper.getFalseIfNull(dr["RequierePersonal"]);
            NewEnt.RequiereKms = SqlImplHelper.getFalseIfNull(dr["RequiereKms"]);
            NewEnt.RequiereMateriales = SqlImplHelper.getFalseIfNull(dr["RequiereMaterial"]);
            NewEnt.RequiereCuadrilla = SqlImplHelper.getFalseIfNull(dr["RequiereCuadrilla"]);
            NewEnt.MasEquipo = SqlImplHelper.getFalseIfNull(dr["MasEquipo"]);
            NewEnt.RequiereTraslado = SqlImplHelper.getFalseIfNull(dr["RequiereTraslado"]);
            NewEnt.RequiereEquipo = SqlImplHelper.getFalseIfNull(dr["RequiereEquipo"]);
            NewEnt.RequiereKmsTaco = SqlImplHelper.getFalseIfNull(dr["RequiereKmsTaco"]);
            NewEnt.RequiereChofer = SqlImplHelper.getFalseIfNull(dr["RequiereChofer"]);

            if (NewEnt.IdTipoServicio != 0)
                NewEnt.oEspecificacion = oEspecificacionTecnicaImpl.EspecificacionTecnicaGetByIdTipoServicio(NewEnt.IdTipoServicio);

            return NewEnt;

        }

		public List<TipoServicio> TipoServicioGetAll()
		{
			List<TipoServicio> lstTipoServicio = new List<TipoServicio>();
			try
			{
                TipoServicioEspecificacionImpl oTipoServicioEspecificacionImpl = new TipoServicioEspecificacionImpl();
				DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(), "TipoServicioGetAll").Tables[0];
				if (dt.Rows.Count > 0)
				{  
					for (int i = 0; dt.Rows.Count > i; i++)
					{
                        TipoServicio NewEnt = new TipoServicio();
                        NewEnt = CargarTipoServicio(dt.Rows[i]);
						lstTipoServicio.Add(NewEnt);
					}
				}
				return lstTipoServicio;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		#endregion
        
        public List<TipoServicio> TipoServicioGetByFilter(string descripcion)
        {
            List<TipoServicio> lstTipoServicio = new List<TipoServicio>();
            try
            {
                TipoServicioEspecificacionImpl oTipoServicioEspecificacionImpl = new TipoServicioEspecificacionImpl();
                DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(), 
                                                        "TipoServicioGetByFilter",
                                                        SqlImplHelper.getNullIfEmpty(descripcion)).Tables[0];
                if (dt.Rows.Count > 0)
                {                    
                    EspecificacionTecnicaImpl oEspecificacionTecnicaImpl = new EspecificacionTecnicaImpl();
                    for (int i = 0; dt.Rows.Count > i; i++)
                    {
                        TipoServicio NewEnt = new TipoServicio();
                        NewEnt = CargarTipoServicio(dt.Rows[i]);
                        lstTipoServicio.Add(NewEnt);
                    }
                }
                return lstTipoServicio;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable CodTangoGetByFilter(string detalle)
        {
            DataTable dt = new DataTable();
            try
            {
                DataSet ds = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(),
                                                            "CodTangoGetByFilter",
                                                             SqlImplHelper.getNullIfEmpty(detalle));
                if (ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                }
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataSet TipoServicioGetByAcuerdoCliente(string idEmpresa, string idSector)
        {
           // DataTable dt = new DataTable();
            try
            {
                DataSet ds = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(),
                                                      "TipoServicioGetByAcuerdoCliente",
                                                      SqlImplHelper.getNullIfCero(idEmpresa),
                                                      SqlImplHelper.getNullIfCero(idSector));
                if (ds.Tables.Count > 0)
                {
                    DataTable dt = ds.Tables[0];
                    if (dt.Rows.Count > 0)
                    {
                        DataColumn[] Parent_PKColumns = new DataColumn[1];
                        Parent_PKColumns[0] = ds.Tables[0].Columns["idTipoServicio"];
                        dt.PrimaryKey = Parent_PKColumns;

                        DataTable dt1 = ds.Tables[1];
                        DataColumn[] Child_PKColumns = new DataColumn[1];
                        Child_PKColumns[0] = dt1.Columns["idEspecificacion"];
                        //dt1.PrimaryKey = Child_PKColumns;
                        DataColumn[] Child_FKColumns = new DataColumn[1];
                        Child_FKColumns[0] = dt1.Columns["idTipoServicio"];

                        ds.Relations.Add("ParentChild", Parent_PKColumns, Child_FKColumns);
                    }
                    
                }
                return ds;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
