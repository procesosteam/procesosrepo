
using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using ProcesoPP.SqlServerLibrary;
using ProcesoPP.Model;

namespace ProcesoPP.SqlServerImpl
{
	public class EmpresaImpl	
	{
		#region Empresa methods

		public int EmpresaAdd(Empresa empresa)
		{
			try
			{
				return (int)SqlHelper.ExecuteScalar(SqlImplHelper.getConnectionString(), "EmpresaAdd",
														empresa.Descripcion, 
														empresa.RazonSocial, 
														empresa.Domicilio, 
														empresa.CondIVA, 
														empresa.CUIT,
                                                        empresa.CodTango,
														empresa.Interna,
                                                        empresa.Baja);
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		public bool EmpresaUpdate(Empresa empresa)
		{
			try
			{
				int update = SqlHelper.ExecuteNonQuery(SqlImplHelper.getConnectionString(), "EmpresaUpdate",
														empresa.IdEmpresa, 
														empresa.Descripcion, 
														empresa.RazonSocial, 
														empresa.Domicilio, 
														empresa.CondIVA,
                                                        empresa.CUIT, 
                                                        empresa.CodTango,
														empresa.Interna,
                                                        empresa.Baja);
				if (update > 0)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		public bool EmpresaDelete(int IdEmpresa)
		{
			try
			{
				int update = SqlHelper.ExecuteNonQuery(SqlImplHelper.getConnectionString(), "EmpresaDelete",
														IdEmpresa);
				if (update > 0)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		public Empresa EmpresaGetById(int IdEmpresa)
		{
			try
			{
				DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(), "EmpresaGetById",
														IdEmpresa).Tables[0];
				Empresa oObjeto = new Empresa();

				if(dt.Rows.Count > 0)
				{
					DataRow dr = dt.Rows[0];
					oObjeto = CargarEmpresa(dr);
				}
				return oObjeto;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		public List<Empresa> EmpresaGetAll()
		{
			List<Empresa> lstEmpresa = new List<Empresa>();
			try
			{
				DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(), "EmpresaGetAll").Tables[0];
				if (dt.Rows.Count > 0)
				{
					for (int i = 0; dt.Rows.Count > i; i++)
					{
						DataRow dr = dt.Rows[i];
						Empresa oObjeto = new Empresa();
						oObjeto = CargarEmpresa(dr);
						lstEmpresa.Add(oObjeto);
					}
				}
				return lstEmpresa;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private Empresa CargarEmpresa(DataRow dr)
		{
			try
			{
				Empresa oObjeto = new Empresa();
                oObjeto.IdEmpresa = Int32.Parse(dr["IdEmpresa"].ToString());
                oObjeto.Descripcion = dr["Descripcion"].ToString();
                oObjeto.RazonSocial = dr["RazonSocial"].ToString();
                oObjeto.Domicilio = dr["Domicilio"].ToString();
                oObjeto.CondIVA = dr["CondicionIVA"].ToString();
                oObjeto.CUIT = dr["CUIT"].ToString();
                oObjeto.CodTango = dr["codTango"].ToString();
				oObjeto.Interna = SqlImplHelper.getFalseIfNull(dr["Interna"]);
                oObjeto.Baja = SqlImplHelper.getFalseIfNull(dr["Baja"]);
				return oObjeto;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

        public List<Empresa> GetInternas()
        {
            List<Empresa> lstEmpresa = new List<Empresa>();
			try
			{
				DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(), "EmpresaGetAllInternas").Tables[0];
				if (dt.Rows.Count > 0)
				{
					for (int i = 0; dt.Rows.Count > i; i++)
					{
						DataRow dr = dt.Rows[i];
						Empresa oObjeto = new Empresa();
						oObjeto = CargarEmpresa(dr);
						lstEmpresa.Add(oObjeto);
					}
				}
				return lstEmpresa;
			}
			catch(Exception ex)
			{
				throw ex;
			}
        }

		#endregion


        public DataTable EmpresaGetByFilter(string nombre)
        {
            
            try
            {
                DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(), 
                                                        "EmpresaGetByFilter", 
                                                        SqlImplHelper.getNullIfEmpty(nombre)).Tables[0];
                if (dt.Rows.Count > 0)
                {
                    return dt;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
