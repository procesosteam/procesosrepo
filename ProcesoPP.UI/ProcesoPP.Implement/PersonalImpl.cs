using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using ProcesoPP.SqlServerLibrary;
using ProcesoPP.Model;

namespace ProcesoPP.SqlServerImpl
{
	public class PersonalImpl	
	{
		#region Personal methods

		public int PersonalAdd(Personal personal)
		{
			try
			{
				personal.IdPersonal= (int)SqlHelper.ExecuteScalar(SqlImplHelper.getConnectionString(), "PersonalAdd",
														personal.NroLegajo, 
														personal.Apellido, 
														personal.Nombres, 
														personal.FechaNacimiento, 
														personal.IdSexo, 
														personal.DNI, 
														personal.CUIL, 
														personal.IdEstadoCivil, 
														personal.IdEstudios, 
														personal.IdNacionalidad, 
														personal.IdLocalidad, 
														personal.Domicilio, 
														personal.Telefono,
                                                        personal.Mail, 
														personal.IdUsuarioAcceso, 
														personal.IdPersonalContacto, 
														personal.IdPersonalDatosLaborales, 
														personal.IdPersonalInformacionMedica, 
														personal.IdPersonalConduccion,
                                                        0);
                SqlImplHelper.commitTransaction();
                return personal.IdPersonal;
                
			}
                
			catch(Exception ex)
			{
                SqlImplHelper.rollbackTransaction();
				throw ex;
			}
		}

		public bool PersonalUpdate(Personal personal)
		{
			try
			{
				int update = SqlHelper.ExecuteNonQuery(SqlImplHelper.getConnectionString(), "PersonalUpdate",
														personal.IdPersonal, 
														personal.NroLegajo, 
														personal.Apellido, 
														personal.Nombres, 
														personal.FechaNacimiento, 
														personal.IdSexo, 
														personal.DNI, 
														personal.CUIL, 
														personal.IdEstadoCivil, 
														personal.IdEstudios, 
														personal.IdNacionalidad, 
														personal.IdLocalidad, 
														personal.Domicilio, 
														personal.Telefono,
                                                        personal.Mail, 
														personal.IdUsuarioAcceso, 
														personal.IdPersonalContacto, 
														personal.IdPersonalDatosLaborales, 
														personal.IdPersonalInformacionMedica, 
														personal.IdPersonalConduccion);
                
                if (update > 0)
				{
                    
					return true;
				}
				else
				{
					return false;
				}
			}
			catch(Exception ex)
			{
                SqlImplHelper.rollbackTransaction();
                throw ex;
			}
		}

		public bool PersonalDelete(int IdPersonal)
		{
			try
			{
				int update = SqlHelper.ExecuteNonQuery(SqlImplHelper.getConnectionString(), "PersonalDelete",
														IdPersonal);
				if (update > 0)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		public Personal PersonalGetById(int IdPersonal)
		{
			try
			{
				DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(), "PersonalGetById",
														IdPersonal).Tables[0];
				
                Personal NewEnt = new Personal();

				if(dt.Rows.Count > 0)
				{
					DataRow dr = dt.Rows[0];
					NewEnt = CargarPersonal(dr);
				}
				return NewEnt;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		public List<Personal> PersonalGetAll()
		{
			List<Personal> lstPersonal = new List<Personal>();
			try
			{
				DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(), "PersonalGetAll").Tables[0];
				if (dt.Rows.Count > 0)
				{
					for (int i = 0; dt.Rows.Count > i; i++)
					{
						DataRow dr = dt.Rows[i];
						Personal NewEnt = new Personal();
						NewEnt = CargarPersonal(dr);
						lstPersonal.Add(NewEnt);
					}
				}
				return lstPersonal;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		private Personal CargarPersonal(DataRow dr)
		{
			try
			{
				Personal oObjeto = new Personal();

				oObjeto.IdPersonal = int.Parse(dr["IdPersonal"].ToString());
				oObjeto.NroLegajo = int.Parse(dr["NroLegajo"].ToString());
				oObjeto.Apellido = (dr["Apellido"].ToString());
				oObjeto.Nombres = (dr["Nombres"].ToString());
                oObjeto.ApellidoNombre = oObjeto.Apellido + ", " + oObjeto.Nombres;  
				oObjeto.FechaNacimiento = DateTime.Parse(dr["FechaNacimiento"].ToString());
				oObjeto.IdSexo = int.Parse(dr["IdSexo"].ToString());
				oObjeto.DNI = int.Parse(dr["DNI"].ToString());
				oObjeto.CUIL = int.Parse(dr["CUIL"].ToString());
				oObjeto.IdEstadoCivil = int.Parse(dr["IdEstadoCivil"].ToString());
				oObjeto.IdEstudios = int.Parse(dr["IdEstudios"].ToString());
				oObjeto.IdNacionalidad = int.Parse(dr["IdNacionalidad"].ToString());
				oObjeto.IdLocalidad = int.Parse(dr["IdLocalidad"].ToString());
				oObjeto.Domicilio = (dr["Domicilio"].ToString());
				oObjeto.Telefono = dr["Telefono"].ToString();
                oObjeto.Mail = (dr["Mail"].ToString());
				oObjeto.IdUsuarioAcceso = int.Parse(dr["IdUsuarioAcceso"].ToString());
				oObjeto.IdPersonalContacto = int.Parse(dr["IdPersonalContacto"].ToString());
				oObjeto.IdPersonalDatosLaborales = int.Parse(dr["IdPersonalDatosLaborales"].ToString());
				oObjeto.IdPersonalInformacionMedica = int.Parse(dr["IdPersonalInformacionMedica"].ToString());
				oObjeto.IdPersonalConduccion = int.Parse(dr["IdPersonalConduccion"].ToString());
				
				return oObjeto;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

        public DataTable PersonalGetByFilter(string ApNom,string dni, string leg, string idEmp, string idSec, string vtoCar, string vtoPsico, string vtoGrales, string vtoPeligrosa, string vtoMDef)
        {
            try
            {
                DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(),
                                                        "PersonalGestionGetByFilter",
                                                        SqlImplHelper.getNullIfEmpty(ApNom),
                                                        SqlImplHelper.getNullIfEmpty(dni),
                                                        SqlImplHelper.getNullIfEmpty(leg),
                                                        SqlImplHelper.getNullIfCero(idEmp),
                                                        SqlImplHelper.getNullIfCero(idSec),
                                                        SqlImplHelper.getNullIfEmpty(vtoCar),
                                                        SqlImplHelper.getNullIfEmpty(vtoGrales),
                                                        SqlImplHelper.getNullIfEmpty(vtoMDef),
                                                        SqlImplHelper.getNullIfEmpty(vtoPeligrosa),
                                                        SqlImplHelper.getNullIfEmpty(vtoPsico) ).Tables[0];

                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable PersonalToXLS(int idPersonal)
        {
            try
            {
                DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(),
                                                        "PersonalToXLS",
                                                        idPersonal).Tables[0];

                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

		#endregion


        public DataTable PersonalHistorialGetByFilter(string ApNom, string dni, string leg, string idEmp, string idSec, string vtoCar, string vtoPsico, string vtoGrales, string vtoPeligrosa, string vtoMDef)
        {
            try
            {
                DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(),
                                                        "PersonalHistorialGetByFilter",
                                                        SqlImplHelper.getNullIfEmpty(ApNom),
                                                        SqlImplHelper.getNullIfEmpty(dni),
                                                        SqlImplHelper.getNullIfEmpty(leg),
                                                        SqlImplHelper.getNullIfCero(idEmp),
                                                        SqlImplHelper.getNullIfCero(idSec),
                                                        SqlImplHelper.getNullIfEmpty(vtoCar),
                                                        SqlImplHelper.getNullIfEmpty(vtoGrales),
                                                        SqlImplHelper.getNullIfEmpty(vtoMDef),
                                                        SqlImplHelper.getNullIfEmpty(vtoPeligrosa),
                                                        SqlImplHelper.getNullIfEmpty(vtoPsico)).Tables[0];

                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Personal> PersonalGetByIdSector(int idSector)
        {
            List<Personal> lstPersonal = new List<Personal>();
            try
            {
                DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(), "PersonalGetByIdSector", idSector).Tables[0];
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; dt.Rows.Count > i; i++)
                    {
                        DataRow dr = dt.Rows[i];
                        Personal NewEnt = new Personal();
                        NewEnt = CargarPersonal(dr);
                        lstPersonal.Add(NewEnt);
                    }
                }
                return lstPersonal;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Personal> PersonalGetByIdSectorEmpresa(string idSector, string idEMpresa)
        {
            List<Personal> lstPersonal = new List<Personal>();
            try
            {
                DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(), "PersonalGetByIdSectorEmpresa",    
                                                        SqlImplHelper.getNullIfCero(idSector),
                                                        SqlImplHelper.getNullIfCero(idEMpresa)).Tables[0];
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; dt.Rows.Count > i; i++)
                    {
                        DataRow dr = dt.Rows[i];
                        Personal NewEnt = new Personal();
                        NewEnt = CargarPersonal(dr);
                        lstPersonal.Add(NewEnt);
                    }
                }
                return lstPersonal;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        public DataTable PersonalGetLogistica()
        {
            try
            {
                DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(), "PersonalGetLogistica").Tables[0];
                if (dt.Rows.Count > 0)
                {
                    return dt;
                }
                else
                    return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable PersonalGetByIdOrden(int idOrden)
        {
            try
            {
                DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(), "PersonalGetByIdOrden", idOrden).Tables[0];
                if (dt.Rows.Count > 0)
                {
                    return dt;
                }
                else
                    return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
