
///////////////////////////////////////////////////////////////////////////
//
// Generated by MyGeneration Version # (1.3.0.9) 
//
///////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using ProcesoPP.SqlServerLibrary;
using ProcesoPP.Model;

namespace ProcesoPP.SqlServerImpl
{
	public class AcuerdoImpl	
	{
		#region Acuerdo methods

		public int AcuerdoAdd(Acuerdo acuerdo)
		{
			try
			{
				int idAcuerdo = (int)SqlHelper.ExecuteScalar(SqlImplHelper.getTransaction(), "AcuerdoAdd",
														acuerdo.NroAcuerdo, 
														acuerdo.IdCliente, 
														acuerdo.FechaInicio, 
														acuerdo.FechaFin, 
														acuerdo.FechaEmision,
                                                        acuerdo.NroRevision,
                                                        acuerdo.Titulo,
                                                        acuerdo.TipoMoneda, 
                                                        acuerdo.idSector,
                                                        acuerdo.Activo);

                AcuerdoServicioImpl oAcuerdoServicioImpl = new AcuerdoServicioImpl();
                foreach (AcuerdoServicio oAcServ in acuerdo.oListServicios)
                {
                    oAcServ.IdAcuerdo = idAcuerdo;
                    oAcuerdoServicioImpl.AcuerdoServicioAdd(oAcServ, true);

                }
                AcuerdoTituloImpl oAcuerdoTituloImpl = new AcuerdoTituloImpl();
                foreach (AcuerdoTitulo oTit in acuerdo.oListTitulos)
                {
                    oTit.IdAcuerdo = idAcuerdo;
                    oAcuerdoTituloImpl.AcuerdoTituloAdd(oTit);
                }

                SqlImplHelper.commitTransaction();
                return idAcuerdo;
			}
			catch(Exception ex)
			{
                SqlImplHelper.rollbackTransaction();            
				throw ex;
			}
		}

		public bool AcuerdoUpdate(Acuerdo acuerdo)
		{
			try
			{
				int update = SqlHelper.ExecuteNonQuery(SqlImplHelper.getTransaction(), "AcuerdoUpdate",
														acuerdo.IdAcuerdo, 
														acuerdo.NroAcuerdo, 
														acuerdo.IdCliente, 
														acuerdo.FechaInicio, 
														acuerdo.FechaFin, 
														acuerdo.Baja,
                                                        acuerdo.FechaEmision,
                                                        acuerdo.NroRevision,
                                                        acuerdo.Titulo,
                                                        acuerdo.TipoMoneda,
                                                        acuerdo.idSector);

                AcuerdoServicioImpl oAcuerdoServicioImpl = new AcuerdoServicioImpl();
                foreach (AcuerdoServicio oAcuerdoServ in acuerdo.oListServicios)
                {
                    oAcuerdoServ.IdAcuerdo = acuerdo.IdAcuerdo;
                    oAcuerdoServicioImpl.AcuerdoServicioUpdate(oAcuerdoServ);
                }
                SqlImplHelper.commitTransaction();
                if (update > 0)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			catch(Exception ex)
			{
                SqlImplHelper.rollbackTransaction();
				throw ex;
			}
		}

		public bool AcuerdoDelete(int IdAcuerdo)
		{
			try
			{
				int update = SqlHelper.ExecuteNonQuery(SqlImplHelper.getConnectionString(), "AcuerdoDelete",
														IdAcuerdo);
				if (update > 0)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		public Acuerdo AcuerdoGetById(int IdAcuerdo)
		{
			try
			{
				DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(), "AcuerdoGetById",
														IdAcuerdo).Tables[0];
				Acuerdo NewEnt = new Acuerdo();

				if(dt.Rows.Count > 0)
				{
					DataRow dr = dt.Rows[0];
					NewEnt = CargarAcuerdo(dr);
				}
				return NewEnt;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		public List<Acuerdo> AcuerdoGetAll()
		{
			List<Acuerdo> lstAcuerdo = new List<Acuerdo>();
			try
			{
				DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(), "AcuerdoGetAll").Tables[0];
				if (dt.Rows.Count > 0)
				{
					for (int i = 0; dt.Rows.Count > i; i++)
					{
						DataRow dr = dt.Rows[i];
						Acuerdo NewEnt = new Acuerdo();
						NewEnt = CargarAcuerdo(dr);
						lstAcuerdo.Add(NewEnt);
					}
				}
				return lstAcuerdo;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		private Acuerdo CargarAcuerdo(DataRow dr)
		{
			try
			{
				Acuerdo oObjeto = new Acuerdo();
                AcuerdoServicioImpl oAcuerdoServicioImpl = new AcuerdoServicioImpl();
                AcuerdoTituloImpl oAcuerdoTituloImpl = new AcuerdoTituloImpl();

				oObjeto.IdAcuerdo = Int32.Parse(dr["IdAcuerdo"].ToString());
				oObjeto.NroAcuerdo = dr["NroAcuerdo"].ToString();
				oObjeto.IdCliente = Int32.Parse(dr["IdCliente"].ToString());
				oObjeto.FechaInicio = DateTime.Parse(dr["FechaInicio"].ToString());
				oObjeto.FechaFin = DateTime.Parse(dr["FechaFin"].ToString());
				oObjeto.FechaEmision = SqlImplHelper.getMinValueIfNull(dr["FechaEmision"].ToString());
                oObjeto.NroRevision = SqlImplHelper.getCeroIfNull(dr["NroRevision"].ToString());
                oObjeto.Titulo = dr["Titulo"].ToString();
                oObjeto.TipoMoneda = dr["TipoMoneda"].ToString();
                oObjeto.oListServicios = oAcuerdoServicioImpl.AcuerdoServicioGetByIdAcuerdo(oObjeto.IdAcuerdo);
                oObjeto.oListTitulos = oAcuerdoTituloImpl.AcuerdoTituloGetByIdAcuerdo(oObjeto.IdAcuerdo);
                oObjeto.oHeader = oAcuerdoTituloImpl.AcuerdoHerderGetByIdAcuerdo(oObjeto.IdAcuerdo);
                oObjeto.oCliente = (new EmpresaImpl()).EmpresaGetById(oObjeto.IdCliente);
                oObjeto.idSector = SqlImplHelper.getCeroIfNull(dr["idSector"].ToString());                
				return oObjeto;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

        public DataTable AcuerdoGetByFilter(string nroAcuerdo, string fechaDesde, string fechaHasta, string idCliente, string idSector)
        {
            try
            {
                DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(), 
                                                        "AcuerdoGetByFilter",
                                                        SqlImplHelper.getNullIfEmpty(nroAcuerdo),
                                                        SqlImplHelper.getNullIfEmpty(fechaDesde),
                                                        SqlImplHelper.getNullIfEmpty(fechaHasta),
                                                        SqlImplHelper.getNullIfCero(idCliente),
                                                        SqlImplHelper.getNullIfCero(idSector)).Tables[0];
                
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion


        public DataTable AcuerdoAlerta()
        {
            try
            {
                DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(),
                                                        "AcuerdoAlerta").Tables[0];

                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable AcuerdoGestionGetByFilter(string nroAcuerdo, string fechaDesde, string fechaHasta, string idCliente, string idSector)
        {
            try
            {
                DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(),
                                                        "AcuerdoGestionGetByFilter",
                                                        SqlImplHelper.getNullIfEmpty(nroAcuerdo),
                                                        SqlImplHelper.getNullIfEmpty(fechaDesde),
                                                        SqlImplHelper.getNullIfEmpty(fechaHasta),
                                                        SqlImplHelper.getNullIfCero(idCliente),
                                                        SqlImplHelper.getNullIfCero(idSector)).Tables[0];

                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable AcuerdoVerificarFechas(string fechaDesde, string fechaHasta, string idCliente, string idSector)
        {
            try
            {
                DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(),
                                                        "AcuerdoVerificarFechas",                                                        
                                                        SqlImplHelper.getNullIfEmpty(fechaDesde),
                                                        SqlImplHelper.getNullIfEmpty(fechaHasta),
                                                        SqlImplHelper.getNullIfCero(idCliente),
                                                        SqlImplHelper.getNullIfCero(idSector)).Tables[0];

                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable AcuerdoDetalleGetById(int _idAcuerdo)
        {
            try
            {
                DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(),
                                                        "AcuerdoDetalleGetById",
                                                        _idAcuerdo).Tables[0];

                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// ESTE PROCEDIMIENTO SELECCIONA EL ACUERDO PARA UN CLIENTE, SECTOR EN PARTICULAR Y EL ACTIVO EN LA FECHA DE LA SOLICITUD
        /// </summary>
        /// <param name="oSS"> Con la Solicitud obtengo sector, empresa e idSS</param>
        /// <returns>Acuerdo</returns>
        public DataTable AcuerdoGetByCliente(SolicitudCliente oSS)
        {
            try
            {
                DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(),
                                                        "AcuerdoGetByCliente",
                                                        oSS.IdSector,
                                                        oSS.oSector.IdEmpresa,
                                                        oSS.IdSolicitud).Tables[0];

                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable AcuerdoCantTrailer(string idCliente, string idSector, List<EspecificacionTecnica> oListEsp)
        {
            try
            {
                DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(),
                                                            "AcuerdoCantTrailer",
                                                            SqlImplHelper.getNullIfCero(idCliente),
                                                            SqlImplHelper.getNullIfCero(idSector),
                                                            getIDS(oListEsp)).Tables[0];
                if (dt.Rows.Count > 0)
                {
                    return dt;
                }
                else
                    return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private string getIDS(List<EspecificacionTecnica> oEspecificaciones)
        {
            string ids = "", coma = "";
            foreach (EspecificacionTecnica oET in oEspecificaciones)
            {
                ids += coma + oET.IdEspecificaciones.ToString();
                coma = ",";
            }

            return ids;
        }

        public bool AcuerdoActivar(int idAcuerdo)
        {
            try
            {
                int update = SqlHelper.ExecuteNonQuery(SqlImplHelper.getConnectionString(), 
                                                        "AcuerdoActivar",
                                                        idAcuerdo);
                if (update > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable AcuerdoBorradoresGetByFilter(string nroAcuerdo, string fechaDesde, string fechaHasta, string idCliente, string idSector)
        {
            try
            {
                DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(),
                                                        "AcuerdoBorradoresGetByFilter",
                                                        SqlImplHelper.getNullIfEmpty(nroAcuerdo),
                                                        SqlImplHelper.getNullIfEmpty(fechaDesde),
                                                        SqlImplHelper.getNullIfEmpty(fechaHasta),
                                                        SqlImplHelper.getNullIfCero(idCliente),
                                                        SqlImplHelper.getNullIfCero(idSector)).Tables[0];

                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
