

using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using ProcesoPP.SqlServerLibrary;
using ProcesoPP.Model;

namespace ProcesoPP.SqlServerImpl
{
	public class VerificaParteImpl	
	{
		#region VerificaParte methods

		public int VerificaParteAdd(VerificaParte verificaparte, bool trans)
		{
			try
			{
                if (trans)
                    return (int)SqlHelper.ExecuteNonQuery(SqlImplHelper.getTransaction(), "VerificaParteAdd",
                                                            verificaparte.IdVerifica,
                                                            verificaparte.IdParte,
                                                            verificaparte.Cantidad);
                else
                    return (int)SqlHelper.ExecuteNonQuery(SqlImplHelper.getConnectionString(), "VerificaParteAdd",
                                                        verificaparte.IdVerifica,
                                                        verificaparte.IdParte,
                                                        verificaparte.Cantidad);
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		public bool VerificaParteUpdate(VerificaParte verificaparte, bool trans)
		{
			try
			{
                int update = 0;
                if (trans)
                {
                    update = SqlHelper.ExecuteNonQuery(SqlImplHelper.getTransaction(), "VerificaParteUpdate",
                                                            verificaparte.IdVerifica,
                                                            verificaparte.IdParte,
                                                            verificaparte.Cantidad);
                }
                else
                {
                    update = SqlHelper.ExecuteNonQuery(SqlImplHelper.getConnectionString(), "VerificaParteUpdate",
                                                            verificaparte.IdVerifica,
                                                            verificaparte.IdParte,
                                                            verificaparte.Cantidad);
                }
				if (update > 0)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

        public bool VerificaParteDelete(int IdVerifica, int IdParte)
		{
			try
			{
				int update = SqlHelper.ExecuteNonQuery(SqlImplHelper.getConnectionString(), "VerificaParteDelete",
														IdVerifica, IdParte);
				if (update > 0)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		public VerificaParte VerificaParteGetById(int IdVerifica, int IdParte, bool trans)
		{
			try
			{
                DataTable dt;
                if (trans)
                    dt = SqlHelper.ExecuteDataset(SqlImplHelper.getTransaction(), "VerificaParteGetById",
                                                            IdVerifica, IdParte).Tables[0];
                else
                    dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(), "VerificaParteGetById",
                                        IdVerifica, IdParte).Tables[0];

				VerificaParte NewEnt = new VerificaParte();

				if(dt.Rows.Count > 0)
				{
					DataRow dr = dt.Rows[0];
					NewEnt.IdVerifica = int.Parse(dr["IdVerifica"].ToString());
					NewEnt.IdParte = int.Parse(dr["IdParte"].ToString());
                    NewEnt.Cantidad = decimal.Parse(dr["Cantidad"].ToString());
				}
				return NewEnt;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		public List<VerificaParte> VerificaParteGetAll()
		{
			List<VerificaParte> lstVerificaParte = new List<VerificaParte>();
			try
			{
				DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(), "VerificaParteGetAll").Tables[0];
				if (dt.Rows.Count > 0)
				{
					int colIdVerifica =  dt.Columns["IdVerifica"].Ordinal;
					int colIdParte =  dt.Columns["IdParte"].Ordinal;
					for (int i = 0; dt.Rows.Count > i; i++)
					{
						VerificaParte NewEnt = new VerificaParte();
						NewEnt.IdVerifica = int.Parse(dt.Rows[i].ItemArray[colIdVerifica].ToString());
						NewEnt.IdParte = int.Parse(dt.Rows[i].ItemArray[colIdParte].ToString());
						lstVerificaParte.Add(NewEnt);
					}
				}
				return lstVerificaParte;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		#endregion


        public DataTable VerificaParteGetByIdParte(int _idParte)
        {
           try
           {
               DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(), "VerificaParteGetByIdParte",
                                                                                            _idParte).Tables[0];

               return dt;
				
           }
           catch (Exception ex)
           {
               throw ex;
           }
        }

    }
}
