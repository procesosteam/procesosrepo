

using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using ProcesoPP.SqlServerLibrary;
using ProcesoPP.Model;

namespace ProcesoPP.SqlServerImpl
{
	public class ParteEntregaImpl	
	{
		#region ParteEntrega methods

		public int ParteEntregaAdd(ParteEntrega parteentrega)
		{
			try
			{
				return (int)SqlHelper.ExecuteScalar(SqlImplHelper.getConnectionString(), "ParteEntregaAdd",
														    parteentrega.Fecha,
                                                            SqlImplHelper.getNullIfDateMinValue(parteentrega.HoraSalida),
                                                            SqlImplHelper.getNullIfDateMinValue(parteentrega.HoraLlegada),
                                                            parteentrega.IdOrdenTrabajo,
                                                            parteentrega.Entrega,
                                                            parteentrega.NroParteEntrega,
                                                            parteentrega.Adicionales,
                                                            parteentrega.Conexion,
                                                            parteentrega.Desconexion,
                                                            parteentrega.Observacion,
                                                            parteentrega.nroRemito);
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		public bool ParteEntregaUpdate(ParteEntrega parteentrega)
		{
			try
			{
				int update = SqlHelper.ExecuteNonQuery(SqlImplHelper.getConnectionString(), "ParteEntregaUpdate",
														parteentrega.IdParteEntrega, 
														parteentrega.Fecha, 
														parteentrega.IdOrdenTrabajo,
                                                        parteentrega.Entrega,
                                                        parteentrega.NroParteEntrega,
                                                        parteentrega.Adicionales,
                                                        parteentrega.Conexion,
                                                        parteentrega.Desconexion,
                                                        parteentrega.Observacion,
                                                        parteentrega.nroRemito,
                                                        parteentrega.Mensual);
				if (update > 0)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}
      
		public bool ParteEntregaDelete(int IdParteEntrega)
		{
			try
			{
				int update = SqlHelper.ExecuteNonQuery(SqlImplHelper.getConnectionString(), "ParteEntregaDelete",
														IdParteEntrega);
				if (update > 0)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

        public ParteEntrega ParteEntregaGetById(int IdParteEntrega)
		{
			try
			{
				DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(), "ParteEntregaGetById",
														IdParteEntrega).Tables[0];
				ParteEntrega NewEnt = new ParteEntrega();

				if(dt.Rows.Count > 0)
				{
					DataRow dr = dt.Rows[0];
                    NewEnt = CargarParte(dr);

				}
				return NewEnt;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

        private ParteEntrega CargarParte(DataRow dr)
        {
            ParteEntrega NewEnt = new ParteEntrega();
            OrdenTrabajoImpl oOrdenTrabajoImpl = new OrdenTrabajoImpl();

            NewEnt.IdParteEntrega = int.Parse(dr["IdParteEntrega"].ToString());
            NewEnt.Fecha = DateTime.Parse(dr["Fecha"].ToString());
            NewEnt.HoraSalida = SqlImplHelper.getMinValueIfNull(dr["HoraSalida"].ToString());
            NewEnt.HoraLlegada = SqlImplHelper.getMinValueIfNull(dr["HoraLlegada"].ToString());
            NewEnt.IdOrdenTrabajo = int.Parse(dr["IdOrdenTrabajo"].ToString());
            NewEnt.NroParteEntrega = dr["NroParte"].ToString();
            NewEnt.Entrega = bool.Parse(dr["Entrega"].ToString());
            NewEnt.Adicionales = dr["Adicionales"].ToString();

            NewEnt.Conexion = bool.Parse(dr["Conexion"].ToString());
            NewEnt.Desconexion = bool.Parse(dr["desconexion"].ToString());
            NewEnt.Observacion = dr["Observacion"].ToString();
            NewEnt.nroRemito = dr["nroRemito"].ToString();

            NewEnt.oOrdenTrabajo = oOrdenTrabajoImpl.OrdenTrabajoGetById(NewEnt.IdOrdenTrabajo);
            NewEnt.oVerificaParte = (new VerificaParteImpl()).VerificaParteGetByIdParte(NewEnt.IdParteEntrega);

            return NewEnt;
        }

		public List<ParteEntrega> ParteEntregaGetAll()
		{
			List<ParteEntrega> lstParteEntrega = new List<ParteEntrega>();
			try
			{
				DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(), "ParteEntregaGetAll").Tables[0];
				if (dt.Rows.Count > 0)
				{
					for (int i = 0; dt.Rows.Count > i; i++)
					{
						ParteEntrega NewEnt = new ParteEntrega();
                        NewEnt = CargarParte(dt.Rows[i]);
                    	lstParteEntrega.Add(NewEnt);
					}
				}
				return lstParteEntrega;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		#endregion


        public int ParteEntregaAdd(ParteEntrega parteentrega, List<VerificaParte> oListCant)
        {
            try
            {
                int idParte = (int)SqlHelper.ExecuteScalar(SqlImplHelper.getTransaction(), "ParteEntregaAdd",
                                                        parteentrega.Fecha,
                                                        SqlImplHelper.getNullIfDateMinValue(parteentrega.HoraSalida),
                                                        SqlImplHelper.getNullIfDateMinValue(parteentrega.HoraLlegada),
                                                        parteentrega.IdOrdenTrabajo,
                                                        parteentrega.Entrega,
                                                        parteentrega.NroParteEntrega,
                                                        parteentrega.Adicionales,
                                                        parteentrega.Conexion,
                                                        parteentrega.Desconexion,
                                                        parteentrega.Observacion,
                                                        parteentrega.nroRemito);

                VerificaParteImpl oVerificaParteImpl = new VerificaParteImpl();
                foreach (VerificaParte verParte in oListCant)
                {
                    verParte.IdParte = idParte;
                    oVerificaParteImpl.VerificaParteAdd(verParte, true);
                }

                SqlImplHelper.commitTransaction();
                return idParte;
            }
            catch (Exception ex)
            {
                SqlImplHelper.rollbackTransaction();
                throw ex;
            }
        }

        public bool ParteEntregaUpdate(ParteEntrega oParteEntrega, List<VerificaParte> oListCant)
        {
            try
            {
                int update = SqlHelper.ExecuteNonQuery(SqlImplHelper.getTransaction(), "ParteEntregaUpdate",
                                                        oParteEntrega.IdParteEntrega,
                                                        oParteEntrega.Fecha,
                                                        oParteEntrega.IdOrdenTrabajo,
                                                        oParteEntrega.Entrega,
                                                        oParteEntrega.NroParteEntrega,
                                                        oParteEntrega.Adicionales,
                                                        oParteEntrega.Conexion,
                                                        oParteEntrega.Desconexion,
                                                        oParteEntrega.Observacion,
                                                        oParteEntrega.nroRemito,
                                                        oParteEntrega.Mensual);

                VerificaParteImpl oVerificaParteImpl = new VerificaParteImpl();
                VerificaParte oVerificaParteAux = new VerificaParte();
                foreach (VerificaParte verParte in oListCant)
                {
                    verParte.IdParte = oParteEntrega.IdParteEntrega;
                    oVerificaParteAux = oVerificaParteImpl.VerificaParteGetById(verParte.IdVerifica, verParte.IdParte, true);
                    if (oVerificaParteAux.IdVerifica == 0)
                        oVerificaParteImpl.VerificaParteAdd(verParte, true);
                    else
                        oVerificaParteImpl.VerificaParteUpdate(verParte, true);
                }

                if (update > 0)
                {
                    SqlImplHelper.commitTransaction();
                    return true;
                }
                else
                {
                    SqlImplHelper.rollbackTransaction();
                    return false;
                }
            }
            catch (Exception ex)
            {
                SqlImplHelper.rollbackTransaction();
                throw ex;
            }
        }

        public DataTable ValidarParteAcuerdo(int IdAcuerdo)
        {
            try
            {
                DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(), 
                                                        "ValidarParteAcuerdo",
                                                        IdAcuerdo).Tables[0];
                ParteEntrega NewEnt = new ParteEntrega();

                if (dt.Rows.Count > 0)
                {
                    return dt;

                }
                else
                    return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
