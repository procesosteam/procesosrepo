
///////////////////////////////////////////////////////////////////////////
//
// Generated by MyGeneration Version # (1.3.1.1) 
//
///////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using ProcesoPP.SqlServerLibrary;
using ProcesoPP.Model;

namespace ProcesoPP.SqlServerImpl
{
	public class CuadrillaImpl	
	{
		#region Cuadrilla methods

		public int CuadrillaAdd(Cuadrilla cuadrilla)
		{
			try
			{
				cuadrilla.IdCuadrilla = (int)SqlHelper.ExecuteScalar(SqlImplHelper.getTransaction(), "CuadrillaAdd",
														cuadrilla.Nombre, 
														cuadrilla.Baja, 
														cuadrilla.IdUsuario,
                                                        cuadrilla.FechaAlta,
                                                        cuadrilla.idSector,
                                                        cuadrilla.idEmpresa);


                CuadrillaPersonalAdd(cuadrilla.IdCuadrilla, cuadrilla.oListCuadrillaPersonal);

                SqlImplHelper.commitTransaction();
                return cuadrilla.IdCuadrilla;

			}
			catch(Exception ex)
			{
                SqlImplHelper.rollbackTransaction();
				throw ex;
			}
		}


        private void CuadrillaPersonalAdd(int idCuadrilla, List<CuadrillaPersonal> oListCuadrillaPersonal)
        {
            CuadrillaPersonalImpl oCuadrillaPersonalImpl = new CuadrillaPersonalImpl();
            foreach (CuadrillaPersonal oCP in oListCuadrillaPersonal)
            {
                oCP.IdCuadrilla = idCuadrilla;
                oCuadrillaPersonalImpl.CuadrillaPersonalAdd(oCP);
            }
        }

		public bool CuadrillaUpdate(Cuadrilla cuadrilla)
		{
			try
			{
				int update = SqlHelper.ExecuteNonQuery(SqlImplHelper.getTransaction(), "CuadrillaUpdate",
														cuadrilla.IdCuadrilla, 
														cuadrilla.Nombre, 
														cuadrilla.Baja, 
														cuadrilla.IdUsuario, 
														cuadrilla.FechaAlta,
                                                        cuadrilla.idSector,
                                                        cuadrilla.idEmpresa);

                CuadrillaPersonalImpl oCuadrillaPersonalImpl = new CuadrillaPersonalImpl();
                if (cuadrilla.oListCuadrillaPersonal.Count > 0)
                {
                    foreach (CuadrillaPersonal oCP in cuadrilla.oListCuadrillaPersonal)
                    {
                        if (oCP.IdCuadrillaPersonal > 0)
                        {
                            oCuadrillaPersonalImpl.CuadrillaPersonalUpdate(oCP);
                        }
                        else
                        {
                            oCP.IdCuadrilla = cuadrilla.IdCuadrilla;
                            oCuadrillaPersonalImpl.CuadrillaPersonalAdd(oCP);
                        }
                    }                    
                }                
                
                SqlImplHelper.commitTransaction();
                if (update > 0)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			catch(Exception ex)
			{
                SqlImplHelper.rollbackTransaction();
				throw ex;
			}
		}

		public bool CuadrillaDelete(int IdCuadrilla)
		{
			try
			{
				int update = SqlHelper.ExecuteNonQuery(SqlImplHelper.getConnectionString(), "CuadrillaDelete",
														IdCuadrilla);
				if (update > 0)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		public Cuadrilla CuadrillaGetById(int IdCuadrilla)
		{
			try
			{
				DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(), "CuadrillaGetById",
														IdCuadrilla).Tables[0];
				Cuadrilla NewEnt = new Cuadrilla();

				if(dt.Rows.Count > 0)
				{
					DataRow dr = dt.Rows[0];
					NewEnt = CargarCuadrilla(dr);
				}
				return NewEnt;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		public List<Cuadrilla> CuadrillaGetAll()
		{
			List<Cuadrilla> lstCuadrilla = new List<Cuadrilla>();
			try
			{
				DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(), "CuadrillaGetAll").Tables[0];
				if (dt.Rows.Count > 0)
				{
					for (int i = 0; dt.Rows.Count > i; i++)
					{
						DataRow dr = dt.Rows[i];
						Cuadrilla NewEnt = new Cuadrilla();
						NewEnt = CargarCuadrilla(dr);
						lstCuadrilla.Add(NewEnt);
					}
				}
				return lstCuadrilla;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		private Cuadrilla CargarCuadrilla(DataRow dr)
		{
			try
			{
				Cuadrilla oObjeto = new Cuadrilla();

				oObjeto.IdCuadrilla = Int32.Parse(dr["IdCuadrilla"].ToString());
				oObjeto.Nombre = dr["Nombre"].ToString();
				oObjeto.Baja = Boolean.Parse(dr["Baja"].ToString());
				oObjeto.IdUsuario = Int32.Parse(dr["IdUsuario"].ToString());
				oObjeto.FechaAlta = DateTime.Parse(dr["FechaAlta"].ToString());
                oObjeto.oListCuadrillaPersonal = (new CuadrillaPersonalImpl()).CuadrillaPersonalGetByIdCuadrilla(oObjeto.IdCuadrilla);
                oObjeto.idSector = SqlImplHelper.getCeroIfNull(dr["idSector"]);
                oObjeto.idEmpresa = SqlImplHelper.getCeroIfNull(dr["idEmpresa"]);

				return oObjeto;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		public List<Cuadrilla> CuadrillaGetAllBaja()
		{
			List<Cuadrilla> lstCuadrilla = new List<Cuadrilla>();
			try
			{
				DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(), "CuadrillaGetAllBaja").Tables[0];
				if (dt.Rows.Count > 0)
				{
					for (int i = 0; dt.Rows.Count > i; i++)
					{
						Cuadrilla NewEnt = new Cuadrilla();
						NewEnt = CargarCuadrilla(dt.Rows[i]);
						lstCuadrilla.Add(NewEnt);
					}
				}
				return lstCuadrilla;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		#endregion


        public DataTable CuadrillaGetByFilter(string nombre)
        {
            try
            {
                DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(),
                                                        "CuadrillaGetByFilter",
                                                        SqlImplHelper.getNullIfEmpty(nombre)).Tables[0];
                Cuadrilla NewEnt = new Cuadrilla();

                if (dt.Rows.Count > 0)
                {
                    return dt;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable CuadrillaGestionGetByFilter(string desde, string hasta, string idPersonal, string idCuadrilla, string idSector, string idEmpresa)
        {
            try
            {
                DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(),
                                                        "CuadrillaGestionGetByFilter",
                                                        SqlImplHelper.getNullIfEmpty(desde),
                                                        SqlImplHelper.getNullIfEmpty(hasta),
                                                        SqlImplHelper.getNullIfEmpty(idPersonal),
                                                        SqlImplHelper.getNullIfEmpty(idCuadrilla),
                                                        SqlImplHelper.getNullIfEmpty(idSector),
                                                        SqlImplHelper.getNullIfEmpty(idEmpresa)).Tables[0];
                Cuadrilla NewEnt = new Cuadrilla();

                if (dt.Rows.Count > 0)
                {
                    return dt;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable SolicitudCuadrillaPosibleGet(int nroSolicitud, int idTipoServicio)
        {
            try
            {
                DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(),
                                                        "SolicitudCuadrillaPosibleGet",
                                                        nroSolicitud,
                                                        idTipoServicio).Tables[0];
                Cuadrilla NewEnt = new Cuadrilla();

                if (dt.Rows.Count > 0)
                {
                    return dt;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
