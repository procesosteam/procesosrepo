
using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using ProcesoPP.SqlServerLibrary;
using ProcesoPP.Model;

namespace ProcesoPP.SqlServerImpl
{
	public class SolicitudClienteImpl	
	{
		#region SolicitudCliente methods

		public int SolicitudClienteAdd(SolicitudCliente solicitudcliente)
		{
			try
			{
                if (solicitudcliente.oMovimiento != null && solicitudcliente.oMovimiento.oLugarOrigen != null)
                {
                    MovimientoImpl oMovimientoImpl = new MovimientoImpl();
                    solicitudcliente.IdMovimiento = oMovimientoImpl.MovimientoAdd(solicitudcliente.oMovimiento);
                }

                solicitudcliente.IdSolicitud = (int)SqlHelper.ExecuteScalar(SqlImplHelper.getTransaction(), 
                                                        "SolicitudClienteAdd",
														solicitudcliente.Fecha, 
														solicitudcliente.FechaAlta, 
														solicitudcliente.IdUsuario, 
														solicitudcliente.IdTipoServicio, 													    
														solicitudcliente.IdMovimiento, 
														SqlImplHelper.getNullIfCero(solicitudcliente.IdSolicitante.ToString()), 														 
														solicitudcliente.NroPedidoCliente,
                                                        solicitudcliente.NroSolicitud,
                                                        solicitudcliente.Entrega,
                                                        SqlImplHelper.getNullIfCero(solicitudcliente.IdSector.ToString()),
                                                        solicitudcliente.ServicioAux,
                                                        SqlImplHelper.getNullIfDateMinValue(solicitudcliente.FechaEstimada),
                                                        solicitudcliente.nuevoMovimiento,
                                                        solicitudcliente.IdCondicion,
                                                        solicitudcliente.idSolicitudOrig);

                SolicitudEspecificacionImpl oSolicitudEspecificacionImpl = new SolicitudEspecificacionImpl();
                foreach (EspecificacionTecnica oEsp in solicitudcliente.oListEspecificaciones)
                {                    
                    SolicitudEspecificacion oSolicitudEspecificacion = new SolicitudEspecificacion();
                    oSolicitudEspecificacion.idSolicitud = solicitudcliente.IdSolicitud;
                    oSolicitudEspecificacion.idEspecificacion = oEsp.IdEspecificaciones;
                    oSolicitudEspecificacionImpl.SolicitudEspecificacionAdd(oSolicitudEspecificacion);
                }
                SqlImplHelper.commitTransaction();
                return solicitudcliente.IdSolicitud;
			}
			catch(Exception ex)
			{
                SqlImplHelper.rollbackTransaction();
				throw ex;
			}
		}

		public bool SolicitudClienteUpdate(SolicitudCliente solicitudcliente)
		{
			try
			{

                if (solicitudcliente.oMovimiento != null)
                {
                    MovimientoImpl oMovimientoImpl = new MovimientoImpl();
                    oMovimientoImpl.MovimientoUpdate(solicitudcliente.oMovimiento);
                }
				int update = SqlHelper.ExecuteNonQuery(SqlImplHelper.getTransaction(), 
                                                        "SolicitudClienteUpdate",
														solicitudcliente.IdSolicitud, 
														solicitudcliente.Fecha, 
														SqlImplHelper.getNullIfDateMinValue(solicitudcliente.Hora), 
														solicitudcliente.FechaAlta, 
														solicitudcliente.IdUsuario, 
														solicitudcliente.IdTipoServicio, 
														0, 
														solicitudcliente.IdMovimiento, 
														SqlImplHelper.getNullIfCero(solicitudcliente.IdSolicitante.ToString()), 
														solicitudcliente.IdCondicion,
                                                        solicitudcliente.NroPedidoCliente,
                                                        solicitudcliente.NroSolicitud,
                                                        solicitudcliente.Baja,
                                                        solicitudcliente.Entrega,
                                                        SqlImplHelper.getNullIfCero(solicitudcliente.IdSector.ToString()),
                                                        solicitudcliente.ServicioAux,
                                                        SqlImplHelper.getNullIfDateMinValue(solicitudcliente.FechaEstimada),
                                                        solicitudcliente.nuevoMovimiento,
                                                        solicitudcliente.idSolicitudOrig);


                SolicitudEspecificacionImpl oSolicitudEspecificacionImpl = new SolicitudEspecificacionImpl();
                oSolicitudEspecificacionImpl.SolicitudEspecificacionDelete(solicitudcliente.IdSolicitud);
                foreach (EspecificacionTecnica oEsp in solicitudcliente.oListEspecificaciones)
                {
                    SolicitudEspecificacion oSolicitudEspecificacion = new SolicitudEspecificacion();
                    oSolicitudEspecificacion.idSolicitud = solicitudcliente.IdSolicitud;
                    oSolicitudEspecificacion.idEspecificacion = oEsp.IdEspecificaciones;
                    oSolicitudEspecificacionImpl.SolicitudEspecificacionAdd(oSolicitudEspecificacion);
                }

                SqlImplHelper.commitTransaction();

				if (update > 0)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			catch(Exception ex)
			{
                SqlImplHelper.rollbackTransaction();
				throw ex;
			}
		}

		public bool SolicitudClienteDelete(int IdSolicitud)
		{
			try
			{
				int update = SqlHelper.ExecuteNonQuery(SqlImplHelper.getConnectionString(), "SolicitudClienteDelete",
														IdSolicitud);
				if (update > 0)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		public SolicitudCliente SolicitudClienteGetById(int IdSolicitud)
		{
			try
			{
				DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(), "SolicitudClienteGetById",
														IdSolicitud).Tables[0];
				SolicitudCliente NewEnt = new SolicitudCliente();
                
				if(dt.Rows.Count > 0)
				{
					DataRow dr = dt.Rows[0];
                    NewEnt = CargarSolicitudCliente(dr);

				}
				return NewEnt;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

        public SolicitudCliente SolicitudClienteGetById(int IdSolicitud, bool transac)
        {
            try
            {
                DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getTransaction(), "SolicitudClienteGetById",
                                                        IdSolicitud).Tables[0];
                SolicitudCliente NewEnt = new SolicitudCliente();

                if (dt.Rows.Count > 0)
                {
                    DataRow dr = dt.Rows[0];
                    NewEnt = CargarSolicitudCliente(dr);

                }
                return NewEnt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private SolicitudCliente CargarSolicitudCliente(DataRow dr)
        {
            SolicitudCliente NewEnt = new SolicitudCliente();

            CondicionImpl oCondicionImpl = new CondicionImpl();
            SolicitanteImpl oSolicitanteImpl = new SolicitanteImpl();
            MovimientoImpl oMovimientoImpl = new MovimientoImpl();
            TipoServicioImpl oTipoServicioImpl = new TipoServicioImpl();
            EspecificacionTecnicaImpl oEspecificacionTecnicaImpl = new EspecificacionTecnicaImpl();
            SectorImpl oSectorImpl = new SectorImpl();

            NewEnt.IdSolicitud = Int32.Parse(dr["IdSolicitud"].ToString());
            NewEnt.Fecha = SqlImplHelper.getMinValueIfNull(dr["Fecha"].ToString());            
            NewEnt.FechaAlta = SqlImplHelper.getMinValueIfNull(dr["FechaAlta"].ToString());
            NewEnt.IdUsuario = Int32.Parse(dr["IdUsuario"].ToString());
            NewEnt.IdTipoServicio = SqlImplHelper.getCeroIfNull(dr["IdTipoServicio"].ToString());
            NewEnt.IdMovimiento = SqlImplHelper.getCeroIfNull(dr["IdMovimiento"].ToString());
            NewEnt.IdSector = SqlImplHelper.getCeroIfNull(dr["IdSector"].ToString());
            NewEnt.IdSolicitante = SqlImplHelper.getCeroIfNull(dr["IdSolicitante"].ToString());
            NewEnt.IdCondicion = SqlImplHelper.getCeroIfNull(dr["IdCondicion"].ToString());
            NewEnt.NroPedidoCliente = dr["NroPedidoCliente"].ToString();
            NewEnt.NroSolicitud = SqlImplHelper.getCeroIfNull(dr["NroSolicitud"].ToString());            
            NewEnt.Entrega = bool.Parse(dr["Entrega"].ToString());
            NewEnt.ServicioAux = dr["ServicioAux"].ToString();
            NewEnt.FechaEstimada = SqlImplHelper.getMinValueIfNull(dr["FechaEstimada"].ToString());
            NewEnt.nuevoMovimiento = bool.Parse(dr["NuevoMovimiento"].ToString());
            NewEnt.letra = dr["letra"].ToString();
            NewEnt.fechaRegreso = dr["fechaRegreso"].ToString();
            if (dr["idSolicitudOrig"] != null)
                NewEnt.idSolicitudOrig = SqlImplHelper.getCeroIfNull(dr["idSolicitudOrig"].ToString());

            NewEnt.oListEspecificaciones = oEspecificacionTecnicaImpl.EspecificacionTecnicaGetByIdSolicitud(NewEnt.IdSolicitud);
            if (NewEnt.IdCondicion > 0)
                NewEnt.oCondicion = oCondicionImpl.CondicionGetById(NewEnt.IdCondicion);
            if (NewEnt.IdMovimiento > 0)
                NewEnt.oMovimiento = oMovimientoImpl.MovimientoGetById(NewEnt.IdMovimiento);
            if (NewEnt.IdSector > 0)
            {
                NewEnt.oSector = oSectorImpl.SectorGetById(NewEnt.IdSector);                
            }
            if (NewEnt.IdTipoServicio > 0)
                NewEnt.oTipoServicio = oTipoServicioImpl.TipoServicioGetById(NewEnt.IdTipoServicio);
            if (NewEnt.IdSolicitante > 0)
                NewEnt.oSolicitante = oSolicitanteImpl.SolicitanteGetById(NewEnt.IdSolicitante);
            
            return NewEnt;
        }

		public List<SolicitudCliente> SolicitudClienteGetAll()
		{
			List<SolicitudCliente> lstSolicitudCliente = new List<SolicitudCliente>();
			try
			{
				DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(), "SolicitudClienteGetAll").Tables[0];
				if (dt.Rows.Count > 0)
				{
					
					for (int i = 0; dt.Rows.Count > i; i++)
					{
                        DataRow dr = dt.Rows[0];
						SolicitudCliente NewEnt = new SolicitudCliente();
                        NewEnt = CargarSolicitudCliente(dr);
                        lstSolicitudCliente.Add(NewEnt);
					}
				}
				return lstSolicitudCliente;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		#endregion


        public int SolicitudClienteSugerirNro()
        {
            try
            {
                int UltimoNro = (int)SqlHelper.ExecuteScalar(SqlImplHelper.getConnectionString(), "SolicitudServicioGetUltimoNro");

                return UltimoNro + 1;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable SolicitudClienteGetByFilter(string NroSolicitud, string idEmpresa, string fechaDesde, string fechaHasta, string idTrailer, string idCuadrilla)
        {
            try
            {
                DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(),
                                                        "SolicitudClienteGetByFilter",
                                                        SqlImplHelper.getNullIfCero(NroSolicitud),
                                                        SqlImplHelper.getNullIfCero(idEmpresa),
                                                        SqlImplHelper.getNullIfDate(fechaDesde),
                                                        SqlImplHelper.getNullIfDate(fechaHasta),
                                                        SqlImplHelper.getNullIfCero(idTrailer),
                                                        SqlImplHelper.getNullIfCero(idCuadrilla)).Tables[0];

                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable SolicitudRegresoGetByFilter(string NroSolicitud, string idEmpresa, string fechaDesde, string fechaHasta, string idTrailer)
        {
            try
            {
                DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(),
                                                        "SolicitudRegresoGetByFilter",
                                                        SqlImplHelper.getNullIfCero(NroSolicitud),
                                                        SqlImplHelper.getNullIfCero(idEmpresa),
                                                        SqlImplHelper.getNullIfDate(fechaDesde),
                                                        SqlImplHelper.getNullIfDate(fechaHasta),
                                                        SqlImplHelper.getNullIfCero(idTrailer)).Tables[0];

                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable SolicitudClienteRegresoGetByFilter(string NroSolicitud, string idEmpresa, string fechaDesde, string fechaHasta)
        {
            try
            {
                DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(),
                                                        "SolicitudClienteRegresoGetByFilter",
                                                        SqlImplHelper.getNullIfCero(NroSolicitud),
                                                        SqlImplHelper.getNullIfCero(idEmpresa),
                                                        SqlImplHelper.getNullIfDate(fechaDesde),
                                                        SqlImplHelper.getNullIfDate(fechaHasta)).Tables[0];

                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable ReporteMovimiento(string fechaDesde, string fechaHasta, string idEmpresa, string idSector, string idCondicion, string idTrailer, string idOrigen, string idDestino, string idChofer, string idVehiculo, string nroSol, string TipoServicio, string remito)
        {
            try
            {

                DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(),
                                                        "ReporteMovimiento",
                                                        SqlImplHelper.getNullIfEmpty(fechaDesde),
                                                        SqlImplHelper.getNullIfEmpty(fechaHasta),
                                                        SqlImplHelper.getNullIfCero(idEmpresa),
                                                        SqlImplHelper.getNullIfCero(idSector),
                                                        SqlImplHelper.getNullIfCero(idCondicion),
                                                        SqlImplHelper.getNullIfCero(idTrailer),
                                                        SqlImplHelper.getNullIfCero(idOrigen),
                                                        SqlImplHelper.getNullIfCero(idDestino),
                                                        SqlImplHelper.getNullIfCero(idChofer),
                                                        SqlImplHelper.getNullIfCero(idVehiculo),
                                                        SqlImplHelper.getNullIfCero(nroSol),
                                                        SqlImplHelper.getNullIfCero(TipoServicio),
                                                        SqlImplHelper.getNullIfEmpty(remito)).Tables[0];

                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable SolicitudHistorialGetByFilter(string nroSolicitud, string idEmpresa, string fechaDesde, string fechaHasta, string idTrailer, string nroParte)
        {

            try
            {

                DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(),
                                                        "SolicitudHistorialGetByFilter",
                                                        SqlImplHelper.getNullIfCero(nroSolicitud),
                                                        SqlImplHelper.getNullIfCero(idEmpresa),
                                                        SqlImplHelper.getNullIfDate(fechaDesde),
                                                        SqlImplHelper.getNullIfDate(fechaHasta),
                                                        SqlImplHelper.getNullIfCero(idTrailer),
                                                        SqlImplHelper.getNullIfCero(nroParte)).Tables[0];

                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable ReportePosicionActual(string idEmpresa, string idSector, string idCondicion, string idEquipos, string idDestino, string idTipoServicio)
        {
            try
            {

                DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(),
                                                        "ReportePosicionActual",
                                                        SqlImplHelper.getNullIfCero(idEmpresa),
                                                        SqlImplHelper.getNullIfCero(idSector),
                                                        SqlImplHelper.getNullIfCero(idCondicion),
                                                        SqlImplHelper.getNullIfCero(idEquipos),
                                                        SqlImplHelper.getNullIfCero(idDestino),
                                                        SqlImplHelper.getNullIfCero(idTipoServicio)).Tables[0];

                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable SolicitudClienteSinOTI()
        {

            try
            {

                DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(),
                                                        "SolicitudClienteSinOTI").Tables[0];

                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable SolicitudClienteEliminadas(string nroSolicitud, string idEmpresa, string fechaDesde, string fechaHasta, string idTrailer)
        {
            try
            {

                DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(),
                                                        "SolicitudClienteEliminadas",
                                                        SqlImplHelper.getNullIfCero(nroSolicitud),
                                                        SqlImplHelper.getNullIfCero(idEmpresa),
                                                        SqlImplHelper.getNullIfDate(fechaDesde),
                                                        SqlImplHelper.getNullIfDate(fechaHasta),
                                                        SqlImplHelper.getNullIfCero(idTrailer)).Tables[0];

                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int SolicitudClienteGetFechaRegreso(int nroSolicitud, int idoSolicitud, DateTime fechaSC)
        {
            try
            {

                DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(),
                                                        "SolicitudClienteGetFechaRegreso",
                                                        nroSolicitud,
                                                        idoSolicitud,
                                                        fechaSC).Tables[0];
                int result;
                if (dt.Rows.Count > 0)
                    if (dt.Rows[0]["CantDias"].ToString().Length > 0)
                        result = int.Parse(dt.Rows[0]["CantDias"].ToString());
                    else
                        result = 0;
                else
                    result = 0;

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable ReporteAlertaMovimiento()
        {
            try
            {

                DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(),
                                                        "ReporteAlertaMovimiento").Tables[0];
            
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<SolicitudCliente> SolicitudClienteGetByNro(int nroSolicitud)
        {
            List<SolicitudCliente> lstSolicitudCliente = new List<SolicitudCliente>();
            try
            {
                DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(), "SolicitudClienteGetByNro",
                                                                                                nroSolicitud).Tables[0];
                if (dt.Rows.Count > 0)
                {

                    for (int i = 0; dt.Rows.Count > i; i++)
                    {
                        DataRow dr = dt.Rows[i];
                        SolicitudCliente NewEnt = new SolicitudCliente();
                        NewEnt = CargarSolicitudCliente(dr);
                        lstSolicitudCliente.Add(NewEnt);
                    }
                }
                return lstSolicitudCliente;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public TipoServicio SolicitudClienteGetByIdMovimiento(int idMovimiento)
        {
            try
            {
                TipoServicio NewEnt = new TipoServicio();
                DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(), "SOLUCITUDCLIENTEGETBYIDMOVIMIENTO",
                                                                                                idMovimiento).Tables[0];
                if (dt.Rows.Count > 0)
                {
                    DataRow dr = dt.Rows[0];                    
                    NewEnt = (new TipoServicioImpl()).CargarTipoServicio(dr);                    
                }

                return NewEnt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable ReporteAlquileresMensuales(string fechaDesde, string fechaHasta, string idEmpresa, string idSector, string idTrailer, string nroSol, string TipoServicio, string remito)
        {
            try
            {

                DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(),
                                                        "ReporteAlquileresMensuales",
                                                        SqlImplHelper.getNullIfEmpty(fechaDesde),
                                                        SqlImplHelper.getNullIfEmpty(fechaHasta),
                                                        SqlImplHelper.getNullIfCero(idEmpresa),
                                                        SqlImplHelper.getNullIfCero(idSector),
                                                        SqlImplHelper.getNullIfCero(idTrailer),
                                                        SqlImplHelper.getNullIfCero(nroSol),
                                                        SqlImplHelper.getNullIfCero(TipoServicio),
                                                        SqlImplHelper.getNullIfEmpty(remito)).Tables[0];

                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
