using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using ProcesoPP.SqlServerLibrary;
using ProcesoPP.Model;

namespace ProcesoPP.Implement
{
	public class PermisoConduccionImpl	
	{
		#region PermisoConduccion methods

		public int PermisoConduccionAdd(PermisoConduccion permisoconduccion)
		{
			try
			{
				permisoconduccion.IdPermiso= (int)SqlHelper.ExecuteScalar(SqlImplHelper.getConnectionString(), "PermisoConduccionAdd",
														permisoconduccion.Descripcion,
                                                        permisoconduccion.IdPersonalACargo,
                                                        permisoconduccion.IdPersonal,
                                                        SqlImplHelper.getNullIfDateMinValue(permisoconduccion.Fecha),
                                                        SqlImplHelper.getNullIfDateMinValue(permisoconduccion.FechaOriginal));
                SqlImplHelper.commitTransaction();
                return permisoconduccion.IdPermiso;

			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		public bool PermisoConduccionUpdate(PermisoConduccion permisoconduccion)
		{
			try
			{
				int update = SqlHelper.ExecuteNonQuery(SqlImplHelper.getConnectionString(), "PermisoConduccionUpdate",
														permisoconduccion.IdPermiso, 
														permisoconduccion.Descripcion,
                                                        permisoconduccion.IdPersonalACargo,
                                                        permisoconduccion.IdPersonal,
                                                        SqlImplHelper.getNullIfDateMinValue(permisoconduccion.Fecha),
                                                        SqlImplHelper.getNullIfDateMinValue(permisoconduccion.FechaOriginal));
				if (update > 0)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		public bool PermisoConduccionDelete(PermisoConduccion oPermisoConduccion)
		{
			try
			{
				int update = SqlHelper.ExecuteNonQuery(SqlImplHelper.getConnectionString(), "PermisoConduccionDelete",oPermisoConduccion.IdPermiso
														);
				if (update > 0)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		public PermisoConduccion PermisoConduccionGetById(int id)
		{
			try
			{

				DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(), "PermisoConduccionGetById",id
														).Tables[0];
				PermisoConduccion NewEnt = new PermisoConduccion();

				if(dt.Rows.Count > 0)
				{
					DataRow dr = dt.Rows[0];
					NewEnt = CargarPermisoConduccion(dr);
				}
				return NewEnt;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		public List<PermisoConduccion> PermisoConduccionGetAll()
		{
			List<PermisoConduccion> lstPermisoConduccion = new List<PermisoConduccion>();
			try
			{
				DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(), "PermisoConduccionGetAll").Tables[0];
				if (dt.Rows.Count > 0)
				{
					for (int i = 0; dt.Rows.Count > i; i++)
					{
						DataRow dr = dt.Rows[i];
						PermisoConduccion NewEnt = new PermisoConduccion();
						NewEnt = CargarPermisoConduccion(dr);
						lstPermisoConduccion.Add(NewEnt);
					}
				}
				return lstPermisoConduccion;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

        public List<PermisoConduccion> PermisoConduccionGetByLegajo(int idLegajo)
        {
            List<PermisoConduccion> lstPermisoConduccion = new List<PermisoConduccion>();
            try
            {
                DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(), "PermisoConduccionGetByLegajo",idLegajo).Tables[0];
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; dt.Rows.Count > i; i++)
                    {
                        DataRow dr = dt.Rows[i];
                        PermisoConduccion NewEnt = new PermisoConduccion();
                        NewEnt = CargarPermisoConduccion(dr);
                        lstPermisoConduccion.Add(NewEnt);
                    }
                }
                return lstPermisoConduccion;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

		private PermisoConduccion CargarPermisoConduccion(DataRow dr)
		{
			try
			{
				PermisoConduccion oObjeto = new PermisoConduccion();

				oObjeto.IdPermiso = int.Parse(dr["IdPermiso"].ToString());
				oObjeto.Descripcion = (dr["Descripcion"].ToString());
                oObjeto.IdPersonalACargo = int.Parse(dr["IdPersonalACargo"].ToString());
                oObjeto.IdPersonal = int.Parse(dr["IdPersonal"].ToString());
                oObjeto.Fecha = SqlImplHelper.getMinValueIfNull(dr["Fecha"].ToString());
                oObjeto.FechaOriginal = SqlImplHelper.getMinValueIfNull(dr["FechaOriginal"].ToString());
                
				return oObjeto;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		#endregion

	}
}
