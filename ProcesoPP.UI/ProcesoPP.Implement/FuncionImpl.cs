
///////////////////////////////////////////////////////////////////////////
//
// Generated by MyGeneration Version # (1.3.0.9) 
//
///////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using ProcesoPP.SqlServerLibrary;
using ProcesoPP.Model;

namespace ProcesoPP.SqlServerImpl
{
	public class FuncionImpl	
	{
		#region Funcion methods

		public int FuncionAdd(Funcion funcion)
		{
			try
			{
				funcion.IdFuncion= (int)SqlHelper.ExecuteScalar(SqlImplHelper.getConnectionString(), "FuncionAdd",
														funcion.Descripcion, 
														funcion.Baja);
                SqlImplHelper.commitTransaction();
                return funcion.IdFuncion;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		public bool FuncionUpdate(Funcion funcion)
		{
			try
			{
				int update = SqlHelper.ExecuteNonQuery(SqlImplHelper.getConnectionString(), "FuncionUpdate",
														funcion.IdFuncion, 
														funcion.Descripcion, 
														funcion.Baja);
				if (update > 0)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		public bool FuncionDelete(int IdFuncion)
		{
			try
			{
				int update = SqlHelper.ExecuteNonQuery(SqlImplHelper.getConnectionString(), "FuncionDelete",
														IdFuncion);
				if (update > 0)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		public Funcion FuncionGetById(int IdFuncion)
		{
			try
			{
				DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(), "FuncionGetById",
														IdFuncion).Tables[0];
				Funcion NewEnt = new Funcion();

				if(dt.Rows.Count > 0)
				{
					DataRow dr = dt.Rows[0];
					NewEnt = CargarFuncion(dr);
				}
				return NewEnt;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		public List<Funcion> FuncionGetAll()
		{
			List<Funcion> lstFuncion = new List<Funcion>();
			try
			{
				DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(), "FuncionGetAll").Tables[0];
				if (dt.Rows.Count > 0)
				{
					for (int i = 0; dt.Rows.Count > i; i++)
					{
						DataRow dr = dt.Rows[i];
						Funcion NewEnt = new Funcion();
						NewEnt = CargarFuncion(dr);
						lstFuncion.Add(NewEnt);
					}
				}
				return lstFuncion;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		private Funcion CargarFuncion(DataRow dr)
		{
			try
			{
				Funcion oObjeto = new Funcion();

				oObjeto.IdFuncion = int.Parse(dr["IdFuncion"].ToString());
				oObjeto.Descripcion = dr["Descripcion"].ToString();
				oObjeto.Baja = bool.Parse(dr["Baja"].ToString());
				
				return oObjeto;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		public List<Funcion> FuncionGetAllBaja()
		{
			List<Funcion> lstFuncion = new List<Funcion>();
			try
			{
				DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(), "FuncionGetAllBaja").Tables[0];
				if (dt.Rows.Count > 0)
				{
					for (int i = 0; dt.Rows.Count > i; i++)
					{
						Funcion NewEnt = new Funcion();
						NewEnt = CargarFuncion(dt.Rows[i]);
						lstFuncion.Add(NewEnt);
					}
				}
				return lstFuncion;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		#endregion

	}
}
