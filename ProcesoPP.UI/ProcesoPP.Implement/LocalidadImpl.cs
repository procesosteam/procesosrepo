
///////////////////////////////////////////////////////////////////////////
//
// Generated by MyGeneration Version # (1.3.0.9) 
//
///////////////////////////////////////////////////////////////////////////

using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using ProcesoPP.SqlServerLibrary;
using ProcesoPP.Model;

namespace ProcesoPP.SqlServerImpl
{
	public class LocalidadImpl	
	{
		#region Localidad methods

		public int LocalidadAdd(Localidad localidad)
		{
			try
			{
				return (int)SqlHelper.ExecuteScalar(SqlImplHelper.getConnectionString(), "LocalidadAdd",
														localidad.IdDepartamento, 
														localidad.Nombre);
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		public bool LocalidadUpdate(Localidad localidad)
		{
			try
			{
				int update = SqlHelper.ExecuteNonQuery(SqlImplHelper.getConnectionString(), "LocalidadUpdate",
														localidad.IdLocalidad, 
														localidad.IdDepartamento, 
														localidad.Nombre);
				if (update > 0)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		public bool LocalidadDelete(int IdLocalidad)
		{
			try
			{
				int update = SqlHelper.ExecuteNonQuery(SqlImplHelper.getConnectionString(), "LocalidadDelete",
														IdLocalidad);
				if (update > 0)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		public Localidad LocalidadGetById(int IdLocalidad)
		{
			try
			{
				DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(), "LocalidadGetById",
														IdLocalidad).Tables[0];
				Localidad NewEnt = new Localidad();

				if(dt.Rows.Count > 0)
				{
					DataRow dr = dt.Rows[0];
					NewEnt = CargarLocalidad(dr);
				}
				return NewEnt;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		public List<Localidad> LocalidadGetAll()
		{
			List<Localidad> lstLocalidad = new List<Localidad>();
			try
			{
				DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(), "LocalidadGetAll").Tables[0];
				if (dt.Rows.Count > 0)
				{
					for (int i = 0; dt.Rows.Count > i; i++)
					{
						DataRow dr = dt.Rows[i];
						Localidad NewEnt = new Localidad();
						NewEnt = CargarLocalidad(dr);
						lstLocalidad.Add(NewEnt);
					}
				}
				return lstLocalidad;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

        public List<Localidad> LocalidadGetByDepto(int idDepto)
        {
            List<Localidad> lstLocalidad = new List<Localidad>();
            try
            {
                DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(), "LocalidadGetAll").Tables[0];
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; dt.Rows.Count > i; i++)
                    {
                        DataRow dr = dt.Rows[i];
                        Localidad NewEnt = new Localidad();
                        NewEnt = CargarLocalidad(dr);
                        if (NewEnt.IdDepartamento == idDepto)
                        {
                            lstLocalidad.Add(NewEnt);
                        }
                    }
                }
                return lstLocalidad;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

		private Localidad CargarLocalidad(DataRow dr)
		{
			try
			{
				Localidad oObjeto = new Localidad();

				oObjeto.IdLocalidad = int.Parse(dr["IdLocalidad"].ToString());
				oObjeto.IdDepartamento = int.Parse(dr["IdDepartamento"].ToString());
				oObjeto.Nombre = (dr["Nombre"].ToString());
				
				return oObjeto;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		#endregion


        public List<Localidad> LocalidadGetByIdProvincia(int idProvincia)
        {
            List<Localidad> lstLocalidad = new List<Localidad>();
            try
            {
                DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(), "LocalidadGetByIdProvincia", idProvincia).Tables[0];
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; dt.Rows.Count > i; i++)
                    {
                        DataRow dr = dt.Rows[i];
                        Localidad NewEnt = new Localidad();
                        NewEnt = CargarLocalidad(dr);
                        lstLocalidad.Add(NewEnt);
                    }
                }
                return lstLocalidad;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
