
using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using ProcesoPP.SqlServerLibrary;
using ProcesoPP.Model;

namespace ProcesoPP.SqlServerImpl
{
	public class CodificacionImpl	
	{
		#region Codificacion methods

		public int CodificacionAdd(Codificacion codificacion)
		{
			try
			{
				codificacion.IdCodificacion= (int)SqlHelper.ExecuteScalar(SqlImplHelper.getConnectionString(), "CodificacionAdd",
														codificacion.Descripcion,
                                                        codificacion.Codigo,
                                                        codificacion.Equipo);
                SqlImplHelper.commitTransaction();
                return codificacion.IdCodificacion;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		public bool CodificacionUpdate(Codificacion codificacion)
		{
			try
			{
				int update = SqlHelper.ExecuteNonQuery(SqlImplHelper.getConnectionString(), "CodificacionUpdate",
														codificacion.IdCodificacion, 
														codificacion.Descripcion,
                                                        codificacion.Codigo,
                                                        codificacion.Equipo);
                SqlImplHelper.commitTransaction();
				if (update > 0)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		public bool CodificacionDelete(int IdCodificacion)
		{
			try
			{
				int update = SqlHelper.ExecuteNonQuery(SqlImplHelper.getConnectionString(), "CodificacionDelete",
														IdCodificacion);
				if (update > 0)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		public Codificacion CodificacionGetById(int IdCodificacion)
		{
			try
			{
				DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(), "CodificacionGetById",
														IdCodificacion).Tables[0];
				Codificacion NewEnt = new Codificacion();

				if(dt.Rows.Count > 0)
				{
					DataRow dr = dt.Rows[0];
					NewEnt = CargarCodificacion(dr);
				}
				return NewEnt;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		public List<Codificacion> CodificacionGetAll()
		{
			List<Codificacion> lstCodificacion = new List<Codificacion>();
			try
			{
				DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(), "CodificacionGetAll").Tables[0];
				if (dt.Rows.Count > 0)
				{
					for (int i = 0; dt.Rows.Count > i; i++)
					{
						DataRow dr = dt.Rows[i];
						Codificacion NewEnt = new Codificacion();
						NewEnt = CargarCodificacion(dr);
						lstCodificacion.Add(NewEnt);
					}
				}
				return lstCodificacion;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

        public List<Codificacion> CodificacionGetEquipo()
        {
            List<Codificacion> lstCodificacion = new List<Codificacion>();
            try
            {
                DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(), "CodificacionGetEquipo").Tables[0];
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; dt.Rows.Count > i; i++)
                    {
                        DataRow dr = dt.Rows[i];
                        Codificacion NewEnt = new Codificacion();
                        NewEnt = CargarCodificacion(dr);
                        lstCodificacion.Add(NewEnt);
                    }
                }
                return lstCodificacion;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Codificacion> CodificacionGetVehiculo()
        {
            List<Codificacion> lstCodificacion = new List<Codificacion>();
            try
            {
                DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(), "CodificacionGetVehiculo").Tables[0];
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; dt.Rows.Count > i; i++)
                    {
                        DataRow dr = dt.Rows[i];
                        Codificacion NewEnt = new Codificacion();
                        NewEnt = CargarCodificacion(dr);
                        lstCodificacion.Add(NewEnt);
                    }
                }
                return lstCodificacion;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

		private Codificacion CargarCodificacion(DataRow dr)
		{
			try
			{
				Codificacion oObjeto = new Codificacion();

				oObjeto.IdCodificacion = int.Parse(dr["IdCodificacion"].ToString());
				oObjeto.Descripcion = (dr["Descripcion"].ToString());
                oObjeto.Codigo = (dr["Codigo"].ToString());
                oObjeto.Equipo = SqlImplHelper.getFalseIfNull(dr["Equipo"].ToString());
				return oObjeto;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		#endregion

	}
}
