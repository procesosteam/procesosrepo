
using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using ProcesoPP.SqlServerLibrary;
using ProcesoPP.Model;

namespace ProcesoPP.SqlServerImpl
{
	public class VehiculoIdentificacionImpl	
	{
		#region VehiculoIdentificacion methods

		public int VehiculoIdentificacionAdd(VehiculoIdentificacion vehiculoidentificacion)
		{
			try
			{
				vehiculoidentificacion.IdIdentificacion= (int)SqlHelper.ExecuteScalar(SqlImplHelper.getConnectionString(), "VehiculoIdentificacionAdd",
														vehiculoidentificacion.IdVehiculo, 
														vehiculoidentificacion.IdCodificacion, 
														vehiculoidentificacion.NroTacografo, 
														vehiculoidentificacion.Alta, 
														vehiculoidentificacion.Baja,
                                                        vehiculoidentificacion.NroCodificacion);
                SqlImplHelper.commitTransaction();
                return vehiculoidentificacion.IdIdentificacion;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		public bool VehiculoIdentificacionUpdate(VehiculoIdentificacion vehiculoidentificacion)
		{
			try
			{
				int update = SqlHelper.ExecuteNonQuery(SqlImplHelper.getConnectionString(), "VehiculoIdentificacionUpdate",
														vehiculoidentificacion.IdIdentificacion, 
														vehiculoidentificacion.IdVehiculo, 
														vehiculoidentificacion.IdCodificacion, 
														vehiculoidentificacion.NroTacografo, 
														vehiculoidentificacion.Alta, 
														vehiculoidentificacion.Baja,
                                                        vehiculoidentificacion.NroCodificacion);
                SqlImplHelper.commitTransaction();
				if (update > 0)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		public bool VehiculoIdentificacionDelete(int IdIdentificacion)
		{
			try
			{
				int update = SqlHelper.ExecuteNonQuery(SqlImplHelper.getConnectionString(), "VehiculoIdentificacionDelete",
														IdIdentificacion);
				if (update > 0)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		public VehiculoIdentificacion VehiculoIdentificacionGetById(int IdIdentificacion)
		{
			try
			{
				DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(), "VehiculoIdentificacionGetById",
														IdIdentificacion).Tables[0];
				VehiculoIdentificacion NewEnt = new VehiculoIdentificacion();

				if(dt.Rows.Count > 0)
				{
					DataRow dr = dt.Rows[0];
					NewEnt = CargarVehiculoIdentificacion(dr);
				}
				return NewEnt;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		public List<VehiculoIdentificacion> VehiculoIdentificacionGetAll()
		{
			List<VehiculoIdentificacion> lstVehiculoIdentificacion = new List<VehiculoIdentificacion>();
			try
			{
				DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(), "VehiculoIdentificacionGetAll").Tables[0];
				if (dt.Rows.Count > 0)
				{
					for (int i = 0; dt.Rows.Count > i; i++)
					{
						DataRow dr = dt.Rows[i];
						VehiculoIdentificacion NewEnt = new VehiculoIdentificacion();
						NewEnt = CargarVehiculoIdentificacion(dr);
						lstVehiculoIdentificacion.Add(NewEnt);
					}
				}
				return lstVehiculoIdentificacion;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		private VehiculoIdentificacion CargarVehiculoIdentificacion(DataRow dr)
		{
			try
			{
				VehiculoIdentificacion oObjeto = new VehiculoIdentificacion();

				oObjeto.IdIdentificacion = Int32.Parse(dr["IdIdentificacion"].ToString());
				oObjeto.IdVehiculo = Int32.Parse(dr["IdVehiculo"].ToString());
				oObjeto.IdCodificacion = Int32.Parse(dr["IdCodificacion"].ToString());
				oObjeto.NroTacografo = dr["NroTacografo"].ToString();
				oObjeto.Alta = Boolean.Parse(dr["Alta"].ToString());
				oObjeto.Baja = Boolean.Parse(dr["Baja"].ToString());
                oObjeto.NroCodificacion = (dr["NroCodificacion"].ToString());
                if (oObjeto.IdCodificacion > 0)
                    oObjeto.oCodificacion = (new CodificacionImpl()).CodificacionGetById(oObjeto.IdCodificacion);
				return oObjeto;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		public List<VehiculoIdentificacion> VehiculoIdentificacionGetAllBaja()
		{
			List<VehiculoIdentificacion> lstVehiculoIdentificacion = new List<VehiculoIdentificacion>();
			try
			{
				DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(), "VehiculoIdentificacionGetAllBaja").Tables[0];
				if (dt.Rows.Count > 0)
				{
					for (int i = 0; dt.Rows.Count > i; i++)
					{
						VehiculoIdentificacion NewEnt = new VehiculoIdentificacion();
						NewEnt = CargarVehiculoIdentificacion(dt.Rows[i]);
						lstVehiculoIdentificacion.Add(NewEnt);
					}
				}
				return lstVehiculoIdentificacion;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

        public VehiculoIdentificacion VehiculoIdentificacionGetByIdVehiculo(int IdVehiculo)
        {
            try
            {
                DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(), "VehiculoIdentificacionGetByIdVehiculo",
                                                        IdVehiculo).Tables[0];
                VehiculoIdentificacion NewEnt = new VehiculoIdentificacion();

                if (dt.Rows.Count > 0)
                {
                    DataRow dr = dt.Rows[0];
                    NewEnt = CargarVehiculoIdentificacion(dr);
                }
                return NewEnt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

		#endregion


        
    }
}
