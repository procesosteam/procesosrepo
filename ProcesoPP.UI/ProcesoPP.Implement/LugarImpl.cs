﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ProcesoPP.Model;
using System.Data;
using ProcesoPP.SqlServerLibrary;

namespace ProcesoPP.SqlServerImpl
{
    public class LugarImpl
    {
        #region Lugar methods

        public int LugarAdd(Lugar Lugar)
        {
            try
            {
                return (int)SqlHelper.ExecuteScalar(SqlImplHelper.getConnectionString(), "LugarAdd",
                                                        Lugar.Descripcion);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool LugarUpdate(Lugar Lugar)
        {
            try
            {
                int update = SqlHelper.ExecuteNonQuery(SqlImplHelper.getConnectionString(), "LugarUpdate",
                                                        Lugar.IdLugar,
                                                        Lugar.Descripcion);
                if (update > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool LugarDelete(int IdLugar)
        {
            try
            {
                int update = SqlHelper.ExecuteNonQuery(SqlImplHelper.getConnectionString(), "LugarDelete",
                                                        IdLugar);
                if (update > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Lugar LugarGetById(int IdLugar)
        {
            try
            {
                DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(), "LugarGetById",
                                                        IdLugar).Tables[0];
                Lugar NewEnt = new Lugar();

                if (dt.Rows.Count > 0)
                {
                    DataRow dr = dt.Rows[0];
                    NewEnt.IdLugar = Int32.Parse(dr["IdLugar"].ToString());
                    NewEnt.Descripcion = dr["Descripcion"].ToString();
                }
                return NewEnt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Lugar> LugarGetAll()
        {
            List<Lugar> lstLugar = new List<Lugar>();
            try
            {
                DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(), "LugarGetAll").Tables[0];
                if (dt.Rows.Count > 0)
                {
                    int colIdLugar = dt.Columns["IdLugar"].Ordinal;
                    int colDescripcion = dt.Columns["Descripcion"].Ordinal;
                    for (int i = 0; dt.Rows.Count > i; i++)
                    {
                        Lugar NewEnt = new Lugar();
                        NewEnt.IdLugar = Int32.Parse(dt.Rows[i].ItemArray[colIdLugar].ToString());
                        NewEnt.Descripcion = dt.Rows[i].ItemArray[colDescripcion].ToString();
                        lstLugar.Add(NewEnt);
                    }
                }
                return lstLugar;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion
    }
}
