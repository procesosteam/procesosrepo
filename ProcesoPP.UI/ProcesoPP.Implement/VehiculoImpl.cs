
using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using ProcesoPP.SqlServerLibrary;
using ProcesoPP.Model;

namespace ProcesoPP.SqlServerImpl
{
	public class VehiculoImpl	
	{
        #region Vehiculo methods

        public int VehiculoAdd(Vehiculo vehiculo)
        {
            try
            {
                vehiculo.IdVehiculo = (int)SqlHelper.ExecuteScalar(SqlImplHelper.getConnectionString(), "VehiculoAdd",
                                                        vehiculo.IdMarca,
                                                        vehiculo.Modelo,
                                                        vehiculo.Anio,
                                                        vehiculo.Patente,
                                                        vehiculo.IdEmpresa,
                                                        vehiculo.IdSector,
                                                        SqlImplHelper.getNullIfDateMinValue(vehiculo.FechaAltaEmpresa),
                                                        SqlImplHelper.getNullIfDateMinValue(vehiculo.FechaBajaEmpresa),
                                                        vehiculo.IdUsuario,
                                                        SqlImplHelper.getNullIfDateMinValue(vehiculo.FechaAlta),
                                                        vehiculo.Titular,
                                                        vehiculo.PotenciaMotor,
                                                        vehiculo.Color,
                                                        vehiculo.NroMotor,
                                                        vehiculo.NroChasis,
                                                        vehiculo.Alta,
                                                        vehiculo.Baja,
                                                        vehiculo.EsEquipo);
                
                return vehiculo.IdVehiculo;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool VehiculoUpdate(Vehiculo vehiculo)
        {
            try
            {
                int update = SqlHelper.ExecuteNonQuery(SqlImplHelper.getConnectionString(), "VehiculoUpdate",
                                                        vehiculo.IdVehiculo,
                                                        vehiculo.IdMarca,
                                                        vehiculo.Modelo,
                                                        vehiculo.Anio,
                                                        vehiculo.Patente,
                                                        vehiculo.IdEmpresa,
                                                        vehiculo.IdSector,
                                                        SqlImplHelper.getNullIfDateMinValue(vehiculo.FechaAltaEmpresa),
                                                        SqlImplHelper.getNullIfDateMinValue(vehiculo.FechaBajaEmpresa),
                                                        vehiculo.IdUsuario,
                                                        SqlImplHelper.getNullIfDateMinValue(vehiculo.FechaAlta),
                                                        vehiculo.Titular,
                                                        vehiculo.PotenciaMotor,
                                                        vehiculo.Color,
                                                        vehiculo.NroMotor,
                                                        vehiculo.NroChasis,
                                                        vehiculo.Alta,
                                                        vehiculo.Baja,
                                                        vehiculo.EsEquipo);
            
				if (update > 0)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		public bool VehiculoDelete(int IdVehiculo)
		{
			try
			{
				int update = SqlHelper.ExecuteNonQuery(SqlImplHelper.getConnectionString(), "VehiculoDelete",
														IdVehiculo);
				if (update > 0)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		public Vehiculo VehiculoGetById(int IdVehiculo)
		{
			try
			{
				DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(), "VehiculoGetById",
														IdVehiculo).Tables[0];
				Vehiculo NewEnt = new Vehiculo();

				if(dt.Rows.Count > 0)
				{
					DataRow dr = dt.Rows[0];
					NewEnt = CargarVehiculo(dr);
				}
				return NewEnt;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

        public DataTable VehiculoGetByFilter(string patente, string ruta, string vCedula, string vPatente, string vtv, string idCod, string idEMpresa)
        {
            try
            {
                DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(),
                                                        "VehiculoGestionGetByFilter",
                                                        SqlImplHelper.getNullIfEmpty(patente),
                                                        SqlImplHelper.getNullIfEmpty(ruta),
                                                        SqlImplHelper.getNullIfEmpty(vtv),
                                                        SqlImplHelper.getNullIfEmpty(vCedula),
                                                        SqlImplHelper.getNullIfEmpty(vPatente),
                                                        SqlImplHelper.getNullIfCero(idCod),
                                                        SqlImplHelper.getNullIfCero(idEMpresa)).Tables[0];
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable EquipoGetByFilter(string patente, string ruta, string vCedula, string vPatente, string vtv, string idCod, string idEMpresa)
        {
            try
            {
                DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(),
                                                        "EquipoGestionGetByFilter",
                                                        SqlImplHelper.getNullIfEmpty(patente),
                                                        SqlImplHelper.getNullIfEmpty(ruta),
                                                        SqlImplHelper.getNullIfEmpty(vtv),
                                                        SqlImplHelper.getNullIfEmpty(vCedula),
                                                        SqlImplHelper.getNullIfEmpty(vPatente),
                                                        SqlImplHelper.getNullIfCero(idCod),
                                                        SqlImplHelper.getNullIfCero(idEMpresa)).Tables[0];
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable VehiculoHistorialGetByFilter(string patente, string ruta, string vCedula, string vPatente, string vtv, string idCod, string idEMpresa)
        {
            try
            {
                DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(),
                                                        "VehiculoHistorialGetByFilter",
                                                        SqlImplHelper.getNullIfEmpty(patente),
                                                        SqlImplHelper.getNullIfEmpty(ruta),
                                                        SqlImplHelper.getNullIfEmpty(vtv),
                                                        SqlImplHelper.getNullIfEmpty(vCedula),
                                                        SqlImplHelper.getNullIfEmpty(vPatente),
                                                        SqlImplHelper.getNullIfCero(idCod),
                                                        SqlImplHelper.getNullIfCero(idEMpresa)).Tables[0];
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable VehiculoGetVehiculos()
        {            
            try
            {
                DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(), "VehiculoGetVehiculos").Tables[0];
                if (dt.Rows.Count > 0)
                {
                    return dt;
                }
                else
                    return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable VehiculoGetEquipos()
        {
            
            try
            {
                DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(), "VehiculoGetEquipos").Tables[0];
                if (dt.Rows.Count > 0)
                {
                    return dt;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        
		public List<Vehiculo> VehiculoGetAll()
		{
			List<Vehiculo> lstVehiculo = new List<Vehiculo>();
			try
			{
				DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(), "VehiculoGetAll").Tables[0];
				if (dt.Rows.Count > 0)
				{
					for (int i = 0; dt.Rows.Count > i; i++)
					{
						DataRow dr = dt.Rows[i];
						Vehiculo NewEnt = new Vehiculo();
						NewEnt = CargarVehiculo(dr);
						lstVehiculo.Add(NewEnt);
					}
				}
				return lstVehiculo;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		public Vehiculo CargarVehiculo(DataRow dr)
		{
			try
			{
				Vehiculo oObjeto = new Vehiculo();

				oObjeto.IdVehiculo = Int32.Parse(dr["IdVehiculo"].ToString());
				oObjeto.IdMarca = Int32.Parse(dr["IdMarca"].ToString());
				oObjeto.Modelo = dr["Modelo"].ToString();
				oObjeto.Anio = Int32.Parse(dr["Anio"].ToString());
				oObjeto.Patente = dr["Patente"].ToString();
				oObjeto.IdEmpresa = Int32.Parse(dr["IdEmpresa"].ToString());
				oObjeto.IdSector = Int32.Parse(dr["IdSector"].ToString());
                oObjeto.FechaAltaEmpresa = SqlImplHelper.getMinValueIfNull(dr["FechaAltaEmpresa"].ToString());
                oObjeto.FechaBajaEmpresa = SqlImplHelper.getMinValueIfNull(dr["FechaBajaEmpresa"].ToString());
				oObjeto.IdUsuario = Int32.Parse(dr["IdUsuario"].ToString());
				oObjeto.FechaAlta = SqlImplHelper.getMinValueIfNull(dr["FechaAlta"].ToString());
				oObjeto.Titular = dr["Titular"].ToString();
				oObjeto.PotenciaMotor = dr["PotenciaMotor"].ToString();
				oObjeto.Color = dr["Color"].ToString();
				oObjeto.NroMotor = dr["NroMotor"].ToString();
				oObjeto.NroChasis = dr["NroChasis"].ToString();
                oObjeto.EsEquipo = SqlImplHelper.getFalseIfNull(dr["esEquipo"].ToString());

                oObjeto.oVehiculoIdentificacion = (new VehiculoIdentificacionImpl()).VehiculoIdentificacionGetByIdVehiculo(oObjeto.IdVehiculo);
				return oObjeto;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		#endregion

        
        public DataTable VehiculoGetByIdSector(int idSector, int idEmpresa)
        {
            
            try
            {
                DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(), "VehiculoGetByIdSector", idSector, idEmpresa).Tables[0];
                if (dt.Rows.Count > 0)
                {
                    return dt;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public DataTable VehiculosGetByAcuerdoCliente(int idEmpresa, int idSector)
        {
            try
            {
                DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(),
                                                        "VehiculosGetByAcuerdoCliente", 
                                                        idEmpresa, 
                                                        idSector).Tables[0];
                if (dt.Rows.Count > 0)
                {
                    return dt;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
