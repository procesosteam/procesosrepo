using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using ProcesoPP.SqlServerLibrary;
using ProcesoPP.Model;

namespace ProcesoPP.SqlServerImpl
{
	public class PersonalConduccionImpl	
	{
		#region PersonalConduccion methods

		public int PersonalConduccionAdd(PersonalConduccion personalconduccion)
		{
			try
			{
				personalconduccion.IdPersonalConduccion= (int)SqlHelper.ExecuteScalar(SqlImplHelper.getConnectionString(), "PersonalConduccionAdd",
														personalconduccion.ManejaEmpresa,
                                                        SqlImplHelper.getNullIfDateMinValue(personalconduccion.VtoCarnet), 
														personalconduccion.Psicofisico,
                                                        SqlImplHelper.getNullIfDateMinValue(personalconduccion.VtoPsicofisico), 
														personalconduccion.CargarGenerales,
                                                        SqlImplHelper.getNullIfDateMinValue(personalconduccion.VtoCargasGenerales), 
														personalconduccion.CargarPeligrosas,
                                                        SqlImplHelper.getNullIfDateMinValue(personalconduccion.VtoCargasPeligrosas), 
														personalconduccion.ManejoDefensivo, 
														SqlImplHelper.getNullIfDateMinValue(personalconduccion.VtoManejoDefensivo), 
														personalconduccion.Empresa, 
														personalconduccion.CertCNRT,
                                                        SqlImplHelper.getCeroIfNull(personalconduccion.IdCategoria),
                                                        SqlImplHelper.getFalseIfNull(personalconduccion.CarnetPsicofisico),
                                                        SqlImplHelper.getNullIfDateMinValue(personalconduccion.VencimientoCarnet));
                SqlImplHelper.commitTransaction();
                return personalconduccion.IdPersonalConduccion;
			}
			catch(Exception ex)
			{
                SqlImplHelper.rollbackTransaction();
				throw ex;
			}
		}

		public bool PersonalConduccionUpdate(PersonalConduccion personalconduccion)
		{
			try
			{
				int update = SqlHelper.ExecuteNonQuery(SqlImplHelper.getConnectionString(), "PersonalConduccionUpdate",
														personalconduccion.IdPersonalConduccion, 
														personalconduccion.ManejaEmpresa,
                                                        SqlImplHelper.getNullIfDateMinValue(personalconduccion.VtoCarnet), 
														personalconduccion.Psicofisico,
                                                        SqlImplHelper.getNullIfDateMinValue(personalconduccion.VtoPsicofisico), 
														personalconduccion.CargarGenerales,
                                                        SqlImplHelper.getNullIfDateMinValue(personalconduccion.VtoCargasGenerales), 
														personalconduccion.CargarPeligrosas,
                                                        SqlImplHelper.getNullIfDateMinValue(personalconduccion.VtoCargasPeligrosas), 
														personalconduccion.ManejoDefensivo,
                                                        SqlImplHelper.getNullIfDateMinValue(personalconduccion.VtoManejoDefensivo), 
														personalconduccion.Empresa,
                                                        personalconduccion.CertCNRT,
                                                        SqlImplHelper.getCeroIfNull(personalconduccion.IdCategoria),
                                                        SqlImplHelper.getFalseIfNull(personalconduccion.CarnetPsicofisico),
                                                        SqlImplHelper.getNullIfDateMinValue(personalconduccion.VencimientoCarnet));
                SqlImplHelper.commitTransaction();
				if (update > 0)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			catch(Exception ex)
			{
                SqlImplHelper.rollbackTransaction();
				throw ex;
			}
		}

		public bool PersonalConduccionDelete(int IdPersonalConduccion)
		{
			try
			{
				int update = SqlHelper.ExecuteNonQuery(SqlImplHelper.getConnectionString(), "PersonalConduccionDelete",
														IdPersonalConduccion);
				if (update > 0)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		public PersonalConduccion PersonalConduccionGetById(int IdPersonalConduccion)
		{
			try
			{
				DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(), "PersonalConduccionGetById",
														IdPersonalConduccion).Tables[0];
				PersonalConduccion NewEnt = new PersonalConduccion();

				if(dt.Rows.Count > 0)
				{
					DataRow dr = dt.Rows[0];
					NewEnt = CargarPersonalConduccion(dr);
				}
				return NewEnt;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		public List<PersonalConduccion> PersonalConduccionGetAll()
		{
			List<PersonalConduccion> lstPersonalConduccion = new List<PersonalConduccion>();
			try
			{
				DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(), "PersonalConduccionGetAll").Tables[0];
				if (dt.Rows.Count > 0)
				{
					for (int i = 0; dt.Rows.Count > i; i++)
					{
						DataRow dr = dt.Rows[i];
						PersonalConduccion NewEnt = new PersonalConduccion();
						NewEnt = CargarPersonalConduccion(dr);
						lstPersonalConduccion.Add(NewEnt);
					}
				}
				return lstPersonalConduccion;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		private PersonalConduccion CargarPersonalConduccion(DataRow dr)
		{
			try
			{
				PersonalConduccion oObjeto = new PersonalConduccion();

				oObjeto.IdPersonalConduccion = int.Parse(dr["IdPersonalConduccion"].ToString());
				oObjeto.ManejaEmpresa = bool.Parse(dr["ManejaEmpresa"].ToString());
				oObjeto.VtoCarnet = SqlImplHelper.getMinValueIfNull(dr["VtoCarnet"].ToString());
				oObjeto.Psicofisico = bool.Parse(dr["Psicofisico"].ToString());
				oObjeto.VtoPsicofisico = SqlImplHelper.getMinValueIfNull( dr["VtoPsicofisico"].ToString());
				oObjeto.CargarGenerales = bool.Parse(dr["CargarGenerales"].ToString());
				oObjeto.VtoCargasGenerales = SqlImplHelper.getMinValueIfNull( dr["VtoCargasGenerales"].ToString());
				oObjeto.CargarPeligrosas = bool.Parse(dr["CargarPeligrosas"].ToString());
				oObjeto.VtoCargasPeligrosas = SqlImplHelper.getMinValueIfNull( dr["VtoCargasPeligrosas"].ToString());
				oObjeto.ManejoDefensivo = bool.Parse(dr["ManejoDefensivo"].ToString());
				oObjeto.VtoManejoDefensivo = SqlImplHelper.getMinValueIfNull( dr["VtoManejoDefensivo"].ToString());
				oObjeto.Empresa = (dr["Empresa"].ToString());
				oObjeto.CertCNRT = (dr["CertCNRT"].ToString());
                oObjeto.IdCategoria = SqlImplHelper.getCeroIfNull(dr["IdCategoria"].ToString());
                oObjeto.CarnetPsicofisico = SqlImplHelper.getFalseIfNull(dr["CarnetPsicofisico"].ToString());
                oObjeto.VencimientoCarnet = SqlImplHelper.getMinValueIfNull(dr["VencimientoCarnet"].ToString());
				
				return oObjeto;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		#endregion

	}
}
