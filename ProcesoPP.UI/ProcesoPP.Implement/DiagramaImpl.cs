

using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using ProcesoPP.SqlServerLibrary;
using ProcesoPP.Model;

namespace ProcesoPP.SqlServerImpl
{
	public class DiagramaImpl	
	{
		#region Diagrama methods

		public int DiagramaAdd(Diagrama diagrama)
		{
			try
			{
				diagrama.IdDiagrama= (int)SqlHelper.ExecuteScalar(SqlImplHelper.getConnectionString(), "DiagramaAdd",
														diagrama.Descripcion, 
														diagrama.Baja);
                SqlImplHelper.commitTransaction();
                return diagrama.IdDiagrama;

			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		public bool DiagramaUpdate(Diagrama diagrama)
		{
			try
			{
				int update = SqlHelper.ExecuteNonQuery(SqlImplHelper.getConnectionString(), "DiagramaUpdate",
														diagrama.IdDiagrama, 
														diagrama.Descripcion, 
														diagrama.Baja);
				if (update > 0)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		public bool DiagramaDelete(int IdDiagrama)
		{
			try
			{
				int update = SqlHelper.ExecuteNonQuery(SqlImplHelper.getConnectionString(), "DiagramaDelete",
														IdDiagrama);
				if (update > 0)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		public Diagrama DiagramaGetById(int IdDiagrama)
		{
			try
			{
				DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(), "DiagramaGetById",
														IdDiagrama).Tables[0];
				Diagrama NewEnt = new Diagrama();

				if(dt.Rows.Count > 0)
				{
					DataRow dr = dt.Rows[0];
					NewEnt = CargarDiagrama(dr);
				}
				return NewEnt;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		public List<Diagrama> DiagramaGetAll()
		{
			List<Diagrama> lstDiagrama = new List<Diagrama>();
			try
			{
				DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(), "DiagramaGetAll").Tables[0];
				if (dt.Rows.Count > 0)
				{
					for (int i = 0; dt.Rows.Count > i; i++)
					{
						DataRow dr = dt.Rows[i];
						Diagrama NewEnt = new Diagrama();
						NewEnt = CargarDiagrama(dr);
						lstDiagrama.Add(NewEnt);
					}
				}
				return lstDiagrama;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		private Diagrama CargarDiagrama(DataRow dr)
		{
			try
			{
				Diagrama oObjeto = new Diagrama();

				oObjeto.IdDiagrama = int.Parse(dr["IdDiagrama"].ToString());
				oObjeto.Descripcion = dr["Descripcion"].ToString();
				oObjeto.Baja = bool.Parse(dr["Baja"].ToString());
				
				return oObjeto;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		public List<Diagrama> DiagramaGetAllBaja()
		{
			List<Diagrama> lstDiagrama = new List<Diagrama>();
			try
			{
				DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(), "DiagramaGetAllBaja").Tables[0];
				if (dt.Rows.Count > 0)
				{
					for (int i = 0; dt.Rows.Count > i; i++)
					{
						Diagrama NewEnt = new Diagrama();
						NewEnt = CargarDiagrama(dt.Rows[i]);
						lstDiagrama.Add(NewEnt);
					}
				}
				return lstDiagrama;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		#endregion

	}
}
