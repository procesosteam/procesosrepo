
using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using ProcesoPP.SqlServerLibrary;
using ProcesoPP.Model;

namespace ProcesoPP.SqlServerImpl
{
	public class PersonalDatosLaboralesImpl	
	{
		#region PersonalDatosLaborales methods

		public int PersonalDatosLaboralesAdd(PersonalDatosLaborales personaldatoslaborales)
		{
			try
			{
				personaldatoslaborales.IdPersonalDatosLaborales= (int)SqlHelper.ExecuteScalar(SqlImplHelper.getConnectionString(), "PersonalDatosLaboralesAdd",
														personaldatoslaborales.IdEmpresa,
                                                        personaldatoslaborales.FechaAlta, 
														personaldatoslaborales.IdRelacion, 
														personaldatoslaborales.IdModalidad, 
														personaldatoslaborales.IdFuncion, 
														personaldatoslaborales.IdSector, 
														personaldatoslaborales.IdDiagrama, 
														personaldatoslaborales.IdBanco,
                                                        personaldatoslaborales.CBU);
                
                return personaldatoslaborales.IdPersonalDatosLaborales;
			}
			catch(Exception ex)
			{
                SqlImplHelper.rollbackTransaction();
				throw ex;
			}
		}

		public bool PersonalDatosLaboralesUpdate(PersonalDatosLaborales personaldatoslaborales)
		{
			try
			{
				int update = SqlHelper.ExecuteNonQuery(SqlImplHelper.getConnectionString(), "PersonalDatosLaboralesUpdate",
														personaldatoslaborales.IdPersonalDatosLaborales, 
														personaldatoslaborales.IdEmpresa,
                                                        personaldatoslaborales.FechaAlta, 
														personaldatoslaborales.IdRelacion, 
														personaldatoslaborales.IdModalidad, 
														personaldatoslaborales.IdFuncion, 
														personaldatoslaborales.IdSector, 
														personaldatoslaborales.IdDiagrama, 
														personaldatoslaborales.IdBanco,
                                                        personaldatoslaborales.CBU);
                
				if (update > 0)
				{
                    
					return true;
				}
				else
				{
					return false;
				}
			}
			catch(Exception ex)
			{
                SqlImplHelper.rollbackTransaction();
				throw ex;
			}
		}

		public bool PersonalDatosLaboralesDelete(int IdPersonalDatosLaborales)
		{
			try
			{
				int update = SqlHelper.ExecuteNonQuery(SqlImplHelper.getConnectionString(), "PersonalDatosLaboralesDelete",
														IdPersonalDatosLaborales);
				if (update > 0)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			catch(Exception ex)
			{
                SqlImplHelper.rollbackTransaction();				throw ex;
			}
		}

		public PersonalDatosLaborales PersonalDatosLaboralesGetById(int IdPersonalDatosLaborales)
		{
			try
			{
				DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(), "PersonalDatosLaboralesGetById",
														IdPersonalDatosLaborales).Tables[0];
				PersonalDatosLaborales NewEnt = new PersonalDatosLaborales();

				if(dt.Rows.Count > 0)
				{
					DataRow dr = dt.Rows[0];
					NewEnt = CargarPersonalDatosLaborales(dr);
				}
				return NewEnt;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		public List<PersonalDatosLaborales> PersonalDatosLaboralesGetAll()
		{
			List<PersonalDatosLaborales> lstPersonalDatosLaborales = new List<PersonalDatosLaborales>();
			try
			{
				DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(), "PersonalDatosLaboralesGetAll").Tables[0];
				if (dt.Rows.Count > 0)
				{
					for (int i = 0; dt.Rows.Count > i; i++)
					{
						DataRow dr = dt.Rows[i];
						PersonalDatosLaborales NewEnt = new PersonalDatosLaborales();
						NewEnt = CargarPersonalDatosLaborales(dr);
						lstPersonalDatosLaborales.Add(NewEnt);
					}
				}
				return lstPersonalDatosLaborales;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		private PersonalDatosLaborales CargarPersonalDatosLaborales(DataRow dr)
		{
			try
			{
				PersonalDatosLaborales oObjeto = new PersonalDatosLaborales();

				oObjeto.IdPersonalDatosLaborales = int.Parse(dr["IdPersonalDatosLaborales"].ToString());
				oObjeto.IdEmpresa = int.Parse(dr["IdEmpresa"].ToString());
                oObjeto.FechaAlta = SqlImplHelper.getMinValueIfNull(dr["FechaAlta"].ToString());
				oObjeto.IdRelacion = int.Parse(dr["IdRelacion"].ToString());
				oObjeto.IdModalidad = int.Parse(dr["IdModalidad"].ToString());
				oObjeto.IdFuncion = int.Parse(dr["IdFuncion"].ToString());
				oObjeto.IdSector = int.Parse(dr["IdSector"].ToString());
				oObjeto.IdDiagrama = int.Parse(dr["IdDiagrama"].ToString());
				oObjeto.IdBanco = int.Parse(dr["IdBanco"].ToString());
                oObjeto.CBU = dr["cbu"].ToString();
				
				return oObjeto;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		#endregion

	}
}
