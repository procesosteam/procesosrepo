

using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using ProcesoPP.SqlServerLibrary;
using ProcesoPP.Model;

namespace ProcesoPP.SqlServerImpl
{
	public class TransporteImpl	
	{
		#region Transporte methods

		public int TransporteAdd(Transporte transporte)
		{
			try
			{
				return (int)SqlHelper.ExecuteScalar(SqlImplHelper.getConnectionString(), "TransporteAdd",
														transporte.Descripcion, 
														transporte.Marca, 
														transporte.Modelo, 
														transporte.Anio, 
														transporte.ItemVerificacion, 
														transporte.Patente);
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		public bool TransporteUpdate(Transporte transporte)
		{
			try
			{
				int update = SqlHelper.ExecuteNonQuery(SqlImplHelper.getConnectionString(), "TransporteUpdate",
														transporte.IdTransporte, 
														transporte.Descripcion, 
														transporte.Marca, 
														transporte.Modelo, 
														transporte.Anio, 
														transporte.ItemVerificacion, 
														transporte.Patente);
				if (update > 0)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		public bool TransporteDelete(int IdTransporte)
		{
			try
			{
				int update = SqlHelper.ExecuteNonQuery(SqlImplHelper.getConnectionString(), "TransporteDelete",
														IdTransporte);
				if (update > 0)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		public Transporte TransporteGetById(int IdTransporte)
		{
			try
			{
				DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(), "TransporteGetById",
														IdTransporte).Tables[0];
				Transporte NewEnt = new Transporte();

				if(dt.Rows.Count > 0)
				{
					DataRow dr = dt.Rows[0];
					NewEnt.IdTransporte = Int32.Parse(dr["IdTransporte"].ToString());
					NewEnt.Descripcion = dr["Descripcion"].ToString();
					NewEnt.Marca = dr["Marca"].ToString();
					NewEnt.Modelo = dr["Modelo"].ToString();
					NewEnt.Anio = Int32.Parse(dr["Anio"].ToString());
					NewEnt.ItemVerificacion = dr["ItemVerificacion"].ToString();
					NewEnt.Patente = dr["Patente"].ToString();
				}
				return NewEnt;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		public List<Transporte> TransporteGetAll()
		{
			List<Transporte> lstTransporte = new List<Transporte>();
			try
			{
				DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(), "TransporteGetAll").Tables[0];
				if (dt.Rows.Count > 0)
				{
					int colIdTransporte =  dt.Columns["IdTransporte"].Ordinal;
					int colDescripcion =  dt.Columns["Descripcion"].Ordinal;
					int colMarca =  dt.Columns["Marca"].Ordinal;
					int colModelo =  dt.Columns["Modelo"].Ordinal;
					int colAnio =  dt.Columns["Anio"].Ordinal;
					int colItemVerificacion =  dt.Columns["ItemVerificacion"].Ordinal;
					int colPatente =  dt.Columns["Patente"].Ordinal;
					for (int i = 0; dt.Rows.Count > i; i++)
					{
						Transporte NewEnt = new Transporte();
						NewEnt.IdTransporte = Int32.Parse(dt.Rows[i].ItemArray[colIdTransporte].ToString());
						NewEnt.Descripcion = dt.Rows[i].ItemArray[colDescripcion].ToString();
						NewEnt.Marca = dt.Rows[i].ItemArray[colMarca].ToString();
						NewEnt.Modelo = dt.Rows[i].ItemArray[colModelo].ToString();
						NewEnt.Anio = Int32.Parse(dt.Rows[i].ItemArray[colAnio].ToString());
						NewEnt.ItemVerificacion = dt.Rows[i].ItemArray[colItemVerificacion].ToString();
						NewEnt.Patente = dt.Rows[i].ItemArray[colPatente].ToString();
						lstTransporte.Add(NewEnt);
					}
				}
				return lstTransporte;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		#endregion

	}
}
