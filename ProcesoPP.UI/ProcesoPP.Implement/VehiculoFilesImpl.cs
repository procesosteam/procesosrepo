using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using ProcesoPP.SqlServerLibrary;
using ProcesoPP.Model;

namespace ProcesoPP.SqlServerImpl
{
	public class VehiculoFilesImpl	
	{
		#region VehiculoFiles methods

		public int VehiculoFilesAdd(VehiculoFiles vehiculofiles)
		{
			try
			{
				vehiculofiles.IdVehiculoFiles= (int)SqlHelper.ExecuteScalar(SqlImplHelper.getConnectionString(), "VehiculoFilesAdd",
														vehiculofiles.IdVehiculo, 
														vehiculofiles.Nombre, 
														vehiculofiles.Ruta);
                SqlImplHelper.commitTransaction();
                return vehiculofiles.IdVehiculoFiles;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		public bool VehiculoFilesUpdate(VehiculoFiles vehiculofiles)
		{
			try
			{
				int update = SqlHelper.ExecuteNonQuery(SqlImplHelper.getConnectionString(), "VehiculoFilesUpdate",
														vehiculofiles.IdVehiculoFiles, 
														vehiculofiles.IdVehiculo, 
														vehiculofiles.Nombre, 
														vehiculofiles.Ruta);
                SqlImplHelper.commitTransaction();
				if (update > 0)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		public bool VehiculoFilesDelete(int IdVehiculoFiles)
		{
			try
			{
				int update = SqlHelper.ExecuteNonQuery(SqlImplHelper.getConnectionString(), "VehiculoFilesDelete",
														IdVehiculoFiles);
				if (update > 0)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		public VehiculoFiles VehiculoFilesGetById(int IdVehiculoFiles)
		{
			try
			{
				DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(), "VehiculoFilesGetById",
														IdVehiculoFiles).Tables[0];
				VehiculoFiles NewEnt = new VehiculoFiles();

				if(dt.Rows.Count > 0)
				{
					DataRow dr = dt.Rows[0];
					NewEnt = CargarVehiculoFiles(dr);
				}
				return NewEnt;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

        public List<VehiculoFiles> VehiculoFilesGetByIdVehiculo(int IdVehiculo)
        {
            List<VehiculoFiles> lstVehiculoFiles = new List<VehiculoFiles>();
            try
            {
                DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(), "VehiculoFilesGetByIdVehiculo", IdVehiculo).Tables[0];
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; dt.Rows.Count > i; i++)
                    {
                        DataRow dr = dt.Rows[i];
                        VehiculoFiles NewEnt = new VehiculoFiles();
                        NewEnt = CargarVehiculoFiles(dr);
                        lstVehiculoFiles.Add(NewEnt);
                    }
                }
                return lstVehiculoFiles;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

		public List<VehiculoFiles> VehiculoFilesGetAll()
		{
			List<VehiculoFiles> lstVehiculoFiles = new List<VehiculoFiles>();
			try
			{
				DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(), "VehiculoFilesGetAll").Tables[0];
				if (dt.Rows.Count > 0)
				{
					for (int i = 0; dt.Rows.Count > i; i++)
					{
						DataRow dr = dt.Rows[i];
						VehiculoFiles NewEnt = new VehiculoFiles();
						NewEnt = CargarVehiculoFiles(dr);
						lstVehiculoFiles.Add(NewEnt);
					}
				}
				return lstVehiculoFiles;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		private VehiculoFiles CargarVehiculoFiles(DataRow dr)
		{
			try
			{
				VehiculoFiles oObjeto = new VehiculoFiles();

				oObjeto.IdVehiculoFiles = int.Parse(dr["IdVehiculoFiles"].ToString());
				oObjeto.IdVehiculo = int.Parse(dr["IdVehiculo"].ToString());
				oObjeto.Nombre = (dr["Nombre"].ToString());
				oObjeto.Ruta = (dr["Ruta"].ToString());
				
				return oObjeto;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		#endregion

    }
}
