﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ProcesoPP.Model;
using System.Data;
using ProcesoPP.SqlServerLibrary;

namespace ProcesoPP.SqlServerImpl
{
    public class RemitosImpl
    {

        public DataTable RemitosGetByFilter(string nroSolicitud, string idEmpresa, string desde, string hasta, string nroParte, string idEquipo, string idSector, string idTipoServicio, string idCondicion, string nroRemito, string chkRem)
        {
            try
            {
                List<Remitos> lstRemitos = new List<Remitos>();
                DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(),
                                                        "RemitosGetByFilter",
                                                        SqlImplHelper.getNullIfEmpty(nroSolicitud),
                                                        SqlImplHelper.getNullIfCero(idEmpresa),
                                                        SqlImplHelper.getNullIfEmpty(desde),
                                                        SqlImplHelper.getNullIfEmpty(hasta),
                                                        SqlImplHelper.getNullIfEmpty(nroParte),
                                                        SqlImplHelper.getNullIfCero(idEquipo),
                                                        SqlImplHelper.getNullIfCero(idSector),
                                                        SqlImplHelper.getNullIfCero(idTipoServicio),
                                                        SqlImplHelper.getNullIfCero(idCondicion),
                                                        SqlImplHelper.getNullIfEmpty(nroRemito),
                                                        SqlImplHelper.getNullIfEmpty(chkRem)).Tables[0];
			
				return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private Remitos CargarRemito(DataRow dataRow)
        {
            Remitos NewEnt = new Remitos();
            ParteEntregaImpl oParte = new ParteEntregaImpl();
            RemitoDetalleImpl oRemitoDetalleImpl = new RemitoDetalleImpl();

            NewEnt.IdRemito = Int32.Parse(dataRow["idRemito"].ToString());
            NewEnt.IdUsuario = int.Parse(dataRow["idUsuario"].ToString());
            NewEnt.Anulado = bool.Parse(dataRow["anulado"].ToString());
            NewEnt.Cantidad = int.Parse(dataRow["cantidad"].ToString());
            NewEnt.Fecha = DateTime.Parse(dataRow["fecha"].ToString());
            NewEnt.Impreso = bool.Parse(dataRow["impreso"].ToString());
            NewEnt.NroRemito = int.Parse(dataRow["nroRemito"].ToString());
            NewEnt.Observacion = dataRow["observacion"].ToString();
            NewEnt.oParte = oParte.ParteEntregaGetById(int.Parse(dataRow["idParte"].ToString()));
            NewEnt.oListDetalles = oRemitoDetalleImpl.RemitoDetalleAddGetByRemito(NewEnt.IdRemito);
            NewEnt.NroOC = dataRow["NroOC"].ToString();
            NewEnt.CCosto = dataRow["CCosto"].ToString();
            return NewEnt;
        }

        public Remitos RemitoGetByIdParte(int idParte)
        {
            try
            {
                DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(), 
                                                        "RemitoGetByIdParte",
                                                        idParte).Tables[0];
                Remitos NewEnt = new Remitos();

                if (dt.Rows.Count > 0)
                {
                    DataRow dr = dt.Rows[0];
                    NewEnt = CargarRemito(dr);
                }
                return NewEnt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int RemitosAdd(Remitos remitos, List<RemitoDetalleTango> oListTango)
        {
            try
            {
                int idRemito = (int)SqlHelper.ExecuteScalar(SqlImplHelper.getTransaction(), "RemitosAdd",
                                                        remitos.NroRemito,
                                                        remitos.Fecha,
                                                        remitos.oParte.IdParteEntrega,
                                                        remitos.Cantidad,
                                                        remitos.Observacion,
                                                        remitos.Anulado,
                                                        remitos.Impreso,
                                                        remitos.IdUsuario,
                                                        remitos.CCosto,
                                                        remitos.NroOC);
              
                RemitoDetalleImpl oRemitoDetalleImpl = new RemitoDetalleImpl();                
                foreach (RemitoDetalle oDet in remitos.oListDetalles)
                {
                    oDet.IdRemito = idRemito;
                    oRemitoDetalleImpl.RemitoDetalleAdd(oDet);
                }
                SqlImplHelper.commitTransaction();

                if (oListTango != null && remitos.NroRemito > 0)
                {
                    if (oListTango != null && oListTango.Count > 0)
                        RemitosTantoAdd(remitos, oListTango);
                }
                return idRemito;
            }
            catch (Exception ex)
            {
                SqlImplHelper.rollbackTransaction();                
                throw ex;
            }
        }

        private void RemitosTantoAdd(Remitos remitos, List<RemitoDetalleTango> oListTango)
        {
            try
            {
                string N_Comp = System.Configuration.ConfigurationManager.AppSettings["N_Comp"].ToString();
                string TComp = System.Configuration.ConfigurationManager.AppSettings["TComp"].ToString();
                string TCompInS = System.Configuration.ConfigurationManager.AppSettings["TCompInS"].ToString();
                string Talonario = System.Configuration.ConfigurationManager.AppSettings["Talonario"].ToString();
                string user = System.Configuration.ConfigurationManager.AppSettings["UsuarioTango"].ToString();

                N_Comp = N_Comp + remitos.NroRemito.ToString().PadLeft(8,'0');
                ParteEntrega oParte = (new ParteEntregaImpl()).ParteEntregaGetById(remitos.oListDetalles[0].IdParte);

                int idRemito = (int)SqlHelper.ExecuteScalar(SqlImplHelper.getTransactionTango(),
                                                           "RemitosTangoAdd",
                                                           "",                              //FILLER
                                                           (oParte != null && oParte.IdParteEntrega > 0) ? oParte.oOrdenTrabajo.oSolicitudCliente.oSector.oEmpresa.CodTango : "", //COD_PRO_CL	
                                                           remitos.Cantidad,                //COTIZ
                                                           "P",                             //ESTADO_MOV
                                                           0,                               //EXPORTADO
                                                           0,                               //EXP_STOCK
                                                           SqlImplHelper.getMinValueTango(remitos.FechaAnulado),    //FECHA_ANU
                                                           remitos.Fecha,                   //FECHA_MOV
                                                           DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + DateTime.Now.Second.ToString(),   //HORA
                                                           "",                              //ID_CARPETA
                                                           0,                               //LISTA_REM
                                                           0,                               //LOTE
                                                           0,                               //LOTE_ANU
                                                           1,                               //MON_CTE
                                                           "",                              //MOTIVO_REM
                                                           N_Comp,                          //N_COMP
                                                           N_Comp,                          //N_REMITO
                                                           remitos.NroRemito.ToString().PadLeft(8, '0'),               //NCOMP_IN_S
                                                           "",                              //NCOMP_ORIG
                                                           0,                               //NRO_SUCURS
                                                           remitos.Observacion,             //OBSERVACION
                                                           0,                               //SUC_ORIG
                                                           TComp,                           //T_COMP
                                                           Talonario,                       //TALONARIO
                                                           TCompInS,                        //TCOMP_IN_S
                                                           "",                              //TCOMP_ORIG
                                                           user,                            //USUARIO
                                                           1,                               //COD_TRANSP
                                                           "",                              //,@HORA_COMP		varchar(6) 
                                                           0,                               //,@ID_A_RENTA	ENTEROXL_TG 
                                                           0,                               //,@DOC_ELECTR	bit 
                                                           "",                              //,@COD_CLASIF	varchar(6) 
                                                           "",                              //,@AUDIT_IMP		varchar(60) 
                                                           0,                               //,@IMP_IVA		DECIMAL_TG 
                                                           0,                               //,@IMP_OTIMP		DECIMAL_TG 
                                                           0,                               //,@IMPORTE_BO	DECIMAL_TG 
                                                           0,                               //,@IMPORTE_TO	DECIMAL_TG 
                                                           "N",                             //,@DIFERENCIA	varchar(1) 
                                                           0,                               //,@SUC_DESTIN	ENTERO_TG 
                                                           "",                              //,@T_DOC_DTE		varchar(3) 
                                                           remitos.Leyenda1,                //,@LEYENDA1		varchar(60) 
                                                           remitos.Leyenda2,                //,@LEYENDA2		varchar(60) 
                                                           remitos.Leyenda3,                //,@LEYENDA3		varchar(60) 
                                                           remitos.Leyenda4,                //,@LEYENDA4		varchar(60) 
                                                           "",                              //,@LEYENDA5		varchar(60) 
                                                           0,                               //,@DCTO_CLIEN	DECIMAL_TG 
                                                           "",                              //,@T_INT_ORI		varchar(2) 
                                                           "",                              //,@N_INT_ORI		varchar(8) 
                                                           DateTime.Now,                    //,@FECHA_INGRESO datetime 
                                                           DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + DateTime.Now.Second.ToString(), //,@HORA_INGRESO	varchar(6) 
                                                           System.Environment.UserName,     //,@USUARIO_INGRESO				varchar(120) 
                                                           System.Environment.MachineName,  //,@TERMINAL_INGRESO				varchar(255) 
                                                           0,                               //,@IMPORTE_TOTAL_CON_IMPUESTOS	DECIMAL_TG 
                                                           0,                               //,@CANTIDAD_KILOS				DECIMAL_TG 
                                                           1                                //,@ID_DIRECCION_ENTREGA			D_ID 
                                                           );

                RemitoDetalleImpl oRemitoDetalleImpl = new RemitoDetalleImpl();
                
                foreach (RemitoDetalleTango oDet in oListTango)
                {                    
                    oRemitoDetalleImpl.RemitoDetalleTangoAdd(oDet, TComp, TCompInS, Talonario, N_Comp);
                }
                SqlImplHelper.commitTransactionTango();
            }
            catch (Exception ex)
            {
                SqlImplHelper.rollbackTransactionTango();
                ex.Source = "Remitos Tango Add";
                throw ex;
            }
        }

        public bool RemitosUpdate(Remitos remitos)
        {
            try
            {
                int idRemtio = SqlHelper.ExecuteNonQuery(SqlImplHelper.getTransaction(), "RemitosUpdate",
                                                        remitos.IdRemito,
                                                        remitos.NroRemito,
                                                        remitos.Fecha,
                                                        remitos.oParte.IdParteEntrega,
                                                        remitos.Cantidad,
                                                        remitos.Observacion,
                                                        remitos.Anulado,
                                                        remitos.Impreso,
                                                        remitos.IdUsuario,
                                                        remitos.CCosto,
                                                        remitos.NroOC);

                RemitoDetalleImpl oRemitoDetalleImpl = new RemitoDetalleImpl();
                List<RemitoDetalle> olistDet = oRemitoDetalleImpl.RemitoDetalleAddGetByRemito(remitos.IdRemito);
                foreach (RemitoDetalle oDet in remitos.oListDetalles)
                {
                    oDet.IdRemito = remitos.IdRemito;
                    if (oDet.IdRemitoDetalle != 0)
                        oRemitoDetalleImpl.RemitoDetalleUpdate(oDet);
                    else
                        oRemitoDetalleImpl.RemitoDetalleAdd(oDet);
                    if (olistDet.Exists(d => d.IdRemitoDetalle == oDet.IdRemitoDetalle))
                        olistDet.Remove(olistDet.Find(d=> d.IdRemitoDetalle == oDet.IdRemitoDetalle));
                }
                foreach (RemitoDetalle oDet in olistDet)
                {
                    oRemitoDetalleImpl.RemitoDetalleDelete(oDet.IdRemitoDetalle);
                }

                SqlImplHelper.commitTransaction();


                if (idRemtio > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool RemitosDelete(int IdRemito)
        {
            try
            {
                int update = SqlHelper.ExecuteNonQuery(SqlImplHelper.getConnectionString(), "RemitosDelete",
                                                        IdRemito);
                if (update > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Remitos RemitosGetById(int IdRemito)
        {
            try
            {
                DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(), "RemitosGetById",
                                                        IdRemito).Tables[0];
                Remitos NewEnt = new Remitos();

                if (dt.Rows.Count > 0)
                {
                    DataRow dr = dt.Rows[0];
                    NewEnt = CargarRemito(dr);
                }
                return NewEnt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Remitos> RemitosGetAll()
        {
            List<Remitos> lstRemitos = new List<Remitos>();
            try
            {
                DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(), "RemitosGetAll").Tables[0];
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; dt.Rows.Count > i; i++)
                    {
                        Remitos NewEnt = new Remitos();
                        NewEnt = CargarRemito(dt.Rows[i]);
                        lstRemitos.Add(NewEnt);
                    }
                }
                return lstRemitos;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int RemitoGetNroRemito()
        {
            try
            {
                return (int)SqlHelper.ExecuteScalar(SqlImplHelper.getConnectionString(), 
                                                    "RemitoGetNroRemito");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DateTime RemitosGetUltimaFecha()
        {
            try
            {
                DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(),
                                                    "RemitosGetUltimaFecha").Tables[0];

                if (dt.Rows.Count > 0)
                    return DateTime.Parse(dt.Rows[0]["FECHA"].ToString());
                else
                    return DateTime.MinValue;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable RemitosAnuladosGetByFilter(string nroSolicitud, string idEmpresa, string desde, string hasta, string nroParte, string idEquipo, string idSector, string idTipoServicio, string idCondicion)
        {

            try
            {
               
                DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(),
                                                        "RemitosAnuladosGetByFilter",
                                                        SqlImplHelper.getNullIfEmpty(nroSolicitud),
                                                        SqlImplHelper.getNullIfCero(idEmpresa),
                                                        SqlImplHelper.getNullIfEmpty(desde),
                                                        SqlImplHelper.getNullIfEmpty(hasta),
                                                        SqlImplHelper.getNullIfEmpty(nroParte),
                                                        SqlImplHelper.getNullIfCero(idEquipo),
                                                        SqlImplHelper.getNullIfCero(idSector),
                                                        SqlImplHelper.getNullIfCero(idTipoServicio),
                                                        SqlImplHelper.getNullIfCero(idCondicion)).Tables[0];

                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable RemitoGetRPT(int _idRemito)
        {
            try
            {
                DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(),
                                                        "RemitoGetRPT",
                                                       _idRemito).Tables[0];

                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool RemitoNumeroUpdate(int nroRemito)
        {
            try
            {
                int countRemtio = SqlHelper.ExecuteNonQuery(SqlImplHelper.getConnectionString(), 
                                                        "RemitoNumeroUpdate",
                                                        nroRemito);

                
                if (countRemtio > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool RemitoBonificado(ParteEntrega oParte)
        {
            try
            {
                Remitos oRemito = new Remitos();
                oRemito.Fecha = DateTime.Today;
                oRemito.Observacion = "REMITO BONIFICADO";
                oRemito.NroRemito = 0;
                oRemito.oListDetalles = CargarDetalle(oRemito.oListDetalles, oParte);
                oRemito.Impreso = true;
                oRemito.oParte = oParte;

                if (RemitosAdd(oRemito, null) > 0)
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
        }

        private List<RemitoDetalle> CargarDetalle(List<RemitoDetalle> list, ParteEntrega oParte)
        {
            DataTable dtAcuerdo = (new AcuerdoImpl()).AcuerdoGetByCliente(oParte.oOrdenTrabajo.oSolicitudCliente);
            string det = oParte.oOrdenTrabajo.oSolicitudCliente.oTipoServicio.Descripcion + " ";
            int cant = 1;
            cant = (new SolicitudClienteImpl()).SolicitudClienteGetFechaRegreso(oParte.oOrdenTrabajo.oSolicitudCliente.NroSolicitud,
                                                                               oParte.oOrdenTrabajo.oSolicitudCliente.IdSolicitud,
                                                                               oParte.oOrdenTrabajo.oSolicitudCliente.Fecha);
            if (oParte.oOrdenTrabajo.oSolicitudCliente.oMovimiento != null)
            {
                det += "\nDesde: " + oParte.oOrdenTrabajo.oSolicitudCliente.oMovimiento.oLugarOrigen.Descripcion + " " +
                                            oParte.oOrdenTrabajo.oSolicitudCliente.oMovimiento.PozoOrigen;
                det += "\nHasta: " + oParte.oOrdenTrabajo.oSolicitudCliente.oMovimiento.oLugarDestino.Descripcion + " " +
                                            oParte.oOrdenTrabajo.oSolicitudCliente.oMovimiento.PozoDestino;

                cant = oParte.oOrdenTrabajo.oSolicitudCliente.oMovimiento.KmsEstimados;
            }
            else if (oParte.oOrdenTrabajo.oSolicitudCliente.oListEspecificaciones.Count > 0)
            {
                oParte.oOrdenTrabajo.oSolicitudCliente.oListEspecificaciones.ForEach(e => det += e.Descripcion + " ");
            }
            else if (oParte.oOrdenTrabajo.oVehiculo != null)
            {
                if (oParte.oOrdenTrabajo.oSolicitudCliente.oCondicion != null)
                    det += " Condicion: " + oParte.oOrdenTrabajo.oSolicitudCliente.oCondicion.Descripcion;
                det += " Equipo: " + oParte.oOrdenTrabajo.oVehiculo.oVehiculoIdentificacion.oCodificacion.Codigo + ' ' + oParte.oOrdenTrabajo.oVehiculo.oVehiculoIdentificacion.NroCodificacion;
            }

            List<DataRow> drDetalle = dtAcuerdo.Select("idTipoServicio=" + oParte.oOrdenTrabajo.oSolicitudCliente.IdTipoServicio.ToString()).ToList();

            decimal pu = 0, total = 0;
            if (oParte.oOrdenTrabajo.oSolicitudCliente.IdCondicion == (int)Condicion.id.diario)
            {
                DataRow drs = drDetalle.Find(d => d["Concepto"].ToString().Contains("Día"));
                if (drs != null)
                    pu = decimal.Parse(drs["Costo"].ToString());
                if ((cant / 15) > 1)
                {
                    cant = 15 + (int)(Math.Truncate((double)(cant / 15)));
                }
            }
            else
            {
                DataRow drs = drDetalle.Find(d => d["Concepto"].ToString().Contains("Mes") && d["Concepto"].ToString().Contains("Costo"));
                if (drs != null)
                    pu = decimal.Parse(drs["Costo"].ToString());
                total = pu;
                //cant = 1;
            }
            if (drDetalle.Exists(d => d["Concepto"].ToString().Contains("Vehiculo")))
            {
                DataRow drs = drDetalle.FindAll(d => d["Concepto"].ToString().Contains("Vehiculo")).Last();
                pu = decimal.Parse(drs["Costo"].ToString());
                det += "\n" + drs["Concepto"].ToString();
            }
            total = (pu * cant);

            det += "\nRealizado " + oParte.oOrdenTrabajo.oSolicitudCliente.Fecha.ToString("dd/MM/yyyy");
            string ss = "SS: " + oParte.oOrdenTrabajo.oSolicitudCliente.NroPedidoCliente + "\r\n" + "PO: " + oParte.NroParteEntrega;

            RemitoDetalle oDetalle = new RemitoDetalle();
            oDetalle.Cantidad = cant;
            oDetalle.Detalle = det;
            oDetalle.IdParte = oParte.IdParteEntrega;
            oDetalle.PrecioTotal = total;
            oDetalle.PrecioUnit = pu;

            list.Add(oDetalle);
            return list;

        }

        public DataTable RemitosBonificadosGetByFilter(string nroSolicitud, string idEmpresa, string desde, string hasta, string nroParte, string idEquipo, string idSector, string idTipoServicio, string idCondicion, string nroRemito)
        {
            try
            {
                List<Remitos> lstRemitos = new List<Remitos>();
                DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(),
                                                        "RemitosBonificadosGetByFilter",
                                                        SqlImplHelper.getNullIfEmpty(nroSolicitud),
                                                        SqlImplHelper.getNullIfCero(idEmpresa),
                                                        SqlImplHelper.getNullIfEmpty(desde),
                                                        SqlImplHelper.getNullIfEmpty(hasta),
                                                        SqlImplHelper.getNullIfEmpty(nroParte),
                                                        SqlImplHelper.getNullIfCero(idEquipo),
                                                        SqlImplHelper.getNullIfCero(idSector),
                                                        SqlImplHelper.getNullIfCero(idTipoServicio),
                                                        SqlImplHelper.getNullIfCero(idCondicion),
                                                        SqlImplHelper.getNullIfEmpty(nroRemito)).Tables[0];

                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable RemitosHistorialGetByFilter(string nroSolicitud, string idEmpresa, string desde, string hasta, string nroParte, string idEquipo, string idSector, string idTipoServicio, string idCondicion, string nroRemito, string chkRem)
        {
            try
            {
                List<Remitos> lstRemitos = new List<Remitos>();
                DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(),
                                                        "RemitosHistorialGetByFilter",
                                                        SqlImplHelper.getNullIfEmpty(nroSolicitud),
                                                        SqlImplHelper.getNullIfCero(idEmpresa),
                                                        SqlImplHelper.getNullIfEmpty(desde),
                                                        SqlImplHelper.getNullIfEmpty(hasta),
                                                        SqlImplHelper.getNullIfEmpty(nroParte),
                                                        SqlImplHelper.getNullIfCero(idEquipo),
                                                        SqlImplHelper.getNullIfCero(idSector),
                                                        SqlImplHelper.getNullIfCero(idTipoServicio),
                                                        SqlImplHelper.getNullIfCero(idCondicion),
                                                        SqlImplHelper.getNullIfEmpty(nroRemito),
                                                        SqlImplHelper.getNullIfEmpty(chkRem)).Tables[0];

                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
