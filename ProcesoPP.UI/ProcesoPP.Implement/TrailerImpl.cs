
using System;
using System.Data;
using System.Linq;
using System.Data.SqlClient;
using System.Collections.Generic;
using ProcesoPP.SqlServerLibrary;
using ProcesoPP.Model;
using System.ComponentModel;

namespace ProcesoPP.SqlServerImpl
{
	public class TrailerImpl	
	{
		
        public DataTable TrailerGetDisponibles(List<EspecificacionTecnica> oEspecificaciones, int idTipoServicio)
        {
            try
            {
               
                DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(), 
                                                        "TrailerGetDisponibles",
                                                        getIDS(oEspecificaciones),
                                                        idTipoServicio).Tables[0];
                if (dt.Rows.Count > 0)
                { 
                    return dt;
                }
                else
                    return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private string getIDS(List<EspecificacionTecnica> oEspecificaciones)
        {
            string ids = "", coma = "";
            foreach (EspecificacionTecnica oET in oEspecificaciones)
            {
                ids += coma + oET.IdEspecificaciones.ToString();
                coma = ",";
            }

            return ids;
        }

        public int TrailerGetSolicitudAnterior(int nroSolicitud)
        {
            try
            {
                int res = (int)SqlHelper.ExecuteScalar(SqlImplHelper.getConnectionString(), "TrailerGetSolicitudAnterior",
                                                                                         nroSolicitud);
               
                return res;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static DataTable ListToDataTable<T>(IList<T> data)
        {
            DataTable table = new DataTable("table");

            //special handling for value types and string
            if (typeof(T).IsValueType || typeof(T).Equals(typeof(string)))
            {

                DataColumn dc = new DataColumn("Value");

                table.Columns.Add(dc);

                foreach (T item in data)
                {
                    DataRow dr = table.NewRow();
                    dr[0] = item;

                    table.Rows.Add(dr);
                }
            }
            else
            {
                PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(typeof(T));

                foreach (PropertyDescriptor prop in properties)
                {
                    table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
                }

                foreach (T item in data)
                {
                    DataRow row = table.NewRow();
                    foreach (PropertyDescriptor prop in properties)
                    {
                        try
                        {
                            row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
                        }
                        catch (Exception ex)
                        {
                            row[prop.Name] = DBNull.Value;
                        }

                    }
                    table.Rows.Add(row);
                }
            }
            return table;

        }

        public DataTable TrailerGetNoDisponibles(List<EspecificacionTecnica> oEspecificaciones, int nroSolicitud)
        {            
            try
            {

                DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(),
                                                        "TrailerGetNoDisponibles",
                                                        getIDS(oEspecificaciones),
                                                        SqlImplHelper.getNullIfCero(nroSolicitud.ToString())).Tables[0];
                if (dt.Rows.Count > 0)
                {                   
                    return dt;
                }
                else
                    return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Vehiculo> SolicitudTrailerPosibleGet(int nroSolicitud, int idTipoServicio)
        {
            try
            {
                List<Vehiculo> oList = new List<Vehiculo>();
                VehiculoImpl oVE = new VehiculoImpl();

                DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(),
                                                        "SolicitudTrailerPosibleGet",
                                                        nroSolicitud,
                                                        idTipoServicio).Tables[0];
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; dt.Rows.Count > i; i++)
                    {
                        DataRow dr = dt.Rows[i];
                        Vehiculo NewEnt = new Vehiculo();
                        NewEnt = oVE.CargarVehiculo(dr);
                        oList.Add(NewEnt);
                    }

                }

                return oList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
