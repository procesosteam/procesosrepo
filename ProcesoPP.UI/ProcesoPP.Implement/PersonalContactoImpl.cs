
using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using ProcesoPP.SqlServerLibrary;
using ProcesoPP.Model;

namespace ProcesoPP.SqlServerImpl
{
	public class PersonalContactoImpl	
	{
		#region PersonalContacto methods

		public int PersonalContactoAdd(PersonalContacto personalcontacto)
		{
			try
			{
				personalcontacto.IdPersonalContacto= (int)SqlHelper.ExecuteScalar(SqlImplHelper.getConnectionString(), "PersonalContactoAdd",
														personalcontacto.Apellido, 
														personalcontacto.Nombre, 
														personalcontacto.Relacion, 
														personalcontacto.Telefono);
                SqlImplHelper.commitTransaction();
                return personalcontacto.IdPersonalContacto;
			}
			catch(Exception ex)
			{
                SqlImplHelper.rollbackTransaction();
				throw ex;
			}
		}

		public bool PersonalContactoUpdate(PersonalContacto personalcontacto)
		{
			try
			{
				int update = SqlHelper.ExecuteNonQuery(SqlImplHelper.getConnectionString(), "PersonalContactoUpdate",
														personalcontacto.IdPersonalContacto, 
														personalcontacto.Apellido, 
														personalcontacto.Nombre, 
														personalcontacto.Relacion, 
														personalcontacto.Telefono);
				if (update > 0)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			catch(Exception ex)
			{
                SqlImplHelper.rollbackTransaction();
				throw ex;
			}
		}

		public bool PersonalContactoDelete(int IdPersonalContacto)
		{
			try
			{
				int update = SqlHelper.ExecuteNonQuery(SqlImplHelper.getConnectionString(), "PersonalContactoDelete",
														IdPersonalContacto);
				if (update > 0)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			catch(Exception ex)
			{
                SqlImplHelper.rollbackTransaction();
				throw ex;
			}
		}

		public PersonalContacto PersonalContactoGetById(int IdPersonalContacto)
		{
			try
			{
				DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(), "PersonalContactoGetById",
														IdPersonalContacto).Tables[0];
				PersonalContacto NewEnt = new PersonalContacto();

				if(dt.Rows.Count > 0)
				{
					DataRow dr = dt.Rows[0];
					NewEnt = CargarPersonalContacto(dr);
				}
				return NewEnt;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		public List<PersonalContacto> PersonalContactoGetAll()
		{
			List<PersonalContacto> lstPersonalContacto = new List<PersonalContacto>();
			try
			{
				DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(), "PersonalContactoGetAll").Tables[0];
				if (dt.Rows.Count > 0)
				{
					for (int i = 0; dt.Rows.Count > i; i++)
					{
						DataRow dr = dt.Rows[i];
						PersonalContacto NewEnt = new PersonalContacto();
						NewEnt = CargarPersonalContacto(dr);
						lstPersonalContacto.Add(NewEnt);
					}
				}
				return lstPersonalContacto;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		private PersonalContacto CargarPersonalContacto(DataRow dr)
		{
			try
			{
				PersonalContacto oObjeto = new PersonalContacto();

				oObjeto.IdPersonalContacto = int.Parse(dr["IdPersonalContacto"].ToString());
				oObjeto.Apellido = (dr["Apellido"].ToString());
				oObjeto.Nombre = (dr["Nombre"].ToString());
				oObjeto.Relacion = (dr["Relacion"].ToString());
				oObjeto.Telefono = (dr["Telefono"].ToString());
				
				return oObjeto;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		#endregion

	}
}
