﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ProcesoPP.SqlServerLibrary;
using ProcesoPP.Model;

namespace ProcesoPP.SqlServerImpl
{
    public class SolicitudEspecificacionImpl
    {
        public int SolicitudEspecificacionAdd(SolicitudEspecificacion oSsEsp)
        {
            try
            {
                return (int)SqlHelper.ExecuteNonQuery(SqlImplHelper.getTransaction(), "SolicitudEspecificacionAdd",
                                                            oSsEsp.idEspecificacion,
                                                            oSsEsp.idSolicitud);
            }
            catch (Exception ex)
            {
                SqlImplHelper.rollbackTransaction();
                throw ex;
            }
        }

        public int SolicitudEspecificacionDelete(int idSolicitud)
        {
            try
            {
                return (int)SqlHelper.ExecuteNonQuery(SqlImplHelper.getTransaction(),
                                                    "SolicitudEspecificacionDelete",
                                                      idSolicitud);
            }
            catch (Exception ex)
            {
                SqlImplHelper.rollbackTransaction();
                throw ex;
            }
        }

    }
}
