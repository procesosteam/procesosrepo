
using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using ProcesoPP.SqlServerLibrary;
using ProcesoPP.Model;

namespace ProcesoPP.SqlServerImpl
{
	public class PersonalFilesImpl	
	{
		#region PersonalFiles methods

		public int PersonalFilesAdd(PersonalFiles personalfiles)
		{
			try
			{
				personalfiles.IdPersonalFiles= (int)SqlHelper.ExecuteScalar(SqlImplHelper.getConnectionString(), "PersonalFilesAdd",
														personalfiles.IdPersonal, 
														personalfiles.Nombre, 
														personalfiles.Ruta);
                SqlImplHelper.commitTransaction();
                return personalfiles.IdPersonalFiles;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		public bool PersonalFilesUpdate(PersonalFiles personalfiles)
		{
			try
			{
				int update = SqlHelper.ExecuteNonQuery(SqlImplHelper.getConnectionString(), "PersonalFilesUpdate",
														personalfiles.IdPersonalFiles, 
														personalfiles.IdPersonal, 
														personalfiles.Nombre, 
														personalfiles.Ruta);
				if (update > 0)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		public bool PersonalFilesDelete(int IdPersonalFiles)
		{
			try
			{
				int update = SqlHelper.ExecuteNonQuery(SqlImplHelper.getConnectionString(), "PersonalFilesDelete",
														IdPersonalFiles);
				if (update > 0)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		public PersonalFiles PersonalFilesGetById(int IdPersonalFiles)
		{
			try
			{
				DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(), "PersonalFilesGetById",
														IdPersonalFiles).Tables[0];
				PersonalFiles NewEnt = new PersonalFiles();

				if(dt.Rows.Count > 0)
				{
					DataRow dr = dt.Rows[0];
					NewEnt = CargarPersonalFiles(dr);
				}
				return NewEnt;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		public List<PersonalFiles> PersonalFilesGetAll()
		{
			List<PersonalFiles> lstPersonalFiles = new List<PersonalFiles>();
			try
			{
				DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(), "PersonalFilesGetAll").Tables[0];
				if (dt.Rows.Count > 0)
				{
					for (int i = 0; dt.Rows.Count > i; i++)
					{
						DataRow dr = dt.Rows[i];
						PersonalFiles NewEnt = new PersonalFiles();
						NewEnt = CargarPersonalFiles(dr);
						lstPersonalFiles.Add(NewEnt);
					}
				}
				return lstPersonalFiles;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

        public List<PersonalFiles> PersonalFilesGetByFilter(int idPersonal, string nombre, int longuitud)
        {
            List<PersonalFiles> lstPersonalFiles = new List<PersonalFiles>();
            try
            {
                DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(), "GetPersonalFilesByFilter",nombre,idPersonal,longuitud).Tables[0];
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; dt.Rows.Count > i; i++)
                    {
                        DataRow dr = dt.Rows[i];
                        PersonalFiles NewEnt = new PersonalFiles();
                        NewEnt = CargarPersonalFiles(dr);
                        lstPersonalFiles.Add(NewEnt);
                    }
                }
                return lstPersonalFiles;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

		private PersonalFiles CargarPersonalFiles(DataRow dr)
		{
			try
			{
				PersonalFiles oObjeto = new PersonalFiles();

				oObjeto.IdPersonalFiles = int.Parse(dr["IdPersonalFiles"].ToString());
				oObjeto.IdPersonal = int.Parse(dr["IdPersonal"].ToString());
				oObjeto.Nombre = (dr["Nombre"].ToString());
				oObjeto.Ruta = (dr["Ruta"].ToString());
				
				return oObjeto;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

        public List<PersonalFiles> getPersonalFilesByIdPersonal(int idPersonal)
        {
            List<PersonalFiles> lstPersonalFiles = new List<PersonalFiles>();
            try
            {
                DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(), "PersonalFilesGetByIdPersonal", idPersonal).Tables[0];
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; dt.Rows.Count > i; i++)
                    {
                        DataRow dr = dt.Rows[i];
                        PersonalFiles NewEnt = new PersonalFiles();
                        NewEnt = CargarPersonalFiles(dr);
                        lstPersonalFiles.Add(NewEnt);
                    }
                }
                return lstPersonalFiles;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public int getIdFilesByIdPersonal(int idPersonal)
        {
            try
            {
                return int.Parse(SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(), "getIdFilesByIdPersonal",
                                                        idPersonal).Tables[0].ToString() );
            }
            catch (Exception e)
            {
                throw e;
            }
        }

		#endregion

	}
}
