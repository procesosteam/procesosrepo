
using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using ProcesoPP.SqlServerLibrary;
using ProcesoPP.Model;

namespace ProcesoPP.SqlServerImpl
{
	public class CondicionImpl	
	{
		#region Condicion methods

		public int CondicionAdd(Condicion condicion)
		{
			try
			{
				return (int)SqlHelper.ExecuteScalar(SqlImplHelper.getConnectionString(), "CondicionAdd",
														condicion.Descripcion);
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		public bool CondicionUpdate(Condicion condicion)
		{
			try
			{
				int update = SqlHelper.ExecuteNonQuery(SqlImplHelper.getConnectionString(), "CondicionUpdate",
														condicion.IdCondicion, 
														condicion.Descripcion);
				if (update > 0)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		public bool CondicionDelete(int IdCondicion)
		{
			try
			{
				int update = SqlHelper.ExecuteNonQuery(SqlImplHelper.getConnectionString(), "CondicionDelete",
														IdCondicion);
				if (update > 0)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		public Condicion CondicionGetById(int IdCondicion)
		{
			try
			{
				DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(), "CondicionGetById",
														IdCondicion).Tables[0];
				Condicion NewEnt = new Condicion();

				if(dt.Rows.Count > 0)
				{
					DataRow dr = dt.Rows[0];
					NewEnt.IdCondicion = Int32.Parse(dr["IdCondicion"].ToString());
					NewEnt.Descripcion = dr["Descripcion"].ToString();
				}
				return NewEnt;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		public List<Condicion> CondicionGetAll()
		{
			List<Condicion> lstCondicion = new List<Condicion>();
			try
			{
				DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(), "CondicionGetAll").Tables[0];
				if (dt.Rows.Count > 0)
				{
					int colIdCondicion =  dt.Columns["IdCondicion"].Ordinal;
					int colDescripcion =  dt.Columns["Descripcion"].Ordinal;
					for (int i = 0; dt.Rows.Count > i; i++)
					{
						Condicion NewEnt = new Condicion();
						NewEnt.IdCondicion = Int32.Parse(dt.Rows[i].ItemArray[colIdCondicion].ToString());
						NewEnt.Descripcion = dt.Rows[i].ItemArray[colDescripcion].ToString();
						lstCondicion.Add(NewEnt);
					}
				}
				return lstCondicion;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		#endregion

	}
}
