
using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using ProcesoPP.SqlServerLibrary;
using ProcesoPP.Model;

namespace ProcesoPP.SqlServerImpl
{
	public class PersonalInformacionMedicaImpl	
	{
		#region PersonalInformacionMedica methods

		public int PersonalInformacionMedicaAdd(PersonalInformacionMedica personalinformacionmedica)
		{
			try
			{
				personalinformacionmedica.IdPersonalInformacionMedica= (int)SqlHelper.ExecuteScalar(SqlImplHelper.getConnectionString(), "PersonalInformacionMedicaAdd",
														personalinformacionmedica.GrupoSanguineo, 
														personalinformacionmedica.Factor, 
														personalinformacionmedica.Alergico, 
														personalinformacionmedica.Observacion, 
														personalinformacionmedica.HapatitisB, 
														personalinformacionmedica.Antitetanica, 
														personalinformacionmedica.ExPreocupacional, 
														SqlImplHelper.getNullIfDateMinValue(personalinformacionmedica.FechaPreocupacional), 
														personalinformacionmedica.AprobacionPreocupacional, 
														personalinformacionmedica.ExPeriodico,
                                                        SqlImplHelper.getNullIfDateMinValue(personalinformacionmedica.FechaPeriodico), 
														personalinformacionmedica.AprobacionPeriodico);
                
                return personalinformacionmedica.IdPersonalInformacionMedica;
			}
			catch(Exception ex)
			{
                SqlImplHelper.rollbackTransaction();
				throw ex;
			}
		}

		public bool PersonalInformacionMedicaUpdate(PersonalInformacionMedica personalinformacionmedica)
		{
			try
			{
				int update = SqlHelper.ExecuteNonQuery(SqlImplHelper.getConnectionString(), "PersonalInformacionMedicaUpdate",
														personalinformacionmedica.IdPersonalInformacionMedica, 
														personalinformacionmedica.GrupoSanguineo, 
														personalinformacionmedica.Factor, 
														personalinformacionmedica.Alergico, 
														personalinformacionmedica.Observacion, 
														personalinformacionmedica.HapatitisB, 
														personalinformacionmedica.Antitetanica, 
														personalinformacionmedica.ExPreocupacional,
                                                        SqlImplHelper.getNullIfDateMinValue(personalinformacionmedica.FechaPreocupacional), 
														personalinformacionmedica.AprobacionPreocupacional, 
														personalinformacionmedica.ExPeriodico,
                                                        SqlImplHelper.getNullIfDateMinValue(personalinformacionmedica.FechaPeriodico), 
														personalinformacionmedica.AprobacionPeriodico);
                
				if (update > 0)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			catch(Exception ex)
			{
                SqlImplHelper.rollbackTransaction();
				throw ex;
			}
		}

		public bool PersonalInformacionMedicaDelete(int IdPersonalInformacionMedica)
		{
			try
			{
				int update = SqlHelper.ExecuteNonQuery(SqlImplHelper.getConnectionString(), "PersonalInformacionMedicaDelete",
														IdPersonalInformacionMedica);
				if (update > 0)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		public PersonalInformacionMedica PersonalInformacionMedicaGetById(int IdPersonalInformacionMedica)
		{
			try
			{
				DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(), "PersonalInformacionMedicaGetById",
														IdPersonalInformacionMedica).Tables[0];
				PersonalInformacionMedica NewEnt = new PersonalInformacionMedica();

				if(dt.Rows.Count > 0)
				{
					DataRow dr = dt.Rows[0];
					NewEnt = CargarPersonalInformacionMedica(dr);
				}
				return NewEnt;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		public List<PersonalInformacionMedica> PersonalInformacionMedicaGetAll()
		{
			List<PersonalInformacionMedica> lstPersonalInformacionMedica = new List<PersonalInformacionMedica>();
			try
			{
				DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(), "PersonalInformacionMedicaGetAll").Tables[0];
				if (dt.Rows.Count > 0)
				{
					for (int i = 0; dt.Rows.Count > i; i++)
					{
						DataRow dr = dt.Rows[i];
						PersonalInformacionMedica NewEnt = new PersonalInformacionMedica();
						NewEnt = CargarPersonalInformacionMedica(dr);
						lstPersonalInformacionMedica.Add(NewEnt);
					}
				}
				return lstPersonalInformacionMedica;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		private PersonalInformacionMedica CargarPersonalInformacionMedica(DataRow dr)
		{
			try
			{
				PersonalInformacionMedica oObjeto = new PersonalInformacionMedica();

				oObjeto.IdPersonalInformacionMedica = int.Parse(dr["IdPersonalInformacionMedica"].ToString());
				oObjeto.GrupoSanguineo = (dr["GrupoSanguineo"].ToString());
				oObjeto.Factor = (dr["Factor"].ToString());
				oObjeto.Alergico = (dr["Alergico"].ToString());
				oObjeto.Observacion = (dr["Observacion"].ToString());
				oObjeto.HapatitisB = bool.Parse(dr["HapatitisB"].ToString());
				oObjeto.Antitetanica = bool.Parse(dr["Antitetanica"].ToString());
				oObjeto.ExPreocupacional = bool.Parse(dr["ExPreocupacional"].ToString());
				oObjeto.FechaPreocupacional = SqlImplHelper.getMinValueIfNull(dr["FechaPreocupacional"].ToString());
				oObjeto.AprobacionPreocupacional = bool.Parse(dr["AprobacionPreocupacional"].ToString());
				oObjeto.ExPeriodico = bool.Parse(dr["ExPeriodico"].ToString());
                oObjeto.FechaPeriodico = SqlImplHelper.getMinValueIfNull(dr["FechaPeriodico"].ToString());
				oObjeto.AprobacionPeriodico = bool.Parse(dr["AprobacionPeriodico"].ToString());
				
				return oObjeto;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		#endregion

	}
}
