using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using ProcesoPP.SqlServerLibrary;
using ProcesoPP.Model;

namespace ProcesoPP.SqlServerImpl
{
	public class VehiculoSeguroImpl	
	{
		#region VehiculoSeguro methods

		public int VehiculoSeguroAdd(VehiculoSeguro vehiculoseguro)
		{
			try
			{
				vehiculoseguro.IdSeguro= (int)SqlHelper.ExecuteScalar(SqlImplHelper.getConnectionString(), "VehiculoSeguroAdd",
														vehiculoseguro.IdVehiculo, 
														SqlImplHelper.getFalseIfNull(vehiculoseguro.CompaniaAseg), 
														vehiculoseguro.NroPoliza, 
														SqlImplHelper.getNullIfDateMinValue(vehiculoseguro.VigenciaDesde), 
														SqlImplHelper.getNullIfDateMinValue(vehiculoseguro.VigenciaHasta));
                SqlImplHelper.commitTransaction();
                return vehiculoseguro.IdSeguro;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		public bool VehiculoSeguroUpdate(VehiculoSeguro vehiculoseguro)
		{
			try
			{
				int update = SqlHelper.ExecuteNonQuery(SqlImplHelper.getConnectionString(), "VehiculoSeguroUpdate",
														vehiculoseguro.IdSeguro, 
														vehiculoseguro.IdVehiculo,
                                                        SqlImplHelper.getFalseIfNull(vehiculoseguro.CompaniaAseg), 
														vehiculoseguro.NroPoliza,
                                                        SqlImplHelper.getNullIfDateMinValue(vehiculoseguro.VigenciaDesde), 
														SqlImplHelper.getNullIfDateMinValue(vehiculoseguro.VigenciaHasta));
                SqlImplHelper.commitTransaction();
				if (update > 0)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		public bool VehiculoSeguroDelete(int IdSeguro)
		{
			try
			{
				int update = SqlHelper.ExecuteNonQuery(SqlImplHelper.getConnectionString(), "VehiculoSeguroDelete",
														IdSeguro);
				if (update > 0)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		public VehiculoSeguro VehiculoSeguroGetById(int IdSeguro)
		{
			try
			{
				DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(), "VehiculoSeguroGetById",
														IdSeguro).Tables[0];
				VehiculoSeguro NewEnt = new VehiculoSeguro();

				if(dt.Rows.Count > 0)
				{
					DataRow dr = dt.Rows[0];
					NewEnt = CargarVehiculoSeguro(dr);
				}
				return NewEnt;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

        public VehiculoSeguro VehiculoSeguroGetByIdVehiculo(int IdVehiculo)
        {
            try
            {
                DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(), "VehiculoSeguroGetByIdVehiculo",
                                                        IdVehiculo).Tables[0];
                VehiculoSeguro NewEnt = new VehiculoSeguro();

                if (dt.Rows.Count > 0)
                {
                    DataRow dr = dt.Rows[0];
                    NewEnt = CargarVehiculoSeguro(dr);
                }
                return NewEnt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

		public List<VehiculoSeguro> VehiculoSeguroGetAll()
		{
			List<VehiculoSeguro> lstVehiculoSeguro = new List<VehiculoSeguro>();
			try
			{
				DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(), "VehiculoSeguroGetAll").Tables[0];
				if (dt.Rows.Count > 0)
				{
					for (int i = 0; dt.Rows.Count > i; i++)
					{
						DataRow dr = dt.Rows[i];
						VehiculoSeguro NewEnt = new VehiculoSeguro();
						NewEnt = CargarVehiculoSeguro(dr);
						lstVehiculoSeguro.Add(NewEnt);
					}
				}
				return lstVehiculoSeguro;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		private VehiculoSeguro CargarVehiculoSeguro(DataRow dr)
		{
			try
			{
				VehiculoSeguro oObjeto = new VehiculoSeguro();

				oObjeto.IdSeguro = Int32.Parse(dr["IdSeguro"].ToString());
				oObjeto.IdVehiculo = Int32.Parse(dr["IdVehiculo"].ToString());
				oObjeto.CompaniaAseg = SqlImplHelper.getFalseIfNull(dr["CompaniaAseg"].ToString());
				oObjeto.NroPoliza = dr["NroPoliza"].ToString();
				oObjeto.VigenciaDesde = SqlImplHelper.getMinValueIfNull(dr["VigenciaDesde"].ToString());
				oObjeto.VigenciaHasta = SqlImplHelper.getMinValueIfNull(dr["VigenciaHasta"].ToString());
				
				return oObjeto;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		#endregion


        
    }
}
