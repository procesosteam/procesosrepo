﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ProcesoPP.SqlServerLibrary;
using ProcesoPP.Model;
using System.Data;

namespace ProcesoPP.SqlServerImpl
{
   public class TipoServicioEspecificacionImpl
    {
        public int TipoServicioEspecificacionAdd(TipoServicioEspecificacion otsEsp)
        {
            try
            {
                return (int)SqlHelper.ExecuteScalar(SqlImplHelper.getTransaction(), "TipoServicioEspecificacionAdd",
                                                            otsEsp.IdEspecificacion,
                                                            otsEsp.IdTipoServicio);
            }
            catch (Exception ex)
            {
                SqlImplHelper.rollbackTransaction();
                throw ex;
            }
        }



        public bool TipoServicioEspecificacionUpdate(TipoServicioEspecificacion oTipoServicioEspecificacion)
        {
            try
            {
                int result = (int)SqlHelper.ExecuteNonQuery(SqlImplHelper.getTransaction(), 
                                                            "TipoServicioEspecificacionUpdate",
                                                            oTipoServicioEspecificacion.IdEspecificacion,
                                                            oTipoServicioEspecificacion.IdTipoServicio,
                                                            oTipoServicioEspecificacion.IdTipoEspecificacion);



                if (result > 0)
                    return true;
                else
                    return false;

            }
            catch (Exception ex)
            {
                SqlImplHelper.rollbackTransaction();
                throw ex;
            }
        }

        public List<TipoServicioEspecificacion>TipoServicioEspecificacionGetByIdTipoServicio(int IdTipoServicio)
        {
            try
            {
                DataSet ds = SqlHelper.ExecuteDataset(SqlImplHelper.getTransaction(),
                                                            "TipoServicioEspecificacionGetByIdTipoServicio",
                                                            IdTipoServicio);

                List<TipoServicioEspecificacion> oLista = new List<TipoServicioEspecificacion>();
                if (ds.Tables.Count > 0)
                {
                    DataTable dt = ds.Tables[0];
                    if (dt.Rows.Count > 0)
                    {
                        DataRow dr = dt.Rows[0];
                        TipoServicioEspecificacion oTipoServicioEspecificacion = new TipoServicioEspecificacion();
                        oTipoServicioEspecificacion.IdEspecificacion = int.Parse(dr["IdEspecificacion"].ToString());
                        oTipoServicioEspecificacion.IdTipoServicio = int.Parse(dr["IdTipoServicio"].ToString());
                        oTipoServicioEspecificacion.IdTipoEspecificacion = int.Parse(dr["IdTipoEspecificacion"].ToString());

                        oLista.Add(oTipoServicioEspecificacion);
                    }
                }

                return oLista;
            }
            catch (Exception ex)
            {
                SqlImplHelper.rollbackTransaction();
                throw ex;
            }
        }

        public bool TipoServicioEspecificacionDeleteByIdTipoServicio(int IdTipoServicio)
        {
            try
            {
                int result = (int)SqlHelper.ExecuteNonQuery(SqlImplHelper.getTransaction(),
                                                            "TipoServicioEspecificacionDeleteByIdTipoServicio",
                                                            IdTipoServicio);

                if (result > 0)
                    return true;
                else
                    return false;

            }
            catch (Exception ex)
            {
                SqlImplHelper.rollbackTransaction();
                throw ex;
            }
        }
    }
}
