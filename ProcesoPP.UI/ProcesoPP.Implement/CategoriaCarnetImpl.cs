
using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using ProcesoPP.SqlServerLibrary;
using ProcesoPP.Model;

namespace ProcesoPP.SqlServerImpl
{
	public class CategoriaCarnetImpl	
	{
		#region CategoriaCarnet methods

		public int CategoriaCarnetAdd(CategoriaCarnet categoriacarnet)
		{
			try
			{
				return (int)SqlHelper.ExecuteScalar(SqlImplHelper.getConnectionString(), "CategoriaCarnetAdd",
														categoriacarnet.IdCarnet, 
														categoriacarnet.Descripcion);
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		public bool CategoriaCarnetUpdate(CategoriaCarnet categoriacarnet)
		{
			try
			{
				int update = SqlHelper.ExecuteNonQuery(SqlImplHelper.getConnectionString(), "CategoriaCarnetUpdate",
														categoriacarnet.IdCarnet, 
														categoriacarnet.Descripcion);
				if (update > 0)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		public bool CategoriaCarnetDelete(int IdCarnet)
		{
			try
			{
				int update = SqlHelper.ExecuteNonQuery(SqlImplHelper.getConnectionString(), "CategoriaCarnetDelete",
														IdCarnet);
				if (update > 0)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		public CategoriaCarnet CategoriaCarnetGetById(int IdCarnet)
		{
			try
			{
				DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(), "CategoriaCarnetGetById",
														IdCarnet).Tables[0];
				CategoriaCarnet NewEnt = new CategoriaCarnet();

				if(dt.Rows.Count > 0)
				{
					DataRow dr = dt.Rows[0];
					NewEnt = CargarCategoriaCarnet(dr);
				}
				return NewEnt;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		public List<CategoriaCarnet> CategoriaCarnetGetAll()
		{
			List<CategoriaCarnet> lstCategoriaCarnet = new List<CategoriaCarnet>();
			try
			{
				DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(), "CategoriaCarnetGetAll").Tables[0];
				if (dt.Rows.Count > 0)
				{
					for (int i = 0; dt.Rows.Count > i; i++)
					{
						DataRow dr = dt.Rows[i];
						CategoriaCarnet NewEnt = new CategoriaCarnet();
						NewEnt = CargarCategoriaCarnet(dr);
						lstCategoriaCarnet.Add(NewEnt);
					}
				}
				return lstCategoriaCarnet;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		private CategoriaCarnet CargarCategoriaCarnet(DataRow dr)
		{
			try
			{
				CategoriaCarnet oObjeto = new CategoriaCarnet();

				oObjeto.IdCarnet = int.Parse(dr["IdCarnet"].ToString());
				oObjeto.Descripcion = (dr["Descripcion"].ToString());
				
				return oObjeto;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		#endregion

	}
}
