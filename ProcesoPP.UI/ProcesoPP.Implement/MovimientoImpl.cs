
using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using ProcesoPP.SqlServerLibrary;
using ProcesoPP.Model;
using ProcesoPP.SqlServerImpl;

namespace ProcesoPP.SqlServerImpl
{
	public class MovimientoImpl	
	{
		#region Movimiento methods

		public int MovimientoAdd(Movimiento movimiento)
		{
			try
			{
                KilometrosImpl oKilometrosImpl = new KilometrosImpl();
                List<Kilometros> oListKMS = oKilometrosImpl.KilometrosGetByFilter(movimiento.oLugarOrigen.IdLugar.ToString(), movimiento.oLugarDestino.IdLugar.ToString());
                if (!oListKMS.Exists(km=> km.Kms == movimiento.KmsEstimados))
                {
                    Kilometros oKms = new Kilometros();
                    oKms.oLugarDesde.IdLugar = movimiento.oLugarOrigen.IdLugar;
                    oKms.oLugarHasta.IdLugar = movimiento.oLugarDestino.IdLugar;
                    oKms.Kms = movimiento.KmsEstimados;
                    oKilometrosImpl.KilometrosAdd(oKms);
                }

                return (int)SqlHelper.ExecuteScalar(SqlImplHelper.getTransaction(), "MovimientoAdd",
                                                        movimiento.oLugarOrigen.IdLugar,
                                                        movimiento.oLugarDestino.IdLugar,
                                                        movimiento.PozoOrigen,
                                                        movimiento.PozoDestino,
                                                        movimiento.KmsEstimados,
                                                        SqlImplHelper.getNullIfDateMinValue(movimiento.FechaEstimada));
			}
			catch(Exception ex)
			{
                SqlImplHelper.rollbackTransaction();
				throw ex;
			}
		}

		public bool MovimientoUpdate(Movimiento movimiento)
		{
			try
			{
				int update = SqlHelper.ExecuteNonQuery(SqlImplHelper.getTransaction(), "MovimientoUpdate",
                                                        movimiento.IdMovimiento,
                                                        movimiento.oLugarOrigen.IdLugar,
                                                        movimiento.oLugarDestino.IdLugar,														 
														movimiento.PozoOrigen, 
														movimiento.PozoDestino, 														
														movimiento.KmsEstimados, 
													    SqlImplHelper.getNullIfDateMinValue(movimiento.FechaEstimada));
				if (update > 0)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			catch(Exception ex)
			{
                SqlImplHelper.rollbackTransaction();
				throw ex;
			}
		}

		public bool MovimientoDelete(int IdMovimiento)
		{
			try
			{
				int update = SqlHelper.ExecuteNonQuery(SqlImplHelper.getConnectionString(), "MovimientoDelete",
														IdMovimiento);
				if (update > 0)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		public Movimiento MovimientoGetById(int IdMovimiento)
		{
			try
			{
				DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(), "MovimientoGetById",
														IdMovimiento).Tables[0];
                Movimiento NewEnt = new Movimiento();

				if(dt.Rows.Count > 0)
				{
					DataRow dr = dt.Rows[0];
                    NewEnt = LlenarMovimiento(dr);
				}
				return NewEnt;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

        private Movimiento LlenarMovimiento(DataRow dr)
        {
            Movimiento NewEnt = new Movimiento();
            LugarImpl oLugarImpl = new LugarImpl();

            NewEnt.IdMovimiento = Int32.Parse(dr["IdMovimiento"].ToString());
            NewEnt.oLugarDestino = oLugarImpl.LugarGetById(int.Parse(dr["IdLugarDestino"].ToString()));
            NewEnt.oLugarOrigen = oLugarImpl.LugarGetById(int.Parse(dr["IdLugarOrigen"].ToString()));
            NewEnt.PozoOrigen = dr["pozoOrigen"].ToString();
            NewEnt.PozoDestino = dr["pozoDestino"].ToString();
            NewEnt.KmsEstimados = (int)decimal.Parse(dr["KmsEstimados"].ToString());
            NewEnt.FechaEstimada = SqlImplHelper.getMinValueIfNull(dr["FechaEstimada"]);

            return NewEnt;
        }

		public List<Movimiento> MovimientoGetAll()
		{
			List<Movimiento> lstMovimiento = new List<Movimiento>();
			try
			{
				DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(), "MovimientoGetAll").Tables[0];
				if (dt.Rows.Count > 0)
				{
					int colIdMovimiento =  dt.Columns["IdMovimiento"].Ordinal;
					int colOrigen =  dt.Columns["Origen"].Ordinal;
					int colDestino =  dt.Columns["Destino"].Ordinal;
					int colPozo =  dt.Columns["Pozo"].Ordinal;
					int colKmsEstimados =  dt.Columns["KmsEstimados"].Ordinal;
					int colFechaEstimada =  dt.Columns["FechaEstimada"].Ordinal;
					for (int i = 0; dt.Rows.Count > i; i++)
					{
						Movimiento NewEnt = new Movimiento();
						NewEnt.IdMovimiento = Int32.Parse(dt.Rows[i].ItemArray[colIdMovimiento].ToString());
						NewEnt.PozoOrigen = dt.Rows[i].ItemArray[colOrigen].ToString();
						NewEnt.PozoDestino = dt.Rows[i].ItemArray[colDestino].ToString();
                        NewEnt.KmsEstimados = (int)decimal.Parse(dt.Rows[i]["KmsEstimados"].ToString());
						NewEnt.FechaEstimada = DateTime.Parse(dt.Rows[i].ItemArray[colFechaEstimada].ToString());
						lstMovimiento.Add(NewEnt);
					}
				}
				return lstMovimiento;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		#endregion

	}
}
