

using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using ProcesoPP.Model;
using ProcesoPP.SqlServerLibrary;

namespace ProcesoPP.SqlServerImpl
{
	public class ChoferImpl	
	{
		#region Chofer methods

		public int ChoferAdd(Chofer chofer)
		{
			try
			{
				return (int)SqlHelper.ExecuteScalar(SqlImplHelper.getConnectionString(), "ChoferAdd",
														chofer.Nombre, 
														chofer.Apellido, 
														chofer.Direccion, 
														chofer.Telefono, 
														chofer.DNI);
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		public bool ChoferUpdate(Chofer chofer)
		{
			try
			{
				int update = SqlHelper.ExecuteNonQuery(SqlImplHelper.getConnectionString(), "ChoferUpdate",
														chofer.IdChofer, 
														chofer.Nombre, 
														chofer.Apellido, 
														chofer.Direccion, 
														chofer.Telefono, 
														chofer.DNI);
				if (update > 0)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		public bool ChoferDelete(int IdChofer)
		{
			try
			{
				int update = SqlHelper.ExecuteNonQuery(SqlImplHelper.getConnectionString(), "ChoferDelete",
														IdChofer);
				if (update > 0)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		public Chofer ChoferGetById(int IdChofer)
		{
			try
			{
				DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(), "ChoferGetById",
														IdChofer).Tables[0];
				Chofer NewEnt = new Chofer();

				if(dt.Rows.Count > 0)
				{
					DataRow dr = dt.Rows[0];
                    NewEnt.IdChofer = int.Parse(dr["IdChofer"].ToString());
					NewEnt.Nombre = dr["Nombre"].ToString();
					NewEnt.Apellido = dr["Apellido"].ToString();
					NewEnt.Direccion = dr["Direccion"].ToString();
					NewEnt.Telefono = dr["Telefono"].ToString();
					NewEnt.DNI = dr["DNI"].ToString();
				}
				return NewEnt;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		public List<Chofer> ChoferGetAll()
		{
			List<Chofer> lstChofer = new List<Chofer>();
			try
			{
				DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(), "ChoferGetAll").Tables[0];
				if (dt.Rows.Count > 0)
				{
					int colIdChofer =  dt.Columns["IdChofer"].Ordinal;
					int colNombre =  dt.Columns["Nombre"].Ordinal;
					int colApellido =  dt.Columns["Apellido"].Ordinal;
					int colDireccion =  dt.Columns["Direccion"].Ordinal;
					int colTelefono =  dt.Columns["Telefono"].Ordinal;
					int colDNI =  dt.Columns["DNI"].Ordinal;
					for (int i = 0; dt.Rows.Count > i; i++)
					{
						Chofer NewEnt = new Chofer();
                        NewEnt.IdChofer = int.Parse(dt.Rows[i].ItemArray[colIdChofer].ToString());
						NewEnt.Nombre = (dt.Rows[i].ItemArray[colNombre].ToString());
						NewEnt.Apellido = (dt.Rows[i].ItemArray[colApellido].ToString());
						NewEnt.Direccion = (dt.Rows[i].ItemArray[colDireccion].ToString());
						NewEnt.Telefono = (dt.Rows[i].ItemArray[colTelefono].ToString());
						NewEnt.DNI = (dt.Rows[i].ItemArray[colDNI].ToString());
						lstChofer.Add(NewEnt);
					}
				}
				return lstChofer;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		#endregion
        
        public List<Chofer> ChoferGetAllCombo()
        {
            List<Chofer> lstChofer = new List<Chofer>();
            try
            {
                DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(), "ChoferGetAll").Tables[0];
                if (dt.Rows.Count > 0)
                {
                    int colIdChofer = dt.Columns["IdChofer"].Ordinal;
                    int colNombre = dt.Columns["Nombre"].Ordinal;
                    int colApellido = dt.Columns["Apellido"].Ordinal;
                    int colDireccion = dt.Columns["Direccion"].Ordinal;
                    int colTelefono = dt.Columns["Telefono"].Ordinal;
                    int colDNI = dt.Columns["DNI"].Ordinal;
                    for (int i = 0; dt.Rows.Count > i; i++)
                    {
                        Chofer NewEnt = new Chofer();
                        NewEnt.IdChofer = int.Parse(dt.Rows[i].ItemArray[colIdChofer].ToString());
                        NewEnt.Nombre = (dt.Rows[i].ItemArray[colApellido].ToString()) + ", " + (dt.Rows[i].ItemArray[colNombre].ToString());
                        NewEnt.Apellido = (dt.Rows[i].ItemArray[colApellido].ToString());
                        NewEnt.Direccion = (dt.Rows[i].ItemArray[colDireccion].ToString());
                        NewEnt.Telefono = (dt.Rows[i].ItemArray[colTelefono].ToString());
                        NewEnt.DNI = (dt.Rows[i].ItemArray[colDNI].ToString());
                        lstChofer.Add(NewEnt);
                    }
                }
                return lstChofer;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
