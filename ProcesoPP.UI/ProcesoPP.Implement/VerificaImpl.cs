

using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using ProcesoPP.SqlServerLibrary;
using ProcesoPP.Model;

namespace ProcesoPP.SqlServerImpl
{
	public class VerificaImpl	
	{
		#region Verifica methods

		public int VerificaAdd(Verifica verifica)
		{
			try
			{
				return (int)SqlHelper.ExecuteScalar(SqlImplHelper.getConnectionString(), "VerificaAdd",
														verifica.Descripcion);
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		public bool VerificaUpdate(Verifica verifica)
		{
			try
			{
				int update = SqlHelper.ExecuteNonQuery(SqlImplHelper.getConnectionString(), "VerificaUpdate",
														verifica.IdVerifica, 
														verifica.Descripcion);
				if (update > 0)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		public bool VerificaDelete(int IdVerifica)
		{
			try
			{
				int update = SqlHelper.ExecuteNonQuery(SqlImplHelper.getConnectionString(), "VerificaDelete",
														IdVerifica);
				if (update > 0)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		public Verifica VerificaGetById(int IdVerifica)
		{
			try
			{
				DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(), "VerificaGetById",
														IdVerifica).Tables[0];
				Verifica NewEnt = new Verifica();

				if(dt.Rows.Count > 0)
				{
					DataRow dr = dt.Rows[0];
					NewEnt.IdVerifica = int.Parse(dr["IdVerifica"].ToString());
					NewEnt.Descripcion = dr["Descripcion"].ToString();
				}
				return NewEnt;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		public List<Verifica> VerificaGetAll()
		{
			List<Verifica> lstVerifica = new List<Verifica>();
			try
			{
				DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(), "VerificaGetAll").Tables[0];
				if (dt.Rows.Count > 0)
				{
					int colIdVerifica =  dt.Columns["IdVerifica"].Ordinal;
					int colDescripcion =  dt.Columns["Descripcion"].Ordinal;
					for (int i = 0; dt.Rows.Count > i; i++)
					{
						Verifica NewEnt = new Verifica();
						NewEnt.IdVerifica = int.Parse(dt.Rows[i].ItemArray[colIdVerifica].ToString());
						NewEnt.Descripcion = dt.Rows[i].ItemArray[colDescripcion].ToString();
						lstVerifica.Add(NewEnt);
					}
				}
				return lstVerifica;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		#endregion

	}
}
