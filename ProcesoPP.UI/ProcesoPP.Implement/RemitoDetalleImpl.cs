
using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using ProcesoPP.SqlServerLibrary;
using ProcesoPP.Model;

namespace ProcesoPP.SqlServerImpl
{
	public class RemitoDetalleImpl	
	{
		#region RemitoDetalle methods

		public int RemitoDetalleAdd(RemitoDetalle remitodetalle)
		{
			try
			{
				return (int)SqlHelper.ExecuteScalar(SqlImplHelper.getTransaction(), "RemitoDetalleAdd",
														remitodetalle.IdRemito, 
														remitodetalle.Cantidad, 
														remitodetalle.Detalle, 
														remitodetalle.PrecioUnit, 
														remitodetalle.PrecioTotal,
                                                        remitodetalle.IdParte);
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

        public int RemitoDetalleTangoAdd(RemitoDetalleTango remitodetalle, string TComp, string TCompInS, string Talonario, string N_Comp)
        {
            try
            {
                //ParteEntrega oParte = remitodetalle.oRemito.oParte;
                
                int id = (int)SqlHelper.ExecuteScalar(SqlImplHelper.getTransactionTango(), 
                                                    "RemitoDetalleTangoAdd",
                                                     "",                        // [FILLER]
                                                     remitodetalle.cantidad,    //,[CAN_EQUI_V]
                                                     0,                         //,[CANT_DEV]
                                                     0,                         //,[CANT_OC]
                                                     remitodetalle.cantidad,    //,[CANT_PEND]
                                                     0,                         //,[CANT_SCRAP]
                                                     remitodetalle.cantidad,    //,[CANTIDAD]
                                                     remitodetalle.codTango,    //,[COD_ARTICU]
                                                     1,                         //,[COD_DEPOSI]
                                                     "",                        //,[DEPOSI_DDE]
                                                     remitodetalle.cantidad,    //,[EQUIVALENC]
                                                     DateTime.Now,              //,[FECHA_MOV]
                                                     "",                        //,[N_ORDEN_CO]
                                                     remitodetalle.nRenglon ,   //,[N_RENGL_OC]
                                                     remitodetalle.nRenglon,    //,[N_RENGL_S]
                                                     remitodetalle.nroRemito.ToString().PadLeft(8, '0'),   //,[NCOMP_IN_S]
                                                     0,                         //,[PLISTA_REM]
                                                     0,                         //,[PPP_EX]
                                                     0,                         //,[PPP_LO]
                                                     remitodetalle.PrecioUnit, //,[PRECIO]
                                                     remitodetalle.PrecioTotal, //,[PRECIO_REM]
                                                     TCompInS,                  //,[TCOMP_IN_S]
                                                     "S",                       //,[TIPO_MOV]
                                                     "",                        //,[COD_CLASIF]
                                                     0,                         //,[CANT_FACTU]
                                                     0,                         //,[DCTO_FACTU]
                                                     0,                         //,[CANT_DEV_2]
                                                     0,                         //,[CANT_PEND_2]
                                                     remitodetalle.cantidad,    //,[CANTIDAD_2]
                                                     0,                         //,[CANT_FACTU_2]
                                                     null,                      //,[ID_MEDIDA_STOCK_2]
                                                     3,                         //,[ID_MEDIDA_STOCK]
                                                     3,                         //,[ID_MEDIDA_VENTAS]
                                                     null,                      //,[ID_MEDIDA_COMPRA]
                                                     "P",                       //,[UNIDAD_MEDIDA_SELECCIONADA]
                                                      0,                        //,[PRECIO_REMITO_VENTAS]
                                                      0,                        //,[CANT_OC_2]
                                                      0,                        //,[RENGL_PADR]
                                                      "",                       //,[COD_ARTICU_KIT]
                                                      0                         //,[PROMOCION]
                                               );


                InsertDescripcionAdicional(remitodetalle, TComp, Talonario, N_Comp);

                return id;
            }
            catch (Exception ex)
            {
                ex.Source = "Remitosdetalle TantoAdd";
                throw ex;
            }
        }

        private int InsertDescripcionAdicional(RemitoDetalleTango remitodetalle, string TComp, string Talonario, string N_Comp)
        {
            try
            {
                return (int)SqlHelper.ExecuteScalar(SqlImplHelper.getTransactionTango(),
                                                       "RemitoDetalleDescripcionAdd",
                                                        "",                          // [FILLER]
                                                        "",                          //,[COD_MODELO]
                                                        remitodetalle.Descripcion,   //,[DESC]
                                                        remitodetalle.DescripAdic,   //,[DESC_ADIC]
                                                        N_Comp,                      //,[N_COMP]
                                                        remitodetalle.nRenglon,      //,[N_RENGLON]
                                                        Talonario,                   //,[TALONARIO]
                                                        TComp                        //,[T_COMP]
                   );

            }
            catch (Exception ex)
            {
                ex.Source = "Remitosdetalle TantoAdd insert descrip.Adic";
                throw ex;
            }
        }

		public bool RemitoDetalleUpdate(RemitoDetalle remitodetalle)
		{
			try
			{
				int update = SqlHelper.ExecuteNonQuery(SqlImplHelper.getConnectionString(), "RemitoDetalleUpdate",
														remitodetalle.IdRemitoDetalle, 
														remitodetalle.IdRemito, 
														remitodetalle.Cantidad, 
														remitodetalle.Detalle, 
														remitodetalle.PrecioUnit, 
														remitodetalle.PrecioTotal);
				if (update > 0)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		public bool RemitoDetalleDelete(int IdRemitoDetalle)
		{
			try
			{
				int update = SqlHelper.ExecuteNonQuery(SqlImplHelper.getConnectionString(), "RemitoDetalleDelete",
														IdRemitoDetalle);
				if (update > 0)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		public RemitoDetalle RemitoDetalleGetById(int IdRemitoDetalle)
		{
			try
			{
				DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(), "RemitoDetalleGetById",
														IdRemitoDetalle).Tables[0];
				RemitoDetalle NewEnt = new RemitoDetalle();

				if(dt.Rows.Count > 0)
				{
					DataRow dr = dt.Rows[0];
                    NewEnt = CargarRemito(dr);
				}
				return NewEnt;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

        private RemitoDetalle CargarRemito(DataRow dr)
        {
            RemitoDetalle NewEnt = new RemitoDetalle();

            NewEnt.IdRemitoDetalle = Int32.Parse(dr["IdRemitoDetalle"].ToString());
            NewEnt.IdRemito = Int32.Parse(dr["IdRemito"].ToString());
            NewEnt.Cantidad = Int32.Parse(dr["Cantidad"].ToString());
            NewEnt.Detalle = dr["Detalle"].ToString();
            NewEnt.PrecioUnit = Decimal.Parse(dr["PrecioUnit"].ToString());
            NewEnt.PrecioTotal = Decimal.Parse(dr["PrecioTotal"].ToString());
            NewEnt.IdParte = SqlImplHelper.getCeroIfNull(dr["idParte"].ToString());
            return NewEnt;
        }

		public List<RemitoDetalle> RemitoDetalleGetAll()
		{
			List<RemitoDetalle> lstRemitoDetalle = new List<RemitoDetalle>();
			try
			{
				DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(), "RemitoDetalleGetAll").Tables[0];
				if (dt.Rows.Count > 0)
				{
					for (int i = 0; dt.Rows.Count > i; i++)
					{
						RemitoDetalle NewEnt = new RemitoDetalle();
                        DataRow dr = dt.Rows[i];
                        NewEnt = CargarRemito(dr);
						lstRemitoDetalle.Add(NewEnt);
					}
				}
				return lstRemitoDetalle;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		#endregion

        internal List<RemitoDetalle> RemitoDetalleAddGetByRemito(int idRemito)
        {
            List<RemitoDetalle> lstRemitoDetalle = new List<RemitoDetalle>();
            try
            {
                DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(), "RemitoDetalleAddGetByRemito", idRemito).Tables[0];
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; dt.Rows.Count > i; i++)
                    {
                        RemitoDetalle NewEnt = new RemitoDetalle();
                        DataRow dr = dt.Rows[i];
                        NewEnt = CargarRemito(dr);
                        lstRemitoDetalle.Add(NewEnt);
                    }
                }
                return lstRemitoDetalle;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
