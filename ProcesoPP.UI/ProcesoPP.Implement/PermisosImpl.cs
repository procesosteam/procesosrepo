﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ProcesoPP.Model;
using ProcesoPP.SqlServerLibrary;
using System.Data;

namespace ProcesoPP.SqlServerImpl
{
    public class PermisosImpl
    {
        #region Permisos
        public int PermisosAdd(Permisos permiso)
        {
            try
            {
                return (int)SqlHelper.ExecuteScalar(SqlImplHelper.getConnectionString(), "PermisosAdd",
                                                        permiso.oModulo.idModulo,
                                                        permiso.oTipoUsuario.idTipoUsuario,
                                                        permiso.Lectura,
                                                        permiso.Escritura);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Permisos> PermisosGetAll()
        {
            List<Permisos> lstPermiso = new List<Permisos>();
            try
            {
                DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(), "PermisosGetAll").Tables[0];
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; dt.Rows.Count > i; i++)
                    {
                        lstPermiso.Add(CargarPermiso(dt.Rows[i]));
                    }
                }
                lstPermiso.Sort(delegate(Permisos p1, Permisos p2)
                {
                    return p1.oModulo.Descripcion.CompareTo(p2.oModulo.Descripcion);
                });

                return lstPermiso;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private Permisos CargarPermiso(DataRow dr)
        {
            Permisos NewEnt = new Permisos();
            NewEnt.idPermiso = Int32.Parse(dr["IdPermiso"].ToString());
            NewEnt.Escritura = bool.Parse(dr["Escritura"].ToString());
            NewEnt.Lectura = bool.Parse(dr["Lectura"].ToString());
            NewEnt.oTipoUsuario = TipoUsuariosGetById(int.Parse(dr["idTipoUsuario"].ToString()));
            NewEnt.oModulo = ModulosGetById(int.Parse(dr["idModulo"].ToString()));

            return NewEnt;
        }

        public bool PermisosDelete(int idPermisos)
        {
            try
            {
                int update = SqlHelper.ExecuteNonQuery(SqlImplHelper.getConnectionString(), "PermisosDelete",
                                                        idPermisos);
                if (update > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public List<Permisos> PermisosGetIdTipoUsuario(int idTipoUsuario)
        {
            List<Permisos> lstPermiso = new List<Permisos>();
            try
            {
                DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(), 
                                                        "PermisosGetIdTipoUsuario", 
                                                        idTipoUsuario).Tables[0];
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; dt.Rows.Count > i; i++)
                    {
                        lstPermiso.Add(CargarPermiso(dt.Rows[i]));
                    }
                }

                return lstPermiso;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region Modulo

        private Modulo ModulosGetById(int idModulo)
        {
            try
            {
                Modulo newEnt = new Modulo();
                DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(), 
                                                        "ModulosGetById",
                                                        idModulo).Tables[0];
                if (dt.Rows.Count > 0)
                {
                    
                    newEnt = CargarModulo(dt.Rows[0]);
                }
                return newEnt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Modulo> ModulosGetAll()
        {
            List<Modulo> lstModulo = new List<Modulo>();
            try
            {
                DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(), "ModulosGetAll").Tables[0];
                if (dt.Rows.Count > 0)
                {                   
                    for (int i = 0; dt.Rows.Count > i; i++)
                    {                        
                        lstModulo.Add(CargarModulo(dt.Rows[i]));
                    }
                }
                return lstModulo;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private Modulo CargarModulo(DataRow dr)
        {
            Modulo NewEnt = new Modulo();
            NewEnt.idModulo = Int32.Parse(dr["IdModulo"].ToString());
            NewEnt.Descripcion = dr["Descripcion"].ToString();

            return NewEnt;
        }
        
        #endregion

        #region TipoUsuario

        public TipoUsuario TipoUsuariosGetById(int idTipoUsuario)
        {
            try
            {
                TipoUsuario newEnt = new TipoUsuario();
                DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(),
                                                        "TipoUsuariosGetById",
                                                        idTipoUsuario).Tables[0];
                if (dt.Rows.Count > 0)
                {

                    newEnt = CargarTipoUsuario(dt.Rows[0]);
                }
                return newEnt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<TipoUsuario> TipoUsuariosGetAll()
        {
            List<TipoUsuario> lstTipoUsuario = new List<TipoUsuario>();
            try
            {
                DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(), "TipoUsuarioGetAll").Tables[0];
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; dt.Rows.Count > i; i++)
                    {
                        lstTipoUsuario.Add(CargarTipoUsuario(dt.Rows[i]));
                    }
                }
                return lstTipoUsuario;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private TipoUsuario CargarTipoUsuario(DataRow dr)
        {
            TipoUsuario NewEnt = new TipoUsuario();
            NewEnt.idTipoUsuario = Int32.Parse(dr["IdTipoUsuario"].ToString());
            NewEnt.Descripcion = dr["Descripcion"].ToString();

            return NewEnt;
        }

        #endregion

    }
}
