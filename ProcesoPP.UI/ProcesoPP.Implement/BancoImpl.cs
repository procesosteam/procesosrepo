

using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using ProcesoPP.SqlServerLibrary;
using ProcesoPP.Model;

namespace ProcesoPP.SqlServerImpl
{
	public class BancoImpl	
	{
		#region Banco methods

		public int BancoAdd(Banco banco)
		{
			try
			{
				return (int)SqlHelper.ExecuteScalar(SqlImplHelper.getConnectionString(), "BancoAdd",
														banco.Descripcion);
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		public bool BancoUpdate(Banco banco)
		{
			try
			{
				int update = SqlHelper.ExecuteNonQuery(SqlImplHelper.getConnectionString(), "BancoUpdate",
														banco.IdBanco, 
														banco.Descripcion);
				if (update > 0)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		public bool BancoDelete(int IdBanco)
		{
			try
			{
				int update = SqlHelper.ExecuteNonQuery(SqlImplHelper.getConnectionString(), "BancoDelete",
														IdBanco);
				if (update > 0)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		public Banco BancoGetById(int IdBanco)
		{
			try
			{
				DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(), "BancoGetById",
														IdBanco).Tables[0];
				Banco NewEnt = new Banco();

				if(dt.Rows.Count > 0)
				{
					DataRow dr = dt.Rows[0];
					NewEnt = CargarBanco(dr);
				}
				return NewEnt;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		public List<Banco> BancoGetAll()
		{
			List<Banco> lstBanco = new List<Banco>();
			try
			{
				DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(), "BancoGetAll").Tables[0];
				if (dt.Rows.Count > 0)
				{
					for (int i = 0; dt.Rows.Count > i; i++)
					{
						DataRow dr = dt.Rows[i];
						Banco NewEnt = new Banco();
						NewEnt = CargarBanco(dr);
						lstBanco.Add(NewEnt);
					}
				}
				return lstBanco;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		private Banco CargarBanco(DataRow dr)
		{
			try
			{
				Banco oObjeto = new Banco();

				oObjeto.IdBanco = int.Parse(dr["IdBanco"].ToString());
				oObjeto.Descripcion = (dr["Descripcion"].ToString());
				
				return oObjeto;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		#endregion

	}
}
