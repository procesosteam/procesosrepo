
using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using ProcesoPP.SqlServerLibrary;
using ProcesoPP.Model;

namespace ProcesoPP.SqlServerImpl
{
	public class SolicitanteImpl	
	{
		#region Solicitante methods

		public int SolicitanteAdd(Solicitante solicitante)
		{
			try
			{
				return (int)SqlHelper.ExecuteScalar(SqlImplHelper.getConnectionString(), "SolicitanteAdd",
														solicitante.Nombre, 
														solicitante.Apellido, 
														SqlImplHelper.getNullIfCero(solicitante.IdSector.ToString()), 
													    SqlImplHelper.getNullIfCero(solicitante.IdEmpresa.ToString()), 
														solicitante.Telefono);
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		public bool SolicitanteUpdate(Solicitante solicitante)
		{
			try
			{
				int update = SqlHelper.ExecuteNonQuery(SqlImplHelper.getConnectionString(), "SolicitanteUpdate",
														solicitante.IdSolicitante, 
														solicitante.Nombre, 
														solicitante.Apellido,
                                                        SqlImplHelper.getNullIfCero(solicitante.IdSector.ToString()),
                                                        SqlImplHelper.getNullIfCero(solicitante.IdEmpresa.ToString()),
                                                        solicitante.Telefono);
				if (update > 0)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		public bool SolicitanteDelete(int IdSolicitante)
		{
			try
			{
				int update = SqlHelper.ExecuteNonQuery(SqlImplHelper.getConnectionString(), "SolicitanteDelete",
														IdSolicitante);
				if (update > 0)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		public Solicitante SolicitanteGetById(int IdSolicitante)
		{
			try
			{
				DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(), "SolicitanteGetById",
														IdSolicitante).Tables[0];
				Solicitante NewEnt = new Solicitante();
				if(dt.Rows.Count > 0)
                {
                    EmpresaImpl oEmpresaImpl = new EmpresaImpl();
                    SectorImpl oSectorImpl = new SectorImpl();
					DataRow dr = dt.Rows[0];

					NewEnt.IdSolicitante = Int32.Parse(dr["IdSolicitante"].ToString());
					NewEnt.Nombre = dr["Nombre"].ToString();
					NewEnt.Apellido = dr["Apellido"].ToString();
                    NewEnt.IdSector = SqlImplHelper.getCeroIfNull(dr["IdSector"].ToString());
                    NewEnt.IdEmpresa = SqlImplHelper.getCeroIfNull(dr["IdEmpresa"].ToString());
					NewEnt.Telefono = dr["Telefono"].ToString();
                    NewEnt.oSector = oSectorImpl.SectorGetById(NewEnt.IdSector);
				}
				return NewEnt;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		public List<Solicitante> SolicitanteGetAll()
		{
			List<Solicitante> lstSolicitante = new List<Solicitante>();
			try
			{
				DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(), "SolicitanteGetAll").Tables[0];
				if (dt.Rows.Count > 0)
				{
                    EmpresaImpl oEmpresaImpl = new EmpresaImpl();
                    SectorImpl oSectorImpl = new SectorImpl();
					int colIdSolicitante =  dt.Columns["IdSolicitante"].Ordinal;
					int colNombre =  dt.Columns["Nombre"].Ordinal;
					int colApellido =  dt.Columns["Apellido"].Ordinal;
					int colIdSector =  dt.Columns["IdSector"].Ordinal;
					int colIdEmpresa =  dt.Columns["IdEmpresa"].Ordinal;
					int colTelefono =  dt.Columns["Telefono"].Ordinal;
					for (int i = 0; dt.Rows.Count > i; i++)
					{
						Solicitante NewEnt = new Solicitante();
						NewEnt.IdSolicitante = Int32.Parse(dt.Rows[i].ItemArray[colIdSolicitante].ToString());
						NewEnt.Nombre = dt.Rows[i].ItemArray[colNombre].ToString();
						NewEnt.Apellido = dt.Rows[i].ItemArray[colApellido].ToString();
						NewEnt.IdSector = Int32.Parse(dt.Rows[i].ItemArray[colIdSector].ToString());
						NewEnt.IdEmpresa = Int32.Parse(dt.Rows[i].ItemArray[colIdEmpresa].ToString());
						NewEnt.Telefono = dt.Rows[i].ItemArray[colTelefono].ToString();
                        NewEnt.oSector = oSectorImpl.SectorGetById(NewEnt.IdSector);
                        NewEnt.oEmpresa = oEmpresaImpl.EmpresaGetById(NewEnt.IdEmpresa);
						lstSolicitante.Add(NewEnt);
					}
				}
				return lstSolicitante;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		#endregion


        public List<Solicitante> SolicitanteGetByFilter(int idSector, int idEmpresa)
        {
            List<Solicitante> lstSolicitante = new List<Solicitante>();
			try
			{
				DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(), "SolicitanteGetByFilter",
                                                                                              SqlImplHelper.getNullIfCero(idSector.ToString()),
                                                                                              SqlImplHelper.getNullIfCero(idEmpresa.ToString())).Tables[0];
				if (dt.Rows.Count > 0)
				{
                    EmpresaImpl oEmpresaImpl = new EmpresaImpl();
                    SectorImpl oSectorImpl = new SectorImpl();
					int colIdSolicitante =  dt.Columns["IdSolicitante"].Ordinal;
					int colNombre =  dt.Columns["Nombre"].Ordinal;
					int colApellido =  dt.Columns["Apellido"].Ordinal;
					int colIdSector =  dt.Columns["IdSector"].Ordinal;
					int colIdEmpresa =  dt.Columns["IdEmpresa"].Ordinal;
					int colTelefono =  dt.Columns["Telefono"].Ordinal;
					for (int i = 0; dt.Rows.Count > i; i++)
					{
                        DataRow dr = dt.Rows[i];
						Solicitante NewEnt = new Solicitante();
						NewEnt.IdSolicitante = Int32.Parse(dr[colIdSolicitante].ToString());
						NewEnt.Nombre = dr[colNombre].ToString();
						NewEnt.Apellido = dr[colApellido].ToString();
						NewEnt.IdSector = Int32.Parse(dr[colIdSector].ToString());
						NewEnt.IdEmpresa = Int32.Parse(dr[colIdEmpresa].ToString());
						NewEnt.Telefono = dr[colTelefono].ToString();
                        NewEnt.oSector = oSectorImpl.SectorGetById(NewEnt.IdSector);
                        NewEnt.oEmpresa = oEmpresaImpl.EmpresaGetById(NewEnt.IdEmpresa);
						lstSolicitante.Add(NewEnt);
					}
				}
				return lstSolicitante;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
