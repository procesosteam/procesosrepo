
using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using ProcesoPP.SqlServerLibrary;
using ProcesoPP.Model;

namespace ProcesoPP.SqlServerImpl
{
	public class OrdenTrabajoImpl	
	{
		#region OrdenTrabajo methods

		public int OrdenTrabajoAdd(OrdenTrabajo ordentrabajo)
		{
            try
            {
                int idOTI = (int)SqlHelper.ExecuteScalar(SqlImplHelper.getTransaction(), "OrdenTrabajoAdd",
                                                        ordentrabajo.IdTransporte,
                                                        SqlImplHelper.getNullIfCero(ordentrabajo.IdEquipo.ToString()),
                                                        SqlImplHelper.getNullIfDateMinValue(ordentrabajo.Horario),
                                                        ordentrabajo.Salida,
                                                        ordentrabajo.Llegada,
                                                        ordentrabajo.KmsRecorridos,
                                                        ordentrabajo.IdSolicitudCliente,
                                                        ordentrabajo.Controlo,
                                                        ordentrabajo.Verifico,
                                                        SqlImplHelper.getNullIfCero(ordentrabajo.IdVehiculo.ToString()),
                                                        SqlImplHelper.getNullIfCero(ordentrabajo.IdChofer.ToString()),
                                                        SqlImplHelper.getNullIfCero(ordentrabajo.IdAcompanante.ToString()),
                                                        ordentrabajo.Entraga);
                OrdenTrabajoConceptoImpl oOTIConImp = new OrdenTrabajoConceptoImpl();
                if (ordentrabajo.oListOTIConcepto != null || ordentrabajo.oListOTIConcepto.Count > 0)
                {
                    foreach (OrdenTrabajoConcepto item in ordentrabajo.oListOTIConcepto)
                    {
                        item.IdOti = idOTI;
                        oOTIConImp.OrdenTrabajoConceptoAdd(item);
                    }
                }

                OrdenTrabajoDetalleImpl oOrdenTrabajoDetalleImpl = new OrdenTrabajoDetalleImpl();
                if (ordentrabajo.oListOTIDetalle != null || ordentrabajo.oListOTIDetalle.Count > 0)
                {
                    foreach (OrdenTrabajoDetalle oDet in ordentrabajo.oListOTIDetalle)
                    {
                        oDet.IdOrden = idOTI;
                        oOrdenTrabajoDetalleImpl.OrdenTrabajoDetalleAdd(oDet);
                    }
                }

                SolicitudClienteImpl oSCImp = new SolicitudClienteImpl();
                SolicitudCliente oSC = oSCImp.SolicitudClienteGetById(ordentrabajo.IdSolicitudCliente, true);
                if (oSC.Entrega != ordentrabajo.Entraga && !(oSC.NroPedidoCliente.Substring(oSC.NroPedidoCliente.Length-1,1).Equals("Z")))
                {
                    oSC.Entrega = ordentrabajo.Entraga;
                    oSCImp.SolicitudClienteUpdate(oSC);    
                }
                
                SqlImplHelper.commitTransaction();
                return idOTI;
            }
            catch (Exception ex)
            {
                SqlImplHelper.rollbackTransaction();
                throw ex;
            }
		}

		public bool OrdenTrabajoUpdate(OrdenTrabajo ordentrabajo)
		{
			try
			{
				int update = SqlHelper.ExecuteNonQuery(SqlImplHelper.getTransaction(), "OrdenTrabajoUpdate",
														ordentrabajo.IdOrden, 
														ordentrabajo.IdTransporte,
                                                        SqlImplHelper.getNullIfCero(ordentrabajo.IdEquipo.ToString()), 
                                                        SqlImplHelper.getNullIfDateMinValue(ordentrabajo.Horario), 
														ordentrabajo.Salida, 
														ordentrabajo.Llegada, 
														ordentrabajo.KmsRecorridos, 
														ordentrabajo.IdSolicitudCliente,
														ordentrabajo.Controlo, 
														ordentrabajo.Verifico,
                                                        SqlImplHelper.getNullIfCero(ordentrabajo.IdVehiculo.ToString()),
                                                        SqlImplHelper.getNullIfCero(ordentrabajo.IdChofer.ToString()),
                                                        SqlImplHelper.getNullIfCero(ordentrabajo.IdAcompanante.ToString()),
                                                        ordentrabajo.Baja,
                                                        ordentrabajo.Entraga);
                OrdenTrabajoConceptoImpl oOTIConImp = new OrdenTrabajoConceptoImpl();
                if (ordentrabajo.oListOTIConcepto != null || ordentrabajo.oListOTIConcepto.Count > 0)
                {
                    foreach (OrdenTrabajoConcepto item in ordentrabajo.oListOTIConcepto)
                    {
                        oOTIConImp.OrdenTrabajoConceptoUpdate(item);
                    }
                }
                OrdenTrabajoDetalleImpl oOrdenTrabajoDetalleImpl = new OrdenTrabajoDetalleImpl();
                if (ordentrabajo.oListOTIDetalle != null || ordentrabajo.oListOTIDetalle.Count > 0)
                {
                    foreach (OrdenTrabajoDetalle oDet in ordentrabajo.oListOTIDetalle)
                    {
                        oOrdenTrabajoDetalleImpl.OrdenTrabajoDetalleUpdate(oDet);
                    }
                }

                SolicitudClienteImpl oSCImp = new SolicitudClienteImpl();
                SolicitudCliente oSC = oSCImp.SolicitudClienteGetById(ordentrabajo.IdSolicitudCliente, true);
                if (oSC.Entrega != ordentrabajo.Entraga)
                {
                    oSC.Entrega = ordentrabajo.Entraga;
                    oSCImp.SolicitudClienteUpdate(oSC);
                }

                SqlImplHelper.commitTransaction();
				if (update > 0)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			catch(Exception ex)
			{
                SqlImplHelper.rollbackTransaction();
				throw ex;
			}
		}

		public bool OrdenTrabajoDelete(int IdOrden)
		{
			try
			{
				int update = SqlHelper.ExecuteNonQuery(SqlImplHelper.getConnectionString(), "OrdenTrabajoDelete",
														IdOrden);
				if (update > 0)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		public OrdenTrabajo OrdenTrabajoGetById(int IdOrden)
		{
			try
			{
				DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(), "OrdenTrabajoGetById",
														IdOrden).Tables[0];
				OrdenTrabajo NewEnt = new OrdenTrabajo();

				if(dt.Rows.Count > 0)
				{
					DataRow dr = dt.Rows[0];
                    NewEnt = CargarOrden(dr);
				}
				return NewEnt;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

        private OrdenTrabajo CargarOrden(DataRow dr)
        {
            OrdenTrabajo NewEnt = new OrdenTrabajo();
            SolicitudClienteImpl oSolicitudClienteImpl = new SolicitudClienteImpl();            
            VehiculoImpl oVehiculoImpl = new VehiculoImpl();
            PersonalImpl oPersonalImpl = new PersonalImpl(); 
            OrdenTrabajoConceptoImpl oOTICon = new OrdenTrabajoConceptoImpl();
            CuadrillaImpl oCuadrillaImpl = new CuadrillaImpl();
            OrdenTrabajoDetalleImpl oOrdenTrabajoDetalleImpl = new OrdenTrabajoDetalleImpl();

            NewEnt.IdOrden = Int32.Parse(dr["IdOrden"].ToString());
            NewEnt.IdTransporte = Int32.Parse(dr["IdTransporte"].ToString());
            NewEnt.IdEquipo = SqlImplHelper.getCeroIfNull(dr["IdEquipo"]);
            NewEnt.Horario = dr["Horario"].ToString() == string.Empty ? DateTime.MinValue : DateTime.Parse(dr["Horario"].ToString());
            NewEnt.Salida = dr["Salida"].ToString();
            NewEnt.Llegada = dr["Llegada"].ToString();
            NewEnt.KmsRecorridos = SqlImplHelper.getCeroDecIfNull(dr["KmsRecorridos"]);
            NewEnt.IdSolicitudCliente = int.Parse(dr["IdSolicitudCliente"].ToString());
            NewEnt.Controlo = dr["Controlo"].ToString();
            NewEnt.Verifico = dr["Verifico"].ToString();
            NewEnt.IdVehiculo = SqlImplHelper.getCeroIfNull(dr["IdTrailer"]);
            NewEnt.IdChofer = SqlImplHelper.getCeroIfNull(dr["idChofer"]);
            NewEnt.IdAcompanante = SqlImplHelper.getCeroIfNull(dr["idAcompanante"]);
            NewEnt.oSolicitudCliente = oSolicitudClienteImpl.SolicitudClienteGetById(NewEnt.IdSolicitudCliente);
            
            NewEnt.oListOTIConcepto = oOTICon.OrdenTrabajoConceptoGetByIdOrden(NewEnt.IdOrden);
            NewEnt.oListOTIDetalle = oOrdenTrabajoDetalleImpl.OrdenTrabajoDetalleGetByIdOrden(NewEnt.IdOrden);
            if (NewEnt.IdChofer != 0)
                NewEnt.oChofer = oPersonalImpl.PersonalGetById(NewEnt.IdChofer);
            if (NewEnt.IdAcompanante != 0)
                NewEnt.oAcompanante = oPersonalImpl.PersonalGetById(NewEnt.IdAcompanante);
            if (NewEnt.IdTransporte != 0)
                NewEnt.oTransporte = oVehiculoImpl.VehiculoGetById(NewEnt.IdTransporte);
            if (NewEnt.IdVehiculo != 0)
                NewEnt.oVehiculo = oVehiculoImpl.VehiculoGetById(NewEnt.IdVehiculo);
            if (NewEnt.IdEquipo != 0)
                NewEnt.oCuadrilla = oCuadrillaImpl.CuadrillaGetById(NewEnt.IdEquipo);
                
            return NewEnt;
        }

		public List<OrdenTrabajo> OrdenTrabajoGetAll()
		{
			List<OrdenTrabajo> lstOrdenTrabajo = new List<OrdenTrabajo>();
			try
			{
				DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(), "OrdenTrabajoGetAll").Tables[0];
				if (dt.Rows.Count > 0)
				{					
					for (int i = 0; dt.Rows.Count > i; i++)
					{
						OrdenTrabajo NewEnt = new OrdenTrabajo();
                        SolicitudClienteImpl oSolicitudClienteImpl = new SolicitudClienteImpl();
                        NewEnt = CargarOrden(dt.Rows[i]);

						lstOrdenTrabajo.Add(NewEnt);
					}
				}
				return lstOrdenTrabajo;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		#endregion


        public DataTable OrdenTrabajoGetByFilter(string nroSolicitud, string idEmpresa, string fechaDesde, string fechaHasta, string nroParte, string idTrailer, string idSector, string idCuadrilla, string idTipoServicio, string idCondicion)
        {
            try
            {
                DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(),
                                                        "OrdenTrabajoGetByFilter",
                                                        SqlImplHelper.getNullIfCero(nroSolicitud),
                                                        SqlImplHelper.getNullIfCero(idEmpresa),
                                                        SqlImplHelper.getNullIfDate(fechaDesde),
                                                        SqlImplHelper.getNullIfDate(fechaHasta),
                                                        SqlImplHelper.getNullIfCero(nroParte),
                                                        SqlImplHelper.getNullIfCero(idTrailer),
                                                        SqlImplHelper.getNullIfCero(idSector),
                                                        SqlImplHelper.getNullIfCero(idCuadrilla),
                                                        SqlImplHelper.getNullIfCero(idTipoServicio),
                                                        SqlImplHelper.getNullIfCero(idCondicion)).Tables[0];

                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public object OrdenTrabajoRegresoGetByFilter(string nroSolicitud, string idEmpresa, string fechaDesde, string fechaHasta)
        {
            try
            {
                DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(),
                                                        "OrdenTrabajoRegresoGetByFilter",
                                                        SqlImplHelper.getNullIfCero(nroSolicitud),
                                                        SqlImplHelper.getNullIfCero(idEmpresa),
                                                        SqlImplHelper.getNullIfDate(fechaDesde),
                                                        SqlImplHelper.getNullIfDate(fechaHasta)).Tables[0];

                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public OrdenTrabajo OrdenTrabajoGetByIdSolicitud(int idSS)
        {
            try
            {
                DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(), 
                                                        "OrdenTrabajoGetByIdSolicitud",
                                                        idSS).Tables[0];
                OrdenTrabajo NewEnt = new OrdenTrabajo();

                if (dt.Rows.Count > 0)
                {
                    DataRow dr = dt.Rows[0];
                    NewEnt = CargarOrden(dr);
                }
                return NewEnt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<OrdenTrabajo> SolicitudesParteInterno(DateTime hoy)
        {
            List<OrdenTrabajo> lstOrdenTrabajo = new List<OrdenTrabajo>();
            try
            {
                DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(), "SolicitudesParteInterno",
                                                                                              hoy).Tables[0];
                if (dt.Rows.Count > 0)
                {

                    for (int i = 0; dt.Rows.Count > i; i++)
                    {
                        DataRow dr = dt.Rows[i];
                        OrdenTrabajo NewEnt = new OrdenTrabajo();
                        NewEnt = CargarOrden(dr);
                        lstOrdenTrabajo.Add(NewEnt);
                    }
                }
                return lstOrdenTrabajo;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
