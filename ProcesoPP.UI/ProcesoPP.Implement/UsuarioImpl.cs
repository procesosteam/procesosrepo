
using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using ProcesoPP.SqlServerLibrary;
using ProcesoPP.Model;

namespace ProcesoPP.SqlServerImpl
{
	public class UsuarioImpl	
	{
		#region Usuario methods

		public int UsuarioAdd(Usuario usuario)
		{
			try
			{
				usuario.IdUsuario= (int)SqlHelper.ExecuteScalar(SqlImplHelper.getConnectionString(), "UsuarioAdd",
														usuario.Nombre, 
														usuario.Apellido, 
														usuario.Telefono, 
														usuario.Direccion, 
														usuario.Clave,
                                                        usuario.UserName,
                                                        usuario.oTipoUsuario.idTipoUsuario,
                                                        usuario.Mail);
                SqlImplHelper.commitTransaction();
                return usuario.IdUsuario;
			}
			catch(Exception ex)
			{
                SqlImplHelper.rollbackTransaction();
				throw ex;
			}
		}

		public bool UsuarioUpdate(Usuario usuario)
		{
			try
			{
				int update = SqlHelper.ExecuteNonQuery(SqlImplHelper.getConnectionString(), "UsuarioUpdate",
														usuario.IdUsuario, 
														usuario.Nombre, 
														usuario.Apellido, 
														usuario.Telefono, 
														usuario.Direccion,
                                                        usuario.Clave,
                                                        usuario.UserName,
                                                        usuario.oTipoUsuario.idTipoUsuario,
                                                        usuario.Mail);
                SqlImplHelper.commitTransaction();
				if (update > 0)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			catch(Exception ex)
			{
                SqlImplHelper.rollbackTransaction();
				throw ex;
			}
		}

		public bool UsuarioDelete(int IdUsuario)
		{
			try
			{
				int update = SqlHelper.ExecuteNonQuery(SqlImplHelper.getConnectionString(), "UsuarioDelete",
														IdUsuario);
				if (update > 0)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		public Usuario UsuarioGetById(int IdUsuario)
		{
			try
			{
				DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(), "UsuarioGetById",
														IdUsuario).Tables[0];
				Usuario NewEnt = new Usuario();

				if(dt.Rows.Count > 0)
				{
					DataRow dr = dt.Rows[0];
                    NewEnt = CargarUsuario(dr);
				}
				return NewEnt;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

        private Usuario CargarUsuario(DataRow dr)
        {
            Usuario NewEnt = new Usuario();
            PermisosImpl oPermisosImpl = new PermisosImpl();
            NewEnt.IdUsuario = Int32.Parse(dr["IdUsuario"].ToString());
            NewEnt.Nombre = dr["Nombre"].ToString();
            NewEnt.Apellido = dr["Apellido"].ToString();
            NewEnt.Telefono = dr["Telefono"].ToString();
            NewEnt.Direccion = dr["Direccion"].ToString();
            NewEnt.Clave = dr["Clave"].ToString();
            NewEnt.UserName = dr["UserName"].ToString();
            NewEnt.Mail = dr["mail"].ToString();
            NewEnt.oTipoUsuario = oPermisosImpl.TipoUsuariosGetById(int.Parse(dr["idTipoUsuario"].ToString()));
            return NewEnt;
        }

		public List<Usuario> UsuarioGetAll()
		{
			List<Usuario> lstUsuario = new List<Usuario>();
			try
			{
				DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(), "UsuarioGetAll").Tables[0];
				if (dt.Rows.Count > 0)
				{				
					for (int i = 0; dt.Rows.Count > i; i++)
					{
						Usuario NewEnt = new Usuario();
                        NewEnt = CargarUsuario(dt.Rows[i]);
						lstUsuario.Add(NewEnt);
					}
				}
				return lstUsuario;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		#endregion

	}
}
