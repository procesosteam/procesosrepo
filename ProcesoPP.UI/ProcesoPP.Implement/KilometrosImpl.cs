﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ProcesoPP.Model;
using ProcesoPP.SqlServerLibrary;
using System.Data;

namespace ProcesoPP.SqlServerImpl
{
    public class KilometrosImpl
    {
        public int KilometrosAdd(Kilometros oKilometros)
        {
            try
            {

                if (KilometrosGetBy(oKilometros) == null)
                {
                    oKilometros.idKms = (int)SqlHelper.ExecuteScalar(SqlImplHelper.getConnectionString(),
                                                        "KilometrosAdd",
                                                        oKilometros.oLugarDesde.IdLugar,
                                                        oKilometros.oLugarHasta.IdLugar,
                                                        oKilometros.Kms,
                                                        oKilometros.KmsEstablecidos);

                    return oKilometros.idKms;
                }
                else
                    return 0;
                //{
                //    throw new ArgumentException("Los kilometros ya fueron asignados");
                //}
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Kilometros KilometrosGetBy(Kilometros oKilometros)
        {
            try
            {
                DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(), 
                                                        "KilometrosGetBy",
                                                        oKilometros.oLugarDesde.IdLugar,
                                                        oKilometros.oLugarHasta.IdLugar,
                                                        oKilometros.KmsEstablecidos).Tables[0];
                if (dt.Rows.Count > 0)
                {
                    DataRow dr = dt.Rows[0];
                    return LlenarKilometros(dr);
                }
                else
                {
                    return null;
                }
                 
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Kilometros> KilometrosGetAll()
        {
            try
            {              
                List<Kilometros> oListKms = new List<Kilometros>();
                DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(), "KilometrosGetAll", null).Tables[0];
                if (dt.Rows.Count > 0)
                {                   
                    for (int i = 0; dt.Rows.Count > i; i++)
                    {
                        DataRow dr = dt.Rows[i];                    
                        oListKms.Add(LlenarKilometros(dr));
                    }
                }
                
                return oListKms;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Kilometros> KilometrosGetAll(string idTipoServicio)
        {
            try
            {
                List<Kilometros> oListKms = new List<Kilometros>();
                DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(), "KilometrosGetAll",
                                                                                              SqlImplHelper.getNullIfEmpty(idTipoServicio)).Tables[0];
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; dt.Rows.Count > i; i++)
                    {
                        DataRow dr = dt.Rows[i];
                        oListKms.Add(LlenarKilometros(dr));
                    }
                }

                return oListKms;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private Kilometros LlenarKilometros(DataRow dr)
        {            
            Kilometros NewEnt = new Kilometros();
            NewEnt.idKms = Int32.Parse(dr["idKms"].ToString());
            NewEnt.oLugarDesde = new Lugar();
            NewEnt.oLugarDesde.IdLugar = int.Parse(dr["idLugarDesde"].ToString());
            NewEnt.oLugarDesde.Descripcion = dr["lugarDesde"].ToString();
            NewEnt.oLugarHasta = new Lugar();
            NewEnt.oLugarHasta.IdLugar = int.Parse(dr["idLugarHasta"].ToString());
            NewEnt.oLugarHasta.Descripcion = dr["lugarHasta"].ToString();
            NewEnt.Kms = decimal.Parse(dr["Kms"].ToString());
            NewEnt.KmsEstablecidos = SqlImplHelper.getFalseIfNull(dr["kmsEstablecidos"]);
            return NewEnt;
        }

        public int KilometrosDelete(int idKms)
        {
            try
            {
                int update = SqlHelper.ExecuteNonQuery(SqlImplHelper.getConnectionString(), "KilometrosDelete",
                                                        idKms);
                return update;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Kilometros> KilometrosGetByFilter(string idDesde, string idHasta)
        {
            try
            {
                List<Kilometros> oListKms = new List<Kilometros>();
                DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(), 
                                                        "KilometrosGetByFilter",
                                                        SqlImplHelper.getNullIfCero(idDesde),
                                                        SqlImplHelper.getNullIfCero(idHasta)).Tables[0];
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; dt.Rows.Count > i; i++)
                    {
                        DataRow dr = dt.Rows[i];
                        oListKms.Add(LlenarKilometros(dr));
                    }
                }
                return oListKms;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
