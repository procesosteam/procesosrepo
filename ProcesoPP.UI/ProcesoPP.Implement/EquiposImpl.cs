
using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using ProcesoPP.SqlServerLibrary;
using ProcesoPP.Model;

namespace ProcesoPP.SqlServerImpl
{
	public class EquiposImpl	
	{
		#region Equipos methods

		public int EquiposAdd(Equipos equipos)
		{
			try
			{
				return (int)SqlHelper.ExecuteScalar(SqlImplHelper.getConnectionString(), "EquiposAdd",
														equipos.Descripcion,
                                                        equipos.idCliente);
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		public bool EquiposUpdate(Equipos equipos)
		{
			try
			{
				int update = SqlHelper.ExecuteNonQuery(SqlImplHelper.getConnectionString(), "EquiposUpdate",
														equipos.IdEquipo,
                                                        equipos.Descripcion,
                                                        equipos.idCliente);
				if (update > 0)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		public bool EquiposDelete(int IdEquipo)
		{
			try
			{
				int update = SqlHelper.ExecuteNonQuery(SqlImplHelper.getConnectionString(), "EquiposDelete",
														IdEquipo);
				if (update > 0)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		public Equipos EquiposGetById(int IdEquipo)
		{
			try
			{
				DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(), "EquiposGetById",
														IdEquipo).Tables[0];
				Equipos NewEnt = new Equipos();

				if(dt.Rows.Count > 0)
				{
                    DataRow dr = dt.Rows[0];
                    NewEnt = CargarEquipo(dr);
					
				}
				return NewEnt;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

        private Equipos CargarEquipo(DataRow dr)
        {
            Equipos NewEnt = new Equipos();

            NewEnt.IdEquipo = Int32.Parse(dr["IdEquipo"].ToString());
            NewEnt.Descripcion = dr["Descripcion"].ToString();
            NewEnt.idCliente = SqlImplHelper.getCeroIfNull(dr["idEmpresa"]);

            return NewEnt;
        }

		public List<Equipos> EquiposGetAll()
		{
			List<Equipos> lstEquipos = new List<Equipos>();
			try
			{
				DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(), "EquiposGetAll").Tables[0];
				if (dt.Rows.Count > 0)
				{
					
					for (int i = 0; dt.Rows.Count > i; i++)
					{
						Equipos NewEnt = new Equipos();
                        DataRow dr = dt.Rows[i];
                        NewEnt = CargarEquipo(dr);
						lstEquipos.Add(NewEnt);
					}
				}
				return lstEquipos;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		#endregion


        public DataTable EquiposGetByIdCliente(int idCliente)
        {
            try
            {
                DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(), "EquiposGetByIdCliente", 
                                                                                               SqlImplHelper.getNullIfCero(idCliente.ToString())).Tables[0];
                if (dt.Rows.Count > 0)
                {
                    return dt;
                }
                else
                    return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable EquiposGetByClienteEspecificacion(int idEmpresa, List<EspecificacionTecnica> oListET)
        {
            try
            {
                DataTable dt = SqlHelper.ExecuteDataset(SqlImplHelper.getConnectionString(), "EquiposGetByClienteEspecificacion",
                                                                                               SqlImplHelper.getNullIfCero(idEmpresa.ToString()),
                                                                                               getIDS(oListET)).Tables[0];
                if (dt.Rows.Count > 0)
                {
                    return dt;
                }
                else
                    return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        private string getIDS(List<EspecificacionTecnica> oEspecificaciones)
        {
            string ids = "", coma = "";
            foreach (EspecificacionTecnica oET in oEspecificaciones)
            {
                ids += coma + oET.IdEspecificaciones.ToString();
                coma = ",";
            }

            return ids;
        }

    }
}
