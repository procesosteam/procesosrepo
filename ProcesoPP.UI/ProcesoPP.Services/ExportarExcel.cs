﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Collections;
using System.Data;
using System.Web.UI.WebControls;
using System.IO;
using System.Web.UI;
using System.Globalization;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.HPSF;
using NPOI.POIFS.FileSystem;
using NPOI.SS.Util;
using NPOI.HSSF.Util;

namespace ParteDiario.Services
{    
    public class ExportarExcel
    {
        #region Propiedades

        private CultureInfo _culInfo
        {
            get
            {
                return (CultureInfo)CultureInfo.CreateSpecificCulture(HttpContext.Current.Request.UserLanguages[0]);
            }
        }
        private string _NombreArchivo { get; set; }
        private string _pathImage { get; set; }
        private string _title { get; set; }

        private static HSSFWorkbook Libro;

        #endregion

        #region Constructor

        /// <summary>
        /// Esta clase nos permite exportar a excel los datos apartir de un conjunto de datos, como un dataTable.
        /// </summary>
        /// <param name="nombreArchivo">Es el nombre que se le dará al archivo que ser exportará. No se debe especificar la extensión del mismo.</param>
        public ExportarExcel(string nombreArchivo)
        {
            _NombreArchivo = nombreArchivo + ".xls";            
        }
        public ExportarExcel(string nombreArchivo, string pathImage, string title)
        {
            _NombreArchivo = nombreArchivo + ".xls";
            _pathImage = pathImage;
            _title = title;
        }

        #endregion

        #region < NPOI >

        /// <summary>
        /// Exportar a Excel 2003 con libreria NPOI
        /// </summary>
        /// <param name="Response">HttpResponse de la Pagina que lo llama</param>
        /// <param name="dtDatos">DataTable con datos a exportar</param>
        public void ExportarExcel2003(DataTable dtDatos)
        {            
            System.Web.HttpResponse Response = System.Web.HttpContext.Current.Response;
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", _NombreArchivo));
            Response.Clear();

            InitializeWorkbook();
            HSSFSheet Hoja = new HSSFSheet(Libro);
            Hoja = CargaExcel2003(dtDatos);            
            CrearEncabezado(Libro, Hoja);
                   
            Response.BinaryWrite(WriteToStream(Libro).GetBuffer());
            Response.End();
        }

        static void InitializeWorkbook()
        {
            Libro = new HSSFWorkbook();

            //create a entry of DocumentSummaryInformation
            DocumentSummaryInformation dsi = PropertySetFactory.CreateDocumentSummaryInformation();
            dsi.Company = "PROCESOS PATAGONICOS";
            Libro.DocumentSummaryInformation = dsi;

            //create a entry of SummaryInformation
            SummaryInformation si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "PROCESOS PATAGONICOS";
            Libro.SummaryInformation = si;
        }

        static MemoryStream WriteToStream(HSSFWorkbook hssfworkbook)
        {
            //Write the stream data of workbook to the root directory
            MemoryStream file = new MemoryStream();
            hssfworkbook.Write(file);
            return file;
        }

        /// <summary>
        /// Exporta Datatable a Excel 2003
        /// </summary>
        /// <param name="dtDatos">DataTable con los datos</param>
        /// <param name="sheetData">Hoja de Excel (HSSFSheet)</param>
        /// <returns></returns>
        private HSSFSheet CargaExcel2003(DataTable dtDatos)
        {
            HSSFSheet Hoja = (HSSFSheet)Libro.CreateSheet();
            DateTime dateResult;
            int irow = 4;
            IRow rowHeader = Hoja.CreateRow(irow);
            foreach (DataColumn col in dtDatos.Columns)
            {
                ICell cellHeader = rowHeader.CreateCell(col.Ordinal);
                cellHeader.SetCellValue(col.ColumnName.ToUpper());

                ICellStyle style = Libro.CreateCellStyle();
                style.Alignment = HorizontalAlignment.CENTER;
                style.VerticalAlignment = VerticalAlignment.CENTER;
                style.FillPattern = FillPatternType.SOLID_FOREGROUND;
                style.FillForegroundColor = HSSFColor.GREY_25_PERCENT.index;
                IFont font = Libro.CreateFont();
                font.Boldweight = (short)FontBoldWeight.BOLD;                
                style.SetFont(font);
                cellHeader.CellStyle = style;
                Hoja.AutoSizeColumn(col.Ordinal);
            }
            irow++;
            foreach (DataRow dr in dtDatos.Rows)
            {
                IRow row = Hoja.CreateRow(irow);
                irow++;
                for (int i = 0; i < dr.ItemArray.Length; i++)
                {
                    ICell cell = row.CreateCell(i);
                    Hoja.AutoSizeColumn(cell.ColumnIndex);
                    if (dr[i].GetType() == typeof(decimal))
                    {
                        cell.SetCellType(NPOI.SS.UserModel.CellType.NUMERIC);
                        cell.SetCellValue(double.Parse(dr[i].ToString().Replace(',', '.')));
                    }
                    else if (dr[i].GetType() == typeof(DateTime) || DateTime.TryParse(dr[i].ToString(), out dateResult))
                    {
                        ICellStyle cellStyle = Libro.CreateCellStyle();
                        if (rowHeader.Cells[i].StringCellValue.ToLower().Contains("hora"))
                            cellStyle.DataFormat = Libro.CreateDataFormat().GetFormat("HH:mm");
                        else
                            cellStyle.DataFormat = Libro.CreateDataFormat().GetFormat("dd/MM/yyyy");

                        cell.SetCellValue(DateTime.Parse(dr[i].ToString()));
                        cell.CellStyle = cellStyle;
                    }
                    else if (dr[i].GetType() == typeof(int))
                    {
                        cell.SetCellType(NPOI.SS.UserModel.CellType.NUMERIC);
                        cell.SetCellValue(int.Parse(dr[i].ToString()));
                    }
                    else if (dr[i].GetType() == typeof(bool))
                    {
                        cell.SetCellType(NPOI.SS.UserModel.CellType.BOOLEAN);
                        cell.SetCellValue(dr[i].ToString());
                    }
                    else
                    {
                        cell.SetCellType(NPOI.SS.UserModel.CellType.STRING);
                        cell.SetCellValue(dr[i].ToString());
                    }
                }
            }
            
            return Hoja;
        }

        private void CrearEncabezado(HSSFWorkbook Libro, HSSFSheet Hoja)
        {
            /********LOGO*********/
            string imagesPath = System.IO.Path.Combine(_pathImage, "logo.png");
            HSSFPatriarch patriarch = (HSSFPatriarch)Hoja.CreateDrawingPatriarch();            
            HSSFClientAnchor anchor = new HSSFClientAnchor(0, 0, 0, 255, 1, 1, 1, 2);
            anchor.AnchorType = 2;
            HSSFPicture picture = (HSSFPicture)patriarch.CreatePicture(anchor, LoadImage(imagesPath, Libro));            
            picture.Resize();
            
            CellRangeAddress region0 = new CellRangeAddress(0, 2, 0, 1);            
            Hoja.AddMergedRegion(region0);
            Hoja.SetEnclosedBorderOfRegion(region0, NPOI.SS.UserModel.BorderStyle.THIN, NPOI.HSSF.Util.HSSFColor.BLACK.index);
            
            /********TITULO*********/
            IRow row = Hoja.CreateRow(0);
            ICell cell = row.CreateCell(2);
            cell.SetCellValue(_title);

            ICellStyle style = Libro.CreateCellStyle();
            style.Alignment = HorizontalAlignment.CENTER;
            style.VerticalAlignment = VerticalAlignment.CENTER;       
            IFont font = Libro.CreateFont();
            font.FontHeight = 20 * 15;
            style.SetFont(font);
            cell.CellStyle = style;

            CellRangeAddress region = new CellRangeAddress(0, 2, 2, 6);
            Hoja.AddMergedRegion(region);            
            Hoja.SetEnclosedBorderOfRegion(region, NPOI.SS.UserModel.BorderStyle.THIN, NPOI.HSSF.Util.HSSFColor.BLACK.index);
            
            /*********FECHA*********/
            ICell cell2 = row.CreateCell(7);
            cell2.SetCellValue("Fecha Emisión: " + DateTime.Today.ToString("dd/MM/yyyy"));

            ICellStyle style2 = Libro.CreateCellStyle();
            style2.Alignment = HorizontalAlignment.CENTER;
            style2.VerticalAlignment = VerticalAlignment.CENTER;            
            IFont font2 = Libro.CreateFont();
            font2.FontHeight = 20*10;
            style2.SetFont(font2);
            cell2.CellStyle = style2;

            CellRangeAddress region2 = new CellRangeAddress(0, 2, 7, 10);
            Hoja.AddMergedRegion(region2);
            Hoja.SetEnclosedBorderOfRegion(region2, NPOI.SS.UserModel.BorderStyle.THIN, NPOI.HSSF.Util.HSSFColor.BLACK.index);
            
        }

        public static int LoadImage(string path, HSSFWorkbook wb)
        {
            FileStream file = new FileStream(path, FileMode.Open, FileAccess.Read);
            byte[] buffer = new byte[file.Length];
            file.Read(buffer, 0, (int)file.Length);
            return wb.AddPicture(buffer, PictureType.PNG);

        }
       
        #endregion
    }
}
