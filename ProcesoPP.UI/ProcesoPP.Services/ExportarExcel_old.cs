﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web.UI;
using System.Web;

namespace ProcesoPP.Services
{
    public class ExportarExcel
    {
        public static void exportarGrilla(GridView gvMovimientos, string fileName)
        {
            HttpResponse Response =  HttpContext.Current.Response;

            Response.ClearContent();
            Response.AddHeader("content-disposition", "attachment;filename="+ fileName + ".xls");
            Response.Charset = "ISO-8859-13";
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.ContentType = "application/vnd.xls";

            System.IO.StringWriter stringWrite = new System.IO.StringWriter();
            HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
            Page page = new Page();
            HtmlForm form = new HtmlForm();
            
            gvMovimientos.EnableViewState = false;
            page.EnableEventValidation = false;
            page.DesignerInitialize();
            gvMovimientos.Columns.RemoveAt(gvMovimientos.Columns.Count - 1);
            gvMovimientos.Columns.RemoveAt(gvMovimientos.Columns.Count - 1);
            gvMovimientos.Columns.RemoveAt(gvMovimientos.Columns.Count - 1);
           
            Label lbl = new Label();
            lbl.Text = "Fecha Reporte:  ";
            form.Controls.Add(lbl);
            lbl = new Label();
            lbl.Text = DateTime.Now.ToString("dd/MM/yyyy");
            form.Controls.Add(lbl);

            form.Controls.Add(gvMovimientos);
            page.Controls.Add(form);
            page.RenderControl(htmlWrite);
            Response.Write(stringWrite.ToString());
            Response.End();
        }
    }
}
