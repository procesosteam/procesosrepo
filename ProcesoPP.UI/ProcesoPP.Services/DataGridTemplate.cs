﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI;

namespace ProcesoPP.Services
{
    public class DataGridTemplate : System.Web.UI.Page, ITemplate
    {

        ListItemType templateType;
        string columnName;
        bool validarDec;
        System.Type _sistemType;

        public DataGridTemplate(ListItemType type, string colname, bool valiInt, System.Type systemType)
        {

            templateType = type;
            columnName = colname;
            validarDec = valiInt;
            _sistemType = systemType;
        }

        void ITemplate.InstantiateIn(System.Web.UI.Control container)
        {
            Label lbl;

            switch (templateType)
            {
                case ListItemType.Header:
                    //Creates a new label control and add it to the container.
                    lbl = new Label();            //Allocates the new label object.
                    lbl.Text = columnName;             //Assigns the name of the column in the lable.
                    container.Controls.Add(lbl);        //Adds the newly created label control to the container.
                    break;

                case ListItemType.Item:
                    lbl = new Label();            //Allocates the new label object.
                    lbl.DataBinding += new EventHandler(tb1_DataBinding);  //Assigns the name of the column in the lable.
                    container.Controls.Add(lbl);                         //Adds the newly created textbox to the container.
                    break;

                case ListItemType.EditItem:
                    //Creates a new text box control and add it to the container.
                    TextBox tb1 = new TextBox();                            //Allocates the new text box object.
                    tb1.DataBinding += new EventHandler(tb1_DataBinding);   //Attaches the data binding event.
                    if (validarDec)
                        tb1.Attributes.Add("onkeypress", "return ValidarNumeroDecimal(event,this,2);");
                    //tb1.Columns = 4;                                        //Creates a column with size 4.
                    container.Controls.Add(tb1);
                    break;

                case ListItemType.Footer:
                    CheckBox chkColumn = new CheckBox();
                    chkColumn.ID = "Chk" + columnName;
                    container.Controls.Add(chkColumn);
                    break;
            }
        }

        /// <summary>
        /// This is the event, which will be raised when the binding happens.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void tb1_DataBinding(object sender, EventArgs e)
        {
            switch (sender.GetType().Name)
            {
                case "TextBox":
                    TextBox txtdata = (TextBox)sender;
                    GridViewRow container = (GridViewRow)txtdata.NamingContainer;
                    object dataValue = DataBinder.Eval(container.DataItem, columnName);
                    if (dataValue != DBNull.Value)
                        txtdata.Text = dataValue.ToString();
                    
                    break;
                case "Label":
                    Label lbldata = (Label)sender;
                    container = (GridViewRow)lbldata.NamingContainer;
                    dataValue = DataBinder.Eval(container.DataItem, columnName);
                    if (dataValue != DBNull.Value)
                        lbldata.Text = dataValue.ToString();                    
                    break;
            }
                        
        }
    }
}