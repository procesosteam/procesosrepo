﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Mail;
using System.Net;

namespace ProcesoPP.Services
{
    public class EnvioMail
    {
        public string para { get; set; }
        public string para2 { get; set; }
        public string asunto { get; set; }
        public string cuerpo { get; set; }

        public string enviar()
        {
            string msj = "";
            MailMessage correo = new MailMessage();
            correo.To.Add(para);
            if (para2 != null)
                correo.To.Add(para2);
            correo.Subject = asunto;
            correo.Body = cuerpo;
            correo.IsBodyHtml = true;
            correo.Priority = MailPriority.Normal;

            SmtpClient smtp = new SmtpClient();
            //smtp.EnableSsl = true;
            smtp.UseDefaultCredentials = false;
            smtp.Credentials = new NetworkCredential("envios@procesospatagonicos.com.ar", "PPenvio2013");
            smtp.Host = "mail.procesospatagonicos.com.ar";
            smtp.Port = 25;
            
            try
            {
                smtp.Send(correo);
                msj = "Mensaje enviado satisfactoriamente";
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return msj;
        }
    }
}
