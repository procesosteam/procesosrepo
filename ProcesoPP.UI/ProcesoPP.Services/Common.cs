﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ProcesoPP.Model;
using System.IO;

namespace ProcesoPP.Services
{
    public class Common
    {
        public static void LimpiarControles(Control page)
        {
            foreach (Control ctrl in page.Controls)
            {
                if (ctrl is TextBox)
                    ((TextBox)ctrl).Text = string.Empty;
                else if (ctrl is DropDownList)
                    ((DropDownList)ctrl).SelectedIndex = 0;
                else
                {
                    foreach (Control child in ctrl.Controls)
                    {
                        LimpiarControles(child);
                    }
                }
                
            }
                                        
        }

        public static string encriptarURL(string strAEncr)
        {
           string encript = PAB.Utils.Encryption.Hex(strAEncr);

           return encript;
        }
        public static string desEncriptarURL(string strAEncr)
        {
            string encript = PAB.Utils.Encryption.DeHex(strAEncr);
            return encript;
        }

        public static void AnularControles(Control content)
        {

            if (content is TextBox)
                ((TextBox)content).Enabled = false;
            else if (content is DropDownList)
                ((DropDownList)content).Enabled = false;
            else if (content is RadioButton)
                ((RadioButton)content).Enabled = false;
            else if (content is Button)
                ((Button)content).Enabled = false;
            else if (content is FileUpload)
            {
                ((FileUpload)content).Enabled = false;
                ((FileUpload)content).Visible = false;
            }
            else
            {
                foreach (Control ctrl in content.Controls)
                {
                    AnularControles(ctrl);

                }
            }

        }

        public static Permisos getModulo(System.Web.HttpRequest Request, Page page)
        {
            try
            {
                int _idModulo;
                
                if (Request["idModulo"] != null)
                    _idModulo = int.Parse(Request["idModulo"]);
                    //_idModulo = int.Parse(desEncriptarURL(Request["idModulo"]));
                else
                    _idModulo = 0;

                Permisos oPermiso = new Permisos();
                if (page.Session["Permisos"] != null)
                {
                    List<Permisos> oPermisos = (List<Permisos>)page.Session["Permisos"];
                    if (!oPermisos.Exists(m => m.oModulo.idModulo == _idModulo))
                        oPermiso = null;
                    else
                        oPermiso = oPermisos.Find(m => m.oModulo.idModulo == _idModulo);

                }
                return oPermiso;
            }
            catch (Exception ex)
            {                
                throw ex;
            }
            
        }

        public static void CargarRow(GridViewRowEventArgs e, Permisos _Permiso)
        {
            if (!_Permiso.Escritura)
            {
                LinkButton btnEditar = e.Row.FindControl("btnEditar") as LinkButton;
                LinkButton btnEliminar = e.Row.FindControl("btnEliminar") as LinkButton;
                LinkButton btnModificar = e.Row.FindControl("btnModificar") as LinkButton;
                if (btnEditar != null)
                    btnEditar.Visible = false;
                if (btnEliminar != null)
                    btnEliminar.Visible = false;
                if (btnModificar != null)
                    btnModificar.Visible = false;
            }
        }

        public static int EvaluarSession(System.Web.HttpRequest Request, Page page)
        {
            int idUsuario = 0;
            if (page.Session["idUsuario"] != null)
            {
                idUsuario = int.Parse(page.Session["idUsuario"].ToString());
            }

            return idUsuario;
        }

        /// <summary>
        /// Devuelve la letra siguiente, si el parametro viene vacio devuelve siempre 'A'
        /// </summary>
        /// <param name="find">letra actual, para devolver la siguiente con respecto a esa</param>
        /// <returns></returns>
        public static string letra(string find, bool next)
        {
            List<string> oListLetras = new List<string>();
            oListLetras.Add("A");
            oListLetras.Add("B");
            oListLetras.Add("C");
            oListLetras.Add("D");
            oListLetras.Add("E");
            oListLetras.Add("F");
            oListLetras.Add("G");
            oListLetras.Add("H");
            oListLetras.Add("I");
            oListLetras.Add("J");
            oListLetras.Add("K");
            oListLetras.Add("L");
            oListLetras.Add("M");
            oListLetras.Add("N");
            oListLetras.Add("O");
            oListLetras.Add("P");
            oListLetras.Add("Q");
            oListLetras.Add("R");
            oListLetras.Add("S");
            oListLetras.Add("T");
            oListLetras.Add("U");
            oListLetras.Add("V");
            oListLetras.Add("W");
            oListLetras.Add("X");
            oListLetras.Add("Y");
            oListLetras.Add("Z");

            string str = oListLetras[0];
            if (find != string.Empty)
            {
                if (oListLetras.Exists(l => l == find))
                {
                    int ind = oListLetras.FindIndex(l => l == find);
                    if (ind < oListLetras.Count-1)
                        str = oListLetras[ind + 1];
                    else
                        str = oListLetras.Find(l => l == find);
                }
            }

            return str;
        }

        public static void LogError(Exception ex, string path)
        {
            string message = string.Format("Time: {0}", DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt"));
            message += Environment.NewLine;
            message += "-----------------------------------------------------------";
            message += Environment.NewLine;
            message += string.Format("Message: {0}", ex.Message);
            message += Environment.NewLine;
            message += string.Format("StackTrace: {0}", ex.StackTrace);
            message += Environment.NewLine;
            message += string.Format("Source: {0}", ex.Source);
            message += Environment.NewLine;
            message += string.Format("TargetSite: {0}", ex.TargetSite.ToString());
            message += Environment.NewLine;
            message += "-----------------------------------------------------------";
            message += Environment.NewLine;
            
            using (StreamWriter writer = new StreamWriter(path, true))
            {
                writer.WriteLine(message);
                writer.Close();
            }
        }

        public static void LogError(string ex, string path)
        {
            string message = string.Format("Time: {0}", DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt"));
            message += Environment.NewLine;
            message += "-----------------------------------------------------------";
            message += Environment.NewLine;
            message += string.Format("ERROR: {0}", ex);
            message += Environment.NewLine;
            message += "-----------------------------------------------------------";
            message += Environment.NewLine;
            //string path = "~/ErrorLog/ErrorLog.txt";
            using (StreamWriter writer = new StreamWriter(path, true))
            {
                writer.WriteLine(message);
                writer.Close();
            }
        }

    }


}
