﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using System.Web.UI;
using System.Web.UI.HtmlControls;

namespace ProcesoPP.Services
{
    public class Mensaje
    {
        public static void successMsj(string msg, Page page, string title, string redirect)
        {
            string sMsg = msg.Replace("\n", "\\n");
            sMsg = sMsg.Replace("\"", " ");
            sMsg = sMsg.Replace("'", " ");

            string s = "new Messi('" + sMsg + "', " +
                " {title: '" + title + "', modal: true, titleClass: 'success', " +
                " buttons: [{id: 0, label: 'Aceptar', val: 'Y'}]";
            if (redirect != null && redirect != string.Empty)
                s += ", callback: function(val) { window.location.href = '" + @redirect + "';}";

            s += "});";

            renderizarScript(s, page);
        }

        public static void successMsjOpen(string msg, Page page, string title, string redirect)
        {
            string sMsg = msg.Replace("\n", "\\n");
            sMsg = sMsg.Replace("\"", " ");
            sMsg = sMsg.Replace("'", " ");
            
            redirect = redirect.Replace("\n", "\\n");

            string s = "new Messi('" + sMsg + "', " +
                " {title: '" + title + "', modal: true, titleClass: 'success', " +
                " buttons: [{id: 0, label: 'Aceptar', val: 'Y'}]";
            if (redirect != null && redirect != string.Empty)
                s += ", callback: function(val) { window.open('" + @redirect + "');}";

            s += "});";

            renderizarScript(s, page);
        }

        public static void errorMsj(string msg, Page page, string title, string redirect)
        {
            string sMsg = msg.Replace("\n", "\\n");
            sMsg = sMsg.Replace("\r", " ");
            sMsg = sMsg.Replace("\"", " ");
            sMsg = sMsg.Replace("'", " ");
            string s = "new Messi('" + sMsg + "', " +
                " {title: '" + @title + "',modal: true, titleClass: 'error', " +
                " buttons: [{id: 0, label: 'Aceptar', val: 'Y'}]";
            if (redirect != null && redirect != string.Empty)
                s += ", callback: function(val) { window.location.href = '" + @redirect + "';}";

            s += "});";

            renderizarScript(s, page);
        }

        public static void preguntaMsj(string msg, Page page, string title, string SIredirect)
        {
            string sMsg = msg.Replace("\n", "\\n");
            if (SIredirect != null)
                SIredirect = SIredirect.Replace(@"\", @"\\");

            sMsg = sMsg.Replace("\r", "\\n");
            sMsg = sMsg.Replace("'", " ");
            //string s = "new Messi('" + sMsg + "', " +
            //    " {title: '" + title + "', modal: true, titleClass: 'success', " +
            //    " buttons: [" +
            //               "{ id:0, label: 'Si', val: 'Y', 'class': 'btn-success'}," +
            //               "{ id:1, label: 'No', val: 'N', 'class': 'btn-danger'} " +
            //               "]" +
            //    ", callback: function(val) { alert('Your selection: ' + val);}"; // if(val == 'S'){window.location.href = '" + SIredirect + "';};}";

            string s = "preguntar('" + sMsg + "','" + SIredirect + "');";
            
           // s += "});";

            //renderizarScript(s, page);
            page.ClientScript.RegisterStartupScript(page.GetType(), "error", s,true);
        }

        public static void openAcuerdo(Page page, string SIredirect)
        {
            //string sMsg = msg.Replace("\n", "\\n");
            SIredirect = SIredirect.Replace("\n", "\\n");

            string s = "window.open('" + SIredirect + "');";
            page.ClientScript.RegisterStartupScript(page.GetType(), "open", s, true);
        }

        private static void renderizarScript(string s, Page page)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(@"<script type='text/javascript'>");
            sb.Append(@s);
            sb.Append(@"</script>");

            page.ClientScript.RegisterStartupScript(page.GetType(), "error", sb.ToString());
        }

        public static void Eliminar(string descripcion, Page page)
        {
             string sMsg = descripcion.Replace("\n", "\\n");
             sMsg = descripcion.Replace("\"", " ");
             sMsg = descripcion.Replace("'", " ");
             string oMesi = "new Messi('Desea eliminar '" + descripcion + "'?'," +
                          " { title: 'Pregunta', modal: true, " +
                          " buttons: [{ id: 0, label: 'Si', val: 'true' }, " +
                          "           { id: 1, label: 'No', val: 'false'}], " +
                          " callback: function(val) { return val; }" +
                          "});";

             renderizarScript(oMesi, page);
        }
        /// <summary>
        /// Invisibiliza el div que se pasa por parametro
        /// </summary>
        /// <param name="divMensaje"></param>
        public static void ocultarDivMsj(HtmlGenericControl divMensaje)
        {
            divMensaje.Visible = false;
        }

        /// <summary>
        /// Abre el pop up mostrando los trailers disponible, si hay
        /// </summary>
        /// <param name="page"></param>
        public static void mostrarPopUp( Page page)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(@"<script language='javascript'>");
            sb.Append(@"abrirPopUp();");
            sb.Append(@"</script>");

            page.ClientScript.RegisterStartupScript(page.GetType(), "starScript", sb.ToString());
                
        }


        public static void preguntaTrailerMsj(string msg, Page page, string title, string SIredirect)
        {
            string sMsg = msg.Replace("\n", "\\n");
            if (SIredirect != null)
                SIredirect = SIredirect.Replace(@"\", @"\\");

            sMsg = sMsg.Replace("\r", "\\n");
            sMsg = sMsg.Replace("'", " ");
            //string s = "new Messi('" + sMsg + "', " +
            //    " {title: '" + title + "', modal: true, titleClass: 'success', " +
            //    " buttons: [" +
            //               "{ id:0, label: 'Si', val: 'Y', 'class': 'btn-success'}," +
            //               "{ id:1, label: 'No', val: 'N', 'class': 'btn-danger'} " +
            //               "]" +
            //    ", callback: function(val) { alert('Your selection: ' + val);}"; // if(val == 'S'){window.location.href = '" + SIredirect + "';};}";

            string s = "preguntarTrailer('" + sMsg + "','" + SIredirect + "');";

            // s += "});";

            //renderizarScript(s, page);
            page.ClientScript.RegisterStartupScript(page.GetType(), "error", s, true);
        }
    }
}
