﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="frmLogin.aspx.cs" Inherits="ProcesoPP.UI.frmLogin" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>LOGIN</title>    
    <link href="Css/estilos.css" rel="stylesheet" type="text/css"/>
    <link href="Css/messi.min.css" rel="stylesheet" type="text/css" />
    <link href="Css/messi.css" rel="stylesheet" type="text/css" />
    
    <script src="Js/jquery-1.10.1.min.js" type="text/javascript"></script>
    <script src="Js/messi.min.js" type="text/javascript"></script>
    <script src="Js/messi.js" type="text/javascript"></script>
    <script src="Js/Validadores.js" type="text/javascript"></script>
    <script src="Js/Funciones.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">    
        <div id="logintxt">INICIO DE SESION</div>     
        <div class="logintxt2">
            <label>Usuario:</label>
        </div>
        <div class="logintxt3">
            <asp:TextBox runat="server" CssClass="login_campo" ID="txtNombre"></asp:TextBox></div><div class="loginrelleno"></div>
        <div class="logintxt2">
          <label>Contraseña:</label>
        </div>
        <div class="logintxt3">
            <asp:TextBox runat="server" CssClass="login_campo" ID="txtClave" TextMode="Password"></asp:TextBox></div>
            <div class="loginrelleno"></div>
        <div class="logintxt3">
        <asp:LinkButton runat="server" CssClass="login_campo2" ID="lnkContra" Text="¿Olvido la contraseña?"  PostBackUrl="~/Administracion/frmOlvidoContrasenia.aspx"/></div>
        
        <div class="logintxt5">
             <asp:Button runat="server" ID="btn_form" CssClass="btn_form" Text="Iniciar Sesión" OnClientClick="return ValidarObj('txtNombre|Debe ingresar el Nombre de Usuario.~txtClave|Debe ingresar la Clave.');"
                OnClick="btnGuardar_Click" /></div><div class="loginrelleno"></div>
    </form>
</body>
</html>
