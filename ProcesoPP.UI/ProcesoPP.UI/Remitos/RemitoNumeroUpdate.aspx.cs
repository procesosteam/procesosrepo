﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ProcesoPP.Services;
using ProcesoPP.Model;
using System.Configuration;
using ProcesoPP.Business;

namespace ProcesoPP.UI.Remitos
{
    public partial class RemitoNumeroUpdate : System.Web.UI.Page
    {
        public int _idUsuario
        {
            get { return (int)ViewState["idUsuario"]; }
            set { ViewState["idUsuario"] = value; }
        }
        public Permisos _Permiso
        {
            get { return (Permisos)ViewState["permiso"]; }
            set { ViewState["permiso"] = value; }
        }
        public int _ADM
        {
            get { return int.Parse(ConfigurationManager.AppSettings["ADM"].ToString()); }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                _idUsuario = Common.EvaluarSession(Request, this.Page);
                if (_idUsuario != 0)
                {
                    _Permiso = Common.getModulo(Request, this.Page);
                    if (!IsPostBack)
                    {
                        if (_Permiso != null && _Permiso.Lectura)
                        {
                            if (_Permiso.oTipoUsuario.idTipoUsuario == _ADM)
                            {
                                btnGuardar.Visible = true;
                            }
                            else
                            {
                                btnGuardar.Visible = false;
                            }
                            getNroRemito();
                        }
                        else
                        {
                            Mensaje.errorMsj("NO TIENE PERMISOS A ESTE FORMULARIO", this.Page, "SIN ACCESO", "../frmPrincipal.aspx");
                        }
                    }
                }
                else
                {
                    Mensaje.errorMsj("DEBE LOGUEARSE", this.Page, "LOGIN", "../frmLogin.aspx");
                }
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message + "\nSource: " + ex.Source , this.Page, "Error", null);
            }
        }

        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                Guardar();
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        private void Guardar()
        {
            RemitosBus oRemBus = new RemitosBus();
            if (oRemBus.RemitoNumeroUpdate(int.Parse(txtNumero.Text)))
            {
                Mensaje.successMsj("Numero remito actualizado", this, "Nro. Remito", null);
            }
            else
            {
                Mensaje.errorMsj("No se actualizo Nro.", this.Page, "Error", null);
            }
        }


        private void getNroRemito()
        {
            RemitosBus oRemBus = new RemitosBus();
            txtNumero.Text = oRemBus.RemitoGetNroRemito().ToString();

        }

    }
}
