﻿<%@ Page Title="" Language="C#" MasterPageFile="~/frmPrincipal.Master" AutoEventWireup="true" CodeBehind="frmVerificacionRegreso.aspx.cs" Inherits="ProcesoPP.UI.Regreso.frmVerificacionRegreso" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div class="tabla3" id="divMensaje" runat="server"><asp:Label ID="lblError" CssClass="error2" runat="server" Text="Error" Visible="false"></asp:Label></div>
<div id="contenido" runat="server" >
<div id="apertura">Verificación de IDA.</div>
<div class="solicitud100">
    <div class="sol_tit2"><asp:Label ID="Label1" runat="server" name="NroParte" Text="Parte Nº:" /></div>
    <div class="sol_campob"><asp:Label ID="lblNroParte" name="NroParte" runat="server"/></div>  
    <div class="sol_tit2"><asp:Label ID="Label2" runat="server" Text="Trailer:" /></div>
    <div class="sol_campob"><asp:Label ID="lblTrailer"  runat="server"/></div>
      
    <div class="sol_tit2"><asp:Label ID="Label3" runat="server" Text="SS Nº"/></div>
    <div class="sol_campob"><asp:Label id="lblNroSS"  runat="server"/></div>
</div>
<div class="solicitud100">
    <div class="sol_tit"><asp:Label ID="Label4" runat="server" Text="Hs Salida:" /></div>
    <div class="sol_campo"><asp:TextBox id="txtHsSalida"  runat="server"/></div>
    <div  class="sol_fecha"><img id="btnFecha" alt="calendario" src="../img/calendario.gif" style="width:20px; height:20px; cursor:pointer;" onmouseover="CambiarConfiguracionCalendario('txtHsSalida',this);"/></div>
    
    <div class="sol_tit"><asp:Label ID="Label5" runat="server" Text="Hs Regreso:"/></div>
    <div class="sol_campo"><asp:TextBox id="txtHSRegreso" runat="server"/></div>
    <div  class="sol_fecha"><img id="Img1" alt="calendario" src="../img/calendario.gif" style="width:20px; height:20px; cursor:pointer;" onmouseover="CambiarConfiguracionCalendario('txtHSRegreso',this);"/></div>
    
</div>
<div runat="server" id="divVerificacion">
<div id="titulos">Detalle Exterior</div>
<div class="solicitud100">
    <div class="campo"><asp:Label ID="Label6" runat="server" name="Lanza" Text="Lanza"/></div>
    <div class="columnav"><asp:TextBox ID="txtLanza" name="Lanza" runat="server" onkeypress="return ValidarNumeroEntero(event,this);" /></div>

    <div class="campo"><asp:Label ID="Label7" runat="server" name="Cajon HTA" Text="Cajón HTA"/></div>
    <div class="columnav"><asp:TextBox runat="server" name="Cajon HTA" id="txtHTA" onkeypress="return ValidarNumeroEntero(event,this);"/></div>

    <div class="campo"><asp:Label ID="Label8" runat="server" name="Ojal" Text="Ojal"/></div>
    <div class="columnav"><asp:TextBox runat="server" name="Ojal" id="txtOjal" onkeypress="return ValidarNumeroEntero(event,this);"/></div>
</div>
<div class="solicitud100">
    <div class="campo"><asp:Label ID="Label9" runat="server" name="Reventim" Text="Revestim."/></div>
    <div class="columnav"><asp:TextBox  runat="server" name="Reventim" id="txtRev" onkeypress="return ValidarNumeroEntero(event,this);" /></div>

    <div class="campo"><asp:Label ID="Label10" runat="server" name="Pinza" Text="Pinza"/></div>
    <div class="columnav"><asp:TextBox  runat="server" name="Pinza" id="txtPinza" onkeypress="return ValidarNumeroEntero(event,this);" /></div>

    <div class="campo"><asp:Label ID="Label11" runat="server" name="Escalera" Text="Escalera"/></div>
    <div class="columnav"><asp:TextBox  runat="server" name="Escalera" id="txtEscalera" onkeypress="return ValidarNumeroEntero(event,this);" /></div>
</div>
<div class="solicitud100">
    <div class="campo"><asp:Label ID="Label12" runat="server" name="Plato" Text="Plato"/></div>
    <div class="columnav"><asp:TextBox  runat="server" name="Plato" id="txtPlato" onkeypress="return ValidarNumeroEntero(event,this);" /></div>

    <div class="campo"><asp:Label ID="Label13" runat="server" name="Descanso" Text="Descanso"/></div>
    <div class="columnav"><asp:TextBox  runat="server" name="Descanso" id="txtDescanso" onkeypress="return ValidarNumeroEntero(event,this);" /></div>

    <div class="campo"><asp:Label ID="Label14" runat="server" name="Barandas" Text="Barandas"/></div>
    <div class="columnav"><asp:TextBox  runat="server" name="Barandas" id="txtBarandas" onkeypress="return ValidarNumeroEntero(event,this);" /></div>
</div>
<div class="solicitud100">
    <div class="campo"><asp:Label ID="Label15" runat="server" name="Ejes" Text="Ejes"/></div>
    <div class="columnav"><asp:TextBox  runat="server" name="Ejes" id="txtEjes" onkeypress="return ValidarNumeroEntero(event,this);" /></div>

    <div class="campo"><asp:Label ID="Label16" runat="server" name="Soporte Esc." Text="Soporte Esc."/></div>
    <div class="columnav"><asp:TextBox  runat="server" name="Soporte Esc." id="txtSopor" onkeypress="return ValidarNumeroEntero(event,this);" /></div>

    <div class="campo"><asp:Label ID="Label17" runat="server" name="Estacas" Text="Estacas"/></div>
    <div class="columnav"><asp:TextBox runat="server" name="Estacas" id="txtEstaca" onkeypress="return ValidarNumeroEntero(event,this);" /></div>
</div>
<div class="solicitud100">
    <div class="campo"><asp:Label ID="Label18" runat="server" name="Elasticos" Text="Elasticos"/></div>
    <div class="columnav"><asp:TextBox runat="server" name="Elasticos" id="txtElasticos" onkeypress="return ValidarNumeroEntero(event,this);" /></div>

    <div class="campo"><asp:Label ID="Label19" runat="server" name="Mazas" Text="Mazas"/></div>
    <div class="columnav"><asp:TextBox runat="server" name="Mazas" id="txtMazas" onkeypress="return ValidarNumeroEntero(event,this);" /></div>

    <div class="campo"><asp:Label ID="Label20" runat="server" name="Luces Tran." Text="Luces Tran."/></div>
    <div class="columnav"><asp:TextBox runat="server" id="txtLuces" name="Luces Tran." onkeypress="return ValidarNumeroEntero(event,this);"/></div>
</div>
<div id="titulos">Detalle Interior</div>

<div class="solicitud100">
    <div id="pe_campo">Dormitorio</div>
    <div class="campo"><asp:Label ID="Label21" runat="server" Text="Camas" name="Camas"/></div>
    <div class="columnav"><asp:TextBox  runat="server" id="txtCamas" name="Camas" onkeypress="return ValidarNumeroEntero(event,this);"/></div>

    <div class="campo"><asp:Label ID="Label22" runat="server" Text="Colchones" name="Colchones"/></div>
    <div class="columnav"><asp:TextBox  runat="server" id="txtColchones" name="Colchones" onkeypress="return ValidarNumeroEntero(event,this);"/></div>
</div>
<div class="solicitud100">
    <div id="pe_spacer"></div>
    <div class="campo"><asp:Label ID="Label23" runat="server" Text="Almohadas" name="Almohadas"/></div>
    <div class="columnav"><asp:TextBox  runat="server" id="txtAlmohadas" name="Almohadas" onkeypress="return ValidarNumeroEntero(event,this);"/></div>

    <div class="campo"><asp:Label ID="Label24" runat="server" Text="Cortinas" name="Cortinas"/></div>
    <div class="columnav"><asp:TextBox  runat="server" id="txtCortinas" name="Cortinas" onkeypress="return ValidarNumeroEntero(event,this);"/></div>
</div>
<div class="solicitud100">

    <div id="pe_spacer"></div>
    <div class="campo"><asp:Label ID="Label25" runat="server" Text="Veladores" name="Veladores"/></div>
    <div class="columnav"><asp:TextBox  runat="server" id="txtVeladores" name="Veladores" onkeypress="return ValidarNumeroEntero(event,this);"/></div>

    <div class="campo"><asp:Label ID="Label26" runat="server" Text="Placard" name="Placard"/></div>
    <div class="columnav"><asp:TextBox  runat="server" id="txtPlacard" name="Placard" onkeypress="return ValidarNumeroEntero(event,this);"/></div>
</div>
<div class="solicitud100">
    <div id="pe_campo">Baño</div>
    <div class="campo"><asp:Label ID="Label27" runat="server" Text="Inodoro" name="Inodoro"/></div>
    <div class="columnav"><asp:TextBox  runat="server" id="txtInodoro" name="Inodoro" onkeypress="return ValidarNumeroEntero(event,this);"/></div>

    <div class="campo"><asp:Label ID="Label28" runat="server" Text="Ducha" name="Ducha"/></div>
    <div class="columnav"><asp:TextBox  runat="server" id="txtDucha" name="Ducha" onkeypress="return ValidarNumeroEntero(event,this);"/></div>
</div>
<div class="solicitud100">
    <div id="pe_spacer"></div>
    <div class="campo"><asp:Label ID="Label29" runat="server" Text="Lavatorio" name="Lavatorio"/></div>
    <div class="columnav"><asp:TextBox  runat="server" id="txtLavatorio" name="Lavatorio" onkeypress="return ValidarNumeroEntero(event,this);"/></div>
    <div class="campo"><asp:Label ID="Label30" runat="server" Text="--"/></div>
    <div class="columnav"><asp:TextBox  runat="server" id="txt" onkeypress="return ValidarNumeroEntero(event,this);" /></div>
</div>
<div class="solicitud100">
    <div id="pe_campo">Comedor</div>
    <div class="campo"><asp:Label ID="Label31" runat="server" Text="Cocina" name="Cocina"/></div>
    <div class="columnav"><asp:TextBox  runat="server" id="txtCocina" name="Cocina" onkeypress="return ValidarNumeroEntero(event,this);" /></div>

    <div class="campo"><asp:Label ID="Label32" runat="server" Text="Horno Elect." name="Horno Elect."/></div>
    <div class="columnav"><asp:TextBox  runat="server" id="txtHorno" name="Horno Elect." onkeypress="return ValidarNumeroEntero(event,this);"/></div>
</div>
<div class="solicitud100">
    <div id="pe_spacer"></div>
    <div class="campo"><asp:Label ID="Label33" runat="server" Text="Extractor" name="Extractor"/></div>
    <div class="columnav"><asp:TextBox  runat="server" id="txtExtractor" name="Extractor" onkeypress="return ValidarNumeroEntero(event,this);"/></div>

    <div class="campo"><asp:Label ID="Label34" runat="server" Text="Cubiertos" name="Cubiertos"/></div>
    <div class="columnav"><asp:TextBox  runat="server" id="txtCubiertos" name="Cubiertos" onkeypress="return ValidarNumeroEntero(event,this);"/></div>
</div>
<div class="solicitud100">
    <div id="pe_spacer"></div>
    <div class="campo"><asp:Label ID="Label35" runat="server" Text="Bancos" name="Bancos"/></div>
    <div class="columnav"><asp:TextBox  runat="server" id="txtBancos" name="Bancos" onkeypress="return ValidarNumeroEntero(event,this);"/></div>

    <div class="campo"><asp:Label ID="Label36" runat="server" Text="Sillas" name="Sillas"/></div>
    <div class="columnav"><asp:TextBox  runat="server" id="txtSillas" name="Sillas" onkeypress="return ValidarNumeroEntero(event,this);"/></div>
</div>
<div class="solicitud100">
    <div id="pe_spacer"></div>
    <div class="campo"><asp:Label ID="Label37" runat="server" Text="Mesa" name="Mesa"/></div>
    <div class="columnav"><asp:TextBox  runat="server" id="txtxMesa" name="Mesa" onkeypress="return ValidarNumeroEntero(event,this);"/></div>

    <div class="campo"><asp:Label ID="Label38" runat="server" Text="Termotanque" name="Termotanque"/></div>
    <div class="columnav"><asp:TextBox  runat="server" id="txtTermo" name="Termotanque" onkeypress="return ValidarNumeroEntero(event,this);"/></div>
</div>
<div class="solicitud100">
    <div id="pe_spacer"></div>
    <div class="campo"><asp:Label ID="Label39" runat="server" Text="Split" name="Split"/></div>
    <div class="columnav"><asp:TextBox ID="TextBox1"  runat="server" class="txtSplit" name="Split" onkeypress="return ValidarNumeroEntero(event,this);"/></div>
    <div class="campo"><asp:Label ID="Label40" runat="server" Text="Caloventor" name="Caloventor"/></div>
    <div class="columnav"><asp:TextBox  runat="server" id="txtCaloventor" name="Caloventor" onkeypress="return ValidarNumeroEntero(event,this);"/></div>
</div>
<div class="solicitud100">
    <div id="pe_campo">Oficina</div>
    <div class="campo"><asp:Label ID="Label41" runat="server" Text="Escritorio" name="Escritorio"/></div>
    <div class="columnav"><asp:TextBox  runat="server" id="txtEscritorio" name="Escritorio" onkeypress="return ValidarNumeroEntero(event,this);" /></div>
    <div class="campo"><asp:Label ID="Label42" runat="server" Text="Biblioteca" name="Biblioteca"/></div>
    <div class="columnav"><asp:TextBox  runat="server" id="txtBiblio" name="Biblioteca" onkeypress="return ValidarNumeroEntero(event,this);"/></div>
</div>
<div class="solicitud100">
    <div id="pe_spacer"></div>
    <div class="campo"><asp:Label ID="Label43" runat="server" Text="Sillas" name="SillasOf"/></div>
    <div class="columnav"><asp:TextBox  runat="server" id="txtSillasOf" name="SillasOf" onkeypress="return ValidarNumeroEntero(event,this);"/></div>
    <div class="campo"><asp:Label ID="Label44" runat="server" Text="--"/></div>
    <div class="columnav"><asp:TextBox  runat="server" id="txt2" onkeypress="return ValidarNumeroEntero(event,this);" /></div>
</div>
<div class="solicitud100">
    <div id="pe_campo">Conexión</div>
    <div class="campo"><asp:Label ID="Label45" runat="server" Text="Manguera" name="Manguera"/></div>
    <div class="columnav"><asp:TextBox  runat="server" id="txtManguera" name="Manguera" onkeypress="return ValidarNumeroEntero(event,this);" /></div>
    <div class="campo"><asp:Label ID="Label46" runat="server" Text="Tee" name="Tee"/></div>
    <div class="columnav"><asp:TextBox  runat="server" id="txtTee" name="Tee" onkeypress="return ValidarNumeroEntero(event,this);"/></div>
</div>
<div class="solicitud100">
    <div id="pe_spacer"></div>
    <div class="campo"><asp:Label ID="Label47" runat="server" Text="Caño 110" name="Caño 110"/></div>
    <div class="columnav"><asp:TextBox  runat="server" id="txtCanio" name="Caño 110" onkeypress="return ValidarNumeroEntero(event,this);"/></div>
    <div class="campo"><asp:Label ID="Label48" runat="server" Text="Cble Sin. 3x4" name="Cable sin"/></div>
    <div class="columnav"><asp:TextBox  runat="server" id="txtCableSin" name="Cable sin" onkeypress="return ValidarNumeroEntero(event,this);" /></div>
</div>
<div class="solicitud100">
    <div id="pe_spacer"></div>
    <div class="campo"><asp:Label ID="Label49" runat="server" Text="Codo" name="Codo"/></div>
    <div class="columnav"><asp:TextBox  runat="server" id="txtCodo" name="Codo" onkeypress="return ValidarNumeroEntero(event,this);"/></div>
    <div class="campo"><asp:Label ID="Label50" runat="server" Text="Fichas" name="Fichas"/></div>
    <div class="columnav"><asp:TextBox  runat="server" id="txtFichas" name="Fichas" onkeypress="return ValidarNumeroEntero(event,this);" /></div>
</div>
<div class="solicitud100">
    <div id="pe_spacer"></div>
    <div class="campo"><asp:Label runat="server" Text="Curvas" name="Curvas"/></div>
    <div class="columnav"><asp:TextBox  runat="server" id="txtCurvas" name="Curvas" onkeypress="return ValidarNumeroEntero(event,this);"/></div>
    <div class="campo"><asp:Label runat="server" Text="Cable Masa" name="Cable Masa"/></div>
    <div class="columnav"><asp:TextBox  runat="server" id="txtCableMasa" name="Cable Masa" onkeypress="return ValidarNumeroEntero(event,this);"/></div>
</div>
<div class="solicitud100">
    <div id="pe_campo">Adicionales</div>
    <div class="celda50"><asp:TextBox  runat="server" name="Adicionales" id="txtAdicional"/></div>
</div>
</div>
<div id="botones"> 
    <div class="regreso_3"> 
        <asp:Button runat="server" class="btn_form" Text="GUARDAR" ID="btnGuardar" 
            onclick="btnGuardar_Click"></asp:Button>
    </div>   
    <div class="regreso_3">
        <input type="button" id="btnVolver" value="VOLVER" class="btn_form" onclick="Volver();"/>
    </div> 
    <div class="regreso_3">
        <asp:Button runat="server" ID="btnImprimir" CssClass="btn_form" Text="IMPRIMIR" 
            onclick="btnImprimir_Click"/></div>
</div>

</div>

</asp:Content>
