﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ProcesoPP.Business;
using ProcesoPP.Model;
using System.Data;
using ProcesoPP.Services;
using System.Configuration;

namespace ProcesoPP.UI.Remitos
{
    public partial class frmRemitosCarga : System.Web.UI.Page
    {
        #region Propiedades

        public int _idRemito
        {
            get { return (int)ViewState["idRemito"]; }
            set { ViewState["idRemito"] = value; }
        }
        public int _idParte
        {
            get { return (int)ViewState["idParte"]; }
            set { ViewState["idParte"] = value; }
        }

        public string _movimiento
        {
            get { return (string)ViewState["detalle"]; }
            set { ViewState["detalle"] = value; }
        }

        public string _cantMovimiento
        {
            get { return (string)ViewState["cantMovimiento"]; }
            set { ViewState["cantMovimiento"] = value; }
        }

        public string _alquiler
        {
            get { return (string)ViewState["alquiler"]; }
            set { ViewState["alquiler"] = value; }
        }
        public string _cantAlquiler
        {
            get { return (string)ViewState["cantAlquiler"]; }
            set { ViewState["cantAlquiler"] = value; }
        }

        public string _conn
        {
            get { return (string)ViewState["conn"]; }
            set { ViewState["conn"] = value; }
        }
        public string _accion
        {
            get { return (string)ViewState["accion"]; }
            set { ViewState["accion"] = value; }
        }
        public string _des
        {
            get { return (string)ViewState["des"]; }
            set { ViewState["des"] = value; }
        }

        private DataTable _dt
        {
            get { return (DataTable)ViewState["dt"]; }
            set { ViewState["dt"] = value; }
        }

        public Permisos _Permiso
        {
            get { return (Permisos)ViewState["permiso"]; }
            set { ViewState["permiso"] = value; }
        }

        public int _idUsuario
        {
            get { return (int)ViewState["idUsuario"]; }
            set { ViewState["idUsuario"] = value; }
        }
        public int _ADM
        {
            get { return int.Parse(ConfigurationManager.AppSettings["ADM"].ToString()); }
        }
        #endregion
        
        #region Eventos
        
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                 _idUsuario = Common.EvaluarSession(Request, this.Page);
                 if (_idUsuario != 0)
                 {
                     _Permiso = Common.getModulo(Request, this.Page);
                     if (!IsPostBack)
                     {
                         InitScreen();
                     }
                 }
                 else
                 {
                     Mensaje.errorMsj("DEBE LOGUEARSE", this.Page, "LOGIN", "../frmLogin.aspx");
                 }
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        protected void gvDetalles_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "addRow":
                    addRow(e);
                    break;
                case "deleteRow":
                    deleteRow(e);
                    break;
            }
        }

        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                GuardarRemito();
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message + "\nSource: " + ex.Source + "\nMétodo: " + ex.TargetSite, this.Page, "Error", null);
            }
        }

        protected void gvDetalles_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                TextBox txtCantidad = e.Row.FindControl("txtCantidad") as TextBox;
                TextBox txtDetalle = e.Row.FindControl("txtDetalle") as TextBox;
                DropDownList ddlCodTango = e.Row.FindControl("ddlCodTangoI") as DropDownList;
                HiddenField hfCodTango = e.Row.FindControl("hfCodTango") as HiddenField;
                if (txtDetalle.Text.Split('\n').Count() > 3)
                    txtDetalle.Rows = txtDetalle.Text.Split('\n').Count();
                txtCantidad.Rows = txtDetalle.Rows;
                CargarCodTango(ddlCodTango, txtDetalle.Text);
                ddlCodTango.SelectedValue = hfCodTango.Value;
            }
            if (e.Row.RowType == DataControlRowType.Footer)
            {
                DropDownList ddlCodTango = e.Row.FindControl("ddlCodTango") as DropDownList;
                CargarCodTango(ddlCodTango, "");
            }
        }

        #endregion

        #region Metodos

        private void CargarCodTango(DropDownList ddlCodTango, string detalle)
        {
            TipoServicioBus oTsBus = new TipoServicioBus();
            if (detalle.Length > 0)
            {
                if (detalle.IndexOf("servicio") > 1)
                    detalle = detalle.Substring(detalle.IndexOf(":") + 2, 4);
                else
                    detalle = detalle.Substring(0, 4);
            }
            ddlCodTango.DataSource = oTsBus.CodTangoGetByFilter(detalle);
            ddlCodTango.DataTextField = "Descripcio";
            ddlCodTango.DataValueField = "cod_articu";
            ddlCodTango.DataBind();
            ddlCodTango.Items.Insert(0, new ListItem("Seleccione...", "0"));
        }

        private void InitScreen()
        {
            _idRemito = 0;
            _idParte = 0;
            _des = string.Empty;
            _conn = string.Empty;
            _movimiento = string.Empty;
            _alquiler = string.Empty;
            _accion = string.Empty;

            CrearDT();
            if (Request["idParte"] != null)
            {
                _idParte = int.Parse(Request["idParte"]);
                CargarRemitoPorParte();
                gvDetalles.Columns[0].Visible = true;
            }
            if (Request["accion"] != null)
            {
                _accion = Request["accion"];
            }
            if (_accion == "Ver")
            {
                btnGuardar.Visible = false;
            } 
            if (Request["idRemito"] != null && int.Parse(Request["idRemito"]) != 0)
            {
                _idRemito = int.Parse(Request["idRemito"]);
                CargarRemitoById();
                gvDetalles.Columns[0].Visible = false;
            }
            else
            {
                SugerirNroRemito();               
                CargarGrillaDetalle();
            }
            if(_accion.Equals("Bonificado"))
                gvDetalles.Columns[0].Visible = false;

        }

        private void CargarRemitoById()
        {
            Model.Remitos oRemito = new ProcesoPP.Model.Remitos();
            RemitosBus oRemitosBus = new RemitosBus();
            oRemito = oRemitosBus.RemitosGetById(_idRemito);
            txtFecha.Text = oRemito.Fecha.ToString("dd/MM/yyyy");
            txtObservacion.Text = oRemito.Observacion;
            lblNroRemito.Text = oRemito.NroRemito.ToString();
            txtCCosto.Text = oRemito.CCosto;
            txtNroOC.Text = oRemito.NroOC;

            foreach (RemitoDetalle oDetalle in oRemito.oListDetalles)
            {
                DataRow dr = _dt.NewRow();
                dr["idRemitoDetalle"] = oDetalle.IdRemitoDetalle;
                dr["Cantidad"] = oDetalle.Cantidad;
                dr["detalle"] = oDetalle.Detalle;
                dr["PU"] = oDetalle.PrecioUnit;
                dr["Total"] = oDetalle.PrecioTotal;
                _dt.Rows.Add(dr);
            }
            if (oRemito.Impreso && _Permiso.oTipoUsuario.idTipoUsuario == _ADM)
            {
                btnGuardar.Visible = true;
                btnGuardar.Text = "Re-Imprimir";
            }
            BindGrid();
        }

        private void GuardarRemito()
        {
            Model.Remitos oRemito = new ProcesoPP.Model.Remitos();
            RemitoDetalle oDetalle = new RemitoDetalle();
            RemitosBus oRemitosBus = new RemitosBus();
            ParteEntregaBus oParteEntregaBus = new ParteEntregaBus();
            DateTime ultimaFecha = oRemitosBus.RemitosGetUltimaFecha();
            List<RemitoDetalleTango> oListDetTango = new List<RemitoDetalleTango>();

            oRemito.oParte.IdParteEntrega = _idParte;
            oRemito.oParte = oParteEntregaBus.ParteEntregaGetById(_idParte);
            oRemito.IdUsuario = _idUsuario;
            oRemito.Impreso = false;
            oRemito.Anulado = false;
            oRemito.NroRemito = int.Parse(lblNroRemito.Text);
            oRemito.Observacion = txtObservacion.Text.ToUpper();
            oRemito.Cantidad = 1;
            oRemito.Fecha = DateTime.Parse(txtFecha.Text);
            oRemito.NroOC = txtNroOC.Text;
            oRemito.CCosto = txtCCosto.Text;

            int nRenglon = 1;
            
            foreach (GridViewRow row in gvDetalles.Rows)
            {

                HiddenField hfIdRemitoDetalle = row.FindControl("hfIdRemitoDetalle") as HiddenField;
                TextBox txtCantidad = row.FindControl("txtCantidad") as TextBox;
                TextBox txtDetalle = row.FindControl("txtDetalle") as TextBox;
                TextBox txtPU = row.FindControl("txtPU") as TextBox;
                TextBox txtTotal = row.FindControl("txtTotal") as TextBox;
                              
                oDetalle = new RemitoDetalle();
                oDetalle.Detalle = txtDetalle.Text;
                oDetalle.IdRemitoDetalle = int.Parse(hfIdRemitoDetalle.Value);
                oDetalle.Cantidad = int.Parse(txtCantidad.Text);
                oDetalle.PrecioUnit = decimal.Parse(txtPU.Text);
                oDetalle.PrecioTotal = decimal.Parse(txtTotal.Text);
                
                oRemito.oListDetalles.Add(oDetalle);
                if (!_accion.Equals("Bonificado"))
                    oListDetTango.AddRange(CargarDetalleTango(row, ref nRenglon));

                //nRenglon++;
                
            }

            if (_idRemito == 0)
            {
                if (ultimaFecha <= oRemito.Fecha)
                {
                    _idRemito = oRemitosBus.RemitosAdd(oRemito, oListDetTango);
                }
                else
                    Mensaje.errorMsj("Debe ingresar una fecha mayor o igual al último remito", this.Page, "Fecha Invalida", null);
            }
            else
            {
                oRemito.IdRemito = _idRemito;
                oRemitosBus.RemitosUpdate(oRemito);
            }
            if (_idRemito > 0 && (!_accion.Equals("Bonificado")))
            {
                Mensaje.preguntaMsj("Remito Guardado! \n Desea Imprimir?", this.Page, "Exito", "../Reportes/frmRemitoRPT.aspx?idRemito=" + _idRemito.ToString());
                //Response.Redirect("frmRemitosGestion.aspx?idModulo=8");
            }
            if (_accion.Equals("Bonificado"))
            {
                Mensaje.successMsj("Remito Bonificado con éxito!", this, "Exito", "frmRemitosGestion.aspx?idModulo=8");
            }

        }

        private List<RemitoDetalleTango> CargarDetalleTango(GridViewRow row, ref int nRenglon)
        {
            List<RemitoDetalleTango> oListDetalle = new List<RemitoDetalleTango>();
            RemitoDetalleTango oDetalle = new RemitoDetalleTango();
            TextBox txtCantidad = row.FindControl("txtCantidad") as TextBox;
            TextBox txtDetalle = row.FindControl("txtDetalle") as TextBox;
            TextBox txtPU = row.FindControl("txtPU") as TextBox;
            TextBox txtTotal = row.FindControl("txtTotal") as TextBox;
            DropDownList ddlCodTango = row.FindControl("ddlCodTangoI") as DropDownList;

            string[] descrip = txtDetalle.Text.Split('\n');
            
            for (int i = 0; i < descrip.Length; i++)
            {
                if (descrip[i] != string.Empty)
                {                   
                    string renglon = descrip[i];

                    for (int pos = 0; pos < renglon.Length; pos = pos + 30)
                    {
                        oDetalle = new RemitoDetalleTango();
                        string linea = renglon.Substring(pos).Length > 30 ? renglon.Substring(pos, 30) : renglon.Substring(pos);
                        oDetalle.Descripcion = linea;

                        //if (renglon.Length > 30)
                        //{
                        //    oDetalle.Descripcion = renglon.Substring(0, 30);
                        //    oDetalle.DescripAdic = renglon.Substring(30);
                        //}
                        //else
                        //{
                        //    oDetalle.Descripcion = renglon;
                        //}

                        oDetalle.nRenglon = nRenglon;
                        oDetalle.nroRemito = lblNroRemito.Text;
                        if (i == 0 && pos == 0)
                        {
                            oDetalle.cantidad = int.Parse(txtCantidad.Text);
                            oDetalle.PrecioUnit = decimal.Parse(txtPU.Text);
                            oDetalle.PrecioTotal = decimal.Parse(txtTotal.Text);
                            oDetalle.codTango = ddlCodTango.SelectedValue;
                        }
                        oListDetalle.Add(oDetalle);
                        nRenglon++;
                    }
                }

            }

            return oListDetalle;
        }

        private void imprimir()
        {
          
        }

        private void deleteRow(GridViewCommandEventArgs e)
        {
            int indexRow = int.Parse(e.CommandArgument.ToString());
            _dt.Rows.RemoveAt(indexRow);
           BindGrid();
        }

        private void addRow(GridViewCommandEventArgs e)
        {
            revisarGrilla();

            GridViewRow row = (GridViewRow)((ImageButton)e.CommandSource).NamingContainer;
            TextBox txtCant = row.FindControl("txtCantidad") as TextBox;
            TextBox txtDetalle = row.FindControl("txtDetalle") as TextBox;
            TextBox txtPU = row.FindControl("txtPU") as TextBox;
            TextBox txtTotal = row.FindControl("txtTotal") as TextBox;
            DataRow dr = _dt.NewRow();
            dr["idRemitoDetalle"] = 0;
            dr["Cantidad"] = txtCant.Text.Length == 0 ? "0" : txtCant.Text;
            dr["detalle"] = txtDetalle.Text;
            dr["PU"] = txtPU.Text;
            dr["Total"] = txtTotal.Text;

            if (txtDetalle.Text.Length > 0)
            {
                _dt.Rows.Add(dr);
                BindGrid();
            }
            else
                Mensaje.errorMsj("Debe ingresar detalle para agregar la fila", this.Page, "Error", null);

        }

        private void revisarGrilla()
        {
            _dt.Rows.Clear();
            foreach (GridViewRow row in gvDetalles.Rows)
            {
                TextBox txtCant = row.FindControl("txtCantidad") as TextBox;
                TextBox txtDetalle = row.FindControl("txtDetalle") as TextBox;                
                TextBox txtPU = row.FindControl("txtPU") as TextBox;
                TextBox txtTotal = row.FindControl("txtTotal") as TextBox;
                DropDownList ddlCodTango = row.FindControl("ddlCodTangoI") as DropDownList;
                HiddenField hfCodTango = row.FindControl("hfCodTango") as HiddenField;
                DataRow dr = _dt.NewRow();
                dr["idRemitoDetalle"] = 0;
                dr["Cantidad"] = txtCant.Text.Length == 0 ? "0" : txtCant.Text;
                dr["detalle"] = txtDetalle.Text;
                dr["PU"] = txtPU.Text;
                dr["Total"] = txtTotal.Text;
                dr["codTango"] = ddlCodTango.SelectedValue;
                hfCodTango.Value = ddlCodTango.SelectedValue;
                if (txtDetalle.Text.Length > 0)
                {
                    _dt.Rows.Add(dr);
                }
            }
        }

        private void CrearDT()
        {
            _dt = new DataTable();
            _dt.Columns.Add("idRemitoDetalle");
            _dt.Columns.Add("Cantidad");
            _dt.Columns.Add("Detalle");
            _dt.Columns.Add("PU");
            _dt.Columns.Add("Total");
            _dt.Columns.Add("codTango");
        }

        private void CargarGrillaDetalle()
        {
            
            DataRow dr = _dt.NewRow();
            dr["idRemitoDetalle"] = 0;
            dr["Cantidad"] = _cantMovimiento;
            dr["detalle"] = _movimiento.ToUpper();
            dr["PU"] = 0;
            dr["Total"] = 0;
            dr["codTango"] = 0;
            _dt.Rows.Add(dr);

            if (_alquiler != string.Empty)
            {
                dr = _dt.NewRow();
                dr["idRemitoDetalle"] = 0;
                dr["Cantidad"] = _cantAlquiler;
                dr["detalle"] = _alquiler.ToUpper();
                dr["PU"] = 0;
                dr["Total"] = 0;
                dr["codTango"] = 0;
                _dt.Rows.Add(dr);
            }
            
            if (_des != string.Empty)
            {
                dr = _dt.NewRow();
                dr["idRemitoDetalle"] = 0;
                dr["Cantidad"] = 1;
                dr["detalle"] = _des.ToUpper();
                dr["PU"] = 0;
                dr["Total"] = 0;
                dr["codTango"] = 0;
                _dt.Rows.Add(dr);
            }
            if (_conn != string.Empty)
            {
                dr = _dt.NewRow();
                dr["idRemitoDetalle"] = 0;
                dr["Cantidad"] = 1;
                dr["detalle"] = _conn.ToUpper();
                dr["PU"] = 0;
                dr["Total"] = 0;
                dr["codTango"] = 0;
                _dt.Rows.Add(dr);
            }
            BindGrid();
        }

        private void BindGrid()
        {
            gvDetalles.DataSource = _dt;
            gvDetalles.DataBind();

        }

        private void SugerirNroRemito()
        {
            if (_accion.Equals("Bonificado"))
            {
                lblNroRemito.Text = "0";
            }
            else
            {
                RemitosBus oRemitosBus = new RemitosBus();
                lblNroRemito.Text = oRemitosBus.RemitoGetNroRemito().ToString();
            }
        }

        private void CargarRemitoPorParte()
        {
            ParteEntregaBus oParteEntregaBus = new ParteEntregaBus();
            ParteEntrega oParte = new ParteEntrega();
            
            oParte = oParteEntregaBus.ParteEntregaGetById(_idParte);
            txtFecha.Text = DateTime.Today.ToString("dd/MM/yyyy");
            lblCUIT.Text = oParte.oOrdenTrabajo.oSolicitudCliente.oSector.oEmpresa.CUIT;
            lblDomicilio.Text = oParte.oOrdenTrabajo.oSolicitudCliente.oSector.oEmpresa.Domicilio;
            lblEmpresa.Text = oParte.oOrdenTrabajo.oSolicitudCliente.oSector.oEmpresa.RazonSocial;
            lblIVA.Text = oParte.oOrdenTrabajo.oSolicitudCliente.oSector.oEmpresa.CondIVA;
            lblNroParte.Text = oParte.NroParteEntrega;
            lblNroSS.Text = oParte.oOrdenTrabajo.oSolicitudCliente.NroSolicitud.ToString();
            lblSector.Text = oParte.oOrdenTrabajo.oSolicitudCliente.oSector.Descripcion;
            lblSolicitante.Text = oParte.oOrdenTrabajo.oSolicitudCliente.oSolicitante.Apellido;
            int cantDias = (new SolicitudClienteBus()).SolicitudClienteGetFechaRegreso(oParte.oOrdenTrabajo.oSolicitudCliente.NroSolicitud,
                                                                                        oParte.oOrdenTrabajo.oSolicitudCliente.IdSolicitud,
                                                                                        oParte.oOrdenTrabajo.oSolicitudCliente.Fecha);

            #region PRIMER FILA POR DEFECTO

            //Tipo Servicio
            _movimiento = oParte.oOrdenTrabajo.oSolicitudCliente.oTipoServicio.Descripcion;
            //Equipo
            if (oParte.oOrdenTrabajo != null && oParte.oOrdenTrabajo.IdVehiculo > 0)
            {
                _movimiento += " " + oParte.oOrdenTrabajo.oVehiculo.Descripcion;
            }
            //Especificaciones Tecnicas
            if (oParte.oOrdenTrabajo.oSolicitudCliente.oListEspecificaciones.Count == 0)
                _movimiento += " " + oParte.oOrdenTrabajo.oSolicitudCliente.ServicioAux;
            else
            {                
                oParte.oOrdenTrabajo.oSolicitudCliente.oListEspecificaciones.ForEach(e => _movimiento += " " + e.Descripcion + " ");
            }
            _movimiento += "\nDesde: " + oParte.oOrdenTrabajo.oSolicitudCliente.oMovimiento.oLugarOrigen.Descripcion + " " +
                                                 oParte.oOrdenTrabajo.oSolicitudCliente.oMovimiento.PozoOrigen;
            _movimiento += "\nHasta: " + oParte.oOrdenTrabajo.oSolicitudCliente.oMovimiento.oLugarDestino.Descripcion + " " +
                                                 oParte.oOrdenTrabajo.oSolicitudCliente.oMovimiento.PozoDestino;
            _movimiento += "\nVehiculo pesado";
            _movimiento += "\nRealizado " + oParte.oOrdenTrabajo.oSolicitudCliente.Fecha.ToString("dd/MM/yyyy");
            _cantMovimiento = oParte.oOrdenTrabajo.oSolicitudCliente.oMovimiento.KmsEstimados.ToString();
            
            #endregion
            
            #region SEGUNDA FILA POR DEFECTO

            if (oParte.oOrdenTrabajo != null && oParte.oOrdenTrabajo.IdVehiculo > 0)
            {
                _alquiler = "Alquiler " + oParte.oOrdenTrabajo.oSolicitudCliente.oCondicion.Descripcion + " de equipo ";
                //Especificaciones Tecnicas
                if (oParte.oOrdenTrabajo.oSolicitudCliente.oListEspecificaciones.Count == 0)
                    _alquiler += oParte.oOrdenTrabajo.oSolicitudCliente.ServicioAux;
                else
                {
                    oParte.oOrdenTrabajo.oSolicitudCliente.oListEspecificaciones.ForEach(e => _alquiler += e.Descripcion + " ");
                }
                //_alquiler += "\nRealizado " + oParte.oOrdenTrabajo.oSolicitudCliente.Fecha.ToString("dd/MM/yyyy") + "\n";

                if (oParte.oOrdenTrabajo.oSolicitudCliente.letra != string.Empty && 
                    oParte.oOrdenTrabajo.oSolicitudCliente.letra != "A")
                {
                    _alquiler += "\nEn " + oParte.oOrdenTrabajo.oSolicitudCliente.oMovimiento.oLugarOrigen.Descripcion;
                    _alquiler += " el día " + oParte.oOrdenTrabajo.oSolicitudCliente.fechaRegreso + " al " + oParte.oOrdenTrabajo.oSolicitudCliente.Fecha.ToString("dd/MM/yyyy");
                }

                //Cant Días
                if (!oParte.oOrdenTrabajo.oSolicitudCliente.oCondicion.Descripcion.Contains("Mensual"))
                    _cantAlquiler = cantDias.ToString();
                else
                    _cantAlquiler = "0";

            }

            #endregion

            #region TERCERA Y CUARTA FILA POR DEFECTO
            if (oParte.Desconexion)
            {
                _des = "SERVICIO DE DESCONEXIÓN";
            }
            if (oParte.Conexion)
            {
                _conn = "SERVICIO DE CONEXIÓN";
            }
            #endregion


        }

        #endregion

    }
}
    