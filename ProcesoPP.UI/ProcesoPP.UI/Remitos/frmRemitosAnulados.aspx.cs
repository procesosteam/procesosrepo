﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ProcesoPP.Model;
using System.Configuration;
using ProcesoPP.Business;
using ProcesoPP.Services;
using ParteDiario.Services;
using System.Data;

namespace ProcesoPP.UI.Remitos
{
    public partial class frmRemitosAnulados : System.Web.UI.Page
    {
        #region PROPIEDADES

        public Permisos _Permiso
        {
            get { return (Permisos)ViewState["permiso"]; }
            set { ViewState["permiso"] = value; }
        }

        public int _idUsuario
        {
            get { return (int)ViewState["idUsuario"]; }
            set { ViewState["idUsuario"] = value; }
        }
        public int _ADM
        {
            get { return int.Parse(ConfigurationManager.AppSettings["ADM"].ToString()); }
        }
        #endregion

        #region EVENTOS

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                _idUsuario = Common.EvaluarSession(Request, this.Page);
                if (_idUsuario != 0)
                {
                    _Permiso = Common.getModulo(Request, this.Page);
                    if (_Permiso != null && _Permiso.Lectura)
                    {
                        if (!IsPostBack)
                        {
                            CargarEmpresa();
                            CargarEquipo();
                            CargarTipoServicio();
                            CargarCondicion();
                            ddlSector.Items.Insert(0, new ListItem("Seleccione...", "0"));
                        }
                    }
                    else
                    {
                        Mensaje.errorMsj("NO TIENE PERMISOS A ESTE FORMULARIO", this.Page, "SIN ACCESO", "../frmPrincipal.aspx");
                    }
                }
                else
                {
                    Mensaje.errorMsj("DEBE LOGUEARSE", this.Page, "LOGIN", "../frmLogin.aspx");
                }
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                RemitosAnuladosGetByFilter(txtNroSolicitud.Text, ddlEmpresa.SelectedValue, txtFecha.Text, txtFechaHasta.Text, txtNroParte.Text, ddlTrailer.SelectedValue, ddlSector.SelectedValue, ddlTipoServicio.SelectedValue, ddlCondicion.SelectedValue);
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        protected void gvSolicitud_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
                CargarRow(e);
        }

        protected void gvOTI_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvSolicitud.PageIndex = e.NewPageIndex;
            RemitosAnuladosGetByFilter(txtNroSolicitud.Text, ddlEmpresa.SelectedValue, txtFecha.Text, txtFechaHasta.Text, txtNroParte.Text, ddlEmpresa.SelectedValue, ddlSector.SelectedValue, ddlTipoServicio.SelectedValue, ddlCondicion.SelectedValue);
        }

        protected void ddlEmpresa_SelectedIndexChanged(object sender, EventArgs e)
        {
            CargarSector(ddlEmpresa.SelectedValue);
            ddlSector.Items.Insert(0, new ListItem("Seleccione...", "0"));
        }

        protected void gvSolicitud_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            GridViewRow row = (GridViewRow)((LinkButton)e.CommandSource).NamingContainer;
            HiddenField hfIdRemito = row.FindControl("hfIdRemito") as HiddenField;
            switch (e.CommandName.ToString())
            {
                case "Ver":
                    Response.Redirect("../SSe3/frmSSRemitoCarga.aspx?idModulo=8&idParte=" + e.CommandArgument.ToString() + "&idRemito=" + hfIdRemito.Value + "&accion=" + e.CommandName);
                    break;
            }
        }

        protected void btExportar_Click(object sender, EventArgs e)
        {
            try
            {
                exportar(txtNroSolicitud.Text, ddlEmpresa.SelectedValue, txtFecha.Text, txtFechaHasta.Text, txtNroParte.Text, ddlTrailer.SelectedValue, ddlSector.SelectedValue, ddlTipoServicio.SelectedValue, ddlCondicion.SelectedValue);
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        #endregion
        #region METODOS

        private void exportar(string nroSolicitud, string idEmpresa, string desde, string hasta, string nroParte, string idEquipo, string idSector, string idTipoServicio, string idCondicion)
        {
            RemitosBus oRemitosBus = new RemitosBus();
            DataTable dt = oRemitosBus.RemitosAnuladosGetByFilter(nroSolicitud, idEmpresa, desde, hasta, nroParte, idEquipo, idSector, idTipoServicio, idCondicion);
            dt.Columns.Remove("idSolicitud");
            dt.Columns.Remove("idOrden");
            dt.Columns.Remove("idVehiculo");
            dt.Columns.Remove("idRemito");
            dt.Columns.Remove("idParte");
            dt.Columns.Remove("idParteEntrega");
            dt.Columns["NroSolicitud"].ColumnName = "N° SS";
            dt.Columns["NroRemito"].ColumnName = "N° Remito";
            dt.Columns["NroParte"].ColumnName = "N° Parte";
            dt.Columns["TipoServicio"].ColumnName = "Tipo Servicio";
            dt.Columns["Trailer"].ColumnName = "Equipo";

            string path = System.IO.Path.Combine(Server.MapPath("~"), "img");
            ExportarExcel oExport = new ExportarExcel("Remitos_" + DateTime.Now.ToString("yyyyMMddHHmmss"), path, "REMITOS");
            oExport.ExportarExcel2003(dt);

        }


        private void CargarRow(GridViewRowEventArgs e)
        {
            Common.CargarRow(e, _Permiso);
        }

        private void RemitosAnuladosGetByFilter(string nroSolicitud, string idEmpresa, string desde, string hasta, string nroParte, string idEquipo, string idSector, string idTipoServicio, string idCondicion)
        {
            RemitosBus oRemitosBus = new RemitosBus();

            gvSolicitud.DataSource = oRemitosBus.RemitosAnuladosGetByFilter(nroSolicitud, idEmpresa, desde, hasta, nroParte, idEquipo, idSector, idTipoServicio, idCondicion);
            gvSolicitud.DataBind();
        }

        private void CargarCondicion()
        {
            CondicionBus oCondicionBus = new CondicionBus();
            oCondicionBus.CargarCombo(ref ddlCondicion, "Seleccione...");
        }

        private void CargarTipoServicio()
        {
            TipoServicioBus oTipoServicioBus = new TipoServicioBus();
            oTipoServicioBus.CargarCombo(ref ddlTipoServicio, "Selecione..");
        }

        private void CargarSector(string idEmpresa)
        {
            SectorBus oSectorBus = new SectorBus();
            oSectorBus.CargarSectorPorEmpresa(ref ddlSector, int.Parse(idEmpresa));
        }

        private void CargarEmpresa()
        {
            EmpresaBus oEmpresaBus = new EmpresaBus();
            oEmpresaBus.CargarEmpresa(ref ddlEmpresa, "Seleccione...");
        }

        private void CargarEquipo()
        {
            VehiculoBus oVehiculoBus = new VehiculoBus();
            oVehiculoBus.CargarComboEquipo(ref ddlTrailer, "Seleccione un equipo...");
        }
        #endregion

    }
}
