﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ProcesoPP.Model;
using ProcesoPP.Services;
using ProcesoPP.Business;
using System.Data;
using ParteDiario.Services;
using System.Configuration;

namespace ProcesoPP.UI.Remitos
{
    public partial class frmRemitosGestion : System.Web.UI.Page
    {
        #region PROPIEDADES

        public Permisos _Permiso
        {
            get { return (Permisos)ViewState["permiso"]; }
            set { ViewState["permiso"] = value; }
        }

        public int _idUsuario
        {
            get { return (int)ViewState["idUsuario"]; }
            set { ViewState["idUsuario"] = value; }
        }
        public int _ADM
        {
            get { return int.Parse(ConfigurationManager.AppSettings["ADM"].ToString()); }
        }
        #endregion
        #region EVENTOS

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                _idUsuario = Common.EvaluarSession(Request, this.Page);
                if (_idUsuario != 0)
                {
                    _Permiso = Common.getModulo(Request, this.Page);
                    if (_Permiso != null && _Permiso.Lectura)
                    {
                        if (!IsPostBack)
                        {                            
                            CargarEmpresa();
                            CargarEquipo();
                            CargarTipoServicio();
                            CargarCondicion();
                            ddlSector.Items.Insert(0, new ListItem("Seleccione...", "0"));
                        }
                    }
                    else
                    {
                        Mensaje.errorMsj("NO TIENE PERMISOS A ESTE FORMULARIO", this.Page, "SIN ACCESO", "../frmPrincipal.aspx");
                    }
                }
                else
                {
                    Mensaje.errorMsj("DEBE LOGUEARSE", this.Page, "LOGIN", "../frmLogin.aspx");
                }
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                RemitosGetByFilter(txtNroSolicitud.Text, ddlEmpresa.SelectedValue, txtFecha.Text, txtFechaHasta.Text, txtNroParte.Text, ddlTrailer.SelectedValue, ddlSector.SelectedValue, ddlTipoServicio.SelectedValue, ddlCondicion.SelectedValue, txtNroRemito.Text, "");
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        protected void gvSolicitud_RowDataBound(object sender, GridViewRowEventArgs e)
        {            
            if (e.Row.RowType == DataControlRowType.DataRow)
               CargarRow(e);
        }

        protected void gvSolicitud_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvSolicitud.PageIndex = e.NewPageIndex;
            RemitosGetByFilter(txtNroSolicitud.Text, ddlEmpresa.SelectedValue, txtFecha.Text, txtFechaHasta.Text, txtNroParte.Text, ddlTrailer.SelectedValue, ddlSector.SelectedValue, ddlTipoServicio.SelectedValue, ddlCondicion.SelectedValue, txtNroRemito.Text, "");
        }

        protected void ddlEmpresa_SelectedIndexChanged(object sender, EventArgs e)
        {
            CargarSector(ddlEmpresa.SelectedValue);
            ddlSector.Items.Insert(0, new ListItem("Seleccione...", "0"));
        }

        protected void gvSolicitud_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            
            switch (e.CommandName.ToString())
            {
                case "GenerarRemito":
                    GridViewRow row = (GridViewRow)((LinkButton)e.CommandSource).NamingContainer;
                    HiddenField hfIdRemito = row.FindControl("hfIdRemito") as HiddenField;
                    Response.Redirect("../SSe3/frmSSRemitoCarga.aspx?idModulo=8&idParte=" + e.CommandArgument.ToString() + "&idRemito=" + hfIdRemito.Value + "&accion=" + e.CommandName);
                    break;
                case "Ver":
                    row = (GridViewRow)((LinkButton)e.CommandSource).NamingContainer;
                    hfIdRemito = row.FindControl("hfIdRemito") as HiddenField;
                    Response.Redirect("../SSe3/frmSSRemitoCarga.aspx?idModulo=8&idParte=" + e.CommandArgument.ToString() + "&idRemito=" + hfIdRemito.Value + "&accion=" + e.CommandName);
                    break;
                case "Anular":
                    Anular(e.CommandArgument.ToString());
                    break;
                case "Bonificado":
                    Response.Redirect("../SSe3/frmSSRemitoCarga.aspx?idModulo=7&idParte=" + e.CommandArgument.ToString() + "&accion=" + e.CommandName);
                    break;
            }
        }

        protected void btExportar_Click(object sender, EventArgs e)
        {
            try
            {
                exportar(txtNroSolicitud.Text, ddlEmpresa.SelectedValue, txtFecha.Text, txtFechaHasta.Text, txtNroParte.Text, ddlTrailer.SelectedValue, ddlSector.SelectedValue, ddlTipoServicio.SelectedValue, ddlCondicion.SelectedValue, txtNroRemito.Text, "");
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        #endregion

        #region METODOS

        private void exportar(string nroSolicitud, string idEmpresa, string desde, string hasta, string nroParte, string idEquipo, string idSector, string idTipoServicio, string idCondicion, string nroRemito, string chkRem)
        {
            RemitosBus oRemitosBus = new RemitosBus();
            DataTable dt = oRemitosBus.RemitosHistorialGetByFilter(nroSolicitud, idEmpresa, desde, hasta, nroParte, idEquipo, idSector, idTipoServicio, idCondicion, nroRemito, chkRem); 
            dt.Columns.Remove("idSolicitud");
            dt.Columns.Remove("idOrden");            
            dt.Columns.Remove("idRemito");
            dt.Columns.Remove("idParte");
            dt.Columns.Remove("idParteEntrega");
            dt.Columns["NroSolicitud"].ColumnName = "N° SS";
            dt.Columns["NroRemito"].ColumnName = "N° Remito";
            dt.Columns["NroParte"].ColumnName = "N° Parte";
            dt.Columns["TipoServicio"].ColumnName = "Tipo Servicio";
            dt.Columns["Trailer"].ColumnName = "Equipo";

            string path = System.IO.Path.Combine(Server.MapPath("~"), "img");
            ExportarExcel oExport = new ExportarExcel("Remitos_" + DateTime.Now.ToString("yyyyMMddHHmmss"), path, "REMITOS");
            oExport.ExportarExcel2003(dt);

        }

        private void Anular(string idRemito)
        {
            RemitosBus oRemitosBus = new RemitosBus();
            Model.Remitos oRemito = new ProcesoPP.Model.Remitos();
            oRemito = oRemitosBus.RemitosGetById(int.Parse(idRemito));
            oRemito.Anulado = true;
            oRemito.FechaAnulado = DateTime.Now;

            oRemitosBus.RemitosUpdate(oRemito);
            RemitosGetByFilter(txtNroSolicitud.Text, ddlEmpresa.SelectedValue, txtFecha.Text, txtFechaHasta.Text, txtNroParte.Text, ddlEmpresa.SelectedValue, ddlSector.SelectedValue, ddlTipoServicio.SelectedValue, ddlCondicion.SelectedValue, txtNroRemito.Text, "");
        }

        private void CargarRow(GridViewRowEventArgs e)
        {
            LinkButton btnVer = e.Row.FindControl("btnVer") as LinkButton;
            LinkButton btnEliminar = e.Row.FindControl("btnEliminar") as LinkButton;
            LinkButton btnModificar = e.Row.FindControl("btnModificar") as LinkButton;
            LinkButton btnBonificado = e.Row.FindControl("btnBonificado") as LinkButton;
            HiddenField hfIdRemito = e.Row.FindControl("hfIdRemito") as HiddenField;
            HiddenField hfImpreso = e.Row.FindControl("hfImpreso") as HiddenField;
            bool impreso = false;
            
            Common.CargarRow(e, _Permiso);

            if (bool.TryParse(hfImpreso.Value, out impreso))
            {
                if (impreso)
                {
                    btnModificar.Visible = false;
                }
                else
                    btnModificar.Visible = true;
            }
                

            if (int.Parse(hfIdRemito.Value) == 0)
            {
                btnVer.Visible = false;
                btnEliminar.Visible = false;
                if (_Permiso.oModulo.idModulo == 9)
                {                 
                    btnBonificado.Visible = true;
                }
            }
            else
            {
                btnVer.Visible = true;
                if (_Permiso.oModulo.idModulo == 9)
                {
                    btnEliminar.Visible = true;                    
                }
            }


        }

        private void RemitosGetByFilter(string nroSolicitud, string idEmpresa, string desde, string hasta, string nroParte, string idEquipo, string idSector, string idTipoServicio, string idCondicion, string nroRemito, string chkRem)
        {
            RemitosBus oRemitosBus = new RemitosBus();
            
            gvSolicitud.DataSource = oRemitosBus.RemitosHistorialGetByFilter(nroSolicitud, idEmpresa, desde, hasta, nroParte, idEquipo, idSector, idTipoServicio, idCondicion, nroRemito, chkRem);
            gvSolicitud.DataBind();
        }

        private void CargarCondicion()
        {
            CondicionBus oCondicionBus = new CondicionBus();
            oCondicionBus.CargarCombo(ref ddlCondicion, "Seleccione...");
        }

        private void CargarTipoServicio()
        {
            TipoServicioBus oTipoServicioBus = new TipoServicioBus();
            oTipoServicioBus.CargarCombo(ref ddlTipoServicio, "Selecione..");
        }

        private void CargarSector(string idEmpresa)
        {
            SectorBus oSectorBus = new SectorBus();
            oSectorBus.CargarSectorPorEmpresa(ref ddlSector,int.Parse(idEmpresa));
        }

        private void CargarEmpresa()
        {
            EmpresaBus oEmpresaBus = new EmpresaBus();
            oEmpresaBus.CargarEmpresa(ref ddlEmpresa, "Seleccione...");
        }

        private void CargarEquipo()
        {
            VehiculoBus oVehiculoBus = new VehiculoBus();
            oVehiculoBus.CargarComboEquipo(ref ddlTrailer, "Seleccione un equipo...");
        }
        #endregion

    }
}
