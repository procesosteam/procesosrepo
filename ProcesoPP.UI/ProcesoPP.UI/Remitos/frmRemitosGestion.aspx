﻿<%@ Page Title="" Language="C#" MasterPageFile="~/frmPrincipal.Master" AutoEventWireup="true"
    CodeBehind="frmRemitosGestion.aspx.cs" Inherits="ProcesoPP.UI.Remitos.frmRemitosGestion" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        $(document).ready(function () { SetMenu("liRemito"); }); 
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="contenido" runat="server">
        <div id="apertura">
            Historial de Remitos</div>
        <div class="solicitud100">
            <div class="sol_tit50">
                <label> Fecha Desde</label></div>
            <div class="sol_campo30">
                <asp:TextBox runat="server" CssClass="sol_campo25" ID="txtFecha" /></div>
            <div class="sol_fecha">
                <img id="btnFecha" alt="calendario" src="../img/calendario.gif" style="width: 20px;
                    height: 20px; cursor: pointer;" onmouseover="CambiarConfiguracionCalendario('txtFecha',this);" /></div>
            <div class="sol_fecha">
            </div>
            <div class="sol_tit50">
                <label>Fecha Hasta</label></div>
            <div class="sol_campo30">
                <asp:TextBox runat="server" CssClass="sol_campo25" ID="txtFechaHasta" /></div>
            <div class="sol_fecha">
                <img id="btnFechaHasta" alt="calendario" src="../img/calendario.gif" style="width: 20px;
                    height: 20px; cursor: pointer;" onmouseover="CambiarConfiguracionCalendario('txtFechaHasta',this);" /></div>
        </div>
        <div class="solicitud100">
            <div class="sol_tit50">
                <label>Empresa</label></div>
            <div class="sol_campo30">
                <asp:DropDownList runat="server" CssClass="sol_campo50c" ID="ddlEmpresa" AutoPostBack="true"
                    OnSelectedIndexChanged="ddlEmpresa_SelectedIndexChanged" />
            </div>
            <div class="sol_fecha">
            </div>
            <div class="sol_fecha">
            </div>
            <div class="sol_tit50">
                <label>
                    Sector</label></div>
            <div class="sol_campo30">
                <asp:DropDownList runat="server" CssClass="sol_campo50c" ID="ddlSector" />
            </div>
            <div class="sol_fecha">
            </div>
        </div>
        <div class="solicitud100">
            <div class="sol_tit50">
                <label>Tipo Servicio</label></div>
            <div class="sol_campo30">
                <asp:DropDownList runat="server" CssClass="sol_campo50c" ID="ddlTipoServicio" />
            </div>
            <div class="sol_fecha">
            </div>
            <div class="sol_fecha">
            </div>
            <div class="sol_tit50">
                <label>Nº Solicitud</label></div>
            <div class="sol_campo30">
                <asp:TextBox CssClass="sol_campo25" runat="server" ID="txtNroSolicitud" onkeypress="return ValidarNumeroEntero(event,this);" /></div>
            <div class="sol_fecha">
            </div>
        </div>
        <div class="solicitud100">
            <div class="sol_tit50">
                <label>Nº Parte</label></div>
            <div class="sol_campo30">
                <asp:TextBox CssClass="sol_campo25" runat="server" ID="txtNroParte" onkeypress="return ValidarNumeroEntero(event,this);" /></div>
            <div class="sol_fecha">
            </div>
            <div class="sol_fecha">
            </div>
            <div class="sol_tit50">
                <label>
                    Nº Remito</label></div>
            <div class="sol_campo30">
                <asp:TextBox CssClass="sol_campo25" runat="server" ID="txtNroRemito" onkeypress="return ValidarNumeroEntero(event,this);" /></div>
            <div class="sol_fecha">
            </div>
        </div>
        <div class="solicitud100">
            <div class="sol_tit50">
                <label>
                    Condición</label></div>
            <div class="sol_campo30">
                <asp:DropDownList runat="server" CssClass="sol_campo50c" ID="ddlCondicion" />
            </div>
            <div class="sol_fecha">
            </div>
            <div class="sol_fecha">
            </div>
            <div class="sol_tit50">
                <label>
                    N° Equipo</label></div>
            <div class="sol_campo30">
                <asp:DropDownList runat="server" CssClass="sol_campo50c" ID="ddlTrailer" />
            </div>
            <div class="sol_fecha">
            </div>
        </div>
        <div class="solicitud100sin">
            <div class="sol_btn">
                <asp:Button runat="server" ID="btnBuscar" CssClass="btn_form" Text="Buscar" OnClick="btnBuscar_Click" />
                <asp:Button runat="server" ID="btnExportar" CssClass="btn_form" Text="Exportar" OnClick="btExportar_Click" />
            </div>
        </div>
    </div>
    <asp:GridView ID="gvSolicitud" runat="server" AutoGenerateColumns="False" DataMember="idCondicion"
        CssClass="tabla3" HeaderStyle-CssClass="celda1" AllowPaging="true" PageSize="20"
        OnPageIndexChanging="gvSolicitud_PageIndexChanging" PagerStyle-CssClass="tabla_font"
        OnRowCommand="gvSolicitud_RowCommand" OnRowDataBound="gvSolicitud_RowDataBound">
        <Columns>
            <asp:TemplateField HeaderText="Nº SS" HeaderStyle-CssClass="columna7" ItemStyle-CssClass="columna7">
                <ItemTemplate>
                    <asp:Label ID="lblNroSolicitud" runat="server" Text='<%# Bind("NroSolicitud") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Nº Parte" HeaderStyle-CssClass="columna7" ItemStyle-CssClass="columna7">
                <ItemTemplate>
                    <asp:Label ID="lblNroParte" runat="server" Text='<%# Bind("NroParte") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Nº Remito" HeaderStyle-CssClass="columna7" ItemStyle-CssClass="columna7">
                <ItemTemplate>
                    <asp:Label ID="lblNroRemito" runat="server" Text='<%# Bind("NroRemito") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Fecha" HeaderStyle-CssClass="columna7" ItemStyle-CssClass="columna7">
                <ItemTemplate>
                    <asp:Label ID="lblFecha" runat="server" Text='<%# Bind("Fecha") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Importe" HeaderStyle-CssClass="columna7" ItemStyle-CssClass="columna7">
                <ItemTemplate>
                    <asp:Label ID="lblImporte" runat="server" Text='<%# Bind("Importe") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Empresa" HeaderStyle-CssClass="columna5" ItemStyle-CssClass="columna5">
                <ItemTemplate>
                    <asp:Label ID="lblSolicitante" runat="server" Text='<%# Bind("Empresa") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Sector" HeaderStyle-CssClass="columna6" ItemStyle-CssClass="columna6">
                <ItemTemplate>
                    <asp:Label ID="lblEmpresa" runat="server" Text='<%# Bind("Sector") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Servicio" HeaderStyle-CssClass="columna5" ItemStyle-CssClass="columna5">
                <ItemTemplate>
                    <asp:Label ID="lblTipoServ" runat="server" Text='<%# Bind("TipoServicio") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Nº Equipo" HeaderStyle-CssClass="columna7" ItemStyle-CssClass="columna7">
                <ItemTemplate>
                    <asp:Label ID="lblTrailer" runat="server" Text='<%# Bind("Trailer") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField ItemStyle-CssClass="btn_iconos" HeaderStyle-CssClass="btn_iconos">
                <ItemTemplate>
                    <asp:HiddenField runat="server" ID="hfIdRemito" Value='<%# Eval("idRemito") %>' />
                    <asp:LinkButton ID="btnModificar" runat="server" title="Cargar Remito" CssClass="editar"
                        CommandArgument='<%# Bind("idParte") %>' CommandName="GenerarRemito"></asp:LinkButton>
                    <asp:HiddenField runat="server" ID="hfImpreso" Value='<%# Eval("Impreso") %>' />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField ItemStyle-CssClass="btn_iconos" HeaderStyle-CssClass="btn_iconos">
                <ItemTemplate>
                    <asp:LinkButton ID="btnVer" runat="server" title="Ver Remito" CssClass="ver" CommandArgument='<%# Bind("idParte") %>'
                        CommandName="Ver"></asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField ItemStyle-CssClass="btn_iconos" HeaderStyle-CssClass="btn_iconos">
                <ItemTemplate>
                    <asp:LinkButton ID="btnEliminar" runat="server" title="Anular Regreso" CssClass="eliminar"
                        CommandArgument='<%# Bind("idRemito") %>' CommandName="Anular" Visible="false"
                        OnClientClick="return Eliminar('el Remito');"></asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField ItemStyle-CssClass="btn_iconos" HeaderStyle-CssClass="btn_iconos" Visible="false">
                <ItemTemplate>
                    <asp:LinkButton ID="btnBonificado" runat="server" Visible="false" title="Remito Bonificado"
                        CssClass="bonif" CommandArgument='<%# Bind("idParte") %>' CommandName="Bonificado"></asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
</asp:Content>
