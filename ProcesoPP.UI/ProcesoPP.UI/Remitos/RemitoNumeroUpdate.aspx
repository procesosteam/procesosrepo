﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/frmPrincipal.Master"  CodeBehind="RemitoNumeroUpdate.aspx.cs" Inherits="ProcesoPP.UI.Remitos.RemitoNumeroUpdate" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div id="contenido" runat="server">
    <div id="apertura">MODIFICAR NUMERO REMITO</div>  
    
    <div class="solicitud100">
        <div class="sol_tit">
           <label class="numeracion">Próximo N°: </label>
        </div>
         <div class="sol_campo2">
            <asp:TextBox ID="txtNumero" runat="server" CssClass="sol_campo25" onkeypress="return ValidarNumeroEntero(event,this);" />
        </div> 
    <div class="sol_btn">
            <asp:Button ID="btnGuardar" runat="server" CssClass="btn_form" Text="GUARDAR" 
                OnClientClick="return ValidarObj('txtNumero|Ingrese un Numero.')" Visible="false"
                onclick="btnGuardar_Click" />
     </div>
     </div>
</div>
</asp:Content>
