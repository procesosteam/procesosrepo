﻿<%@ Page Title="PROCESOS PATAGONICOS | Sistema Interno" Language="C#" MasterPageFile="~/frmPrincipal.Master"
    AutoEventWireup="true" EnableEventValidation="false" 
    CodeBehind="frmPersonalCarga2.aspx.cs" Inherits="ProcesoPP.UI.Administracion.frmPersonalCarga2" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>    
<%@ PreviousPageType VirtualPath="frmPersonalCarga.aspx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../Css/personal.css" rel="stylesheet" type="text/css" />
    <script src="../Js/jquery.maskedinput.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () { SetMenu("liPersonal"); });
        function verificarForm() {
            msj = 'txtFechaAlta|Ingrese la fecha de alta del Personal.';
            val = ValidarObj(msj);
            hfEsValido = document.getElementById('<%=hfEsValido.ClientID%>');
            if (val) {
                hfEsValido.value = true;
                return true;
            }
            else
                return val;
        }

        function AbrirVentana(url) {

            window.open("../Administracion/VerAdjunto.aspx?idArchivo=" + url + "&esPersonal=true", '');

        }

        jQuery(function ($) {
            $("#<%=txtFechaEPreocupacional.ClientID%>").mask("99/99/9999", { placeholder: "dd/mm/yyyy" });
            });
        jQuery(function ($) {
            $("#<%=txtVManeja.ClientID%>").mask("99/99/9999", { placeholder: "dd/mm/yyyy" });
        });
        jQuery(function ($) {
            $("#<%=txtVPsicofisico.ClientID%>").mask("99/99/9999", { placeholder: "dd/mm/yyyy" });
        });
        jQuery(function ($) {
            $("#<%=txtVCPsicofisico.ClientID%>").mask("99/99/9999", { placeholder: "dd/mm/yyyy" });
        });
        jQuery(function ($) {
            $("#<%=txtVCargasGrales.ClientID%>").mask("99/99/9999", { placeholder: "dd/mm/yyyy" });
        });
        jQuery(function ($) {
            $("#<%=txtVCargasPeligrosas.ClientID%>").mask("99/99/9999", { placeholder: "dd/mm/yyyy" });
        });
        jQuery(function ($) {
            $("#<%=txtVManejoDefensivo.ClientID%>").mask("99/99/9999", { placeholder: "dd/mm/yyyy" });
        });
        jQuery(function ($) {
            $("#<%=txtFechaAlta.ClientID%>").mask("99/99/9999", { placeholder: "dd/mm/yyyy" });
        });
        jQuery(function ($) {
            $("#<%=txtCBU.ClientID%>").mask("99999999-99999999999999");
        });
      </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<asp:HiddenField ID="hfEsValido" Value="false" runat="server" />
    <div id="ape_personal">
        Personal. Alta de Legajo</div>
    <div id="tit_personal">
        Datos Laborales.</div>
    <div id="celda_p50">
    <cc1:toolkitscriptmanager id="ToolkitScriptManager1" runat="server">
        </cc1:toolkitscriptmanager>
        <div id="campo_per30">
            <label>
                Empresa:</label></div>
        <div id="campo_per50">
            <asp:DropDownList CssClass="sol_campo50c" ID="ddlEmpresa" runat="server" />
            <cc1:cascadingdropdown id="cddEmpresa" targetcontrolid="ddlEmpresa" prompttext="N/A"
                promptvalue="0" servicemethod="GetEmpresa" runat="server" category="idEmpresa"
                loadingtext="Cargando..." />
        </div>
    </div>
    
    <div id="celda_p50">
        <div id="campo_per30">
            <label>
                Fecha de Alta:</label></div>
        <div id="campo_per50bis">
            <asp:TextBox CssClass="campo_per50" ID="txtFechaAlta" runat="server"> </asp:TextBox>
        </div>
        <div id="campo_per_icono">
            <asp:FileUpload ID="FileUploadAlta" runat="server" CssClass="subir" Width="18" Height="1"
                border="0"></asp:FileUpload>
        </div>
    </div>
    <div id="celda_p50">
        <div id="campo_per30">
            <label>
                Regimen:</label></div>
        <div id="campo_per50">
            <asp:DropDownList ID="ddlRelacion" CssClass="sol_campo50c" runat="server" />
        </div>
    </div>
    <div id="celda_p50">
        <div id="campo_per30">
            <label>
                Modalidad:</label></div>
        <div id="campo_per50">
            <asp:DropDownList ID="ddlModalidad" CssClass="sol_campo50c" runat="server" />
        </div>
    </div>
    <div id="celda_p50">
        <div id="campo_per30">
            <label>
                Función:</label></div>
        <div id="campo_per50">
            <asp:DropDownList ID="ddlFuncion" CssClass="sol_campo50c" runat="server" />
        </div>
    </div>
    
    <div id="celda_p50">
        <div id="campo_per30">
            <label>
                Sector:</label></div>
        <div id="campo_per50">
            <asp:DropDownList CssClass="sol_campo50c" ID="ddlSector" runat="server" />
            <cc1:cascadingdropdown id="cddSector" targetcontrolid="ddlSector" prompttext="N/A"
                promptvalue="0" servicemethod="GetSector" runat="server" category="idSector"
                parentcontrolid="ddlEmpresa" loadingtext="Cargando..." />
        </div>
    </div>
    <div id="celda_p100">
        <div id="campo_per30">
            <label>
                Diagrama:</label></div>
        <div id="campo_per70">
            <asp:DropDownList ID="ddlDiagrama" CssClass="sol_campo50c" runat="server" />
        </div>
    </div>
    <div id="celda_p100">
        <div id="campo_per30">
            <label>
                Lugar de Trabajo:</label></div>
        <div id="campo_per70">
            <asp:TextBox ID="txtLugarTrabajo" runat="server" Enabled="false" />
        </div>
    </div>
    <div id="celda_p100">
        <div id="campo_per30">
            <label>
                Acreditación de Haberes:</label></div>
        <div id="campo_per50">
            <asp:DropDownList ID="ddlBanco" CssClass="sol_campo50c" runat="server" />
        </div>
    </div>
    <div id="celda_p100">
        <div id="campo_per30">
            <label>
                C.B.U. N°:</label></div>
        <div id="campo_per70">
            <asp:TextBox ID="txtCBU" CssClass="campos" runat="server" onkeypress="return ValidarNumeroEntero(event,this);" />
        </div>
    </div>
    
    <div id="tit_personal">
        Información Médica</div>
    <div id="celda_p50">
        <div id="campo_per30">
            <label>
                Grupo Sanguineo:</label></div>
        <div id="campo_per50">
            <asp:TextBox CssClass="campos" ID="txtGrupoSanguineo" runat="server"> </asp:TextBox>
        </div>
    </div>
    <div id="celda_p50">
        <div id="campo_per30">
            <label>
                Factor:</label></div>
        <div id="campo_per50">
            <asp:TextBox CssClass="campo_per50" ID="txtFactor" runat="server"> </asp:TextBox>
        </div>
    </div>
    <div id="celda_p50">
        <div id="campo_per30">
            <label>
                Alergíco:</label></div>
        <div id="campo_per50">
            <asp:TextBox CssClass="campo_per50" ID="txtAlergias" runat="server"> </asp:TextBox>
        </div>
    </div>
    <div id="celda_p50">
        <div id="campo_per30">
            <label>
                Observación:</label></div>
        <div id="campo_per50">
            <asp:TextBox CssClass="campo_per50" ID="txtObservacion" runat="server"> </asp:TextBox>
        </div>
    </div>
    <div id="celda_p50">
        <div id="campo_per30">
            <label>
                Hepatitis B:</label></div>
        <div id="campo_per50bis">
            <div id="check">
                Si</div>
            <asp:RadioButton CssClass="check2" GroupName="radioHepB" ID="rhSi" runat="server" />
            <div id="check">
                No</div>
            <asp:RadioButton CssClass="check2" GroupName="radioHepB" ID="rhNo" runat="server" />
        </div>
        <div id="campo_per_icono">
            <asp:FileUpload ID="FileUploadHepatitis" runat="server" CssClass="subir" Width="18"
                Height="1" border="0"></asp:FileUpload>
        </div>
    </div>
    <div id="celda_p50">
        <div id="campo_per30">
            <label>
                Antitetanica:</label></div>
        <div id="campo_per50bis">
            <div id="check">
                Si</div>
            <asp:RadioButton CssClass="check2" GroupName="radioAntiTet" ID="ratSi" runat="server" />
            <div id="check">
                No</div>
            <asp:RadioButton CssClass="check2" GroupName="radioAntiTet" ID="ratNo" runat="server" />
        </div>
        <div id="campo_per_icono">
            <asp:FileUpload ID="FileUploadAntitetanica" runat="server" CssClass="subir" Width="18"
                Height="1" border="0"></asp:FileUpload>
        </div>
    </div>
    <div id="celda_p50">
        <div id="campo_per30">
            <label>
                E. Preocupacional:</label></div>
        <div id="campo_per50bis">
            <div id="check">
                Si</div>
            <asp:RadioButton CssClass="check2" GroupName="radioOcup" ID="repSi" runat="server" />
            <div id="check">
                No</div>
            <asp:RadioButton CssClass="check2" GroupName="radioOcup" ID="repNo" runat="server" />
        </div>
        <div id="campo_per_icono">
            <asp:FileUpload ID="FileUploadPreocupacional" runat="server" CssClass="subir" Width="18"
                Height="1" border="0"></asp:FileUpload>
        </div>
    </div>
    <div id="celda_p50">
        <div id="campo_per30">
            <label>
                Fecha:</label></div>
        <div id="campo_per50bis">
            <asp:TextBox class="campos" type="text" name="nombre" ID="txtFechaEPreocupacional"
                runat="server"></asp:TextBox>
        </div>
        <div id="campo_per_icono">
            <asp:FileUpload ID="FileUploadPreocupacionalFecha" runat="server" CssClass="subir"
                Width="18" Height="1" border="0"></asp:FileUpload>
        </div>
    </div>
    <div id="celda_p100">
        <div id="campo_per100">
            Aprobado por el Médico Laboral:</div>
        <div id="campo_per50bis">
            <div id="check">
                Si</div>
            <asp:RadioButton CssClass="check2" GroupName="radioMedicoLab" ID="rmlSi" runat="server" />
            <div id="check">
                No</div>
            <asp:RadioButton CssClass="check2" GroupName="radioMedicoLab" ID="rmlNo" runat="server" />
        </div>
        <div id="campo_per_icono">
            <asp:FileUpload ID="FileUploadMedicoLaboral" runat="server" CssClass="subir" Width="18"
                Height="1" border="0"></asp:FileUpload>
        </div>
    </div>
    <div id="tit_personal">
        Conducción Vehícular</div>
    <div id="celda_p50">
        <div id="campo_per30">
            <label>
                Maneja en Emp:</label></div>
        <div id="campo_per50bis">
            <div id="check">
                Si</div>
            <asp:RadioButton CssClass="check2" GroupName="radioManeja" ID="rmemSi" runat="server" />
            <div id="check">
                No</div>
            <asp:RadioButton CssClass="check2" GroupName="radioManeja" ID="rmemNo" runat="server" />
        </div>
    </div>
    <div id="celda_p50">
        <div id="campo_per30">
            <label>
                Vencimiento:</label></div>
        <div id="campo_per50bis">
            <asp:TextBox CssClass="campo_per50" ID="txtVManeja" runat="server"> </asp:TextBox></div>
        <div id="campo_per_icono">
            <asp:FileUpload ID="FileUploadCarnet" runat="server" CssClass="subir" Width="18"
                Height="1" border="0"></asp:FileUpload>
        </div>
    </div>
    <div id="celda_p100">
        <div id="campo_per30">
            <label>
                Categoria Carnet:</label></div>
        <div id="campo_per50">
            <asp:DropDownList ID="ddlCategoriaCarnet" CssClass="sol_campo50c" runat="server" />
        </div>
    </div>
    <div id="celda_p50">
        <div id="campo_per30">
            <label>
                Certif. Psicofisico:</label></div>
        <div id="campo_per50bis">
            <div id="check">
                Si</div>
            <asp:RadioButton CssClass="check2" GroupName="radioPsicofisico" ID="rpsSi" runat="server" />
            <div id="check">
                No</div>
            <asp:RadioButton CssClass="check2" GroupName="radioPsicofisico" ID="rpsNo" runat="server" />
        </div>
    </div>
    <div id="celda_p50">
        <div id="campo_per30">
            <label>
                Vencimiento:</label></div>
        <div id="campo_per50bis">
            <asp:TextBox CssClass="campo_per50" ID="txtVPsicofisico" runat="server"> </asp:TextBox></div>
    <div id="campo_per_icono">
        <asp:FileUpload ID="FileUploadPsicofisico" runat="server" CssClass="subir" Width="18"
            Height="1" border="0"></asp:FileUpload>
    </div>
    </div>
    <div id="celda_p50">
        <div id="campo_per30">
            <label>
                Carnet Psicofisico:</label></div>
        <div id="campo_per50bis">
            <div id="check">
                Si.</div>
            <asp:RadioButton CssClass="check2" GroupName="radioPsicoTipo" ID="rpscSi" runat="server" />
            <div id="check">
                No</div>
            <asp:RadioButton CssClass="check2" GroupName="radioPsicoTipo" ID="rpscNo" runat="server" />
        </div>
    </div>
    <div id="celda_p50">
        <div id="campo_per30">
            <label>
                Vencimiento:</label></div>
        <div id="campo_per50bis">
            <asp:TextBox CssClass="campo_per50" ID="txtVCPsicofisico" runat="server"> </asp:TextBox></div>
    <div id="campo_per_icono">
        <asp:FileUpload ID="FileUpload1" runat="server" CssClass="subir" Width="18"
            Height="1" border="0"></asp:FileUpload>
    </div>
    </div>
    <div id="celda_p50">
        <div id="campo_per30">
            <label>
                Cargas Grales:</label></div>
        <div id="campo_per50bis">
            <div id="check">
                Si</div>
            <asp:RadioButton CssClass="check2" GroupName="radioCargasGrales" ID="rcgSi" runat="server" />
            <div id="check">
                No</div>
            <asp:RadioButton CssClass="check2" GroupName="radioCargasGrales" ID="rcgNo" runat="server" />
        </div>
    </div>
    <div id="celda_p50">
        <div id="campo_per30">
            <label>
                Vencimiento:</label></div>
        <div id="campo_per50bis">
            <asp:TextBox class="campo_per50" type="text" ID="txtVCargasGrales" runat="server"> </asp:TextBox>
        </div>
        <div id="campo_per_icono">
            <asp:FileUpload ID="FileUploadCargasGrales" runat="server" CssClass="subir" Width="18"
                Height="1" border="0"></asp:FileUpload>
        </div>
    </div>
    <div id="celda_p50">
        <div id="campo_per30">
            <label>
                Cargas Peligrosas:</label></div>
        <div id="campo_per50bis">
            <div id="check">
                Si</div>
            <asp:RadioButton CssClass="check2" GroupName="radioCargasPeligrosas" ID="rcpSi" runat="server" />
            <div id="check">
                No</div>
            <asp:RadioButton CssClass="check2" GroupName="radioCargasPeligrosas" ID="rcpNo" runat="server" />
        </div>
    </div>
    <div id="celda_p50">
        <div id="campo_per30">
            <label>
                Vencimiento:</label></div>
        <div id="campo_per50bis">
            <asp:TextBox CssClass="campo_per50" ID="txtVCargasPeligrosas" runat="server"> </asp:TextBox>
        </div>
        <div id="campo_per_icono">
            <asp:FileUpload ID="FileUploadCargasPeligrosas" runat="server" CssClass="subir" Width="18"
                Height="1" border="0"></asp:FileUpload>
        </div>
    </div>
    <div id="celda_p50">
        <div id="campo_per30">
            <label>
                Manejo Defensivo:</label></div>
        <div id="campo_per50bis">
            <div id="check">
                Si</div>
            <asp:RadioButton CssClass="check2" GroupName="radioManejoDefensivo" ID="rmdSi" runat="server" />
            <div id="check">
                No</div>
            <asp:RadioButton CssClass="check2" GroupName="radioManejoDefensivo" ID="rmdNo" runat="server" />
        </div>
    </div>
    <div id="celda_p50">
        <div id="campo_per30">
            <label>
                Vencimiento:</label></div>
        <div id="campo_per50bis">
            <asp:TextBox CssClass="campos" ID="txtVManejoDefensivo" runat="server"> </asp:TextBox>
        </div>
        <div id="campo_per_icono">
            <asp:FileUpload ID="FileUploadMDefensivo" runat="server" CssClass="subir" Width="18"
                Height="1" border="0"></asp:FileUpload>
        </div>
    </div>
    <div id="celda_p50">
        <div id="campo_per30">
            <label>
                Empresa:</label></div>
        <div id="campo_per50">
            <asp:TextBox CssClass="campos" ID="txtEmpresaCNRT" runat="server"> </asp:TextBox>
        </div>
    </div>
    <div id="celda_p50">
        <div id="campo_per30">
            <label>
                Cert. CNRT:</label></div>
        <div id="campo_per50">
            <asp:TextBox CssClass="campo_per50" ID="txtCNRT" runat="server"> </asp:TextBox>
        </div>
    </div>
    <div id="campo_p100">
        <div id="campo_per50">
            <asp:Button ID="uno" runat="server" BackColor="LightGray" Text="1" Enabled="false"
                OnClick="btn_uno_Click" />
        </div>
        <div id="campo_per50">
            <asp:Button ID="dos" runat="server" BackColor="LightGray" Text="2" Enabled="false" />
        </div>
        <div id="campo_per50">
            <asp:Button ID="btn_archivos" runat="server" BackColor="LightGray" Text="Archivos"
                Enabled="false" Visible="false" OnClick="btn_cargarArchivos_click" />
        </div>
    </div>
    <div id="celda_pbtns">
        <asp:Button ID="btn_Guardar" CssClass="btn_personal" Text="GUARDAR" runat="server"
            OnClick="btn_Guardar_Click" />
        <asp:Button ID="btn_Cancelar" CssClass="btn_personal" Text="CANCELAR" runat="server"
            OnClick="btn_cancelar_Click" />
    </div>
    <%-- Espacio de PopUp de Archivos --%>
    <div id="capaPopUp">
    </div>
    <div id="popUpDiv">
        <div id="capaContent">
            <div style="width: 90%; padding-top: 5%; padding-left: 10%;	padding-bottom: 5%; height:75%; overflow:auto;">
                <asp:GridView ID="gvPersonalFiles" runat="server" AutoGenerateColumns="False" DataMember="idPersonal"
                    CssClass="columna_per" HeaderStyle-CssClass="celda1" OnRowCommand="gvPersonalFiles_RowCommand"
                    OnRowDataBound="gvPersonalFiles_RowDataBound" AllowPaging="true" PageSize="20" OnPageIndexChanging="gvPersonalFiles_PageIndexChanging"
                    PagerStyle-CssClass="tabla_font" EmptyDataText="No existen Archivos" EmptyDataRowStyle-CssClass="celda50" Width="95%" 
                    EmptyDataRowStyle-Width="80%">
                    <Columns>
                        <asp:TemplateField HeaderText="Nombre del Archivo" HeaderStyle-CssClass="columna_per"
                            ItemStyle-CssClass="columna_per">
                            <ItemTemplate>
                                <asp:Label ID="lblNombreArchivo" runat="server" Text='<%# Bind("Nombre") %>' ItemStyle-CssClass="columna5" ></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <%--<asp:TemplateField HeaderText="Ruta del Archivo" HeaderStyle-CssClass="columna5"
                            ItemStyle-CssClass="columna3">
                            <ItemTemplate>
                                <asp:Label ID="lblRutaArchivo" runat="server" Text='<%# Bind("Ruta") %>' ItemStyle-CssClass="columna3" ></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Fecha de Creacion" HeaderStyle-CssClass="columna7" ItemStyle-CssClass="columna7">
                            <ItemTemplate>
                                <asp:Label ID="lblFecha" runat="server" Text='<%# Bind("Fecha") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>--%>
                        <asp:TemplateField ItemStyle-CssClass="btn_iconos">
                            <ItemTemplate>
                                <asp:LinkButton ID="btnVer" runat="server" title="Ver Archivo" CssClass="ver" OnClientClick='<%# string.Format("javascript:return AbrirVentana(\"{0}\")",Eval("idPersonalFiles")) %>'></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-CssClass="btn_iconos">
                            <ItemTemplate>
                                <asp:LinkButton ID="btnEliminar" runat="server" title="Eliminar Archivo" CssClass="eliminar"
                                    CommandArgument='<%# Bind("idPersonalFiles") %>' CommandName="Eliminar" OnClientClick="return Eliminar('Archivo');"></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>
            <div id="botones" style="width: 95%; height: 10%;">
                <div class="regreso_3">
                    <input type="button" onclick="cerrarPopUp();" title="Cerrar" id="btnCerrar" class="btn_form"
                        value="CERRAR" />
                </div>
            </div>
        </div>
    </div>
</asp:Content>
