﻿<%@ Page Title="" Language="C#" MasterPageFile="~/frmPrincipal.Master" AutoEventWireup="true" CodeBehind="frmPermisoExtendido.aspx.cs" Inherits="ProcesoPP.UI.Vehiculo.frmPermisoExtendido" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<link href="../Css/estilos.css" rel="stylesheet" type="text/css" />
<script src="../Js/jquery.maskedinput.js" type="text/javascript"></script>
    
<script type="text/javascript">
    $(document).ready(function () { SetMenu("liPersonal"); });
    jQuery(function ($) {
        $("#<%=txtFecha.ClientID%>").mask("99/99/9999", { placeholder: "dd/mm/yyyy" });

    });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div id="apertura">
        Alta | Concesion de Permiso Extraordinario</div>
    <div class="tabla_admin">
        <div class="sol_adm100">
            <div class="label_admint">
                <label>
                    Descripción:
                </label>
            </div>
            <div class="label_adminc">
                <asp:TextBox ID="txtDescripcion" CssClass="sol_campoadm" runat="server"></asp:TextBox></div>
            <div class="label_admint">
            </div>
            
             <div class="label_admint">
            </div>
            <div class="label_admint">
                <label>
                    Valido Desde:</label></div>
            <div class="label_adminc">
                <asp:TextBox ID="txtFechaOriginal" CssClass="sol_campoadm" Enabled="False" runat="server"></asp:TextBox></div>
            <div class="label_admint">
            </div>
            <div class="label_admint">
            </div>
            <div class="label_admint">
                <label>
                    Valido Hasta:</label></div>
            <div class="label_adminc">
                <asp:TextBox ID="txtFecha" CssClass="sol_campoadm" runat="server"></asp:TextBox></div>
            <div class="label_admint">
            </div>
            <div class="label_admint">
            </div>
            <div class="label_relleno20">
            </div>
            <div class="label_admint">
            </div>
            <div class="label_admint">
            </div>
            <div class="label_admintb">
                <asp:Button ID="btnGuardar" runat="server" Visible="true" Text="Guardar" CssClass="btn_form" OnClick="btnGuardar_Click"
                    OnClientClick="return ValidarObj('txtDescripcion|Ingrese una Descripción.'+'~txtFecha|Ingrese una Fecha');" />
               </div>
               <div class="label_admintb">
                <asp:Button ID="btn_Cancelar" CssClass="btn_form" Text="Cancelar" runat="server"
            OnClick="btn_cancelar_Click" />
            </div>
            
        </div>
    </div>
    <div id="divMensaje" runat="server">
        <asp:Label ID="lblError" runat="server" Text="Error" Visible="false"></asp:Label>
    </div>
    <div>
        <asp:GridView ID="gvPermiso" runat="server" AutoGenerateColumns="False" DataMember="idPermiso"
            CssClass="tabla3" OnRowCancelingEdit="gvPermiso_RowCancelingEdit" OnRowEditing="gvPermiso_RowEditing"
            OnRowUpdating="gvPermiso_RowUpdating" OnRowDeleting="gvPermiso_RowDeleting"
            OnRowDataBound="gvPermiso_RowDataBound">
            <Columns>
                <asp:TemplateField HeaderText="Id Permiso" HeaderStyle-CssClass="columna5" ItemStyle-CssClass="columna5"
                    Visible="false">
                    <EditItemTemplate>
                        <asp:Label ID="lblIdPermiso" runat="server" Text='<%# Bind("idPermiso") %>'></asp:Label>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lblPermisoI" runat="server" Text='<%# Bind("idPermiso") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Descripción" HeaderStyle-CssClass="columna5" ItemStyle-CssClass="columna5">
                    <EditItemTemplate>
                        <asp:TextBox ID="txtDescripcion" runat="server" Text='<%# Bind("Descripcion") %>'></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lblDescripcion" runat="server" Text='<%# Bind("Descripcion") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Fecha Desde" HeaderStyle-CssClass="columna5" ItemStyle-CssClass="columna5">
                    <EditItemTemplate>
                        <asp:TextBox ID="txtFechaOriginal" runat="server" Text='<%# Bind("FechaOriginal") %>'></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lblFechaOriginal" runat="server" Text='<%# Bind("FechaOriginal") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Fecha Hasta" HeaderStyle-CssClass="columna5" ItemStyle-CssClass="columna5">
                    <EditItemTemplate>
                        <asp:TextBox ID="txtFecha" runat="server" Text='<%# Bind("Fecha") %>'></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lblFecha" runat="server" Text='<%# Bind("Fecha") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                
            </Columns>
        </asp:GridView>
    </div>
</asp:Content>
