﻿<%@ Page Title="PROCESOS PATAGONICOS | Sistema Interno" Language="C#" MasterPageFile="~/frmPrincipal.Master" EnableEventValidation="false"
    AutoEventWireup="true" CodeBehind="frmPersonalCarga.aspx.cs" Inherits="ProcesoPP.UI.Administracion.frmPersonalCarga"
    MaintainScrollPositionOnPostback="true" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../Css/personal.css" rel="stylesheet" type="text/css" />
    <script src="../Js/jquery.maskedinput.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () { SetMenu("liPersonal"); });
        function verificarForm() {
            msj = 'txtLegajo|Ingrese Nro. de Legajo.' +
                '~txtApellido|Debe ingresar Apellido(s)' +
                '~txtNombre|Debe ingresar Nombre(s).' +
                '~txtFechaNac|Debe ingresar fecha de nacimiento.' +
                '~ddlSexo|Debe ingresar el sexo.' +
                '~ddlEstadoCivil|Debe ingresar el DNI.' +
                '~ddlEstudios|Debe seleccionar los estudios.' +
                '~ddlNacionalidad|Debe seleccionar Nacionalidad.' +
                '~ddlPaisOrigen|Debe seleccionar el País de origen.' +
                '~ddlProvincia|Debe seleccionar la Provincia.' +
                '~ddlLocalidad|Debe seleccionar la Localidad.' +
                '~txtDomicilio|Debe ingresar un domicilio.' +
                '~txtTelefono|Debe ingresar un número de telefono.' +
                '~txtArea|Debe ingresar un código de área' +
                '~txtCUIL|Debe ingresa CUIL.' +
                '~txtMail|Debe ingresar un email valido.';
            val = ValidarObj(msj);
            hfEsValido = document.getElementById('<%=hfEsValido.ClientID%>');
            if (val) {
                hfEsValido.value = true;
                return true;
            }
            else
                return val;

        }

        jQuery(function ($) {
            $("#<%=txtFechaNac.ClientID%>").mask("99/99/9999", { placeholder: "dd/mm/yyyy" });
            
        });
        jQuery(function ($) {
            $("#<%=txtCUIL.ClientID%>").mask("99-99999999-9");

        });

        function AbrirVentana(url){

           window.open("../Administracion/VerAdjunto.aspx?idArchivo="+url+"&esPersonal=true", '');
           
        }

    </script>
</asp:Content>
<asp:content id="Content2" contentplaceholderid="ContentPlaceHolder1" runat="server">
    <asp:HiddenField ID="hfEsValido" Value="false" runat="server" />
    <div id="ape_personal">
        Personal. Alta de Legajo
    </div>
    <div id="tit_personal">
        Personales
    </div>
    <div id="celda_p100">
        <div id="campo_per30">
            <label>
                Legajo:</label></div>
        <div id="campo_per70">
            <asp:TextBox CssClass="campos" ID="txtLegajo" runat="server"> </asp:TextBox>
        </div>
    </div>
    <div id="celda_p100">
        <div id="campo_per30">
            <label>
                Nombres:</label></div>
        <div id="campo_per70">
            <asp:TextBox CssClass="campos" ID="txtNombre" runat="server"> </asp:TextBox>
        </div>
    </div>
    <div id="celda_p100">
        <div id="campo_per30">
            <label>
                Apellidos:</label></div>
        <div id="campo_per70">
            <asp:TextBox CssClass="campos" ID="txtApellido" runat="server"> </asp:TextBox>
        </div>
    </div>
    <div id="celda_p50">
        <div id="campo_per30">
            <label>
                Fecha Nac:</label>
        </div>
        <div id="campo_per50bis">
         <asp:TextBox CssClass="campos" ID="txtFechaNac" runat="server" > </asp:TextBox>
         
        </div>
        <div id="campo_per_icono">
            <img id="calendario" alt="calendario" src="../img/calendario.gif" style="width: 15px;
                height: 15px; cursor: pointer;" onmouseover="CambiarConfiguracionCalendario('txtFechaNac',this);" /></div>
    </div>
    <div id="celda_p50">
        <div id="campo_per30">
            <label>
                Sexo:</label></div>
        <div id="campo_per50">
            <asp:DropDownList CssClass="sol_campo50c" ID="ddlSexo" runat="server" />
        </div>
    </div>
    <div id="celda_p50">
        <div id="campo_per30">
            <label>
                DNI.</label>
        </div>
        <div id="campo_per50bis">
            <asp:TextBox CssClass="campos" ID="txtDNI" MaxLength="10" runat="server" onkeypress="return ValidarNumeroEntero(event,this);"> </asp:TextBox>
        </div>
        <div id="campo_per_icono">
            <asp:FileUpload ID="FileUploadDNI" runat="server" CssClass="subir" Width="18" Height="1"
                border="0"></asp:FileUpload>
        </div>
    </div>
    <div id="celda_p50">
        <div id="campo_per30">
            <label>
                CUIL:</label></div>
        <div id="campo_per50bis">
            <asp:TextBox CssClass="campos" ID="txtCUIL" runat="server"> </asp:TextBox>
        </div>
        <div id="campo_per_icono">
            <asp:FileUpload ID="FileUploadCUIL" runat="server" CssClass="subir" Width="18" Height="1"
                border="0"></asp:FileUpload>
        </div>
    </div>
    <div id="celda_p50">
        <div id="campo_per30">
            <label>
                Estado Civil:</label></div>
        <div id="campo_per50">
            <asp:DropDownList CssClass="sol_campo50c" ID="ddlEstadoCivil" runat="server" />
        </div>
    </div>
    <div id="celda_p50">
        <div id="campo_per30">
            <label>
                Estudios:</label></div>
        <div id="campo_per50">
            <asp:DropDownList CssClass="sol_campo50c" ID="ddlEstudios" runat="server" />
        </div>
        <div id="campo_per_icono">
            <asp:FileUpload ID="FileUploadEstudios" runat="server" CssClass="subir" Width="18"
                Height="1" border="0"></asp:FileUpload>
        </div>
    </div>
    <div id="tit_personal">
        Información de Residencia
    </div>
    <div id="celda_p50">
        <div id="campo_per30">
            <label>
                Nacionalidad:</label></div>
        <div id="campo_per50">
            <asp:DropDownList CssClass="sol_campo50c" ID="ddlNacionalidad" runat="server" />
        </div>
    </div>
    <div id="celda_p50">
        <div id="campo_per30">
            <label>
                País Origen:</label></div>
        <div id="campo_per50">
            <asp:DropDownList CssClass="sol_campo50c" ID="ddlPaisOrigen" runat="server" />

        </div>
    </div>
    <cc1:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
        </cc1:ToolkitScriptManager>
    <div id="celda_p50">
        <div id="campo_per30">
            <label>
                Provincia:</label></div>
        <div id="campo_per50">
            <asp:DropDownList CssClass="sol_campo50c" ID="ddlProvincia" runat="server" />
            <cc1:CascadingDropDown ID="cddProvincia" TargetControlID="ddlProvincia" PromptText="Seleccione..." PromptValue="0" 
                ServiceMethod="GetProvincias" runat="server" Category="idProvincia" 
                LoadingText="Cargando..."/>
        </div>
    </div>
    <div id="celda_p50">
        <div id="campo_per30">
            <label> Localidad:</label></div>
        <div id="campo_per50">
            <asp:DropDownList CssClass="sol_campo50c" ID="ddlLocalidad" runat="server" />
            <cc1:CascadingDropDown ID="cddLocalidad" TargetControlID="ddlLocalidad" PromptText="Seleccione..." PromptValue="0" 
                ServiceMethod="GetLocalidad" runat="server" Category="idLocalidad" 
                ParentControlID="ddlProvincia" LoadingText="Cargando..." />
        </div>
    </div>
    <div id="celda_p100">
        <div id="campo_per30">
            <label>
                Domicilio:</label></div>
        <div id="campo_per70bis">
            <asp:TextBox CssClass="campos" ID="txtDomicilio" runat="server"> </asp:TextBox>
        </div>
        <div id="campo_per_icono">
            <asp:FileUpload ID="FileUploadDomicilio" runat="server" CssClass="subir" Width="18"
                Height="1" border="0"></asp:FileUpload>
        </div>
    </div>
    <div id="celda_p50">
        <div id="campo_per30">
            <label>
                Teléfono:</label></div>
        <div id="campo_per50">
            <asp:TextBox CssClass="campos" ID="txtTelefono" runat="server" onkeypress="return ValidarNumeroEntero(event,this);"> </asp:TextBox>
        </div>
    </div>
    <div id="celda_p50">
        <div id="campo_per30">
            <label>
                Mail:</label></div>
        <div id="campo_per50">
            <asp:TextBox CssClass="campos" ID="txtMail" runat="server"> </asp:TextBox>
        </div>
    </div>
    <div id="tit_personal">
        Contacto de Emergencia</div>
    <div id="celda_p100">
        <div id="campo_per30">
            <label>
                Nombres:</label></div>
        <div id="campo_per70">
            <asp:TextBox CssClass="campos" ID="txtEmergenciaNombres" runat="server"> </asp:TextBox>
        </div>
    </div>
    <div id="celda_p100">
        <div id="campo_per30">
            <label>
                Apellidos:</label></div>
        <div id="campo_per70">
            <asp:TextBox CssClass="campos" ID="txtEmergenciaApellidos" runat="server"> </asp:TextBox>
        </div>
    </div>
    <div id="celda_p50">
        <div id="campo_per30">
            <label>
                Relación:</label></div>
        <div id="campo_per50">
            <asp:TextBox CssClass="campos" ID="txtEmergenciaRelacion" runat="server"> </asp:TextBox>
        </div>
    </div>
    <div id="celda_p50">
        <div id="campo_per30">
            <label>
                Teléfono:</label></div>
        <div id="campo_per50">
            <asp:TextBox CssClass="campos" ID="txtEmergenciaTelefono" runat="server" onkeypress="return ValidarNumeroEntero(event,this);"> </asp:TextBox>
        </div>
    </div>
    <%--<div id="tit_personal">
        Datos de Acceso al Sistema</div>
    <div id="celda_p50">
        <div id="campo_per30">
            <label>
                Usuario:</label></div>
        <div id="campo_per50">
            <asp:TextBox CssClass="campos" ID="txtUsuario" runat="server" > </asp:TextBox>
        </div>
    </div>
    <div id="celda_p50">
        <div id="campo_per30">
            <label>
                Contraseña:</label></div>
        <div id="campo_per50">
            <asp:TextBox runat="server" CssClass="login_campo" ID="txtClave" TextMode="Password"></asp:TextBox>
        </div>
    </div>
    <div id="celda_p50">
        <div id="campo_per30">
            <label>
                Tipo de Usuario:</label></div>
        <div id="campo_per50">
            <asp:DropDownList ID="ddlTipoUsuario" runat="server" CssClass="sol_campo50c" />
        </div>
    </div>
    <div id="celda_p50">
        <div id="campo_per30">
            <label>
                Repita Contraseña:</label></div>
        <div id="campo_per50">
            <asp:TextBox runat="server" CssClass="campos" ID="txtClaveRepetida" TextMode="Password"></asp:TextBox>
        </div>
    </div>--%>
    <div id="celda_p100">
        <div id="campo_per50">
            <asp:Button ID="uno" runat="server" BackColor="LightGray" Text="1" Enabled="false" />
        </div>
        <div id="campo_per50">
            <asp:Button ID="dos" runat="server" BackColor="LightGray" Text="2" Enabled="false"
                OnClick="btn_dos_Click" />
        </div>
        <div id="campo_per50">
            <asp:Button ID="btn_archivos" runat="server" BackColor="LightGray" Text="Archivos"
                Enabled="false" Visible="false" OnClick="btn_cargarArchivos_click" />
        </div>
        
    </div>
    <div id="celda_pbtns">
        <asp:Button ID="btn_siguiente" CssClass="btn_personal" Text="SIGUIENTE" runat="server"
            OnClientClick="return verificarForm()" OnClick="btn_siguiente_Click" />
        <asp:Button ID="btn_cancelar" CssClass="btn_personal" Text="CANCELAR" runat="server"
            OnClick="btn_cancelar_Click" />
    </div>
    <%-- Espacio de PopUp de Archivos --%>
    <div id="capaPopUp">
    </div>
    <div id="popUpDiv">
        <div id="capaContent">
            <div style="width: 90%; padding-top: 5%; padding-left: 10%; padding-bottom: 5%; height: 75%;
                overflow: auto;">
                <asp:GridView ID="gvPersonalFiles" runat="server" AutoGenerateColumns="False" DataMember="idPersonal"
                    CssClass="columna_per" HeaderStyle-CssClass="celda1" OnRowCommand="gvPersonalFiles_RowCommand"
                    OnRowDataBound="gvPersonalFiles_RowDataBound" AllowPaging="true" PageSize="20"
                    OnPageIndexChanging="gvPersonalFiles_PageIndexChanging" PagerStyle-CssClass="tabla_font"
                    EmptyDataText="No existen Archivos" EmptyDataRowStyle-CssClass="celda50" Width="95%"
                    EmptyDataRowStyle-Width="80%">
                    <Columns>
                        <asp:TemplateField HeaderText="Nombre del Archivo" HeaderStyle-CssClass="columna_per"
                            ItemStyle-CssClass="columna_per">
                            <ItemTemplate>
                                <asp:Label ID="lblNombreArchivo" runat="server" Text='<%# Bind("Nombre") %>' ItemStyle-CssClass="columna5"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <%--<asp:TemplateField HeaderText="Ruta del Archivo" HeaderStyle-CssClass="columna5"
                            ItemStyle-CssClass="columna3">
                            <ItemTemplate>
                                <asp:Label ID="lblRutaArchivo" runat="server" Text='<%# Bind("Ruta") %>' ItemStyle-CssClass="columna3" ></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Fecha de Creacion" HeaderStyle-CssClass="columna7" ItemStyle-CssClass="columna7">
                            <ItemTemplate>
                                <asp:Label ID="lblFecha" runat="server" Text='<%# Bind("Fecha") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>--%>
                        <asp:TemplateField ItemStyle-CssClass="btn_iconos">
                            <ItemTemplate>
                                <asp:LinkButton ID="btnVer" runat="server" OnClientClick='<%# string.Format("javascript:return AbrirVentana(\"{0}\")",Eval("idPersonalFiles")) %>' title="Ver Archivo" CssClass="ver" ></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-CssClass="btn_iconos">
                            <ItemTemplate>
                                <asp:LinkButton ID="btnEliminar" runat="server" title="Eliminar Archivo" CssClass="eliminar"
                                    CommandArgument='<%# Bind("idPersonalFiles") %>' CommandName="Eliminar" OnClientClick="return Eliminar('Archivo');"></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>
            <div id="botones" style="width: 95%; height: 10%;">
                <div class="regreso_3">
                    <input type="button" onclick="cerrarPopUp();" title="Cerrar" id="btnCerrar" class="btn_form"
                        value="CERRAR" />
                </div>
            </div>
        </div>
    </div>
</asp:content>
