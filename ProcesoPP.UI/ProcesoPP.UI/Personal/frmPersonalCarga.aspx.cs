﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ProcesoPP.Business;
using ProcesoPP.Model;
using ProcesoPP.Services;
using System.Data;
using System.Configuration;
using System.IO;
using System.Diagnostics;
using System.Web.Services;
using AjaxControlToolkit;
using ParteDiario.Services;
using ProcesoPP.UI.Personal;

namespace ProcesoPP.UI.Administracion
{
    public partial class frmPersonalCarga : System.Web.UI.Page
    {
        #region <PROPIEDADES>

        public int _idUsuario
        {
            get { return (int)ViewState["idUsuario"]; }
            set { ViewState["idUsuario"] = value; }
        }

        public int _ADM
        {
            get { return int.Parse(ConfigurationManager.AppSettings["ADM"].ToString()); }
        }

        public int _idPersonal
        {
            get { return (int)ViewState["idPersonal"]; }
            set { ViewState["idPersonal"] = value; }
        }

        public int _Legajo {
            get { return (int)ViewState["legajo"]; }
            set { ViewState["legajo"] = value; }
        }

        public int _idContacto
        {
            get { return (int)ViewState["idContacto"]; }
            set { ViewState["idContacto"] = value; }
        }

        public Permisos _Permiso
        {
            get { return (Permisos)ViewState["permiso"]; }
            set { ViewState["permiso"] = value; }
        }


        public bool _Modificando
        {
            get { return (bool)ViewState["Modificando"]; }
            set { ViewState["Modificando"] = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                _idUsuario = Common.EvaluarSession(Request, this.Page);
                if (_idUsuario != 0)
                {
                    _Permiso = Common.getModulo(Request, this.Page);
                    if (_Permiso != null && (_Permiso.Lectura && _Permiso.Escritura))
                    {
                        if (!IsPostBack)
                        {

                            InitScreen();
                        }
                    }
                    else
                    {
                        Mensaje.errorMsj("NO TIENE PERMISOS A ESTE FORMULARIO", this.Page, "SIN ACCESO", "../frmPrincipal.aspx");
                    }
                }
                else
                {
                    Mensaje.errorMsj("DEBE LOGUEARSE", this.Page, "LOGIN", "../frmLogin.aspx");
                }
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

       

        protected void btn_siguiente_Click(object sender, EventArgs e)
        {
            try
            {
                if (bool.Parse(hfEsValido.Value))
                {
                    GuardarPersonal();
                    GuardarArchivos();
                    Mensaje.successMsj("Datos guardados correctamente", this.Page, "Guardado", "frmPersonalCarga2.aspx?idPersonal=" + this._idPersonal + "&idUsuario=" + this._idUsuario + "&idModulo=" + this._Permiso.oModulo.idModulo + "&Modificando=" + (this._Modificando).ToString() + "&Enabled=" + txtLegajo.Enabled.ToString());
                }
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        

        public void btn_cancelar_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("frmPersonalGestion.aspx?idUsuario=" + this._idUsuario + "&idModulo=" + this._Permiso.oModulo.idModulo);
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        protected void btn_dos_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("frmPersonalCarga2.aspx?idPersonal=" + this._idPersonal + "&idUsuario=" + this._idUsuario + "&idModulo=" + this._Permiso.oModulo.idModulo + "&Modificando=" + (this._Modificando).ToString() + "&Enabled=" + txtLegajo.Enabled.ToString());
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        protected void btn_cargarArchivos_click(object sender, EventArgs e)
        {
            try
            {
                gvPersonalFiles.Visible = true;
                CargarPersonalFiles();
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        

        protected void gvPersonalFiles_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            switch (e.CommandName)
            {

                //case "Ver":
                //    string ruta = e.CommandArgument.ToString();
                //    if (ruta.EndsWith("pdf"))
                //    {
                //        Response.ContentType = "Application/pdf";
                //        Response.TransmitFile(ruta);
                //    }
                //    else if (ruta.EndsWith("jpg"))
                //    {
                //        Response.ContentType = "image/jpeg";
                //        Response.TransmitFile(ruta);
                //    }
                //    else if (ruta.EndsWith("jpeg"))
                //    {
                //        Response.ContentType = "image/pjpeg";
                //        Response.TransmitFile(ruta);
                //    }
                //    else if (ruta.EndsWith("png"))
                //    {
                //        Response.ContentType = "image/png";
                //        Response.TransmitFile(ruta);
                //    }
                //    else if (ruta.EndsWith("bmp"))
                //    {
                //        Response.ContentType = "image/bmp";
                //        Response.TransmitFile(ruta);
                //    }
                //    else
                //    {
                //        Response.TransmitFile(ruta);
                //    }
                //    break;
                case "Eliminar":
                    try
                    {
                        Eliminar(int.Parse(e.CommandArgument.ToString()));
                    }
                    catch (Exception ex)
                    {
                        Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
                    }
                    break;
            }
        }

        protected void gvPersonalFiles_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
                CargarRow(e);
        }

        protected void gvPersonalFiles_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvPersonalFiles.PageIndex = e.NewPageIndex;
            CargarPersonalFiles();
        }

        protected void ddlProvincia_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddlProvincia.SelectedIndex > 0)
                {
                    //CargarLocalidades(ListaCargarLocalidades());
                }
                else
                {
                    ddlLocalidad.Visible = false;
                }
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        private void CargarRow(GridViewRowEventArgs e)
        {
            LinkButton btnEliminar = e.Row.FindControl("btnEliminar") as LinkButton;
            Common.CargarRow(e, _Permiso);


            if (_Permiso.oTipoUsuario.idTipoUsuario == _ADM)
                btnEliminar.Visible = true;
            else
                btnEliminar.Visible = false;

        }

        #endregion

        #region METODOS

        private void InitScreen()
        {
            CargarPaises();
            GetProvincias("","");
            CargarNacionalidades();
            CargarSexo();
            CargarEstadoCivil();
            CargarEstudios();
            if (Request["idPersonal"] != null)
            {

                Model.Personal oPersonal;
                PersonalBus oPersonalBus = new PersonalBus();
                if (Request["Modificando"] != null)
                    _Modificando = bool.Parse(Request["Modificando"]);
                else
                    _Modificando = true;
                this.uno.Enabled = false;
                this.dos.Enabled = true;
                int id = int.Parse(Request["idPersonal"]);
                _idPersonal = id;
                oPersonal = oPersonalBus.PersonalGetById(id);
                _Legajo = oPersonal.NroLegajo;
                CargarPersonalExistente(oPersonal);
                if (_Modificando == false)
                {
                    Common.AnularControles(this.Form);
                    this.btn_siguiente.Visible = false;
                    this.dos.Enabled = true;
                    this.btn_archivos.Visible = true;
                    this.btn_archivos.Enabled = true;
                    this.btn_cancelar.Enabled = true;
                    
                }

            }
            else
            {
                SugerirLegajo();
                _Modificando = false;
                this.uno.Visible = false;
                this.dos.Visible = false;
            }


            if (!_Permiso.Escritura)
            {
                btn_siguiente.Visible = false;
            }
        }

        private bool esFecha(TextBox t)
        {
            string cadena = t.Text;
            bool esValida = true;
            if( (cadena.IndexOf('/') < 0) && (!esNumerico(t)) )
            {
                esValida = false;
            }
            return esValida;
        }

        private bool esNumerico(TextBox t)
        {
            string cadena = t.Text;
            bool esValida = true;
            int pos=0;
            while ((esValida) && (cadena.Length-1 < pos))
            {
                if (!char.IsNumber(cadena,pos) )
                {
                    esValida = false;
                }
            }
            return esValida;
        }

        private void CargarOrigen(int idLocalidad)
        {
            LocalidadBus oLocalidadBus = new LocalidadBus();
            Localidad oLocalidad = oLocalidadBus.LocalidadGetById(idLocalidad);
            DepartamentoBus oDepartamentoBus = new DepartamentoBus();
            Departamento oDepto = oDepartamentoBus.DepartamentoGetById(oLocalidad.IdDepartamento);
            cddProvincia.SelectedValue = oDepto.IdProvincia.ToString();
            Provincia oProvincia = (new ProvinciaBus()).ProvinciaGetById(oDepto.IdProvincia);
            ddlPaisOrigen.SelectedValue = oProvincia.IdPais.ToString();
            GetLocalidad("idProvincia:"+cddProvincia.SelectedValue+";", "idLocalidad");
            cddLocalidad.SelectedValue = idLocalidad.ToString();



        }

        private void CargarPersonalExistente(Model.Personal oPersonal)
        {
            PersonalContacto oPersonalC;
            PersonalContactoBus oPersonalCBus = new PersonalContactoBus();
            ddlEstadoCivil.SelectedValue = oPersonal.IdEstadoCivil.ToString();
            ddlNacionalidad.SelectedValue = oPersonal.IdNacionalidad.ToString();
            ddlSexo.SelectedValue = oPersonal.IdSexo.ToString();
            ddlEstudios.SelectedValue = oPersonal.IdEstudios.ToString();
            //CargarOrigen(oPersonal.IdLocalidad);
            txtDNI.Text = oPersonal.DNI.ToString();
            CalcularCUIL((oPersonal.CUIL.ToString()), (oPersonal.DNI.ToString()));
            txtDomicilio.Text = oPersonal.Domicilio;
            txtFechaNac.Text = oPersonal.FechaNacimiento.ToString("dd/MM/yyyy");
            txtTelefono.Text = oPersonal.Telefono;
            txtMail.Text = oPersonal.Mail;
            txtApellido.Text = oPersonal.Apellido;
            txtNombre.Text = oPersonal.Nombres;
            txtLegajo.Text = oPersonal.NroLegajo.ToString();
            CargarOrigen(oPersonal.IdLocalidad);
            oPersonalC = oPersonalCBus.PersonalContactoGetById(oPersonal.IdPersonalContacto);
            if (oPersonalC != null)
            {
                _idContacto = oPersonal.IdPersonalContacto;
                txtEmergenciaApellidos.Text = oPersonalC.Apellido;
                txtEmergenciaNombres.Text = oPersonalC.Nombre;
                txtEmergenciaRelacion.Text = oPersonalC.Relacion;
                txtEmergenciaTelefono.Text = oPersonalC.Telefono.ToString();
            }

        }


        private void CalcularCUIL(string cuil, string dni)
        {
            txtCUIL.Text = cuil.Substring(0, 2) + "-" + dni + "-" + cuil.Substring(2);
        }

        private void SugerirLegajo()
        {
            int ultimo;
            List<Model.Personal> lista;
            PersonalBus oPersonalBus = new PersonalBus();
            lista = oPersonalBus.PersonalGetAll();
            if (lista.Count > 0)
            {
                ultimo = lista.Max(x => x.NroLegajo);//lista.Last();
                txtLegajo.Text = (ultimo + 1).ToString();
            }
            else
                txtLegajo.Text = "000000001";

        }




        private List<Localidad> ListaCargarLocalidades()
        {
            List<Departamento> deptos = new List<Departamento>();
            List<Localidad> localidades = new List<Localidad>();
            List<Localidad> aBuscar = new List<Localidad>();
            deptos = CargarDepartamentos();
            foreach (Departamento oDep in deptos)
            {
                LocalidadBus oLocalidad = new LocalidadBus();
                localidades = oLocalidad.LocalidadGetByDepartamento(oDep.IdDepartamento);
                foreach (Localidad oLoc in localidades)
                {
                    aBuscar.Add(oLoc);
                }
            }
            return aBuscar;
        }

        private List<Departamento> CargarDepartamentos()
        {
            DepartamentoBus oDeptoBus = new DepartamentoBus();
            return oDeptoBus.DepartamentoGetByProvincia(int.Parse(ddlProvincia.SelectedValue));

        }

        protected void CargarCUIL(object sender, EventArgs e)
        {
            txtCUIL.Text = ("XX-" + txtDNI.Text + "-XX");


        }

        private string CortarCUIL()
        {
            int aux;
            string r;
            aux = txtCUIL.Text.IndexOf('-');
            r = txtCUIL.Text.Substring(0, aux);
            aux = txtCUIL.Text.LastIndexOf('-');
            aux = aux + 1;
            return (r + (txtCUIL.Text.Substring(aux)));
        }

        private void GuardarPersonal()
        {
            bool tieneContacto = false;
            Model.Personal oPersonal;
            PersonalBus oPersonalBus = new PersonalBus();
            PersonalContactoBus oPersonalCBus = new PersonalContactoBus();
            PersonalContacto oPersonalC;
            if (_Modificando)
            {
                int id = int.Parse(Request["idPersonal"]);
                oPersonal = oPersonalBus.PersonalGetById(id);
                oPersonalC = oPersonalCBus.PersonalContactoGetById(oPersonal.IdPersonalContacto);
                if (oPersonalC.IdPersonalContacto == 0)
                {
                    oPersonalC = new PersonalContacto();
                }
            }
            else
            {
                oPersonal = new Model.Personal();
                oPersonalC = new PersonalContacto();
            }
            
            oPersonal.IdEstadoCivil = int.Parse(ddlEstadoCivil.SelectedValue);
            oPersonal.IdLocalidad = int.Parse(ddlLocalidad.SelectedValue);
            oPersonal.IdNacionalidad = int.Parse(ddlNacionalidad.SelectedValue);
            oPersonal.IdSexo = int.Parse(ddlSexo.SelectedValue);
            oPersonal.IdEstudios = int.Parse(ddlEstudios.SelectedValue);
            oPersonal.CUIL = int.Parse(CortarCUIL());
            oPersonal.DNI = int.Parse(txtDNI.Text);
            oPersonal.Domicilio = txtDomicilio.Text;
            if(esFecha(txtFechaNac))
                oPersonal.FechaNacimiento = DateTime.Parse(txtFechaNac.Text);
            if (esNumerico(txtTelefono))
                oPersonal.Telefono = txtTelefono.Text;
            
            oPersonal.Mail = txtMail.Text;
            oPersonal.Apellido = txtApellido.Text;
            oPersonal.Nombres = txtNombre.Text;
            if ((txtEmergenciaApellidos.Text != "") && (txtEmergenciaNombres.Text != ""))
            {
                tieneContacto = true;
                oPersonalC.Apellido = (txtEmergenciaApellidos.Text);
                oPersonalC.Nombre = (txtEmergenciaNombres.Text);
                oPersonalC.Relacion = (txtEmergenciaRelacion.Text);
            }
            if (txtEmergenciaTelefono.Text != "")
                oPersonalC.Telefono = txtEmergenciaTelefono.Text;

            if (_Modificando)
            {
                bool actualizo = oPersonalBus.PersonalUpdate(oPersonal);
                oPersonalCBus.PersonalContactoUpdate(oPersonalC);
                _idPersonal = oPersonal.IdPersonal;
            }
            else
            {
                if (txtLegajo.Text.Length > 0)
                {
                    oPersonal.NroLegajo = int.Parse(txtLegajo.Text);
                }
                else
                {

                    SugerirLegajo();
                    txtLegajo.Text = oPersonal.NroLegajo.ToString();

                }
                _Legajo = oPersonal.NroLegajo;
                if (tieneContacto)
                {
                    oPersonalC.IdPersonalContacto = oPersonalCBus.PersonalContactoAdd(oPersonalC);
                    oPersonal.IdPersonalContacto = oPersonalC.IdPersonalContacto;
                }

                _idPersonal = oPersonalBus.PersonalAdd(oPersonal);
            }
                                    
        }
        private void guardaIndividual(string nombreArchivo,ref List<PersonalFiles> archivos,string ruta, FileUpload control)
        {
            PersonalFiles nuevo = new PersonalFiles();
            string extension = control.FileName.ToString();
            extension = extension.Substring(extension.IndexOf('.'));
            string fecha = DateTime.Today.ToString("dd/MM/yyyy");
            fecha = fecha.Replace('/','-');
            nuevo.IdPersonal = _idPersonal;
            nuevo.Nombre = nombreArchivo+"_" + fecha + extension ;
            //nuevo.Ruta = Server.MapPath("~/Adjuntos/Personal/" + _Legajo + "/" + nuevo.Nombre);
            nuevo.Ruta = ruta + (nuevo.Nombre);
            archivos.Add(nuevo);
            control.SaveAs(ruta + nuevo.Nombre);
        }
        private void GuardarArchivos()
        {
            List<PersonalFiles> archivos = new List<PersonalFiles>();
            string ruta = Server.MapPath(@"~/Adjuntos/Personal/" + _Legajo.ToString() + "/");
            if (!Directory.Exists(ruta))
            {
                DirectoryInfo di = Directory.CreateDirectory(ruta);
            }
            
            if (FileUploadCUIL.HasFile)
            {
                guardaIndividual("CUIL",ref archivos,ruta,FileUploadCUIL);
            }
            if (FileUploadDNI.HasFile)
            {
                guardaIndividual("DNI",ref archivos,ruta,FileUploadDNI);
            }
            if (FileUploadDomicilio.HasFile)
            {
                guardaIndividual("DOMICILIO",ref archivos,ruta,FileUploadDomicilio);
            }
            if (FileUploadEstudios.HasFile)
            {
                guardaIndividual("ESTUDIOS",ref archivos,ruta,FileUploadEstudios);
            }
            foreach (PersonalFiles pf in archivos)
            {
                PersonalFilesBus oPFBus = new PersonalFilesBus();
                oPFBus.PersonalFilesAdd(pf);
            }
        }

        
        private void CargarPersonalFiles()
        {
            PersonalFilesBus oPersonalFilesBus = new PersonalFilesBus();
            gvPersonalFiles.DataSource = oPersonalFilesBus.getPersonalFilesByIdPersonal(_idPersonal);
            gvPersonalFiles.DataBind();
            Mensaje.mostrarPopUp(this);
        }

              

        private void Eliminar(int idPersonalFile)
        {
            PersonalFilesBus oPersonalFilesBus = new PersonalFilesBus();
            PersonalFiles oPersonalFiles = oPersonalFilesBus.PersonalFilesGetById(idPersonalFile);
            //System.IO.File.Delete(oPersonalFiles.Ruta); //Activar esta linea para permitir el borrado fisico de los archivos
            oPersonalFilesBus.PersonalFilesDelete(idPersonalFile);
           
        }

        private void CargarPaises()
        {
            PaisBus oPaisBus = new PaisBus();
            ddlPaisOrigen.DataSource = oPaisBus.PaisGetAll();
            ddlPaisOrigen.DataTextField = "Nombre";
            ddlPaisOrigen.DataValueField = "CodigoPais";
            ddlPaisOrigen.DataBind();
            ddlPaisOrigen.Items.Insert(0, new ListItem("Seleccione Pais", "0"));
        }

        private void CargarNacionalidades()
        {
            NacionalidadBus oNacionalidad = new NacionalidadBus();
            ddlNacionalidad.DataSource = oNacionalidad.NacionalidadGetAll();
            ddlNacionalidad.DataTextField = "Descripcion";
            ddlNacionalidad.DataValueField = "IdNacionalidad";
            ddlNacionalidad.DataBind();
            ddlNacionalidad.Items.Insert(0, new ListItem("Seleccione Nacionalidad", "0"));
        }

        //private void CargarProvincias()
        //{
        //    ProvinciaBus oProvincia = new ProvinciaBus();
        //    ddlProvincia.DataSource = oProvincia.ProvinciaGetAll();
        //    ddlProvincia.DataTextField = "Nombre";
        //    ddlProvincia.DataValueField = "IdProvincia";
        //    ddlProvincia.DataBind();
        //    ddlProvincia.Items.Insert(0, new ListItem("Seleccione Provincia", "0"));
        //}

        [WebMethod]
        public static CascadingDropDownNameValue[] GetProvincias(string knownCategoryValues, string category)
        {            
            ProvinciaBus oProvinciaBus = new ProvinciaBus();
            List<CascadingDropDownNameValue> Provincias = oProvinciaBus.GetDataProvincias();
            return Provincias.ToArray();
        }
        [WebMethod]
        public static CascadingDropDownNameValue[] GetLocalidad(string knownCategoryValues, string category)
        {
            string idProvincia = CascadingDropDown.ParseKnownCategoryValuesString(knownCategoryValues)["idProvincia"];
            LocalidadBus oLocalidadBus = new LocalidadBus();
            List<CascadingDropDownNameValue> Localidades = oLocalidadBus.GetDataLocalidad(int.Parse(idProvincia));
            return Localidades.ToArray();
        }

        //private void CargarLocalidades(List<Localidad> oLocal)
        //{

        //    ddlLocalidad.DataSource = oLocal;
        //    ddlLocalidad.DataTextField = "Nombre";
        //    ddlLocalidad.DataValueField = "IdLocalidad";
        //    ddlLocalidad.DataBind();
        //    ddlLocalidad.Items.Insert(0, new ListItem("Seleccione Localidad", "0"));
        //}


        private void CargarEstadoCivil()
        {
            EstadoCivilBus oEstadoCiv = new EstadoCivilBus();
            ddlEstadoCivil.DataSource = oEstadoCiv.EstadoCivilGetAll();
            ddlEstadoCivil.DataTextField = "Descripcion";
            ddlEstadoCivil.DataValueField = "IdEstadoCivil";
            ddlEstadoCivil.DataBind();
            ddlEstadoCivil.Items.Insert(0, new ListItem("Seleccione Estado Civil", "0"));
        }

        private void CargarSexo()
        {
            SexoBus oSexo = new SexoBus();
            ddlSexo.DataSource = oSexo.SexoGetAll();
            ddlSexo.DataTextField = "Descripcion";
            ddlSexo.DataValueField = "IdSexo";
            ddlSexo.DataBind();
            ddlSexo.Items.Insert(0, new ListItem("Seleccione Sexo", "0"));
        }

        private void CargarEstudios()
        {
            EstudiosBus oEstudios = new EstudiosBus();
            ddlEstudios.DataSource = oEstudios.EstudiosGetAll();
            ddlEstudios.DataTextField = "Descripcion";
            ddlEstudios.DataValueField = "IdEstudio";
            ddlEstudios.DataBind();
            ddlEstudios.Items.Insert(0, new ListItem("Seleccione Estudios", "0"));
        }

        #endregion



    }
}