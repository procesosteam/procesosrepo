﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ProcesoPP.Services;
using ProcesoPP.Business;
using System.Configuration;
using ProcesoPP.Model;

namespace ProcesoPP.UI.Personal
{
    public partial class frmPersonalHistorial : System.Web.UI.Page
    {
        #region PROPIEDADES

        public Permisos _Permiso
        {
            get { return (Permisos)ViewState["permiso"]; }
            set { ViewState["permiso"] = value; }
        }

        public int _idUsuario
        {
            get { return (int)ViewState["idUsuario"]; }
            set { ViewState["idUsuario"] = value; }
        }
        
        public int _ADM
        {
            get { return int.Parse(ConfigurationManager.AppSettings["ADM"].ToString()); }
        }

        #endregion
        
        #region EVENTOS

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                 _idUsuario = Common.EvaluarSession(Request, this.Page);
                 if (_idUsuario != 0)
                 {
                     _Permiso = Common.getModulo(Request, this.Page);
                     if (_Permiso != null && (_Permiso.Lectura && _Permiso.Escritura))
                     {
                         if (!IsPostBack)
                         {
                             CargarEmpresa();
                             //Cargamos el valor por defecto de sector
                             ddlSector.Items.Insert(0, new ListItem("Seleccione Sector", "0"));
                         }
                     }
                     else
                     {
                         Mensaje.errorMsj("NO TIENE PERMISOS A ESTE FORMULARIO", this.Page, "SIN ACCESO", "../frmPrincipal.aspx");
                     }
                 }
                 else
                 {
                     Mensaje.errorMsj("DEBE LOGUEARSE", this.Page, "LOGIN", "../frmLogin.aspx");
                 }
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        private void CargarSector()
        {
            SectorBus oSectorBus = new SectorBus();
            oSectorBus.CargarSectorPorEmpresa(ref ddlSector,int.Parse(ddlEmpresa.SelectedValue));
            ddlSector.Items.Insert(0,new ListItem("Seleccione Sector","0"));
        }

        private void CargarEmpresa()
        {
            EmpresaBus oEmpresaBus = new EmpresaBus();
            oEmpresaBus.CargarEmpresaInterna(ref ddlEmpresa, "Seleccione Empresa");
        }

        

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                CargarPersonal();
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        protected void ddlEmpresa_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddlEmpresa.SelectedIndex > 0)
                {
                    CargarSector();
                }

            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        protected void gvPersonal_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            switch (e.CommandName)
            {

                case "Ver": 
                    Response.Redirect("frmPersonalCarga.aspx?idPersonal=" + e.CommandArgument + "&idModulo=" + _Permiso.oModulo.idModulo + "&Modificando=false");
                    break;
                
            }
        }

        protected void gvPersonal_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
                CargarRow(e);
        }

        protected void gvPersonal_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvPersonal.PageIndex = e.NewPageIndex;
            CargarPersonal();
        }

        #endregion

        #region METODOS


        private void CargarRow(GridViewRowEventArgs e)
        {           
           Common.CargarRow(e, _Permiso);
           HiddenField hfBaja = e.Row.FindControl("hfBaja") as HiddenField;
           if (hfBaja.Value != string.Empty && bool.Parse(hfBaja.Value))
           {
               e.Row.CssClass = "rowBaja";
               e.Row.ToolTip = "Persona eliminada"; 
           }

        }

        private void CargarPersonal()
        {
            PersonalBus oPersonalBus = new PersonalBus();
            gvPersonal.DataSource = oPersonalBus.PersonalHistorialGetByFilter(txtApellidoNom.Text,txtDNI.Text,txtLegajo.Text,ddlEmpresa.SelectedValue,ddlSector.SelectedValue,txtVtoCarnet.Text,txtVtoPsico.Text,txtVtoCargasGrales.Text,txtVtoCargasPel.Text,txtVtoMD.Text);
            gvPersonal.DataBind();
        }
        
        #endregion

    }
}
