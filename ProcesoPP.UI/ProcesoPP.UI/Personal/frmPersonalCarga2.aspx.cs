﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ProcesoPP.Business;
using ProcesoPP.Model;
using ProcesoPP.Services;
using System.Data;
using System.Configuration;
using System.IO;
using System.Diagnostics;
using System.Web.Services;
using AjaxControlToolkit;


namespace ProcesoPP.UI.Administracion
{
    public partial class frmPersonalCarga2 : System.Web.UI.Page
    {
        #region <PROPIEDADES>


        public int _idUsuario
        {
            get { return (int)ViewState["idUsuario"]; }
            set { ViewState["idUsuario"] = value; }
        }

        public int _idPersonal
        {
            get { return (int)ViewState["idPersonal"]; }
            set { ViewState["idPersonal"] = value; }
        }

        public int _idDatosLaborales
        {
            get { return (int)ViewState["idDatosLaborales"]; }
            set { ViewState["idDatosLaborales"] = value; }
        }
        public int _Legajo
        {
            get { return (int)ViewState["legajo"]; }
            set { ViewState["legajo"] = value; }
        }
        public Permisos _Permiso
        {
            get { return (Permisos)ViewState["permiso"]; }
            set { ViewState["permiso"] = value; }
        }

        public int _ADM
        {
            get { return int.Parse(ConfigurationManager.AppSettings["ADM"].ToString()); }
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                _idUsuario = Common.EvaluarSession(Request, this.Page);
                if (_idUsuario != 0)
                {
                    _Permiso = Common.getModulo(Request, this.Page);
                    if (_Permiso != null && _Permiso.Lectura)
                    {
                        if (!IsPostBack)
                        {

                            InitScreen();
                        }
                    }
                    else
                    {
                        Mensaje.errorMsj("NO TIENE PERMISOS A ESTE FORMULARIO", this.Page, "SIN ACCESO", "../frmPrincipal.aspx");
                    }
                }
                else
                {
                    Mensaje.errorMsj("DEBE LOGUEARSE", this.Page, "LOGIN", "../frmLogin.aspx");
                }
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }


        protected void btn_uno_Click(object sender, EventArgs e)
        {
            try
            {
                if ((Request["Enabled"] != null) && (bool.Parse(Request["Enabled"])))
                    Response.Redirect("frmPersonalCarga.aspx?idPersonal=" + this._idPersonal + "&idUsuario=" + this._idUsuario + "&idModulo=" + this._Permiso.oModulo.idModulo + "&Modificando=true");
                else
                    Response.Redirect("frmPersonalCarga.aspx?idPersonal=" + this._idPersonal + "&idUsuario=" + this._idUsuario + "&idModulo=" + this._Permiso.oModulo.idModulo + "&Modificando=false");
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        protected void btn_Guardar_Click(object sender, EventArgs e)
        {
            try
            {
                comprobarFechas();
                if (hfEsValido.Value == "true")
                {
                    GuardarPersonal();
                    GuardarArchivos();
                    Mensaje.successMsj("Datos guardados correctamente", this.Page, "Guardado", "frmPersonalGestion.aspx?idUsuario=" + this._idUsuario + "&idModulo=" + this._Permiso.oModulo.idModulo);
                }
                else
                {
                    Mensaje.errorMsj("Falta(n) fecha(s). Por favor revise la(s) fecha(s)", this.Page, "Error", null);
                }
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        private void comprobarFechas()
        {
            //Corroboramos que si esta marcado algun radio tenga el vencimiento correspondiente

            //Por defecto es valido
            hfEsValido.Value = "true";
            //Consultamos uno por uno si ambos datos fueron cargados
            if ( (rmemSi.Checked) && (txtVManeja.Text == "") )
                hfEsValido.Value = "false";
            if ( (rpsSi.Checked) && (txtVPsicofisico.Text == "") )
                hfEsValido.Value = "false";
            if ( (rcgSi.Checked) && (txtVCargasGrales.Text == "") )
                hfEsValido.Value = "false";
            if ( (rcpSi.Checked) && (txtVCargasPeligrosas.Text == "") )
                hfEsValido.Value = "false";
            if ((rmdSi.Checked) && (txtVManejoDefensivo.Text == ""))
                hfEsValido.Value = "false";
            if ((rpscSi.Checked) && (txtVCPsicofisico.Text == ""))
                hfEsValido.Value = "false";
        }

        public void btn_cancelar_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("frmPersonalGestion.aspx?idUsuario=" + this._idUsuario + "&idModulo=" + this._Permiso.oModulo.idModulo);
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }
        protected void btn_cargarArchivos_click(object sender, EventArgs e)
        {
            try
            {
                gvPersonalFiles.Visible = true;
                CargarPersonalFiles();
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }


        #endregion

        #region METODOS

        private void InitScreen()
        {

            CargarBancos();
            CargarDiagrama();
            CargarEmpresa();
            CargarFuncion();
            CargarModalidad();
            CargarRelacion();
            CargarCategoria();
            if (Request["idPersonal"] != null)
            {
                _idPersonal = int.Parse(Request["idPersonal"]);
                if (bool.Parse(Request["Enabled"]) == false)
                {
                    Common.AnularControles(this.Form);
                    this.btn_Guardar.Enabled = false;
                    this.btn_Guardar.Visible = false;
                    this.uno.Visible = true;
                    this.uno.Enabled = true;
                    this.btn_Cancelar.Enabled = true;
                    this.uno.Enabled = true;
                    this.dos.Enabled = false;
                    this.btn_archivos.Visible = true;
                    this.btn_archivos.Enabled = true;
                }
                else if (bool.Parse(Request["Modificando"]))
                {
                    this.uno.Enabled = true;
                    this.dos.Enabled = false;
                }
                else if (!bool.Parse(Request["Modificando"]))
                {
                    this.uno.Visible = false;
                    this.dos.Visible = false;
                    
                }
                this.btn_Cancelar.Enabled = true;
                CargarPersonal(_idPersonal);
            }

            if (!_Permiso.Escritura)
            {
                btn_Guardar.Visible = false;
            }
        }


        protected void ddlEmpresa_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddlEmpresa.SelectedIndex > 0)
                {
                    CargarLugarTrabajo();
                    CargarSector(int.Parse(ddlEmpresa.SelectedValue));
                }

            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }


        private void GuardarArchivos()
        {
            List<PersonalFiles> archivos = new List<PersonalFiles>();
            string ruta = Server.MapPath(@"~/Adjuntos/Personal/" + _Legajo.ToString() + "/");
            if (!Directory.Exists(ruta))
            {
                DirectoryInfo di = Directory.CreateDirectory(ruta);
            }

            if (FileUploadAlta.HasFile)
            {
                guardaIndividual("ALTA+EN+EMPRESA", ref archivos, ruta, FileUploadAlta);
            }
            if (FileUploadAntitetanica.HasFile)
            {
                guardaIndividual("ANTITETANICA", ref archivos, ruta, FileUploadAntitetanica);
            }
            if (FileUploadCargasGrales.HasFile)
            {
                guardaIndividual("CARGAS+GENERALES", ref archivos, ruta, FileUploadCargasGrales);
            }
            if (FileUploadCargasPeligrosas.HasFile)
            {
                guardaIndividual("CARGAS+PELIGROSAS", ref archivos, ruta, FileUploadCargasPeligrosas);
            }
            if (FileUploadCarnet.HasFile)
            {
                guardaIndividual("LICENCIA", ref archivos, ruta, FileUploadCarnet);
            }
            if (FileUploadHepatitis.HasFile)
            {
                guardaIndividual("HEPATITIS", ref archivos, ruta, FileUploadHepatitis);
            }
            if (FileUploadMDefensivo.HasFile)
            {
                guardaIndividual("MANEJO+DEFENSIVO", ref archivos, ruta, FileUploadMDefensivo);
            }
            if (FileUploadMedicoLaboral.HasFile)
            {
                guardaIndividual("MEDICO+LABORAL", ref archivos, ruta, FileUploadMedicoLaboral);
            }
            if (FileUploadPreocupacional.HasFile)
            {
                guardaIndividual("PREOCUPACIONAL", ref archivos, ruta, FileUploadPreocupacional);
            }
            if (FileUploadPreocupacionalFecha.HasFile)
            {
                guardaIndividual("PREOCUPACIONAL+FECHA", ref archivos, ruta, FileUploadPreocupacionalFecha);
            }
            if (FileUploadPsicofisico.HasFile)
            {
                guardaIndividual("PSICOFISICO", ref archivos, ruta, FileUploadPsicofisico);
            }
            foreach (PersonalFiles pf in archivos)
            {
                PersonalFilesBus oPFBus = new PersonalFilesBus();
                oPFBus.PersonalFilesAdd(pf);
            }
        }


        private void CargarPersonal(int _idPersonal)
        {
            Model.Personal oPersonal;
            PersonalBus oPersonalBus = new PersonalBus();
            oPersonal = oPersonalBus.PersonalGetById(_idPersonal);
            PersonalConduccionBus oPersonalCBus = new PersonalConduccionBus();
            PersonalConduccion oPersonalC = oPersonalCBus.PersonalConduccionGetById(oPersonal.IdPersonalConduccion);
            PersonalDatosLaboralesBus oPersonalDLBus = new PersonalDatosLaboralesBus();
            PersonalDatosLaborales oPersonalDL = oPersonalDLBus.PersonalDatosLaboralesGetById(oPersonal.IdPersonalDatosLaborales);
            PersonalInformacionMedicaBus oPersonalIMBus = new PersonalInformacionMedicaBus();
            PersonalInformacionMedica oPersonalIM = oPersonalIMBus.PersonalInformacionMedicaGetById(oPersonal.IdPersonalInformacionMedica);
            _Legajo = oPersonal.NroLegajo;

            //Carga de datos laborales del Personal
            ddlBanco.SelectedValue = oPersonalDL.IdBanco.ToString();
            ddlDiagrama.SelectedValue = oPersonalDL.IdDiagrama.ToString();
            //ddlEmpresa.SelectedValue = oPersonalDL.IdEmpresa.ToString();
            CargarEmpresaSector(oPersonalDL.IdSector);
            CargarLugarTrabajo();
            //CargarSector(int.Parse(ddlEmpresa.SelectedValue));
            ddlFuncion.SelectedValue = oPersonalDL.IdFuncion.ToString();
            ddlModalidad.SelectedValue = oPersonalDL.IdModalidad.ToString();
            ddlRelacion.SelectedValue = oPersonalDL.IdRelacion.ToString();
            ddlSector.SelectedValue = oPersonalDL.IdSector.ToString();
            txtCBU.Text = oPersonalDL.CBU;
            if(oPersonalDL.FechaAlta.Year > 1900)
                txtFechaAlta.Text = oPersonalDL.FechaAlta.ToString("dd/MM/yyyy");
            

            //Carga de datos Medicos del Personal
            txtAlergias.Text = oPersonalIM.Alergico;
            if (oPersonalIM.Antitetanica)
                ratSi.Checked = oPersonalIM.Antitetanica;
            else
                ratNo.Checked = true;
            if (oPersonalIM.AprobacionPeriodico)
                rmlSi.Checked = oPersonalIM.AprobacionPeriodico;
            else
                rmlNo.Checked = true;
            if (oPersonalIM.AprobacionPreocupacional)
                repSi.Checked = oPersonalIM.Antitetanica;
            else
                repNo.Checked = true;
            if (oPersonalIM.HapatitisB)
                rhSi.Checked = oPersonalIM.HapatitisB;
            else
                rhNo.Checked = true;

            txtFactor.Text = oPersonalIM.Factor;
            if (oPersonalIM.FechaPeriodico.Year > 1910)
                txtFechaEPreocupacional.Text = oPersonalIM.FechaPeriodico.ToString("dd/MM/yyyy");
            if (oPersonalIM.FechaPreocupacional.Year > 1910)
                txtFechaEPreocupacional.Text = oPersonalIM.FechaPreocupacional.ToString("dd/MM/yyyy");
            txtGrupoSanguineo.Text = oPersonalIM.GrupoSanguineo;
            txtObservacion.Text = oPersonalIM.Observacion;

            //Carga de Datos de Conduccion
            if (oPersonalC.CargarGenerales)
                rcgSi.Checked = oPersonalC.CargarGenerales;
            else
                rcgNo.Checked = true;
            if (oPersonalC.CargarPeligrosas)
                rcpSi.Checked = oPersonalC.CargarPeligrosas;
            else
                rcpNo.Checked = true;

            txtCNRT.Text = oPersonalC.CertCNRT;
            txtEmpresaCNRT.Text = oPersonalC.Empresa;

            if (oPersonalC.ManejaEmpresa)
                rmemSi.Checked = oPersonalC.ManejaEmpresa;
            else
                rmemNo.Checked = true;

            if (oPersonalC.ManejoDefensivo)
                rmdSi.Checked = oPersonalC.ManejoDefensivo;
            else
                rmdNo.Checked = true;

            if (oPersonalC.Psicofisico) 
                rpsSi.Checked = oPersonalC.Psicofisico;
            else
                rpsNo.Checked = true;
            if (oPersonalC.CarnetPsicofisico)
                rpscSi.Checked = true;
            else
                rpscNo.Checked = true;
            ddlCategoriaCarnet.SelectedValue = oPersonalC.IdCategoria.ToString();
            //Verificamos no levantar fechas nulas
            if(oPersonalC.VtoCargasGenerales.Year > 1910)
                txtVCargasGrales.Text = oPersonalC.VtoCargasGenerales.ToString("dd/MM/yyyy");
            if (oPersonalC.VtoCargasPeligrosas.Year > 1910)
                txtVCargasPeligrosas.Text = oPersonalC.VtoCargasPeligrosas.ToString("dd/MM/yyyy");
            if (oPersonalC.VtoCarnet.Year > 1910)
                txtVManeja.Text = oPersonalC.VtoCarnet.ToString("dd/MM/yyyy");
            if (oPersonalC.VtoManejoDefensivo.Year > 1910)
                txtVManejoDefensivo.Text = oPersonalC.VtoManejoDefensivo.ToString("dd/MM/yyyy");
            if (oPersonalC.VtoPsicofisico.Year > 1910)
                txtVPsicofisico.Text = oPersonalC.VtoPsicofisico.ToString("dd/MM/yyyy");
            if (oPersonalC.VencimientoCarnet.Year > 1910)
                txtVCPsicofisico.Text = oPersonalC.VencimientoCarnet.ToString("dd/MM/yyyy");
        }

        



        private void recuperarPersonal()
        {
            //Recuperamos el id de personal de la sesion
            string aux;
            aux = this.Session.ToString();
            _idUsuario = int.Parse(aux.Substring(aux.IndexOf('?')));
        }

        private void GuardarPersonal()
        {
            Model.Personal oPersonal;
            PersonalBus oPersonalBus = new PersonalBus();
            PersonalConduccion oPersonalC;
            PersonalConduccionBus oPersonalCBus = new PersonalConduccionBus();
            PersonalDatosLaborales oPersonalDL;
            PersonalDatosLaboralesBus oPersonalDLBus = new PersonalDatosLaboralesBus();
            PersonalInformacionMedica oPersonalIM;
            PersonalInformacionMedicaBus oPersonalIMBus = new PersonalInformacionMedicaBus();

            //Buscamos y cargamos al personal correspondiente y sus "colaboradores internos"
            oPersonal = oPersonalBus.PersonalGetById(_idPersonal);
            if (bool.Parse(Request["Modificando"]))
            {
                oPersonalC = oPersonalCBus.PersonalConduccionGetById(oPersonal.IdPersonalConduccion);
                oPersonalDL = oPersonalDLBus.PersonalDatosLaboralesGetById(oPersonal.IdPersonalDatosLaborales);
                oPersonalIM = oPersonalIMBus.PersonalInformacionMedicaGetById(oPersonal.IdPersonalInformacionMedica);
                if(oPersonalC.IdPersonalConduccion == 0)
                    oPersonalC = new PersonalConduccion();
                if (oPersonalDL.IdPersonalDatosLaborales == 0)
                    oPersonalDL = new PersonalDatosLaborales();
                if (oPersonalIM.IdPersonalInformacionMedica == 0)
                    oPersonalIM = new PersonalInformacionMedica();
            }
            else
            {
                oPersonalC = new PersonalConduccion();
                oPersonalDL = new PersonalDatosLaborales();
                oPersonalIM = new PersonalInformacionMedica();
            }

            //Carga de datos laborales del Personal
            oPersonalDL.IdBanco = int.Parse(ddlBanco.SelectedValue);
            oPersonalDL.IdDiagrama = int.Parse(ddlDiagrama.SelectedValue);
            if (ddlEmpresa.SelectedValue != "")
                oPersonalDL.IdEmpresa = int.Parse(ddlEmpresa.SelectedValue);
            else
                oPersonalDL.IdEmpresa = 0;
            oPersonalDL.IdFuncion = int.Parse(ddlFuncion.SelectedValue);
            oPersonalDL.IdModalidad = int.Parse(ddlModalidad.SelectedValue);
            oPersonalDL.IdRelacion = int.Parse(ddlRelacion.SelectedValue);
            if (ddlSector.SelectedValue != "")
                oPersonalDL.IdSector = int.Parse(ddlSector.SelectedValue);
            else
                oPersonalDL.IdSector = 0;
            oPersonalDL.CBU = txtCBU.Text;
            if (txtFechaAlta.Text != "")
                oPersonalDL.FechaAlta = DateTime.Parse(txtFechaAlta.Text);
            else
                //Si no se carga una fecha de alta, se tomara como el dia de la fecha ya que la base no admite nulos
                oPersonalDL.FechaAlta = DateTime.Today;

            //Carga de datos Medicos del Personal
            oPersonalIM.Alergico = txtAlergias.Text;
            oPersonalIM.Antitetanica = ratSi.Checked;
            oPersonalIM.AprobacionPeriodico = rmlSi.Checked;
            oPersonalIM.AprobacionPreocupacional = repSi.Checked;
            oPersonalIM.ExPeriodico = repSi.Checked;
            oPersonalIM.Factor = (txtFactor.Text);
            if (txtFechaEPreocupacional.Text != "")
                oPersonalIM.FechaPeriodico = DateTime.Parse(txtFechaEPreocupacional.Text);
            else
                oPersonalIM.FechaPeriodico = DateTime.MinValue;
            if (txtFechaEPreocupacional.Text != "")
                oPersonalIM.FechaPreocupacional = DateTime.Parse(txtFechaEPreocupacional.Text);
            else
                oPersonalIM.FechaPreocupacional = DateTime.MinValue;
            oPersonalIM.GrupoSanguineo = txtGrupoSanguineo.Text;
            oPersonalIM.HapatitisB = repSi.Checked;
            oPersonalIM.Observacion = txtObservacion.Text;

            //Carga de Datos de Conduccion
            oPersonalC.CargarGenerales = rcgSi.Checked;
            oPersonalC.CargarPeligrosas = rcpSi.Checked;
            oPersonalC.CertCNRT = txtCNRT.Text;
            oPersonalC.Empresa = txtEmpresaCNRT.Text;
            oPersonalC.ManejaEmpresa = rmemSi.Checked;
            oPersonalC.ManejoDefensivo = rmdSi.Checked;
            oPersonalC.Psicofisico = rpsSi.Checked;
            oPersonalC.IdCategoria = int.Parse(ddlCategoriaCarnet.SelectedValue);
            oPersonalC.CarnetPsicofisico = rpscSi.Checked;

            if (txtVCargasGrales.Text != "")
                oPersonalC.VtoCargasGenerales = DateTime.Parse(txtVCargasGrales.Text);
            else
                oPersonalC.VtoCargasGenerales = DateTime.MinValue;
                
            if (txtVCargasPeligrosas.Text != "")
                oPersonalC.VtoCargasPeligrosas = DateTime.Parse(txtVCargasPeligrosas.Text);
            else
                oPersonalC.VtoCargasPeligrosas = DateTime.MinValue;

            if (txtVManeja.Text != "")
            {
                oPersonalC.VtoCarnet = DateTime.Parse(txtVManeja.Text);
                VerificarPermisos();
            }
            else
                oPersonalC.VtoCarnet = DateTime.MinValue;

            if (txtVManejoDefensivo.Text != "")
                oPersonalC.VtoManejoDefensivo = DateTime.Parse(txtVManejoDefensivo.Text);
            else
                oPersonalC.VtoManejoDefensivo = DateTime.MinValue;

            if (txtVPsicofisico.Text != "")
                oPersonalC.VtoPsicofisico = DateTime.Parse(txtVPsicofisico.Text);
            else
                oPersonalC.VtoPsicofisico = DateTime.MinValue;

            if (txtVCPsicofisico.Text != "")
                oPersonalC.VencimientoCarnet = DateTime.Parse(txtVCPsicofisico.Text);
            else
                oPersonalC.VencimientoCarnet = DateTime.MinValue;

            if (bool.Parse(Request["Modificando"]))
            {
                //Actualizamos los datos en los ya existentes objetos
                oPersonalCBus.PersonalConduccionUpdate(oPersonalC);
                oPersonalIMBus.PersonalInformacionMedicaUpdate(oPersonalIM);
                oPersonalDLBus.PersonalDatosLaboralesUpdate(oPersonalDL);

                //Verificamos que alguno de ellos puede no existir
                bool necesitoUpdate = false;
                if (oPersonalC.IdPersonalConduccion == 0)
                {
                    oPersonal.IdPersonalConduccion = oPersonalCBus.PersonalConduccionAdd(oPersonalC);
                    necesitoUpdate = true;
                }
                if (oPersonalDL.IdPersonalDatosLaborales == 0)
                {
                    necesitoUpdate = true;
                    oPersonal.IdPersonalDatosLaborales = oPersonalDLBus.PersonalDatosLaboralesAdd(oPersonalDL);
                }
                if (oPersonalIM.IdPersonalInformacionMedica == 0)
                {
                    necesitoUpdate = true;
                    oPersonal.IdPersonalInformacionMedica = oPersonalIMBus.PersonalInformacionMedicaAdd(oPersonalIM);
                }
                if(necesitoUpdate)
                    oPersonalBus.PersonalUpdate(oPersonal);
                    
            }
            else
            {
                //Cargamos los ids necesarios a personal y actualizamos
                oPersonal.IdPersonalConduccion = oPersonalCBus.PersonalConduccionAdd(oPersonalC);
                oPersonal.IdPersonalInformacionMedica = oPersonalIMBus.PersonalInformacionMedicaAdd(oPersonalIM);
                oPersonal.IdPersonalDatosLaborales = oPersonalDLBus.PersonalDatosLaboralesAdd(oPersonalDL);
                oPersonalBus.PersonalUpdate(oPersonal);
            }
        }

        private void VerificarPermisos()
        {
            /*
                 *Verificamos si la fecha del carnet fue modificada para revocar los permisos de conduccion adicionales que surgieron 
                 */
            if (bool.Parse(Request["Modificando"]))
            {
                PermisoConduccionBus oPermisoBus = new PermisoConduccionBus();
                List<PermisoConduccion> oPermisos = oPermisoBus.PermisoConduccionGetByLegajo(_idPersonal);
                //Verificamos que tiene al menos un permiso
                if (oPermisos.Count > 0)
                {
                    DateTime nuevaFecha = DateTime.Parse(txtVManeja.Text);
                    //Verificamos que la fecha es distinta, es decir, renovo el carnet
                    if (! (oPermisos.First()).Fecha.Equals(nuevaFecha))
                    {
                        foreach (PermisoConduccion perm in oPermisos)
                        {
                            oPermisoBus.PermisoConduccionDelete(perm);
                        }
                    }
                }
            }
        }

        private void guardaIndividual(string nombreArchivo, ref List<PersonalFiles> archivos, string ruta, FileUpload control)
        {
            PersonalFiles nuevo = new PersonalFiles();
            string extension = control.FileName.ToString();
            extension = extension.Substring(extension.IndexOf('.'));
            string fecha = DateTime.Today.ToString("dd/MM/yyyy");
            fecha = fecha.Replace('/', '-');
            nuevo.IdPersonal = _idPersonal;
            nuevo.Nombre = nombreArchivo + "_" + fecha + extension;
            //nuevo.Ruta = Server.MapPath("~/Adjuntos/Personal/" + _Legajo + "/" + nuevo.Nombre);
            nuevo.Ruta = ruta + (nuevo.Nombre);
            archivos.Add(nuevo);
            control.SaveAs(ruta + nuevo.Nombre);
        }


        private void CargarPersonalFiles()
        {
            PersonalFilesBus oPersonalFilesBus = new PersonalFilesBus();
            gvPersonalFiles.DataSource = oPersonalFilesBus.getPersonalFilesByIdPersonal(_idPersonal);
            gvPersonalFiles.DataBind();
            Mensaje.mostrarPopUp(this);
        }

        private void CargarRow(GridViewRowEventArgs e)
        {
            LinkButton btnEliminar = e.Row.FindControl("btnEliminar") as LinkButton;
            Common.CargarRow(e, _Permiso);


            if (_Permiso.oTipoUsuario.idTipoUsuario == _ADM)
                btnEliminar.Visible = true;
            else
                btnEliminar.Visible = false;

        }

        protected void gvPersonalFiles_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            switch (e.CommandName)
            {

                //case "Ver":
                //    string ruta = e.CommandArgument.ToString();
                //    if (ruta.EndsWith("pdf"))
                //    {
                //        Response.ContentType = "Application/pdf";
                //        Response.TransmitFile(ruta);
                //    }
                //    else if (ruta.EndsWith("jpg"))
                //    {
                //        Response.ContentType = "image/jpeg";
                //        Response.TransmitFile(ruta);
                //    }
                //    else if (ruta.EndsWith("jpeg"))
                //    {
                //        Response.ContentType = "image/pjpeg";
                //        Response.TransmitFile(ruta);
                //    }
                //    else if (ruta.EndsWith("png"))
                //    {
                //        Response.ContentType = "image/png";
                //        Response.TransmitFile(ruta);
                //    }
                //    else if (ruta.EndsWith("bmp"))
                //    {
                //        Response.ContentType = "image/bmp";
                //        Response.TransmitFile(ruta);
                //    }
                //    else
                //    {
                //        Response.TransmitFile(ruta);
                //    }
                    
                //        break;
                case "Eliminar":
                    try
                    {
                        Eliminar(int.Parse(e.CommandArgument.ToString()));
                    }
                    catch (Exception ex)
                    {
                        Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
                    }
                    break;
            }
        }

        protected void gvPersonalFiles_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
                CargarRow(e);
        }

        protected void gvPersonalFiles_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvPersonalFiles.PageIndex = e.NewPageIndex;
            CargarPersonalFiles();
        }

        private void Eliminar(int idPersonalFile)
        {
            PersonalFilesBus oPersonalFilesBus = new PersonalFilesBus();
            PersonalFiles oPersonalFiles = oPersonalFilesBus.PersonalFilesGetById(idPersonalFile);
            //System.IO.File.Delete(oPersonalFiles.Ruta);
            oPersonalFilesBus.PersonalFilesDelete(idPersonalFile);

        }



        private void CargarEmpresa()
        {
            //Solo empresas Internas
            EmpresaBus oEmpresaBus = new EmpresaBus();
            oEmpresaBus.CargarEmpresaInterna(ref ddlEmpresa, "Seleccione Empresa");

        }

        private void CargarRelacion()
        {
            RelacionBus oRelacionBus = new RelacionBus();
            ddlRelacion.DataSource = oRelacionBus.RelacionGetAll();
            ddlRelacion.DataTextField = "Descripcion";
            ddlRelacion.DataValueField = "IdRelacion";
            ddlRelacion.DataBind();
            ddlRelacion.Items.Insert(0, new ListItem("Seleccione Regimen", "0"));
        }

        private void CargarCategoria()
        {
            CategoriaCarnetBus oCategoriaBus = new CategoriaCarnetBus();
            ddlCategoriaCarnet.DataSource = oCategoriaBus.CategoriaCarnetGetAll();
            ddlCategoriaCarnet.DataTextField = "Descripcion";
            ddlCategoriaCarnet.DataValueField = "IdCarnet";
            ddlCategoriaCarnet.DataBind();
            ddlCategoriaCarnet.Items.Insert(0, new ListItem("Seleccione Categoria", "0"));
        }

        private void CargarModalidad()
        {
            ModalidadBus oModalidadBus = new ModalidadBus();
            ddlModalidad.DataSource = oModalidadBus.ModalidadGetAll();
            ddlModalidad.DataTextField = "Descripcion";
            ddlModalidad.DataValueField = "IdModalidad";
            ddlModalidad.DataBind();
            ddlModalidad.Items.Insert(0, new ListItem("Seleccione Modalidad", "0"));
        }

        private void CargarEmpresaSector(int idSector)
        {
            SectorBus oSectorBus = new SectorBus();
            Sector oSector = oSectorBus.SectorGetById(idSector);
            cddEmpresa.SelectedValue = oSector.IdEmpresa.ToString();
            GetSector("idEmpresa:" + cddEmpresa.SelectedValue + ";", "idSector");
            cddSector.SelectedValue = idSector.ToString();

        }

        private void CargarFuncion()
        {

            FuncionBus oFuncionBus = new FuncionBus();
            ddlFuncion.DataSource = oFuncionBus.FuncionGetAll();
            ddlFuncion.DataTextField = "Descripcion";
            ddlFuncion.DataValueField = "IdFuncion";
            ddlFuncion.DataBind();
            ddlFuncion.Items.Insert(0, new ListItem("Seleccione Funcion", "0"));
        }


        private void CargarDiagrama()
        {
            DiagramaBus oDiagrama = new DiagramaBus();
            ddlDiagrama.DataSource = oDiagrama.DiagramaGetAll();
            ddlDiagrama.DataTextField = "Descripcion";
            ddlDiagrama.DataValueField = "IdDiagrama";
            ddlDiagrama.DataBind();
            ddlDiagrama.Items.Insert(0, new ListItem("Seleccione Diagrama", "0"));
        }

        private void CargarSector(int idEmpresa)
        {
            SectorBus oSector = new SectorBus();
            ddlSector.DataSource = oSector.SectorGetAllByIdEmpresa(idEmpresa);
            ddlSector.DataTextField = "Descripcion";
            ddlSector.DataValueField = "IdSector";
            ddlSector.DataBind();
        }

        private void CargarLugarTrabajo()
        {/*
          *El Lugar de Trabajo depende exclusivamente de la Empresa seleccionada 
          */
            EmpresaBus oEmpresa = new EmpresaBus();
            string domicilio = ((oEmpresa.EmpresaGetById(int.Parse(ddlEmpresa.SelectedValue))).Domicilio);
            txtLugarTrabajo.Text = domicilio;
        }

        private void CargarBancos()
        {
            BancoBus oBanco = new BancoBus();
            ddlBanco.DataSource = oBanco.BancoGetAll();
            ddlBanco.DataTextField = "Descripcion";
            ddlBanco.DataValueField = "IdBanco";
            ddlBanco.DataBind();
            ddlBanco.Items.Insert(0, new ListItem("Seleccione Banco", "0"));
        }

        [WebMethod]
        public static CascadingDropDownNameValue[] GetEmpresa(string knownCategoryValues, string category)
        {
            EmpresaBus oEmpresaBus = new EmpresaBus();
            List<CascadingDropDownNameValue> empresa = oEmpresaBus.GetDataEmpresaInteras();
            return empresa.ToArray();
        }

        [WebMethod]
        public static CascadingDropDownNameValue[] GetSector(string knownCategoryValues, string category)
        {
            string idEmpresa = CascadingDropDown.ParseKnownCategoryValuesString(knownCategoryValues)["idEmpresa"];
            SectorBus oSectorBus = new SectorBus();
            List<CascadingDropDownNameValue> sectores = oSectorBus.GetDataSector(int.Parse(idEmpresa));
            return sectores.ToArray();
        }

        #endregion

    }
}