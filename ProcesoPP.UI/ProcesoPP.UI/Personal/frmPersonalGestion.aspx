﻿<%@ Page Language="C#" MasterPageFile="~/frmPrincipal.Master" AutoEventWireup="true"
    CodeBehind="frmPersonalGestion.aspx.cs" Inherits="ProcesoPP.UI.Personal.frmPersonalGestion"
    Title="Gestión de Personal" EnableEventValidation="false" %>
    <%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %> 

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        $(document).ready(function () { SetMenu("liPersonal"); }); 
    </script>
</asp:Content>
<asp:Content ID="cphContenedor" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="contenido" runat="server">
        <div id="apertura">
            Gestión de Legajo de Personal.</div>
        <div class="solicitud100">
            <div class="sol_tit50">
                <label>
                    Apellido y Nombre</label></div>
            <div class="sol_campo30">
                <asp:TextBox runat="server" ID="txtApellidoNom" /></div>
<div class="sol_fecha"></div><div class="sol_fecha"></div>
            <div class="sol_tit50">
                <label>
                    Legajo</label></div>
            <div class="sol_campo50">
                <asp:TextBox runat="server" ID="txtLegajo" /></div>
        </div>
        <!--<div class="sol_tit50">
            <label>
                DNI</label></div>
        <div class="sol_campo50">
            <asp:TextBox runat="server" ID="txtDNI" /></div>
    </div>-->
    <div class="solicitud100">
        <cc1:toolkitscriptmanager id="ToolkitScriptManager1" runat="server">
        </cc1:toolkitscriptmanager>
        <div class="sol_tit50">
            <label>
                Empresa</label></div>
        <div class="sol_campo30">
            <asp:DropDownList CssClass="sol_campo50c" ID="ddlEmpresa" runat="server" />
            <cc1:cascadingdropdown id="cddEmpresa" targetcontrolid="ddlEmpresa" prompttext="N/A"
                promptvalue="0" servicemethod="GetEmpresa" runat="server" category="idEmpresa"
                loadingtext="Cargando..." />
        </div>
        <div class="sol_fecha"></div>
        <div class="sol_fecha"></div>
        <div class="sol_tit50">
            <label>
                Sector</label></div>
        <div class="sol_campo50">
            <asp:DropDownList CssClass="sol_campo50c" ID="ddlSector" runat="server" />
            <cc1:cascadingdropdown id="cddSector" targetcontrolid="ddlSector" prompttext="N/A"
                promptvalue="0" servicemethod="GetSector" runat="server" category="idSector"
                parentcontrolid="ddlEmpresa" loadingtext="Cargando..." />
        </div>
    </div>
    <div class="solicitud100">
        <div class="sol_tit50">
            <label>
                Vto. Carnet</label></div>
        <div class="sol_campo30">
            <asp:TextBox runat="server" CssClass="sol_campo25" ID="txtVtoCarnet" /></div>
        <div class="sol_fecha">
            <img id="btnFecha" alt="calendario" src="../img/calendario.gif" style="width: 20px;
                height: 20px; cursor: pointer;" onmouseover="CambiarConfiguracionCalendario('txtFecha',this);" /></div>

<div class="sol_fecha"></div>
<div class="sol_tit50">
            <label>
                Vto. Psicofisico</label></div>
        <div class="sol_campo30">
            <asp:TextBox runat="server" CssClass="sol_campo25" ID="txtVtoPsico" /></div>
        <div class="sol_fecha">
            <img id="btnFechaHasta" alt="calendario" src="../img/calendario.gif" style="width: 20px;
                height: 20px; cursor: pointer;" onmouseover="CambiarConfiguracionCalendario('txtFechaHasta',this);" /></div>
    </div>
    <div class="solicitud100">
        <div class="sol_tit50">
            <label>
                Vto. C. Grales</label></div>
        <div class="sol_campo30">
            <asp:TextBox CssClass="sol_campo25" runat="server" ID="txtVtoCargasGrales" onkeypress="return ValidarNumeroEntero(event,this);" /></div>
        <div class="sol_fecha">
            <img id="btnFechaHasta" alt="calendario" src="../img/calendario.gif" style="width: 20px;
                height: 20px; cursor: pointer;" onmouseover="CambiarConfiguracionCalendario('txtFechaHasta',this);" /></div>

<div class="sol_fecha"></div>

<div class="sol_tit50">
            <label>
                Vto. C. Peligrosas</label></div>
        <div class="sol_campo30">
            <asp:TextBox CssClass="sol_campo25" runat="server" ID="txtVtoCargasPel" onkeypress="return ValidarNumeroEntero(event,this);" /></div>
        <div class="sol_fecha">
            <img id="btnFechaHasta" alt="calendario" src="../img/calendario.gif" style="width: 20px;
                height: 20px; cursor: pointer;" onmouseover="CambiarConfiguracionCalendario('txtFechaHasta',this);" /></div>
    </div>

<div class="solicitud100">
        <div class="sol_tit50"><label>
Vto. M. Defensivo</label></div>

<div class="sol_campo30">
            <asp:TextBox CssClass="sol_campo25" runat="server" ID="txtVtoMD" onkeypress="return ValidarNumeroEntero(event,this);" /></div>
        <div class="sol_fecha">
            <img id="btnFechaHasta" alt="calendario" src="../img/calendario.gif" style="width: 20px;
                height: 20px; cursor: pointer;" onmouseover="CambiarConfiguracionCalendario('txtFechaHasta',this);" /></div>
   </div>

<div class="solicitud100sin">

<div class="sol_btn">
            <asp:Button runat="server" ID="btnBuscar" CssClass="btn_form" Text="Buscar" OnClick="btnBuscar_Click" />
            <asp:Button runat="server" ID="btnExportar" CssClass="btn_form" Text="Exportar" OnClick="btExportar_Click" />
        </div>
    </div>
    <asp:GridView ID="gvPersonal" runat="server" AutoGenerateColumns="False" DataMember="idPersonal"
        CssClass="tabla3" HeaderStyle-CssClass="celda1" OnRowCommand="gvPersonal_RowCommand"
        OnRowDataBound="gvPersonal_RowDataBound" AllowPaging="true" PageSize="20" OnPageIndexChanging="gvPersonal_PageIndexChanging"
        PagerStyle-CssClass="tabla_font">
        <Columns>
            <asp:TemplateField HeaderText="Legajo Personal" HeaderStyle-CssClass="columna1" ItemStyle-CssClass="columna1">
                <ItemTemplate>
                    <asp:Label ID="lblLegPersonal" runat="server" Text='<%# Bind("nroLegajo") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Apellido" HeaderStyle-CssClass="columna7" ItemStyle-CssClass="columna7">
                <ItemTemplate>
                    <asp:Label ID="lblApellido" runat="server" Text='<%# Bind("Apellido") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Nombre" HeaderStyle-CssClass="columna5" ItemStyle-CssClass="columna5">
                <ItemTemplate>
                    <asp:Label ID="lblNombre" runat="server" Text='<%# Bind("Nombres") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Fecha Nacimiento" HeaderStyle-CssClass="columna6"
                ItemStyle-CssClass="columna6">
                <ItemTemplate>
                    <asp:Label ID="lblFechaNac" runat="server" Text='<%# Bind("FechaNac") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Sexo" HeaderStyle-CssClass="columna6" ItemStyle-CssClass="columna6">
                <ItemTemplate>
                    <asp:Label ID="lblSexo" runat="server" Text='<%# Bind("Sexo") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="DNI" HeaderStyle-CssClass="columna7" ItemStyle-CssClass="columna7">
                <ItemTemplate>
                    <asp:Label ID="lblDNI" runat="server" Text='<%# Bind("DNI") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Vto Carnet" HeaderStyle-CssClass="columna7" ItemStyle-CssClass="columna7">
                <ItemTemplate>
                    <asp:Label ID="lblFechaVenc" runat="server" Text='<%# Bind("VtoCarnet") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Vto Psicofisico" HeaderStyle-CssClass="columna7" ItemStyle-CssClass="columna7">
                <ItemTemplate>
                    <asp:Label ID="lblFechaPsico" runat="server" Text='<%# Bind("VtoPsicofisico") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Vto C Grales" HeaderStyle-CssClass="columna7" ItemStyle-CssClass="columna7">
                <ItemTemplate>
                    <asp:Label ID="lblFechaCGrales" runat="server" Text='<%# Bind("VtoCargasGenerales") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField ItemStyle-CssClass="btn_iconos">
                <ItemTemplate>
                    <asp:LinkButton ID="btnVer" runat="server" title="Ver" CssClass="ver" CommandArgument='<%# Bind("idPersonal") %>'
                        CommandName="Ver"></asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField ItemStyle-CssClass="btn_iconos">
                <ItemTemplate>
                    <asp:LinkButton ID="btnModificar" runat="server" title="Editar Personal" CssClass="editar"
                        CommandArgument='<%# Bind("idPersonal") %>' CommandName="Modificar"></asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField ItemStyle-CssClass="btn_iconos">
                <ItemTemplate>
                    <asp:LinkButton ID="btnPermiso" runat="server" title="Otorgar Permiso" CssClass="permiso"
                        CommandArgument='<%# Bind("idPersonal") %>' CommandName="Permiso"></asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField ItemStyle-CssClass="btn_iconos">
                <ItemTemplate>
                    <asp:LinkButton ID="btnEliminar" runat="server" title="Eliminar Personal" CssClass="eliminar"
                        CommandArgument='<%# Bind("idPersonal") %>' CommandName="Eliminar" OnClientClick="return Eliminar('el Personal');"></asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
</asp:Content>
