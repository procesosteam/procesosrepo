﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ProcesoPP.Model;
using ProcesoPP.Business;
using ProcesoPP.Services;

namespace ProcesoPP.UI.Vehiculo
{
    public partial class frmPermisoExtendido : System.Web.UI.Page
    {
        #region PROPIEDADES

        public Permisos _Permiso
        {
            get { return (Permisos)ViewState["permiso"]; }
            set { ViewState["permiso"] = value; }
        }

        public int _idUsuario
        {
            get { return (int)ViewState["idUsuario"]; }
            set { ViewState["idUsuario"] = value; }
        }

        public int _idLegajo
        {
            get { return (int)ViewState["idLegajo"]; }
            set { ViewState["idLegajo"] = value; }
        }

        #endregion

        #region EVENTOS

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                _idUsuario = Common.EvaluarSession(Request, this.Page);
                if (_idUsuario != 0)
                {
                    _Permiso = Common.getModulo(Request, this.Page);
                    if (_Permiso != null && _Permiso.Lectura)
                    {
                        Mensaje.ocultarDivMsj(divMensaje);
                        if (!IsPostBack)
                        {
                            InitScrren();
                            CargarPermisos();
                        }
                    }
                    else
                    {
                        Mensaje.errorMsj("NO TIENE PERMISOS A ESTE FORMULARIO", this.Page, "SIN ACCESO", "../frmPrincipal.aspx");
                    }
                }
                else
                {
                    Mensaje.errorMsj("DEBE LOGUEARSE", this.Page, "LOGIN", "../frmLogin.aspx");
                }
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                Guardar();
                CargarPermisos();
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        protected void btn_cancelar_Click(object sender, EventArgs e)
        {
            Response.Redirect("frmPersonalGestion.aspx?idUsuario=" + this._idUsuario + "&idModulo=" + this._Permiso.oModulo.idModulo);
            
        }


        protected void gvPermiso_RowEditing(object sender, GridViewEditEventArgs e)
        {
            CargarPermisos();
        }

        protected void gvPermiso_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            CargarPermisos();
        }

        protected void gvPermiso_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {

            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        protected void gvPermiso_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {

            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }


        protected void gvPermiso_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                    CargarRow(e);
            }
        }

        #endregion

        #region Metodos

        private void InitScrren()
        {
            _idLegajo = int.Parse(Request["idPersonal"]);
            if (_Permiso.Escritura)
            {
                btnGuardar.Visible = true;
                CargarFechaDesde();
            }
            else
            {
                btnGuardar.Visible = false;
            }
        }



        private void Guardar()
        {
            PermisoConduccionBus oPermisoBus = new PermisoConduccionBus();
            DateTime fecha = DateTime.Parse(txtFecha.Text);
            DateTime fechaOrg;
            if (txtFechaOriginal.Text != "")
            {
                 fechaOrg = DateTime.Parse(txtFechaOriginal.Text);
            }
            else
            {
                 fechaOrg = DateTime.MinValue;
            }
            
            int tamanio = (oPermisoBus.PermisoConduccionGetByLegajo(_idLegajo)).Count;
            //La cantidad de permisos maximos es "2" - Si requiere modificacion es en la linea que sigue
            if ((tamanio < 2) && (fechaOrg < fecha) )
            {
                PermisoConduccion oPermiso = new PermisoConduccion(0, txtDescripcion.Text, _idUsuario, _idLegajo, fecha, fechaOrg);
                oPermiso.IdPermiso = oPermisoBus.PermisoConduccionAdd(oPermiso);

                if (oPermiso.IdPermiso > 0)
                {
                    ModificarFechaPersonal();
                    Mensaje.successMsj("Guardado con Exito!", this.Page, "Agregado", "frmPersonalGestion.aspx?idUsuario=" + this._idUsuario + "&idModulo=" + this._Permiso.oModulo.idModulo);
                    
                    //txtDescripcion.Text = "";
                    //txtFecha.Text = "";
                    //CargarPermisos();
                    //CargarFechaDesde();
                }
            }
            else
            {
                Mensaje.errorMsj("Se ha superado el numero de Permisos posibles para este legajo", this.Page, "Error", null);
            }
        }

        private void CargarPermisos()
        {
            PermisoConduccionBus oPermisoBus = new PermisoConduccionBus();
            gvPermiso.DataSource = oPermisoBus.PermisoConduccionGetByLegajo(_idLegajo);
            gvPermiso.DataBind();
        }

        private void CargarRow(GridViewRowEventArgs e)
        {
            Common.CargarRow(e, _Permiso);

            Label lblFecha;
            lblFecha = e.Row.FindControl("lblFecha") as Label;
            if (lblFecha.Text != "")
            {
                DateTime fechaN = DateTime.Parse(lblFecha.Text);
                lblFecha.Text = fechaN.ToString("dd/MM/yyyy");

            }
            lblFecha = e.Row.FindControl("lblFechaOriginal") as Label;
            if (lblFecha.Text != "")
            {
                DateTime fechaN = DateTime.Parse(lblFecha.Text);
                if (fechaN.Year > 1950)
                    lblFecha.Text = fechaN.ToString("dd/MM/yyyy");
                else
                    lblFecha.Text = "";

            }

        
        }


        private void CargarFechaDesde()
        {
            PersonalBus oPersonalBus = new PersonalBus();
            Model.Personal oPersonal = oPersonalBus.PersonalGetById(_idLegajo);
            PersonalConduccionBus oPersonalCBus = new PersonalConduccionBus();
            PersonalConduccion oPersonalC = oPersonalCBus.PersonalConduccionGetById(oPersonal.IdPersonalConduccion);
            DateTime fecha = oPersonalC.VtoCarnet;
            if (fecha.Year > 1950)
            {
                txtFechaOriginal.Text = fecha.ToString("dd/MM/yyyy");
            }
        }

        private void ModificarFechaPersonal()
        {
            DateTime fecha = DateTime.Parse(txtFecha.Text);
            PersonalBus oPersonalBus = new PersonalBus();
            Model.Personal oPersonal = oPersonalBus.PersonalGetById(_idLegajo);
            PersonalConduccionBus oPersonalCBus = new PersonalConduccionBus();
            PersonalConduccion oPersonalC = oPersonalCBus.PersonalConduccionGetById(oPersonal.IdPersonalConduccion);
            
            oPersonalC.VtoCarnet = fecha;
            
            oPersonalCBus.PersonalConduccionUpdate(oPersonalC);

        }

        #endregion
    }
}