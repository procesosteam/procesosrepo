﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using ProcesoPP.Business;
using ProcesoPP.Model;
using ProcesoPP.Services;
using ParteDiario.Services;
using System.Web.Services;
using AjaxControlToolkit;
using System.Collections.Generic;

namespace ProcesoPP.UI.Personal
{
    public partial class frmPersonalGestion : System.Web.UI.Page 
    {
        #region PROPIEDADES

        public Permisos _Permiso
        {
            get { return (Permisos)ViewState["permiso"]; }
            set { ViewState["permiso"] = value; }
        }

        public int _idUsuario
        {
            get { return (int)ViewState["idUsuario"]; }
            set { ViewState["idUsuario"] = value; }
        }
        
        public int _ADM
        {
            get { return int.Parse(ConfigurationManager.AppSettings["ADM"].ToString()); }
        }

        #endregion
        
        #region EVENTOS

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                 _idUsuario = Common.EvaluarSession(Request, this.Page);
                 if (_idUsuario != 0)
                 {
                     _Permiso = Common.getModulo(Request, this.Page);
                     if (_Permiso != null && _Permiso.Lectura)
                     {
                         if (!IsPostBack)
                         {
                             //CargarEmpresa();
                             //Cargamos el valor por defecto de sector
                             ddlSector.Items.Insert(0, new ListItem("Seleccione Sector", "0"));
                         }
                     }
                     else
                     {
                         Mensaje.errorMsj("NO TIENE PERMISOS A ESTE FORMULARIO", this.Page, "SIN ACCESO", "../frmPrincipal.aspx");
                     }
                 }
                 else
                 {
                     Mensaje.errorMsj("DEBE LOGUEARSE", this.Page, "LOGIN", "../frmLogin.aspx");
                 }
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        //private void CargarSector()
        //{
        //    SectorBus oSectorBus = new SectorBus();
        //    oSectorBus.CargarSectorPorEmpresa(ref ddlSector,int.Parse(ddlEmpresa.SelectedValue));
        //    ddlSector.Items.Insert(0,new ListItem("Seleccione Sector","0"));
        //}

        //private void CargarEmpresa()
        //{
        //    EmpresaBus oEmpresaBus = new EmpresaBus();
        //    oEmpresaBus.CargarEmpresaInterna(ref ddlEmpresa, "Seleccione Empresa");
        //}

        

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                CargarPersonal();
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        protected void btExportar_Click(object sender, EventArgs e)
        {
            try
            {
                exportar();
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        

        //protected void ddlEmpresa_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        if (ddlEmpresa.SelectedIndex > 0)
        //        {
        //            CargarSector();
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
        //    }
        //}

        protected void gvPersonal_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            switch (e.CommandName)
            {

                case "Ver": 
                    Response.Redirect("frmPersonalCarga.aspx?idPersonal=" + e.CommandArgument + "&idModulo=" + _Permiso.oModulo.idModulo + "&Modificando=false");
                    break;

                case "Modificar":
                    Response.Redirect("frmPersonalCarga.aspx?idPersonal=" + e.CommandArgument + "&idModulo=" + _Permiso.oModulo.idModulo + "&Modificando=true");
                    break;
                case"Eliminar":
                    try
                    {
                        Eliminar(e.CommandArgument.ToString());
                    }
                    catch (Exception ex)
                    {
                        Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
                    }
                    break;
                case "Permiso":
                    Response.Redirect("frmPermisoExtendido.aspx?idPersonal=" + e.CommandArgument + "&idModulo=" + _Permiso.oModulo.idModulo);
                    break;
            }
             
          }

        protected void gvPersonal_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
                CargarRow(e);
        }

        protected void gvPersonal_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvPersonal.PageIndex = e.NewPageIndex;
            CargarPersonal();
        }

        #endregion

        #region METODOS


        private void CargarRow(GridViewRowEventArgs e)
        {
            LinkButton btnPermiso = e.Row.FindControl("btnPermiso") as LinkButton;
            LinkButton btnVer = e.Row.FindControl("btnVer") as LinkButton;
            LinkButton btnModificar = e.Row.FindControl("btnModificar") as LinkButton;
            LinkButton btnEliminar = e.Row.FindControl("btnEliminar") as LinkButton;
            TimeSpan comparacion;
            Common.CargarRow(e, _Permiso);


            if (_Permiso.Escritura)
            {
                btnEliminar.Visible = true;
                btnModificar.Visible = true;                
                btnVer.Visible = true;
                btnPermiso.Visible = true;
            }
            else
            {
                btnEliminar.Visible = false;
                btnModificar.Visible = false;                
                btnVer.Visible = false;
                btnPermiso.Visible = false;
            }


            Label lblFechaVenc;
            lblFechaVenc = e.Row.FindControl("lblFechaVenc") as Label;
            if (lblFechaVenc.Text != "")
            {
                DateTime fechaVenc = DateTime.Parse(lblFechaVenc.Text);
                if (fechaVenc <= DateTime.Now)
                {
                    ((System.Web.UI.WebControls.DataControlFieldCell)(lblFechaVenc.Parent)).BackColor = System.Drawing.Color.Red;
                    if (_Permiso.Escritura)
                        btnPermiso.Visible = true;
                }
                else
                {
                    comparacion = (fechaVenc).Subtract(DateTime.Now);
                    if (comparacion.TotalDays <= 30)
                    {
                        ((System.Web.UI.WebControls.DataControlFieldCell)(lblFechaVenc.Parent)).BackColor = System.Drawing.Color.Yellow;                        
                    }
                }
            }
            lblFechaVenc = e.Row.FindControl("lblFechaPsico") as Label;
            if (lblFechaVenc.Text != "")
            {
                DateTime fechaVenc = DateTime.Parse(lblFechaVenc.Text);
                if (fechaVenc <= DateTime.Now)
                {
                    ((System.Web.UI.WebControls.DataControlFieldCell)(lblFechaVenc.Parent)).BackColor = System.Drawing.Color.Red;                    
                }
                else
                {
                    comparacion = (fechaVenc).Subtract(DateTime.Now);
                    if (comparacion.TotalDays <= 30)
                    {
                        ((System.Web.UI.WebControls.DataControlFieldCell)(lblFechaVenc.Parent)).BackColor = System.Drawing.Color.Yellow;                        
                    }
                }
            }
            lblFechaVenc = e.Row.FindControl("lblFechaCGrales") as Label;
            if (lblFechaVenc.Text != "")
            {
                DateTime fechaVenc = DateTime.Parse(lblFechaVenc.Text);
                if (fechaVenc <= DateTime.Now)
                {
                    ((System.Web.UI.WebControls.DataControlFieldCell)(lblFechaVenc.Parent)).BackColor = System.Drawing.Color.Red;                    
                }
                else
                {
                    comparacion = (fechaVenc).Subtract(DateTime.Now);
                    if (comparacion.TotalDays <= 30)
                    {
                        ((System.Web.UI.WebControls.DataControlFieldCell)(lblFechaVenc.Parent)).BackColor = System.Drawing.Color.Yellow;                        
                    }
                }
            }

            //if (_Permiso.oTipoUsuario.idTipoUsuario == _ADM)
            

        }


       

        private void Eliminar(string idPersonal)
        {
            PersonalBus oPersonalBus = new PersonalBus();
            bool eliminado = oPersonalBus.PersonalDelete(int.Parse(idPersonal));
            CargarPersonal();
        }

        private void CargarPersonal()
        {
            int idEmpresa, idSector = 0;
            PersonalBus oPersonalBus = new PersonalBus();
            if (ddlEmpresa.SelectedValue != "")
                idEmpresa = int.Parse(ddlEmpresa.SelectedValue);
            else
                idEmpresa = 0;
            if (ddlSector.SelectedValue != "")
                idSector = int.Parse(ddlSector.SelectedValue);
            else
                idSector = 0;
            
            //if(ddlEmpresa.SelectedValue)
            gvPersonal.DataSource = oPersonalBus.PersonalGetByFilter(txtApellidoNom.Text, txtDNI.Text, txtLegajo.Text, idEmpresa.ToString(), idSector.ToString(), txtVtoCarnet.Text, txtVtoPsico.Text, txtVtoCargasGrales.Text, txtVtoCargasPel.Text, txtVtoMD.Text);
            gvPersonal.DataBind();
        }

        private void exportar()
        {
            PersonalBus oPersonalBus = new PersonalBus();
            DataTable dt = oPersonalBus.PersonalGetByFilter(txtApellidoNom.Text, txtDNI.Text, txtLegajo.Text, ddlEmpresa.SelectedValue, ddlSector.SelectedValue, txtVtoCarnet.Text, txtVtoPsico.Text, txtVtoCargasGrales.Text, txtVtoCargasPel.Text, txtVtoMD.Text);
            dt.Columns.Remove("idPersonal");
            


            string path = System.IO.Path.Combine(Server.MapPath("~"), "img");
            ExportarExcel oExport = new ExportarExcel("Personal_" + DateTime.Now.ToString("yyyyMMddHHmmss"), path, "PERSONAL");
            oExport.ExportarExcel2003(dt);

        }


        [WebMethod]
        public static CascadingDropDownNameValue[] GetEmpresa(string knownCategoryValues, string category)
        {
            EmpresaBus oEmpresaBus = new EmpresaBus();
            List<CascadingDropDownNameValue> empresa = oEmpresaBus.GetDataEmpresaInteras();
            return empresa.ToArray();
        }

        [WebMethod]
        public static CascadingDropDownNameValue[] GetSector(string knownCategoryValues, string category)
        {
            string idEmpresa = CascadingDropDown.ParseKnownCategoryValuesString(knownCategoryValues)["idEmpresa"];
            SectorBus oSectorBus = new SectorBus();
            List<CascadingDropDownNameValue> sectores = oSectorBus.GetDataSector(int.Parse(idEmpresa));
            return sectores.ToArray();
        }


        #endregion

    }
}
