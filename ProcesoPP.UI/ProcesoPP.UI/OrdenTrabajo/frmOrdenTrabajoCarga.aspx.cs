﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ProcesoPP.Business;
using ProcesoPP.Model;
using System.IO;
using System.Text;
using ProcesoPP.Services;
using System.Configuration;

namespace ProcesoPP.UI.OrdenTrabajo
{
    public partial class frmOrdenTrabajoCarga : System.Web.UI.Page
    {
        #region <PROPIEDADES>

        public Permisos _Permiso
        {
            get { return (Permisos)ViewState["permiso"]; }
            set { ViewState["permiso"] = value; }
        }
        
        public int _idUsuario
        {
            get { return (int)ViewState["idUsuario"]; }
            set { ViewState["idUsuario"] = value; }
        }

        private int _idSolicitud
        {
            get { return (int)ViewState["idSolicitud"]; }
            set { ViewState["idSolicitud"] = value; }
        }

        private int _idTrailer
        {
            get { return (int)ViewState["idTrailer"]; }
            set { ViewState["idTrailer"] = value; }
        }

        private int _idOTI
        {
            get { return (int)ViewState["idOTI"]; }
            set { ViewState["idOTI"] = value; }
        }
        public int _ADM
        {
            get { return int.Parse(ConfigurationManager.AppSettings["ADM"].ToString()); }
        }

        #endregion

        #region <EVENTOS>

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                _idUsuario = Common.EvaluarSession(Request, this.Page);
                if (_idUsuario != 0)
                {
                    _Permiso = Common.getModulo(Request, this.Page);
                    if (_Permiso != null && _Permiso.Lectura)
                    {
                        if (!IsPostBack)
                        {
                            CargarVehiculo();
                            CargarAcompanante();
                            CargarChofer();
                            InitScreen();
                        }
                    }
                    else
                    {
                        Mensaje.errorMsj("NO TIENE PERMISOS A ESTE FORMULARIO", this.Page, "SIN ACCESO", "../frmPrincipal.aspx");
                    }
                }
                else
                {
                    Mensaje.errorMsj("DEBE LOGUEARSE", this.Page, "LOGIN", "../frmLogin.aspx");
                }
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                btnGuardar.Visible = false;
                GuardarOT();
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }
              
        protected void btnCambiarTrailer_Click(object sender, EventArgs e)
        {
            ddlTrailer.Visible = true;
            lblTrailer.Visible = false;
            btnCambiarTrailer.Visible = false;
        }

        #endregion

        #region <METODOS>

        private void GuardarOT()
        {
            OrdenTrabajoBus oOrdenTrabajoBus = new OrdenTrabajoBus();
            Model.OrdenTrabajo oOrdenTrabajo = new Model.OrdenTrabajo();

            oOrdenTrabajo.IdSolicitudCliente = _idSolicitud;
            oOrdenTrabajo.IdTransporte = int.Parse(ddlVehiculo.SelectedValue);
            oOrdenTrabajo.IdTrailer = ddlTrailer.Visible ? int.Parse(ddlTrailer.SelectedValue) : _idTrailer;
            //oOrdenTrabajo.IdEquipo = int.Parse(ddlEquipo.SelectedValue);
            oOrdenTrabajo.IdAcompanante = int.Parse(ddlAcompanante.SelectedValue);
            oOrdenTrabajo.IdChofer = int.Parse(ddlChofer.SelectedValue);
            oOrdenTrabajo.Entraga = true;

            if (_idOTI == 0)
            {
                oOrdenTrabajo.IdOrden = oOrdenTrabajoBus.OrdenTrabajoAdd(oOrdenTrabajo);
                _idOTI = oOrdenTrabajo.IdOrden;
            }
            else
            {
                oOrdenTrabajo.IdOrden = _idOTI;
                oOrdenTrabajoBus.OrdenTrabajoUpdate(oOrdenTrabajo);
            }

            if (oOrdenTrabajo.IdOrden > 0)
            {
                Mensaje.preguntaMsj("Orden Guardada! \n Desea Imprimir?", this.Page, "Exito", "../Reportes/frmImprimir.aspx" + strRequest());
                lblNroOT.Text = _idOTI.ToString();
            }

        }

        public string strRequest()
        {
            string strRequest = "";
            strRequest += "?LblFecha=" + lblFecha.Text;
            strRequest += "&lblEmpresa=" + lblEmpresa.Text;
            strRequest += "&lblNroSS=" + lblNroSS.Text;
            strRequest += "&lblSector=" + lblSector.Text;
            strRequest += "&lblCondicion=" + lblCondicion.Text;
            strRequest += "&txtNombre=" + txtNombre.Text;
            strRequest += "&lblTipoServicio=" + lblTipoServicio.Text;
            strRequest += "&lblTrailer=" + lblTrailer.Text;
            strRequest += "&ddlTrailer=" + ddlTrailer.SelectedItem.Text;
            strRequest += "&lblEspecificacion=" + getEspecificaciones();
            strRequest += "&lblPozoOrigen=" + lblPozoOrigen.Text;
            strRequest += "&lblPozoDestino=" + lblPozoDestino.Text;
            strRequest += "&lblLugarOrigen=" + lblLugarOrigen.Text;
            strRequest += "&lblLugarDestino=" + lblLugarDestino.Text;
            strRequest += "&lblKmsEstimados=" + lblKmsEstimados.Text;
            strRequest += "&ddlVehiculo=" + ddlVehiculo.SelectedItem.Text;
            strRequest += "&ddlChofer=" + ddlChofer.SelectedItem.Text;
            strRequest += "&ddlAcompanante=" + ddlAcompanante.SelectedItem.Text;
            
            return strRequest;

        }

        private string getEspecificaciones()
        {
            string item ="", coma = "";
            foreach (ListItem it in lblEspecificacion.Items)
            {
                item += coma + it.Text;
            }

            return item;
        }

        private void InitScreen()
        {
            
            _idTrailer = 0;
            _idSolicitud = 0;
            _idOTI = 0;

            if (Request["idTrailer"] != null)
                _idTrailer = int.Parse(Request["idTrailer"]);
            

            if (Request["idSolicitud"] != null)
            {
                _idSolicitud = int.Parse(Request["idSolicitud"]);
                CargarSolicitud();
            }

            if (Request["idOTI"] != null && Request["idOTI"] != string.Empty)
            {
                _idOTI = int.Parse(Request["idOTI"]);
                CargarOTI();
            }
            if (!_Permiso.Escritura)
            {
                btnGuardar.Visible = false;
                btnCambiarTrailer.Visible = false;
            }
            if(_Permiso.oTipoUsuario.idTipoUsuario == _ADM)
                btnCambiarTrailer.Visible = true;
            else
                btnCambiarTrailer.Visible = false;
        }

        private void CargarOTI()
        {
            Model.OrdenTrabajo oOrdenTrabajo = new Model.OrdenTrabajo();
            OrdenTrabajoBus oOrdenTrabajoBus = new OrdenTrabajoBus();
            oOrdenTrabajo = oOrdenTrabajoBus.OrdenTrabajoGetById(_idOTI);
            ddlVehiculo.SelectedValue = oOrdenTrabajo.IdTransporte.ToString();            
            ddlChofer.SelectedValue = oOrdenTrabajo.IdChofer.ToString();
            ddlAcompanante.SelectedValue = oOrdenTrabajo.IdAcompanante.ToString();
            //ddlEquipo.SelectedValue = oOrdenTrabajo.IdEquipo.ToString();
            _idTrailer = oOrdenTrabajo.IdTrailer;
            lblNroOT.Text = oOrdenTrabajo.IdOrden.ToString();

            if (_idTrailer != 0)
            {
                ddlTrailer.Visible = false;
                lblTrailer.Visible = true;
                //btnCambiarTrailer.Visible = true;

                Trailer oTrailer = (new TrailerBus()).TrailerGetById(oOrdenTrabajo.IdTrailer);
                lblTrailer.Text = oTrailer.Descripcion;
                lblEspecificacion.Text = oTrailer.oEspecificacion.Descripcion;

                //List<EspecificacionTecnica>oListaEsp = new List<EspecificacionTecnica>();
                //oListaEsp.Add(oTrailer.oEspecificacion);
                //ddlTrailer.DataSource = (new TrailerBus()).TrailerGetDisponibles(oListaEsp);
                //ddlTrailer.DataBind();
            }
            //else
            //{
            //    lblTrailer.Text = " - ";
            //    lblTrailer.Visible = true;
            //    ddlTrailer.Visible = false;
            //    btnCambiarTrailer.Visible = false;
            //    //lblEspecificacion.Items.Clear();
            //}
        }

        private void CargarSolicitud()
        {
            SolicitudClienteBus oSolicitudClienteBus = new SolicitudClienteBus();
            SolicitudCliente oSolicitudCliente = new SolicitudCliente();
            oSolicitudCliente = oSolicitudClienteBus.SolicitudClienteGetById(_idSolicitud);

            lblNroSS.Text = oSolicitudCliente.NroSolicitud.ToString() + " " + oSolicitudCliente.letra;
            lblCondicion.Text = oSolicitudCliente.oCondicion.Descripcion;
            lblFecha.Text = oSolicitudCliente.Fecha.ToString("dd/MM/yyyy");

            lblEmpresa.Text = oSolicitudCliente.oSector.oEmpresa.Descripcion;
            txtNombre.Text = oSolicitudCliente.oSolicitante.Nombre;
            lblSector.Text = oSolicitudCliente.oSector.Descripcion;
            lblNroAviso.Text = oSolicitudCliente.NroPedidoCliente;
            lblTelefono.Text = oSolicitudCliente.oSolicitante.Telefono;

            lblLugarOrigen.Text = oSolicitudCliente.oMovimiento.oLugarOrigen.Descripcion;
            lblPozoOrigen.Text = oSolicitudCliente.oMovimiento.PozoOrigen;
            lblLugarDestino.Text = oSolicitudCliente.oMovimiento.oLugarDestino.Descripcion;
            lblPozoDestino.Text = oSolicitudCliente.oMovimiento.PozoDestino;
            lblKmsEstimados.Text = oSolicitudCliente.oMovimiento.KmsEstimados.ToString();
            lblTipoServicio.Text = oSolicitudCliente.oTipoServicio.Descripcion;

            CargarTrailer(oSolicitudCliente.oListEspecificaciones, oSolicitudCliente.nuevoMovimiento, oSolicitudCliente.NroSolicitud);

            if (_idTrailer != 0)
            {
                TrailerBus oTrailerBus = new TrailerBus();
                Trailer oTrailer = new Trailer();
                oTrailer = oTrailerBus.TrailerGetById(_idTrailer);
                lblTrailer.Text = oTrailer.Descripcion;
                lblEspecificacion.Items.Add(oTrailer.oEspecificacion.Descripcion);
                //btnCambiarTrailer.Visible = true;
            }
            else
            {
                if (oSolicitudCliente.oListEspecificaciones.Count > 0)
                {
                    lblTrailer.Visible = false;
                    ddlTrailer.Visible = true;
                    btnCambiarTrailer.Visible = false;
                                        
                    lblEspecificacion.DataSource = oSolicitudCliente.oListEspecificaciones;                    
                    lblEspecificacion.DataValueField = "idEspecificaciones";
                    lblEspecificacion.DataTextField = "Descripcion";
                    lblEspecificacion.DataBind();
                }
                else
                {
                    lblTrailer.Text = " - ";
                    lblEspecificacion.Items.Add(oSolicitudCliente.ServicioAux);
                    btnCambiarTrailer.Visible = false;
                }
            }
        }

        private void CargarEquipos()
        {
            //EquiposBus oEquiposBus = new EquiposBus();
            //ddlEquipo.DataSource = oEquiposBus.EquiposGetAll();
            //ddlEquipo.DataTextField = "Descripcion";
            //ddlEquipo.DataValueField = "idEquipo";
            //ddlEquipo.DataBind();
            //ddlEquipo.Items.Insert(0, new ListItem("Seleccione", "0"));
        }

        private void CargarTrailer(List<EspecificacionTecnica> oListsEsp, bool nuevoMov, int nroSolicitud)
        {
            
            TrailerBus oTrailerBus = new TrailerBus();
            if (!nuevoMov)
                ddlTrailer.DataSource = oTrailerBus.TrailerGetDisponibles(oListsEsp);
            else
                ddlTrailer.DataSource = oTrailerBus.TrailerGetNoDisponibles(oListsEsp,nroSolicitud);
            ddlTrailer.DataTextField = "Descripcion";
            ddlTrailer.DataValueField = "idTrailer";
            ddlTrailer.DataBind();
            ddlTrailer.Items.Insert(0, new System.Web.UI.WebControls.ListItem("Seleccione un Equipo", "0"));
        }

        private void CargarChofer()
        {
            ChoferBus oChoferBus = new ChoferBus();
            oChoferBus.CargarCombo(ref ddlChofer, "Asignar Chofer");
        }

        private void CargarAcompanante()
        {
            ChoferBus oChoferBus = new ChoferBus();
            oChoferBus.CargarCombo(ref ddlAcompanante, "Asignar Acompañante");
        }

        private void CargarVehiculo()
        {
            TransporteBus oTransporteBus = new TransporteBus();
            ddlVehiculo.DataSource = oTransporteBus.TransporteGetAll();
            ddlVehiculo.DataTextField = "Descripcion";
            ddlVehiculo.DataValueField = "idTransporte";            
            ddlVehiculo.DataBind();
            ddlVehiculo.Items.Insert(0, new System.Web.UI.WebControls.ListItem("Seleccione un Vehículo", "0"));
        }

        #endregion

    }
}
