﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ProcesoPP.Model;
using ProcesoPP.Business;
using ProcesoPP.Services;

namespace ProcesoPP.UI.OrdenTrabajo
{
    public partial class frmOrdenTrabajoGestion : System.Web.UI.Page
    {
        #region PROPIEDADES
        public int _idUsuario
        {
            get { return (int)ViewState["idUsuario"]; }
            set { ViewState["idUsuario"] = value; }
        }

        public Permisos _Permiso
        {
            get { return (Permisos)ViewState["permiso"]; }
            set { ViewState["permiso"] = value; }
        }
        #endregion

        #region EVENTOS

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                _idUsuario = Common.EvaluarSession(Request, this.Page);
                if (_idUsuario != 0)
                {
                    _Permiso = Common.getModulo(Request, this.Page);
                    if (_Permiso != null)
                    {
                        if (!IsPostBack)
                        {
                            CargarEmpresa();
                            CargarEquipo();
                        }
                    }
                    else
                    {
                        Mensaje.errorMsj("NO TIENE PERMISOS A ESTE FORMULARIO", this.Page, "SIN ACCESO", "../frmPrincipal.aspx");
                    }
                }
                else
                {
                    Mensaje.errorMsj("DEBE LOGUEARSE", this.Page, "LOGIN", "../frmLogin.aspx");
                }
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                CargarOTI();
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        protected void gvOTI_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "Verificar":
                    HiddenField hfIdParte = ((DataControlFieldCell)((Control)(e.CommandSource)).Parent).FindControl("hfidParte") as HiddenField;
                    Response.Redirect("../Verificacion/frmVerificacionIda.aspx?idOTI=" + e.CommandArgument + "&idParte=" + hfIdParte.Value + "&idModulo=" + _Permiso.oModulo.idModulo);
                    break;
                case "Ver":
                    HiddenField hfIdSC = ((DataControlFieldCell)((Control)(e.CommandSource)).Parent).FindControl("hfIdSC") as HiddenField;
                    HiddenField hfIdParteVer = ((DataControlFieldCell)((Control)(e.CommandSource)).Parent).FindControl("hfidParteVer") as HiddenField;
                    Response.Redirect("../Verificacion/frmVerificacionVer.aspx?idOTI=" + e.CommandArgument + "&idParte=" + hfIdParteVer.Value + "&idModulo=" + _Permiso.oModulo.idModulo);
                    break;
            }
        }
        
        protected void gvOTI_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                CargarRow(e);
            }
        }
        
        protected void gvOTI_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvOTI.PageIndex = e.NewPageIndex;
            CargarOTI();
        }

        #endregion

        #region METODOS

        private void CargarRow(GridViewRowEventArgs e)
        {
            Common.CargarRow(e, _Permiso);

            LinkButton btnVer = e.Row.FindControl("btnVer") as LinkButton;
            HiddenField hfidParte = e.Row.FindControl("hfidParte") as HiddenField;

            if (int.Parse(hfidParte.Value) == 0)
                btnVer.Visible = false;
            else
                btnVer.Visible = true;
        }
        
        private void Eliminar(string idOrden)
        {
            Model.OrdenTrabajo oOrdenTrabajo = new Model.OrdenTrabajo();
            OrdenTrabajoBus oOrdenTrabajoBus = new OrdenTrabajoBus();
            oOrdenTrabajo = oOrdenTrabajoBus.OrdenTrabajoGetById(int.Parse(idOrden));
            oOrdenTrabajo.Baja = true;
            oOrdenTrabajoBus.OrdenTrabajoUpdate(oOrdenTrabajo);

            CargarOTI();
        }

        private void CargarEmpresa()
        {
            EmpresaBus oEmpresaBus = new EmpresaBus();
            oEmpresaBus.CargarEmpresa(ref ddlEmpresa,"Seleccione...");
        }

        private void CargarOTI()
        {
            OrdenTrabajoBus oOrdenTrabajoBus = new OrdenTrabajoBus();
            gvOTI.DataSource = oOrdenTrabajoBus.OrdenTrabajoGetByFilter(txtNroSolicitud.Text == "" ? "0" : txtNroSolicitud.Text,
                                                                          ddlEmpresa.SelectedValue,
                                                                          txtFecha.Text,
                                                                          txtFechaHasta.Text,
                                                                          txtNroParte.Text,
                                                                          ddlTrailer.SelectedValue);
            gvOTI.DataBind();
        }

        private void CargarEquipo()
        {
            TrailerBus oTrailerBus = new TrailerBus();
            oTrailerBus.CargarCombo(ref ddlTrailer, "Seleccione un equipo...");
        }
        
        #endregion

        
    }
}
