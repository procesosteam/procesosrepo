﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ProcesoPP.Business;
using ProcesoPP.Model;
using System.IO;
using System.Text;

using iTextSharp;
using iTextSharp.text.pdf;
using iTextSharp.text;
using iTextSharp.text.pdf.parser;
using System.Collections;

namespace ProcesoPP.UI.OrdenTrabajo
{
    public partial class frmOrdenTrabajoCarga : System.Web.UI.Page
    {
        #region <PROPIEDADES>

        public int _idUsuario
        {
            get { return (int)ViewState["idUsuario"]; }
            set { ViewState["idUsuario"] = value; }
        }

        private int _idSolicitud
        {
            get { return (int)ViewState["idSolicitud"]; }
            set { ViewState["idSolicitud"] = value; }
        }

        private int _idTrailer
        {
            get { return (int)ViewState["idTrailer"]; }
            set { ViewState["idTrailer"] = value; }
        }

        private int _idOTI
        {
            get { return (int)ViewState["idOTI"]; }
            set { ViewState["idOTI"] = value; }
        }

        #endregion

        #region <EVENTOS>

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {                  
                    CargarVehiculo();
                    CargarAcompanante();
                    CargarChofer();
                    InitScreen();
                }
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                GuardarOT();
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        protected void btnImprimir_Click(object sender, EventArgs e)
        {
            try
            {
                //GuardarOT();
                Imprimir();
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        protected void btnCambiarTrailer_Click(object sender, EventArgs e)
        {
            ddlTrailer.Visible = true;
            lblTrailer.Visible = false;
            btnCambiarTrailer.Visible = false;
        }

        #endregion

        #region <METODOS>

        public override void VerifyRenderingInServerForm(Control control)
        { 
        }

        private void Imprimir()
        {
            string sPathApp = System.AppDomain.CurrentDomain.BaseDirectory.ToString();
            string fileName = sPathApp + @"Reportes\oti.pdf";
            string copia = "ParteEntrega_" + DateTime.Now.ToString("yyyyMMdd_HHmmss") +".pdf";

            //PdfReader reader = new PdfReader(fileName);
            //PdfStamper stamper = new PdfStamper(reader, new FileStream(copia, FileMode.Create));
            
            
            //Document doc = new Document();            

            ////PdfWriter writer = PdfWriter.GetInstance(doc, memoryStream);
            //doc.Open();
            //doc.Add(new Paragraph(ExtractText(fileName)));
            ////sw.Close();
            //doc.Close();
            //Get the pointer to the beginning of the stream. 

            MemoryStream memoryStream = new MemoryStream();
            
            Merge(copia, fileName);

             
        }

        public void Merge(string strFileTarget, string arrStrFilesSource)
        {
            
            // Crea el PDF de salida
            try
            {

                //System.IO.FileStream stmFile = new System.IO.FileStream(strFileTarget, System.IO.FileMode.Create);
                System.IO.MemoryStream stmFile = new MemoryStream();

                Document objDocument = null;
                PdfWriter objWriter = null;

                // Recorre los archivos
                //for (int intIndexFile = 0; intIndexFile < arrStrFilesSource.Length; intIndexFile++)
                //{
                PdfReader objReader = new PdfReader(arrStrFilesSource);
                int intNumberOfPages = objReader.NumberOfPages;

                // La primera vez, inicializa el documento y el escritor
                //if (intIndexFile == 0)
                //{ // Asigna el documento y el generador
                objDocument = new Document(objReader.GetPageSizeWithRotation(1));
                objWriter = PdfWriter.GetInstance(objDocument, stmFile);
                // Abre el documento
                objDocument.Open();
                //}
                // Añade las páginas
                for (int intPage = 0; intPage < intNumberOfPages; intPage++)
                {
                    int intRotation = objReader.GetPageRotation(intPage + 1);
                    PdfImportedPage objPage = objWriter.GetImportedPage(objReader, intPage + 1);

                    // Asigna el tamaño de la página
                    objDocument.SetPageSize(objReader.GetPageSizeWithRotation(intPage + 1));
                    // Crea una nueva página
                    objDocument.NewPage();
                    // Añade la página leída
                    if (intRotation == 90 || intRotation == 270)
                        objWriter.DirectContent.AddTemplate(objPage, 0, -1f, 1f, 0, 0,
                                                            objReader.GetPageSizeWithRotation(intPage + 1).Height);
                    else
                        objWriter.DirectContent.AddTemplate(objPage, 1f, 0, 0, 1f, 0, 0);
                    agregarCampos(objWriter, objPage);
                }
                //}
                // Cierra el documento
                if (objDocument != null)
                    objDocument.Close();
                // Cierra el stream del archivo
                //stmFile.Close();
                Response.ContentType = "application/pdf";

                Response.AddHeader("content-disposition", "attachment;filename="+ strFileTarget);

                Response.Buffer = true;

                Response.Clear();

                Response.OutputStream.Write(stmFile.GetBuffer(), 0, stmFile.GetBuffer().Length);

                Response.OutputStream.Flush();

                Response.End();
                // Indica que se ha creado el documento
               
            }
            catch (Exception objException)
            {
                System.Diagnostics.Debug.WriteLine(objException.Message);
                
            }
            // Devuelve el valor que indica si se han mezclado los archivos
           // return blnMerged;
        }

        private void agregarCampos(PdfWriter objWriter, PdfImportedPage objPage)
        {
            PdfContentByte pb = objWriter.DirectContent;
            pb.BeginText();            
            pb.ShowText(lblEmpresa.Text);
            pb.EndText();
        }

        //public void DownloadLabTestResult(Document doc,string fileNewName)
        //{
           

        //    Response.Clear();
        //    Response.ContentType = "application/pdf";
        //    Response.AddHeader("content-disposition", "attachment;filename="+fileNewName);
                       
        //    Response.Write(doc);
        //    Response.Flush();
        //    Response.End();

        //   // return new FileStreamResult(Response.OutputStream, "application/pdf");

        //}
        //private void CreatePDFByCopy(string fileNameCopia, string fileNameOrig)
        //{
        //    using (Document document = new Document())
        //    {
        //        using (PdfSmartCopy copy = new PdfSmartCopy(document, new FileStream(fileNameCopia, FileMode.Create)))
        //        {
        //            document.Open();

        //            string text = ExtractText(fileNameOrig);
        //            document.Add(new Paragraph(text));
                   
        //        }
        //    }
        //}
        //public byte[] AddDataSheets(string _data, string fileNameOrig)
        //{
        //    PdfReader reader = new PdfReader(fileNameOrig);
        //    using (MemoryStream ms = new MemoryStream())
        //    {
        //        using (PdfStamper stamper = new PdfStamper(reader, ms))
        //        {
        //            stamper.RotateContents = false;
        //            PdfContentByte canvas = stamper.GetOverContent(1);
        //            ColumnText.ShowTextAligned(canvas, Element.ALIGN_LEFT, new Phrase(DateTime.Now.ToShortDateString()), 0, 0, 0);
        //            stamper.Close();
        //        }
        //        return ms.ToArray();
        //    }
        //}
        private string ExtractText(string fileNameOrig)
        {
            PdfReader reader = new PdfReader(fileNameOrig);
            string txt = PdfTextExtractor.GetTextFromPage(reader, 1, new LocationTextExtractionStrategy());
            return txt;
        }

        //private void DownloadAsPDF(FileStream ms )
        //{
        //    Response.Clear();
        //    Response.ClearContent();
        //    Response.ClearHeaders();
        //    Response.ContentType = "application/pdf";
        //    Response.AppendHeader("Content-Disposition", "attachment;filename=abc.pdf");
        //    Response.Write(ms);
        //    Response.Flush();
        //    Response.Close();
        //    Response.End();
        //    ms.Close();
        //}

        //private void ShowPdf(string fileNameCopia, StreamWriter sw)
        //{

        //    //HttpContext.Current.Response.ContentType = "application/pdf";
        //    //HttpContext.Current.Response.AddHeader("content-disposition", "attachment;filename=ParteEntrega.pdf");
        //    //HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        //    //HttpContext.Current.Response.Write(pdfDoc);
        //    //HttpContext.Current.Response.End();
                        
        //    Response.Clear();
        //    Response.ContentType = "application/pdf";
        //    Response.AddHeader("content-disposition", "attachment;filename=" + fileNameCopia);
        //    Response.ContentType = "application/octet-stream";
        //    Response.Write(sw);
        //    sw.Close();
        //    Response.End();

        //}


        private void GuardarOT()
        {
            OrdenTrabajoBus oOrdenTrabajoBus = new OrdenTrabajoBus();
            Model.OrdenTrabajo oOrdenTrabajo = new Model.OrdenTrabajo();

            oOrdenTrabajo.IdSolicitudCliente = _idSolicitud;
            oOrdenTrabajo.IdTransporte = int.Parse(ddlVehiculo.SelectedValue);
            oOrdenTrabajo.IdTrailer = ddlTrailer.Visible ? int.Parse(ddlTrailer.SelectedValue) : _idTrailer;
            //oOrdenTrabajo.IdEquipo = int.Parse(ddlEquipo.SelectedValue);
            oOrdenTrabajo.IdAcompanante = int.Parse(ddlAcompanante.SelectedValue);
            oOrdenTrabajo.IdChofer = int.Parse(ddlChofer.SelectedValue);
            oOrdenTrabajo.Entraga = true;

            if (_idOTI == 0)
            {
                oOrdenTrabajo.IdOrden = oOrdenTrabajoBus.OrdenTrabajoAdd(oOrdenTrabajo);
                _idOTI = oOrdenTrabajo.IdOrden;
            }
            else
            {
                oOrdenTrabajo.IdOrden = _idOTI;
                oOrdenTrabajoBus.OrdenTrabajoUpdate(oOrdenTrabajo);
            }

            if (oOrdenTrabajo.IdOrden > 0)
            {
                Mensaje.successMsj("Orden Guardada!", this.Page, "Exito", "../SolicitudServicio/frmSolicitudGestion.aspx");
                lblNroOT.Text = _idOTI.ToString();
            }

        }

        private void InitScreen()
        {
            _idTrailer = 0;
            _idSolicitud = 0;
            _idOTI = 0;

            if (Request["idTrailer"] != null)
                _idTrailer = int.Parse(Request["idTrailer"]);
            

            if (Request["idSolicitud"] != null)
            {
                _idSolicitud = int.Parse(Request["idSolicitud"]);
                CargarSolicitud();
            }

            if (Request["idOTI"] != null && Request["idOTI"] != string.Empty)
            {
                _idOTI = int.Parse(Request["idOTI"]);
                CargarOTI();
            }
            
        }

        private void CargarOTI()
        {
            Model.OrdenTrabajo oOrdenTrabajo = new Model.OrdenTrabajo();
            OrdenTrabajoBus oOrdenTrabajoBus = new OrdenTrabajoBus();
            oOrdenTrabajo = oOrdenTrabajoBus.OrdenTrabajoGetById(_idOTI);
            ddlVehiculo.SelectedValue = oOrdenTrabajo.IdTransporte.ToString();            
            ddlChofer.SelectedValue = oOrdenTrabajo.IdChofer.ToString();
            ddlAcompanante.SelectedValue = oOrdenTrabajo.IdAcompanante.ToString();
            //ddlEquipo.SelectedValue = oOrdenTrabajo.IdEquipo.ToString();
            _idTrailer = oOrdenTrabajo.IdTrailer;
            lblNroOT.Text = oOrdenTrabajo.IdOrden.ToString();

            if (_idTrailer != 0)
            {
                ddlTrailer.Visible = false;
                lblTrailer.Visible = true;
                btnCambiarTrailer.Visible = true;

                Trailer oTrailer = (new TrailerBus()).TrailerGetById(oOrdenTrabajo.IdTrailer);
                lblTrailer.Text = oTrailer.Descripcion;
                lblEspecificacion.Text = oTrailer.oEspecificacion.Descripcion;

                //List<EspecificacionTecnica>oListaEsp = new List<EspecificacionTecnica>();
                //oListaEsp.Add(oTrailer.oEspecificacion);
                //ddlTrailer.DataSource = (new TrailerBus()).TrailerGetDisponibles(oListaEsp);
                //ddlTrailer.DataBind();
            }
        }

        private void CargarSolicitud()
        {
            SolicitudClienteBus oSolicitudClienteBus = new SolicitudClienteBus();
            SolicitudCliente oSolicitudCliente = new SolicitudCliente();
            oSolicitudCliente = oSolicitudClienteBus.SolicitudClienteGetById(_idSolicitud);

            lblNroSS.Text = oSolicitudCliente.NroSolicitud.ToString();
            lblCondicion.Text = oSolicitudCliente.oCondicion.Descripcion;
            lblFecha.Text = oSolicitudCliente.Fecha.ToString("dd/MM/yyyy HH:mm");

            lblEmpresa.Text = oSolicitudCliente.oSector.oEmpresa.Descripcion;
            txtNombre.Text = oSolicitudCliente.oSolicitante.Nombre;
            lblSector.Text = oSolicitudCliente.oSector.Descripcion;
            lblNroAviso.Text = oSolicitudCliente.NroPedidoCliente;
            lblTelefono.Text = oSolicitudCliente.oSolicitante.Telefono;

            lblLugarOrigen.Text = oSolicitudCliente.oMovimiento.oLugarOrigen.Descripcion;
            lblPozoOrigen.Text = oSolicitudCliente.oMovimiento.PozoOrigen;
            lblLugarDestino.Text = oSolicitudCliente.oMovimiento.oLugarDestino.Descripcion;
            lblPozoDestino.Text = oSolicitudCliente.oMovimiento.PozoDestino;
            lblKmsEstimados.Text = oSolicitudCliente.oMovimiento.KmsEstimados.ToString();
            lblTipoServicio.Text = oSolicitudCliente.oTipoServicio.Descripcion;

            CargarTrailer(oSolicitudCliente.oListEspecificaciones, oSolicitudCliente.nuevoMovimiento);

            if (_idTrailer != 0)
            {
                TrailerBus oTrailerBus = new TrailerBus();
                Trailer oTrailer = new Trailer();
                oTrailer = oTrailerBus.TrailerGetById(_idTrailer);
                lblTrailer.Text = oTrailer.Descripcion;
                lblEspecificacion.Items.Add(oTrailer.oEspecificacion.Descripcion);
                btnCambiarTrailer.Visible = true;
            }
            else
            {
                if (oSolicitudCliente.oListEspecificaciones.Count > 0)
                {
                    lblTrailer.Visible = false;
                    ddlTrailer.Visible = true;
                    btnCambiarTrailer.Visible = false;
                                        
                    lblEspecificacion.DataSource = oSolicitudCliente.oListEspecificaciones;                    
                    lblEspecificacion.DataValueField = "idEspecificaciones";
                    lblEspecificacion.DataTextField = "Descripcion";
                    lblEspecificacion.DataBind();
                }
                else
                {
                    lblTrailer.Text = " - ";
                    lblEspecificacion.Items.Add(oSolicitudCliente.ServicioAux);
                    btnCambiarTrailer.Visible = false;
                }
            }
        }

        private void CargarEquipos()
        {
            //EquiposBus oEquiposBus = new EquiposBus();
            //ddlEquipo.DataSource = oEquiposBus.EquiposGetAll();
            //ddlEquipo.DataTextField = "Descripcion";
            //ddlEquipo.DataValueField = "idEquipo";
            //ddlEquipo.DataBind();
            //ddlEquipo.Items.Insert(0, new ListItem("Seleccione", "0"));
        }

        private void CargarTrailer(List<EspecificacionTecnica> oListsEsp, bool nuevoMov)
        {
            
            TrailerBus oTrailerBus = new TrailerBus();
            if (!nuevoMov)
                ddlTrailer.DataSource = oTrailerBus.TrailerGetDisponibles(oListsEsp);
            else
                ddlTrailer.DataSource = oTrailerBus.TrailerGetNoDisponibles(oListsEsp);
            ddlTrailer.DataTextField = "Descripcion";
            ddlTrailer.DataValueField = "idTrailer";
            ddlTrailer.DataBind();
        }

        private void CargarChofer()
        {
            UsuarioBus oUsuarioBus = new UsuarioBus();
            ddlChofer.DataSource = oUsuarioBus.UsuarioGetAll();
            ddlChofer.DataValueField = "idUsuario";
            ddlChofer.DataTextField = "Nombre";
            ddlChofer.DataBind();
            ddlChofer.Items.Insert(0, new System.Web.UI.WebControls.ListItem("Asignar Chofer", "0"));
        }

        private void CargarAcompanante()
        {
            UsuarioBus oUsuarioBus = new UsuarioBus();
            ddlAcompanante.DataSource = oUsuarioBus.UsuarioGetAll();
            ddlAcompanante.DataValueField = "idUsuario";
            ddlAcompanante.DataTextField = "Nombre";
            ddlAcompanante.DataBind();
            ddlAcompanante.Items.Insert(0, new System.Web.UI.WebControls.ListItem("Asignar Acompañante", "0"));
        }

        private void CargarVehiculo()
        {
            TransporteBus oTransporteBus = new TransporteBus();
            ddlVehiculo.DataSource = oTransporteBus.TransporteGetAll();
            ddlVehiculo.DataTextField = "Descripcion";
            ddlVehiculo.DataValueField = "idTransporte";            
            ddlVehiculo.DataBind();
            ddlVehiculo.Items.Insert(0, new System.Web.UI.WebControls.ListItem("Seleccione un Vehículo", "0"));
        }

        #endregion

    }
}
