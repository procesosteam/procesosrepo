﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ProcesoPP.Model;
using ProcesoPP.Business;

namespace ProcesoPP.UI
{
    public partial class frmPrincipal : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["idUsuario"] != null)
            {
                int _idUsuario = int.Parse(Session["idUsuario"].ToString());
                CargarUsuario(_idUsuario);
            }
        }

        private void CargarUsuario(int _idUsuario)
        {
            Usuario oUsuario = new Usuario();
            UsuarioBus oUsuarioBus = new UsuarioBus();
            oUsuario = oUsuarioBus.UsuarioGetById(_idUsuario);
            lblUser.Text = oUsuario.UserName;
        }
    }
}
