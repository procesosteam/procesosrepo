<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="frmVerificacionIda.aspx.cs"  
EnableEventValidation="false"  MasterPageFile="~/frmPrincipal.Master"  
Inherits="ProcesoPP.UI.Verificacion.frmVerificacionIda" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server"> </asp:Content>
<asp:Content ID="cphContenedor" ContentPlaceHolderID="ContentPlaceHolder1" runat="server" >
<div class="tabla3" id="divMensaje" runat="server"><asp:Label ID="lblError" CssClass="error2" runat="server" Text="Error" Visible="false"></asp:Label></div>
<div id="contenido" runat="server" >
<div id="apertura">Parte de Entrega Trailer.</div>
<div class="solicitud100">
    <div class="sol_tit2"><label>Empresa: </label></div>
   <div class="sol_ver30"><asp:Label runat="server" ID="lblEmpresa"></asp:Label></div>
    <div class="sol_tit2"><label>Sector</label></div>
<div class="sol_ver30"><asp:Label runat="server" ID="lblSector"></asp:Label></div>
<div class="sol_tit2"><label>Solicitante: </label></div>
<div class="sol_ver30"><asp:Label runat="server" ID="lblSolicitante"></asp:Label></div></div>
<div class="solicitud100">
    <div class="sol_tit2"><label>Condici�n</label></div>
    <div class="sol_ver30"> <asp:Label runat ="server" ID="lblCondicion" /> </div>
    <div class="sol_tit2"><label>Equipo</label></div>
    <div class="sol_ver30"><asp:Label runat="server" ID="lblTrailer"></asp:Label></div>

<div class="sol_tit2"><label>Km Est.</label></div>
<div class="sol_ver30"><asp:Label runat="server" ID="lblKms"></asp:Label></div></div>


<div class="solicitud100">
    <div class="sol_tit2"><label>Origen:</label></div>
    <div class="sol_ver30"><asp:Label runat="server" ID="lblOrigen"></asp:Label></div>
    <div class="sol_tit2b"><label>Eq:</label></div>
    <div class="sol_ver10"><asp:Label runat="server" ID="lblPozoOrigen"></asp:Label></div>
    <div class="sol_tit2"><label>Destino:</label></div>
    <div class="sol_ver30"><asp:Label runat="server" ID="lblDestino"></asp:Label></div>
    <div class="sol_tit2b"><label>Eq:</label></div>
    <div class="sol_ver10"><asp:Label runat="server" ID="lblPozoDestino"></asp:Label></div>
</div>

<div class="solicitud100">
    <div class="sol_campo50">
    <asp:CheckBox runat ="server" ID="chkCon" Text="Conexi�n" TextAlign="Left"/>
    <asp:CheckBox runat ="server" ID="chkDescon" Text="Desconexi�n" TextAlign="Left"/></div>
</div>

<div class="solicitud100">
    <div class="sol_tit2"><asp:Label ID="Label1" runat="server" Text="SS N�"/></div>
    <div class="sol_ver30"><asp:Label id="lblNroSS"  runat="server"/></div>
    <div class="sol_tit2"><asp:Label runat="server" name="NroParte" Text="Parte N�:" /></div>
    <div class="sol_ver30"><asp:TextBox ID="lblNroParte" name="NroParte" CssClass="sol_campo25" runat="server" onkeypress="return ValidarNumeroEntero(event,this);"/></div>  
    <div class="sol_tit2"><asp:Label runat="server" Text="Remito:" Visible="false" ID="lblRemito"/></div>
    <div class="sol_ver30"><asp:TextBox ID="txtNroRemito" Visible="false"  CssClass="sol_campo25" runat="server" onkeypress="return ValidarNumeroEntero(event,this);"/></div>    
    
</div>

<%--<div class="solicitud100">
    <div class="sol_tit50"><asp:Label runat="server" Text="Hs Salida:" /></div>
    <div class="sol_campo30"><asp:TextBox id="txtHsSalida" CssClass="sol_campo25" runat="server"/></div>
    <div  class="sol_fecha"><img id="btnFecha" alt="calendario" src="../img/calendario.gif" style="width:20px; height:20px; cursor:pointer;" onmouseover="CambiarConfiguracionCalendario('txtHsSalida',this);"/></div>
    
    <div class="sol_tit50"><asp:Label runat="server" Text="Hs Regreso:"/></div>
    <div class="sol_campo30"><asp:TextBox id="txtHSRegreso" CssClass="sol_campo25" runat="server"/></div>
    <div  class="sol_fecha"><img id="Img1" alt="calendario" src="../img/calendario.gif" style="width:20px; height:20px; cursor:pointer;" onmouseover="CambiarConfiguracionCalendario('txtHSRegreso',this);"/></div><div class="sol_fecha"></div>
</div>--%>
<div runat="server" id="divVerificacion">
<div id="titulos">Detalle Exterior</div>
<div class="solicitud100">
    <div class="campo_p"><asp:Label runat="server" name="Lanza" Text="Lanza"/></div>
    <div class="columnav"><asp:TextBox ID="txtLanza" CssClass="sol_campo25" name="Lanza" runat="server" onkeypress="return ValidarNumeroEntero(event,this);" /></div>

    <div class="campo_p"><asp:Label runat="server" name="Cajon HTA" Text="Caj�n HTA"/></div>
    <div class="columnav"><asp:TextBox runat="server" CssClass="sol_campo25" name="Cajon HTA" id="txtHTA" onkeypress="return ValidarNumeroEntero(event,this);"/></div>

    <div class="campo_p"><asp:Label runat="server" name="Ojal" Text="Ojal"/></div>
    <div class="columnav"><asp:TextBox runat="server" CssClass="sol_campo25" name="Ojal" id="txtOjal" onkeypress="return ValidarNumeroEntero(event,this);"/></div>
</div>
<div class="solicitud100">
    <div class="campo_p"><asp:Label runat="server" name="Reventim" Text="Revestim."/></div>
    <div class="columnav"><asp:TextBox  runat="server" CssClass="sol_campo25" name="Reventim" id="txtRev" onkeypress="return ValidarNumeroEntero(event,this);" /></div>

    <div class="campo_p"><asp:Label runat="server" name="Pinza" Text="Pinza"/></div>
    <div class="columnav"><asp:TextBox  runat="server" CssClass="sol_campo25" name="Pinza" id="txtPinza" onkeypress="return ValidarNumeroEntero(event,this);" /></div>

    <div class="campo_p"><asp:Label runat="server" name="Escalera" Text="Escalera"/></div>
    <div class="columnav"><asp:TextBox  runat="server" CssClass="sol_campo25" name="Escalera" id="txtEscalera" onkeypress="return ValidarNumeroEntero(event,this);" /></div>
</div>
<div class="solicitud100">
    <div class="campo_p"><asp:Label runat="server" name="Plato" Text="Plato"/></div>
    <div class="columnav"><asp:TextBox  runat="server" CssClass="sol_campo25" name="Plato" id="txtPlato" onkeypress="return ValidarNumeroEntero(event,this);" /></div>

    <div class="campo_p"><asp:Label runat="server" name="Descanso" Text="Descanso"/></div>
    <div class="columnav"><asp:TextBox  runat="server" CssClass="sol_campo25" name="Descanso" id="txtDescanso" onkeypress="return ValidarNumeroEntero(event,this);" /></div>

    <div class="campo_p"><asp:Label runat="server" name="Barandas" Text="Barandas"/></div>
    <div class="columnav"><asp:TextBox  runat="server" CssClass="sol_campo25" name="Barandas" id="txtBarandas" onkeypress="return ValidarNumeroEntero(event,this);" /></div>
</div>
<div class="solicitud100">
    <div class="campo_p"><asp:Label runat="server" name="Ejes" Text="Ejes"/></div>
    <div class="columnav"><asp:TextBox  runat="server" CssClass="sol_campo25" name="Ejes" id="txtEjes" onkeypress="return ValidarNumeroEntero(event,this);" /></div>

    <div class="campo_p"><asp:Label runat="server" name="Soporte Esc." Text="Soporte Esc."/></div>
    <div class="columnav"><asp:TextBox  runat="server" CssClass="sol_campo25" name="Soporte Esc." id="txtSopor" onkeypress="return ValidarNumeroEntero(event,this);" /></div>

   <div class="campo_p"><asp:Label runat="server" name="Estacas" Text="Estacas"/></div>
    <div class="columnav"><asp:TextBox runat="server" CssClass="sol_campo25" name="Estacas" id="txtEstaca" onkeypress="return ValidarNumeroEntero(event,this);" /></div>
</div>
<div class="solicitud100">
    <div class="campo_p"><asp:Label runat="server" name="Elasticos" Text="Elasticos"/></div>
    <div class="columnav"><asp:TextBox runat="server" CssClass="sol_campo25" name="Elasticos" id="txtElasticos" onkeypress="return ValidarNumeroEntero(event,this);" /></div>

    <div class="campo_p"><asp:Label runat="server" name="Mazas" Text="Mazas"/></div>
    <div class="columnav"><asp:TextBox runat="server" CssClass="sol_campo25" name="Mazas" id="txtMazas" onkeypress="return ValidarNumeroEntero(event,this);" /></div>

    <div class="campo_p"><asp:Label runat="server" name="Luces Tran." Text="Luces Tran."/></div>
    <div class="columnav"><asp:TextBox runat="server" id="txtLuces" CssClass="sol_campo25" name="Luces Tran." onkeypress="return ValidarNumeroEntero(event,this);"/></div>
</div>
<div id="titulos">Detalle Interior</div>

<div class="solicitud100">
    <div id="pe_campo">Dormitorio</div>
    <div class="campo_p"><asp:Label runat="server" Text="Camas" name="Camas"/></div>
    <div class="columnav"><asp:TextBox  runat="server" id="txtCamas" CssClass="sol_campo25" name="Camas" onkeypress="return ValidarNumeroEntero(event,this);"/></div>

    <div class="campo_p"><asp:Label runat="server" Text="Colchones" name="Colchones"/></div>
    <div class="columnav"><asp:TextBox  runat="server" id="txtColchones" CssClass="sol_campo25" name="Colchones" onkeypress="return ValidarNumeroEntero(event,this);"/></div>
</div>
<div class="solicitud100">
    <div id="pe_spacer"></div>
   <div class="campo_p"><asp:Label runat="server" Text="Almohadas" name="Almohadas"/></div>
    <div class="columnav"><asp:TextBox  runat="server" id="txtAlmohadas" CssClass="sol_campo25" name="Almohadas" onkeypress="return ValidarNumeroEntero(event,this);"/></div>

    <div class="campo_p"><asp:Label runat="server" Text="Cortinas" name="Cortinas"/></div>
    <div class="columnav"><asp:TextBox  runat="server" id="txtCortinas" CssClass="sol_campo25" name="Cortinas" onkeypress="return ValidarNumeroEntero(event,this);"/></div>
</div>
<div class="solicitud100">

    <div id="pe_spacer"></div>
   <div class="campo_p"><asp:Label runat="server" Text="Veladores" name="Veladores"/></div>
    <div class="columnav"><asp:TextBox  runat="server" id="txtVeladores" CssClass="sol_campo25" name="Veladores" onkeypress="return ValidarNumeroEntero(event,this);"/></div>

    <div class="campo_p"><asp:Label runat="server" Text="Placard" name="Placard"/></div>
    <div class="columnav"><asp:TextBox  runat="server" id="txtPlacard" CssClass="sol_campo25" name="Placard" onkeypress="return ValidarNumeroEntero(event,this);"/></div>
</div>
<div class="solicitud100">
    <div id="pe_campo">Ba�o</div>
    <div class="campo_p"><asp:Label runat="server" Text="Inodoro" name="Inodoro"/></div>
    <div class="columnav"><asp:TextBox  runat="server" id="txtInodoro" CssClass="sol_campo25" name="Inodoro" onkeypress="return ValidarNumeroEntero(event,this);"/></div>

    <div class="campo_p"><asp:Label runat="server" Text="Ducha" name="Ducha"/></div>
    <div class="columnav"><asp:TextBox  runat="server" id="txtDucha" CssClass="sol_campo25" name="Ducha" onkeypress="return ValidarNumeroEntero(event,this);"/></div>
</div>
<div class="solicitud100">
    <div id="pe_spacer"></div>
    <div class="campo_p"><asp:Label runat="server" Text="Lavatorio" name="Lavatorio"/></div>
    <div class="columnav"><asp:TextBox  runat="server" id="txtLavatorio" CssClass="sol_campo25" name="Lavatorio" onkeypress="return ValidarNumeroEntero(event,this);"/></div>
    <div class="campo_p"><asp:Label runat="server" Text="--"/></div>
    <div class="columnav"></div>
</div>
<div class="solicitud100">
    <div id="pe_campo">Comedor</div>
    <div class="campo_p"><asp:Label runat="server" Text="Cocina" name="Cocina"/></div>
    <div class="columnav"><asp:TextBox  runat="server" id="txtCocina" CssClass="sol_campo25" name="Cocina" onkeypress="return ValidarNumeroEntero(event,this);" /></div>

    <div class="campo_p"><asp:Label runat="server" Text="Horno Elect." name="Horno Elect."/></div>
    <div class="columnav"><asp:TextBox  runat="server" id="txtHorno" CssClass="sol_campo25" name="Horno Elect." onkeypress="return ValidarNumeroEntero(event,this);"/></div>
</div>
<div class="solicitud100">
    <div id="pe_spacer"></div>
   <div class="campo_p"><asp:Label runat="server" Text="Extractor" name="Extractor"/></div>
    <div class="columnav"><asp:TextBox  runat="server" id="txtExtractor" CssClass="sol_campo25" name="Extractor" onkeypress="return ValidarNumeroEntero(event,this);"/></div>

    <div class="campo_p"><asp:Label runat="server" Text="Cubiertos" name="Cubiertos"/></div>
    <div class="columnav"><asp:TextBox  runat="server" id="txtCubiertos" CssClass="sol_campo25" name="Cubiertos" onkeypress="return ValidarNumeroEntero(event,this);"/></div>
</div>
<div class="solicitud100">
    <div id="pe_spacer"></div>
    <div class="campo_p"><asp:Label runat="server" Text="Bancos" name="Bancos"/></div>
    <div class="columnav"><asp:TextBox  runat="server" id="txtBancos" CssClass="sol_campo25" name="Bancos" onkeypress="return ValidarNumeroEntero(event,this);"/></div>

    <div class="campo_p"><asp:Label runat="server" Text="Sillas" name="Sillas"/></div>
    <div class="columnav"><asp:TextBox  runat="server" id="txtSillas" CssClass="sol_campo25" name="Sillas" onkeypress="return ValidarNumeroEntero(event,this);"/></div>
</div>
<div class="solicitud100">
    <div id="pe_spacer"></div>
   <div class="campo_p"><asp:Label runat="server" Text="Mesa" name="Mesa"/></div>
    <div class="columnav"><asp:TextBox  runat="server" id="txtxMesa" CssClass="sol_campo25" name="Mesa" onkeypress="return ValidarNumeroEntero(event,this);"/></div>

    <div class="campo_p"><asp:Label runat="server" Text="Termotanque" name="Termotanque"/></div>
    <div class="columnav"><asp:TextBox  runat="server" id="txtTermo" CssClass="sol_campo25" name="Termotanque" onkeypress="return ValidarNumeroEntero(event,this);"/></div>
</div>
<div class="solicitud100">
    <div id="pe_spacer"></div>
    <div class="campo_p"><asp:Label runat="server" Text="Split" name="Split"/></div>
    <div class="columnav"><asp:TextBox  runat="server" class="txtSplit" CssClass="sol_campo25" name="Split" onkeypress="return ValidarNumeroEntero(event,this);"/></div>
    <div class="campo_p"><asp:Label runat="server" Text="Caloventor" name="Caloventor"/></div>
    <div class="columnav"><asp:TextBox  runat="server" id="txtCaloventor" CssClass="sol_campo25" name="Caloventor" onkeypress="return ValidarNumeroEntero(event,this);"/></div>
</div>
<div class="solicitud100">
    <div id="pe_campo">Oficina</div>
    <div class="campo_p"><asp:Label runat="server" Text="Escritorio" name="Escritorio"/></div>
    <div class="columnav"><asp:TextBox  runat="server" id="txtEscritorio" CssClass="sol_campo25" name="Escritorio" onkeypress="return ValidarNumeroEntero(event,this);" /></div>
    <div class="campo_p"><asp:Label runat="server" Text="Biblioteca" name="Biblioteca"/></div>
    <div class="columnav"><asp:TextBox  runat="server" id="txtBiblio" CssClass="sol_campo25" name="Biblioteca" onkeypress="return ValidarNumeroEntero(event,this);"/></div>
</div>
<div class="solicitud100">
    <div id="pe_spacer"></div>
    <div class="campo_p"><asp:Label runat="server" Text="Sillas" name="SillasOf"/></div>
    <div class="columnav"><asp:TextBox  runat="server" id="txtSillasOf" CssClass="sol_campo25" name="SillasOf" onkeypress="return ValidarNumeroEntero(event,this);"/></div>
    <div class="campo_p"><asp:Label runat="server" Text="--"/></div>
    <div class="columnav"><asp:TextBox  runat="server" CssClass="sol_campo25" id="txt2" onkeypress="return ValidarNumeroEntero(event,this);" /></div>
</div>
<div class="solicitud100">
    <div id="pe_campo">Conexi�n</div>
    <div class="campo_p"><asp:Label runat="server" Text="Manguera" name="Manguera"/></div>
    <div class="columnav"><asp:TextBox  runat="server" id="txtManguera" CssClass="sol_campo25" name="Manguera" onkeypress="return ValidarNumeroEntero(event,this);" /></div>
    <div class="campo_p"><asp:Label runat="server" Text="Tee" name="Tee"/></div>
    <div class="columnav"><asp:TextBox  runat="server" id="txtTee" CssClass="sol_campo25"  name="Tee" onkeypress="return ValidarNumeroEntero(event,this);"/></div>
</div>
<div class="solicitud100">
    <div id="pe_spacer"></div>
    <div class="campo_p"><asp:Label runat="server" Text="Ca�o 110" name="Ca�o 110"/></div>
    <div class="columnav"><asp:TextBox  runat="server"  CssClass="sol_campo25" id="txtCanio" name="Ca�o 110" onkeypress="return ValidarNumeroEntero(event,this);"/></div>
    <div class="campo_p"><asp:Label runat="server" Text="Cble Sin. 3x4" name="Cable sin"/></div>
    <div class="columnav"><asp:TextBox  runat="server" CssClass="sol_campo25" id="txtCableSin" name="Cable sin" onkeypress="return ValidarNumeroEntero(event,this);" /></div>
</div>
<div class="solicitud100">
    <div id="pe_spacer"></div>
    <div class="campo_p"><asp:Label runat="server" Text="Codo" name="Codo"/></div>
    <div class="columnav"><asp:TextBox  runat="server" CssClass="sol_campo25" id="txtCodo" name="Codo" onkeypress="return ValidarNumeroEntero(event,this);"/></div>
    <div class="campo_p"><asp:Label runat="server" Text="Fichas" name="Fichas"/></div>
    <div class="columnav"><asp:TextBox  runat="server" CssClass="sol_campo25" id="txtFichas" name="Fichas" onkeypress="return ValidarNumeroEntero(event,this);" /></div>
</div>
<div class="solicitud100">
    <div id="pe_spacer"></div>
    <div class="campo_p"><asp:Label runat="server" Text="Curvas" name="Curvas"/></div>
    <div class="columnav"><asp:TextBox  runat="server" id="txtCurvas" CssClass="sol_campo25" name="Curvas" onkeypress="return ValidarNumeroEntero(event,this);"/></div>
    <div class="campo_p"><asp:Label runat="server" Text="Cable Masa" name="Cable Masa"/></div>
    <div class="columnav"><asp:TextBox  runat="server" id="txtCableMasa" CssClass="sol_campo25"  name="Cable Masa" onkeypress="return ValidarNumeroEntero(event,this);"/></div>
</div>
<div class="solicitud100">
    <div id="pe_campo">Adicionales</div>
    <div class="columnav_2"><asp:TextBox CssClass="sol_campo25"  runat="server" name="Adicionales" id="txtAdicional" TextMode="MultiLine" Rows="4"/></div>
</div>
</div>
<div class="solicitud100">
    <div id="pe_campo">Observacion</div>
    <div class="columnav_2"><asp:TextBox CssClass="sol_campo25"  runat="server" id="txtObservacion" TextMode="MultiLine" Rows="4"/></div>
</div>
<div id="botones"> 
    <div class="regreso_3"> 
        <asp:Button runat="server" class="btn_form" Text="GUARDAR" ID="btnGuardar" 
            OnClientClick="return ValidarObj('lblNroParte|DebeIngresar Nro. del parte~txtNroRemito|Debe ingresar Nro. Remito');"
            onclick="btnGuardar_Click"></asp:Button>
    </div>   
    <div class="regreso_3">
        <input type="button" id="btnVolver" value="VOLVER" class="btn_form" onclick="Volver();"/>
    </div> 
    <div class="regreso_3">
        <asp:Button runat="server" ID="btnImprimir" CssClass="btn_form" Text="IMPRIMIR" Visible="false"
            onclick="btnImprimir_Click"/></div>
</div>

</div>


</asp:Content>