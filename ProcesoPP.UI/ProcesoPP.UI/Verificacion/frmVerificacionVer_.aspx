﻿<%@ Page Title="" Language="C#" MasterPageFile="~/frmPrincipal.Master" AutoEventWireup="true" 
CodeBehind="frmVerificacionVer.aspx.cs" Inherits="ProcesoPP.UI.Verificacion.frmVerificacionVer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="cphContenedor" ContentPlaceHolderID="ContentPlaceHolder1" runat="server" >
<div class="tabla3" id="divMensaje" runat="server"><asp:Label ID="lblError" CssClass="error2" runat="server" Text="Error" Visible="false"></asp:Label></div>
<div id="contenido" runat="server">
<div id="apertura">Parte de Entrega Trailer.</div>
<div class="solicitud100">
    <div class="sol_tit2"><label>Empresa: </label></div>
    <div class="sol_campo50"><asp:Label runat="server" ID="lblEmpresa"></asp:Label></div>
    <div class="sol_tit2"><label>Sector</label></div>
    <div class="sol_campo50"><asp:Label runat="server" ID="lblSector"></asp:Label></div>
</div>
<div class="solicitud100">
    <div class="sol_tit2"><label>Solicitado por: </label></div>
    <div class="sol_campo50"><asp:Label runat="server" ID="lblSolicitante"></asp:Label></div>
</div>
<div class="solicitud100">
    <div class="sol_tit2"><label>Origen:</label></div>
    <div class="sol_campo50"><asp:Label runat="server" ID="lblOrigen"></asp:Label></div>
    <div class="sol_tit2"><label>Eq:</label></div>
    <div class="sol_campo30"><asp:Label runat="server" ID="lblPozoOrigen"></asp:Label></div>
    <div class="sol_tit2"><label>Destino:</label></div>
    <div class="sol_campo50"><asp:Label runat="server" ID="lblDestino"></asp:Label></div>
    <div class="sol_tit2"><label>Eq:</label></div>
    <div class="sol_campo30"><asp:Label runat="server" ID="lblPozoDestino"></asp:Label></div>
</div>
<div class="solicitud100">
    <div class="sol_tit2"><label>Condición:</label></div>
    <div class="sol_campo30"> <asp:Label runat ="server" ID="lblCondicion" /> </div>
    <div class="sol_tit2"><label>Unidad:</label></div>
    <div class="sol_campo30"><asp:Label runat="server" ID="lblUnidad"></asp:Label></div>
</div>
<div class="solicitud100">
    <div class="sol_tit2"><label>Km recorridos:</label></div>
    <div class="sol_campo50"><asp:Label runat="server" ID="lblKms"></asp:Label></div>
    <div class="sol_campo50">
        <asp:CheckBox runat ="server" ID="chkCon" Text="Conexión" TextAlign="Left" Enabled="false"/>
        <asp:CheckBox runat ="server" ID="chkDescon" Text="Desconexión" TextAlign="Left" Enabled="false"/>
    </div>
</div>
<div class="solicitud100">
     <div class="sol_tit2"><asp:Label ID="Label1" runat="server" name="NroParte" Text="Parte Nº:" /></div>
    <div class="sol_campo30"><asp:Label ID="lblNroParte" name="NroParte" runat="server"/></div>  
     <div class="sol_tit2"><asp:Label ID="Label2" runat="server" Text="Trailer:" /></div>
    <div class="sol_campo30"><asp:Label ID="lblTrailer"  CssClass="sol_campo25" runat="server"/></div>
      
     <div class="sol_tit2"><asp:Label ID="Label3" runat="server" Text="SS Nº"/></div>
    <div class="sol_campo30"><asp:Label id="lblNroSS"  runat="server"/></div>
</div>
<div class="solicitud100">
    <div class="sol_tit50"><asp:Label ID="Label4" runat="server" Text="Hs Salida:" /></div>
    <div class="sol_campo30"><asp:TextBox id="txtHsSalida" Enabled="false"  CssClass="sol_campo25" runat="server"/></div>
    
    <div class="sol_tit50"><asp:Label ID="Label5" runat="server" Text="Hs Regreso:"/></div>
    <div class="sol_campo30"><asp:TextBox id="txtHSRegreso" Enabled="false" CssClass="sol_campo25" runat="server"/></div>
    
</div>
<div runat="server" id="divVerificacion">
<div id="titulos">Detalle Exterior</div>
<div class="solicitud100">
    <div class="campo_p"><asp:Label ID="Label6" runat="server" name="Lanza" Text="Lanza"/></div>
    <div class="columnav"><asp:TextBox ID="txtLanza" CssClass="sol_campo25" Enabled="false" name="Lanza" runat="server" onkeypress="return ValidarNumeroEntero(event,this);" /></div>

    <div class="campo_p"><asp:Label ID="Label7" runat="server" name="Cajon HTA" Text="Cajón HTA"/></div>
    <div class="columnav"><asp:TextBox runat="server" CssClass="sol_campo25" Enabled="false" name="Cajon HTA" id="txtHTA" onkeypress="return ValidarNumeroEntero(event,this);"/></div>

    <div class="campo_p"><asp:Label ID="Label8" runat="server" name="Ojal" Text="Ojal"/></div>
    <div class="columnav"><asp:TextBox runat="server" CssClass="sol_campo25" Enabled="false" name="Ojal" id="txtOjal" onkeypress="return ValidarNumeroEntero(event,this);"/></div>
</div>
<div class="solicitud100">
    <div class="campo_p"><asp:Label ID="Label9" runat="server" name="Reventim" Text="Revestim."/></div>
    <div class="columnav"><asp:TextBox  runat="server" CssClass="sol_campo25" name="Reventim" Enabled="false" id="txtRev" onkeypress="return ValidarNumeroEntero(event,this);" /></div>

    <div class="campo_p"><asp:Label ID="Label10" runat="server" name="Pinza" Text="Pinza"/></div>
    <div class="columnav"><asp:TextBox  runat="server" CssClass="sol_campo25" name="Pinza" Enabled="false" id="txtPinza" onkeypress="return ValidarNumeroEntero(event,this);" /></div>

    <div class="campo_p"><asp:Label ID="Label11" runat="server" name="Escalera" Text="Escalera"/></div>
    <div class="columnav"><asp:TextBox  runat="server" CssClass="sol_campo25" name="Escalera" Enabled="false" id="txtEscalera" onkeypress="return ValidarNumeroEntero(event,this);" /></div>
</div>
<div class="solicitud100">
    <div class="campo_p"><asp:Label ID="Label12" runat="server" name="Plato" Text="Plato"/></div>
    <div class="columnav"><asp:TextBox  runat="server" CssClass="sol_campo25" name="Plato" Enabled="false" id="txtPlato" onkeypress="return ValidarNumeroEntero(event,this);" /></div>

    <div class="campo_p"><asp:Label ID="Label13" runat="server" name="Descanso" Text="Descanso"/></div>
    <div class="columnav"><asp:TextBox  runat="server" CssClass="sol_campo25" name="Descanso" Enabled="false" id="txtDescanso" onkeypress="return ValidarNumeroEntero(event,this);" /></div>

    <div class="campo_p"><asp:Label ID="Label14" runat="server" name="Barandas" Text="Barandas"/></div>
    <div class="columnav"><asp:TextBox  runat="server" CssClass="sol_campo25" name="Barandas" Enabled="false" id="txtBarandas" onkeypress="return ValidarNumeroEntero(event,this);" /></div>
</div>
<div class="solicitud100">
    <div class="campo_p"><asp:Label ID="Label15" runat="server" name="Ejes" Text="Ejes"/></div>
    <div class="columnav"><asp:TextBox  runat="server" CssClass="sol_campo25" name="Ejes" Enabled="false" id="txtEjes" onkeypress="return ValidarNumeroEntero(event,this);" /></div>

    <div class="campo_p"><asp:Label ID="Label16" runat="server" name="Soporte Esc." Text="Soporte Esc."/></div>
    <div class="columnav"><asp:TextBox  runat="server" CssClass="sol_campo25" name="Soporte Esc." Enabled="false" id="txtSopor" onkeypress="return ValidarNumeroEntero(event,this);" /></div>

   <div class="campo_p"><asp:Label ID="Label17" runat="server" name="Estacas" Text="Estacas"/></div>
    <div class="columnav"><asp:TextBox runat="server" CssClass="sol_campo25" name="Estacas" Enabled="false" id="txtEstaca" onkeypress="return ValidarNumeroEntero(event,this);" /></div>
</div>
<div class="solicitud100">
    <div class="campo_p"><asp:Label ID="Label18" runat="server" name="Elasticos" Text="Elasticos"/></div>
    <div class="columnav"><asp:TextBox runat="server" CssClass="sol_campo25" name="Elasticos" Enabled="false" id="txtElasticos" onkeypress="return ValidarNumeroEntero(event,this);" /></div>

    <div class="campo_p"><asp:Label ID="Label19" runat="server" name="Mazas" Text="Mazas"/></div>
    <div class="columnav"><asp:TextBox runat="server" CssClass="sol_campo25" name="Mazas" Enabled="false" id="txtMazas" onkeypress="return ValidarNumeroEntero(event,this);" /></div>

    <div class="campo_p"><asp:Label ID="Label20" runat="server" name="Luces Tran." Text="Luces Tran."/></div>
    <div class="columnav"><asp:TextBox runat="server" id="txtLuces" CssClass="sol_campo25" Enabled="false" name="Luces Tran." onkeypress="return ValidarNumeroEntero(event,this);"/></div>
</div>
<div id="titulos">Detalle Interior</div>

<div class="solicitud100">
    <div id="pe_campo">Dormitorio</div>
    <div class="campo_p"><asp:Label ID="Label21" runat="server" Text="Camas" name="Camas"/></div>
    <div class="columnav"><asp:TextBox  runat="server" id="txtCamas" CssClass="sol_campo25" Enabled="false" name="Camas" onkeypress="return ValidarNumeroEntero(event,this);"/></div>

    <div class="campo_p"><asp:Label ID="Label22" runat="server" Text="Colchones" name="Colchones"/></div>
    <div class="columnav"><asp:TextBox  runat="server" id="txtColchones" CssClass="sol_campo25" Enabled="false" name="Colchones" onkeypress="return ValidarNumeroEntero(event,this);"/></div>
</div>
<div class="solicitud100">
    <div id="pe_spacer"></div>
   <div class="campo_p"><asp:Label ID="Label23" runat="server" Text="Almohadas" name="Almohadas"/></div>
    <div class="columnav"><asp:TextBox  runat="server" id="txtAlmohadas" CssClass="sol_campo25" Enabled="false" name="Almohadas" onkeypress="return ValidarNumeroEntero(event,this);"/></div>

    <div class="campo_p"><asp:Label ID="Label24" runat="server" Text="Cortinas" name="Cortinas"/></div>
    <div class="columnav"><asp:TextBox  runat="server" id="txtCortinas" CssClass="sol_campo25" Enabled="false" name="Cortinas" onkeypress="return ValidarNumeroEntero(event,this);"/></div>
</div>
<div class="solicitud100">

    <div id="pe_spacer"></div>
   <div class="campo_p"><asp:Label ID="Label25" runat="server" Text="Veladores" name="Veladores"/></div>
    <div class="columnav"><asp:TextBox  runat="server" id="txtVeladores" CssClass="sol_campo25" Enabled="false" name="Veladores" onkeypress="return ValidarNumeroEntero(event,this);"/></div>

    <div class="campo_p"><asp:Label ID="Label26" runat="server" Text="Placard" name="Placard"/></div>
    <div class="columnav"><asp:TextBox  runat="server" id="txtPlacard" CssClass="sol_campo25"  Enabled="false" name="Placard" onkeypress="return ValidarNumeroEntero(event,this);"/></div>
</div>
<div class="solicitud100">
    <div id="pe_campo">Baño</div>
    <div class="campo_p"><asp:Label ID="Label27" runat="server" Text="Inodoro" name="Inodoro"/></div>
    <div class="columnav"><asp:TextBox  runat="server" id="txtInodoro" CssClass="sol_campo25" Enabled="false" name="Inodoro" onkeypress="return ValidarNumeroEntero(event,this);"/></div>

    <div class="campo_p"><asp:Label ID="Label28" runat="server" Text="Ducha" name="Ducha"/></div>
    <div class="columnav"><asp:TextBox  runat="server" id="txtDucha" CssClass="sol_campo25" Enabled="false" name="Ducha" onkeypress="return ValidarNumeroEntero(event,this);"/></div>
</div>
<div class="solicitud100">
    <div id="pe_spacer"></div>
    <div class="campo_p"><asp:Label ID="Label29" runat="server" Text="Lavatorio" name="Lavatorio"/></div>
    <div class="columnav"><asp:TextBox  runat="server" id="txtLavatorio" CssClass="sol_campo25" Enabled="false" name="Lavatorio" onkeypress="return ValidarNumeroEntero(event,this);"/></div>
    <div class="campo_p"><asp:Label ID="Label30" runat="server" Text="--"/></div>
    <div class="columnav"></div>
</div>
<div class="solicitud100">
    <div id="pe_campo">Comedor</div>
    <div class="campo_p"><asp:Label ID="Label31" runat="server" Text="Cocina" name="Cocina"/></div>
    <div class="columnav"><asp:TextBox  runat="server" id="txtCocina" CssClass="sol_campo25" Enabled="false" name="Cocina" onkeypress="return ValidarNumeroEntero(event,this);" /></div>

    <div class="campo_p"><asp:Label ID="Label32" runat="server" Text="Horno Elect." name="Horno Elect."/></div>
    <div class="columnav"><asp:TextBox  runat="server" id="txtHorno" CssClass="sol_campo25" Enabled="false" name="Horno Elect." onkeypress="return ValidarNumeroEntero(event,this);"/></div>
</div>
<div class="solicitud100">
    <div id="pe_spacer"></div>
   <div class="campo_p"><asp:Label ID="Label33" runat="server" Text="Extractor" name="Extractor"/></div>
    <div class="columnav"><asp:TextBox  runat="server" id="txtExtractor" CssClass="sol_campo25" Enabled="false" name="Extractor" onkeypress="return ValidarNumeroEntero(event,this);"/></div>

    <div class="campo_p"><asp:Label ID="Label34" runat="server" Text="Cubiertos" name="Cubiertos"/></div>
    <div class="columnav"><asp:TextBox  runat="server" id="txtCubiertos" CssClass="sol_campo25" Enabled="false" name="Cubiertos" onkeypress="return ValidarNumeroEntero(event,this);"/></div>
</div>
<div class="solicitud100">
    <div id="pe_spacer"></div>
    <div class="campo_p"><asp:Label ID="Label35" runat="server" Text="Bancos" name="Bancos"/></div>
    <div class="columnav"><asp:TextBox  runat="server" id="txtBancos" CssClass="sol_campo25" Enabled="false" name="Bancos" onkeypress="return ValidarNumeroEntero(event,this);"/></div>

    <div class="campo_p"><asp:Label ID="Label36" runat="server" Text="Sillas" name="Sillas"/></div>
    <div class="columnav"><asp:TextBox  runat="server" id="txtSillas" CssClass="sol_campo25" Enabled="false" name="Sillas" onkeypress="return ValidarNumeroEntero(event,this);"/></div>
</div>
<div class="solicitud100">
    <div id="pe_spacer"></div>
   <div class="campo_p"><asp:Label ID="Label37" runat="server" Text="Mesa" name="Mesa"/></div>
    <div class="columnav"><asp:TextBox  runat="server" id="txtxMesa" CssClass="sol_campo25" Enabled="false" name="Mesa" onkeypress="return ValidarNumeroEntero(event,this);"/></div>

    <div class="campo_p"><asp:Label ID="Label38" runat="server" Text="Termotanque" name="Termotanque"/></div>
    <div class="columnav"><asp:TextBox  runat="server" id="txtTermo" CssClass="sol_campo25" Enabled="false" name="Termotanque" onkeypress="return ValidarNumeroEntero(event,this);"/></div>
</div>
<div class="solicitud100">
    <div id="pe_spacer"></div>
    <div class="campo_p"><asp:Label ID="Label39" runat="server" Text="Split" name="Split"/></div>
    <div class="columnav"><asp:TextBox ID="TextBox1"  runat="server" class="txtSplit" Enabled="false" CssClass="sol_campo25" name="Split" onkeypress="return ValidarNumeroEntero(event,this);"/></div>
    <div class="campo_p"><asp:Label ID="Label40" runat="server" Text="Caloventor" name="Caloventor"/></div>
    <div class="columnav"><asp:TextBox  runat="server" id="txtCaloventor" CssClass="sol_campo25" Enabled="false" name="Caloventor" onkeypress="return ValidarNumeroEntero(event,this);"/></div>
</div>
<div class="solicitud100">
    <div id="pe_campo">Oficina</div>
    <div class="campo_p"><asp:Label ID="Label41" runat="server" Text="Escritorio" name="Escritorio"/></div>
    <div class="columnav"><asp:TextBox  runat="server" id="txtEscritorio" CssClass="sol_campo25" Enabled="false" name="Escritorio" onkeypress="return ValidarNumeroEntero(event,this);" /></div>
    <div class="campo_p"><asp:Label ID="Label42" runat="server" Text="Biblioteca" name="Biblioteca"/></div>
    <div class="columnav"><asp:TextBox  runat="server" id="txtBiblio" CssClass="sol_campo25" Enabled="false" name="Biblioteca" onkeypress="return ValidarNumeroEntero(event,this);"/></div>
</div>
<div class="solicitud100">
    <div id="pe_spacer"></div>
    <div class="campo_p"><asp:Label ID="Label43" runat="server" Text="Sillas" name="SillasOf"/></div>
    <div class="columnav"><asp:TextBox  runat="server" id="txtSillasOf" CssClass="sol_campo25" Enabled="false" name="SillasOf" onkeypress="return ValidarNumeroEntero(event,this);"/></div>
    <div class="campo_p"><asp:Label ID="Label44" runat="server" Text="--"/></div>
    <div class="columnav"><asp:TextBox  runat="server" CssClass="sol_campo25" id="txt2" Enabled="false" onkeypress="return ValidarNumeroEntero(event,this);" /></div>
</div>
<div class="solicitud100">
    <div id="pe_campo">Conexión</div>
    <div class="campo_p"><asp:Label ID="Label45" runat="server" Text="Manguera" name="Manguera"/></div>
    <div class="columnav"><asp:TextBox  runat="server" id="txtManguera" CssClass="sol_campo25" Enabled="false" name="Manguera" onkeypress="return ValidarNumeroEntero(event,this);" /></div>
    <div class="campo_p"><asp:Label ID="Label46" runat="server" Text="Tee" name="Tee"/></div>
    <div class="columnav"><asp:TextBox  runat="server" id="txtTee" CssClass="sol_campo25" Enabled="false" name="Tee" onkeypress="return ValidarNumeroEntero(event,this);"/></div>
</div>
<div class="solicitud100">
    <div id="pe_spacer"></div>
    <div class="campo_p"><asp:Label ID="Label47" runat="server" Text="Caño 110" name="Caño 110"/></div>
    <div class="columnav"><asp:TextBox  runat="server"  CssClass="sol_campo25" id="txtCanio" Enabled="false" name="Caño 110" onkeypress="return ValidarNumeroEntero(event,this);"/></div>
    <div class="campo_p"><asp:Label ID="Label48" runat="server" Text="Cble Sin. 3x4" name="Cable sin"/></div>
    <div class="columnav"><asp:TextBox  runat="server" CssClass="sol_campo25" id="txtCableSin" Enabled="false" name="Cable sin" onkeypress="return ValidarNumeroEntero(event,this);" /></div>
</div>
<div class="solicitud100">
    <div id="pe_spacer"></div>
    <div class="campo_p"><asp:Label ID="Label49" runat="server" Text="Codo" name="Codo"/></div>
    <div class="columnav"><asp:TextBox  runat="server" CssClass="sol_campo25" id="txtCodo" Enabled="false" name="Codo" onkeypress="return ValidarNumeroEntero(event,this);"/></div>
    <div class="campo_p"><asp:Label ID="Label50" runat="server" Text="Fichas" name="Fichas"/></div>
    <div class="columnav"><asp:TextBox  runat="server" CssClass="sol_campo25" id="txtFichas" Enabled="false" name="Fichas" onkeypress="return ValidarNumeroEntero(event,this);" /></div>
</div>
<div class="solicitud100">
    <div id="pe_spacer"></div>
    <div class="campo_p"><asp:Label runat="server" Text="Curvas" name="Curvas"/></div>
    <div class="columnav"><asp:TextBox  runat="server" id="txtCurvas" CssClass="sol_campo25" Enabled="false" name="Curvas" onkeypress="return ValidarNumeroEntero(event,this);"/></div>
    <div class="campo_p"><asp:Label runat="server" Text="Cable Masa" name="Cable Masa"/></div>
    <div class="columnav"><asp:TextBox  runat="server" id="txtCableMasa" CssClass="sol_campo25" Enabled="false" name="Cable Masa" onkeypress="return ValidarNumeroEntero(event,this);"/></div>
</div>
<div class="solicitud100">
    <div id="pe_campo">Adicionales</div>
    <div class="columnav_2"><asp:TextBox CssClass="sol_campo25"  runat="server" name="Adicionales" Enabled="false" id="txtAdicional"/></div>
</div>
</div>
<div class="solicitud100">
    <div id="pe_campo">Observacion</div>
    <div class="columnav_2"><asp:TextBox CssClass="sol_campo25"  runat="server" id="txtObservacion" TextMode="MultiLine" Rows="4"/></div>
</div>
<div id="botones"> 
    <div class="regreso_3"> </div>   
    <div class="regreso_3">
        <input type="button" id="btnVolver" value="VOLVER" class="btn_form" onclick="Volver();"/>
    </div> 
    <div class="regreso_3">
        <asp:Button runat="server" ID="btnImprimir" CssClass="btn_form" Text="IMPRIMIR" 
            onclick="btnImprimir_Click"/></div>
</div>

</div>

</asp:Content>
