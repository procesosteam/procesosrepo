﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ProcesoPP.Business;
using ProcesoPP.Model;
using System.Data;
using ProcesoPP.Services;
using System.Configuration;

namespace ProcesoPP.UI.Verificacion
{
    public partial class frmVerificacionIda : System.Web.UI.Page
    {
        #region <PROPIEDADES>

        public int _idUsuario
        {
            get { return (int)ViewState["idUsuario"]; }
            set { ViewState["idUsuario"] = value; }
        }

        private int _idSolicitud
        {
            get { return (int)ViewState["idSolicitud"]; }
            set { ViewState["idSolicitud"] = value; }
        }

        private int _idTrailer
        {
            get { return (int)ViewState["idTrailer"]; }
            set { ViewState["idTrailer"] = value; }
        }

        private int _idOTI
        {
            get { return (int)ViewState["idOTI"]; }
            set { ViewState["idOTI"] = value; }
        }

        private int _idParte
        {
            get { return (int)ViewState["idParte"]; }
            set { ViewState["idParte"] = value; }
        }

        public Permisos _Permiso
        {
            get { return (Permisos)ViewState["permiso"]; }
            set { ViewState["permiso"] = value; }
        }

        public int _ADM
        {
            get { return int.Parse(ConfigurationManager.AppSettings["ADM"].ToString()); }
        }
        #endregion

        #region <EVENTOS>

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                _idUsuario = Common.EvaluarSession(Request, this.Page);
                if (_idUsuario != 0)
                {
                    _Permiso = Common.getModulo(Request, this.Page);
                    if (_Permiso != null && _Permiso.Lectura)
                    {
                        if (!IsPostBack)
                        {
                            InitScreen();
                        }
                    }
                    else
                    {
                        Mensaje.errorMsj("NO TIENE PERMISOS A ESTE FORMULARIO", this.Page, "SIN ACCESO", "../frmPrincipal.aspx");
                    }
                }
                else
                {
                    Mensaje.errorMsj("DEBE LOGUEARSE", this.Page, "LOGIN", "../frmLogin.aspx");
                }
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                GuardarVerifica();
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }
        protected void btnImprimir_Click(object sender, EventArgs e)
        {
            Imprimir();
        }

        #endregion

        #region <METODOS>

        public override void VerifyRenderingInServerForm(Control control)
        {
        }

        private void Imprimir()
        {
            PDF.PDFCreate exportar = new ProcesoPP.UI.PDF.PDFCreate();
            exportar.exportarPDF("VERIFICACION", divVerificacion);
        }

        private void GuardarVerifica()
        {
            ParteEntrega oParteEntrega = new ParteEntrega();
            ParteEntregaBus oParteEntregaBus = new ParteEntregaBus();

            oParteEntrega.Fecha = DateTime.Now;
            oParteEntrega.HoraLlegada = DateTime.MinValue;
            oParteEntrega.HoraSalida = DateTime.MinValue;
            oParteEntrega.IdOrdenTrabajo = _idOTI;
            oParteEntrega.NroParteEntrega = lblNroParte.Text;
            oParteEntrega.Entrega = true;
            oParteEntrega.Adicionales = txtAdicional.Text;
            oParteEntrega.Conexion = chkCon.Checked;
            oParteEntrega.Desconexion = chkDescon.Checked;
            oParteEntrega.Observacion = txtObservacion.Text;
            oParteEntrega.nroRemito = txtNroRemito.Text;

            List<VerificaParte> oListCant = getVerifica(divVerificacion);
            if (_idParte == 0)
                oParteEntregaBus.ParteEntregaAdd(oParteEntrega, oListCant);
            else
            {
                oParteEntrega.IdParteEntrega = _idParte;
                oParteEntregaBus.ParteEntregaUpdate(oParteEntrega, oListCant);
            }
            Mensaje.successMsj("Parte entrega guardada con éxito.", this.Page, "Exito", "../OrdenTrabajo/frmOrdenTrabajoGestion.aspx?idModulo=" + _Permiso.oModulo.idModulo.ToString());
        }

        public List<VerificaParte> getVerifica(Control ctrl)
        {
            int idVerifica = 0;
            string name = "";
            decimal cant = 0;
            List<VerificaParte> oListCant = new List<VerificaParte>();
            List<Verifica> oListVerifica = new List<Verifica>();
            oListVerifica = (new VerificaBus()).VerificaGetAll();

            for (int i = 0; i < ctrl.Controls.Count; i++)
            {
                if (divVerificacion.Controls[i] is Label)
                {
                    Label lblDesc = (Label)divVerificacion.Controls[i];
                    name = lblDesc.Attributes["name"];
                    Verifica ver = oListVerifica.Find(v => v.Descripcion == name);
                    if (ver != null)
                        idVerifica = ver.IdVerifica;
                    else
                        idVerifica = 0;

                    if (divVerificacion.Controls[i + 2] is TextBox)
                    {
                        TextBox txtCant = (TextBox)divVerificacion.Controls[i + 2];
                        if (txtCant.Attributes["name"] == name)
                        {
                            if (txtCant.Text != string.Empty)
                                cant = decimal.Parse(txtCant.Text);
                            else
                                cant = 0;
                        }
                    }
                    if (idVerifica != 0 && cant != 0)
                    {
                        VerificaParte oVerificaParte = new VerificaParte();
                        oVerificaParte.Cantidad = cant;
                        oVerificaParte.IdVerifica = idVerifica;
                        oListCant.Add(oVerificaParte);
                    }

                }
                else if(divVerificacion.Controls[i].HasControls()){
                    getVerifica(divVerificacion.Controls[i]);
                }
            }

            return oListCant;
        }

        private void InitScreen()
        {
           _idOTI = 0;

            if (Request["idTrailer"] != null)
                _idTrailer = int.Parse(Request["idTrailer"]);
            
            if (Request["idSolicitud"] != null)
            {
                _idSolicitud = int.Parse(Request["idSolicitud"]);              
            }

            if (Request["idOTI"] != null && Request["idOTI"] != string.Empty)
            {
                _idOTI = int.Parse(Request["idOTI"]);
                CargarOTI();
            }
            if (Request["idParte"] != null 
                && Request["idParte"] != string.Empty)
            {
                _idParte = int.Parse(Request["idParte"]);
                if (_idParte != 0)
                    CargarParte();
            }
            else
            {
                _idParte = 0;
            }
            if (!_Permiso.Escritura)
            {
                btnGuardar.Visible = false;
            }
            if (_Permiso != null && _Permiso.oTipoUsuario.idTipoUsuario == _ADM)
            {
                lblRemito.Visible = true;
                txtNroRemito.Visible = true;
            }
            else
            {
                lblRemito.Visible = false;
                txtNroRemito.Visible = false;
            }
        }

        private void CargarParte()
        {
            ParteEntrega oParteEntrega = new ParteEntrega();
            ParteEntregaBus oParteEntregaBus = new ParteEntregaBus();
            oParteEntrega = oParteEntregaBus.ParteEntregaGetById(_idParte);
            lblNroParte.Text = oParteEntrega.NroParteEntrega;
            txtAdicional.Text = oParteEntrega.Adicionales;
            chkCon.Checked = oParteEntrega.Conexion;
            chkDescon.Checked = oParteEntrega.Desconexion;
            txtObservacion.Text = oParteEntrega.Observacion;
            txtNroRemito.Text = oParteEntrega.nroRemito;
            _idOTI = oParteEntrega.IdOrdenTrabajo;
            CargarOTI();
            CargarVerificacion();

        }

        private void CargarVerificacion()
        {
            VerificaBus oVerificaBus  = new VerificaBus();
            DataTable dt = oVerificaBus.VerificaParteGetByIdParte(_idParte);
           
            foreach (DataRow dr in dt.Rows)
            {
                for (int i = 0; i < divVerificacion.Controls.Count; i++)
                {
                    if (divVerificacion.Controls[i] is TextBox)
                    {
                        TextBox txtCant = (TextBox)divVerificacion.Controls[i];                                            
                        if (txtCant.Attributes["name"] == dr["descripcion"].ToString())
                        {
                            txtCant.Text = dr["cantidad"].ToString();
                        }
                    }
                }
            }            
        }

        //private void SugerirNro()
        //{
        //    ParteEntregaBus oParteEntregaBus = new ParteEntregaBus();
        //    int max = oParteEntregaBus.ParteEntregaGetAll().Count() == 0 ? 0 : int.Parse(oParteEntregaBus.ParteEntregaGetAll().Max(p => p.NroParteEntrega));
        //    lblNroParte.Text = (max + 1).ToString();
        //}

        private void CargarOTI()
        {
            Model.OrdenTrabajo oOrdenTrabajo = new Model.OrdenTrabajo();
            OrdenTrabajoBus oOrdenTrabajoBus = new OrdenTrabajoBus();
            oOrdenTrabajo = oOrdenTrabajoBus.OrdenTrabajoGetById(_idOTI);

            lblEmpresa.Text = oOrdenTrabajo.oSolicitudCliente.oSector.oEmpresa.Descripcion;
            lblSector.Text = oOrdenTrabajo.oSolicitudCliente.oSector.Descripcion;
            lblSolicitante.Text = oOrdenTrabajo.oSolicitudCliente.oSolicitante.Nombre;
            lblOrigen.Text = oOrdenTrabajo.oSolicitudCliente.oMovimiento.oLugarOrigen.Descripcion;
            lblPozoOrigen.Text = oOrdenTrabajo.oSolicitudCliente.oMovimiento.PozoOrigen;
            lblDestino.Text = oOrdenTrabajo.oSolicitudCliente.oMovimiento.oLugarDestino.Descripcion;
            lblPozoDestino.Text = oOrdenTrabajo.oSolicitudCliente.oMovimiento.PozoDestino;
            lblCondicion.Text = oOrdenTrabajo.oSolicitudCliente.oCondicion.Descripcion;
            lblKms.Text = oOrdenTrabajo.oSolicitudCliente.oMovimiento.KmsEstimados.ToString();
            
            lblNroSS.Text = oOrdenTrabajo.oSolicitudCliente.NroSolicitud.ToString();
           _idTrailer = oOrdenTrabajo.IdTrailer;

           if (_idTrailer != 0)
           {
               Trailer oTrailer = (new TrailerBus()).TrailerGetById(oOrdenTrabajo.IdTrailer);
               lblTrailer.Text = oTrailer.Descripcion;
           }
           else
               lblTrailer.Text = "-";
        }

        #endregion

    }
}
