﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ProcesoPP.Business;
using ProcesoPP.Services;
using ProcesoPP.Model;
using System.Configuration;

namespace ProcesoPP.UI
{
    public partial class frmPrincipal1 : System.Web.UI.Page
    {
        #region PROPIEDADES

        public int _idUsuario
        {
            get { return (int)ViewState["idUsuario"]; }
            set { ViewState["idUsuario"] = value; }
        }

        public int _ADM
        {
            get { return int.Parse(ConfigurationManager.AppSettings["ADM"].ToString()); }
        }

        #endregion

        #region EVENTOS

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                _idUsuario = Common.EvaluarSession(Request, this.Page);
                if (_idUsuario != 0)
                {                    
                    if (!IsPostBack)
                    {
                        MostrarCant();
                        CargarAlertaSolicitud();
                        CargarAlertaAcuerdos();
                    }
                }
                else
                {
                    Mensaje.errorMsj("DEBE LOGUEARSE", this.Page, "LOGIN", "frmLogin.aspx");
                }
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        protected void gvSolicitud_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "OTI":
                    Response.Redirect("~/SSe3/frmSSOti.aspx?idSolicitud=" + Common.encriptarURL(e.CommandArgument.ToString()) + "&idModulo=2");
                    break;
                case "Alerta":
                    Response.Redirect("~/SSe3/frmSSNueva.aspx?idSolicitud=" + e.CommandArgument + "&idModulo=3&Accion=Regreso");
                    break;
                case "Renovar":
                    Response.Redirect("~/Acuerdo/frmAcuerdo.aspx?idAcuerdo=" + e.CommandArgument + "&idModulo=6&renovar=true");
                    break;
            }
        }

        #endregion

        private void CargarAlertaSolicitud()
        {
            SolicitudClienteBus oSolicitudCliente = new SolicitudClienteBus();
            gvAlerta.DataSource = oSolicitudCliente.ReporteAlertaMovimiento();
            gvAlerta.DataBind();
        }

        private void MostrarCant()
        {
            SolicitudClienteBus oSolicitudCliente = new SolicitudClienteBus();
            lblCant.Text = oSolicitudCliente.SolicitudClienteSinOTI().Rows.Count.ToString();

            gvSolicitud.DataSource = oSolicitudCliente.SolicitudClienteSinOTI();
            gvSolicitud.DataBind();
        }


        private void CargarAlertaAcuerdos()
        {
            UsuarioBus oUsuarioBus = new UsuarioBus();
            Usuario oUser = new Usuario();
            oUser = oUsuarioBus.UsuarioGetById(_idUsuario);
            if (_ADM == oUser.oTipoUsuario.idTipoUsuario)
            {
                divAlertaAcuerdo.Visible = true;
                AcuerdoBus oAcuerdoBus = new AcuerdoBus();
                gvAcuerdos.DataSource = oAcuerdoBus.AcuerdoAlerta();
                gvAcuerdos.DataBind();
            }
            else
                divAlertaAcuerdo.Visible = false;
        }

    }
}
