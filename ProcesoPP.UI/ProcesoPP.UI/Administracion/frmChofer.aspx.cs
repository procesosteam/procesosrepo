﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ProcesoPP.Business;
using ProcesoPP.Model;
using ProcesoPP.Services;

namespace ProcesoPP.UI.Administracion
{
    public partial class frmChofer : System.Web.UI.Page
    {
        #region PROPIEDADES

        public Permisos _Permiso
        {
            get { return (Permisos)ViewState["permiso"]; }
            set { ViewState["permiso"] = value; }
        }

        public int _idUsuario
        {
            get { return (int)ViewState["idUsuario"]; }
            set { ViewState["idUsuario"] = value; }
        }

        #endregion

        #region EVENTOS

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                _idUsuario = Common.EvaluarSession(Request, this.Page);
                if (_idUsuario != 0)
                {
                    _Permiso = Common.getModulo(Request, this.Page);
                    if (_Permiso != null && _Permiso.Lectura)
                    {
                        Mensaje.ocultarDivMsj(divMensaje);
                        if (!IsPostBack)
                        {
                            CargarChofers();
                            InitScrren();
                        }
                    }
                    else
                    {
                        Mensaje.errorMsj("NO TIENE PERMISOS A ESTE FORMULARIO", this.Page, "SIN ACCESO", "../frmPrincipal.aspx");
                    }
                }
                else
                {
                    Mensaje.errorMsj("DEBE LOGUEARSE", this.Page, "LOGIN", "../frmLogin.aspx");
                }
            }
            catch (Exception ex)
            {                
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                Guardar();
                CargarChofers();
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        protected void gvChofer_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            gvChofer.EditIndex = -1;
            CargarChofers();
        }

        protected void gvChofer_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                Eliminar(gvChofer.Rows[e.RowIndex]);
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        protected void gvChofer_RowEditing(object sender, GridViewEditEventArgs e)
        {
            gvChofer.EditIndex = e.NewEditIndex;
            CargarChofers();
        }

        protected void gvChofer_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                Modificar(gvChofer.Rows[e.RowIndex]);
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        protected void gvChofer_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Common.CargarRow(e, _Permiso);  
            }
        }

        #endregion

        #region < METODOS >
        
        private void InitScrren()
        {
            if (!_Permiso.Escritura)
            {
                btnGuardar.Visible = false;
            }
        }

        private void Modificar(GridViewRow row)
        {
            int idChofer = int.Parse((row.FindControl("lblIdChofer") as Label).Text);
            string nombre = (row.FindControl("txtNombre") as TextBox).Text;
            string apellido = (row.FindControl("txtApellido") as TextBox).Text;
            string direccion = (row.FindControl("txtDireccion") as TextBox).Text;
            string telefono = (row.FindControl("txtTelefono") as TextBox).Text;
            string dni = (row.FindControl("txtDNI") as TextBox).Text;
            ChoferBus oChoferBus = new ChoferBus();
            Chofer oChofer = new Chofer();
            oChofer = oChoferBus.ChoferGetById(idChofer);
            oChofer.Nombre = nombre;
            oChofer.Apellido = apellido;
            oChofer.Direccion = direccion;
            oChofer.Telefono = telefono;
            oChofer.DNI = dni;
            if (oChoferBus.ChoferUpdate(oChofer))
            {
                Mensaje.successMsj("Chofer modificado con éxito.", this.Page, "Modificado", "");
                gvChofer.EditIndex = -1;
                CargarChofers();
            }
        }

        private void Eliminar(GridViewRow row)
        {
            int idChofer = int.Parse((row.FindControl("lblIdChofer") as Label).Text);
            ChoferBus oChoferBus = new ChoferBus();
            if (oChoferBus.ChoferDelete(idChofer))
            {
                Mensaje.successMsj("Chofer eliminado con éxito", this.Page, "Eliminado", null);
                CargarChofers();
            }
        }

        private void CargarChofers()
        {
            ChoferBus oChoferBus = new ChoferBus();
            gvChofer.DataSource = oChoferBus.ChoferGetAll();
            gvChofer.DataBind();
        }

        private void Guardar()
        {

            ChoferBus oChoferBus = new ChoferBus();
            Chofer oChofer = new Chofer();
            oChofer.Nombre = txtNombre.Text;
            oChofer.Apellido = txtApellido.Text;
            oChofer.Direccion = txtDireccion.Text;
            oChofer.DNI = txtDNI.Text;
            oChofer.Telefono = txtTelefono.Text;
            if (oChoferBus.ChoferAdd(oChofer) > 0)
            {
                Mensaje.successMsj("Chofer guardado con éxito", this.Page, "Guardado", null);
                CargarChofers();
                LimpiarControles();
            }

        }

        private void LimpiarControles()
        {
            txtNombre.Text = string.Empty;
            txtApellido.Text = string.Empty;
            txtDireccion.Text = string.Empty;
            txtDNI.Text = string.Empty;
            txtTelefono.Text = string.Empty;
        }

        #endregion

    }
}
