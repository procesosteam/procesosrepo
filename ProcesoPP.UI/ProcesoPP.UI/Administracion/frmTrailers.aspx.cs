﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ProcesoPP.Business;
using ProcesoPP.Model;
using ProcesoPP.Services;

namespace ProcesoPP.UI.Administracion
{
    public partial class frmTrailers : System.Web.UI.Page
    {
        #region PROPIEDADES

        public Permisos _Permiso
        {
            get { return (Permisos)ViewState["permiso"]; }
            set { ViewState["permiso"] = value; }
        }

        public int _idUsuario
        {
            get { return (int)ViewState["idUsuario"]; }
            set { ViewState["idUsuario"] = value; }
        }

        #endregion

        #region EVENTOS

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                _idUsuario = Common.EvaluarSession(Request, this.Page);
                if (_idUsuario != 0)
                {
                    _Permiso = Common.getModulo(Request, this.Page);
                    if (_Permiso != null && _Permiso.Lectura)
                    {
                        Mensaje.ocultarDivMsj(divMensaje);
                        if (!IsPostBack)
                        {
                            CargarTrailers();
                            CargarEspecificacion(ddlEspecificacion);
                            InitScreen();
                        }
                    }
                    else
                    {
                        Mensaje.errorMsj("NO TIENE PERMISOS A ESTE FORMULARIO", this.Page, "SIN ACCESO", "../frmPrincipal.aspx");
                    }
                }
                else
                {
                    Mensaje.errorMsj("DEBE LOGUEARSE", this.Page, "LOGIN", "../frmLogin.aspx");
                }
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                Guardar();
                CargarTrailers();
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        protected void gvTrailer_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            gvTrailer.EditIndex = -1;
            CargarTrailers();
        }

        protected void gvTrailer_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                Eliminar(gvTrailer.Rows[e.RowIndex]);
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        protected void gvTrailer_RowEditing(object sender, GridViewEditEventArgs e)
        {
            gvTrailer.EditIndex = e.NewEditIndex;
            HiddenField lblEsp = gvTrailer.Rows[e.NewEditIndex].FindControl("hfidEspecificacion") as HiddenField;
            CargarTrailers();

            DropDownList ddlEspecificacionGV = gvTrailer.Rows[e.NewEditIndex].FindControl("ddlEspecificacion") as DropDownList;
            CargarEspecificacion(ddlEspecificacionGV);
            ddlEspecificacionGV.SelectedValue = lblEsp.Value;
        }

        protected void gvTrailer_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                Modificar(gvTrailer.Rows[e.RowIndex]);
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        protected void gvTrailer_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Common.CargarRow(e, _Permiso);
            }
        } 

        #endregion

        #region METODOS

        private void InitScreen()
        {
            if (!_Permiso.Escritura)
            {
                btnGuardar.Visible = false;
            }
        }

        private void Eliminar(GridViewRow row)
        {
            int id = int.Parse((row.FindControl("lblidTrailer") as Label).Text);
            TrailerBus oTrailerBus = new TrailerBus();

            if (oTrailerBus.TrailerDelete(id))
            {
                Mensaje.successMsj("Equipo Eliminado con éxito.", this.Page, "Eliminado", "");
                CargarTrailers();
            }
        }

        private void Modificar(GridViewRow row)
        {
            int id = int.Parse((row.FindControl("lblIdTrailer") as Label).Text);
            string descripcion = (row.FindControl("txtDescripcion") as TextBox).Text;
            int idEspecificacion = int.Parse((row.FindControl("ddlEspecificacion") as DropDownList).SelectedValue);
            TrailerBus oTrailerBus = new TrailerBus();
            Trailer oTrailer = new Trailer(id, descripcion, idEspecificacion);

            if (oTrailerBus.TrailerUpdate(oTrailer))
            {
                Mensaje.successMsj("Modificado con éxito", this.Page, "Modificado", null);
                gvTrailer.EditIndex = -1;
                CargarTrailers();
            }
        }

        private void Guardar()
        {
            TrailerBus oTrailerBus = new TrailerBus();
            Trailer oTrailer = new Trailer(0, txtDescripcion.Text, int.Parse(ddlEspecificacion.SelectedValue));
            oTrailer.IdTrailer = oTrailerBus.TrailerAdd(oTrailer);

            if (oTrailer.IdTrailer > 0)
            {
                Mensaje.successMsj("Transporte guardado con éxito", this.Page, "Guardado", null);
                txtDescripcion.Text = "";
                CargarTrailers();
            }
        }

        private void CargarEspecificacion(DropDownList ddl)
        {
            EspecificacionTecnicaBus oEspecificacionTecnicaBus = new EspecificacionTecnicaBus();
            ddl.DataSource = oEspecificacionTecnicaBus.EspecificacionTecnicaGetAll();
            ddl.DataTextField = "Descripcion";
            ddl.DataValueField = "IdEspecificaciones";
            ddl.DataBind();
            ddl.Items.Insert(0, new ListItem("Seleccione Especificación", "0"));
        }

        private void CargarTrailers()
        {
            TrailerBus oTrailerBus = new TrailerBus();
            gvTrailer.DataSource = oTrailerBus.TrailerGetAll();
            gvTrailer.DataBind();
        }

        #endregion
    }
}
