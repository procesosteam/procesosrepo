﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ProcesoPP.Model;
using ProcesoPP.Business;
using ProcesoPP.Services;

namespace ProcesoPP.UI.Administracion
{
    public partial class frmMarca : System.Web.UI.Page
    {
        #region PROPIEDADES

        public Permisos _Permiso
        {
            get { return (Permisos)ViewState["permiso"]; }
            set { ViewState["permiso"] = value; }
        }

        public int _idUsuario
        {
            get { return (int)ViewState["idUsuario"]; }
            set { ViewState["idUsuario"] = value; }
        }

        #endregion

        #region EVENTOS

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                _idUsuario = Common.EvaluarSession(Request, this.Page);
                if (_idUsuario != 0)
                {
                    _Permiso = Common.getModulo(Request, this.Page);
                    if (_Permiso != null && _Permiso.Lectura)
                    {
                        Mensaje.ocultarDivMsj(divMensaje);
                        if (!IsPostBack)
                        {
                            CargarMarcas();
                            InitScrren();
                        }
                    }
                    else
                    {
                        Mensaje.errorMsj("NO TIENE PERMISOS A ESTE FORMULARIO", this.Page, "SIN ACCESO", "../frmPrincipal.aspx");
                    }
                }
                else
                {
                    Mensaje.errorMsj("DEBE LOGUEARSE", this.Page, "LOGIN", "../frmLogin.aspx");
                }
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                Guardar();
                CargarMarcas();
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        protected void gvMarca_RowEditing(object sender, GridViewEditEventArgs e)
        {
            gvMarca.EditIndex = e.NewEditIndex;
            CargarMarcas();
        }

        protected void gvMarca_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            gvMarca.EditIndex = -1;
            CargarMarcas();
        }

        protected void gvMarca_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                Modificar(gvMarca.Rows[e.RowIndex]);
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        protected void gvMarca_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                Eliminar(gvMarca.Rows[e.RowIndex]);
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        protected void gvMarca_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Common.CargarRow(e, _Permiso);
            }
        }

        #endregion

        #region Metodos

        private void InitScrren()
        {
            if (!_Permiso.Escritura)
            {
                btnGuardar.Visible = false;
            }
        }

        private void Eliminar(GridViewRow row)
        {

            int id = int.Parse((row.FindControl("lblMarcaI") as Label).Text);
            MarcaBus oMarcaBus = new MarcaBus();

            if (oMarcaBus.MarcaDelete(id))
            {
                Mensaje.successMsj("Marca Eliminada con éxito.", this.Page, "Eliminado", "");
                CargarMarcas();
            }
        }

        private void Modificar(GridViewRow row)
        {
            int id = int.Parse((row.FindControl("lblIdMarca") as Label).Text);
            string descripcion = (row.FindControl("txtDescripcion") as TextBox).Text;
            MarcaBus oMarcaBus = new MarcaBus();
            Marca oMarca = new Marca(id, descripcion);

            if (oMarcaBus.MarcaUpdate(oMarca))
            {
                Mensaje.successMsj("Modificado con Exito!", this.Page, "Modificado", null);
                gvMarca.EditIndex = -1;
                CargarMarcas();
            }
        }

        private void Guardar()
        {
            MarcaBus oMarcaBus = new MarcaBus();
            Marca oMarca = new Marca(0, txtMarca.Text);
            oMarca.IdMarca = oMarcaBus.MarcaAdd(oMarca);

            if (oMarca.IdMarca > 0)
            {
                Mensaje.successMsj("Guardado con Exito!", this.Page, "Agregado", null);
                txtMarca.Text = "";
                CargarMarcas();
            }
        }

        private void CargarMarcas()
        {
            MarcaBus oMarcaBus = new MarcaBus();
            gvMarca.DataSource = oMarcaBus.MarcaGetAll();
            gvMarca.DataBind();
        }

        #endregion
    }
}