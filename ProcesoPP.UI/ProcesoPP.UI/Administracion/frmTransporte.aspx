﻿<%@ Page Title="" Language="C#" MasterPageFile="~/frmPrincipal.Master" AutoEventWireup="true" CodeBehind="frmTransporte.aspx.cs" Inherits="ProcesoPP.UI.Administracion.frmTransporte" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server"> 
<script type="text/javascript" >
    $(document).ready(function() { SetMenu("liCargaDatos"); }); 
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="apertura">Alta | Baja | Modificación de Transporte</div>
    <div class="tabla_admin" id="divContenido" runat="server"> 
      
      <div class="label_admint"><label>Identificación: </label></div> 
        <div class="label_adminc"><asp:TextBox CssClass="sol_campoadm" runat="server" ID="txtItemVerificacion"></asp:TextBox></div> <div class="label_admint"></div><div class="label_admint"></div>
        
        <div class="label_admint"><label>Marca: </label> </div> 
        <div class="label_adminc"><asp:TextBox CssClass="sol_campoadm" runat="server" ID="txtMarca"></asp:TextBox></div> <div class="label_admint"></div>
        <div class="label_admint"></div>
        
        <div class="label_admint"><label>Modelo: </label></div> 
        <div class="label_adminc"><asp:TextBox CssClass="sol_campoadm" runat="server" ID="txtModelo"></asp:TextBox></div> <div class="label_admint"></div>
        <div class="label_admint"></div>
        
        <div class="label_admint"><label>Año: </label></div> 
        <div class="label_adminc"><asp:TextBox CssClass="sol_campoadm" runat="server" ID="txtAnio" onkeypress="return ValidarNumeroEntero(event,this);"></asp:TextBox></div> <div class="label_admint"></div><div class="label_admint"></div>
        
      
        
        <div class="label_admint"><label>Patente: </label></div> 
        <div class="label_adminc"><asp:TextBox CssClass="sol_campoadm" runat="server" ID="txtPatente"></asp:TextBox></div>  <div class="label_relleno20"></div>

        <div class="label_admintb"><asp:Button runat="server" id="btnGuardar" CssClass="btn_form" Text="Guardar"
        OnClientClick="return ValidarObj('txtDescripcion|Debe ingresar la descripción.~txtMarca|Debe ingresar la Marca.~txtModelo|Debe ingresar el Modelo.~txtAnio|Debe ingresar el anio.~txtPatente|Debe ingresar la patente del vehículo.');"
                onclick="btnGuardar_Click"/></div>        
    </div>
    <div class="tabla3" id="divMensaje" runat="server">
         <asp:Label ID="lblError" CssClass="label40t" runat="server" Text="Error" Visible="false"></asp:Label>
     </div>
     
     
     <div class="tabla_admin">
         <asp:GridView ID="gvTransporte" runat="server" AutoGenerateColumns="False" 
             DataMember="idTipoServicio" CssClass="tabla3" Width="100%"
             onrowdeleting="gvTransporte_RowDeleting" 
             onrowediting="gvTransporte_RowEditing" 
             onrowcancelingedit="gvTransporte_RowCancelingEdit" 
             onrowupdating="gvTransporte_RowUpdating"
             OnRowDataBound="gvTransporte_RowDataBound">
             <Columns>
                 <asp:TemplateField HeaderText="Id" HeaderStyle-CssClass="cel_trans20t" ItemStyle-CssClass="cel_trans20" Visible="false">                    
                     <ItemTemplate>
                         <asp:Label ID="lblidTransporte" runat="server" Text='<%# Bind("idTransporte") %>'></asp:Label>
                  
                   </ItemTemplate>
                 </asp:TemplateField>
                 <asp:TemplateField HeaderText="IDENTIFICACION" HeaderStyle-CssClass="cel_trans20t" ItemStyle-CssClass="cel_trans20">
                     <EditItemTemplate>
                         <asp:TextBox ID="txtItemVerificacion" runat="server" Text='<%# Bind("ItemVerificacion") %>'></asp:TextBox>
                     </EditItemTemplate>
                     <ItemTemplate>
                         <asp:Label ID="lblItemVerificacion" runat="server" Text='<%# Bind("ItemVerificacion") %>'></asp:Label>
                     </ItemTemplate>
                 </asp:TemplateField>
                 <asp:TemplateField HeaderText="Marca" HeaderStyle-CssClass="celda20t" ItemStyle-CssClass="celda20">
                     <EditItemTemplate>
                         <asp:TextBox ID="txtMarca" runat="server" Text='<%# Bind("Marca") %>'></asp:TextBox>
                     </EditItemTemplate>
                     <ItemTemplate>
                         <asp:Label ID="lblMarca" runat="server" Text='<%# Bind("Marca") %>'></asp:Label>
                     </ItemTemplate>
                 </asp:TemplateField>
                 <asp:TemplateField HeaderText="Modelo" HeaderStyle-CssClass="celda20t" ItemStyle-CssClass="celda20">
                     <EditItemTemplate>
                         <asp:TextBox ID="txtModelo" runat="server" Text='<%# Bind("Modelo") %>'></asp:TextBox>
                     </EditItemTemplate>
                     <ItemTemplate>
                         <asp:Label ID="lblModelo" runat="server" Text='<%# Bind("Modelo") %>'></asp:Label>
                     </ItemTemplate>
                 </asp:TemplateField>
                 <asp:TemplateField HeaderText="Año" HeaderStyle-CssClass="cel_trans15t" ItemStyle-CssClass="cel_trans15">
                     <EditItemTemplate>
                         <asp:TextBox ID="txtAnio" runat="server" Text='<%# Bind("Anio") %>'></asp:TextBox>
                     </EditItemTemplate>
                     <ItemTemplate>
                         <asp:Label ID="lblAnio" runat="server" Text='<%# Bind("Anio") %>'></asp:Label>
                    
                     </ItemTemplate>
                 </asp:TemplateField>
                   <asp:TemplateField HeaderText="Patente" HeaderStyle-CssClass="cel_trans15t" ItemStyle-CssClass="cel_trans15">
                     <EditItemTemplate>
                         <asp:TextBox ID="txtPatente" runat="server" Text='<%# Bind("Patente") %>'></asp:TextBox>
                     </EditItemTemplate>
                     <ItemTemplate>
                         <asp:Label ID="lblPatente" runat="server" Text='<%# Bind("Patente") %>'></asp:Label>
                     </ItemTemplate>
                 </asp:TemplateField>
                 <asp:TemplateField ShowHeader="False" HeaderStyle-CssClass="celda10t" ItemStyle-CssClass="celda10">
                     <EditItemTemplate>
                         <asp:LinkButton ID="btnEditar" runat="server" CausesValidation="True" CssClass="boton40"
                             CommandName="Update" Text="Actualizar"></asp:LinkButton>
                         <asp:LinkButton ID="btnEliminar" runat="server" CausesValidation="False" CssClass="boton40"
                             CommandName="Cancel" Text="Cancelar"></asp:LinkButton>                  
                     </EditItemTemplate>
                     <ItemTemplate>
                         <asp:LinkButton ID="btnEditar" runat="server" CausesValidation="False" CssClass="boton40"
                             CommandName="Edit" Text="Editar" ></asp:LinkButton>      
                         <asp:LinkButton ID="btnEliminar" runat="server" CausesValidation="False" CssClass="boton40"
                             CommandName="Delete" Text="Eliminar" OnClientClick="return Eliminar('el Transporte');"></asp:LinkButton> 
                     </ItemTemplate>
                 </asp:TemplateField>
             </Columns>
         </asp:GridView>
     </div>
</asp:Content>
