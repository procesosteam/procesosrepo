﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ProcesoPP.Model;
using ProcesoPP.Business;
using ProcesoPP.Services;

namespace ProcesoPP.UI.Administracion
{
    public partial class frmCodificacion : System.Web.UI.Page
    {
        #region PROPIEDADES

        public Permisos _Permiso
        {
            get { return (Permisos)ViewState["permiso"]; }
            set { ViewState["permiso"] = value; }
        }

        public int _idUsuario
        {
            get { return (int)ViewState["idUsuario"]; }
            set { ViewState["idUsuario"] = value; }
        }

        #endregion

        #region EVENTOS

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                _idUsuario = Common.EvaluarSession(Request, this.Page);
                if (_idUsuario != 0)
                {
                    _Permiso = Common.getModulo(Request, this.Page);
                    if (_Permiso != null && _Permiso.Lectura)
                    {
                        Mensaje.ocultarDivMsj(divMensaje);
                        if (!IsPostBack)
                        {
                            CargarCodificaciones();
                            InitScrren();
                        }
                    }
                    else
                    {
                        Mensaje.errorMsj("NO TIENE PERMISOS A ESTE FORMULARIO", this.Page, "SIN ACCESO", "../frmPrincipal.aspx");
                    }
                }
                else
                {
                    Mensaje.errorMsj("DEBE LOGUEARSE", this.Page, "LOGIN", "../frmLogin.aspx");
                }
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                Guardar();
                CargarCodificaciones();
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        protected void gvCodificacion_RowEditing(object sender, GridViewEditEventArgs e)
        {
            gvCodificacion.EditIndex = e.NewEditIndex;
            CargarCodificaciones();
        }

        protected void gvCodificacion_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            gvCodificacion.EditIndex = -1;
            CargarCodificaciones();
        }

        protected void gvCodificacion_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                Modificar(gvCodificacion.Rows[e.RowIndex]);
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        protected void gvCodificacion_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                Eliminar(gvCodificacion.Rows[e.RowIndex]);
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        protected void gvCodificacion_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Common.CargarRow(e, _Permiso);
            }
        }

        #endregion

        #region Metodos

        private void InitScrren()
        {
            if (!_Permiso.Escritura)
            {
                btnGuardar.Visible = false;
            }
        }

        private void Eliminar(GridViewRow row)
        {

            int id = int.Parse((row.FindControl("lblCodificacionI") as Label).Text);
            CodificacionBus oCodificacionBus = new CodificacionBus();

            if (oCodificacionBus.CodificacionDelete(id))
            {
                Mensaje.successMsj("Codificacion Eliminada con éxito.", this.Page, "Eliminado", "");
                CargarCodificaciones();
            }
        }

        private void Modificar(GridViewRow row)
        {
            int id = int.Parse((row.FindControl("lblIdCodificacion") as Label).Text);
            string descripcion = (row.FindControl("txtDescripcion") as TextBox).Text;
            string codigo = (row.FindControl("txtCodigo") as TextBox).Text;
            bool equipo = (row.FindControl("txtequipo") as CheckBox).Checked;
            CodificacionBus oCodificacionBus = new CodificacionBus();
            Codificacion oCodificacion = new Codificacion(id, descripcion,codigo,equipo);

            if (oCodificacionBus.CodificacionUpdate(oCodificacion))
            {
                Mensaje.successMsj("Modificado con Exito!", this.Page, "Modificado", null);
                gvCodificacion.EditIndex = -1;
                CargarCodificaciones();
            }
        }

        private void Guardar()
        {
            CodificacionBus oCodificacionBus = new CodificacionBus();
            Codificacion oCodificacion = new Codificacion(0, txtCodificacion.Text,txtCodigo.Text,reSi.Checked);
            oCodificacion.IdCodificacion = oCodificacionBus.CodificacionAdd(oCodificacion);

            if (oCodificacion.IdCodificacion > 0)
            {
                Mensaje.successMsj("Guardado con Exito!", this.Page, "Agregado", null);
                txtCodificacion.Text = "";
                txtCodigo.Text = "";
                CargarCodificaciones();
            }
        }

        private void CargarCodificaciones()
        {
            CodificacionBus oCodificacionBus = new CodificacionBus();
            gvCodificacion.DataSource = oCodificacionBus.CodificacionGetAll();
            gvCodificacion.DataBind();
        }

        #endregion
    }
}