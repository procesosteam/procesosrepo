﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ProcesoPP.Business;
using ProcesoPP.Model;
using ProcesoPP.Services;

namespace ProcesoPP.UI.Administracion
{
    public partial class frmPermisos : System.Web.UI.Page
    {
        #region PROPIEDADES

        public Permisos _Permiso
        {
            get { return (Permisos)ViewState["permiso"]; }
            set { ViewState["permiso"] = value; }
        }

        public int _idUsuario
        {
            get { return (int)ViewState["idUsuario"]; }
            set { ViewState["idUsuario"] = value; }
        }

        #endregion

        #region EVENTOS

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                _idUsuario = Common.EvaluarSession(Request, this.Page);
                if (_idUsuario != 0)
                {
                    _Permiso = Common.getModulo(Request, this.Page);
                    if (_Permiso != null && _Permiso.Lectura)
                    {
                        if (!IsPostBack)
                        {
                            InetScreen();
                        }
                    }
                    else
                    {
                        Mensaje.errorMsj("NO TIENE PERMISOS A ESTE FORMULARIO", this.Page, "SIN ACCESO", "../frmPrincipal.aspx");
                    }
                }
                else
                {
                    Mensaje.errorMsj("DEBE LOGUEARSE", this.Page, "LOGIN", "../frmLogin.aspx");
                }
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            GuardarPermiso();
        }


        protected void gvPermisos_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                Eliminar(gvPermisos.Rows[e.RowIndex]);
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        protected void gvPermisos_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Common.CargarRow(e, _Permiso);
            }
        } 

        #endregion

        #region METODOS

        private void Eliminar(GridViewRow row)
        {
            int idPermisos = int.Parse((row.FindControl("lblIdPermiso") as Label).Text);
            PermisosBus oPermisosBus = new PermisosBus();
            if (oPermisosBus.PermisosDelete(idPermisos))
            {
                Mensaje.successMsj("Permisos eliminado con éxito", this.Page, "Eliminado", null);
                CargarPermisos();
            }
        }

        private void GuardarPermiso()
        {
            Permisos oPermisos = new Permisos();
            PermisosBus oPermisosBus = new PermisosBus();            
            oPermisos.oModulo = new Modulo();
            oPermisos.oTipoUsuario = new TipoUsuario();

            oPermisos.oTipoUsuario.idTipoUsuario = int.Parse(ddlTipoUsuario.SelectedValue);
            oPermisos.oModulo.idModulo = int.Parse(ddlModulo.SelectedValue);
            oPermisos.Lectura = chkLectura.Checked;
            oPermisos.Escritura = chkEscritura.Checked;
            if (!oPermisosBus.verificarPermiso(oPermisos))
            {
                if (oPermisosBus.PermisosAdd(oPermisos) > 0)
                {
                    Mensaje.successMsj("Permiso guardado con éxito.", this.Page, "EXITO", "");
                    CargarPermisos();
                }
            }
            else
            {
                Mensaje.errorMsj("Permiso ya existe.", this.Page, "ERROR", "");
            }
        }

        private void CargarPermisos()
        {
            PermisosBus oPermisosBus = new PermisosBus();
            gvPermisos.DataSource = oPermisosBus.PermisosGetAll();
            gvPermisos.DataBind();
        }

        private void InetScreen()
        {
            PermisosBus oPermisosBus = new PermisosBus();
            oPermisosBus.CargarModulos(ref ddlModulo, "Seleccione...");
            oPermisosBus.CargarTipoUsuarios(ref ddlTipoUsuario, "Seleccione...");

            CargarPermisos();

            if (!_Permiso.Escritura)
            {
                btnGuardar.Visible = false;
            }
        }

        #endregion
    }
}
