﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ProcesoPP.Model;
using ProcesoPP.Business;
using ProcesoPP.Services;

namespace ProcesoPP.UI.Administracion
{
    public partial class frmAcreditacionDeHaberes : System.Web.UI.Page
    {
        #region PROPIEDADES

        public Permisos _Permiso
        {
            get { return (Permisos)ViewState["permiso"]; }
            set { ViewState["permiso"] = value; }
        }

        public int _idUsuario
        {
            get { return (int)ViewState["idUsuario"]; }
            set { ViewState["idUsuario"] = value; }
        }

        #endregion

        #region EVENTOS

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                _idUsuario = Common.EvaluarSession(Request, this.Page);
                if (_idUsuario != 0)
                {
                    _Permiso = Common.getModulo(Request, this.Page);
                    if (_Permiso != null && _Permiso.Lectura)
                    {
                        Mensaje.ocultarDivMsj(divMensaje);
                        if (!IsPostBack)
                        {
                            Cargarbancoes();
                            InitScrren();
                        }
                    }
                    else
                    {
                        Mensaje.errorMsj("NO TIENE PERMISOS A ESTE FORMULARIO", this.Page, "SIN ACCESO", "../frmPrincipal.aspx");
                    }
                }
                else
                {
                    Mensaje.errorMsj("DEBE LOGUEARSE", this.Page, "LOGIN", "../frmLogin.aspx");
                }
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                Guardar();
                Cargarbancoes();
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        protected void gvbanco_RowEditing(object sender, GridViewEditEventArgs e)
        {
            gvbanco.EditIndex = e.NewEditIndex;
            Cargarbancoes();
        }

        protected void gvbanco_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            gvbanco.EditIndex = -1;
            Cargarbancoes();
        }

        protected void gvbanco_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                Modificar(gvbanco.Rows[e.RowIndex]);
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        protected void gvbanco_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                Eliminar(gvbanco.Rows[e.RowIndex]);
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        protected void gvbanco_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Common.CargarRow(e, _Permiso);
            }
        }

        #endregion

        #region Metodos

        private void InitScrren()
        {
            if (!_Permiso.Escritura)
            {
                btnGuardar.Visible = false;
            }
        }

        private void Eliminar(GridViewRow row)
        {
            int id = int.Parse((row.FindControl("lblbancoI") as Label).Text);
            BancoBus oBancoBus = new BancoBus();

            if (oBancoBus.BancoDelete(id))
            {
                Mensaje.successMsj("Entidad eliminada con éxito.", this.Page, "Eliminado", "");
                Cargarbancoes();
            }
        }

        private void Modificar(GridViewRow row)
        {
            int id = int.Parse((row.FindControl("lblIdbanco") as Label).Text);
            string descripcion = (row.FindControl("txtDescripcion") as TextBox).Text;
            BancoBus oBancoBus = new BancoBus();
            Banco obanco = new Banco(id, descripcion);

            if (oBancoBus.BancoUpdate(obanco))
            {
                Mensaje.successMsj("Modificado con Exito!", this.Page, "Modificado", null);
                gvbanco.EditIndex = -1;
                Cargarbancoes();
            }
        }

        private void Guardar()
        {
            BancoBus oBancoBus = new BancoBus();
            Banco obanco = new Banco(0, txtbanco.Text);
            obanco.IdBanco = oBancoBus.BancoAdd(obanco);

            if (obanco.IdBanco > 0)
            {
                Mensaje.successMsj("Guardado con Exito!", this.Page, "Agregado", null);
                txtbanco.Text = "";
                Cargarbancoes();
            }
        }

        private void Cargarbancoes()
        {
            BancoBus oBancoBus = new BancoBus();
            gvbanco.DataSource = oBancoBus.BancoGetAll();
            gvbanco.DataBind();
        }

        #endregion


    }
}
