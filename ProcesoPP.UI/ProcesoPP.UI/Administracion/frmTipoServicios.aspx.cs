﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ProcesoPP.Model;
using ProcesoPP.Business;
using ProcesoPP.Services;
using System.Text;
using System.Web.Services;
using AjaxControlToolkit;

namespace ProcesoPP.UI.Administracion
{
    public partial class frmTipoServicios : System.Web.UI.Page
    {
        #region PROPIEDADES

        public Permisos _Permiso
        {
            get { return (Permisos)ViewState["permiso"]; }
            set { ViewState["permiso"] = value; }
        }

        public int _idUsuario
        {
            get { return (int)ViewState["idUsuario"]; }
            set { ViewState["idUsuario"] = value; }
        }

        public int _idTipoServicio
        {
            get { return (int)ViewState["idTipoServicio"]; }
            set { ViewState["idTipoServicio"] = value; }
        }

        #endregion

        #region EVENTOS
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                 _idUsuario = Common.EvaluarSession(Request, this.Page);
                 if (_idUsuario != 0)
                 {
                     _Permiso = Common.getModulo(Request, this.Page);
                     if (_Permiso != null && _Permiso.Lectura)
                     {
                         if (!IsPostBack)
                         {                             
                             InitScreen();
                         }

                     }
                     else
                     {
                         Mensaje.errorMsj("NO TIENE PERMISOS A ESTE FORMULARIO", this.Page, "SIN ACCESO", "../frmPrincipal.aspx");
                     }
                 }
                 else
                 {
                     Mensaje.errorMsj("DEBE LOGUEARSE", this.Page, "LOGIN", "../frmLogin.aspx");
                 }
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        protected void gvTipoServicio_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            gvTipoServicio.EditIndex = -1;
            CargarTipoServicios();
        }

        protected void gvTipoServicio_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                Eliminar(gvTipoServicio.Rows[e.RowIndex]);
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        protected void gvTipoServicio_RowEditing(object sender, GridViewEditEventArgs e)
        {
            try
            {                
                MostrarTipoServ(e.NewEditIndex);
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        /*
        private void SelectCheckBox(CheckBoxList chkEspecificacionGV, List<EspecificacionTecnica> lblEsp)
        {
            foreach (ListItem item in chkEspecificacionGV.Items)
            {
                EspecificacionTecnica esp = lblEsp.Find(e=> e.IdEspecificaciones.ToString() == item.Value);
                if (esp != null)
                {
                    item.Selected = true;
                }
            }
        }
        */
      
        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                Guardar();
                CargarTipoServicios();
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        protected void gvTipoServicio_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Common.CargarRow(e, _Permiso);
            }
        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                CargarTipoServicios();
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        protected void btnCancelar_Click(object sender, EventArgs e)
        {
            LimpiarControles();            
            cambiarHidden("0");
        }


        protected void gvEspCuadrilla_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            TipoServicio oTS = (new TipoServicioBus()).TipoServicioGetById(_idTipoServicio);

            switch (e.CommandName)
            {
                case "addEsp":
                    addEspecificacion(gvEspCuadrilla.FooterRow, true, false, false, false, false);
                    CargarEspecificaciones(gvEspCuadrilla, false, true, false, false, false);
                    checkList(gvEspCuadrilla, oTS.oEspecificacion);
                    upEsp.Update();
                    
                    break;
                case "addEQ":
                    addEspecificacion(gvEspEquipo.FooterRow, false, true, false, false, false);
                    CargarEspecificaciones(gvEspEquipo, true, false, false, false, false);
                    checkList(gvEspEquipo, oTS.oEspecificacion);
                    upEQ.Update();
                    break;
                case "addPer":
                    addEspecificacion(gvPersona.FooterRow, false, false, true, false, false);
                    CargarEspecificaciones(gvPersona, false, false, true, false, false);
                    checkList(gvPersona, oTS.oEspecificacion);
                    upPersonal.Update();
                    break;
                case "addTras":
                    addEspecificacion(gvTralado.FooterRow, false, false, false, true, false);
                    CargarEspecificaciones(gvTralado, false, false, false, true, false);
                    checkList(gvTralado, oTS.oEspecificacion);
                    upTraslado.Update();
                    break;
                case "addMat":
                    addEspecificacion(gvMaterial.FooterRow, false, false, false, false, true);
                    CargarEspecificaciones(gvMaterial, false, false, false, false, true);
                    upMaterial.Update();
                    break;
                default:
                    break;
            }
        }
        
        #endregion

        #region METODOS

        [WebMethod]
        public static CascadingDropDownNameValue[] GetEmpresa(string knownCategoryValues, string category)
        {
            EmpresaBus oEmpresaBus = new EmpresaBus();
            List<CascadingDropDownNameValue> empresa = oEmpresaBus.GetDataEmpresaInteras();
            return empresa.ToArray();
        }

        [WebMethod]
        public static CascadingDropDownNameValue[] GetSector(string knownCategoryValues, string category)
        {
            string idEmpresa = CascadingDropDown.ParseKnownCategoryValuesString(knownCategoryValues)["idEmpresa"];
            SectorBus oSectorBus = new SectorBus();
            List<CascadingDropDownNameValue> sectores = oSectorBus.GetDataSector(int.Parse(idEmpresa));
            return sectores.ToArray();
        }

        private void addEspecificacion(GridViewRow row, bool cuadrilla, bool equipo, bool persona, bool traslado, bool material)
        {

            TextBox txtEsp = row.FindControl("txtDescripcion") as TextBox;
            EspecificacionTecnicaBus oEspBus = new EspecificacionTecnicaBus();
            EspecificacionTecnica Oet = new EspecificacionTecnica();
            if (!txtEsp.Text.Equals(string.Empty))
            {
                Oet.Descripcion = txtEsp.Text;
                Oet.RequiereCuadrilla = cuadrilla;
                Oet.RequiereEquipo = equipo;
                Oet.RequierePersonal = persona;
                Oet.RequiereTraslado = traslado;
                Oet.RequiereMaterial = material;
                oEspBus.EspecificacionTecnicaAdd(Oet);
                cambiarHidden("1");
            }
        }


        private void cambiarHidden(string val)
        {
            hdidLastTab.Value = val;
            string hiddenField = hdidLastTab.Value;
            StringBuilder js = new StringBuilder();
            js.Append("<script type='text/javascript'>");
            js.Append("var previuslySelectedTab = ");
            js.Append(hiddenField);
            js.Append("</script>");
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "acttab", js.ToString());

        }

        private void MostrarTipoServ(int rowIndex)
        {
            GridViewRow row = gvTipoServicio.Rows[rowIndex];
            Label lblIdTipoServicio = row.FindControl("lblTipoServicioI") as Label;
            _idTipoServicio = int.Parse(lblIdTipoServicio.Text);

            TipoServicio oTS = (new TipoServicioBus()).TipoServicioGetById(_idTipoServicio);
            txtDescripcion.Text = oTS.Descripcion;
            txtCodTango.Text = oTS.CodTango;
            txtAbreviacion.Text = oTS.Abreviacion;
            chkMateriales.Checked = oTS.RequiereMateriales;
            if (oTS.RequiereMateriales)
            {
                chkMateriales.Text = "Si";
                divMaterial.Attributes.Add("style", "display:block");
                checkList(gvMaterial, oTS.oEspecificacion);
            }
            chkPersonal.Checked = oTS.RequierePersonal;
            if (oTS.RequierePersonal)
            {
                chkPersonal.Text = "Si";
                cddSector.SelectedValue = oTS.idSector.ToString();
                cddEmpresa.SelectedValue = (new SectorBus()).SectorGetById(oTS.idSector).IdEmpresa.ToString();
                divSector.Attributes.Add("style", "display:block");
                checkList(gvPersona, oTS.oEspecificacion);
            }
            chkMasEquipo.Checked = oTS.MasEquipo;
            if (oTS.MasEquipo)
            {
                chkMasEquipo.Text = "Si";
            }
            chkTranslado.Checked = oTS.RequiereTraslado;
            chkKms.Checked = oTS.RequiereKms;
            chkTaco.Checked = oTS.RequiereKmsTaco;
            if (oTS.RequiereKms)
            {
                chkKms.Text = "Si";
            }
            if (oTS.RequiereKmsTaco)
            {
                chkTaco.Text = "Si";
            }
            if (oTS.RequiereTraslado)
            {
                chkTranslado.Text = "Si";
                divKms.Attributes.Add("style", "display:block");
                checkList(gvTralado, oTS.oEspecificacion);
            }
            chkCuadrilla.Checked = oTS.RequiereCuadrilla;
            if (oTS.RequiereCuadrilla)
            {
                chkCuadrilla.Text = "Si";
                divEsp.Attributes.Add("style", "display:block");
                checkList(gvEspCuadrilla, oTS.oEspecificacion);
            }
            
            chkReqEquipo.Checked = oTS.RequiereEquipo;
            if (oTS.RequiereEquipo)
            {
                chkReqEquipo.Text = "Si";
                divEspEquipo.Attributes.Add("style", "display:block");
                checkList(gvEspEquipo, oTS.oEspecificacion);
            }
            
            chkChofer.Checked = oTS.RequiereChofer;
            if (oTS.RequiereChofer)
            {
                chkChofer.Text = "Si";
            }

            cambiarHidden("1");
        }

        private void checkList(GridView gv, List<EspecificacionTecnica> list)
        {
            foreach (GridViewRow row in gv.Rows)
            {
                CheckBox chk = row.FindControl("chkEspecificacion") as CheckBox;
                Label lblEspecificacion = row.FindControl("lblEspecificacion") as Label;
                if (list.Exists(e => e.IdEspecificaciones == int.Parse(lblEspecificacion.Text)))                
                    chk.Checked = true;
                
                else
                    chk.Checked = false;
            }
        }

        private void InitScreen()
        {
            _idTipoServicio = 0;
            if (!_Permiso.Escritura)
            {
                btnGuardar.Visible = false;
            }
            CargarEspecificaciones(gvEspCuadrilla, false, true, false, false, false);
            CargarEspecificaciones(gvEspEquipo, true, false, false, false, false);
            CargarEspecificaciones(gvPersona, false, false, true, false, false);
            CargarEspecificaciones(gvTralado, false, false, false, true, false);
            CargarEspecificaciones(gvMaterial, false, false, false, false, true);
            CargarTipoServicios();
            //CargarSectoresByEmpresaInterna(ddlSector);

        }

        private void Eliminar(GridViewRow row)
        {
            int id = int.Parse((row.FindControl("lblTipoServicioI") as Label).Text);
            TipoServicioBus oTipoServicioBus = new TipoServicioBus();

            if (oTipoServicioBus.TipoServicioDelete(id))
            {
                Mensaje.successMsj("Tipo servicio eliminado con éxito", this.Page, "Eliminado", null);
                CargarTipoServicios();
            }
        }

        private void Guardar()
        {
            TipoServicioBus oTipoServicioBus = new TipoServicioBus();
            TipoServicio oTipoServicio = new TipoServicio();
            oTipoServicio.Descripcion = txtDescripcion.Text;
            oTipoServicio.CodTango = txtCodTango.Text;
            
            oTipoServicio.Abreviacion = txtAbreviacion.Text;
            oTipoServicio.RequierePersonal = chkPersonal.Checked;
            if (chkPersonal.Checked)
            {
                oTipoServicio.oEspecificacion = getId(gvPersona, oTipoServicio.oEspecificacion);    
            }
            oTipoServicio.RequiereTraslado = chkTranslado.Checked;
            if (chkTranslado.Checked)
            {
                oTipoServicio.oEspecificacion = getId(gvTralado, oTipoServicio.oEspecificacion);
            }
            oTipoServicio.RequiereKms = chkKms.Checked;            
            oTipoServicio.RequiereKmsTaco = chkTaco.Checked;
            oTipoServicio.RequiereChofer = chkChofer.Checked;
            oTipoServicio.RequiereMateriales = chkMateriales.Checked;
            if (chkMateriales.Checked)
            {
                oTipoServicio.oEspecificacion = getId(gvMaterial, oTipoServicio.oEspecificacion);
            }
            oTipoServicio.RequiereCuadrilla = chkCuadrilla.Checked;
            if (chkCuadrilla.Checked)
            {
                oTipoServicio.oEspecificacion = getId(gvEspCuadrilla, oTipoServicio.oEspecificacion);
            }
            oTipoServicio.MasEquipo = chkMasEquipo.Checked;
            oTipoServicio.RequiereEquipo = chkReqEquipo.Checked;
            if (chkReqEquipo.Checked)
            {
                oTipoServicio.oEspecificacion = getId(gvEspEquipo, oTipoServicio.oEspecificacion);
            }
            if (chkPersonal.Checked)
            {
                oTipoServicio.idSector = ddlSector.SelectedValue.ToString() == string.Empty ? 0 : int.Parse(ddlSector.SelectedValue);
            }
            if (_idTipoServicio == 0)
            {
                oTipoServicio.IdTipoServicio = oTipoServicioBus.TipoServicioAdd(oTipoServicio);                
            }
            else
            {
                oTipoServicio.IdTipoServicio = _idTipoServicio;
                oTipoServicioBus.TipoServicioUpdate(oTipoServicio);                
            }

            if (oTipoServicio.IdTipoServicio > 0)
            {
                Mensaje.successMsj("Guardado con éxito", this.Page, "Guardado", null);
                LimpiarControles();
                CargarTipoServicios();
            }
        }

        private void LimpiarControles()
        {
            _idTipoServicio = 0;
            txtDescripcion.Text = string.Empty;
            txtAbreviacion.Text = string.Empty;
            txtCodTango.Text = string.Empty;
            chkPersonal.Checked = false;
            chkPersonal.Text = "No";
            chkKms.Checked = false;
            chkKms.Text = "No";
            chkMateriales.Checked = false;
            chkMateriales.Text = "No";
            chkCuadrilla.Checked = false;
            chkCuadrilla.Text = "No";
            chkMasEquipo.Checked = false;
            chkMasEquipo.Text = "No";
            chkTranslado.Checked = false;
            chkReqEquipo.Checked = false;
            chkReqEquipo.Text = "No";
            divSector.Attributes.Add("style", "display:none");
            divEsp.Attributes.Add("style", "display:none");
            divEspEquipo.Attributes.Add("style", "display:none");
            divSector.Attributes.Add("style", "display:none");
            divKms.Attributes.Add("style", "display:none");
            divMaterial.Attributes.Add("style", "display:none");
            
        }

        private List<EspecificacionTecnica> getId(GridView chkList, List<EspecificacionTecnica> oList)
        {            
            foreach (GridViewRow row in chkList.Rows)
            {
                CheckBox oCHk = row.FindControl("chkEspecificacion") as CheckBox;
                if (oCHk.Checked)
                {
                    Label lblEspecificacion = row.FindControl("lblEspecificacion") as Label;
                    Label lblDescripcion = row.FindControl("lblDescripcion") as Label;
                    EspecificacionTecnica tsEsp = new EspecificacionTecnica();
                    tsEsp.IdEspecificaciones = int.Parse(lblEspecificacion.Text);
                    tsEsp.Descripcion = lblDescripcion.Text;
                    oList.Add(tsEsp);
                }
            }

            return oList;
        }

        private void CargarSectoresByEmpresaInterna(DropDownList ddl)
        {
            SectorBus oSectorBus = new SectorBus();
            List<Sector> oListSectores = oSectorBus.SectorGetAllByIdEmpresaInterna();            
            ddl.DataSource = oListSectores;
            ddl.DataBind();
            ddl.Items.Insert(0, new ListItem("N/A", "0"));
        }

        private void CargarTipoServicios()
        {
            TipoServicioBus oTipoServicioBus = new TipoServicioBus();
            List<TipoServicio> oList = oTipoServicioBus.TipoServicioGetByFilter(txtBusqueda.Text);
            foreach (TipoServicio ts in oList)
            {
                if(ts.oEspecificacion.Count == 0)
                    ts.oEspecificacion.Add(new EspecificacionTecnica(0, "No Aplica", false, false, false));
            }
            
            gvTipoServicio.DataSource = oList;
            gvTipoServicio.DataBind();

            cambiarHidden("0");
        }
                
        private void CargarEspecificaciones(GridView grid, bool eq, bool cuadri, bool pers, bool tras, bool mat) 
        {
            EspecificacionTecnicaBus oEspecificacionTecnicaBus = new EspecificacionTecnicaBus();
            List<EspecificacionTecnica> oList = oEspecificacionTecnicaBus.EspecificacionTecnicaGet(eq, cuadri, pers, tras, mat);
            if (oList.Count == 0)
            {
                EspecificacionTecnica oET = new EspecificacionTecnica();
                oList.Add(oET);

            }
            grid.DataSource = oList;
            grid.DataBind();
            
        }

        
        #endregion

    }
}
