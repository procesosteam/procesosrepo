﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ProcesoPP.Business;
using ProcesoPP.Model;
using ProcesoPP.Services;

namespace ProcesoPP.UI.Administracion
{
    public partial class frmTransporte : System.Web.UI.Page
    {
        #region PROPIEDADES

        public Permisos _Permiso
        {
            get { return (Permisos)ViewState["permiso"]; }
            set { ViewState["permiso"] = value; }
        }

        public int _idUsuario
        {
            get { return (int)ViewState["idUsuario"]; }
            set { ViewState["idUsuario"] = value; }
        }

        #endregion

        #region EVENTOS

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                _idUsuario = Common.EvaluarSession(Request, this.Page);
                if (_idUsuario != 0)
                {
                    _Permiso = Common.getModulo(Request, this.Page);
                    if (_Permiso != null && _Permiso.Lectura)
                    {
                        Mensaje.ocultarDivMsj(divMensaje);
                        if (!IsPostBack)
                        {
                            CargarTransportes();
                            InitScreen();
                        }
                    }
                    else
                    {
                        Mensaje.errorMsj("NO TIENE PERMISOS A ESTE FORMULARIO", this.Page, "SIN ACCESO", "../frmPrincipal.aspx");
                    }
                }
                else
                {
                    Mensaje.errorMsj("DEBE LOGUEARSE", this.Page, "LOGIN", "../frmLogin.aspx");
                }
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        protected void gvTransporte_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                Eliminar(gvTransporte.Rows[e.RowIndex]);
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        protected void gvTransporte_RowEditing(object sender, GridViewEditEventArgs e)
        {
            gvTransporte.EditIndex = e.NewEditIndex;
            CargarTransportes();
        }

        protected void gvTransporte_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                Modificar(gvTransporte.Rows[e.RowIndex]);
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        protected void gvTransporte_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            gvTransporte.EditIndex = -1;
            CargarTransportes();
        }


        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                Guardar();
                CargarTransportes();
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message,this.Page, "Error", null);
            }
        }

        protected void gvTransporte_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Common.CargarRow(e, _Permiso);
            }
        } 

        #endregion
        
        #region METODOS

        private void InitScreen()
        {
            if (!_Permiso.Escritura)
            {
                btnGuardar.Visible = false;
            }
        }

        private void Modificar(GridViewRow row)
        {
            int idTransporte = int.Parse((row.FindControl("lblidTransporte") as Label).Text);
            string descripcion = null;
            string marca = (row.FindControl("txtMarca") as TextBox).Text;
            string modelo = (row.FindControl("txtModelo") as TextBox).Text;
            string anio = (row.FindControl("txtAnio") as TextBox).Text;
            string itemVerificacion = (row.FindControl("txtItemVerificacion") as TextBox).Text;
            string patente = (row.FindControl("txtPatente") as TextBox).Text;

            TransporteBus oTransporteBus = new TransporteBus();
            Transporte oTransporte = new Transporte();
            oTransporte = oTransporteBus.TransporteGetById(idTransporte);
            oTransporte.Descripcion = descripcion;
            oTransporte.Marca = marca;
            oTransporte.Modelo = modelo;
            oTransporte.Anio = int.Parse(anio);
            oTransporte.ItemVerificacion = itemVerificacion;
            oTransporte.Patente = patente;

            if (oTransporteBus.TransporteUpdate(oTransporte))
            {
                Mensaje.successMsj("Transporte Modificado con éxito.", this.Page, "Modificado", "");
                gvTransporte.EditIndex = -1;
                CargarTransportes();
            }
        }

        private void Eliminar(GridViewRow row)
        {
            int idTransporte = int.Parse((row.FindControl("lblidTransporte") as Label).Text);
            TransporteBus oTransporteBus = new TransporteBus();
            if (oTransporteBus.TransporteDelete(idTransporte))
            {
                Mensaje.successMsj("Transporte eliminado con éxito", this.Page, "Eliminado", null);
                CargarTransportes();
            }
        }

        private void CargarTransportes()
        {
            TransporteBus oTransporteBus = new TransporteBus();
            gvTransporte.DataSource = oTransporteBus.TransporteGetAll();
            gvTransporte.DataBind();
        }

        private void Guardar()
        {

            TransporteBus oTransporteBus = new TransporteBus();
            Transporte oTransporte = new Transporte();
            oTransporte.Descripcion = null;
            oTransporte.Marca = txtMarca.Text;
            oTransporte.Modelo = txtModelo.Text;
            oTransporte.Anio = int.Parse(txtAnio.Text);
            oTransporte.ItemVerificacion = txtItemVerificacion.Text;
            oTransporte.Patente = txtPatente.Text;

            if (oTransporteBus.TransporteAdd(oTransporte) > 0)
            {
                Mensaje.successMsj("Transporte Guardado con éxito!", this.Page, "Guardado", null);
                CargarTransportes();
                Common.LimpiarControles(divContenido);
            }
        }

        #endregion
    }
}
