﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ProcesoPP.Business;
using ProcesoPP.Model;
using ProcesoPP.Services;
using System.Text;
using System.Data;

namespace ProcesoPP.UI.Administracion
{
    public partial class frmEspecificacionTecnica : System.Web.UI.Page
    {
        #region PROPIEDADES

        public Permisos _Permiso
        {
            get { return (Permisos)ViewState["permiso"]; }
            set { ViewState["permiso"] = value; }
        }

        public int _idUsuario
        {
            get { return (int)ViewState["idUsuario"]; }
            set { ViewState["idUsuario"] = value; }
        }

        public int _idEspecificacion
        {
            get { return (int)ViewState["idEspecificacion"]; }
            set { ViewState["idEspecificacion"] = value; }
        }
        public DataTable _listEquipos
        {
            get { return (DataTable)ViewState["listVehiculo"]; }
            set { ViewState["listVehiculo"] = value; }
        }

        #endregion

        #region EVENTOS

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                _idUsuario = Common.EvaluarSession(Request, this.Page);
                if (_idUsuario != 0)
                {
                    _Permiso = Common.getModulo(Request, this.Page);
                    if (_Permiso != null && _Permiso.Lectura)
                    {
                        if (!IsPostBack)
                        {                            
                            InitScren();
                        }
                    }
                    else
                    {
                        cambiarHidden("0");
                        Mensaje.errorMsj("NO TIENE PERMISOS A ESTE FORMULARIO", this.Page, "SIN ACCESO", "../frmPrincipal.aspx");
                    }
                }
                else
                {
                    cambiarHidden("0");
                    Mensaje.errorMsj("DEBE LOGUEARSE", this.Page, "LOGIN", "../frmLogin.aspx");
                }
            }
            catch (Exception ex)
            {
                cambiarHidden("0");
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                CargarEspecificacionTecnicas();
            }
            catch (Exception ex)
            {
                cambiarHidden("0");
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                Guardar();
                CargarEspecificacionTecnicas();
            }
            catch (Exception ex)
            {
                cambiarHidden("0");
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        protected void gvEspecificacion_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            gvEspecificacion.EditIndex = -1;
            CargarEspecificacionTecnicas();
        }

        protected void gvEspecificacion_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                Eliminar(gvEspecificacion.Rows[e.RowIndex]);
            }
            catch (Exception ex)
            {
                cambiarHidden("0");
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        protected void gvEspecificacion_RowEditing(object sender, GridViewEditEventArgs e)
        {
            MostrarEspecificacion(e.NewEditIndex);
        }

        protected void gvEspecificacion_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                Modificar(gvEspecificacion.Rows[e.RowIndex]);
            }
            catch (Exception ex)
            {
                cambiarHidden("0");
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        protected void gvEspecificacion_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Common.CargarRow(e, _Permiso);
            }
        }
        
        protected void btnCancelar_Click(object sender, EventArgs e)
        {
            LimpiarControles();            
        }

        #endregion

        #region METODOS

        private void LimpiarControles()
        {
            _idEspecificacion = 0;
            txtBusqueda.Text = string.Empty;
            txtDescripcion.Text = string.Empty;
            txtFiltroEq.Text = string.Empty;
            chkCuadrilla.Checked = false;
            chkCuadrilla.Text = "No";
            chkEquipo.Checked = false;
            chkEquipo.Text = "No";
            chkPropio.Checked = false;
            chkPropio.Text = "No";
            chkCuadrillas.ClearSelection();
            chkEquiposCli.ClearSelection();
            chkEquiposPropios.ClearSelection();
            divEquiposPropios.Attributes.Add("style", "display:none");
            divEqClientes.Attributes.Add("style", "display:none");
            divCuadrilla.Attributes.Add("style", "display:none");
            divPropio.Attributes.Add("style", "display:none");

            cambiarHidden("0");
        }

        private void InitScren()
        {
            if (!_Permiso.Escritura)
            {
                btnGuardar.Visible = false;
            }
            _idEspecificacion = 0;
            CargarEspecificacionTecnicas();
            CargarEquipoPropios();
            CargarEquipoCliente();
            CargarCuadrilla();
            cambiarHidden("0");
        }

        private void CargarEquipoPropios()
        {
            VehiculoBus oVehiculoBus = new VehiculoBus();
            _listEquipos = oVehiculoBus.VehiculoGetEquipos();
            chkEquiposPropios.DataSource = _listEquipos;
            chkEquiposPropios.DataTextField = "codificacion";
            chkEquiposPropios.DataValueField = "idVehiculo";
            chkEquiposPropios.DataBind();
        }

        private void CargarEquipoCliente()
        {
            EquiposBus oVehiculoBus = new EquiposBus();
            chkEquiposCli.DataSource = oVehiculoBus.EquiposGetAll();
            chkEquiposCli.DataTextField = "Descripcion";
            chkEquiposCli.DataValueField = "idEquipo";
            chkEquiposCli.DataBind();
        }

        private void CargarCuadrilla()
        {
            CuadrillaBus oCuadrillaBus = new CuadrillaBus();
            chkCuadrillas.DataSource = oCuadrillaBus.CuadrillaGetAllBaja();
            chkCuadrillas.DataTextField = "Nombre";
            chkCuadrillas.DataValueField = "idCuadrilla";
            
            divCuadrilla.Attributes.Add("style", "display:none");
            divPropio.Attributes.Add("style", "display:none");
            divEquiposPropios.Attributes.Add("style", "display:none");
            divEqClientes.Attributes.Add("style", "display:none");

            chkCuadrillas.DataBind();
        }

        private void cambiarHidden(string val)
        {
            hdidLastTab.Value = val;
            string hiddenField = hdidLastTab.Value;
            StringBuilder js = new StringBuilder();
            js.Append("<script type='text/javascript'>");
            js.Append("var previuslySelectedTab = ");
            js.Append(hiddenField);
            js.Append("</script>");
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "acttab", js.ToString());

        }

        private void MostrarEspecificacion(int rowIndex)
        {
            GridViewRow row = gvEspecificacion.Rows[rowIndex];
            Label lblEspecificacionI = row.FindControl("lblEspecificacionI") as Label;
            _idEspecificacion = int.Parse(lblEspecificacionI.Text);

            EspecificacionTecnica oET = (new EspecificacionTecnicaBus()).EspecificacionTecnicaGetById(_idEspecificacion);
            txtDescripcion.Text = oET.Descripcion;
            chkCuadrilla.Checked = oET.RequiereCuadrilla;
            if (oET.RequiereCuadrilla)
            {
                chkCuadrilla.Text = "Si";
                divCuadrilla.Attributes.Add("style", "display:block");
                foreach (ListItem item in chkCuadrillas.Items)
                {
                    if (oET.oEspDetalle.Exists(e=> e.IdCuadrilla == int.Parse(item.Value)))
                    {
                        item.Selected = true;
                    }
                }
            }
            chkEquipo.Checked = oET.RequiereEquipo;
            if (oET.RequiereEquipo)
            {
                chkEquipo.Text = "Si";
                divPropio.Attributes.Add("style", "display:block");
                divNoPropio.Attributes.Add("style", "display:block");

                chkPropio.Checked = oET.EquipoPropio;
                chkNoPropio.Checked = !oET.EquipoPropio;
                if (oET.EquipoPropio)
                {
                    chkPropio.Text = "Si";
                    chkNoPropio.Text = "No";
                    divEquiposPropios.Attributes.Add("style", "display:block");
                    divEqClientes.Attributes.Add("style", "display:none");
                    foreach (ListItem item in chkEquiposPropios.Items)
                    {
                        if (oET.oEspDetalle.Exists(e => e.IdEquipoPropio == int.Parse(item.Value)))
                        {
                            item.Selected = true;
                        }
                    }
                }
                else
                {
                    chkPropio.Text = "No";
                    chkNoPropio.Text = "Si";
                    divEqClientes.Attributes.Add("style", "display:block");
                    divEquiposPropios.Attributes.Add("style", "display:none");
                    foreach (ListItem item in chkEquiposCli.Items)
                    {
                        if (oET.oEspDetalle.Exists(e => e.IdEquipoCliente == int.Parse(item.Value)))
                        {
                            item.Selected = true;
                        }
                    }
                }
            }
            cambiarHidden("1");
        }


        private void Eliminar(GridViewRow row)
        {
            int id = int.Parse((row.FindControl("lblEspecificacionI") as Label).Text);
            EspecificacionTecnicaBus oEspecificacionTecnicaBus = new EspecificacionTecnicaBus();

            if (oEspecificacionTecnicaBus.EspecificacionTecnicaDelete(id))
            {
                Mensaje.successMsj("Especificación Eliminada con éxito.", this.Page, "Eliminado", "");
                CargarEspecificacionTecnicas();
            }
        }

        private void Modificar(GridViewRow row)
        {
            int id = int.Parse((row.FindControl("lblIdEspecificacion") as Label).Text);
            string descripcion = (row.FindControl("txtDescripcion") as TextBox).Text;
            EspecificacionTecnicaBus oEspecificacionTecnicaBus = new EspecificacionTecnicaBus();
            EspecificacionTecnica oEspecificacionTecnica = new EspecificacionTecnica();
            oEspecificacionTecnica.IdEspecificaciones = id;
            oEspecificacionTecnica.Descripcion = descripcion;

            if (oEspecificacionTecnicaBus.EspecificacionTecnicaUpdate(oEspecificacionTecnica))
            {
                Mensaje.successMsj("Modificado con éxito", this.Page, "Modificado", null);
                gvEspecificacion.EditIndex = -1;
                CargarEspecificacionTecnicas();
            }
        }

        private void Guardar()
        {
            EspecificacionTecnicaBus oEspecificacionTecnicaBus = new EspecificacionTecnicaBus();
            EspecificacionTecnica oEspecificacionTecnica = new EspecificacionTecnica();
            oEspecificacionTecnica.IdEspecificaciones = 0;
            
            if (_idEspecificacion>0)
            {
                oEspecificacionTecnica = oEspecificacionTecnicaBus.EspecificacionTecnicaGetById(_idEspecificacion);
            }
            oEspecificacionTecnica.Descripcion = txtDescripcion.Text;
            oEspecificacionTecnica.RequiereCuadrilla = chkCuadrilla.Checked;
            oEspecificacionTecnica.RequiereEquipo = chkEquipo.Checked;
            oEspecificacionTecnica.EquipoPropio = chkPropio.Checked;
            oEspecificacionTecnica.oEspDetalle = CargarDetalle(oEspecificacionTecnica.oEspDetalle);
            if (_idEspecificacion==0)
            {
                oEspecificacionTecnica.IdEspecificaciones = oEspecificacionTecnicaBus.EspecificacionTecnicaAdd(oEspecificacionTecnica);                
            }
            else
            {
                oEspecificacionTecnicaBus.EspecificacionTecnicaUpdate(oEspecificacionTecnica);
            }


            if (oEspecificacionTecnica.IdEspecificaciones > 0)
            {
                Mensaje.successMsj("Especificacion guardada con éxito", this.Page, "Guardado", null);
                txtDescripcion.Text = "";
                CargarEspecificacionTecnicas();
                LimpiarControles();
            }
        }

        private List<EspecificacionDetalle> CargarDetalle(List<EspecificacionDetalle> list)
        {
            if (chkCuadrilla.Checked)
            {
                foreach (ListItem item in chkCuadrillas.Items)
                {
                    if (item.Selected)
                    {
                        EspecificacionDetalle oED = new EspecificacionDetalle();
                        oED.IdCuadrilla = int.Parse(item.Value);
                        list.Add(oED);
                    }
                }
            }
            if (chkPropio.Checked)
            {
                foreach (ListItem item in chkEquiposPropios.Items)
                {
                    if (item.Selected)
                    {
                        EspecificacionDetalle oED = new EspecificacionDetalle();
                        oED.IdEquipoPropio = int.Parse(item.Value);
                        list.Add(oED);
                    }
                }
            }
            else
            {
                foreach (ListItem item in chkEquiposCli.Items)
                {
                    if (item.Selected)
                    {
                        EspecificacionDetalle oED = new EspecificacionDetalle();
                        oED.IdEquipoCliente = int.Parse(item.Value);
                        list.Add(oED);
                    }
                }
            }

            return list;
        }

        private void CargarEspecificacionTecnicas()
        {
            EspecificacionTecnicaBus oEspecificacionTecnicaBus = new EspecificacionTecnicaBus();
            gvEspecificacion.DataSource = oEspecificacionTecnicaBus.EspecificacionTecnicaGetByFilter(txtBusqueda.Text);
            gvEspecificacion.DataBind();

            cambiarHidden("0");
        }

        #endregion

    }
}
