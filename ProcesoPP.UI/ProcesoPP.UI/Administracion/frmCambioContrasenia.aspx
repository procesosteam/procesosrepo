﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="frmCambioContrasenia.aspx.cs" Inherits="ProcesoPP.UI.Administracion.frmCambioContrasenia" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>CAMBIO CONTRASEÑA</title>
    <link href="../Css/estilos.css" rel="stylesheet" type="text/css"/>
    <link href="../Css/messi.min.css" rel="stylesheet" type="text/css" />
    <link href="../Css/messi.css" rel="stylesheet" type="text/css" />
    <link href="../Css/estilosPopUp.css" rel="stylesheet" type="text/css" />
    
    <script src="../Js/jquery-1.10.1.min.js" type="text/javascript"></script>
    <script src="../Js/messi.min.js" type="text/javascript"></script>
    <script src="../Js/messi.js" type="text/javascript"></script>
    <script src="../Js/Validadores.js" type="text/javascript"></script>
    <script src="../Js/Funciones.js" type="text/javascript"></script>
    <script src="../Js/popUp.js" type="text/javascript"></script>
    <script type="text/javascript">
        function irLogin() {
            cerrarPopUp();
            window.location.href = '../frmLogin.aspx';
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
  <div id="contenido">
        <div id="apertura">CAMBIO CONTRASEÑA</div>     
         <div class="label_relleno20"></div>        
        <div class="label_admint">
            <label>Usuario:</label>
        </div>
        <div class="label_adminc">
            <asp:Label runat="server" CssClass="sol_campoadm" ID="lblUser" /></div>
        <div class="label_relleno20"></div>
        <div class="label_relleno20"></div>        
        <div class="label_admint">
            <label>Contraseña Actual:</label>
        </div>
        <div class="label_adminc">
            <asp:TextBox runat="server" CssClass="sol_campoadm" ID="txtActual"  TextMode="Password"></asp:TextBox></div>
        <div class="label_relleno20"></div>
        <div class="label_relleno20"></div>        
        <div class="label_admint">
            <label>Contraseña Nueva:</label>
        </div>
        <div class="label_adminc">
            <asp:TextBox runat="server" CssClass="sol_campoadm" ID="txtNueva" TextMode="Password"></asp:TextBox></div>
        <div class="label_relleno20"></div> 
        <div class="label_relleno20"></div>        
        <div class="label_admint">
            <label>Repita Contraseña Nueva:</label>
        </div>
        <div class="label_adminc">
            <asp:TextBox runat="server" CssClass="sol_campoadm" ID="txtRepetida" TextMode="Password"></asp:TextBox></div>
        <div class="label_relleno20"></div> 
        <div class="label_relleno20"></div>
        <div class="label_adminc">
             <asp:Button runat="server" ID="btnGuardar" CssClass="btn_form" Text="Guardar nueva Contraseña" 
               OnClientClick="return ValidarObj('txtActual|Debe ingresar la clave Actual.~txtRepetida|Debe ingresar la Clave repetida.');"
                OnClick="btnGuardar_Click" /></div>
         <div class="label_relleno20"></div> 
         <div class="label_relleno20"></div> 
    </div>
    <div id="capaPopUp"></div>
    <div id="popUpDiv">
        <div id="capaContent">        
        <div style="width: 90%; height:100px; padding-top: 5%; padding-left: 10%;	padding-bottom: 5%; height:75%; overflow:auto;">
            <div class="label_relleno20"></div>        
            <div class="label_admint">
                <label>Ingrese Nombre Usuario:</label>
            </div>
            <div class="label_adminc">
                <asp:TextBox runat="server" CssClass="sol_campoadm" ID="txtNombreUser"></asp:TextBox></div>
            <div class="label_relleno20"></div> 
        </div>
        <div id="botones" style="width: 95%; height:10%;">
            <div class="regreso_3">
                <input type="button" onclick="irLogin();" title="Cancelar" id="btnCerrar" class="btn_form"
                    value="CANCELAR" />
            </div>
            <div class="regreso_3">
                <asp:Button runat="server" ToolTip="Buscar Nombre Usuario" ID="btnBuscar"                
                    CssClass="btn_form" Text="BUSCAR" onclick="btnBuscar_Click" />
            </div>
        </div>
        </div>
    </div>
    <asp:HiddenField runat="server" ID="hfIdUsuario" Value="0" />
    </form>
</body>
</html>
