﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ProcesoPP.Business;
using ProcesoPP.Model;
using ProcesoPP.Services;

namespace ProcesoPP.UI.Administracion
{
    public partial class frmLugarKms : System.Web.UI.Page
    {
        #region PROPIEDADES

        public Permisos _Permiso
        {
            get { return (Permisos)ViewState["permiso"]; }
            set { ViewState["permiso"] = value; }
        }

        public int _idUsuario
        {
            get { return (int)ViewState["idUsuario"]; }
            set { ViewState["idUsuario"] = value; }
        }

        #endregion

        #region EVENTOS

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                _idUsuario = Common.EvaluarSession(Request, this.Page);
                if (_idUsuario != 0)
                {
                    _Permiso = Common.getModulo(Request, this.Page);
                    if (_Permiso != null && _Permiso.Lectura)
                    {
                        if (!IsPostBack)
                        {
                            InitScreen();
                        }
                    }
                    else
                    {
                        Mensaje.errorMsj("NO TIENE PERMISOS A ESTE FORMULARIO", this.Page, "SIN ACCESO", "../frmPrincipal.aspx");
                    }
                }
                else
                {
                    Mensaje.errorMsj("DEBE LOGUEARSE", this.Page, "LOGIN", "../frmLogin.aspx");
                }
             }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                Guardar(new Lugar(int.Parse(ddlLugarDesde.SelectedItem.Value), ddlLugarDesde.SelectedItem.Text),
                        new Lugar(int.Parse(ddlLugarHasta.SelectedItem.Value), ddlLugarHasta.SelectedItem.Text),
                        decimal.Parse(txtKms.Text),
                        bool.Parse(rbKmsEst.SelectedValue));
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                CargarKilometros();
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        protected void gvKilometros_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            gvKilometros.EditIndex = -1;
            CargarKilometros();           
        }

        protected void gvKilometros_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            Eliminar((gvKilometros.Rows[e.RowIndex].FindControl("lblidKmsI") as Label).Text);
            CargarKilometros();
        }

        protected void gvKilometros_RowEditing(object sender, GridViewEditEventArgs e)
        {
            gvKilometros.EditIndex = e.NewEditIndex;
            CargarKilometros();

            string idLugarDesde = (gvKilometros.Rows[e.NewEditIndex].FindControl("lblidLugarDesde") as Label).Text;
            DropDownList ddldesde = gvKilometros.Rows[e.NewEditIndex].FindControl("ddlDesde") as DropDownList;
            CargarLugares(ddldesde);
            ddldesde.SelectedValue = idLugarDesde;

            string idLugarHasta = (gvKilometros.Rows[e.NewEditIndex].FindControl("lblidLugarHasta") as Label).Text;
            DropDownList ddlHasta = gvKilometros.Rows[e.NewEditIndex].FindControl("ddlHasta") as DropDownList;
            CargarLugares(ddlHasta);
            ddlHasta.SelectedValue = idLugarHasta;

        }

        protected void gvKilometros_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                Modificar(gvKilometros.Rows[e.RowIndex]);
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        protected void gvKilometros_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Common.CargarRow(e, _Permiso);
                RadioButtonList rbl = e.Row.FindControl("rbKmsEstGV") as RadioButtonList;
                if(rbl != null)
                {
                    rbl.SelectedValue = ((Kilometros)(e.Row.DataItem)).KmsEstablecidos.ToString();
                }
            }

        } 
        
        #endregion

        #region METODOS

        private void Modificar(GridViewRow row)
        {          

            Eliminar((row.FindControl("lblIdKms") as Label).Text);

            DropDownList ddlDesde = row.FindControl("ddlDesde") as DropDownList;
            DropDownList ddlHasta = row.FindControl("ddlHasta") as DropDownList;
            TextBox txtKmsGV = row.FindControl("txtKmsGV") as TextBox;
            RadioButtonList rbKmsEstGV = row.FindControl("rbKmsEstGV") as RadioButtonList;
            gvKilometros.EditIndex = -1;

            Guardar(new Lugar(int.Parse(ddlDesde.SelectedItem.Value), ddlDesde.SelectedItem.Text),
                    new Lugar(int.Parse(ddlHasta.SelectedItem.Value), ddlHasta.SelectedItem.Text),
                    decimal.Parse(txtKmsGV.Text),
                    bool.Parse(rbKmsEstGV.SelectedValue)
                    );                       
        }

        private void Eliminar(string idKm)
        {
            int idKms = int.Parse(idKm);
            KilometrosBus oKilometrosBus = new KilometrosBus();
            oKilometrosBus.KilometrosDelete(idKms);
        } 

        private void Guardar(Lugar idLugarDesde, Lugar idLugarHasta, decimal kms, bool kmsEstablecidos)
        {
            KilometrosBus oKilometrosBus = new KilometrosBus();
            Kilometros oKilometros = new Kilometros();
            oKilometros.oLugarDesde = idLugarDesde;
            oKilometros.oLugarHasta = idLugarHasta;
            oKilometros.Kms = kms;
            oKilometros.KmsEstablecidos = kmsEstablecidos;
                                                
            oKilometros.idKms = oKilometrosBus.KilometrosAdd(oKilometros);

            if (oKilometros.idKms > 0)
            {
                ddlLugarDesde.SelectedIndex = 0;
                ddlLugarHasta.SelectedIndex = 0;
                txtKms.Text = string.Empty;
                Mensaje.successMsj("Lugar guardado con éxito", this.Page, "Guardado", null);
                CargarKilometros();
            }
        }



        private void CargarKilometros()
        {
            KilometrosBus oKilometrosBus = new KilometrosBus();
            gvKilometros.DataSource = oKilometrosBus.KilometrosGetByFilter(ddlDesdeFiltro.SelectedValue, ddlHastaFiltro.SelectedValue);
            gvKilometros.DataBind();
        }

        private void InitScreen()
        {
            CargarLugares(ddlLugarDesde);
            CargarLugares(ddlLugarHasta);
            CargarLugares(ddlDesdeFiltro);
            CargarLugares(ddlHastaFiltro);
            CargarKilometros();
            if (!_Permiso.Escritura)
            {
                btnGuardar.Visible = false;
            }
        }

        private void CargarLugares(DropDownList ddl)
        {
            LugarBus oLugarBus = new LugarBus();
            oLugarBus.CargarLugar(ref ddl);
            ddl.Items.Insert(0, new ListItem("Seleccione...", "0"));
        }

        #endregion
    }
}
