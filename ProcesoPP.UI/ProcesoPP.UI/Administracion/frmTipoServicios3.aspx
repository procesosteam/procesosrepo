﻿<%@ Page Title="" Language="C#" MaintainScrollPositionOnPostback="true" MasterPageFile="~/frmPrincipal.Master" AutoEventWireup="true" CodeBehind="frmTipoServicios.aspx.cs" Inherits="ProcesoPP.UI.Administracion.frmTipoServicios" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server"></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="apertura">Alta | Baja | Modificación de Tipo Servicios</div>
    <div class="tabla_admin" style="height:auto;">
        <div class="label_admint">    
            <label>Tipo de Servicio: </label>  </div> 
        <div class="label_adminc">      
            <asp:TextBox ID="txtDescripcion" CssClass="sol_campoadm" runat="server" ></asp:TextBox></div>
            <div class="label_adminc"></div>
        <div class="label_admint2">      
            <label>Especificación Técnica</label></div>
        <div class="label_esp" style="overflow:scroll; width:78%;">      
            <asp:CheckBoxList runat="server" RepeatColumns="2" ID="ddlEspecificacion" 
                TextAlign="Right" onclick="return Tildar(this);" ></asp:CheckBoxList></div>
        <div class="label_adminc"></div>
        <div class="label_adminc"></div>
        <div class="label_admint">
            <asp:Button ID="btnGuardar" runat="server" Text="Guardar"  CssClass="btn_form"
                onclick="btnGuardar_Click" 
                OnClientClick="return ValidarObj('txtDescripcion|Ingrese una descripción.');" />
        </div>
    </div>
    
    
    <div class="tabla_admin" id="divMensaje" runat="server">
         <asp:Label ID="lblError" CssClass="label40t" runat="server" Text="Error" Visible="false"></asp:Label>
     </div>
    <div>
         <asp:GridView ID="gvTipoServicio" runat="server" AutoGenerateColumns="False" 
             DataMember="idTipoServicio" CssClass="tabla3"
             onrowcancelingedit="gvTipoServicio_RowCancelingEdit" 
             onrowediting="gvTipoServicio_RowEditing" 
             onrowupdating="gvTipoServicio_RowUpdating" 
             onrowdeleting="gvTipoServicio_RowDeleting">
             <Columns>
                 <asp:TemplateField HeaderText="Id TipoServicio" HeaderStyle-CssClass="celda_tipo5" ItemStyle-CssClass="celda_tipo5" Visible="false">
                     <EditItemTemplate>
                         <asp:Label ID="lblIdTipoServicio" runat="server" Text='<%# Bind("idTipoServicio") %>'></asp:Label>
                     </EditItemTemplate>
                     <ItemTemplate>
                         <asp:Label ID="lblTipoServicioI" runat="server" Text='<%# Bind("idTipoServicio") %>'></asp:Label>
                     </ItemTemplate>
                 </asp:TemplateField>
                 <asp:TemplateField HeaderText="Descripcion" HeaderStyle-CssClass="celda_tiposerv" ItemStyle-CssClass="celda_tiposerv2">
                     <EditItemTemplate>
                         <asp:TextBox ID="txtDescripcion" runat="server" Text='<%# Bind("Descripcion") %>'></asp:TextBox>
                     </EditItemTemplate>
                     <ItemTemplate>
                         <asp:Label ID="lblDescripcion" runat="server" Text='<%# Bind("Descripcion") %>'></asp:Label>
                     </ItemTemplate>
                 </asp:TemplateField>
                 <asp:TemplateField HeaderText="Esp. Técnica" HeaderStyle-CssClass="celda_tiposerv" ItemStyle-CssClass="celda_tiposerv2">
                     <EditItemTemplate>
                         <asp:CheckBoxList ID="ddlEspecificacion" runat="server" TextAlign="Left" RepeatLayout="Table" onclick="return Tildar(this);"></asp:CheckBoxList>
                     </EditItemTemplate>
                     <ItemTemplate>
                        <asp:HiddenField runat="server" ID="hfidEspecificacion" Value="0"/>
                        <asp:ListBox ID="lblEspecificacion" runat="server" Enabled="false" DataSource='<%# Bind("oEspecificacion") %>' DataTextField="Descripcion" ></asp:ListBox>
                     </ItemTemplate>
                 </asp:TemplateField>
                 <asp:TemplateField ShowHeader="False" HeaderStyle-CssClass="celda20t" ItemStyle-CssClass="celda20">
                     <EditItemTemplate>
                         <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="True" CssClass="boton40"
                             CommandName="Update" Text="Actualizar"></asp:LinkButton>
                         <asp:LinkButton ID="LinkButton2" runat="server" CausesValidation="False" CssClass="boton40"
                             CommandName="Cancel" Text="Cancelar"></asp:LinkButton>
                     </EditItemTemplate>
                     <ItemTemplate>
                         <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="False" CssClass="boton40"
                             CommandName="Edit" Text="Editar" ></asp:LinkButton>
                         <asp:LinkButton ID="LinkButton2" runat="server" CausesValidation="False" CssClass="boton40"
                             CommandName="Delete" Text="Eliminar" OnClientClick="return Eliminar('el TipoServicio');"></asp:LinkButton>
                     </ItemTemplate>
                 </asp:TemplateField>
             </Columns>
         </asp:GridView>
     </div>
</asp:Content>
