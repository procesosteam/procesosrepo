﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="frmLugarKms.aspx.cs" MasterPageFile="~/frmPrincipal.Master" MaintainScrollPositionOnPostback="true" Inherits="ProcesoPP.UI.Administracion.frmLugarKms" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server"> 
<link rel="stylesheet" href="../Css/jquery-ui.css" />

<script type="text/javascript" >
    $(document).ready(function() { SetMenu("liCargaDatos"); }); 
</script>
<script type="text/javascript">
    $(function() {
        $("#tabs").tabs();
    });
  </script>
</asp:Content>
<asp:Content ID="cphContenedor" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div id="apertura">Asignar Kms a Lugar</div>
<div id="tabs">
        <ul>
            <li><a href="#tabs-1">Alta</a></li>
            <li><a href="#tabs-2">Filtros</a></li>        
        </ul>    
        <div id="tabs-1"  class="tabla_admin">
            <div class="label_admint"><label>Desde: </label></div> 
            <div class="label_adminc"><asp:DropDownList ID="ddlLugarDesde" CssClass="sol_campoadm" runat="server"></asp:DropDownList> </div>     
            <div class="label_relleno20"></div>
            <div class="label_relleno20"></div>
            <div class="label_admint"><label>Hasta: </label></div> 
            <div class="label_adminc"><asp:DropDownList ID="ddlLugarHasta" CssClass="sol_campoadm" runat="server"></asp:DropDownList> </div>     
             <div class="label_relleno20"></div>
            <div class="label_relleno20"></div>
            <div class="label_admint"><label>Kms Est.</label></div>
            <div class="label_adminc"><asp:TextBox ID="txtKms" CssClass="sol_campoadm" runat="server" onkeypress="return ValidarNumeroDecimal(event,this,2);" /></div>
            <div class="label_relleno20"></div>    
            <div class="label_relleno20"></div>    

            <div class="label_admint"></div>
            <div class="label_adminc"> 
                <asp:RadioButtonList runat="server" RepeatColumns="2" ID="rbKmsEst">                
                    <asp:ListItem Text="Kms Cuadrilla" Value="false" />                    
                    <asp:ListItem Text="Kms Establecidos" Value="true"/>
                </asp:RadioButtonList>
                
            </div>
            <div class="label_relleno20"></div>    
            <div class="label_admintb"><asp:Button ID="btnGuardar" runat="server" Text="Guardar" CssClass="btn_form" onclick="btnGuardar_Click" OnClientClick="return ValidarObj('txtKms|Ingrese un kilometraje.');" /></div>
        </div>
       <div id="tabs-2"  class="tabla_admin">
            <div class="label_admint"><label>Desde: </label></div> 
            <div class="label_adminc"><asp:DropDownList ID="ddlDesdeFiltro" CssClass="sol_campoadm" runat="server"></asp:DropDownList> </div>     
            <div class="label_relleno20"></div>
            <div class="label_relleno20"></div>
            <div class="label_admint"><label>Hasta: </label></div> 
            <div class="label_adminc"><asp:DropDownList ID="ddlHastaFiltro" CssClass="sol_campoadm" runat="server"></asp:DropDownList> </div>     
            <div class="label_relleno20"></div>
            <div class="label_admintb"><asp:Button ID="btnBuscar" runat="server" Text="Buscar" CssClass="btn_form" onclick="btnBuscar_Click"/></div>
        </div>

</div>
<div>
     <asp:GridView ID="gvKilometros" runat="server" AutoGenerateColumns="False" 
         DataMember="idLugar"  CssClass="tabla3" 
         onrowcancelingedit="gvKilometros_RowCancelingEdit" 
         onrowediting="gvKilometros_RowEditing" 
         onrowupdating="gvKilometros_RowUpdating" 
         onrowdeleting="gvKilometros_RowDeleting"
         OnRowDataBound="gvKilometros_RowDataBound">
         <Columns>
             <asp:TemplateField HeaderText="Id Kms" Visible="false">
                 <EditItemTemplate>
                     <asp:Label ID="lblIdKms" runat="server" Text='<%# Bind("idKms") %>'></asp:Label>
                 </EditItemTemplate>
                 <ItemTemplate>
                     <asp:Label ID="lblidKmsI" runat="server" Text='<%# Bind("idKms") %>'></asp:Label>
                 </ItemTemplate>
             </asp:TemplateField>
             <asp:TemplateField HeaderText="Desde" HeaderStyle-CssClass="celda30t" ItemStyle-CssClass="celda30">
                 <EditItemTemplate>
                     <asp:Label ID="lblidLugarDesde" Visible="false" runat="server" Text='<%# Bind("oLugarDesde.idLugar") %>'></asp:Label>
                     <asp:DropDownList ID="ddlDesde" runat="server" CssClass="cajaTextoGrilla"></asp:DropDownList>
                 </EditItemTemplate>
                 <ItemTemplate>
                     <asp:Label ID="lblDesde" runat="server" Text='<%# Bind("oLugarDesde.Descripcion") %>'></asp:Label>
                 </ItemTemplate>
             </asp:TemplateField>
              <asp:TemplateField HeaderText="Hasta" HeaderStyle-CssClass="celda30t" ItemStyle-CssClass="celda30">
                 <EditItemTemplate>
                     <asp:Label ID="lblidLugarHasta" Visible="false" runat="server" Text='<%# Bind("oLugarHasta.idLugar") %>'></asp:Label>
                     <asp:DropDownList ID="ddlHasta" runat="server" CssClass="cajaTextoGrilla"></asp:DropDownList>
                 </EditItemTemplate>
                 <ItemTemplate>
                     <asp:Label ID="lblHasta" runat="server" Text='<%# Bind("oLugarHasta.Descripcion") %>'></asp:Label>
                 </ItemTemplate>
             </asp:TemplateField>
             <asp:TemplateField HeaderText="Kms" HeaderStyle-CssClass="celda10t" ItemStyle-CssClass="celda10">
                 <EditItemTemplate>
                     <asp:TextBox ID="txtKmsGV" runat="server" Text='<%# Bind("Kms") %>' CssClass="cajaTextoGrilla"></asp:TextBox>
                 </EditItemTemplate>
                 <ItemTemplate>
                     <asp:Label ID="lblKmsI" runat="server" Text='<%# Bind("Kms") %>'></asp:Label>
                 </ItemTemplate>
             </asp:TemplateField>
              <asp:TemplateField HeaderText="Kms Establecidos" HeaderStyle-CssClass="celda20t" ItemStyle-CssClass="celda20">
              <EditItemTemplate>
                     <asp:RadioButtonList runat="server" ID="rbKmsEstGV" RepeatColumns="2" > 
                        <asp:ListItem Text="Kms Cuadrilla" Value="False" />                    
                        <asp:ListItem Text="Kms Establecidos" Value="True"/>
                    </asp:RadioButtonList>
                 </EditItemTemplate>
                 <ItemTemplate>
                     <asp:CheckBox ID="chkKmsEst" runat="server" Checked='<%# Bind("KmsEstablecidos") %>' Enabled="false"/>
                 </ItemTemplate>                 
             </asp:TemplateField>
             <asp:TemplateField HeaderStyle-CssClass="celda15t" ItemStyle-CssClass="celda15b">
                 <EditItemTemplate>
                     <asp:LinkButton ID="btnEditar" runat="server" CausesValidation="True" CssClass="boton40"
                         CommandName="Update" Text="Actualizar"></asp:LinkButton>
                     <asp:LinkButton ID="btnEliminar" runat="server" CausesValidation="False" CssClass="boton40"
                         CommandName="Cancel" Text="Cancelar"></asp:LinkButton>
                 </EditItemTemplate>
                 <ItemTemplate>
                     <asp:LinkButton ID="btnEditar" runat="server" CausesValidation="False" CssClass="boton40"
                         CommandName="Edit" Text="Editar" ></asp:LinkButton>
                     <asp:LinkButton ID="btnEliminar" runat="server" CausesValidation="False" CssClass="boton40"
                         CommandName="Delete" Text="Eliminar" OnClientClick="return Eliminar('la Lugar');"></asp:LinkButton>
                 </ItemTemplate>
             </asp:TemplateField>
             
         </Columns>
     </asp:GridView>
 </div>
</asp:Content>
