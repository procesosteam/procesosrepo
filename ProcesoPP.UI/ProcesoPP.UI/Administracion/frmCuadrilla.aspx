﻿<%@ Page Title="PP - Cuadrilla" Language="C#" MasterPageFile="~/frmPrincipal.Master" AutoEventWireup="true" EnableEventValidation="false" CodeBehind="frmCuadrilla.aspx.cs" Inherits="ProcesoPP.UI.Administracion.frmCuadrilla" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<link rel="stylesheet" href="../Css/jquery-ui.css" />
<script type="text/javascript" >
    $(document).ready(function () { SetMenu("liCargaDatos"); });

    $(document).ready(function () {
        cambiarTab($('#<%=hdidLastTab.ClientID%>').val());  
    });

    function cambiarTab(act) {
        $("#tabs").tabs({
            active: function () {
                var newIdx = $("#tabs").tabs('option', 'active');
                $('#<%=hdidLastTab.ClientID%>').val(newIdx);
            },
            /*heightStyle: "auto",*/
            //active: previuslySelectedTab,
            active: act,
            show: { effect: "fadeIn", duration: 500 }
        });
    }
    
    function valida() {
        val = ValidarObj('txtNombre|Ingrese un nombre para la cuadrilla.~txtFecha|Seleccionar Fecha.');
        if (val) {
            //guardarCuadrillaPersonal();
            return true;
        }

        return val;
    }

    function CalendarioGV(CajaTexto, Boton) {
        Calendar.setup({ 
            inputField: CajaTexto,      // id del campo de texto
            ifFormat: "%d/%m/%Y",       // formato de la fecha, cuando se escriba en el campo de texto
            button: Boton   // el id del botón que lanzará el calendario       
        });
    }

    function destilda(oChk) {
        oChk = oChk.childNodes[0];
        row = oChk.parentNode.parentNode.parentNode;
        txtBaja = row.cells[4].childNodes[1];
        txtAlta = row.cells[3].childNodes[1];

        if (!oChk.checked) {
            txtBaja.value = (new Date()).format("dd/MM/yyyy");
        }
        else {
            txtBaja.value = "";
            txtAlta.value = (new Date()).format("dd/MM/yyyy");
        }

    }
</script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <cc1:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" EnablePartialRendering="true" EnableScriptGlobalization="true">
        </cc1:ToolkitScriptManager>
    <div id="apertura">CUADRILLAS</div>
    
    <asp:HiddenField   runat="server" ID="hfPersonal" />
    <div id="tabs">
        <ul>
            <li><a href="#tabs-1" tabindex="1">Búsqueda de Cuadrillas</a></li>
            <li><a href="#tabs-2" tabindex="2">Alta de Cuadrillas</a></li>        
        </ul>
        <div id="tabs-1" class="tabla_admin" style="height:auto;">
            <div class="label_admint">    
                <label>Nombre de la Cuadrilla: </label>  </div> 
            <div class="label_adminc">      
                <asp:TextBox ID="txtBusqueda" CssClass="sol_campoadm" runat="server"  ></asp:TextBox></div>
                <div class="label_adminc"></div>
            <div class="label_adminc"></div>
            <div class="label_adminc"></div>
             <div class="label_admintb">           
                 <asp:Button ID="btnBuscar" runat="server" Text="Buscar"  CssClass="btn_form" 
                     onclick="btnBuscar_Click" />
            </div>    
        </div>
        <div id="tabs-2" class="tabla_admin" style="height:auto;">
              <div class="tabla_admin">
                    <div class="label_admint">
                      <label>Nombre de la Cuadrilla: </label></div> 
                    <div class="label_adminc"><asp:TextBox ID="txtNombre" CssClass="sol_campoadm" runat="server" ></asp:TextBox> </div>     
                    <div class="label_relleno20"></div>
                    <div class="label_relleno20"></div>
                    <div class="label_admint">
                      <label>Fecha de Alta: </label></div> 
                    <div class="label_adminc"><asp:TextBox ID="txtFecha" CssClass="sol_campoadm" runat="server" ></asp:TextBox> </div>     
                    <div class="label_relleno20">
                        <img id="btnFecha" alt="calendario" src="../img/calendario.gif" style="width: 20px;
                        height: 20px; cursor: pointer;" onmouseover="CambiarConfiguracionCalendario('txtFecha',this);" />
                    </div>
                 
                 <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                
                        <ContentTemplate >
                        <asp:HiddenField runat="server" ID="hdidLastTab" Value="0" />

                        <div class="label_relleno20"></div>                   
                        <div class="label_admint"><label>Empresa afectada: </label></div> 
                        <div class="label_adminc">
                            <asp:DropDownList ID="ddlEmpresa" CssClass="sol_campoadm"  runat="server" 
                                 AutoPostBack="true" onselectedindexchanged="ddlEmpresa_SelectedIndexChanged"/>                            
                        </div>     
                        <div class="label_relleno20"></div>
                        <div class="label_relleno20"></div>
                    
                            <div class="label_admint"><label>Sector Afectado: </label></div> 
                            <div class="label_adminc">
                                <asp:DropDownList ID="ddlSector" CssClass="sol_campoadm" runat="server"
                                    AutoPostBack="true" onselectedindexchanged="ddlSector_SelectedIndexChanged"></asp:DropDownList>                                 
                            </div>     
<div class="label_relleno20"></div>
<div class="label_relleno20"></div>

                       
      <div style="height:250px;">
      <div class="label_admint2" style="height: 90%;"><label>Seleccione el Personal afectado: </label></div> 
                                <div class="label_esp" style="height:90%; width:77%; overflow:scroll;" >
                                    <asp:GridView runat="server" id="gvPersonal" AutoGenerateColumns="false" CssClass="tabla3" 
                                    OnRowDataBound="gvPersonal_RowDataBound" >
                                    <Columns>
                                        <asp:TemplateField>
                                            <ItemTemplate><asp:HiddenField runat="server" ID="hfIdPersonal" Value='<%# Bind("IdPersonal") %>'/> </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Act." HeaderStyle-CssClass="celda10t" ItemStyle-CssClass="celda10">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkPersonal" runat="server" onchange="return destilda(this);"></asp:CheckBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Nombre" HeaderStyle-CssClass="celda30t" ItemStyle-CssClass="celda30">
                                            <ItemTemplate>
                                                <asp:Label runat="server" id="lblNombre"/>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="F. Inicio trabajo" HeaderStyle-CssClass="celda30t" ItemStyle-CssClass="celda30">
                                            <ItemTemplate>
                                                <asp:TextBox CssClass="cajaTextoGrilla" runat="server" id="txtFechaAlta" Width="80%" />
                                                <img id="btnFecha" alt="calendario" src="../img/calendario.gif" style="width: 14px;
                                                height: 14px; cursor: pointer;" onmouseover="CalendarioGV(this.parentNode.childNodes[1].id,this);" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Fecha Fin" HeaderStyle-CssClass="celda30t" ItemStyle-CssClass="celda30">
                                            <ItemTemplate>
                                                <asp:TextBox CssClass="cajaTextoGrilla" runat="server" id="txtFechaBaja" Width="80%"/>
                                                <img id="btnFecha" alt="calendario" src="../img/calendario.gif" style="width: 14px;
                                                height: 14px; cursor: pointer;" onmouseover="CalendarioGV(this.parentNode.childNodes[1].id,this);" />
                                            </ItemTemplate>                                        
                                        </asp:TemplateField>
                                    </Columns>
                                    </asp:GridView> 
                                </div>
                            </div>                         
                        </ContentTemplate>
                      
                    </asp:UpdatePanel>
                    

                    <div class="label_admint"></div>
                    <div class="label_adminc"></div>
                    <div class="label_admintb">
                        <asp:Button ID="btnCancelar" runat="server" Text="Cancelar" CssClass="btn_form" onclick="btnCancelar_Click" /></div>
                    <div class="label_admintb">
                        <asp:Button ID="btnGuardar" runat="server" Text="Guardar" CssClass="btn_form" onclick="btnGuardar_Click" OnClientClick="return valida();" /></div>
                </div>
        </div>
    </div>
    <div>
         <asp:GridView ID="gvCuadrilla" runat="server" AutoGenerateColumns="False" 
             DataMember="idCuadrilla"  CssClass="tabla3" 
             onrowediting="gvTipoServicio_RowEditing" 
             onrowdeleting="gvTipoServicio_RowDeleting"
             OnRowDataBound="gvTipoServicio_RowDataBound" 
             >
             <Columns>
                 <asp:TemplateField Visible="false">                     
                     <ItemTemplate>
                         <asp:Label ID="lblCuadrillaI" runat="server" Text='<%# Bind("idCuadrilla") %>'></asp:Label>
                     </ItemTemplate>
                 </asp:TemplateField>
                 <asp:TemplateField HeaderText="DESCRIPCION" HeaderStyle-CssClass="celda50t" ItemStyle-CssClass="celda50">                     
                     <ItemTemplate>
                         <asp:Label ID="lblDescripcion" runat="server" Text='<%# Bind("Nombre") %>'></asp:Label>
                     </ItemTemplate>
                 </asp:TemplateField>
                 <asp:TemplateField HeaderText="Fecha" HeaderStyle-CssClass="celda30t" ItemStyle-CssClass="celda30" >                     
                     <ItemTemplate>
                         <asp:Label ID="lblFecha" runat="server" Text='<%# Bind("FechaAlta", "{0:d}") %>'></asp:Label>
                     </ItemTemplate>
                 </asp:TemplateField>
                
                 <asp:TemplateField ShowHeader="false" HeaderStyle-CssClass="celda15t" ItemStyle-CssClass="celda15b">
                     <ItemTemplate >
                         <asp:LinkButton ID="btnEditar" runat="server" CausesValidation="False" CssClass="boton40"
                             CommandName="Edit" Text="Editar" ></asp:LinkButton>
                         <asp:LinkButton ID="btnEliminar" runat="server" CausesValidation="False" CssClass="boton40"
                             CommandName="Delete" Text="Eliminar" OnClientClick="return Eliminar('la Cuadrilla');"></asp:LinkButton>
                     </ItemTemplate>
                 </asp:TemplateField>
             </Columns>
         </asp:GridView>
     </div>
</asp:Content>
