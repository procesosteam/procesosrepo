﻿<%@ Page Language="C#" MasterPageFile="~/frmPrincipal.Master" AutoEventWireup="true" CodeBehind="frmUsuario.aspx.cs" Inherits="ProcesoPP.UI.Administracion.frmUsuario" Title="Gestion de Usuarios" MaintainScrollPositionOnPostBack="true" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="apertura">Alta | Baja | Modificación de Usuario</div>
    <div class="tabla3"> 
        <div class="label40t"><label>Nombre: </label></div> 
        <div class="label40t"><asp:TextBox runat="server" ID="txtNombre"></asp:TextBox></div>
        <div class="label40t"><label>Apellido: </label> </div> 
        <div class="label40t"><asp:TextBox runat="server" ID="txtApellido" onblur="return proponerNombre('txtNombre', 'txtApellido', 'txtUserName');"></asp:TextBox></div> 
        <div class="label40t"><label>Nombre Usuario: </label></div> 
        <div class="label40t"><asp:TextBox runat="server" ID="txtUserName"></asp:TextBox></div> 
        <div class="label40t"><label>Clave: </label></div> 
        <div class="label40t"><asp:TextBox runat="server" ID="txtClaveRepetida" TextMode="Password"></asp:TextBox></div> 
        <div class="label40t"><label>Repita la Clave : </label></div> 
        <div class="label40t"><asp:TextBox runat="server" ID="txtClave" TextMode="Password" onblur="return ValidarClave('txtClaveRepetida','txtClave')"></asp:TextBox></div> 
        <div class="label40t"></div>
        <div class="labelb30t"><asp:Button runat="server" id="btnGuardar" CssClass="btn_form" Text="Guardar"
        OnClientClick="return ValidarObj('txtNombre|Debe ingresar el Nombre.~txtApellido|Debe ingresar el Apellido.~txtUserName|Debe ingresar el nombre del usuario del sistema.~txtClaveRepetida|Debe ingresar una clave para validar.~txtClave|Debe repetir la clave ingresada.');"
                onclick="btnGuardar_Click"/></div>
        
    </div>
    <div class="tabla3" id="divMensaje" runat="server">
         <asp:Label ID="lblError" CssClass="label40t" runat="server" Text="Error" Visible="false"></asp:Label>
     </div>
     <div style="width:800px;">
         <asp:GridView ID="gvUsuario" runat="server" AutoGenerateColumns="False" 
             DataMember="idTipoServicio" CssClass="tabla3" Width="100%"
             onrowcancelingedit="gvTipoServicio_RowCancelingEdit" 
             onrowediting="gvUsuario_RowEditing" 
             onrowupdating="gvUsuario_RowUpdating" 
             onrowdeleting="gvUsuario_RowDeleting">
             <Columns>
                 <asp:TemplateField HeaderText="Id Us." HeaderStyle-CssClass="celda10t" ItemStyle-CssClass="celda10" Visible="false">                    
                     <ItemTemplate>
                         <asp:Label ID="lblidUsuario" runat="server" Text='<%# Bind("idUsuario") %>'></asp:Label>
                     </ItemTemplate>
                 </asp:TemplateField>
                 <asp:TemplateField HeaderText="Nombre" HeaderStyle-CssClass="celda20t" ItemStyle-CssClass="celda20">
                     <EditItemTemplate>
                         <asp:TextBox ID="txtNombre" runat="server" Text='<%# Bind("Nombre") %>'></asp:TextBox>
                     </EditItemTemplate>
                     <ItemTemplate>
                         <asp:Label ID="lblNombre" runat="server" Text='<%# Bind("Nombre") %>'></asp:Label>
                     </ItemTemplate>
                 </asp:TemplateField>
                 <asp:TemplateField HeaderText="Apellido" HeaderStyle-CssClass="celda20t" ItemStyle-CssClass="celda20">
                     <EditItemTemplate>
                         <asp:TextBox ID="txtApellido" runat="server" Text='<%# Bind("Apellido") %>'></asp:TextBox>
                     </EditItemTemplate>
                     <ItemTemplate>
                         <asp:Label ID="lblApellidoI" runat="server" Text='<%# Bind("Apellido") %>'></asp:Label>
                     </ItemTemplate>
                 </asp:TemplateField>
                 <asp:TemplateField HeaderText="User Name" HeaderStyle-CssClass="celda20t" ItemStyle-CssClass="celda20">
                     <EditItemTemplate>
                         <asp:TextBox ID="txtUserName" runat="server" Text='<%# Bind("userName") %>'></asp:TextBox>
                     </EditItemTemplate>
                     <ItemTemplate>
                         <asp:Label ID="lbluserNameI" runat="server" Text='<%# Bind("userName") %>'></asp:Label>
                     </ItemTemplate>
                 </asp:TemplateField>
                 <asp:TemplateField ShowHeader="False" HeaderStyle-CssClass="celda20t" ItemStyle-CssClass="celda20">
                     <EditItemTemplate>
                         <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="True" CssClass="boton40"
                             CommandName="Update" Text="Actualizar"></asp:LinkButton>
                         <asp:LinkButton ID="LinkButton2" runat="server" CausesValidation="False" CssClass="boton40"
                             CommandName="Cancel" Text="Cancelar"></asp:LinkButton>                  
                     </EditItemTemplate>
                     <ItemTemplate>
                         <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="False" CssClass="boton40"
                             CommandName="Edit" Text="Editar" ></asp:LinkButton>      
                         <asp:LinkButton ID="LinkButton2" runat="server" CausesValidation="False" CssClass="boton40"
                             CommandName="Delete" Text="Eliminar" OnClientClick="return Eliminar('el Usuario');"></asp:LinkButton> 
                     </ItemTemplate>
                 </asp:TemplateField>
             </Columns>
         </asp:GridView>
     </div>
</asp:Content>
