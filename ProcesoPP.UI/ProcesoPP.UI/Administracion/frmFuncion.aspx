﻿<%@ Page Language="C#" MasterPageFile="~/frmPrincipal.Master" AutoEventWireup="true" CodeBehind="frmFuncion.aspx.cs" Inherits="ProcesoPP.UI.Administracion.frmFuncion" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server"> 
<script type="text/javascript" >
    $(document).ready(function () { SetMenu("liCargaDatos,liCarga"); }); 
</script>
</asp:Content>
<asp:Content ContentPlaceHolderID="ContentPlaceHolder1" runat="server" ID="content">
    <div id="apertura">ALTA | BAJA | MODIFICACION DE FUNCION</div>
    <div class="tabla_admin">
    <div class="label_admint"><label>Función: </label></div> 
    <div class="label_adminc"> <asp:TextBox ID="txtFuncion" CssClass="sol_campoadm" runat="server"></asp:TextBox></div>
    
          <div class="label_admintb">
        <asp:Button ID="btnGuardar" runat="server" Text="Guardar"  CssClass="btn_form"        
            onclick="btnGuardar_Click" OnClientClick="return ValidarObj('txtFuncion|Ingrese una Descripción.');" />
       </div>
       
      <div class="label_relleno20"></div>
    </div>
    <div id="divMensaje" runat="server">
         <asp:Label ID="lblError" runat="server" Text="Error" Visible="false"></asp:Label>
     </div>
    <div>
         <asp:GridView ID="gvFuncion" runat="server" AutoGenerateColumns="False" 
             DataMember="idFuncion" CssClass="tabla3"
             onrowcancelingedit="gvFuncion_RowCancelingEdit" 
             onrowediting="gvFuncion_RowEditing" 
             onrowupdating="gvFuncion_RowUpdating" 
             onrowdeleting="gvFuncion_RowDeleting"
             OnRowDataBound="gvFuncion_RowDataBound">
             <Columns>
                 <asp:TemplateField HeaderText="Id Funcion" HeaderStyle-CssClass="celda80" ItemStyle-CssClass="celda80t" Visible="false">
                     <EditItemTemplate>
                         <asp:Label ID="lblIdFuncion" runat="server" Text='<%# Bind("idFuncion") %>' ></asp:Label>
                     </EditItemTemplate>
                     <ItemTemplate>
                         <asp:Label ID="lblFuncionI" runat="server" Text='<%# Bind("idFuncion") %>'></asp:Label>
                     </ItemTemplate>
                 </asp:TemplateField>
                 <asp:TemplateField HeaderText="Descripción" HeaderStyle-CssClass="celda80t" ItemStyle-CssClass="celda80">
                     <EditItemTemplate>
                         <asp:TextBox ID="txtDescripcion" runat="server" Text='<%# Bind("Descripcion") %>'></asp:TextBox>
                     </EditItemTemplate>
                     <ItemTemplate>
                         <asp:Label ID="lblDescripcion" runat="server" Text='<%# Bind("Descripcion") %>'></asp:Label>
                     </ItemTemplate>
                 </asp:TemplateField>
                 <asp:TemplateField ShowHeader="False" HeaderStyle-CssClass="celda15t" ItemStyle-CssClass="celda15b">
                     <EditItemTemplate>
                         <asp:LinkButton ID="btnEditar" runat="server" CausesValidation="True" CssClass="boton40"
                             CommandName="Update" Text="Actualizar"></asp:LinkButton>
                         <asp:LinkButton ID="btnEliminar" runat="server" CausesValidation="False" CssClass="boton40"
                             CommandName="Cancel" Text="Cancelar"></asp:LinkButton>
                     </EditItemTemplate>
                     <ItemTemplate>
                         <asp:LinkButton ID="btnEditar" runat="server" CausesValidation="False"  CssClass="boton40"
                             CommandName="Edit" Text="Editar" ></asp:LinkButton>
                        <asp:LinkButton ID="btnEliminar" runat="server" CausesValidation="False"  CssClass="boton40"
                             CommandName="Delete" Text="Eliminar" OnClientClick="return Eliminar('la Funcion');"></asp:LinkButton>
                     </ItemTemplate>
                 </asp:TemplateField>
             </Columns>
         </asp:GridView>
     </div>
</asp:Content>
