﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ProcesoPP.Model;
using ProcesoPP.Business;
using ProcesoPP.Services;

namespace ProcesoPP.UI.Administracion
{
    public partial class frmModalidad : System.Web.UI.Page
    {
        #region PROPIEDADES

        public Permisos _Permiso
        {
            get { return (Permisos)ViewState["permiso"]; }
            set { ViewState["permiso"] = value; }
        }

        public int _idUsuario
        {
            get { return (int)ViewState["idUsuario"]; }
            set { ViewState["idUsuario"] = value; }
        }

        #endregion

        #region EVENTOS

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                _idUsuario = Common.EvaluarSession(Request, this.Page);
                if (_idUsuario != 0)
                {
                    _Permiso = Common.getModulo(Request, this.Page);
                    if (_Permiso != null && _Permiso.Lectura)
                    {
                        Mensaje.ocultarDivMsj(divMensaje);
                        if (!IsPostBack)
                        {
                            CargarModalidades();
                            InitScrren();
                        }
                    }
                    else
                    {
                        Mensaje.errorMsj("NO TIENE PERMISOS A ESTE FORMULARIO", this.Page, "SIN ACCESO", "../frmPrincipal.aspx");
                    }
                }
                else
                {
                    Mensaje.errorMsj("DEBE LOGUEARSE", this.Page, "LOGIN", "../frmLogin.aspx");
                }
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                Guardar();
                CargarModalidades();
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        protected void gvModalidad_RowEditing(object sender, GridViewEditEventArgs e)
        {
            gvModalidad.EditIndex = e.NewEditIndex;
            CargarModalidades();
        }

        protected void gvModalidad_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            gvModalidad.EditIndex = -1;
            CargarModalidades();
        }

        protected void gvModalidad_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                Modificar(gvModalidad.Rows[e.RowIndex]);
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        protected void gvModalidad_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                Eliminar(gvModalidad.Rows[e.RowIndex]);
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        protected void gvModalidad_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Common.CargarRow(e, _Permiso);
            }
        }

        #endregion

        #region Metodos

        private void InitScrren()
        {
            if (!_Permiso.Escritura)
            {
                btnGuardar.Visible = false;
            }
        }

        private void Eliminar(GridViewRow row)
        {
            
            int id = int.Parse((row.FindControl("lblModalidadI") as Label).Text);
            ModalidadBus oModalidadBus = new ModalidadBus();

            if (oModalidadBus.ModalidadDelete(id))
            {
                Mensaje.successMsj("Modalidad Eliminada con éxito.", this.Page, "Eliminado", "");
                CargarModalidades();
            }
        }

        private void Modificar(GridViewRow row)
        {
            int id = int.Parse((row.FindControl("lblIdModalidad") as Label).Text);
            string descripcion = (row.FindControl("txtDescripcion") as TextBox).Text;
            ModalidadBus oModalidadBus = new ModalidadBus();
            Modalidad oModalidad = new Modalidad(id, descripcion);

            if (oModalidadBus.ModalidadUpdate(oModalidad))
            {
                Mensaje.successMsj("Modificado con Exito!", this.Page, "Modificado", null);
                gvModalidad.EditIndex = -1;
                CargarModalidades();
            }
        }

        private void Guardar()
        {
            ModalidadBus oModalidadBus = new ModalidadBus();
            Modalidad oModalidad = new Modalidad(0, txtModalidad.Text);
            oModalidad.IdModalidad = oModalidadBus.ModalidadAdd(oModalidad);

            if (oModalidad.IdModalidad > 0)
            {
                Mensaje.successMsj("Guardado con Exito!", this.Page, "Agregado", null);
                txtModalidad.Text = "";
                CargarModalidades();
            }
        }

        private void CargarModalidades()
        {
            ModalidadBus oModalidadBus = new ModalidadBus();
            gvModalidad.DataSource = oModalidadBus.ModalidadGetAll();
            gvModalidad.DataBind();
        }

        #endregion


    }
}
