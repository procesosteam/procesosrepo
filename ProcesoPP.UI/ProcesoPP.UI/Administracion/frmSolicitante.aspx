﻿<%@ Page Language="C#" AutoEventWireup="true" MaintainScrollPositionOnPostback="true"  MasterPageFile="~/frmPrincipal.Master"  CodeBehind="frmSolicitante.aspx.cs" Inherits="ProcesoPP.UI.Administracion.frmSolicitante" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server"> 
<script type="text/javascript" >
    $(document).ready(function() { SetMenu("liCargaDatos"); }); 
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="apertura">Alta | Baja | Modificación de Solicitante</div>
<div class="tabla_admin">
    <div class="label_admint"><label>Nombre Apellido: </label></div> 
    <div class="label_adminc"><asp:TextBox ID="txtDescripcion" CssClass="sol_campoadm" runat="server"></asp:TextBox> </div>
    <div class="label_relleno20"></div>
    <div class="label_relleno20"></div>
    
    <div class="label_admint"><label>Empresa: </label></div> 
    <div class="label_adminc"><asp:DropDownList ID="ddlEmpresa" CssClass="sol_campoadm" 
            runat="server" onselectedindexchanged="ddlEmpresa_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList> </div>     
    <div class="label_relleno20"></div>
    <div class="label_relleno20"></div>    
      
    <div class="label_admint"><label>Sector: </label></div> 
    <div class="label_adminc"><asp:DropDownList ID="ddlSector" CssClass="sol_campoadm" runat="server"></asp:DropDownList> </div>     
    <div class="label_relleno20"></div>
    
    <div class="label_admintb"><asp:Button ID="btnGuardar" runat="server" Text="Guardar" CssClass="btn_form" onclick="btnGuardar_Click" OnClientClick="return ValidarObj('txtDescripcion|Ingrese una Descripción.');" /></div>     
    
</div>
<div class="tabla3" id="divMensaje" runat="server">
     <asp:Label ID="lblError" CssClass="celda40t" runat="server" Text="Error" Visible="false"></asp:Label>
 </div>
<div>
     <asp:GridView ID="gvSolicitante" runat="server" AutoGenerateColumns="False" 
         DataMember="idSolicitantes"  CssClass="tabla3" 
         onrowcancelingedit="gvSolicitantes_RowCancelingEdit" 
         onrowediting="gvSolicitantes_RowEditing" 
         onrowupdating="gvSolicitantes_RowUpdating" 
         onrowdeleting="gvSolicitantes_RowDeleting"
         OnRowDataBound="gvSolicitantes_RowDataBound">
         <Columns>
             <asp:TemplateField HeaderText="Id Solicitantes" Visible="false">
                 <EditItemTemplate>
                     <asp:Label ID="lblIdSolicitantes" runat="server" Text='<%# Bind("idSolicitante") %>'></asp:Label>
                 </EditItemTemplate>
                 <ItemTemplate>
                     <asp:Label ID="lblSolicitantesI" runat="server" Text='<%# Bind("idSolicitante") %>'></asp:Label>
                 </ItemTemplate>
             </asp:TemplateField>
             <asp:TemplateField HeaderText="Nombre" HeaderStyle-CssClass="celda30t" ItemStyle-CssClass="celda30">
                 <EditItemTemplate>
                     <asp:TextBox ID="txtNombre" runat="server" Text='<%# Bind("Nombre") %>'></asp:TextBox>
                 </EditItemTemplate>
                 <ItemTemplate>
                     <asp:Label ID="lblDescripcion" runat="server" Text='<%# Bind("Nombre") %>'></asp:Label>
                 </ItemTemplate>
             </asp:TemplateField>
             <asp:TemplateField HeaderText="Empresa" HeaderStyle-CssClass="celda30t" ItemStyle-CssClass="celda30">
                 <EditItemTemplate>
                     <asp:Label ID="lblIdEmpresa" Visible="false" runat="server" Text='<%# Bind("oEmpresa.idEmpresa") %>'></asp:Label>
                     <asp:DropDownList ID="ddlEmpresa" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlEmpresaGV_SelectedIndexChanged"></asp:DropDownList>
                 </EditItemTemplate>
                 <ItemTemplate>
                     <asp:Label ID="lblEmpresa" runat="server" Text='<%# Bind("oEmpresa.Descripcion") %>'></asp:Label>
                 </ItemTemplate>
             </asp:TemplateField>
             <asp:TemplateField HeaderText="Sector" HeaderStyle-CssClass="celda30t" ItemStyle-CssClass="celda30">
                 <EditItemTemplate>
                     <asp:Label ID="lblIdSector" Visible="false" runat="server" Text='<%# Bind("oSector.idSector") %>'></asp:Label>
                     <asp:DropDownList ID="ddlSector" runat="server"></asp:DropDownList>
                 </EditItemTemplate>
                 <ItemTemplate>
                     <asp:Label ID="lblSector" runat="server" Text='<%# Bind("oSector.Descripcion") %>'></asp:Label>
                 </ItemTemplate>
             </asp:TemplateField>
             <asp:TemplateField HeaderStyle-CssClass="celda15t" ItemStyle-CssClass="celda15b">
                 <EditItemTemplate >
                     <asp:LinkButton ID="btnEditar" runat="server" CausesValidation="True" CssClass="boton40"
                         CommandName="Update" Text="Actualizar"></asp:LinkButton>
                     <asp:LinkButton ID="btnEliminar" runat="server" CausesValidation="False" CssClass="boton40"
                         CommandName="Cancel" Text="Cancelar"></asp:LinkButton>
                 </EditItemTemplate>
                 <ItemTemplate >
                     <asp:LinkButton ID="btnEditar" runat="server" CausesValidation="False" CssClass="boton40"
                         CommandName="Edit" Text="Editar" ></asp:LinkButton>
                     <asp:LinkButton ID="btnEliminar" runat="server" CausesValidation="False" CssClass="boton40"
                         CommandName="Delete" Text="Eliminar" OnClientClick="return Eliminar('el Solicitantes');"></asp:LinkButton>
                 </ItemTemplate>
             </asp:TemplateField>
         </Columns>
     </asp:GridView>
 </div>
</asp:Content>