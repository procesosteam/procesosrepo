﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using ProcesoPP.Business;
using ProcesoPP.Model;
using ProcesoPP.Services;
using ParteDiario.Services;
using System.Web.Services;
using AjaxControlToolkit;
using System.Collections.Generic;

namespace ProcesoPP.UI
{
    public partial class frmGestionCuadrilla : System.Web.UI.Page
    {
        #region PROPIEDADES

        public Permisos _Permiso
        {
            get { return (Permisos)ViewState["permiso"]; }
            set { ViewState["permiso"] = value; }
        }

        public int _idUsuario
        {
            get { return (int)ViewState["idUsuario"]; }
            set { ViewState["idUsuario"] = value; }
        }
        
        public int _ADM
        {
            get { return int.Parse(ConfigurationManager.AppSettings["ADM"].ToString()); }
        }

        #endregion
        
        #region EVENTOS

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                 _idUsuario = Common.EvaluarSession(Request, this.Page);
                 if (_idUsuario != 0)
                 {
                     _Permiso = Common.getModulo(Request, this.Page);
                     if (_Permiso != null && _Permiso.Lectura)
                     {
                         if (!IsPostBack)
                         {
                             //Cargamos el valor por defecto de sector
                             ddlSector.Items.Insert(0, new ListItem("Seleccione Sector", "0"));
                         }
                     }
                     else
                     {
                         Mensaje.errorMsj("NO TIENE PERMISOS A ESTE FORMULARIO", this.Page, "SIN ACCESO", "../frmPrincipal.aspx");
                     }
                 }
                 else
                 {
                     Mensaje.errorMsj("DEBE LOGUEARSE", this.Page, "LOGIN", "../frmLogin.aspx");
                 }
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        protected void btExportar_Click(object sender, EventArgs e)
        {
            try
            {
                exportar();
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }
               

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                CargarCuadrilla();
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        
        protected void gvCuadrilla_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "Ver": 
                    Response.Redirect("frmCuadrilla.aspx?idCuadrilla=" + e.CommandArgument + "&idModulo=" + _Permiso.oModulo.idModulo + "&Enabled=true");
                    break;
                
            }
        }

        protected void gvCuadrilla_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
                CargarRow(e);
        }

        protected void gvCuadrilla_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvCuadrilla.PageIndex = e.NewPageIndex;
            CargarCuadrilla();
        }

        #endregion

        #region METODOS


        private void CargarRow(GridViewRowEventArgs e)
        {           
           Common.CargarRow(e, _Permiso);
        }

        private void CargarCuadrilla()
        {
            CuadrillaBus oCuadrillaBus = new CuadrillaBus();
            gvCuadrilla.DataSource = oCuadrillaBus.CuadrillaGestionGetByFilter(txtFecha.Text,txtFechaHasta.Text,ddlPersona.SelectedValue,ddlCuadrilla.SelectedValue,ddlSector.SelectedValue,ddlEmpresa.SelectedValue);
            gvCuadrilla.DataBind();
        }
        
        #endregion

    

        private void exportar()
        {
            CuadrillaBus oCuadrillaBus = new CuadrillaBus();
            DataTable dt = oCuadrillaBus.CuadrillaGestionGetByFilter(txtFecha.Text, txtFechaHasta.Text, ddlPersona.SelectedValue, ddlCuadrilla.SelectedValue, ddlSector.SelectedValue, ddlEmpresa.SelectedValue);
            dt.Columns.Remove("idCuadrilla");
            string path = System.IO.Path.Combine(Server.MapPath("~"), "img");
            ExportarExcel oExport = new ExportarExcel("Cuadrilla_" + DateTime.Now.ToString("yyyyMMddHHmmss"), path, "CUADRILLA");
            oExport.ExportarExcel2003(dt);

        }


        [WebMethod]
        public static CascadingDropDownNameValue[] GetEmpresa(string knownCategoryValues, string category)
        {
            EmpresaBus oEmpresaBus = new EmpresaBus();
            List<CascadingDropDownNameValue> empresa = oEmpresaBus.GetDataEmpresaInteras();
            return empresa.ToArray();
        }

        [WebMethod]
        public static CascadingDropDownNameValue[] GetSector(string knownCategoryValues, string category)
        {
            string idEmpresa = CascadingDropDown.ParseKnownCategoryValuesString(knownCategoryValues)["idEmpresa"];
            SectorBus oSectorBus = new SectorBus();
            List<CascadingDropDownNameValue> sectores = oSectorBus.GetDataSector(int.Parse(idEmpresa));
            return sectores.ToArray();
        }

        [WebMethod]
        public static CascadingDropDownNameValue[] GetPersona(string knownCategoryValues, string category)
        {
            string idPersona = CascadingDropDown.ParseKnownCategoryValuesString(knownCategoryValues)["idPersonal"];
            PersonalBus oPersonalBus = new PersonalBus();
            List<CascadingDropDownNameValue> personal = oPersonalBus.GetDataPersonal();
            return personal.ToArray();
        }

        [WebMethod]
        public static CascadingDropDownNameValue[] GetCuadrilla(string knownCategoryValues, string category)
        {
            string idCuadrilla = CascadingDropDown.ParseKnownCategoryValuesString(knownCategoryValues)["idCuadrilla"];
            CuadrillaBus oCuadrillaBus = new CuadrillaBus();
            List<CascadingDropDownNameValue> cuadrilla = oCuadrillaBus.GetDataCuadrilla();
            return cuadrilla.ToArray();
        }
    }
}