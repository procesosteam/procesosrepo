﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="frmOlvidoContrasenia.aspx.cs" Inherits="ProcesoPP.UI.Administracion.frmOlvidoContrasenia" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>CAMBIO CONTRASEÑA</title>
    <link href="../Css/estilos.css" rel="stylesheet" type="text/css"/>
    <link href="../Css/messi.min.css" rel="stylesheet" type="text/css" />
    <link href="../Css/messi.css" rel="stylesheet" type="text/css" />
    <link href="../Css/estilosPopUp.css" rel="stylesheet" type="text/css" />
    
    <script src="../Js/jquery-1.10.1.min.js" type="text/javascript"></script>
    <script src="../Js/messi.min.js" type="text/javascript"></script>
    <script src="../Js/messi.js" type="text/javascript"></script>
    <script src="../Js/Validadores.js" type="text/javascript"></script>
    <script src="../Js/Funciones.js" type="text/javascript"></script>
    <script src="../Js/popUp.js" type="text/javascript"></script>
    <script type="text/javascript">
        function irLogin() {
            cerrarPopUp();
            window.location.href = '../frmLogin.aspx';
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
  <div id="contenido">
        <div id="apertura">OLVIDO LA CONTRASEÑA??</div>     
         <div class="label_relleno20"></div>        
        <div class="label_admint">
            <label>Nombre Usuario:</label>
        </div>
        <div class="label_adminc">
            <asp:TextBox runat="server" CssClass="sol_campoadm" ID="txtUserName" /></div>
        <div class="label_relleno20"></div> 
        <div class="label_relleno20"></div>        
        <div class="label_admint">
            <label>Mail:</label>
        </div>
        <div class="label_adminc">
            <asp:TextBox runat="server" CssClass="sol_campoadm" ID="txtMail"></asp:TextBox></div>
        <div class="label_relleno20"></div> 
        <div class="label_relleno20"></div>
        <div class="label_adminc">
             <asp:Button runat="server" ID="btnGuardar" CssClass="btn_form" Text="Enviar nueva Contraseña" 
               OnClientClick="return ValidarObj('txtUsuario|Debe ingresar El Nombre de Usuario.~txtMail|Debe ingresar el Mail.');"
                OnClick="btnGuardar_Click" /></div>
         <div class="label_relleno20">
         <asp:Button runat="server" ID="btnVolver" CssClass="btn_form" Text="Volver al Login" 
               PostBackUrl="~/frmLogin.aspx" Width="105px"/></div>
         <div class="label_relleno20"></div> 
    </div>
    <asp:HiddenField runat="server" ID="hfIdUsuario" Value="0" />
    </form>
</body>
</html>
