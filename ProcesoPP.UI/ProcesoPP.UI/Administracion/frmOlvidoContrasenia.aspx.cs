﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ProcesoPP.Model;
using ProcesoPP.Business;
using ProcesoPP.Services;

namespace ProcesoPP.UI.Administracion
{
    public partial class frmOlvidoContrasenia : System.Web.UI.Page
    {
        private int _idUsuario
        {
            get { return (int)ViewState["idUsuario"]; }
            set { ViewState["idUsuario"] = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            BuscarUser();
        }

        private void BuscarUser()
        {
            Usuario oUsuario = new Usuario();
            UsuarioBus oUsuarioBus = new UsuarioBus();
            oUsuario = oUsuarioBus.UsuarioGetByUserName(txtUserName.Text);
            if (oUsuario != null)
            {
                if (oUsuario.Mail.Equals(txtMail.Text))
                    enviarMail(oUsuario);
                else
                    Mensaje.errorMsj("No coincide el mail del usuario. Contáctese con el Administrador.", this.Page, "ERROR", "../frmLogin.aspx");
            }
            else
                Mensaje.errorMsj("No existe usuario", this.Page, "ERROR", null);
        }

        private void enviarMail(Usuario oUsuario)
        {
            string nuevaContrasenia = "123456";
            UsuarioBus oUsuarioBus = new UsuarioBus();
            Utility oUtility = new Utility();
            oUsuario.Clave = oUtility.Encrypt(nuevaContrasenia);
            if (oUsuarioBus.UsuarioUpdate(oUsuario))
            {
                System.Text.StringBuilder message = new System.Text.StringBuilder();
                message.Append("Ud. se ha olvidado la contraseña. Desde el sistema se ha generado una nueva.<br>");
                message.Append("    USUARIO: " + oUsuario.UserName + "<br>");
                message.Append("    NUEVA CONTRASEÑA: " + nuevaContrasenia + "<br>");

                EnvioMail envioMail = new EnvioMail();
                envioMail.asunto = "Cambio Contraseña Procesos";
                envioMail.para = oUsuario.Mail;
                envioMail.cuerpo = message.ToString();
                envioMail.enviar();

                Mensaje.successMsj("Por favor verifique su casilla de correo.", this.Page, "Envio Correcto", "../frmLogin.aspx");
            }
        }


    }
}
