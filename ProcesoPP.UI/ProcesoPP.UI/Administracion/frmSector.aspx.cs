﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ProcesoPP.Business;
using ProcesoPP.Model;
using ProcesoPP.Services;

namespace ProcesoPP.UI.Administracion
{
    public partial class frmSector : System.Web.UI.Page
    {

        #region PROPIEDADES

        public Permisos _Permiso
        {
            get { return (Permisos)ViewState["permiso"]; }
            set { ViewState["permiso"] = value; }
        }

        public int _idUsuario
        {
            get { return (int)ViewState["idUsuario"]; }
            set { ViewState["idUsuario"] = value; }
        }

        #endregion

        #region EVENTOS

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                _idUsuario = Common.EvaluarSession(Request, this.Page);
                if (_idUsuario != 0)
                {
                    _Permiso = Common.getModulo(Request, this.Page);
                    if (_Permiso != null && _Permiso.Lectura)
                    {
                        Mensaje.ocultarDivMsj(divMensaje);
                        if (!IsPostBack)
                        {
                            InitScreen();
                        }
                    }
                    else
                    {
                        Mensaje.errorMsj("NO TIENE PERMISOS A ESTE FORMULARIO", this.Page, "SIN ACCESO", "../frmPrincipal.aspx");
                    }
                }
                else
                {
                    Mensaje.errorMsj("DEBE LOGUEARSE", this.Page, "LOGIN", "../frmLogin.aspx");
                }
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }
        
        protected void gvSector_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            gvSector.EditIndex = -1;
            CargarSectores();
        }

        protected void gvSector_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                Eliminar(gvSector.Rows[e.RowIndex]);
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        protected void gvSector_RowEditing(object sender, GridViewEditEventArgs e)
        {
            gvSector.EditIndex = e.NewEditIndex;
            CargarSectores();
            int id = int.Parse((gvSector.Rows[e.NewEditIndex].FindControl("lblIdEmpresa") as Label).Text);
            DropDownList ddlEmpreGV = gvSector.Rows[e.NewEditIndex].FindControl("ddlEmpresaI") as DropDownList;
            CargarEmpresas(ddlEmpreGV);
            ddlEmpreGV.SelectedValue = id.ToString();
        }

        protected void gvSector_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                Modificar(gvSector.Rows[e.RowIndex]);
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                Guardar();
                CargarSectores();
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        protected void gvSector_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Common.CargarRow(e, _Permiso);
            }
        } 

        #endregion

        #region METODOS

        private void InitScreen()
        {
            CargarSectores();
            CargarEmpresas(ddlEmpresa);
            ddlEmpresa.SelectedIndex = 0;
            if (!_Permiso.Escritura)
            {
                btnGuardar.Visible = false;
            }
        }

        private void Eliminar(GridViewRow row)
        {
            int id = int.Parse((row.FindControl("lblSectorI") as Label).Text);
            SectorBus oSectorBus = new SectorBus();

            if (oSectorBus.SectorDelete(id))
            {
                Mensaje.successMsj("Sector Eliminado con éxito.", this.Page, "Eliminado", "");
                CargarSectores();
            }
        }

        private void Modificar(GridViewRow row)
        {
            int id = int.Parse((row.FindControl("lblIdSector") as Label).Text);
            string descripcion = (row.FindControl("txtDescripcion") as TextBox).Text;
            int idEmpresa = int.Parse((row.FindControl("ddlEmpresaI") as DropDownList).SelectedValue);
            SectorBus oSectorBus = new SectorBus();
            Sector oSector = new Sector(id, descripcion, idEmpresa);

            if (oSectorBus.SectorUpdate(oSector))
            {
                Mensaje.successMsj("Modificado con éxito", this.Page, "Modificado", null);
                gvSector.EditIndex = -1;
                CargarSectores();
            }
        }

        private void Guardar()
        {
            SectorBus oSectorBus = new SectorBus();
            Sector oSector = new Sector(0, txtDescripcion.Text, int.Parse(ddlEmpresa.SelectedValue));
            oSector.IdSector = oSectorBus.SectorAdd(oSector);

            if (oSector.IdSector > 0)
            {
                Mensaje.successMsj("Guardado con éxito", this.Page, "Guardado", null);
                txtDescripcion.Text = "";
                ddlEmpresa.SelectedIndex = 0;
                CargarSectores();
            }
        }

        private void CargarSectores()
        {
            SectorBus oSectorBus = new SectorBus();
            gvSector.DataSource = oSectorBus.SectorGetAll();
            gvSector.DataBind();
        }

        private void CargarEmpresas(DropDownList ddl)
        {
            ddl.DataSource = null;
            EmpresaBus oEmpresaBus = new EmpresaBus();
            oEmpresaBus.CargarEmpresa(ref ddl, "N/A...");
        }

        #endregion

    }
}
