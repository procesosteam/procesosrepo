﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using ProcesoPP.Model;
using ProcesoPP.Business;
using ProcesoPP.Services;

namespace ProcesoPP.UI.Administracion
{
    public partial class frmUsuario : System.Web.UI.Page
    {
        #region PROPIEDADES

        public Permisos _Permiso
        {
            get { return (Permisos)ViewState["permiso"]; }
            set { ViewState["permiso"] = value; }
        }

        public int _idUsuario
        {
            get { return (int)ViewState["idUsuario"]; }
            set { ViewState["idUsuario"] = value; }
        }

        #endregion

        #region EVENTOS

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                 _idUsuario = Common.EvaluarSession(Request, this.Page);
                 if (_idUsuario != 0)
                 {
                     _Permiso = Common.getModulo(Request, this.Page);
                     if (_Permiso != null && _Permiso.Lectura)
                     {
                         Mensaje.ocultarDivMsj(divMensaje);
                         if (!IsPostBack)
                         {
                             CargarTipoUsuarios(ddlTipoUsuario);
                             CargarUsuarios();
                             InitScreen();
                         }
                     }
                     else
                     {
                         Mensaje.errorMsj("NO TIENE PERMISOS A ESTE FORMULARIO", this.Page, "SIN ACCESO", "../frmPrincipal.aspx");
                     }
                 }
                 else
                 {
                     Mensaje.errorMsj("DEBE LOGUEARSE", this.Page, "LOGIN", "../frmLogin.aspx");
                 }
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                Guardar();
                CargarUsuarios();
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        protected void gvTipoServicio_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            gvUsuario.EditIndex = -1;
            CargarUsuarios();
        }

        protected void gvUsuario_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                Eliminar(gvUsuario.Rows[e.RowIndex]);
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        protected void gvUsuario_RowEditing(object sender, GridViewEditEventArgs e)
        {
            gvUsuario.EditIndex = e.NewEditIndex;
            CargarUsuarios();

            DropDownList ddl = gvUsuario.Rows[e.NewEditIndex].FindControl("ddlTipoUsuarioGV") as DropDownList;
            HiddenField hfidTipoUsuario = gvUsuario.Rows[e.NewEditIndex].FindControl("hfidTipoUsuario") as HiddenField;
            CargarTipoUsuarios(ddl);
            ddl.SelectedValue = hfidTipoUsuario.Value;
        }

        protected void gvUsuario_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                Modificar(gvUsuario.Rows[e.RowIndex]);
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        protected void gvUsuario_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Common.CargarRow(e, _Permiso);
            }
        } 

        #endregion

        #region < METODOS >

        private void InitScreen()
        {
            if (!_Permiso.Escritura)
            {
                btnGuardar.Visible = false;
            }
        }

        private void Modificar(GridViewRow row)
        {
            int idUser = int.Parse((row.FindControl("lblIdUsuario") as Label).Text);
            string nombre = (row.FindControl("txtNombre") as TextBox).Text;
            string apellido = (row.FindControl("txtApellido") as TextBox).Text;
            string userName = (row.FindControl("txtUserName") as TextBox).Text;
            string mail = (row.FindControl("txtMail") as TextBox).Text;
            int idTipoUsuario = int.Parse((row.FindControl("ddlTipoUsuarioGV") as DropDownList).SelectedValue);

            UsuarioBus oUsuarioBus = new UsuarioBus();
            Usuario oUsuario = new Usuario();
            oUsuario = oUsuarioBus.UsuarioGetById(idUser);
            oUsuario.Nombre = nombre;
            oUsuario.Apellido = apellido;
            oUsuario.UserName = userName;
            oUsuario.oTipoUsuario.idTipoUsuario = idTipoUsuario;
            oUsuario.Mail = mail;

            if (oUsuarioBus.UsuarioUpdate(oUsuario))
            {
                Mensaje.successMsj("Usuario modificado con éxito.", this.Page, "Modificado", "");
                gvUsuario.EditIndex = -1;
                CargarUsuarios();                
            }
        }

        private void Eliminar(GridViewRow row)
        {
            int idUser = int.Parse((row.FindControl("lblIdUsuario") as Label).Text);
            UsuarioBus oUsuarioBus = new UsuarioBus();
            if (oUsuarioBus.UsuarioDelete(idUser))
            {
                Mensaje.successMsj("Usuario eliminado con éxito", this.Page, "Eliminado", null);
                CargarUsuarios();
            }
        }

        private void CargarTipoUsuarios(DropDownList ddl)
        {
            PermisosBus oPermisosBus = new PermisosBus();
            oPermisosBus.CargarTipoUsuarios(ref ddl, "Seleccione...");
        }

        private void CargarUsuarios()
        {
            UsuarioBus oUsuarioBus = new UsuarioBus();
            gvUsuario.DataSource = oUsuarioBus.UsuarioGetAll();
            gvUsuario.DataBind();
        }

        private void Guardar()
        {
            if (txtClave.Text == txtClaveRepetida.Text)
            {
                Utility oUtility = new Utility();
                UsuarioBus oUsuarioBus = new UsuarioBus();
                Usuario oUsuario = new Usuario();
                oUsuario.Nombre = txtNombre.Text;
                oUsuario.Apellido = txtApellido.Text;
                oUsuario.Telefono = null;
                oUsuario.Direccion = null; 
                oUsuario.Clave = oUtility.Encrypt(txtClave.Text);
                oUsuario.UserName = txtUserName.Text;
                oUsuario.oTipoUsuario = new TipoUsuario();
                oUsuario.oTipoUsuario.idTipoUsuario = int.Parse(ddlTipoUsuario.SelectedValue);
                oUsuario.Mail = txtMail.Text;

                if (!oUsuarioBus.VerificarUserName(oUsuario.UserName))
                {
                    if (oUsuarioBus.UsuarioAdd(oUsuario) > 0)
                    {
                        Mensaje.successMsj("Usuario guardado con éxito", this.Page, "Guardado", null);
                        CargarUsuarios();
                        LimpiarControles();
                    }
                }
                else
                {
                    Mensaje.errorMsj("El nombre de usuario ya existe.", this.Page, "ERROR", null);
                }
            }
            else
            {
                Mensaje.errorMsj("Debe ingresar las claves identicas.", this.Page, "Error", null);
            }
        }

        private void LimpiarControles()
        {            
            txtNombre.Text = string.Empty;
            txtApellido.Text = string.Empty;
            txtClave.Text = string.Empty;
            txtClaveRepetida.Text = string.Empty;
            txtUserName.Text = string.Empty;
            ddlTipoUsuario.SelectedIndex = 0;
        }

        #endregion

    }
}
