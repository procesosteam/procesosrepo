﻿<%@ Page Language="C#" AutoEventWireup="true"  MasterPageFile="~/frmPrincipal.Master" CodeBehind="frmTrailers.aspx.cs" Inherits="ProcesoPP.UI.Administracion.frmTrailers" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server"> 
<script type="text/javascript" >
    $(document).ready(function() { SetMenu("liCargaDatos"); }); 
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="apertura">Alta | Baja | Modificación de Equipos</div>
    <div class="tabla_admin">
        <div class="label_admint">    
            <label>Descripcion: </label></div> 
        <div class="label_adminc">      
            <asp:TextBox ID="txtDescripcion" runat="server" CssClass="sol_campoadm"></asp:TextBox></div>
        <div class="label_relleno20"></div>
        <div class="label_relleno20"></div>

        <div class="label_admint">      
            <label>Especificacion Tecnica</label></div>
        <div class="label_adminc">      
            <asp:DropDownList runat="server" ID="ddlEspecificacion" CssClass="sol_campoadm"></asp:DropDownList></div>        
            
        <div class="label_relleno20"></div>

          <div class="label_admintb">
            <asp:Button ID="btnGuardar" runat="server" Text="Guardar"  CssClass="btn_form"
                onclick="btnGuardar_Click" 
                OnClientClick="return ValidarObj('txtDescripcion|Ingrese una descripción.~ddlEspecificacion|Seleccione una especificación.');" />
        </div>
    </div>
    <div class="tabla3" id="divMensaje" runat="server">
         <asp:Label ID="lblError" CssClass="label40t" runat="server" Text="Error" Visible="false"></asp:Label>
     </div>
    <div>
         <asp:GridView ID="gvTrailer" runat="server" AutoGenerateColumns="False" 
             DataMember="idTrailer" CssClass="tabla3"
             onrowcancelingedit="gvTrailer_RowCancelingEdit" 
             onrowediting="gvTrailer_RowEditing" 
             onrowupdating="gvTrailer_RowUpdating" 
             onrowdeleting="gvTrailer_RowDeleting"
             OnRowDataBound="gvTrailer_RowDataBound">
             <Columns>
                 <asp:TemplateField HeaderText="Id Trailer" HeaderStyle-CssClass="celda30t" ItemStyle-CssClass="celda30" Visible="false">
                     <ItemTemplate>
                         <asp:Label ID="lblIdTrailer" runat="server" Text='<%# Bind("idTrailer") %>'></asp:Label>
                     </ItemTemplate>
                 </asp:TemplateField>
                 <asp:TemplateField HeaderText="Descripcion" HeaderStyle-CssClass="celda20t" ItemStyle-CssClass="celda20">
                     <EditItemTemplate>
                         <asp:TextBox ID="txtDescripcion" runat="server" Text='<%# Bind("Descripcion") %>'></asp:TextBox>
                     </EditItemTemplate>
                     <ItemTemplate>
                         <asp:Label ID="lblDescripcion" runat="server" Text='<%# Bind("Descripcion") %>'></asp:Label>
                     </ItemTemplate>
                 </asp:TemplateField>
                 <asp:TemplateField HeaderText="Esp. Técnica" HeaderStyle-CssClass="celda60t" ItemStyle-CssClass="celda60">
                     <EditItemTemplate>
                         <asp:DropDownList ID="ddlEspecificacion" runat="server"></asp:DropDownList>
                     </EditItemTemplate>
                     <ItemTemplate>
                        <asp:HiddenField runat="server" ID="hfidEspecificacion" Value='<%# Bind("oEspecificacion.idEspecificaciones") %>' />
                        <asp:Label ID="lblEspecificacion" runat="server" Text='<%# Bind("oEspecificacion.Descripcion") %>'></asp:Label>
                     </ItemTemplate>
                 </asp:TemplateField>
                 <asp:TemplateField ShowHeader="False" HeaderStyle-CssClass="celda20t" ItemStyle-CssClass="celda20">
                     <EditItemTemplate>
                         <asp:LinkButton ID="btnEditar" runat="server" CausesValidation="True" CssClass="boton40"
                             CommandName="Update" Text="Actualizar"></asp:LinkButton>
                         <asp:LinkButton ID="btnEliminar" runat="server" CausesValidation="False" CssClass="boton40"
                             CommandName="Cancel" Text="Cancelar"></asp:LinkButton>
                     </EditItemTemplate>
                     <ItemTemplate>
                         <asp:LinkButton ID="btnEditar" runat="server" CausesValidation="False" CssClass="boton40"
                             CommandName="Edit" Text="Editar" ></asp:LinkButton>
                         <asp:LinkButton ID="btnEliminar" runat="server" CausesValidation="False" CssClass="boton40"
                             CommandName="Delete" Text="Eliminar" OnClientClick="return Eliminar('el Trailer');"></asp:LinkButton>
                     </ItemTemplate>
                 </asp:TemplateField>
             </Columns>
         </asp:GridView>
     </div>
</asp:Content>
