﻿

<%@ Page Language="C#" MasterPageFile="~/frmPrincipal.Master" AutoEventWireup="true"
    CodeBehind="frmUsuario.aspx.cs" Inherits="ProcesoPP.UI.Administracion.frmUsuario"
    Title="Gestion de Usuarios" MaintainScrollPositionOnPostback="true" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server"> 
<script type="text/javascript" >
    $(document).ready(function() { SetMenu("liCargaDatos,liAdministracion"); }); 
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="apertura"> Alta | Baja | Modificación de Usuario</div>
    <div class="tabla_admin">
        <div class="label_admint">
            <label>Nombre:</label>
        </div>
        <div class="label_adminc">
            <asp:TextBox runat="server" CssClass="sol_campoadm" ID="txtNombre"></asp:TextBox></div>
        <div class="label_relleno20"></div>
        <div class="label_relleno20"></div>
        <div class="label_admint">
            <label>Apellido:</label>
        </div>
        <div class="label_adminc">
            <asp:TextBox runat="server" CssClass="sol_campoadm" ID="txtApellido" onblur="return proponerNombre('txtNombre', 'txtApellido', 'txtUserName');"></asp:TextBox></div>
        <div class="label_relleno20">
        </div>
        <div class="label_relleno20">
        </div>
        <div class="label_admint">
            <label>Nombre Usuario:</label>
        </div>
        <div class="label_adminc">
            <asp:TextBox runat="server" CssClass="sol_campoadm" ID="txtUserName"></asp:TextBox></div>
        <div class="label_relleno20">
        </div>
        <div class="label_relleno20">
        </div>
        <div class="label_admint">
            <label>Clave:</label>
        </div>
        <div class="label_adminc">
            <asp:TextBox runat="server" CssClass="sol_campoadm" ID="txtClaveRepetida" TextMode="Password"></asp:TextBox></div>
        <div class="label_relleno20">
        </div>
        <div class="label_relleno20">
        </div>
        <div class="label_admint">
            <label>Repita la Clave :</label>
        </div>
        <div class="label_adminc">
            <asp:TextBox runat="server" ID="txtClave" CssClass="sol_campoadm" TextMode="Password"
                onblur="return ValidarClave('txtClaveRepetida','txtClave')"></asp:TextBox></div>
        <div class="label_relleno20">
        </div>
        <div class="label_relleno20">
        </div>
        <div class="label_admint">
            <label>Mail:</label>
        </div>
        <div class="label_adminc">
            <asp:TextBox runat="server" ID="txtMail" CssClass="sol_campoadm"></asp:TextBox></div>        
        <div class="label_relleno20">
        </div>
         <div class="label_relleno20">
        </div>
        <div class="label_admint">
            <label>Tipo Usuario</label></div>
        <div class="label_adminc">
            <asp:DropDownList ID="ddlTipoUsuario" runat="server" CssClass="sol_campoadm">
            </asp:DropDownList>
        </div>
        <div class="label_relleno20">
        </div>
        <div class="label_admintb">
            <asp:Button runat="server" ID="btnGuardar" CssClass="btn_form" Text="Guardar" OnClientClick="return ValidarObj('txtNombre|Debe ingresar el Nombre.~txtApellido|Debe ingresar el Apellido.~txtUserName|Debe ingresar el nombre del usuario del sistema.~txtClaveRepetida|Debe ingresar una clave para validar.~txtClave|Debe repetir la clave ingresada.~ddlTipoUsuario|Debe Seleccionar el  Tipo Usuario.');"
                OnClick="btnGuardar_Click" /></div>
    </div>
    <div class="tabla3" id="divMensaje" runat="server">
        <asp:Label ID="lblError" CssClass="label40t" runat="server" Text="Error" Visible="false"></asp:Label>
    </div>
    <div>
    <asp:GridView ID="gvUsuario" runat="server" AutoGenerateColumns="False" DataMember="idTipoServicio"
        CssClass="tabla3" OnRowCancelingEdit="gvTipoServicio_RowCancelingEdit" OnRowEditing="gvUsuario_RowEditing"
        OnRowUpdating="gvUsuario_RowUpdating" OnRowDeleting="gvUsuario_RowDeleting"
             OnRowDataBound="gvUsuario_RowDataBound">
        <Columns>
            <asp:TemplateField HeaderText="Id Us."  Visible="false">
                <ItemTemplate>
                    <asp:Label ID="lblidUsuario" runat="server" Text='<%# Bind("idUsuario") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Nombre" HeaderStyle-CssClass="celda10t" ItemStyle-CssClass="celda10">
                <EditItemTemplate>
                    <asp:TextBox ID="txtNombre" runat="server" Text='<%# Bind("Nombre") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblNombre" runat="server" Text='<%# Bind("Nombre") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Apellido" HeaderStyle-CssClass="celda20t" ItemStyle-CssClass="celda20">
                <EditItemTemplate>
                    <asp:TextBox ID="txtApellido" runat="server" Text='<%# Bind("Apellido") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblApellidoI" runat="server" Text='<%# Bind("Apellido") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="User Name" HeaderStyle-CssClass="celda20t" ItemStyle-CssClass="celda20">
                <EditItemTemplate>
                    <asp:TextBox ID="txtUserName" runat="server" Text='<%# Bind("userName") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="lbluserNameI" runat="server" Text='<%# Bind("userName") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Mail" HeaderStyle-CssClass="celda30t" ItemStyle-CssClass="celda30">
                <EditItemTemplate>
                    <asp:TextBox ID="txtMail" runat="server" Text='<%# Bind("Mail") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblMailI" runat="server" Text='<%# Bind("Mail") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Tipo Usuario" HeaderStyle-CssClass="celda20t" ItemStyle-CssClass="celda20">
                <EditItemTemplate>
                    <asp:DropDownList ID="ddlTipoUsuarioGV" runat="server">
                    </asp:DropDownList>
                    <asp:HiddenField ID="hfidTipoUsuario" runat="server" Value='<%# Bind("oTipoUsuario.idTipoUsuario") %>'>
                    </asp:HiddenField>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblTipoUsuario" runat="server" Text='<%# Bind("oTipoUsuario.Descripcion") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField ShowHeader="False" HeaderStyle-CssClass="celda14t" ItemStyle-CssClass="celda14">
                <EditItemTemplate>
                    <asp:LinkButton ID="btnEditar" runat="server" CausesValidation="True" CssClass="boton40"
                        CommandName="Update" Text="Actualizar"></asp:LinkButton>
                    <asp:LinkButton ID="btnEliminar" runat="server" CausesValidation="False" CssClass="boton40"
                        CommandName="Cancel" Text="Cancelar"></asp:LinkButton>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:LinkButton ID="btnEditar" runat="server" CausesValidation="False" CssClass="boton40"
                        CommandName="Edit" Text="Editar"></asp:LinkButton>
                    <asp:LinkButton ID="btnEliminar" runat="server" CausesValidation="False" CssClass="boton40"
                        CommandName="Delete" Text="Eliminar" OnClientClick="return Eliminar('el Usuario');"></asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
    </div>
</asp:Content>
