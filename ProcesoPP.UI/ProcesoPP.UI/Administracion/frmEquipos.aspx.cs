﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ProcesoPP.Model;
using ProcesoPP.Business;
using ProcesoPP.Services;
using System.Text;

namespace ProcesoPP.UI.Administracion
{
    public partial class frmEquipos : System.Web.UI.Page
    {
        #region PROPIEDADES

        public Permisos _Permiso
        {
            get { return (Permisos)ViewState["permiso"]; }
            set { ViewState["permiso"] = value; }
        }

        public int _idUsuario
        {
            get { return (int)ViewState["idUsuario"]; }
            set { ViewState["idUsuario"] = value; }
        }

        #endregion

        #region EVENTOS

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                _idUsuario = Common.EvaluarSession(Request, this.Page);
                if (_idUsuario != 0)
                {
                    _Permiso = Common.getModulo(Request, this.Page);
                    if (_Permiso != null && _Permiso.Lectura)
                    {   if (!IsPostBack)
                        {
                            InitScren();
                        }
                    }
                    else
                    {
                        cambiarHidden("0");
                        Mensaje.errorMsj("NO TIENE PERMISOS A ESTE FORMULARIO", this.Page, "SIN ACCESO", "../frmPrincipal.aspx");
                    }
                }
                else
                {
                    cambiarHidden("0");
                    Mensaje.errorMsj("DEBE LOGUEARSE", this.Page, "LOGIN", "../frmLogin.aspx");
                }
            }
            catch (Exception ex)
            {
                cambiarHidden("0");
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                Guardar();
                CargarEquipos();
            }
            catch (Exception ex)
            {
                cambiarHidden("0");
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        protected void gvEquipo_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            gvEquipo.EditIndex = -1;
            CargarEquipos();
        }

        protected void gvEquipo_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                Eliminar(gvEquipo.Rows[e.RowIndex]);
            }
            catch (Exception ex)
            {
                cambiarHidden("0");
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        protected void gvEquipo_RowEditing(object sender, GridViewEditEventArgs e)
        {
            gvEquipo.EditIndex = e.NewEditIndex;
            CargarEquipos();
            DropDownList ddl = gvEquipo.Rows[e.NewEditIndex].FindControl("ddlEmpresa") as DropDownList;
            HiddenField HF = gvEquipo.Rows[e.NewEditIndex].FindControl("hfIdCliente") as HiddenField;
            
            CargarEmpreas(ddl);
            ddl.SelectedValue = HF.Value;
            cambiarHidden("1");
        }

        protected void gvEquipo_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                Modificar(gvEquipo.Rows[e.RowIndex]);
            }
            catch (Exception ex)
            {
                cambiarHidden("0");
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        protected void gvEquipo_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Common.CargarRow(e, _Permiso);
                string id = (e.Row.FindControl("hfIdCliente") as HiddenField).Value;
                DropDownList ddlE = e.Row.FindControl("ddlEmpresa") as DropDownList;
                CargarEmpreas(ddlE);
                ddlE.SelectedValue = id;
            }
        }
        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                CargarEquipos();
            }
            catch (Exception ex)
            {
                cambiarHidden("0");
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }
        #endregion

        #region METODOS

        private void InitScren()
        {
            if (!_Permiso.Escritura)
            {
                btnGuardar.Visible = false;
            }
            CargarEmpreas(ddlEmpresa);
            CargarEmpreas(ddlCliente);
            CargarEquipos();
            cambiarHidden("0");
        }


        private void cambiarHidden(string val)
        {
            hdidLastTab.Value = val;
            string hiddenField = hdidLastTab.Value;
            StringBuilder js = new StringBuilder();
            js.Append("<script type='text/javascript'>");
            js.Append("var previuslySelectedTab = ");
            js.Append(hiddenField);
            js.Append("</script>");
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "acttab", js.ToString());

        }
        private void CargarEmpreas(DropDownList ddl )
        {
            EmpresaBus oEmpresaBus = new EmpresaBus();
            oEmpresaBus.CargarEmpresa(ref ddl, "Seleccione...");
        }

        private void Eliminar(GridViewRow row)
        {
            int id = int.Parse((row.FindControl("lblEquipoI") as Label).Text);
            EquiposBus oEquiposBus = new EquiposBus();

            if (oEquiposBus.EquiposDelete(id))
            {
                Mensaje.successMsj("Equipo Eliminado con éxito.", this.Page, "Eliminado", "");
                CargarEquipos();
            }
        }

        private void Modificar(GridViewRow row)
        {
            int id = int.Parse((row.FindControl("lblIdEquipo") as Label).Text);
            string descripcion = (row.FindControl("txtDescripcion") as TextBox).Text;
            string idEmpresa = (row.FindControl("ddlEmpresa") as DropDownList).SelectedValue;
            EquiposBus oEquiposBus = new EquiposBus();
            Equipos oEquipos = new Equipos();
            oEquipos.IdEquipo = id;
            oEquipos.Descripcion = descripcion;
            oEquipos.idCliente = int.Parse(idEmpresa);

            if (oEquiposBus.EquiposUpdate(oEquipos))
            {
                Mensaje.successMsj("Modificado con Exito!", this.Page, "Modificado", null);
                gvEquipo.EditIndex = -1;
                CargarEquipos();
            }
        }

        private void Guardar()
        {
            EquiposBus oEquiposBus = new EquiposBus();
            Equipos oEquipos = new Equipos();
            oEquipos.Descripcion = txtDescripcion.Text;
            oEquipos.idCliente = int.Parse(ddlEmpresa.SelectedValue);
            oEquipos.IdEquipo = oEquiposBus.EquiposAdd(oEquipos);

            if (oEquipos.IdEquipo > 0)
            {
                Mensaje.successMsj("Equipo guardado con Exito", this.Page, "Agregado", null);
                txtDescripcion.Text = "";
                ddlEmpresa.SelectedIndex = 0;
                CargarEquipos();
            }
        }

        private void CargarEquipos()
        {
            EquiposBus oEquiposBus = new EquiposBus();
            gvEquipo.DataSource = oEquiposBus.EquiposGetByIdCliente(int.Parse(ddlCliente.SelectedValue));
            gvEquipo.DataBind();
            cambiarHidden("0");
        }

        #endregion

    }
}
