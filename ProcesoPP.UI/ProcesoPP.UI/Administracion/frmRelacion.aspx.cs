﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ProcesoPP.Model;
using ProcesoPP.Business;
using ProcesoPP.Services;

namespace ProcesoPP.UI.Administracion
{
    public partial class frmRelacion : System.Web.UI.Page
    {
        #region PROPIEDADES

        public Permisos _Permiso
        {
            get { return (Permisos)ViewState["permiso"]; }
            set { ViewState["permiso"] = value; }
        }

        public int _idUsuario
        {
            get { return (int)ViewState["idUsuario"]; }
            set { ViewState["idUsuario"] = value; }
        }

        #endregion
        
        #region EVENTOS

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                _idUsuario = Common.EvaluarSession(Request, this.Page);
                if (_idUsuario != 0)
                {
                    _Permiso = Common.getModulo(Request, this.Page);
                    if (_Permiso != null && _Permiso.Lectura)
                    {
                        Mensaje.ocultarDivMsj(divMensaje);
                        if (!IsPostBack)
                        {
                            CargarRelaciones();
                            InitScrren();
                        }
                    }
                    else
                    {
                        Mensaje.errorMsj("NO TIENE PERMISOS A ESTE FORMULARIO", this.Page, "SIN ACCESO", "../frmPrincipal.aspx");
                    }
                }
                else
                {
                    Mensaje.errorMsj("DEBE LOGUEARSE", this.Page, "LOGIN", "../frmLogin.aspx");
                }
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                Guardar();
                CargarRelaciones();
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        protected void gvRelacion_RowEditing(object sender, GridViewEditEventArgs e)
        {
            gvRelacion.EditIndex = e.NewEditIndex;
            CargarRelaciones();
        }

        protected void gvRelacion_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            gvRelacion.EditIndex = -1;
            CargarRelaciones();
        }

        protected void gvRelacion_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                Modificar(gvRelacion.Rows[e.RowIndex]);
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        protected void gvRelacion_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                Eliminar(gvRelacion.Rows[e.RowIndex]);
            }
            catch (Exception ex)
            {                
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        protected void gvRelacion_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Common.CargarRow(e, _Permiso);
            }
        }

        #endregion

        #region Metodos

        private void InitScrren()
        {
            if (!_Permiso.Escritura)
            {
                btnGuardar.Visible = false;
            }
        }

        private void Eliminar(GridViewRow row)
        {
           
            int id = int.Parse((row.FindControl("lblRelacionI") as Label).Text);
            RelacionBus oRelacionBus = new RelacionBus();

            if (oRelacionBus.RelacionDelete(id))
            {
                Mensaje.successMsj("Relación Eliminada con éxito.", this.Page, "Eliminado", "");
                CargarRelaciones();
            }
        }
         
        private void Modificar(GridViewRow row)
        {
            int id = int.Parse((row.FindControl("lblIdRelacion") as Label).Text);
            string descripcion = (row.FindControl("txtDescripcion") as TextBox).Text;
            RelacionBus oRelacionBus = new RelacionBus();
            Relacion oRelacion = new Relacion(id, descripcion);

            if (oRelacionBus.RelacionUpdate(oRelacion))
            {
                Mensaje.successMsj("Modificado con Exito!", this.Page, "Modificado", null);
                gvRelacion.EditIndex = -1;
                CargarRelaciones();
            }
        }

        private void Guardar()
        {
            RelacionBus oRelacionBus = new RelacionBus();
            Relacion oRelacion = new Relacion(0, txtRelacion.Text);
            oRelacion.IdRelacion = oRelacionBus.RelacionAdd(oRelacion);

            if (oRelacion.IdRelacion > 0)
            {
                Mensaje.successMsj("Guardado con Exito!", this.Page, "Agregado", null);
                txtRelacion.Text = "";
                CargarRelaciones();
            }
        }

        private void CargarRelaciones()
        {
            RelacionBus oRelacionBus = new RelacionBus();
            gvRelacion.DataSource = oRelacionBus.RelacionGetAll();
            gvRelacion.DataBind();
        }
                
        #endregion

           
    }
}

    