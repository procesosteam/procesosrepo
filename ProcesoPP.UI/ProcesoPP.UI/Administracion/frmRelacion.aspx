﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="frmRelacion.aspx.cs" Inherits="ProcesoPP.UI.Administracion.frmRelacion" MasterPageFile="~/frmPrincipal.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<script type="text/javascript" >
    $(document).ready(function() { SetMenu("liCargaDatos,liCarga"); }); 
</script>
</asp:Content>
<asp:Content ID="content" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="apertura">ALTA | BAJA | MODIFICACION DE RELACION</div>
    <div class="tabla_admin">
    <div class="label_admint"><label>Relación: </label></div> 
    <div class="label_adminc"> <asp:TextBox ID="txtRelacion" CssClass="sol_campoadm" runat="server"></asp:TextBox></div>
    
          <div class="label_admintb">
        <asp:Button ID="btnGuardar" runat="server" Text="Guardar"  CssClass="btn_form"        
            onclick="btnGuardar_Click" OnClientClick="return ValidarObj('txtRelacion|Ingrese una Descripción.');" />
       </div>
       
      <div class="label_relleno20"></div>
    </div>
    <div id="divMensaje" runat="server">
         <asp:Label ID="lblError" runat="server" Text="Error" Visible="false"></asp:Label>
     </div>
    <div>
         <asp:GridView ID="gvRelacion" runat="server" AutoGenerateColumns="False" 
             DataMember="idRelacion" CssClass="tabla3"
             onrowcancelingedit="gvRelacion_RowCancelingEdit" 
             onrowediting="gvRelacion_RowEditing" 
             onrowupdating="gvRelacion_RowUpdating" 
             onrowdeleting="gvRelacion_RowDeleting"
             OnRowDataBound="gvRelacion_RowDataBound">
             <Columns>
                 <asp:TemplateField HeaderText="Id Relacion" HeaderStyle-CssClass="celda80" ItemStyle-CssClass="celda80t" Visible="false">
                     <EditItemTemplate>
                         <asp:Label ID="lblIdRelacion" runat="server" Text='<%# Bind("idRelacion") %>' ></asp:Label>
                     </EditItemTemplate>
                     <ItemTemplate>
                         <asp:Label ID="lblRelacionI" runat="server" Text='<%# Bind("idRelacion") %>'></asp:Label>
                     </ItemTemplate>
                 </asp:TemplateField>
                 <asp:TemplateField HeaderText="Descripción" HeaderStyle-CssClass="celda_admint" ItemStyle-CssClass="celda_admintt">
                     <EditItemTemplate>
                         <asp:TextBox ID="txtDescripcion" runat="server" Text='<%# Bind("Descripcion") %>'></asp:TextBox>
                     </EditItemTemplate>
                     <ItemTemplate>
                         <asp:Label ID="lblDescripcion" runat="server" Text='<%# Bind("Descripcion") %>'></asp:Label>
                     </ItemTemplate>
                 </asp:TemplateField>
                 <asp:TemplateField ShowHeader="False" HeaderStyle-CssClass="celda15t" ItemStyle-CssClass="celda15b">
                     <EditItemTemplate>
                         <asp:LinkButton ID="btnEditar" runat="server" CausesValidation="True" CssClass="boton40"
                             CommandName="Update" Text="Actualizar"></asp:LinkButton>
                         <asp:LinkButton ID="btnEliminar" runat="server" CausesValidation="False" CssClass="boton40"
                             CommandName="Cancel" Text="Cancelar"></asp:LinkButton>
                     </EditItemTemplate>
                     <ItemTemplate>
                         <asp:LinkButton ID="btnEditar" runat="server" CausesValidation="False"  CssClass="boton40"
                             CommandName="Edit" Text="Editar" ></asp:LinkButton>
                        <asp:LinkButton ID="btnEliminar" runat="server" CausesValidation="False"  CssClass="boton40"
                             CommandName="Delete" Text="Eliminar" OnClientClick="return Eliminar('la Relación');"></asp:LinkButton>
                     </ItemTemplate>
                 </asp:TemplateField>
             </Columns>
         </asp:GridView>
     </div>
</asp:Content>
