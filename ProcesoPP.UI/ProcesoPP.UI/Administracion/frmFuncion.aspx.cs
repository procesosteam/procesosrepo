﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ProcesoPP.Model;
using ProcesoPP.Business;
using ProcesoPP.Services;

namespace ProcesoPP.UI.Administracion
{
    public partial class frmFuncion : System.Web.UI.Page
    {
        #region PROPIEDADES

        public Permisos _Permiso
        {
            get { return (Permisos)ViewState["permiso"]; }
            set { ViewState["permiso"] = value; }
        }

        public int _idUsuario
        {
            get { return (int)ViewState["idUsuario"]; }
            set { ViewState["idUsuario"] = value; }
        }

        #endregion

        #region EVENTOS

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                _idUsuario = Common.EvaluarSession(Request, this.Page);
                if (_idUsuario != 0)
                {
                    _Permiso = Common.getModulo(Request, this.Page);
                    if (_Permiso != null && _Permiso.Lectura)
                    {
                        Mensaje.ocultarDivMsj(divMensaje);
                        if (!IsPostBack)
                        {
                            CargarFunciones();
                            InitScrren();
                        }
                    }
                    else
                    {
                        Mensaje.errorMsj("NO TIENE PERMISOS A ESTE FORMULARIO", this.Page, "SIN ACCESO", "../frmPrincipal.aspx");
                    }
                }
                else
                {
                    Mensaje.errorMsj("DEBE LOGUEARSE", this.Page, "LOGIN", "../frmLogin.aspx");
                }
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                Guardar();
                CargarFunciones();
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        protected void gvFuncion_RowEditing(object sender, GridViewEditEventArgs e)
        {
            gvFuncion.EditIndex = e.NewEditIndex;
            CargarFunciones();
        }

        protected void gvFuncion_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            gvFuncion.EditIndex = -1;
            CargarFunciones();
        }

        protected void gvFuncion_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                Modificar(gvFuncion.Rows[e.RowIndex]);
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        protected void gvFuncion_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                Eliminar(gvFuncion.Rows[e.RowIndex]);
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        protected void gvFuncion_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Common.CargarRow(e, _Permiso);
            }
        }

        #endregion

        #region Metodos

        private void InitScrren()
        {
            if (!_Permiso.Escritura)
            {
                btnGuardar.Visible = false;
            }
        }

        private void Eliminar(GridViewRow row)
        {
            int id = int.Parse((row.FindControl("lblFuncionI") as Label).Text);
            FuncionBus oFuncionBus = new FuncionBus();

            if (oFuncionBus.FuncionDelete(id))
            {
                Mensaje.successMsj("Función Eliminada con éxito.", this.Page, "Eliminado", "");
                CargarFunciones();
            }
        }

        private void Modificar(GridViewRow row)
        {
            int id = int.Parse((row.FindControl("lblIdFuncion") as Label).Text);
            string descripcion = (row.FindControl("txtDescripcion") as TextBox).Text;
            FuncionBus oFuncionBus = new FuncionBus();
            Funcion oFuncion = new Funcion(id, descripcion);

            if (oFuncionBus.FuncionUpdate(oFuncion))
            {
                Mensaje.successMsj("Modificado con Exito!", this.Page, "Modificado", null);
                gvFuncion.EditIndex = -1;
                CargarFunciones();
            }
        }

        private void Guardar()
        {
            FuncionBus oFuncionBus = new FuncionBus();
            Funcion oFuncion = new Funcion(0, txtFuncion.Text);
            oFuncion.IdFuncion = oFuncionBus.FuncionAdd(oFuncion);

            if (oFuncion.IdFuncion > 0)
            {
                Mensaje.successMsj("Guardado con Exito!", this.Page, "Agregado", null);
                txtFuncion.Text = "";
                CargarFunciones();
            }
        }

        private void CargarFunciones()
        {
            FuncionBus oFuncionBus = new FuncionBus();
            gvFuncion.DataSource = oFuncionBus.FuncionGetAll();
            gvFuncion.DataBind();
        }

        #endregion


    }
}
