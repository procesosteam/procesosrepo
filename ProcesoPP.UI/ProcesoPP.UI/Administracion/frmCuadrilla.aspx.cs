﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ProcesoPP.Services;
using ProcesoPP.Business;
using ProcesoPP.Model;
using System.Text;
using AjaxControlToolkit;
using System.Web.Services;
using Newtonsoft.Json;

namespace ProcesoPP.UI.Administracion
{
    public partial class frmCuadrilla : System.Web.UI.Page
    {
        #region PROPIEDADES

        public Permisos _Permiso
        {
            get { return (Permisos)ViewState["permiso"]; }
            set { ViewState["permiso"] = value; }
        }

        public int _idUsuario
        {
            get { return (int)ViewState["idUsuario"]; }
            set { ViewState["idUsuario"] = value; }
        }


        public int _idCuadrilla
        {
            get { return (int)ViewState["idCuadrilla"]; }
            set { ViewState["idCuadrilla"] = value; }
        }

        #endregion

        #region EVENTOS

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                _idUsuario = Common.EvaluarSession(Request, this.Page);
                if (_idUsuario != 0)
                {
                    _Permiso = Common.getModulo(Request, this.Page);
                    if (_Permiso != null && _Permiso.Lectura)
                    {                        
                        if (!IsPostBack)
                        {
                            InitScren();                            
                        }
                        
                    }
                    else
                    {
                        Mensaje.errorMsj("NO TIENE PERMISOS A ESTE FORMULARIO", this.Page, "SIN ACCESO", "../frmPrincipal.aspx");
                    }
                }
                else
                {
                    Mensaje.errorMsj("DEBE LOGUEARSE", this.Page, "LOGIN", "../frmLogin.aspx");
                }
            }
            catch (Exception ex)
            {
                cambiarHidden("0");
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null); ;
            }
        }

        protected void btnCancelar_Click(object sender, EventArgs e)
        {
            LimpiarControles();
        }

        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                Guardar();
            }
            catch (Exception ex)
            {
                cambiarHidden("0");
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null); ;
            }

        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                CargarCuadrillas(txtBusqueda.Text);
            }
            catch (Exception ex)
            {
                cambiarHidden("0");
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null); ;
            }
        }


        protected void ddlSector_SelectedIndexChanged(object sender, EventArgs e)
        {
            CargarPersonal(ddlSector.SelectedValue, ddlEmpresa.SelectedValue);
            cambiarHidden("1");
        }

        protected void ddlEmpresa_SelectedIndexChanged(object sender, EventArgs e)
        {
            CargarSector(ddlEmpresa.SelectedValue);
            CargarPersonal("0", ddlEmpresa.SelectedValue);
            cambiarHidden("1");            
        }

        protected void gvTipoServicio_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                Eliminar(gvCuadrilla.Rows[e.RowIndex]);
            }
            catch (Exception ex)
            {
                cambiarHidden("0");
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        protected void gvTipoServicio_RowEditing(object sender, GridViewEditEventArgs e)
        {
            try
            {
                MostrarCuadrilla(e.NewEditIndex);
            }
            catch (Exception ex)
            {
                cambiarHidden("0");
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        protected void gvTipoServicio_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Common.CargarRow(e, _Permiso);
            }
        }

        protected void gvPersonal_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Common.CargarRow(e, _Permiso);

                if ( typeof(Model.CuadrillaPersonal) == e.Row.DataItem.GetType())
                {
                    mostrarCuadrillaPersonalRow(e.Row);
                }
                if (typeof(Model.Personal) == e.Row.DataItem.GetType())
                {
                    mostrarPersonalRow(e.Row);
                }
            }
        }

        #endregion

        #region METODOS

        private void cambiarHidden(string val)
        {
            hdidLastTab.Value = val;
            string hiddenField = hdidLastTab.Value;
            StringBuilder js = new StringBuilder();
            js.Append("<script type='text/javascript'>");
            js.Append("cambiarTab(");
            js.Append(hiddenField);
            js.Append(");</script>");
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "acttab", js.ToString());

        }
        private void InitScren()
        {
            _idCuadrilla = 0;
            if (Request["idCuadrilla"] != null)
                _idCuadrilla = int.Parse(Request["idCuadrilla"]);
                        
            CargarEmpresa();
            ddlSector.Items.Insert(0, new System.Web.UI.WebControls.ListItem("N/A", "0"));
            CargarCuadrillas(string.Empty);

            if (_idCuadrilla > 0)
            {
                CargarCuadrillaID(_idCuadrilla);
            }

            if (!_Permiso.Escritura)
            {
                btnGuardar.Visible = false;
            }

            if (Request["Enabled"] != null)
            {
                if (bool.Parse(Request["Enabled"]) == false)
                {
                    btnGuardar.Visible = false;
                    Common.AnularControles(this.Form);
                }
            }
        }

        #region COMBOSAJAX

        [WebMethod]
        public static CascadingDropDownNameValue[] GetEmpresa(string knownCategoryValues, string category)
        {
            EmpresaBus oEmpresaBus = new EmpresaBus();
            List<CascadingDropDownNameValue> empresa = oEmpresaBus.GetDataEmpresaInteras();
            return empresa.ToArray();
        }

        [WebMethod]
        public static CascadingDropDownNameValue[] GetSector(string knownCategoryValues, string category)
        {
            string idEmpresa = CascadingDropDown.ParseKnownCategoryValuesString(knownCategoryValues)["idEmpresa"];
            SectorBus oSectorBus = new SectorBus();
            List<CascadingDropDownNameValue> sectores = oSectorBus.GetDataSector(int.Parse(idEmpresa));
            return sectores.ToArray();
        }

        #endregion


        private void CargarSector(string idEmpresa)
        {
            if (!idEmpresa.Equals(string.Empty) && !idEmpresa.Equals("0"))
            {
                SectorBus oSector = new SectorBus();
                oSector.CargarSectorPorEmpresa(ref ddlSector, int.Parse(idEmpresa));
            }
            else
            {
                ddlSector.Items.Clear();
                ddlSector.Items.Insert(0, new System.Web.UI.WebControls.ListItem("N/A", "0"));
            }
        }

        private void CargarEmpresa()
        {
            EmpresaBus oEmpresaBus = new EmpresaBus();
            oEmpresaBus.CargarEmpresaInterna(ref ddlEmpresa, "N/A");

        }

        private void CargarPersonal(string idSector, string idEmpresa)
        {
            idSector = idSector.Equals(string.Empty) ? "0" : idSector;
            idEmpresa = idEmpresa.Equals(string.Empty) ? "0" : idEmpresa;

            PersonalBus oPersonalBus = new PersonalBus();
            gvPersonal.DataSource = oPersonalBus.PersonalGetByIdSectorEmpresa(idSector, idEmpresa);
            gvPersonal.DataBind();

            UpdatePanel1.Update();
        }

        private void Eliminar(GridViewRow row)
        {
            int id = int.Parse((row.FindControl("lblCuadrillaI") as Label).Text);
            CuadrillaBus oCuadrillaBus = new CuadrillaBus();

            if (oCuadrillaBus.CuadrillaDelete(id))
            {
                Mensaje.successMsj("Cuadrilla Eliminada con éxito.", this.Page, "Eliminado", "");
                CargarCuadrillas("");
            }
        }

        private void MostrarCuadrilla(int rowIndex)
        {
            LimpiarControles();
            GridViewRow row = gvCuadrilla.Rows[rowIndex];
            int id = int.Parse((row.FindControl("lblCuadrillaI") as Label).Text);
            CargarCuadrillaID(id);

        }

        private void CargarCuadrillaID(int id)
        {
            CuadrillaBus oCuadrillaBus = new CuadrillaBus();
            Cuadrilla oCuadrilla = oCuadrillaBus.CuadrillaGetById(id);
            _idCuadrilla = id;
            txtNombre.Text = oCuadrilla.Nombre;
            txtFecha.Text = oCuadrilla.FechaAlta.ToString("dd/MM/yyyy");
            CargarEmpresa();
            ddlEmpresa.SelectedValue = oCuadrilla.idEmpresa.ToString();
            CargarSector(ddlEmpresa.SelectedValue);
            ddlSector.SelectedValue = oCuadrilla.idSector.ToString();
            CargarPersonal(oCuadrilla.idSector.ToString(), oCuadrilla.idEmpresa.ToString());
            foreach (GridViewRow item in gvPersonal.Rows)
            {
                CheckBox chkPersonal = item.FindControl("chkPersonal") as CheckBox;
                HiddenField hfIdPersonal = item.FindControl("hfIdPersonal") as HiddenField;
                TextBox txtFechaAlta = item.FindControl("txtFechaAlta") as TextBox;
                TextBox txtFechaBaja = item.FindControl("txtFechaBaja") as TextBox;
                if (oCuadrilla.oListCuadrillaPersonal.Exists(p => p.IdPersonal == int.Parse(hfIdPersonal.Value)))
                {
                    CuadrillaPersonal cp = oCuadrilla.oListCuadrillaPersonal.Find(p => p.IdPersonal == int.Parse(hfIdPersonal.Value));
                    if (cp.FechaBaja == DateTime.MinValue)
                        chkPersonal.Checked = true;
                    else
                        chkPersonal.Checked = false;
                    txtFechaAlta.Text = cp.FechaActivo.ToString("dd/MM/yyyy");
                    txtFechaBaja.Text = cp.FechaBaja == DateTime.MinValue ? string.Empty : cp.FechaBaja.ToString("dd/MM/yyyy");
                }
            }

            cambiarHidden("1");
        }
        
        private void mostrarPersonalRow(GridViewRow item)
        {
            Model.Personal per = (Model.Personal)item.DataItem;
            Label lblNombre = item.FindControl("lblNombre") as Label;
            lblNombre.Text = per.Apellido;
        }


        private void mostrarCuadrillaPersonalRow(GridViewRow item)
        {
            CuadrillaPersonal cp = (CuadrillaPersonal)item.DataItem;
            TextBox txtFechaAlta = item.FindControl("txtFechaAlta") as TextBox;
            TextBox txtFechaBaja = item.FindControl("txtFechaBaja") as TextBox;
            Label lblNombre = item.FindControl("lblNombre") as Label;
            CheckBox chkPersonal = item.FindControl("chkPersonal") as CheckBox;

            chkPersonal.Checked = cp.Activo;
            lblNombre.Text = cp.oPersona.Apellido + ", " + cp.oPersona.Nombres;
            txtFechaAlta.Text = cp.FechaActivo.ToString("dd/MM/yyyy");
            txtFechaBaja.Text = cp.FechaBaja== DateTime.MinValue ? string.Empty : cp.FechaBaja.ToString("dd/MM/yyyy");
        }


        private void Guardar()
        {
            CuadrillaBus oCuadrillaBus = new CuadrillaBus();
            Cuadrilla oCuadrilla = new Cuadrilla();
            if (_idCuadrilla >0)
            {
                oCuadrilla = oCuadrillaBus.CuadrillaGetById(_idCuadrilla);
            }

            oCuadrilla.Nombre = txtNombre.Text;
            oCuadrilla.FechaAlta = DateTime.Parse(txtFecha.Text);
            oCuadrilla.Baja = false;
            oCuadrilla.IdUsuario = _idUsuario;
            oCuadrilla.idEmpresa = ddlEmpresa.SelectedValue.Equals(string.Empty) ? 0 : int.Parse(ddlEmpresa.SelectedValue);
            oCuadrilla.idSector = ddlSector.SelectedValue.Equals(string.Empty) ? 0 :  int.Parse(ddlSector.SelectedValue);

            oCuadrilla.oListCuadrillaPersonal = CargarCuadrillaPersonal(oCuadrilla.oListCuadrillaPersonal);
            if (_idCuadrilla == 0)
            {
                oCuadrilla.IdCuadrilla = oCuadrillaBus.CuadrillaAdd(oCuadrilla);
            }
            else
            {
                oCuadrilla.IdCuadrilla = _idCuadrilla;
                oCuadrillaBus.CuadrillaUpdate(oCuadrilla);
            }

            if (oCuadrilla.IdCuadrilla > 0)
            {
                Mensaje.successMsj("Cuadrilla guardada con éxito", this.Page, "Guardado", null);
                LimpiarControles();
                CargarCuadrillas(txtBusqueda.Text);
            }
        }

        private List<CuadrillaPersonal> CargarCuadrillaPersonal(List<CuadrillaPersonal> oList)
        {
            CuadrillaPersonal oCPersona = new CuadrillaPersonal();
            foreach (GridViewRow item in gvPersonal.Rows)
            {
                string idPersona = (item.FindControl("hfIdPersonal") as HiddenField).Value;
                CheckBox chk = item.FindControl("chkPersonal") as CheckBox;
                TextBox txtFechaAlta = item.FindControl("txtFechaAlta") as TextBox;
                TextBox txtFechaBaja = item.FindControl("txtFechaBaja") as TextBox;

                if (oList.Exists(l => l.IdPersonal == int.Parse(idPersona)))
                {
                    oCPersona = oList.First(l => l.IdPersonal == int.Parse(idPersona));

                    if (!chk.Checked)
                    {
                        if (!txtFechaBaja.Text.Equals(string.Empty))
                        {
                            oCPersona = cargarPersonaOBJ(oCPersona, idPersona, txtFechaAlta, txtFechaBaja, chk);
                        }
                        else
                        {
                            throw new Exception("Si una persona se desactiva, debe ingresar fecha de baja.");
                        }
                    }
                    else
                    {

                        if (!txtFechaAlta.Text.Equals(string.Empty))
                        {
                            if (oCPersona.FechaActivo != DateTime.Parse(txtFechaAlta.Text))
                            {
                                oCPersona = new CuadrillaPersonal();
                                oCPersona = cargarPersonaOBJ(oCPersona, idPersona, txtFechaAlta, txtFechaBaja, chk);
                                oList.Add(oCPersona);
                            }
                        }
                        else
                        {
                            throw new Exception("Debe seleccionar fecha de alta de la persona");
                        }
                    }
                }
                else
                {
                    if (chk.Checked)
                    {
                        if (!txtFechaAlta.Text.Equals(string.Empty))
                        {
                            oCPersona = new CuadrillaPersonal();
                            oCPersona = cargarPersonaOBJ(oCPersona, idPersona, txtFechaAlta, txtFechaBaja, chk);
                            oList.Add(oCPersona);
                        }
                        else
                        {
                            // break;
                            throw new Exception("Debe seleccionar fecha de alta de la persona");
                        }
                    }
                }
                
            }
            return oList;
        }

        private CuadrillaPersonal cargarPersonaOBJ(CuadrillaPersonal oCPersona,string idPersona, TextBox txtFechaAlta, TextBox txtFechaBaja, CheckBox chk)
        {            
            oCPersona.IdPersonal = int.Parse(idPersona);
            oCPersona.FechaActivo = txtFechaAlta.Text.Equals(string.Empty) ? DateTime.MinValue : DateTime.Parse(txtFechaAlta.Text);
            oCPersona.FechaBaja = txtFechaBaja.Text.Equals(string.Empty) ? DateTime.MinValue : DateTime.Parse(txtFechaBaja.Text);
            oCPersona.Activo = chk.Checked;
            return oCPersona;
        }

        private void LimpiarControles()
        {
            txtNombre.Text = string.Empty;
            txtFecha.Text = string.Empty;
            txtBusqueda.Text = string.Empty;
            ddlEmpresa.SelectedIndex = 0;
            ddlSector.SelectedIndex = 0;
            //cddSector.SelectedValue = "0";
            //cddEmpresa.SelectedValue = "0";
            gvPersonal.DataSource = null;
            gvPersonal.DataBind();
            _idCuadrilla = 0;
            cambiarHidden("0");
        }

        private void CargarCuadrillas(string nombre)
        {
            CuadrillaBus oCuadrillaBus = new CuadrillaBus();
            gvCuadrilla.DataSource = oCuadrillaBus.CuadrillaGetByFilter(nombre);
            gvCuadrilla.DataBind();

            cambiarHidden("0");
        }

        #endregion

    }
}