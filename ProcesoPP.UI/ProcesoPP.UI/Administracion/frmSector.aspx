﻿<%@ Page Title="" Language="C#" MasterPageFile="~/frmPrincipal.Master" MaintainScrollPositionOnPostback="true" AutoEventWireup="true" CodeBehind="frmSector.aspx.cs" Inherits="ProcesoPP.UI.Administracion.frmSector" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server"> 
<script type="text/javascript" >
    $(document).ready(function() { SetMenu("liCargaDatos"); }); 
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div id="apertura">Alta | Baja | Modificación de Sectores</div>
<div class="tabla_admin">
    <div class="label_admint"><label>Sector </label></div>     
    <div class="label_adminc"><asp:TextBox ID="txtDescripcion" CssClass="sol_campoadm" runat="server"></asp:TextBox></div>    
    <div class="label_relleno20"></div>
    <div class="label_relleno20"></div>
    
    <div class="label_admint"><label>Empresa </label></div> 
    <div class="label_adminc"><asp:DropDownList ID="ddlEmpresa" CssClass="sol_campoadm" runat="server"></asp:DropDownList> </div>     
    <div class="label_relleno20"></div>
    <div class="label_admintb"><asp:Button ID="btnGuardar" runat="server" Text="Guardar" CssClass="btn_form" onclick="btnGuardar_Click" OnClientClick="return ValidarObj('txtDescripcion|Seleccione una Descripción.');" /></div>
</div>
<div class="tabla3" id="divMensaje" runat="server">
     <asp:Label ID="lblError" runat="server" CssClass="celda40t" Text="Error" Visible="false"></asp:Label>
 </div>
<div>
         <asp:GridView ID="gvSector" runat="server" AutoGenerateColumns="False" 
             DataMember="idSector" CssClass="tabla3"
             onrowcancelingedit="gvSector_RowCancelingEdit" 
             onrowediting="gvSector_RowEditing" 
             onrowupdating="gvSector_RowUpdating" 
             onrowdeleting="gvSector_RowDeleting"
             OnRowDataBound="gvSector_RowDataBound">
             <Columns>
                 <asp:TemplateField HeaderText="Id Sector" HeaderStyle-CssClass="celda_admint" ItemStyle-CssClass="celda_tiposerv2" Visible="false">
                     <EditItemTemplate>
                         <asp:Label ID="lblIdSector" runat="server" Text='<%# Bind("idSector") %>'></asp:Label>
                     </EditItemTemplate>
                     <ItemTemplate>
                         <asp:Label ID="lblSectorI" runat="server" Text='<%# Bind("idSector") %>'></asp:Label>
                     </ItemTemplate>
                 </asp:TemplateField>
                 <asp:TemplateField HeaderText="DESCRIPCION" HeaderStyle-CssClass="celda50t" ItemStyle-CssClass="celda50">
                     <EditItemTemplate>
                         <asp:TextBox ID="txtDescripcion" runat="server" Text='<%# Bind("Descripcion") %>'></asp:TextBox>
                     </EditItemTemplate>
                     <ItemTemplate>                     
                         <asp:Label ID="lblDescripcion" runat="server" Text='<%# Bind("Descripcion") %>'></asp:Label>
                     </ItemTemplate>
                 </asp:TemplateField>
                  <asp:TemplateField HeaderText="EMPRESA" HeaderStyle-CssClass="celda35t" ItemStyle-CssClass="celda35">
                     <EditItemTemplate>    
                         <asp:Label ID="lblIdEmpresa" runat="server" Text='<%# Bind("oEmpresa.idEmpresa") %>' Visible="false"></asp:Label>                     
                         <asp:DropDownList ID="ddlEmpresaI" runat="server"></asp:DropDownList>
                     </EditItemTemplate>
                     <ItemTemplate>
                         <asp:Label ID="ddlEmpresa" runat="server" Text='<%# Bind("oEmpresa.Descripcion") %>'></asp:Label>
                     </ItemTemplate>
                 </asp:TemplateField>
                 <asp:TemplateField ShowHeader="False" HeaderStyle-CssClass="celda15t" ItemStyle-CssClass="celda15b">
                     <EditItemTemplate>
                         <asp:LinkButton ID="btnEditar" runat="server" CausesValidation="True" CssClass="boton40"
                             CommandName="Update" Text="Actualizar"></asp:LinkButton>
                         <asp:LinkButton ID="btnEliminar" runat="server" CausesValidation="False" CssClass="boton40"
                             CommandName="Cancel" Text="Cancelar"></asp:LinkButton>
                     </EditItemTemplate>
                     <ItemTemplate>
                         <asp:LinkButton ID="btnEditar" runat="server" CausesValidation="False" CssClass="boton40"
                             CommandName="Edit" Text="Editar" ></asp:LinkButton>
                         <asp:LinkButton ID="btnEliminar" runat="server" CausesValidation="False" CssClass="boton40"
                             CommandName="Delete" Text="Eliminar" OnClientClick="return Eliminar('el Sector');"></asp:LinkButton>
                     </ItemTemplate>
                 </asp:TemplateField>
             </Columns>
         </asp:GridView>
     </div>
</asp:Content>
