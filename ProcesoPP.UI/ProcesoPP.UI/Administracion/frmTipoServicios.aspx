﻿<%@ Page Title="" Language="C#" MasterPageFile="~/frmPrincipal.Master" AutoEventWireup="true" EnableEventValidation="false"
    CodeBehind="frmTipoServicios.aspx.cs" Inherits="ProcesoPP.UI.Administracion.frmTipoServicios" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="../Css/jquery-ui.css" />
    <script type="text/javascript">
        $(document).ready(function () { SetMenu("liCargaDatos"); }); 
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#tabs").tabs({
                active: function () {
                    var newIdx = $("#tabs").tabs('option', 'active');
                    $('#<%=hdidLastTab.ClientID%>').val(newIdx);
                },
                /*heightStyle: "auto",*/
                active: previuslySelectedTab,
                show: { effect: "fadeIn", duration: 500 }
            });
        });

        function CambiarCheck(oChk, mostrarSector) {
            chk = oChk.children[0];
            lbl = oChk.children[1];

            divSector = document.getElementById("<%=divSector.ClientID %>");
            if (chk.checked) {
                lbl.innerText = "Si";
                if (mostrarSector) {
                    divSector.style.display = "block";
                }
            }
            else {
                lbl.innerText = "No";
                if (mostrarSector) {
                    divSector.style.display = "none";
                }
            }
        }

        function MostrarKms(oChk) {
            chk = oChk.children[0];
            lbl = oChk.children[1];
            divKms = document.getElementById("<%=divKms.ClientID %>");
            if (chk.checked) {
                divKms.style.display = "block";
            }
            else {
                divKms.style.display = "none";
            }

        }

        function MostrarCuadrilla(oChk) {
            chk = oChk.children[0];
            lbl = oChk.children[1];

            divKms = document.getElementById("<%=divEsp.ClientID %>");
            if (chk.checked) {
                divKms.style.display = "block";
            }
            else {
                divKms.style.display = "none";
            }
        }

        function MostrarEquipos(oChk) {
            chk = oChk.children[0];
            lbl = oChk.children[1];

            divKms = document.getElementById("<%=divEspEquipo.ClientID %>");
            if (chk.checked) {
                divKms.style.display = "block";
            }
            else {
                divKms.style.display = "none";
            }
        }

        function MostrarMat(oChk) {
            chk = oChk.children[0];
            lbl = oChk.children[1];

            divMaterial = document.getElementById("<%=divMaterial.ClientID %>");
            if (chk.checked) {
                divMaterial.style.display = "block";
            }
            else {
                divMaterial.style.display = "none";
            }
        }

        function selectTodos(grid) {
            grid = 'gvEspEquipo.ClientID';
            gv = document.getElementById('<%= gvEspEquipo.ClientID %>');
            chkHeader = gv.rows[0].cells[0].children[0].children[0];
            for (var i = 1; i < gv.rows.length; i++) {
                row = gv.rows[i];
                chkUnir = row.cells[0].children[0];
                if (chkHeader.checked) {
                    chkUnir.checked = true;
                }
                else {
                    chkUnir.checked = false;
                }
            }

            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="apertura">
        TIPO DE SERVICIOS</div>
    <asp:HiddenField runat="server" ID="hdidLastTab" Value="0" />
    <div id="tabs">
        <ul>
            <li><a href="#tabs-1" tabindex="1">Búsqueda</a></li>
            <li><a href="#tabs-2" tabindex="2">Alta</a></li>
        </ul>
        <div id="tabs-1" class="tabla_admin" style="height: auto;">
            <div class="label_admint">
                <label>
                    Nombre del Servicio:
                </label>
            </div>
            <div class="label_adminc">
                <asp:TextBox ID="txtBusqueda" CssClass="sol_campoadm" runat="server"></asp:TextBox></div>
            <div class="label_adminc">
            </div>
            <div class="label_adminc">
            </div>
            <div class="label_adminc">
            </div>
            <div class="label_admintb">
                <asp:Button ID="btnBuscar" runat="server" Text="Buscar" CssClass="btn_form" OnClick="btnBuscar_Click" />
            </div>
            <div>
                <asp:GridView ID="gvTipoServicio" runat="server" AutoGenerateColumns="False" DataMember="idTipoServicio"
                    CssClass="tabla3" OnRowCancelingEdit="gvTipoServicio_RowCancelingEdit" OnRowEditing="gvTipoServicio_RowEditing"
                    OnRowDeleting="gvTipoServicio_RowDeleting" OnRowDataBound="gvTipoServicio_RowDataBound">
                    <Columns>
                        <asp:TemplateField HeaderText="Id TipoServicio" Visible="false">
                            <EditItemTemplate>
                                <asp:Label ID="lblIdTipoServicio" runat="server" Text='<%# Bind("idTipoServicio") %>'></asp:Label>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblTipoServicioI" runat="server" Text='<%# Bind("idTipoServicio") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Descripcion" HeaderStyle-CssClass="celda20ts" ItemStyle-CssClass="celda20s">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtDescripcion" runat="server" Text='<%# Bind("Descripcion") %>'
                                    CssClass="cajaTextoGrilla"></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblDescripcion" runat="server" CssClass="cajaTextoGrilla" Text='<%# Bind("Descripcion") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Esp. Técnica" HeaderStyle-CssClass="celda30t" ItemStyle-CssClass="celda30s">
                            <EditItemTemplate>
                                <div style="overflow-y: scroll; width: 100%; height: 180px; margin-left: 0px;">
                                    <asp:CheckBoxList ID="ddlEspecificacion" runat="server" CssClass="cajaTextoGrilla"
                                        TextAlign="Right" onclick="return Tildar(this);">
                                    </asp:CheckBoxList>
                                </div>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:HiddenField runat="server" ID="hfidEspecificacion" Value="0" />
                                <asp:ListBox ID="lblEspecificacion" runat="server" CssClass="cajaTextoGrilla" SelectionMode="Single"
                                    DataSource='<%# Bind("oEspecificacion") %>' DataTextField="Descripcion"></asp:ListBox>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Cod. Tango" HeaderStyle-CssClass="celda10t" ItemStyle-CssClass="celda10s">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtCodTango" runat="server" Text='<%# Bind("CodTango") %>' CssClass="cajaTextoGrilla"></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblCodTango" runat="server" Text='<%# Bind("CodTango") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Cod." HeaderStyle-CssClass="celda10t" ItemStyle-CssClass="celda10s">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtAbreviatura" runat="server" Text='<%# Bind("Abreviacion") %>'
                                    CssClass="cajaTextoGrilla"></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblAbreviatura" runat="server" Text='<%# Bind("Abreviacion") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Req. Pers." HeaderStyle-CssClass="celda10t" ItemStyle-CssClass="celda10s">
                            <EditItemTemplate>
                                <asp:CheckBox ID="chktRequierePersona" runat="server" Checked='<%# Bind("RequierePersonal") %>'
                                    Style="float: left;"></asp:CheckBox>
                                <asp:DropDownList ID="ddlSectorItem" runat="server" DataTextField="Descripcion" Style="float: right;"
                                    Width="80%" DataValueField="idSector" ToolTip="Sector al que pertenece el personal relacionado a este Servicio">
                                </asp:DropDownList>
                                </div>
                                <asp:HiddenField runat="server" ID="hfIdSector" Value='<%#Bind("idSector") %>' />
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:CheckBox ID="lblRequierePersona" Enabled="false" runat="server" Checked='<%# Bind("RequierePersonal") %>'>
                                </asp:CheckBox>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Req. Kms." HeaderStyle-CssClass="celda10t" ItemStyle-CssClass="celda10s">
                            <EditItemTemplate>
                                <asp:CheckBox ID="chktRequiereKms" runat="server" Checked='<%# Bind("RequiereKms") %>'
                                    Style="float: left;"></asp:CheckBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:CheckBox ID="lblRequiereKms" Enabled="false" runat="server" Checked='<%# Bind("RequiereKms") %>'>
                                </asp:CheckBox>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ShowHeader="False" ItemStyle-CssClass="celda15b1" HeaderStyle-CssClass="celda15t">
                            <EditItemTemplate>
                                <asp:LinkButton ID="btnEditar" runat="server" CausesValidation="True" CssClass="boton40"
                                    CommandName="Update" Text="Actualizar"></asp:LinkButton>
                                <asp:LinkButton ID="btnEliminar" runat="server" CausesValidation="False" CssClass="boton40"
                                    CommandName="Cancel" Text="Cancelar"></asp:LinkButton>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:LinkButton ID="btnEditar" runat="server" CausesValidation="False" CssClass="boton40"
                                    CommandName="Edit" Text="Editar"></asp:LinkButton>
                                <asp:LinkButton ID="btnEliminar" runat="server" CausesValidation="False" CssClass="boton40"
                                    CommandName="Delete" Text="Eliminar" OnClientClick="return Eliminar('el TipoServicio');"></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>
        </div>
        <div id="tabs-2" class="tabla_admin" style="height: auto;">
            <cc1:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" EnablePartialRendering="true"
                EnableScriptGlobalization="true">
            </cc1:ToolkitScriptManager>
            
      <div id="ser_tabla1">  
        <div id="ser_tabla1_tit"> Datos de Identificación del Servicio </div>
                    
            <div class="label_admint">
                <label>
                    Nombre del Servicio:
                </label>
            </div>
            <div class="label_adminc">
                <asp:TextBox ID="txtDescripcion" CssClass="sol_campoadm" runat="server"></asp:TextBox></div>
            <div class="label_admint">
            </div>
            <div class="label_admint">
            </div>
            <div class="label_admint">
                <label>
                    Código Tango:
                </label>
            </div>
            <div class="label_adminc">
                <asp:TextBox ID="txtCodTango" CssClass="sol_campoadm" runat="server"></asp:TextBox></div>
            <div class="label_admint">
            </div>
            <div class="label_admint">
            </div>
            <div class="label_admint">
                <label>
                    Codificación Interna SS:
                </label>
            </div>
            <div class="label_adminc">
                <asp:TextBox ID="txtAbreviacion" CssClass="sol_campoadm" Style="text-transform: uppercase"
                    runat="server" MaxLength="6"></asp:TextBox></div>
            <div class="label_admint">
            </div>
            <div class="label_admint">
            </div>
       </div>
            
       <div id="ser_tabla1">  
        <div id="ser_tabla1_tit"> Datos de Tecnicos del Servicio </div>     
        
        <div id="ser_tabla1_txt"> Seleccione los items necesarios para dar de alta el servicio</div>    
            
            <div class="label_admint16"> 
                <label>
                   1) Materiales
                </label>
            </div>
            <div class="label_adminc">
                <asp:CheckBox ID="chkMateriales" runat="server" ToolTip="Seleccionar si el tipo de servicio requiere materiales para relacionar en la OTI"
                    Text=" No" onchange="CambiarCheck(this,false);MostrarMat(this);"></asp:CheckBox>
            </div>
        
             <div class="label_admint">
            </div>
                        
            <div id="divMaterial" runat="server" style="height: 130px; display: none;">
                <div class="label_admint2" style="height: 90%;">
                    <label>
                        Detalle del Servicio con Materiales</label></div>
                <div class="label_esp" style="overflow-y: scroll; width: 78%; height: 90%;">
                    <asp:UpdatePanel ID="upMaterial" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:GridView runat="server" ID="gvMaterial" AutoGenerateColumns="false" ShowHeader="false"
                                Height="60px" ShowFooter="true" GridLines="Horizontal" OnRowCommand="gvEspCuadrilla_RowCommand">
                                <Columns>
                                    <asp:TemplateField HeaderText="Id Especificacion" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblEspecificacion" runat="server" Text='<%# Bind("IdEspecificaciones") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Esp">
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkEspecificacion" runat="server" Checked='<%# Bind("RequiereMaterial") %>'>
                                            </asp:CheckBox>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:ImageButton runat="server" ID="btnGuardar" ToolTip="Agregar fila" ImageUrl="~/iconos/btn_mas.png"
                                                Height="19" Width="19" CommandName="addMat" CommandArgument="IdEspecificaciones"></asp:ImageButton>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Descripción">
                                        <ItemTemplate>
                                            <asp:Label ID="lblDescripcion" runat="server" Text='<%# Bind("Descripcion") %>'></asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="txtDescripcion" CssClass="cajaTextoGrilla" runat="server" Text='<%# Bind("Descripcion") %>'></asp:TextBox>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
            <div class="label_admint16">
                <label>
                    2) Traslado:
                </label>
            </div>
            <div class="label_adminc">
                <asp:CheckBox ID="chkTranslado" runat="server" ToolTip="Relaciona si el tipo de servicio es movimiento, para agregar Origen - Destino en OTI"
                    Text=" No" onchange="CambiarCheck(this,false); MostrarKms(this);"></asp:CheckBox></div>
            <div class="label_admint">
            </div>
            
            <div id="divKms" runat="server" style="display: none;">
                <div class="label_admint_ab">
                </div>
                <div class="label_admint_ab">
                    <label>
                        Kms</label></div>
                <div class="label_admint_ab">
                    <asp:CheckBox ID="chkKms" runat="server" groupname="kms" ToolTip="Seleccione si los kms que requiere son los de Procesos"
                        Text="No" onchange="CambiarCheck(this,false);"></asp:CheckBox>
                </div>
                <div class="label_admint_ab">
                    <label>
                        Kms Tacografo</label></div>
                <div class="label_admint_ab">
                    <asp:CheckBox ID="chkTaco" runat="server" groupname="kms" ToolTip="Seleccione si los kms que requiere son los del tacografo"
                        Text="No" onchange="CambiarCheck(this,false);"></asp:CheckBox>
                </div>
                <div class="label_admint_ab">
                </div>
                <div class="label_admint_ab">
                    <label>
                        Requiere Chofer</label></div>
                <div class="label_adminc_ab">
                    <asp:CheckBox ID="chkChofer" runat="server" groupname="kms" ToolTip="Seleccione si el traslado requiere Chofer"
                        Text="No" onchange="CambiarCheck(this,false);"></asp:CheckBox>
                </div>
                <div class="label_admint_ab">
                </div>
                <div id="divEspTRas" runat="server" style="height: 130px;">
                    <div class="label_admint2" style="height: 90%;">
                        <label>
                            Detalle del Servicio con Traslado</label></div>
                    <div class="label_esp" style="overflow-y: scroll; width: 78%; height: 90%;">
                        <asp:UpdatePanel ID="upTraslado" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <asp:GridView runat="server" ID="gvTralado" AutoGenerateColumns="false" ShowHeader="false"
                                    Height="60px" ShowFooter="true" GridLines="Horizontal" OnRowCommand="gvEspCuadrilla_RowCommand">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Id Especificacion" Visible="false">
                                            <ItemTemplate>
                                                <asp:Label ID="lblEspecificacion" runat="server" Text='<%# Bind("IdEspecificaciones") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Esp">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkEspecificacion" runat="server" Checked='<%# Bind("RequiereTraslado") %>'>
                                                </asp:CheckBox>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:ImageButton runat="server" ID="btnGuardar" ToolTip="Agregar fila" ImageUrl="~/iconos/btn_mas.png"
                                                    Height="19" Width="19" CommandName="addTras" CommandArgument="IdEspecificaciones"></asp:ImageButton>
                                            </FooterTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Descripción">
                                            <ItemTemplate>
                                                <asp:Label ID="lblDescripcion" runat="server" Text='<%# Bind("Descripcion") %>'></asp:Label>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:TextBox ID="txtDescripcion" CssClass="cajaTextoGrilla" runat="server" Text='<%# Bind("Descripcion") %>' ToolTip="Agregar detalle de traslado"></asp:TextBox>
                                            </FooterTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
            <div class="label_admint16">
                <label>3) Personal</label>
            </div>
            <div class="label_adminc">
                <asp:CheckBox ID="chkPersonal" runat="server" ToolTip="Relaciona el personal con el sector en la OTI"
                    Text=" No" onchange="CambiarCheck(this,true);"></asp:CheckBox></div>
            <div class="label_admint">
            </div>
           
            <div id="divSector" runat="server" style="display: none;">
                 <div class="label_admint_ab">
                    <label>
                        Empresa:
                    </label>
                </div>
                <div class="label_adminc_ab">
                    <asp:DropDownList ID="ddlEmpresa" CssClass="sol_campoadm" DataTextField="Descripcion"
                        DataValueField="idEmpresa" runat="server" ToolTip="Seleccione la Empresa para seleccionar el sector adecuado.">
                    </asp:DropDownList>
                    <cc1:CascadingDropDown ID="cddEmpresa" TargetControlID="ddlEmpresa" PromptText="N/A" PromptValue="0" 
                    ServiceMethod="GetEmpresa" runat="server" Category="idEmpresa" 
                    LoadingText="Cargando..."/>
                </div>
                <div class="label_adminc_ab">
                </div>
                <div class="label_admint_ab">
                    <label>
                        Sector:
                    </label>
                </div>
                <div class="label_adminc_ab">
                    <asp:DropDownList ID="ddlSector" CssClass="sol_campoadm" DataTextField="Descripcion"
                        DataValueField="idSector" runat="server" ToolTip="Sector al que pertenece el personal relacionado a este Servicio">
                    </asp:DropDownList>
                    <cc1:CascadingDropDown ID="cddSector" TargetControlID="ddlSector" PromptText="N/A" PromptValue="0" 
                    ServiceMethod="GetSector" runat="server" Category="idSector" 
                    ParentControlID="ddlEmpresa" LoadingText="Cargando..." />
                </div>
                <div class="label_adminc_ab">
                </div>
                <div id="divEspPerson" runat="server" style="height: 130px;">
                    <div class="label_admint2" style="height: 90%;">
                        <label>
                            Detalle del Servicio con Personal</label></div>
                    <div class="label_esp" style="overflow-y: scroll; width: 78%; height: 90%;">
                        <asp:UpdatePanel ID="upPersonal" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <asp:GridView runat="server" ID="gvPersona" AutoGenerateColumns="false" ShowHeader="false"
                                    Height="60px" ShowFooter="true" GridLines="Horizontal" OnRowCommand="gvEspCuadrilla_RowCommand">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Id Especificacion" Visible="false">
                                            <ItemTemplate>
                                                <asp:Label ID="lblEspecificacion" runat="server" Text='<%# Bind("IdEspecificaciones") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Esp">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkEspecificacion" runat="server" Checked='<%# Bind("RequierePersonal") %>'>
                                                </asp:CheckBox>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:ImageButton runat="server" ID="btnGuardar" ToolTip="Agregar fila" ImageUrl="~/iconos/btn_mas.png"
                                                    Height="19" Width="19" CommandName="addPer" CommandArgument="IdEspecificaciones"></asp:ImageButton>
                                            </FooterTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Descripción">
                                            <ItemTemplate>
                                                <asp:Label ID="lblDescripcion" runat="server" Text='<%# Bind("Descripcion") %>'></asp:Label>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:TextBox ID="txtDescripcion" CssClass="cajaTextoGrilla" runat="server" Text='<%# Bind("Descripcion") %>' ToolTip="Agregar detalle de personal"></asp:TextBox>
                                            </FooterTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
            <div class="label_admint16">
                <label>
                    4) Cuadrilla:
                </label>
            </div>
            <div class="label_adminc">
                <asp:CheckBox ID="chkCuadrilla" runat="server" ToolTip="Seleccionar si el tipo de servicio requiere cuadrillas para relacionar en la OTI"
                    Text=" No" onchange="CambiarCheck(this,false);MostrarCuadrilla(this);"></asp:CheckBox>
            </div>
            <div class="label_admint">
            </div>
           
            <div id="divEsp" runat="server" style="height: 140px; display: none;">
                <div class="label_admint2" style="height: 90%;">
                    <label>
                        Detalle del Servicio con Cuadrilla
                    </label>
                </div>
                <div class="label_esp" style="overflow-y: scroll; width: 78%; height: 90%;">
                    <asp:UpdatePanel ID="upEsp" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:GridView runat="server" ID="gvEspCuadrilla" AutoGenerateColumns="false" ShowHeader="false"
                                Height="60px" ShowFooter="true" GridLines="Horizontal" OnRowCommand="gvEspCuadrilla_RowCommand">
                                <Columns>
                                    <asp:TemplateField HeaderText="Id Especificacion" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblEspecificacion" runat="server" Text='<%# Bind("IdEspecificaciones") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Esp">
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkEspecificacion" runat="server" Checked='<%# Bind("RequiereCuadrilla") %>'>
                                            </asp:CheckBox>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:ImageButton runat="server" ID="btnGuardar" ToolTip="Agregar fila" ImageUrl="~/iconos/btn_mas.png"
                                                Height="19" Width="19" CommandName="addEsp" CommandArgument="IdEspecificaciones"></asp:ImageButton>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Descripción">
                                        <ItemTemplate>
                                            <asp:Label ID="lblDescripcion" runat="server" Text='<%# Bind("Descripcion") %>'></asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="txtDescripcion" CssClass="cajaTextoGrilla" runat="server" Text='<%# Bind("Descripcion") %>' ToolTip="Agregar detalle de cuadrilla"></asp:TextBox>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
            <div class="label_admint16">
                <label>
                    5) Equipo:
                </label>
            </div>
            <div class="label_adminc">
                <asp:CheckBox ID="chkReqEquipo" runat="server" ToolTip="Seleccionar si el tipo de servicio requiere equipo para relacionar en la OTI"
                    Text=" No" onchange="CambiarCheck(this,false);MostrarEquipos(this);"></asp:CheckBox>
            </div>
            <div class="label_admint">
            </div>
           
            <div id="divEspEquipo" runat="server" style="display: none; height: 130px;">
                <div class="label_admint2" style="height: 90%;">
                    <label>
                        Detalle del Servicio con Equipos</label></div>
                <div class="label_esp" style="overflow-y: scroll; width: 78%; height: 90%;">
                    <asp:UpdatePanel ID="upEQ" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:GridView runat="server" ID="gvEspEquipo" AutoGenerateColumns="false" ShowHeader="true"
                                Height="60px" ShowFooter="true" GridLines="Both" OnRowCommand="gvEspCuadrilla_RowCommand">
                                <Columns>
                                    <asp:TemplateField HeaderText="Id Especificacion" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblEspecificacion" runat="server" Text='<%# Bind("IdEspecificaciones") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField >
                                        <HeaderTemplate>
                                            <asp:CheckBox ID="chkEspecificacion" runat="server" Checked="false" ToolTip="Para seleccionar todos los detalles" onchange="return selectTodos('gvEspEquipo');">
                                            </asp:CheckBox>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkEspecificacion" runat="server" Checked='<%# Bind("RequiereEquipo") %>'>
                                            </asp:CheckBox>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:ImageButton runat="server" ID="btnGuardar" ToolTip="Agregar nuevo detalle" ImageUrl="~/iconos/btn_mas.png"
                                                Height="19" Width="19" CommandName="addEQ" CommandArgument="IdEspecificaciones"></asp:ImageButton>                                              
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Seleccione los detalles">
                                        <ItemTemplate>
                                            <asp:Label ID="lblDescripcion" runat="server" Text='<%# Bind("Descripcion") %>'></asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>                                           
                                            <asp:TextBox ID="txtDescripcion" CssClass="cajaTextoGrilla" runat="server" Text='<%# Bind("Descripcion") %>' ToolTip="Agregar detalle de equipos"></asp:TextBox>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
            
            
            
            
<div class="label_admint16">
<label>6) Trabajo en varios Equipos</label>
            </div>
            
            
<div class="label_admint">
                <asp:CheckBox ID="chkMasEquipo" runat="server" ToolTip="Seleccionar si en el tipo de servicio se debe proporcionar mas de un equipo en la OTI"
                    Text=" No" onchange="CambiarCheck(this,false);"></asp:CheckBox>
            </div>  
            
<div class="label_admint"></div>
<div class="label_admint"></div>

            
            
            
            
            
            </div>
            <div class="label_admint">
            </div>
            <div class="label_admint">
            </div>
            <div class="label_admint">
            </div>
            
            
            
            
            
            <div class="label_admintb">
                <asp:Button ID="btnGuardar" runat="server" Text="Guardar" CssClass="btn_form" OnClick="btnGuardar_Click"
                    OnClientClick="return ValidarObj('txtDescripcion|Ingrese una descripción.');" />
            </div>
            <div class="label_admintb">
                <asp:Button ID="btnCancelar" runat="server" Text="Cancelar" CssClass="btn_form" OnClick="btnCancelar_Click" />
            </div>
        </div>
    </div>
</asp:Content>
