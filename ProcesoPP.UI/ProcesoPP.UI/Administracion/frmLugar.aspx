﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="frmLugar.aspx.cs" MasterPageFile="~/frmPrincipal.Master" MaintainScrollPositionOnPostback="true" Inherits="ProcesoPP.UI.Administracion.frmLugar" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server"> 
<script type="text/javascript" >
    $(document).ready(function() { SetMenu("liCargaDatos"); }); 
</script>
</asp:Content>
<asp:Content ID="cphContenedor" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="apertura">Alta | Baja | Modificación de Lugar</div>
<div class="tabla_admin">
    <div class="label_admint"><label>Lugar: </label></div> 
    <div class="label_adminc"><asp:TextBox ID="txtDescripcion" CssClass="sol_campoadm" runat="server"></asp:TextBox> </div>     
 <div class="label_admintb"><asp:Button ID="btnGuardar" runat="server" Text="Guardar" CssClass="btn_form" onclick="btnGuardar_Click" OnClientClick="return ValidarObj('txtDescripcion|Ingrese una Descripción.');" /></div>
    <div class="label_relleno20"></div>
</div>
<div class="tabla3" id="divMensaje" runat="server">
     <asp:Label ID="lblError" CssClass="celda40t" runat="server" Text="Error" Visible="false"></asp:Label>
 </div>
<div>
     <asp:GridView ID="gvLugar" runat="server" AutoGenerateColumns="False" 
         DataMember="idLugar"  CssClass="tabla3" 
         onrowcancelingedit="gvLugar_RowCancelingEdit" 
         onrowediting="gvLugar_RowEditing" 
         onrowupdating="gvLugar_RowUpdating" 
         onrowdeleting="gvLugar_RowDeleting"
         OnRowDataBound="gvLugar_RowDataBound">
         <Columns>
             <asp:TemplateField HeaderText="Id Lugar" HeaderStyle-CssClass="celda_admint" ItemStyle-CssClass="celda_admintt" Visible="false">
                 <EditItemTemplate>
                     <asp:Label ID="lblIdLugar" runat="server" Text='<%# Bind("idLugar") %>'></asp:Label>
                 </EditItemTemplate>
                 <ItemTemplate>
                     <asp:Label ID="lblLugarI" runat="server" Text='<%# Bind("idLugar") %>'></asp:Label>
                 </ItemTemplate>
             </asp:TemplateField>
             <asp:TemplateField HeaderText="Descripcion" HeaderStyle-CssClass="celda80t" ItemStyle-CssClass="celda80">
                 <EditItemTemplate>
                     <asp:TextBox ID="txtDescripcion" runat="server" Text='<%# Bind("Descripcion") %>'></asp:TextBox>
                 </EditItemTemplate>
                 <ItemTemplate>
                     <asp:Label ID="lblDescripcion" runat="server" Text='<%# Bind("Descripcion") %>'></asp:Label>
                 </ItemTemplate>
             </asp:TemplateField>
             <asp:TemplateField HeaderStyle-CssClass="celda15t" ItemStyle-CssClass="celda15b">
                 <EditItemTemplate >
                     <asp:LinkButton ID="btnEditar" runat="server" CausesValidation="True" CssClass="boton40"
                         CommandName="Update" Text="Actualizar"></asp:LinkButton>
                     <asp:LinkButton ID="btnEliminar" runat="server" CausesValidation="False" CssClass="boton40"
                         CommandName="Cancel" Text="Cancelar"></asp:LinkButton>
                 </EditItemTemplate>
                 <ItemTemplate >
                     <asp:LinkButton ID="btnEditar" runat="server" CausesValidation="False" CssClass="boton40"
                         CommandName="Edit" Text="Editar" ></asp:LinkButton>
                     <asp:LinkButton ID="btnEliminar" runat="server" CausesValidation="False" CssClass="boton40"
                         CommandName="Delete" Text="Eliminar" OnClientClick="return Eliminar('la Lugar');"></asp:LinkButton>
                 </ItemTemplate>
             </asp:TemplateField>
         </Columns>
     </asp:GridView>
 </div>
</asp:Content>
