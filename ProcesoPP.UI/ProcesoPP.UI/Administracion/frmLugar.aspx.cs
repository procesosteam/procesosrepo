﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ProcesoPP.Model;
using ProcesoPP.Business;
using ProcesoPP.Services;

namespace ProcesoPP.UI.Administracion
{
    public partial class frmLugar : System.Web.UI.Page
    {
      
        #region PROPIEDADES

        public Permisos _Permiso
        {
            get { return (Permisos)ViewState["permiso"]; }
            set { ViewState["permiso"] = value; }
        }

        public int _idUsuario
        {
            get { return (int)ViewState["idUsuario"]; }
            set { ViewState["idUsuario"] = value; }
        }

        #endregion

        #region EVENTOS

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                _idUsuario = Common.EvaluarSession(Request, this.Page);
                if (_idUsuario != 0)
                {
                    _Permiso = Common.getModulo(Request, this.Page);
                    if (_Permiso != null && _Permiso.Lectura)
                    {
                        Mensaje.ocultarDivMsj(divMensaje);
                        if (!IsPostBack)
                        {
                            CargarLugars();
                            InitScren();
                        }
                    }
                    else
                    {
                        Mensaje.errorMsj("NO TIENE PERMISOS A ESTE FORMULARIO", this.Page, "SIN ACCESO", "../frmPrincipal.aspx");
                    }
                }
                else
                {
                    Mensaje.errorMsj("DEBE LOGUEARSE", this.Page, "LOGIN", "../frmLogin.aspx");
                }
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                Guardar();
                CargarLugars();
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        protected void gvLugar_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            gvLugar.EditIndex = -1;
            CargarLugars();
        }

        protected void gvLugar_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                Eliminar(gvLugar.Rows[e.RowIndex]);
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        protected void gvLugar_RowEditing(object sender, GridViewEditEventArgs e)
        {
            gvLugar.EditIndex = e.NewEditIndex;
            CargarLugars();
        }

        protected void gvLugar_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                Modificar(gvLugar.Rows[e.RowIndex]);
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        protected void gvLugar_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Common.CargarRow(e, _Permiso);
            }
        } 

        #endregion

        #region METODOS

        private void InitScren()
        {
            if (!_Permiso.Escritura)
            {
                btnGuardar.Visible = false;
            }
        }

        private void Eliminar(GridViewRow row)
        {
            int id = int.Parse((row.FindControl("lblLugarI") as Label).Text);
            LugarBus oLugarBus = new LugarBus();

            if (oLugarBus.LugarDelete(id))
            {
                Mensaje.successMsj("Lugar eliminado con éxito", this.Page, "Eliminado", null);
                CargarLugars();
            }
        }

        private void Modificar(GridViewRow row)
        {
            int id = int.Parse((row.FindControl("lblIdLugar") as Label).Text);
            string descripcion = (row.FindControl("txtDescripcion") as TextBox).Text;
            LugarBus oLugarBus = new LugarBus();
            Lugar oLugar = new Lugar(id, descripcion);

            if (oLugarBus.LugarUpdate(oLugar))
            {
                Mensaje.successMsj("Lugar modificado con éxito", this.Page, "Modificado", null);
                gvLugar.EditIndex = -1;
                CargarLugars();
            }
        }

        private void Guardar()
        {
            LugarBus oLugarBus = new LugarBus();
            Lugar oLugar = new Lugar(0, txtDescripcion.Text);
            oLugar.IdLugar = oLugarBus.LugarAdd(oLugar);

            if (oLugar.IdLugar > 0)
            {
                Mensaje.successMsj("Lugar agregado con éxito.", this.Page, "Guardado", "");
                txtDescripcion.Text = "";
                CargarLugars();
            }
        }

        private void CargarLugars()
        {
            LugarBus oLugarBus = new LugarBus();
            gvLugar.DataSource = oLugarBus.LugarGetAll();
            gvLugar.DataBind();
        }

        #endregion
    }
}
