﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ProcesoPP.Business;
using ProcesoPP.Model;
using ProcesoPP.Services;

namespace ProcesoPP.UI.Administracion
{
    public partial class frmSolicitante : System.Web.UI.Page
    {
        #region PROPIEDADES

        public Permisos _Permiso
        {
            get { return (Permisos)ViewState["permiso"]; }
            set { ViewState["permiso"] = value; }
        }

        public int _idUsuario
        {
            get { return (int)ViewState["idUsuario"]; }
            set { ViewState["idUsuario"] = value; }
        }

        #endregion

        #region EVENTOS

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                 _idUsuario = Common.EvaluarSession(Request, this.Page);
                 if (_idUsuario != 0)
                 {
                     _Permiso = Common.getModulo(Request, this.Page);
                     if (_Permiso != null && _Permiso.Lectura)
                     {
                         if (!IsPostBack)
                             InitScreen();
                     }
                     else
                     {
                         Mensaje.errorMsj("NO TIENE PERMISOS A ESTE FORMULARIO", this.Page, "SIN ACCESO", "../frmPrincipal.aspx");
                     }
                 }
                 else
                 {
                     Mensaje.errorMsj("DEBE LOGUEARSE", this.Page, "LOGIN", "../frmLogin.aspx");
                 }
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        protected void gvSolicitantes_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            gvSolicitante.EditIndex = -1;
            CargarSolicitantes();
        }

        protected void gvSolicitantes_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                Eliminar(gvSolicitante.Rows[e.RowIndex]);
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        protected void gvSolicitantes_RowEditing(object sender, GridViewEditEventArgs e)
        {
            ScreenEdit(e.NewEditIndex);
        }

        protected void gvSolicitantes_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                Modificar(gvSolicitante.Rows[e.RowIndex]);
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                Guardar();
                CargarSolicitantes();
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        protected void ddlEmpresa_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlEmpresa.SelectedIndex > 0)
                CargarSector(ddlEmpresa.SelectedValue, ddlSector);
        }
        
        protected void ddlEmpresaGV_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (((DropDownList)sender).SelectedIndex > 0)
            {
                DropDownList ddl = ((DataControlFieldCell)(((ListControl)(sender)).Parent)).Parent.FindControl("ddlSector") as DropDownList;
                CargarSector(((DropDownList)sender).SelectedValue, ddl);
            }
        }

        protected void gvSolicitantes_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Common.CargarRow(e, _Permiso);
            }
        } 
        #endregion

        #region METODOS

        private void InitScreen()
        {
            CargarEmpresas(ddlEmpresa);
            CargarSolicitantes();
            ddlSector.Items.Insert(0, new ListItem("N/A", "0"));
            if (!_Permiso.Escritura)
            {
                btnGuardar.Visible = false;
            }
        }

        private void ScreenEdit(int NewEditIndex)
        {
            gvSolicitante.EditIndex = NewEditIndex;
            CargarSolicitantes();
            string idEmpresa = (gvSolicitante.Rows[NewEditIndex].FindControl("lblIdEmpresa") as Label).Text;
            DropDownList ddlEmpreGV = gvSolicitante.Rows[NewEditIndex].FindControl("ddlEmpresa") as DropDownList;
            string idSector = (gvSolicitante.Rows[NewEditIndex].FindControl("lblIdSector") as Label).Text;
            DropDownList ddlSectGV = gvSolicitante.Rows[NewEditIndex].FindControl("ddlSector") as DropDownList;
            CargarEmpresas(ddlEmpreGV);
            CargarSector(idEmpresa, ddlSectGV);
            ddlEmpreGV.SelectedValue = idEmpresa;
            ddlSectGV.SelectedValue = idSector;
        }

        private void Eliminar(GridViewRow row)
        {
            SolicitanteBus oSolicitanteBus = new SolicitanteBus();
            int id = int.Parse((row.FindControl("lblSolicitantesI") as Label).Text);
          
            if (oSolicitanteBus.SolicitanteDelete(id))
            {
                Mensaje.successMsj("Solicitante eliminado con éxito", this.Page, "Eliminado", null);
                CargarSolicitantes();
            }
        }

        private void Modificar(GridViewRow row)
        {
            Label lblIdSolicitante = row.FindControl("lblIdSolicitantes") as Label;
            TextBox txtNombre = row.FindControl("txtNombre") as TextBox;
            DropDownList ddlEmprGv = row.FindControl("ddlEmpresa") as DropDownList;
            DropDownList ddlSectGv = row.FindControl("ddlSector") as DropDownList;
            
            SolicitanteBus oSolicitanteBus = new SolicitanteBus();
            Solicitante oSolicitante = new Solicitante();
            oSolicitante.IdSolicitante = int.Parse(lblIdSolicitante.Text);
            oSolicitante.Nombre = txtNombre.Text;
            oSolicitante.Apellido = txtNombre.Text;
            oSolicitante.IdEmpresa = int.Parse(ddlEmprGv.SelectedValue);
            oSolicitante.IdSector = int.Parse(ddlSectGv.SelectedValue);

            if (oSolicitanteBus.SolicitanteUpdate(oSolicitante))
            {
                Mensaje.successMsj("Modificado con éxito", this.Page, "Modificado", null);
                gvSolicitante.EditIndex = -1;
                CargarSolicitantes();
            }

        }

        private void Guardar()
        {
            SolicitanteBus oSolicitanteBus = new SolicitanteBus();
            Solicitante oSolicitante = new Solicitante(0,
                                                       txtDescripcion.Text,
                                                       txtDescripcion.Text,
                                                       int.Parse(ddlSector.SelectedValue),
                                                       int.Parse(ddlEmpresa.SelectedValue),
                                                       "0");
            oSolicitante.IdSolicitante = oSolicitanteBus.SolicitanteAdd(oSolicitante);            
            if (oSolicitante.IdSolicitante > 0)
            {
                Mensaje.successMsj("Solicitante guardado con éxito", this.Page, "Guardado", null);
                txtDescripcion.Text = "";
                ddlSector.SelectedIndex = 0;
                ddlEmpresa.SelectedIndex = 0;
                CargarSolicitantes();
            }
            
        }

        private void CargarSolicitantes()
        {
            SolicitanteBus oSolicitanteBus = new SolicitanteBus();
            gvSolicitante.DataSource = oSolicitanteBus.SolicitanteGetAll();
            gvSolicitante.DataBind();
        }

        private void CargarSector(string idEmpresa, DropDownList ddl)
        {
            SectorBus oSectorBus = new SectorBus();
            ddl.DataSource = oSectorBus.SectorGetAllByIdEmpresa(int.Parse(idEmpresa));
            ddl.DataValueField = "idSector";
            ddl.DataTextField = "Descripcion";
            ddl.DataBind();
            ddl.Items.Insert(0, new ListItem("N/A", "0"));
        }

        private void CargarEmpresas(DropDownList ddl)
        {
            ddl.DataSource = null;
            EmpresaBus oEmpresaBus = new EmpresaBus();
            oEmpresaBus.CargarEmpresa(ref ddl, "Seleccione...");
        }

        #endregion

    }
}
