﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ProcesoPP.Model;
using ProcesoPP.Business;
using ProcesoPP.Services;

namespace ProcesoPP.UI.Administracion
{
    public partial class frmDiagrama: System.Web.UI.Page
    {
        #region PROPIEDADES

        public Permisos _Permiso
        {
            get { return (Permisos)ViewState["permiso"]; }
            set { ViewState["permiso"] = value; }
        }

        public int _idUsuario
        {
            get { return (int)ViewState["idUsuario"]; }
            set { ViewState["idUsuario"] = value; }
        }

        #endregion

        #region EVENTOS

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                _idUsuario = Common.EvaluarSession(Request, this.Page);
                if (_idUsuario != 0)
                {
                    _Permiso = Common.getModulo(Request, this.Page);
                    if (_Permiso != null && _Permiso.Lectura)
                    {
                        Mensaje.ocultarDivMsj(divMensaje);
                        if (!IsPostBack)
                        {
                            CargarDiagramaes();
                            InitScrren();
                        }
                    }
                    else
                    {
                        Mensaje.errorMsj("NO TIENE PERMISOS A ESTE FORMULARIO", this.Page, "SIN ACCESO", "../frmPrincipal.aspx");
                    }
                }
                else
                {
                    Mensaje.errorMsj("DEBE LOGUEARSE", this.Page, "LOGIN", "../frmLogin.aspx");
                }
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                Guardar();
                CargarDiagramaes();
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        protected void gvDiagrama_RowEditing(object sender, GridViewEditEventArgs e)
        {
            gvDiagrama.EditIndex = e.NewEditIndex;
            CargarDiagramaes();
        }

        protected void gvDiagrama_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            gvDiagrama.EditIndex = -1;
            CargarDiagramaes();
        }

        protected void gvDiagrama_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                Modificar(gvDiagrama.Rows[e.RowIndex]);
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        protected void gvDiagrama_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                Eliminar(gvDiagrama.Rows[e.RowIndex]);
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        protected void gvDiagrama_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Common.CargarRow(e, _Permiso);
            }
        }

        #endregion

        #region Metodos

        private void InitScrren()
        {
            if (!_Permiso.Escritura)
            {
                btnGuardar.Visible = false;
            }
        }

        private void Eliminar(GridViewRow row)
        {
            int id = int.Parse((row.FindControl("lblDiagramaI") as Label).Text);
            DiagramaBus oDiagramaBus = new DiagramaBus();

            if (oDiagramaBus.DiagramaDelete(id))
            {
                Mensaje.successMsj("Diagrama Eliminado con éxito.", this.Page, "Eliminado", "");
                CargarDiagramaes();
            }
        }

        private void Modificar(GridViewRow row)
        {
            int id = int.Parse((row.FindControl("lblIdDiagrama") as Label).Text);
            string descripcion = (row.FindControl("txtDescripcion") as TextBox).Text;
            DiagramaBus oDiagramaBus = new DiagramaBus();
            Diagrama oDiagrama = new Diagrama(id, descripcion);

            if (oDiagramaBus.DiagramaUpdate(oDiagrama))
            {
                Mensaje.successMsj("Modificado con Exito!", this.Page, "Modificado", null);
                gvDiagrama.EditIndex = -1;
                CargarDiagramaes();
            }
        }

        private void Guardar()
        {
            DiagramaBus oDiagramaBus = new DiagramaBus();
            Diagrama oDiagrama = new Diagrama(0, txtDiagrama.Text);
            oDiagrama.IdDiagrama = oDiagramaBus.DiagramaAdd(oDiagrama);

            if (oDiagrama.IdDiagrama > 0)
            {
                Mensaje.successMsj("Guardado con Exito!", this.Page, "Agregado", null);
                txtDiagrama.Text = "";
                CargarDiagramaes();
            }
        }

        private void CargarDiagramaes()
        {
            DiagramaBus oDiagramaBus = new DiagramaBus();
            gvDiagrama.DataSource = oDiagramaBus.DiagramaGetAll();
            gvDiagrama.DataBind();
        }

        #endregion


    }
}
