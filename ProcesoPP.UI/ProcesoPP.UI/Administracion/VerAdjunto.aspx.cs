﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ProcesoPP.Business;
using ProcesoPP.Model;
using ProcesoPP.Services;
using System.Data;
using System.Configuration;
using System.IO;
using System.Diagnostics;
using System.Web.Services;
using AjaxControlToolkit;
using ParteDiario.Services;
using ProcesoPP.UI.Personal;

namespace ProcesoPP.UI.Administracion
{
    public partial class VerAdjunto : System.Web.UI.Page
    {

        public int _idUsuario
        {
            get { return (int)ViewState["idUsuario"]; }
            set { ViewState["idUsuario"] = value; }
        }

        public bool _esPersonal
        {
            get { return (bool)ViewState["esPersonal"]; }
            set { ViewState["esPersonal"] = value; }
        }

        public Permisos _Permiso
        {
            get { return (Permisos)ViewState["permiso"]; }
            set { ViewState["permiso"] = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            string ruta = Request["idArchivo"];
            bool esPersonal = bool.Parse(Request["esPersonal"]);
            _esPersonal = esPersonal;
            if (esPersonal)
            {
                PersonalFilesBus oBus = new PersonalFilesBus();
                PersonalFiles oArchivo = oBus.PersonalFilesGetById(int.Parse(ruta));
                ruta = oArchivo.Ruta;
            }
            else
            {
                VehiculoFilesBus oBus = new VehiculoFilesBus();
                VehiculoFiles oArchivo = oBus.VehiculoFilesGetById(int.Parse(ruta));
                ruta = oArchivo.Ruta;
            }
            
            if (ruta.EndsWith("pdf"))
            {
                Response.ContentType = "Application/pdf";
                Response.TransmitFile(ruta);
            }
            else if (ruta.EndsWith("jpg"))
            {
                Response.ContentType = "image/jpeg";
                Response.TransmitFile(ruta);
            }
            else if (ruta.EndsWith("jpeg"))
            {
                Response.ContentType = "image/pjpeg";
                Response.TransmitFile(ruta);
            }
            else if (ruta.EndsWith("png"))
            {
                Response.ContentType = "image/png";
                Response.TransmitFile(ruta);
            }
            else if (ruta.EndsWith("bmp"))
            {
                Response.ContentType = "image/bmp";
                Response.TransmitFile(ruta);
            }
            else
            {
                Response.TransmitFile(ruta);
            }
            

        }

        public void btn_cancelar_Click(object sender, EventArgs e)
        {
            if (_esPersonal)
            {
                try
                {
                    //Response.Redirect("frmPersonalGestion.aspx?idUsuario=" + this._idUsuario + "&idModulo=" + this._Permiso.oModulo.idModulo);
                }
                catch (Exception ex)
                {
                    Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
                }
            }
            else
            {
                try
                {
                    //Response.Redirect("frmVehiculoGestion.aspx?idUsuario=" + this._idUsuario + "&idModulo=" + this._Permiso.oModulo.idModulo + "&EsEquipo=" + _EsEquipo);
                }
                catch (Exception ex)
                {
                    Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
                }

            }

        }
    }
}