﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="frmCondicion.aspx.cs" Inherits="ProcesoPP.UI.Administracion.frmCondicion" MasterPageFile="~/frmPrincipal.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server"> 
<script type="text/javascript" >
    $(document).ready(function() { SetMenu("liCarga"); }); 
</script>
</asp:Content>
<asp:Content ContentPlaceHolderID="ContentPlaceHolder1" runat="server" ID="content">
    <div id="apertura">Alta | Baja | Modificación de Condición</div>
    <div class="tabla_admin">
    <div class="label_admint"><label>Condición: </label></div> 
    <div class="label_adminc"> <asp:TextBox ID="txtCondicion" CssClass="sol_campoadm" runat="server"></asp:TextBox></div>
    
          <div class="label_admintb">
        <asp:Button ID="btnGuardar" runat="server" Text="Guardar"  CssClass="btn_form"        
            onclick="btnGuardar_Click" OnClientClick="return ValidarObj('txtCondicion|Ingrese una Descripción.');" />
       </div>
       
      <div class="label_relleno20"></div>
    </div>
    <div id="divMensaje" runat="server">
         <asp:Label ID="lblError" runat="server" Text="Error" Visible="false"></asp:Label>
     </div>
    <div>
         <asp:GridView ID="gvCoondicion" runat="server" AutoGenerateColumns="False" 
             DataMember="idCondicion" CssClass="tabla3"
             onrowcancelingedit="gvCoondicion_RowCancelingEdit" 
             onrowediting="gvCoondicion_RowEditing" 
             onrowupdating="gvCoondicion_RowUpdating" 
             onrowdeleting="gvCoondicion_RowDeleting"
             OnRowDataBound="gvCoondicion_RowDataBound">
             <Columns>
                 <asp:TemplateField HeaderText="Id Condicion" HeaderStyle-CssClass="celda_admint" ItemStyle-CssClass="celda_admintt" Visible="false">
                     <EditItemTemplate>
                         <asp:Label ID="lblIdCondicion" runat="server" Text='<%# Bind("idCondicion") %>' ></asp:Label>
                     </EditItemTemplate>
                     <ItemTemplate>
                         <asp:Label ID="lblCondicionI" runat="server" Text='<%# Bind("idCondicion") %>'></asp:Label>
                     </ItemTemplate>
                 </asp:TemplateField>
                 <asp:TemplateField HeaderText="Descripción" HeaderStyle-CssClass="celda_admint" ItemStyle-CssClass="celda_admintt">
                     <EditItemTemplate>
                         <asp:TextBox ID="txtDescripcion" runat="server" Text='<%# Bind("Descripcion") %>'></asp:TextBox>
                     </EditItemTemplate>
                     <ItemTemplate>
                         <asp:Label ID="lblDescripcion" runat="server" Text='<%# Bind("Descripcion") %>'></asp:Label>
                     </ItemTemplate>
                 </asp:TemplateField>
                 <asp:TemplateField ShowHeader="False" HeaderStyle-CssClass="cel_btnadmint" ItemStyle-CssClass="cel_btnadmin">
                     <EditItemTemplate>
                         <asp:LinkButton ID="btnEditar" runat="server" CausesValidation="True" CssClass="boton40"
                             CommandName="Update" Text="Actualizar"></asp:LinkButton>
                         <asp:LinkButton ID="btnEliminar" runat="server" CausesValidation="False" CssClass="boton40"
                             CommandName="Cancel" Text="Cancelar"></asp:LinkButton>
                     </EditItemTemplate>
                     <ItemTemplate>
                         <asp:LinkButton ID="btnEditar" runat="server" CausesValidation="False"  CssClass="boton40"
                             CommandName="Edit" Text="Editar" ></asp:LinkButton>
                        <asp:LinkButton ID="btnEliminar" runat="server" CausesValidation="False"  CssClass="boton40"
                             CommandName="Delete" Text="Eliminar" OnClientClick="return Eliminar('la Condicion');"></asp:LinkButton>
                     </ItemTemplate>
                 </asp:TemplateField>
             </Columns>
         </asp:GridView>
     </div>
</asp:Content>