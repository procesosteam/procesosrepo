﻿<%@ Page Language="C#" MasterPageFile="~/frmPrincipal.Master"  AutoEventWireup="true" MaintainScrollPositionOnPostback="true" CodeBehind="frmChofer.aspx.cs" Inherits="ProcesoPP.UI.Administracion.frmChofer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<script type="text/javascript" >
    $(document).ready(function() { SetMenu("liCargaDatos"); }); 
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="apertura">Alta | Baja | Modificación de Choferes</div>
<div class="tabla_admin">
    <div class="label_admint"><label>Nombre: </label></div> 
    <div class="label_adminc"><asp:TextBox runat="server" CssClass="sol_campoadm" ID="txtNombre"></asp:TextBox></div>
     <div class="label_relleno20"></div>
    <div class="label_relleno20"></div>
    
    <div class="label_admint"><label>Apellido: </label> </div> 
    <div class="label_adminc"><asp:TextBox runat="server" CssClass="sol_campoadm" ID="txtApellido" onblur="return proponerNombre('txtNombre', 'txtApellido', 'txtUserName');"></asp:TextBox></div> 
    <div class="label_relleno20"></div>
    <div class="label_relleno20"></div>
    
      <div class="label_admint"><label>Dirección: </label></div> 
      <div class="label_adminc"><asp:TextBox runat="server" CssClass="sol_campoadm" ID="txtDireccion"></asp:TextBox></div> 
       <div class="label_relleno20"></div>
    <div class="label_relleno20"></div>
    
    <div class="label_admint"><label>Telefono: </label></div> 
    <div class="label_adminc"><asp:TextBox runat="server" CssClass="sol_campoadm" ID="txtTelefono" ></asp:TextBox></div> 
     <div class="label_relleno20"></div>
    <div class="label_relleno20"></div>
    
    <div class="label_admint"><label>DNI : </label></div> 
<div class="label_adminc"><asp:TextBox runat="server" ID="txtDNI" CssClass="sol_campoadm" onkeypress="return ValidarNumeroEntero(event,this);"></asp:TextBox></div> 
 <div class="label_relleno20"></div>

   <div class="label_admintb"><asp:Button runat="server" id="btnGuardar" CssClass="btn_form" Text="Guardar"
        OnClientClick="return ValidarObj('txtNombre|Debe ingresar el Nombre.~txtApellido|Debe ingresar el Apellido.');"
                onclick="btnGuardar_Click"/></div>
        
    </div>
    <div class="tabla3" id="divMensaje" runat="server">
         <asp:Label ID="lblError" CssClass="label40t" runat="server" Text="Error" Visible="false"></asp:Label>
     </div>
    
         <asp:GridView ID="gvChofer" runat="server" AutoGenerateColumns="False" 
             DataMember="idChofer" CssClass="tabla3"
             onrowcancelingedit="gvChofer_RowCancelingEdit" 
             onrowediting="gvChofer_RowEditing" 
             onrowupdating="gvChofer_RowUpdating" 
             onrowdeleting="gvChofer_RowDeleting" 
        onrowdatabound="gvChofer_RowDataBound">
             <Columns>
                 <asp:TemplateField HeaderText="Id Us." Visible="false">                    
                     <ItemTemplate>
                         <asp:Label ID="lblidChofer" runat="server" Text='<%# Bind("idChofer") %>'></asp:Label>
                     </ItemTemplate>
                 </asp:TemplateField>
                 <asp:TemplateField HeaderText="Nombre" HeaderStyle-CssClass="celda20t" ItemStyle-CssClass="celda20">
                     <EditItemTemplate>
                         <asp:TextBox ID="txtNombre" runat="server" Text='<%# Bind("Nombre") %>'></asp:TextBox>
                     </EditItemTemplate>
                     <ItemTemplate>
                     <asp:Label ID="lblNombre" runat="server" Text='<%# Bind("Nombre") %>'></asp:Label>
                     </ItemTemplate>
                 </asp:TemplateField>
                 <asp:TemplateField HeaderText="Apellido" HeaderStyle-CssClass="celda20t" ItemStyle-CssClass="celda20">
                     <EditItemTemplate>
                         <asp:TextBox ID="txtApellido" runat="server" Text='<%# Bind("Apellido") %>'></asp:TextBox>
                     </EditItemTemplate>
                     <ItemTemplate>
                         <asp:Label ID="lblApellidoI" runat="server" Text='<%# Bind("Apellido") %>'></asp:Label>
                     </ItemTemplate>
                 </asp:TemplateField>
                 <asp:TemplateField HeaderText="Dirección" HeaderStyle-CssClass="celda30t" ItemStyle-CssClass="celda30">
                     <EditItemTemplate>
                         <asp:TextBox ID="txtDireccion" runat="server" Text='<%# Bind("Direccion") %>'></asp:TextBox>
                     </EditItemTemplate>
                     <ItemTemplate>
                         <asp:Label ID="lblDireccionI" runat="server" Text='<%# Bind("Direccion") %>'></asp:Label>
                     </ItemTemplate>
                 </asp:TemplateField>
                 <asp:TemplateField HeaderText="Tel" HeaderStyle-CssClass="celda10t" ItemStyle-CssClass="celda10">
                     <EditItemTemplate>
                         <asp:TextBox ID="txtTelefono" runat="server" Text='<%# Bind("Telefono") %>'></asp:TextBox>
                     </EditItemTemplate>
                     <ItemTemplate>
                         <asp:Label ID="lblTelefonoI" runat="server" Text='<%# Bind("Telefono") %>'></asp:Label>
                     </ItemTemplate>
                 </asp:TemplateField>
                 <asp:TemplateField HeaderText="DNI" HeaderStyle-CssClass="celda20t" ItemStyle-CssClass="celda20">
                     <EditItemTemplate>
                         <asp:TextBox ID="txtDNI" runat="server" Text='<%# Bind("DNI") %>' onkeypress="return ValidarNumeroEntero(event,this);"></asp:TextBox>
                     </EditItemTemplate>
                     <ItemTemplate>
                         <asp:Label ID="lblDNII" runat="server" Text='<%# Bind("DNI") %>'></asp:Label>
                     </ItemTemplate>
                 </asp:TemplateField>
                 <asp:TemplateField ShowHeader="False" HeaderStyle-CssClass="celda10t" ItemStyle-CssClass="celda10">
                     <EditItemTemplate>
                         <asp:LinkButton ID="btnEditar" runat="server" CausesValidation="True" CssClass="boton40"
                             CommandName="Update" Text="Actualizar"></asp:LinkButton>
                         <asp:LinkButton ID="btnEliminar" runat="server" CausesValidation="False" CssClass="boton40"
                             CommandName="Cancel" Text="Cancelar"></asp:LinkButton>                  
                     </EditItemTemplate>
                     <ItemTemplate>
                         <asp:LinkButton ID="btnEditar" runat="server" CausesValidation="False" CssClass="boton40"
                             CommandName="Edit" Text="Editar" ></asp:LinkButton>      
                         <asp:LinkButton ID="btnEliminar" runat="server" CausesValidation="False" CssClass="boton40"
                             CommandName="Delete" Text="Eliminar" OnClientClick="return Eliminar('el Chofer');"></asp:LinkButton> 
                     </ItemTemplate>
                 </asp:TemplateField>
             </Columns>
         </asp:GridView>
     </div>
</asp:Content>

