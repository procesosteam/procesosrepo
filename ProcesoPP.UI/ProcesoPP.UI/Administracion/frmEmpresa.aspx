﻿<%@ Page Title="" Language="C#" MasterPageFile="~/frmPrincipal.Master" AutoEventWireup="true" CodeBehind="frmEmpresa.aspx.cs" Inherits="ProcesoPP.UI.Administracion.frmEmpresa" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server"> 
<link rel="stylesheet" href="../Css/jquery-ui.css" />
<script type="text/javascript" >
    $(document).ready(function () { SetMenu("liCargaDatos"); });

    $(document).ready(function () {
        $("#tabs").tabs({
            active: function () {
                var newIdx = $("#tabs").tabs('option', 'active');
                $('#<%=hdidLastTab.ClientID%>').val(newIdx);
            },
            /*heightStyle: "auto",*/
            active: previuslySelectedTab,
            show: { effect: "fadeIn", duration: 500 }
        });
    });
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="apertura">CLIENTES</div>
        <asp:HiddenField runat="server" ID="hdidLastTab" Value="0" />
    <div id="tabs">
        <ul>
            <li><a href="#tabs-1" tabindex="1">Búsqueda</a></li>
            <li><a href="#tabs-2" tabindex="2">Alta</a></li>        
        </ul>
        <div id="tabs-1" class="tabla_admin" style="height:auto;">
            <div class="label_admint">    
                <label>Nombre: </label>  </div> 
            <div class="label_adminc">      
                <asp:TextBox ID="txtBusqueda" CssClass="sol_campoadm" runat="server" ></asp:TextBox></div>
                <div class="label_adminc"></div>
            <div class="label_adminc"></div>
            <div class="label_adminc"></div>
             <div class="label_admintb">           
                 <asp:Button ID="btnBuscar" runat="server" Text="Buscar"  CssClass="btn_form" 
                     onclick="btnBuscar_Click" />
            </div>    
        </div>
        <div id="tabs-2" class="tabla_admin" style="height:auto;">
              <div class="tabla_admin">
                    <div class="label_admint"><label>Nombre: </label></div> 
                    <div class="label_adminc"><asp:TextBox ID="txtDescripcion" CssClass="sol_campoadm" runat="server"></asp:TextBox> </div>     
                    <div class="label_relleno20"></div>
                    <div class="label_relleno20"></div>
                    <div class="label_admint"><label>Razón Social: </label></div> 
                    <div class="label_adminc"><asp:TextBox ID="txtRazonSocial" CssClass="sol_campoadm" runat="server"></asp:TextBox> </div>     
                    <div class="label_relleno20"></div>
                    <div class="label_relleno20"></div>
                    <div class="label_admint"><label>Domicilio: </label></div> 
                    <div class="label_adminc"><asp:TextBox ID="txtDomicilio" CssClass="sol_campoadm" runat="server"></asp:TextBox> </div>     
                    <div class="label_relleno20"></div>
                    <div class="label_relleno20"></div> 
                    <div class="label_admint"><label>Condicion IVA: </label></div> 
                    <div class="label_adminc"><asp:DropDownList ID="ddlCondicionIva" CssClass="sol_campoadm" runat="server"></asp:DropDownList> </div>     
                    <div class="label_relleno20"></div>
                    <div class="label_relleno20"></div> 
                    <div class="label_admint"><label>CUIT: </label></div> 
                    <div class="label_adminc"><asp:TextBox ID="txtCuit" CssClass="sol_campoadm" runat="server"></asp:TextBox> </div>     
                    <div class="label_relleno20"></div>
                    <div class="label_relleno20"></div> 
                    <div class="label_admint"><label>Cod. Tango: </label></div> 
                    <div class="label_adminc"><asp:TextBox ID="txtTango" CssClass="sol_campoadm" runat="server"></asp:TextBox> </div>     
                    <div class="label_relleno20"></div>
                    <div class="label_relleno20"></div> 
                    <div class="label_admint"><label>Empresa Interna: </label></div> 
                    <div class="label_adminc"><asp:CheckBox ID="chkInterna" CssClass="sol_campoadm" runat="server"></asp:CheckBox> </div>     
                    <div class="label_admintb">
                        <asp:Button ID="btnCancelar" runat="server" Text="Cancelar" CssClass="btn_form" onclick="btnCancelar_Click" /></div>
                    <div class="label_admintb">
                        <asp:Button ID="btnGuardar" runat="server" Text="Guardar" CssClass="btn_form" onclick="btnGuardar_Click" OnClientClick="return ValidarObj('txtDescripcion|Ingrese una Descripción.');" /></div>
                </div>    
        </div>
    </div>
    <div class="tabla3" id="divMensaje" runat="server">
         <asp:Label ID="lblError" CssClass="celda40t" runat="server" Text="Error" Visible="false"></asp:Label>
     </div>
    <div>
         <asp:GridView ID="gvEmpresa" runat="server" AutoGenerateColumns="False" 
             DataMember="idEmpresa"  CssClass="tabla3" 
             onrowcancelingedit="gvEmpresa_RowCancelingEdit" 
             onrowediting="gvEmpresa_RowEditing"              
             onrowdeleting="gvEmpresa_RowDeleting"
             OnRowDataBound="gvEmpresa_RowDataBound">
             <Columns>
                 <asp:TemplateField Visible="false">
                     <EditItemTemplate>
                         <asp:Label ID="lblIdEmpresa" runat="server" Text='<%# Bind("idEmpresa") %>'></asp:Label>
                     </EditItemTemplate>
                     <ItemTemplate>
                         <asp:Label ID="lblEmpresaI" runat="server" Text='<%# Bind("idEmpresa") %>'></asp:Label>
                     </ItemTemplate>
                 </asp:TemplateField>
                 <asp:TemplateField HeaderText="Descripcion" HeaderStyle-CssClass="celda20t" ItemStyle-CssClass="celda20">
                     <EditItemTemplate>
                         <asp:TextBox ID="txtDescripcion" runat="server" Text='<%# Bind("Descripcion") %>'></asp:TextBox>
                     </EditItemTemplate>
                     <ItemTemplate>
                         <asp:Label ID="lblDescripcion" runat="server" Text='<%# Bind("Descripcion") %>'></asp:Label>
                     </ItemTemplate>
                 </asp:TemplateField>
                 <asp:TemplateField HeaderText="Razon Social" HeaderStyle-CssClass="celda20t" ItemStyle-CssClass="celda20">
                     <EditItemTemplate>
                         <asp:TextBox ID="txtRazonSocial" runat="server" Text='<%# Bind("RazonSocial") %>'></asp:TextBox>
                     </EditItemTemplate>
                     <ItemTemplate>
                         <asp:Label ID="lblRazonSocial" runat="server" Text='<%# Bind("RazonSocial") %>'></asp:Label>
                     </ItemTemplate>
                 </asp:TemplateField>
                 <asp:TemplateField HeaderText="Domicilio" HeaderStyle-CssClass="celda30t" ItemStyle-CssClass="celda30">
                     <EditItemTemplate>
                         <asp:TextBox ID="txtDomicilio" runat="server" Text='<%# Bind("Domicilio") %>'></asp:TextBox>
                     </EditItemTemplate>
                     <ItemTemplate>
                         <asp:Label ID="lblDomicilio" runat="server" Text='<%# Bind("Domicilio") %>'></asp:Label>
                     </ItemTemplate>
                 </asp:TemplateField>
               
                 <asp:TemplateField HeaderText="CUIT" HeaderStyle-CssClass="celda15t" ItemStyle-CssClass="celda15">
                     <EditItemTemplate>
                         <asp:TextBox ID="txtCUIT" runat="server" Text='<%# Bind("CUIT") %>'></asp:TextBox>
                     </EditItemTemplate>
                     <ItemTemplate>
                         <asp:Label ID="lblCUIT" runat="server" Text='<%# Bind("CUIT") %>'></asp:Label>
                     </ItemTemplate>
                 </asp:TemplateField>
                 <asp:TemplateField HeaderText="Interna" HeaderStyle-CssClass="celda10t" ItemStyle-CssClass="celda10">
                     <EditItemTemplate>
                         <asp:CheckBox ID="chkInterna" runat="server" Checked='<%# Bind("Interna") %>'></asp:CheckBox>
                     </EditItemTemplate>
                     <ItemTemplate>
                         <asp:CheckBox ID="chkInterna" runat="server" Checked='<%# Bind("Interna") %>' Enabled="false"></asp:CheckBox>
                     </ItemTemplate>
                 </asp:TemplateField>
                 <asp:TemplateField ShowHeader="false" HeaderStyle-CssClass="celda15t" ItemStyle-CssClass="celda15b">
                     <EditItemTemplate >
                         <asp:LinkButton ID="btnEditar" runat="server" CausesValidation="True" CssClass="boton40"
                             CommandName="Update" Text="Actualizar"></asp:LinkButton>
                         <asp:LinkButton ID="btnEliminar" runat="server" CausesValidation="False" CssClass="boton40"
                             CommandName="Cancel" Text="Cancelar"></asp:LinkButton>
                     </EditItemTemplate>
                     <ItemTemplate >
                         <asp:LinkButton ID="btnEditar" runat="server" CausesValidation="False" CssClass="boton40"
                             CommandName="Edit" Text="Editar" ></asp:LinkButton>
                         <asp:LinkButton ID="btnEliminar" runat="server" CausesValidation="False" CssClass="boton40"
                             CommandName="Delete" Text="Eliminar" OnClientClick="return Eliminar('la Empresa');"></asp:LinkButton>
                     </ItemTemplate>
                 </asp:TemplateField>
             </Columns>
         </asp:GridView>
     </div>
</asp:Content>
