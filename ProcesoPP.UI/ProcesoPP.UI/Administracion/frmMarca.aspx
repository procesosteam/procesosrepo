﻿<%@ Page Title="" Language="C#" MasterPageFile="~/frmPrincipal.Master" AutoEventWireup="true" CodeBehind="frmMarca.aspx.cs" Inherits="ProcesoPP.UI.Administracion.frmMarca" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<script type="text/javascript" >
    $(document).ready(function () { SetMenu("liCargaDatos"); }); 
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div id="apertura">ALTA | BAJA | MODIFICACION DE MARCA</div>
    <div class="tabla_admin">
    <div class="label_admint"><label>Marca: </label></div> 
    <div class="label_adminc"> <asp:TextBox ID="txtMarca" CssClass="sol_campoadm" runat="server"></asp:TextBox></div>
    
          <div class="label_admintb">
        <asp:Button ID="btnGuardar" runat="server" Text="Guardar"  CssClass="btn_form"        
            onclick="btnGuardar_Click" OnClientClick="return ValidarObj('txtMarca|Ingrese una Descripción.');" />
       </div>
       
      <div class="label_relleno20"></div>
    </div>
    <div id="divMensaje" runat="server">
         <asp:Label ID="lblError" runat="server" Text="Error" Visible="false"></asp:Label>
     </div>
    <div>
         <asp:GridView ID="gvMarca" runat="server" AutoGenerateColumns="False" 
             DataMember="idMarca" CssClass="tabla3"
             onrowcancelingedit="gvMarca_RowCancelingEdit" 
             onrowediting="gvMarca_RowEditing" 
             onrowupdating="gvMarca_RowUpdating" 
             onrowdeleting="gvMarca_RowDeleting"
             OnRowDataBound="gvMarca_RowDataBound">
             <Columns>
                 <asp:TemplateField HeaderText="Id Marca" HeaderStyle-CssClass="celda_admint" ItemStyle-CssClass="celda_admintt" Visible="false">
                     <EditItemTemplate>
                         <asp:Label ID="lblIdMarca" runat="server" Text='<%# Bind("idMarca") %>' ></asp:Label>
                     </EditItemTemplate>
                     <ItemTemplate>
                         <asp:Label ID="lblMarcaI" runat="server" Text='<%# Bind("idMarca") %>'></asp:Label>
                     </ItemTemplate>
                 </asp:TemplateField>
                 <asp:TemplateField HeaderText="Descripción" HeaderStyle-CssClass="celda80t" ItemStyle-CssClass="celda80">
                     <EditItemTemplate>
                         <asp:TextBox ID="txtDescripcion" runat="server" Text='<%# Bind("Descripcion") %>'></asp:TextBox>
                     </EditItemTemplate>
                     <ItemTemplate>
                         <asp:Label ID="lblDescripcion" runat="server" Text='<%# Bind("Descripcion") %>'></asp:Label>
                     </ItemTemplate>
                 </asp:TemplateField>
                 <asp:TemplateField ShowHeader="False" HeaderStyle-CssClass="celda15t" ItemStyle-CssClass="celda15b">
                     <EditItemTemplate>
                         <asp:LinkButton ID="btnEditar" runat="server" CausesValidation="True" CssClass="boton40"
                             CommandName="Update" Text="Actualizar"></asp:LinkButton>
                         <asp:LinkButton ID="btnEliminar" runat="server" CausesValidation="False" CssClass="boton40"
                             CommandName="Cancel" Text="Cancelar"></asp:LinkButton>
                     </EditItemTemplate>
                     <ItemTemplate>
                         <asp:LinkButton ID="btnEditar" runat="server" CausesValidation="False"  CssClass="boton40"
                             CommandName="Edit" Text="Editar" ></asp:LinkButton>
                        <asp:LinkButton ID="btnEliminar" runat="server" CausesValidation="False"  CssClass="boton40"
                             CommandName="Delete" Text="Eliminar" OnClientClick="return Eliminar('la Marca');"></asp:LinkButton>
                     </ItemTemplate>
                 </asp:TemplateField>
             </Columns>
         </asp:GridView>
     </div>
</asp:Content>
