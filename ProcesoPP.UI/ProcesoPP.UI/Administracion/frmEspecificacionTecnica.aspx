﻿<%@ Page Title="" Language="C#" MasterPageFile="~/frmPrincipal.Master" AutoEventWireup="true"
    CodeBehind="frmEspecificacionTecnica.aspx.cs" Inherits="ProcesoPP.UI.Administracion.frmEspecificacionTecnica" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="../Css/jquery-ui.css" />
    <script type="text/javascript">
        $(document).ready(function () { SetMenu("liCargaDatos"); });

        $(document).ready(function () {
            $("#tabs").tabs({
                active: function () {
                    var newIdx = $("#tabs").tabs('option', 'active');
                    $('#<%=hdidLastTab.ClientID%>').val(newIdx);
                },
                /*heightStyle: "auto",*/
                active: previuslySelectedTab,
                show: { effect: "fadeIn", duration: 500 }
            });
        });


        function CambiarCheck(oChk, mostrarSector) {
            chk = oChk.children[0];
            lbl = oChk.children[1];

            divPropio = document.getElementById("<%=divPropio.ClientID%>");
            divNoPropio = document.getElementById("<%=divNoPropio.ClientID%>");

            chkPropio = document.getElementById('<%=chkPropio.ClientID %>');
            chkNoPropio = document.getElementById('<%=chkNoPropio.ClientID %>');

            if (chk.checked) {
                lbl.innerText = "Si";
                if (mostrarSector) {
                    divNoPropio.style.display = "block";
                    divPropio.style.display = "block";
                    divEqClientes.style.display = "none";
                    divEquiposPropios.style.display = "none";
                }
            }
            else {
                lbl.innerText = "No";
                if (mostrarSector) {
                    divNoPropio.style.display = "none";
                    divPropio.style.display = "none";
                    divEqClientes.style.display = "none";
                    divEquiposPropios.style.display = "none";
                    chkPropio.checked = false;
                    chkPropio.labels[0].innerText = "No";
                    chkNoPropio.labels[0].innerText = "No";
                    chkNoPropio.checked = false;
                }
            }
        }

        function mostrarEq(oChk, propio) {
            chk = oChk.children[0];
            lbl = oChk.children[1];
            divEquiposPropios = document.getElementById('<%=divEquiposPropios.ClientID %>');
            divEqClientes = document.getElementById('<%=divEqClientes.ClientID %>');

            chkPropio = document.getElementById('<%=chkPropio.ClientID %>');
            chkNoPropio = document.getElementById('<%=chkNoPropio.ClientID %>');

            if (chk.checked) {
                if (propio) {
                    divEquiposPropios.style.display = "block";
                    divEquiposPropios.style.height = "200px";
                    divEqClientes.style.display = "none";
                    chkNoPropio.checked = false;
                    chkNoPropio.labels[0].innerText = "No";
                }
                else {
                    divEqClientes.style.display = "block";
                    divEqClientes.style.height = "200px";
                    divEquiposPropios.style.display = "none";
                    chkPropio.checked = false;
                    chkPropio.labels[0].innerText = "No";
                }               
                
            }
            else {
                if (propio) {
                    divEquiposPropios.style.display = "none";
                }
                else {
                    divEqClientes.style.display = "none";
                }                
            }
        }

        function mostrarCuadrilla(oChk) {
            chk = oChk.children[0];
            lbl = oChk.children[1];
            divCuadrilla = document.getElementById('<%=divCuadrilla.ClientID %>');
            if (chk.checked) {
                divCuadrilla.style.display = "block";
                divCuadrilla.style.height = "100px";
            }
            else {
                divCuadrilla.style.display = "none";
            }
        }

    </script>
    <script type="text/javascript">
         function textChange(e, txtbox, cls) {
              if (!(/[^A-Za-z0-9 ]/.test(String.fromCharCode(e.keyCode))) || e.keyCode == 8) {
                var text = $(txtbox).val().toLowerCase();
                var node = '';
                var count = 0;
                $('#td_' + cls + ' table tr td').each(function () {
                  node = $(this).text().toLowerCase();
                  if (node.match(text) == null)
                    $(this).hide();
                  else {
                    $(this).show();
                    count++;
                  }
                });                
              }
            };
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="apertura">
        VINCULACIÓN DE EQUIPOS Y CUADRILLAS</div>
    <asp:HiddenField runat="server" ID="hdidLastTab" Value="0" />
    <div id="tabs">
        <ul>
            <li><a href="#tabs-1" tabindex="1">Búsqueda</a></li>
            <li><a href="#tabs-2" tabindex="2">Alta</a></li>
        </ul>
        <div id="tabs-1" class="tabla_admin" style="height: auto;">
            <div class="label_admint">
                <label>
                    Detalle del Servicio:
                </label>
            </div>
            <div class="label_adminc">
                <asp:TextBox ID="txtBusqueda" CssClass="sol_campoadm" runat="server"></asp:TextBox></div>
            <div class="label_adminc">
            </div>
            <div class="label_adminc">
            </div>
            <div class="label_adminc">
            </div>
            <div class="label_admintb">
                <asp:Button ID="btnBuscar" runat="server" Text="Buscar" CssClass="btn_form" OnClick="btnBuscar_Click" />
            </div>
            <div>
                <asp:GridView ID="gvEspecificacion" runat="server" AutoGenerateColumns="False" DataMember="IdEspecificaciones"
                    CssClass="tabla3" OnRowCancelingEdit="gvEspecificacion_RowCancelingEdit" OnRowEditing="gvEspecificacion_RowEditing"
                    OnRowUpdating="gvEspecificacion_RowUpdating" OnRowDeleting="gvEspecificacion_RowDeleting"
                    OnRowDataBound="gvEspecificacion_RowDataBound">
                    <Columns>
                        <asp:TemplateField HeaderText="Id Especificacion" HeaderStyle-CssClass="celda_admint"
                            ItemStyle-CssClass="celda_admintt" Visible="false">
                            <EditItemTemplate>
                                <asp:Label ID="lblIdEspecificacion" runat="server" Text='<%# Bind("IdEspecificaciones") %>'></asp:Label>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblEspecificacionI" runat="server" Text='<%# Bind("IdEspecificaciones") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Descripción" HeaderStyle-CssClass="celda_admint" ItemStyle-CssClass="celda_admintt">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtDescripcion" runat="server" Text='<%# Bind("Descripcion") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblDescripcion" runat="server" Text='<%# Bind("Descripcion") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Equipo" HeaderStyle-CssClass="celda10t" ItemStyle-CssClass="celda10">
                            <ItemTemplate>
                                <asp:CheckBox ID="chkEquipo" runat="server" Enabled="false" Checked='<%# Bind("RequiereEquipo") %>'></asp:CheckBox>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Cuadrilla" HeaderStyle-CssClass="celda10t" ItemStyle-CssClass="celda10">
                            <ItemTemplate>
                                <asp:CheckBox ID="chkCuadri" runat="server" Enabled="false" Checked='<%# Bind("RequiereCuadrilla") %>'></asp:CheckBox>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ShowHeader="False" HeaderStyle-CssClass="celda15t" ItemStyle-CssClass="celda15">
                            <EditItemTemplate>
                                <asp:LinkButton ID="btnEditar" runat="server" CausesValidation="True" CssClass="boton40"
                                    CommandName="Update" Text="Actualizar"></asp:LinkButton>
                                <asp:LinkButton ID="btnEliminar" runat="server" CausesValidation="False" CssClass="boton40"
                                    CommandName="Cancel" Text="Cancelar"></asp:LinkButton>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:LinkButton ID="btnEditar" runat="server" CausesValidation="False" CssClass="boton40"
                                    CommandName="Edit" Text="Editar"></asp:LinkButton>
                                <asp:LinkButton ID="btnEliminar" runat="server" CausesValidation="False" CssClass="boton40"
                                    CommandName="Delete" Text="Eliminar" OnClientClick="return Eliminar('la Especificacion');"></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>
        </div>
        <div id="tabs-2" class="tabla_admin" style="height: auto;">
            <div class="tabla_admin">
                <div class="label_admint">
                    <label>
                        Detalle del Servicio:
                    </label>
                </div>
                <div class="label_adminc">
                    <asp:TextBox ID="txtDescripcion" CssClass="sol_campoadm" runat="server"></asp:TextBox>
                </div>
                <div class="label_adminc">
                </div>
                <div id="ser_tabla1_tit"> Datos de Tecnicos del Detalle</div>             
                <div id="ser_tabla1_txt"> Seleccione los items necesarios para dar de alta el detalle del servicio</div>    
                
                <div class="label_admint">
                    <label>
                        1) Cuadrilla:
                    </label>
                </div>
                <div class="label_admint">
                    <asp:CheckBox ID="chkCuadrilla" runat="server" ToolTip="Seleccione Cuadrillas si son necesarias para brindar el servicio"
                        Text="No" onchange="CambiarCheck(this,false);mostrarCuadrilla(this);"></asp:CheckBox>
                </div>
                <div class="label_admint">
                </div>
                <div class="label_adminc">
                </div>
                <div id="divCuadrilla" runat="server" style="display: none; height: 130px;">
                    <div class="label_admint2" style="height: 90%;">
                        <label>
                            Seleccione las Cuadrillas indicadas para vincularlas con el detalle:
                        </label>
                    </div>
                    <div class="label_esp" style="overflow-y: scroll; width: 78%; height: 90%;">
                        <asp:CheckBoxList ID="chkCuadrillas" CellPadding="5" CellSpacing="10" runat="server" RepeatColumns="4">
                        </asp:CheckBoxList>
                    </div>
                </div>
                <div class="label_admint">
                    <label>
                        2) Equipo:
                    </label>
                </div>
                <div class="label_admint">
                    <asp:CheckBox ID="chkEquipo" runat="server" ToolTip="Seleccione Equipos si son necesarios para brindar el servicio"
                        Text="No" onchange="CambiarCheck(this,true);"></asp:CheckBox>
                </div>
                <div class="label_admint">
                </div>
                <div class="label_adminc">
                </div>
                <div id="divPropio" runat="server" style="display: none;">
                    <div class="label_admint_ab">
                        <label>
                            Son Equipos Propios?
                        </label>
                    </div>
                    <div class="label_admint_ab">
                        <asp:CheckBox ID="chkPropio" runat="server" ToolTip="Seleccionar si los equipos son propios."
                            Text="No" onchange="CambiarCheck(this,false); mostrarEq(this, true);"></asp:CheckBox>
                    </div>
                    <div class="label_admint_ab">
                    </div>
                    <div class="label_admint_ab">
                    </div>
                    <div class="label_admint_ab">
                    </div>
                </div>
                <div id="divEquiposPropios" runat="server" style="display: none; height: 500px;">
                    <div class="label_admint2" style="height: 90%;">
                        <label>
                            Equipos Propios:
                        </label>
                    </div>
                    <div id="td_Chk" class="label_esp" style="overflow-y: scroll; width: 78%; height: 90%;">
                        <asp:TextBox runat="server" id="txtFiltroEq" class="sol_campoadm"
                            ToolTip="Escribe para filtrar los equipos" 
                            onkeyup="javascript:textChange(event,this,'Chk');" />
                            
                        <asp:CheckBoxList ID="chkEquiposPropios" runat="server" CellPadding="5" CellSpacing="10" RepeatColumns="4" RepeatDirection="Horizontal">
                        </asp:CheckBoxList>
                    </div>
                </div>
                <div id="divNoPropio" runat="server" style="display: none;">
                    <div class="label_admint_ab">
                        <label>
                            Son Equipos del Cliente?
                        </label>
                    </div>
                    <div class="label_admint_ab">
                        <asp:CheckBox ID="chkNoPropio" runat="server" ToolTip="Seleccionar si los equipos son del cliente."
                            Text="No" onchange="CambiarCheck(this,false); mostrarEq(this,false);"></asp:CheckBox>
                    </div>
                    <div class="label_admint_ab">
                    </div>
                    <div class="label_admint_ab">
                    </div>
                    <div class="label_admint_ab">
                    </div>
                </div>
                <div id="divEqClientes" runat="server" style="display: none; min-height: 500px;">
                    <div class="label_admint2" style="height: 90%;">
                        <label>
                            Equipos del Cliente:
                        </label>
                    </div>
                    <div id="td_Chk2" class="label_esp" style="overflow-y: scroll; width: 78%; height: 90%;">
                        <asp:TextBox runat="server" id="txtFiltroEqCliente" class="sol_campoadm"
                            ToolTip="Escribe para filtrar los equipos" 
                            onkeyup="javascript:textChange(event,this,'Chk2');" />
                            
                        <asp:CheckBoxList ID="chkEquiposCli" RepeatColumns="4" CellPadding="5" CellSpacing="10"  runat="server" RepeatDirection="Horizontal">
                        </asp:CheckBoxList>
                    </div>
                </div>
                <div class="label_adminc">
                </div>
                <div class="label_admintb"></div>
                <div class="label_admintb">
                    <asp:Button ID="btnGuardar" runat="server" Text="Guardar" CssClass="btn_form" OnClick="btnGuardar_Click"
                        OnClientClick="return ValidarObj('txtDescripcion|Ingrese una Descripción.');" /></div>
                <div class="label_admintb">
                    <asp:Button ID="btnCancelar" runat="server" Text="Cancelar" CssClass="btn_form" OnClick="btnCancelar_Click" />
                </div>
            </div>
        </div>
    </div>
</asp:Content>
