﻿<%@ Page Title="" Language="C#" MasterPageFile="~/frmPrincipal.Master" AutoEventWireup="true" CodeBehind="frmGestionCuadrilla.aspx.cs" EnableEventValidation="false" Inherits="ProcesoPP.UI.frmGestionCuadrilla" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %> 
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<script type="text/javascript" >
    $(document).ready(function () { SetMenu("liCargaDatos"); }); 
</script>
<script src="../Js/jquery.maskedinput.js" type="text/javascript"></script>
 <script type="text/javascript">
     jQuery(function ($) {
         $("#<%=txtFecha.ClientID%>").mask("99/99/9999", { placeholder: "dd/mm/yyyy" });
     });
     jQuery(function ($) {
         $("#<%=txtFechaHasta.ClientID%>").mask("99/99/9999", { placeholder: "dd/mm/yyyy" });
     });
     </script>

</asp:Content>
<asp:Content ID="cphContenedor" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div id="contenido" runat="server">

    <div id="apertura">Gestión de Cuadrillas</div> 
        <div class="solicitud100">
            <div class="sol_tit50"> <label>Fecha Desde</label></div>
            <div class="sol_campo30"><asp:TextBox runat="server" CssClass="sol_campo25" id="txtFecha" /></div>
            <div class="sol_fecha"><img id="btnFecha" alt="calendario" src="../img/calendario.gif" style="width:20px; height:20px; cursor:pointer;" onmouseover="CambiarConfiguracionCalendario('txtFecha',this);"/></div>
<div class="sol_fecha"></div>  

 <div class="sol_tit50"> <label>Fecha Hasta</label></div>
            <div class="sol_campo30"><asp:TextBox runat="server" CssClass="sol_campo25" id="txtFechaHasta" /></div>
            <div class="sol_fecha"><img id="btnFechaHasta" alt="calendario" src="../img/calendario.gif" style="width:20px; height:20px; cursor:pointer;" onmouseover="CambiarConfiguracionCalendario('txtFechaHasta',this);"/></div>
        </div>

<cc1:toolkitscriptmanager id="ToolkitScriptManager1" runat="server">
        </cc1:toolkitscriptmanager>
    <div class="solicitud100">
        <div class="sol_tit50"> <label>Persona</label></div>
        <div class="sol_campo30"><asp:DropDownList runat="server" CssClass="sol_campo50c" id="ddlPersona" />
           <cc1:cascadingdropdown id="ccdPersona" targetcontrolid="ddlPersona" prompttext="N/A"
                promptvalue="0" servicemethod="GetPersona" runat="server" category="idPersonal"
                loadingtext="Cargando..." /></div> 
        
<div class="sol_fecha"></div> 
<div class="sol_fecha"></div> 

<div class="sol_tit50"> <label>Cuadrilla</label></div>
<div class="sol_campo30"><asp:DropDownList runat="server" CssClass="sol_campo50c" id="ddlCuadrilla" />
<cc1:cascadingdropdown id="ccdCuadrilla" targetcontrolid="ddlCuadrilla" prompttext="N/A"
                promptvalue="0" servicemethod="GetCuadrilla" runat="server" category="idCuadrilla"
                loadingtext="Cargando..." /></div> </div> 
</div>




<div class="solicitud100">

<div class="sol_tit50"> <label>Empresa</label></div>

<div class="sol_campo30"><asp:DropDownList  runat="server" CssClass="sol_campo50c" id="ddlEmpresa" />
<cc1:cascadingdropdown id="cddEmpresa" targetcontrolid="ddlEmpresa" prompttext="N/A"
                promptvalue="0" servicemethod="GetEmpresa" runat="server" category="idEmpresa"
                loadingtext="Cargando..." /></div>  
        
<div class="sol_fecha"></div> 
<div class="sol_fecha"></div> 
        
<div class="sol_tit50"> <label>Sector</label></div>

<div class="sol_campo30"><asp:DropDownList  runat="server" CssClass="sol_campo50c" id="ddlSector" />
<cc1:cascadingdropdown id="cddSector" targetcontrolid="ddlSector" prompttext="N/A"
                promptvalue="0" servicemethod="GetSector" runat="server" category="idSector"
                parentcontrolid="ddlEmpresa" loadingtext="Cargando..." /></div>    
        
</div>


<div class="solicitud100sin">
<div class="sol_btn">
            <asp:Button runat="server" id="btnBuscar" cssclass="btn_form" Text="Buscar" onclick="btnBuscar_Click" />
            
<asp:Button runat="server" ID="btnExportar" CssClass="btn_form" Text="Exportar" OnClick="btExportar_Click" />
        </div>
    </div>

    <asp:GridView ID="gvCuadrilla" runat="server" AutoGenerateColumns="False" 
         DataMember="idCuadrilla" CssClass="tabla3" 
            onrowcommand="gvCuadrilla_RowCommand" onrowdatabound="gvCuadrilla_RowDataBound"
            AllowPaging="true" PageSize="20" onpageindexchanging="gvCuadrilla_PageIndexChanging"
        PagerStyle-CssClass="tabla_font">
         <Columns>
             <asp:TemplateField HeaderText="CUADRILLA" HeaderStyle-CssClass="celda20t" ItemStyle-CssClass="celda20">
                 <ItemTemplate>
                     <asp:Label ID="lblCuadrilla" runat="server" Text='<%# Bind("Cuadrilla") %>'></asp:Label>
                 </ItemTemplate>
             </asp:TemplateField>
             <asp:TemplateField HeaderText="PERSONA" HeaderStyle-CssClass="celda30t" ItemStyle-CssClass="celda30">
                 <ItemTemplate>
                     <asp:Label ID="lblPersona" runat="server" Text='<%# Bind("Persona") %>'></asp:Label>
                 </ItemTemplate>
             </asp:TemplateField>
             <asp:TemplateField  HeaderText="SECTOR" HeaderStyle-CssClass="celda20t" ItemStyle-CssClass="celda20">
                <ItemTemplate>
                     <asp:Label ID="lblSector" runat="server" Text='<%# Bind("Sector") %>'></asp:Label>
                 </ItemTemplate>
             </asp:TemplateField>
             <asp:TemplateField  HeaderText="FECHA INCIO" HeaderStyle-CssClass="celda15t" ItemStyle-CssClass="celda15">
                <ItemTemplate>
                     <asp:Label ID="lblFechaInicio" runat="server" Text='<%# Bind("FechaInicio") %>'></asp:Label>
                 </ItemTemplate>
             </asp:TemplateField>             
             <asp:TemplateField  HeaderText="FECHA FIN" HeaderStyle-CssClass="celda15t" ItemStyle-CssClass="celda15">
                <ItemTemplate>
                     <asp:Label ID="lblFechaBaja" runat="server" Text='<%# Bind("FechaDesafectacion") %>'></asp:Label>
                 </ItemTemplate>
             </asp:TemplateField>
             <asp:TemplateField ItemStyle-CssClass="celda10b" HeaderStyle-CssClass="celda10t">
                <ItemTemplate>
                    <asp:LinkButton ID="btnVer" runat="server" title="Ver" CssClass="ver" CommandArgument='<%# Bind("idCuadrilla") %>'
                        CommandName="Ver"></asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
         </Columns>
     </asp:GridView>

</asp:Content>
