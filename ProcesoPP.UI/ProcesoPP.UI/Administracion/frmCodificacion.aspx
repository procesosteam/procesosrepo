﻿<%@ Page Title="" Language="C#" MasterPageFile="~/frmPrincipal.Master" AutoEventWireup="true"
    CodeBehind="frmCodificacion.aspx.cs" Inherits="ProcesoPP.UI.Administracion.frmCodificacion" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        $(document).ready(function () { SetMenu("liCargaDatos"); }); 
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="apertura">
        ALTA | BAJA | MODIFICACION DE CODIFICACION</div>
    <div class="tabla_admin">
        <div class="sol_adm100">
            <div class="label_admint">
                <label>
                    Descripción:
                </label>
            </div>
            <div class="label_adminc">
                <asp:TextBox ID="txtCodificacion" CssClass="sol_campoadm" runat="server"></asp:TextBox></div>
            <div class="label_admint">
            </div>
            <div class="label_admint">
            </div>
            <div class="label_admint">
                <label>
                    Codificación:</label></div>
            <div class="label_adminc">
                <asp:TextBox ID="txtCodigo" CssClass="sol_campoadm" runat="server"></asp:TextBox></div>
            <div class="label_admint">
            </div>
            <div class="label_admint">
            </div>
            <%--<div class="label_admint"><label>Pertenece a : </label></div> --%>
            <div class="label_admint">
                <label>
                    Pertenece a:</label></div>
            <div class="label_admint">
                Equipo</div>
            <div class="label_admint">
                <asp:RadioButton CssClass="check_adm" GroupName="radioEquipo" ID="reSi" runat="server" /></div>
            <div class="label_admint">
                Vehiculo</div>
            <div class="label_admint">
                <asp:RadioButton CssClass="check_adm" GroupName="radioEquipo" ID="reNo" runat="server" /></div>
            <div class="label_adminc">
            </div>
            <div class="label_admintb">
                <asp:Button ID="btnGuardar" runat="server" Text="Guardar" CssClass="btn_form" OnClick="btnGuardar_Click"
                    OnClientClick="return ValidarObj('txtCodificacion|Ingrese una Descripción.'+'~txtCodigo|Ingrese un Codigo');" />
            </div>
            <div class="label_relleno20">
            </div>
        </div>
    </div>
    <div id="divMensaje" runat="server">
        <asp:Label ID="lblError" runat="server" Text="Error" Visible="false"></asp:Label>
    </div>
    <div>
        <asp:GridView ID="gvCodificacion" runat="server" AutoGenerateColumns="False" DataMember="idCodificacion"
            CssClass="tabla3" OnRowCancelingEdit="gvCodificacion_RowCancelingEdit" OnRowEditing="gvCodificacion_RowEditing"
            OnRowUpdating="gvCodificacion_RowUpdating" OnRowDeleting="gvCodificacion_RowDeleting"
            OnRowDataBound="gvCodificacion_RowDataBound">
            <Columns>
                <asp:TemplateField HeaderText="Id Codificacion" HeaderStyle-CssClass="columna5" ItemStyle-CssClass="columna5"
                    Visible="false">
                    <EditItemTemplate>
                        <asp:Label ID="lblIdCodificacion" runat="server" Text='<%# Bind("idCodificacion") %>'></asp:Label>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lblCodificacionI" runat="server" Text='<%# Bind("idCodificacion") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="DESCRIPCION" HeaderStyle-CssClass="celda50t" ItemStyle-CssClass="celda50">
                    <EditItemTemplate>
                        <asp:TextBox ID="txtDescripcion" runat="server" Text='<%# Bind("Descripcion") %>'></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lblDescripcion" runat="server" Text='<%# Bind("Descripcion") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Código" HeaderStyle-CssClass="celda20t" ItemStyle-CssClass="celda20">
                    <EditItemTemplate>
                        <asp:TextBox ID="txtCodigo" runat="server" Text='<%# Bind("Codigo") %>'></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lblCodigo" runat="server" Text='<%# Bind("Codigo") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Pertenece a Equipo" HeaderStyle-CssClass="celda20t"
                    ItemStyle-CssClass="celda20">
                    <EditItemTemplate>
                        <asp:CheckBox ID="txtEquipo" Enabled="true" runat="server" Checked='<%# Bind("equipo") %>' />
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:CheckBox ID="lblEquipo" Enabled="false" runat="server" Checked='<%# Bind("equipo") %>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField ShowHeader="False" HeaderStyle-CssClass="celda15t" ItemStyle-CssClass="celda15b">
                    <EditItemTemplate>
                        <asp:LinkButton ID="btnEditar" runat="server" CausesValidation="True" CssClass="boton40"
                            CommandName="Update" Text="Actualizar"></asp:LinkButton>
                        <asp:LinkButton ID="btnEliminar" runat="server" CausesValidation="False" CssClass="boton40"
                            CommandName="Cancel" Text="Cancelar"></asp:LinkButton>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:LinkButton ID="btnEditar" runat="server" CausesValidation="False" CssClass="boton40"
                            CommandName="Edit" Text="Editar"></asp:LinkButton>
                        <asp:LinkButton ID="btnEliminar" runat="server" CausesValidation="False" CssClass="boton40"
                            CommandName="Delete" Text="Eliminar" OnClientClick="return Eliminar('la Codificacion');"></asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
    </div>
</asp:Content>
