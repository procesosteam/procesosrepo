﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ProcesoPP.Model;
using ProcesoPP.Business;
using ProcesoPP.Services;

namespace ProcesoPP.UI.Administracion
{
    public partial class frmCambioContrasenia : System.Web.UI.Page
    {
        private int _idUsuario
        {
            get { return (int)ViewState["idUsuario"]; }
            set { ViewState["idUsuario"] = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["idUsuario"] != null)
                {
                    _idUsuario = int.Parse(Session["idUsuario"].ToString());
                    CargarUsuario();
                }
                else
                    Mensaje.mostrarPopUp(this);
            }
        }

        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            GuardarNuevaContrasenia();
        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            BuscarUser();
        }

        private void BuscarUser()
        {
            Usuario oUsuario = new Usuario();
            UsuarioBus oUsuarioBus = new UsuarioBus();
            oUsuario = oUsuarioBus.UsuarioGetByUserName(txtNombreUser.Text);
            if (oUsuario != null)
            {
                lblUser.Text = oUsuario.UserName;
                _idUsuario = oUsuario.IdUsuario;
                txtActual.Text = oUsuario.Clave;
                txtActual.Enabled = false;
            }
            else
                Mensaje.errorMsj("No existe usuario", this.Page, "ERROR", "../frmLogin.aspx");
        }

        private void CargarUsuario()
        {
            Usuario oUsuario = new Usuario();
            UsuarioBus oUsuarioBus = new UsuarioBus();
            oUsuario = oUsuarioBus.UsuarioGetById(_idUsuario);
            lblUser.Text = oUsuario.UserName;
        }

        private void GuardarNuevaContrasenia()
        {
            if (_idUsuario != 0)
            {
                if (txtNueva.Text == txtRepetida.Text)
                {
                    Usuario oUsuario = new Usuario();
                    UsuarioBus oUsuarioBus = new UsuarioBus();
                    Utility oUtility = new Utility();
                    oUsuario = oUsuarioBus.UsuarioGetById(_idUsuario);
                    oUsuario.Clave = oUtility.Encrypt(txtRepetida.Text);
                    if (oUsuarioBus.UsuarioUpdate(oUsuario))
                    {
                        Mensaje.successMsj("LA CLAVE SE HA CAMBIADO!", this.Page, "EXITO", "../frmLogin.aspx");
                    }
                }
                else
                {
                    Mensaje.errorMsj("Debe ingresar las claves identicas.", this.Page, "Error", null);
                }
            }
            
        }

    }
}
