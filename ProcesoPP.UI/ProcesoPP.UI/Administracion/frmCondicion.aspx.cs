﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ProcesoPP.Model;
using ProcesoPP.Business;
using ProcesoPP.Services;

namespace ProcesoPP.UI.Administracion
{
    public partial class frmCondicion : System.Web.UI.Page
    {
        #region PROPIEDADES

        public Permisos _Permiso
        {
            get { return (Permisos)ViewState["permiso"]; }
            set { ViewState["permiso"] = value; }
        }

        public int _idUsuario
        {
            get { return (int)ViewState["idUsuario"]; }
            set { ViewState["idUsuario"] = value; }
        }

        #endregion
        
        #region EVENTOS

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                _idUsuario = Common.EvaluarSession(Request, this.Page);
                if (_idUsuario != 0)
                {
                    _Permiso = Common.getModulo(Request, this.Page);
                    if (_Permiso != null && _Permiso.Lectura)
                    {
                        Mensaje.ocultarDivMsj(divMensaje);
                        if (!IsPostBack)
                        {
                            CargarCondiciones();
                            InitScrren();
                        }
                    }
                    else
                    {
                        Mensaje.errorMsj("NO TIENE PERMISOS A ESTE FORMULARIO", this.Page, "SIN ACCESO", "../frmPrincipal.aspx");
                    }
                }
                else
                {
                    Mensaje.errorMsj("DEBE LOGUEARSE", this.Page, "LOGIN", "../frmLogin.aspx");
                }
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                Guardar();
                CargarCondiciones();
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        protected void gvCoondicion_RowEditing(object sender, GridViewEditEventArgs e)
        {
            gvCoondicion.EditIndex = e.NewEditIndex;
            CargarCondiciones();
        }

        protected void gvCoondicion_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            gvCoondicion.EditIndex = -1;
            CargarCondiciones();
        }

        protected void gvCoondicion_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                Modificar(gvCoondicion.Rows[e.RowIndex]);
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        protected void gvCoondicion_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                Eliminar(gvCoondicion.Rows[e.RowIndex]);
            }
            catch (Exception ex)
            {                
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        protected void gvCoondicion_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Common.CargarRow(e, _Permiso);
            }
        }

        #endregion

        #region Metodos

        private void InitScrren()
        {
            if (!_Permiso.Escritura)
            {
                btnGuardar.Visible = false;
            }
        }

        private void Eliminar(GridViewRow row)
        {
           // Mensaje.Eliminar("la condición", this.Page);

            int id = int.Parse((row.FindControl("lblCondicionI") as Label).Text);
            CondicionBus oCondicionBus = new CondicionBus();

            if (oCondicionBus.CondicionDelete(id))
            {
                Mensaje.successMsj("Condición Eliminada con éxito.", this.Page, "Eliminado", "");
                CargarCondiciones();
            }
        }
         
        private void Modificar(GridViewRow row)
        {
            int id = int.Parse((row.FindControl("lblIdCondicion") as Label).Text);
            string descripcion = (row.FindControl("txtDescripcion") as TextBox).Text;
            CondicionBus oCondicionBus = new CondicionBus();
            Condicion oCondicion = new Condicion(id, descripcion);

            if (oCondicionBus.CondicionUpdate(oCondicion))
            {
                Mensaje.successMsj("Modificado con Exito!", this.Page, "Modificado", null);
                gvCoondicion.EditIndex = -1;
                CargarCondiciones();
            }
        }

        private void Guardar()
        {
            CondicionBus oCondicionBus = new CondicionBus();
            Condicion oCondicion = new Condicion(0, txtCondicion.Text);
            oCondicion.IdCondicion = oCondicionBus.CondicionAdd(oCondicion);

            if (oCondicion.IdCondicion > 0)
            {
                Mensaje.successMsj("Guardado con Exito!", this.Page, "Agregado", null);
                txtCondicion.Text = "";
                CargarCondiciones();
            }
        }

        private void CargarCondiciones()
        {
            CondicionBus oCondicionBus = new CondicionBus();
            gvCoondicion.DataSource = oCondicionBus.CondicionGetAll();
            gvCoondicion.DataBind();
        }
                
        #endregion

           
    }
}
