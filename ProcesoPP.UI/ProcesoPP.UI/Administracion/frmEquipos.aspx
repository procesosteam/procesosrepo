﻿<%@ Page Title="" Language="C#" MasterPageFile="~/frmPrincipal.Master" AutoEventWireup="true" CodeBehind="frmEquipos.aspx.cs" Inherits="ProcesoPP.UI.Administracion.frmEquipos" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<link rel="stylesheet" href="../Css/jquery-ui.css" />
<script type="text/javascript">
    $(document).ready(function () { SetMenu("liCargaDatos"); });

    $(document).ready(function () {
        $("#tabs").tabs({
            active: function () {
                var newIdx = $("#tabs").tabs('option', 'active');
                $('#<%=hdidLastTab.ClientID%>').val(newIdx);
            },
            /*heightStyle: "auto",*/
            active: previuslySelectedTab,
            show: { effect: "fadeIn", duration: 500 }
        });
    });
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="apertura">EQUIPOS DEL CLIENTE</div>

     <asp:HiddenField runat="server" ID="hdidLastTab" Value="0" />
    <div id="tabs">
        <ul>
            <li><a href="#tabs-1" tabindex="1">Búsqueda</a></li>
            <li><a href="#tabs-2" tabindex="2">Alta</a></li>
        </ul>
        <div id="tabs-1" class="tabla_admin" style="height: auto;">
            <div class="label_admint">
                <label>
                    Cliente:
                </label>
            </div>
            <div class="label_adminc">
                <asp:DropDownList ID="ddlCliente" CssClass="sol_campoadm" runat="server"></asp:DropDownList></div>
            <div class="label_adminc">
            </div>
            <div class="label_adminc">
            </div>
            <div class="label_adminc">
            </div>
            <div class="label_admintb">
                <asp:Button ID="btnBuscar" runat="server" Text="Buscar" CssClass="btn_form" OnClick="btnBuscar_Click" />
            </div>
            <div>
             <asp:GridView ID="gvEquipo" runat="server" AutoGenerateColumns="False" 
                 DataMember="idEquipo" CssClass="tabla3" 
                 onrowcancelingedit="gvEquipo_RowCancelingEdit" 
                 onrowediting="gvEquipo_RowEditing" 
                 onrowupdating="gvEquipo_RowUpdating" 
                 onrowdeleting="gvEquipo_RowDeleting"
                 OnRowDataBound="gvEquipo_RowDataBound">
                 <Columns>
                     <asp:TemplateField HeaderText="Id Equipo" HeaderStyle-CssClass="celda30t" ItemStyle-CssClass="celda30" Visible="false">
                         <EditItemTemplate>
                             <asp:Label ID="lblIdEquipo" runat="server" Text='<%# Bind("idEquipo") %>'></asp:Label>
                         </EditItemTemplate>
                         <ItemTemplate>
                             <asp:Label ID="lblEquipoI" runat="server" Text='<%# Bind("idEquipo") %>'></asp:Label>
                         </ItemTemplate>
                     </asp:TemplateField>
                     <asp:TemplateField HeaderText="DESCRIPCION" HeaderStyle-CssClass="celda40t" ItemStyle-CssClass="celda40">
                         <EditItemTemplate>
                             <asp:TextBox ID="txtDescripcion" runat="server" Text='<%# Bind("Descripcion") %>'></asp:TextBox>
                         </EditItemTemplate>
                         <ItemTemplate>
                             <asp:Label ID="lblDescripcion" runat="server" Text='<%# Bind("Descripcion") %>'></asp:Label>
                         </ItemTemplate>
                     </asp:TemplateField>
                       <asp:TemplateField HeaderText="CLIENTE" HeaderStyle-CssClass="celda40t" ItemStyle-CssClass="celda40">
                         <EditItemTemplate>
                             <asp:DropDownList ID="ddlEmpresa" runat="server"></asp:DropDownList>
                             <asp:HiddenField runat="server" ID="hfIdCliente" value='<%# Bind("idEmpresa") %>'/>
                         </EditItemTemplate>
                         <ItemTemplate>
                             <asp:DropDownList ID="ddlEmpresa" runat="server" Enabled="false" CssClass="cajaTextoGrilla"></asp:DropDownList>
                             <asp:HiddenField runat="server" ID="hfIdCliente" value='<%# Bind("idEmpresa") %>'/>
                         </ItemTemplate>
                     </asp:TemplateField>
                     <asp:TemplateField ShowHeader="False" HeaderStyle-CssClass="celda15t" ItemStyle-CssClass="celda15b">
                         <EditItemTemplate>
                             <asp:LinkButton ID="btnEditar" runat="server" CausesValidation="True" CssClass="boton40"
                                 CommandName="Update" Text="Actualizar"></asp:LinkButton>
                             <asp:LinkButton ID="btnEliminar" runat="server" CausesValidation="False" CssClass="boton40"
                                 CommandName="Cancel" Text="Cancelar"></asp:LinkButton>
                         </EditItemTemplate>
                         <ItemTemplate>
                             <asp:LinkButton ID="btnEditar" runat="server" CausesValidation="False" CssClass="boton40"
                                 CommandName="Edit" Text="Editar" ></asp:LinkButton>
                             <asp:LinkButton ID="btnEliminar" runat="server" CausesValidation="False" CssClass="boton40"
                                 CommandName="Delete" Text="Eliminar" OnClientClick="return Eliminar('el Equipo');"></asp:LinkButton>
                         </ItemTemplate>
                     </asp:TemplateField>
                 </Columns>
             </asp:GridView>
            </div>
        </div>
        <div id="tabs-2" class="tabla_admin" style="height: auto;">

            <div class="tabla3">
                <div class="label_admint">
                    Identificación: </div> 
                <div class="label_adminc">
                    <asp:TextBox ID="txtDescripcion" runat="server" CssClass="sol_campoadm"></asp:TextBox>
                </div>
                <div class="label_relleno20"></div>
                <div class="label_relleno20"></div>
                <div class="label_admint">
                    Cliente:
                </div>
                <div class="label_adminc">            
                    <asp:DropDownList runat="server" ID="ddlEmpresa" CssClass="sol_campoadm">
                    </asp:DropDownList>
                </div>
                <div class="label_relleno20"></div>
                <div class="label_admintb">
                    <asp:Button ID="btnGuardar" runat="server" Text="Guardar"  CssClass="btn_form"
                        onclick="btnGuardar_Click" OnClientClick="return ValidarObj('txtDescripcion|Ingrese una Descripción.~ddlEmpresa|Debe seleccionar una empresa.');" />
                </div>
            </div>
        </div>
   </div>

</asp:Content>
