﻿<%@ Page Language="C#" AutoEventWireup="true"  MasterPageFile="~/frmPrincipal.Master" MaintainScrollPositionOnPostback="true" CodeBehind="frmPermisos.aspx.cs" Inherits="ProcesoPP.UI.Administracion.frmPermisos" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server"> 
<script type="text/javascript" >
    $(document).ready(function() { SetMenu("liCargaDatos,liAdministracion"); }); 
</script>
</asp:Content>
<asp:Content ID="cphContenedor" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="apertura">PERMISOS</div>
<div class="tabla_admin">
   <div class="label_admint"><label>Modulos</label></div>
   <div class="label_adminc">
        <asp:DropDownList ID="ddlModulo" runat="server" CssClass="sol_campoadm">
        </asp:DropDownList>
    </div>
    <div class="label_relleno20"></div>
    <div class="label_relleno20"></div>
    <div class="label_admint"><label>Tipo Usuario</label></div>
    <div class="label_adminc">
        <asp:DropDownList ID="ddlTipoUsuario" runat="server" CssClass="sol_campoadm">
        </asp:DropDownList>       
    </div>     
    <div class="label_relleno20"></div>
    <div class="label_relleno20"></div>
    <div class="label_relleno20"></div>
    <div class="label_admint">
        <asp:CheckBox runat="server" ID="chkLectura" Text="Lectura" />
    </div>
    <div class="label_admint">
        <asp:CheckBox runat="server" ID="chkEscritura" Text="Escritura" />
    </div>
    <div class="label_relleno20"></div>
    <div class="label_admintb"><asp:Button runat="server" id="btnGuardar" CssClass="btn_form" Text="Guardar"
        OnClientClick="return ValidarObj('ddlModulo|Debe seleccionar un modulo.~ddlTipoUsuario|Debe seleccionar el Tipo de Usuario.');"
        onclick="btnGuardar_Click"/></div>
</div>
<div>
    <asp:GridView ID="gvPermisos" runat="server" CssClass="tabla3" 
                    AutoGenerateColumns="false"
                    onrowdeleting="gvPermisos_RowDeleting"
                    OnRowDataBound="gvPermisos_RowDataBound">
     <Columns>
             <asp:TemplateField Visible="false">
                 <ItemTemplate>
                     <asp:Label ID="lblIdPermiso" runat="server" Text='<%# Bind("idPermiso") %>'></asp:Label>
                 </ItemTemplate>
             </asp:TemplateField>
             <asp:TemplateField HeaderText="Modulo" HeaderStyle-CssClass="celda30t" ItemStyle-CssClass="celda30" SortExpression="Modulo"> 
                 <ItemTemplate>
                     <asp:Label ID="lblModulo" runat="server" Text='<%# Bind("oModulo.Descripcion") %>'></asp:Label>
                 </ItemTemplate>
             </asp:TemplateField>
              <asp:TemplateField HeaderText="TipoUsuario" HeaderStyle-CssClass="celda30t" ItemStyle-CssClass="celda30"> 
                 <ItemTemplate>
                     <asp:Label ID="lblTipoUsuario" runat="server" Text='<%# Bind("oTipoUsuario.Descripcion") %>'></asp:Label>
                 </ItemTemplate>
             </asp:TemplateField>
             <asp:TemplateField HeaderText="Lectura" HeaderStyle-CssClass="celda10t" ItemStyle-CssClass="celda10">                 
                 <ItemTemplate>
                     <asp:CheckBox ID="chkLectura" runat="server" Checked='<%# Bind("Lectura") %>' Enabled="false"/>
                 </ItemTemplate>
             </asp:TemplateField>
               <asp:TemplateField HeaderText="Escritura" HeaderStyle-CssClass="celda10t" ItemStyle-CssClass="celda10">                 
                 <ItemTemplate>
                     <asp:CheckBox ID="chkEscritura" runat="server" Checked='<%# Bind("Escritura") %>' Enabled="false"/>
                 </ItemTemplate>
             </asp:TemplateField>
             <asp:TemplateField HeaderStyle-CssClass="celda20t" ItemStyle-CssClass="celda20">             
                 <ItemTemplate >               
                     <asp:LinkButton ID="btnEliminar" runat="server" CausesValidation="False" CssClass="boton40"
                         CommandName="Delete" Text="Eliminar" OnClientClick="return Eliminar('el Permiso');"></asp:LinkButton>
                 </ItemTemplate>
             </asp:TemplateField>             
         </Columns>
    </asp:GridView>
</div>
</asp:Content>