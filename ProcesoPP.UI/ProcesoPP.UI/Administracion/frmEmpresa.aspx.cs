﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ProcesoPP.Business;
using ProcesoPP.Model;
using ProcesoPP.Services;
using System.Text;

namespace ProcesoPP.UI.Administracion
{
    public partial class frmEmpresa : System.Web.UI.Page
    {
        #region PROPIEDADES

        public Permisos _Permiso
        {
            get { return (Permisos)ViewState["permiso"]; }
            set { ViewState["permiso"] = value; }
        }

        public int _idUsuario
        {
            get { return (int)ViewState["idUsuario"]; }
            set { ViewState["idUsuario"] = value; }
        }


        public int _idEmpresa
        {
            get { return (int)ViewState["idEmpresa"]; }
            set { ViewState["idEmpresa"] = value; }
        }

        #endregion

        #region EVENTOS

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                 _idUsuario = Common.EvaluarSession(Request, this.Page);
                 if (_idUsuario != 0)
                 {
                     _Permiso = Common.getModulo(Request, this.Page);
                     if (_Permiso != null && _Permiso.Lectura)
                     {
                         Mensaje.ocultarDivMsj(divMensaje);
                         if (!IsPostBack)
                         {                             
                             InitScren();
                         }
                     }
                     else
                     {
                         Mensaje.errorMsj("NO TIENE PERMISOS A ESTE FORMULARIO", this.Page, "SIN ACCESO", "../frmPrincipal.aspx");
                     }
                 }
                 else
                 {
                     Mensaje.errorMsj("DEBE LOGUEARSE", this.Page, "LOGIN", "../frmLogin.aspx");
                 }
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null); ;
            }
        }

        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                Guardar();                
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        protected void gvEmpresa_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                Eliminar(gvEmpresa.Rows[e.RowIndex]);
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        protected void gvEmpresa_RowEditing(object sender, GridViewEditEventArgs e)
        {
            MostrarEmpresa(e.NewEditIndex);            
        }
        
        protected void gvEmpresa_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            gvEmpresa.EditIndex = -1;
            CargarEmpresas(txtBusqueda.Text);
        }

        protected void gvEmpresa_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Common.CargarRow(e, _Permiso);
            }
        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                CargarEmpresas(txtBusqueda.Text);
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        protected void btnCancelar_Click(object sender, EventArgs e)
        {
            LimpiarControles();
            cambiarHidden("0");
        }

        #endregion

        #region METODOS

        private void cambiarHidden(string val)
        {
            hdidLastTab.Value = val;
            string hiddenField = hdidLastTab.Value;
            StringBuilder js = new StringBuilder();
            js.Append("<script type='text/javascript'>");
            js.Append("var previuslySelectedTab = ");
            js.Append(hiddenField);
            js.Append("</script>");
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "acttab", js.ToString());

        }
        private void InitScren()
        {
            _idEmpresa = 0;
            if (!_Permiso.Escritura)
            {
                btnGuardar.Visible = false;
            }
            CargarCondicionIVA(ddlCondicionIva);
            CargarEmpresas(string.Empty);
            
        }

        private void Eliminar(GridViewRow row)
        {
            int id = int.Parse((row.FindControl("lblEmpresaI") as Label).Text);
            EmpresaBus oEmpresaBus = new EmpresaBus();

            if (oEmpresaBus.EmpresaDelete(id))
            {
                Mensaje.successMsj("Empresa Eliminada con éxito.", this.Page, "Eliminado", "");
                CargarEmpresas("");
            }
        }

        private void MostrarEmpresa(int rowIndex)
        {
            GridViewRow row = gvEmpresa.Rows[rowIndex];
            int id = int.Parse((row.FindControl("lblEmpresaI") as Label).Text);

            EmpresaBus oEmpresaBus = new EmpresaBus();
            Empresa oEmpresa = oEmpresaBus.EmpresaGetById(id);
            _idEmpresa = id;
            txtDescripcion.Text = oEmpresa.Descripcion;
            txtRazonSocial.Text = oEmpresa.RazonSocial;
            txtDomicilio.Text = oEmpresa.Domicilio;
            txtCuit.Text = oEmpresa.CUIT;
            ddlCondicionIva.SelectedItem.Text = oEmpresa.CondIVA ;
            chkInterna.Checked = oEmpresa.Interna ;
            txtTango.Text = oEmpresa.CodTango;

            cambiarHidden("1");
        }

       
        public void CargarCondicionIVA(DropDownList ddl)
        {
            ddl.Items.Insert(0, new ListItem("N/A", "0"));
            ddl.Items.Insert(1, new ListItem(Empresa.IdCondicionIVA.RI.ToString()));
            ddl.Items.Insert(2, new ListItem(Empresa.IdCondicionIVA.Monotributista.ToString()));
            ddl.Items.Insert(3, new ListItem(Empresa.IdCondicionIVA.Exento.ToString()));
            ddl.Items.Insert(4, new ListItem(Empresa.IdCondicionIVA.CF.ToString()));
            ddl.Items.Insert(4, new ListItem(Empresa.IdCondicionIVA.NRI.ToString()));
        }

        private void Guardar()
        {
            EmpresaBus oEmpresaBus = new EmpresaBus();
            Empresa oEmpresa = new Empresa();
            oEmpresa.Descripcion = txtDescripcion.Text;
            oEmpresa.RazonSocial = txtRazonSocial.Text;
            oEmpresa.Domicilio = txtDomicilio.Text;
            oEmpresa.CondIVA = (ddlCondicionIva.SelectedIndex == 0 ? "N/A" : ddlCondicionIva.SelectedItem.Text);
            oEmpresa.CUIT = txtCuit.Text;
            oEmpresa.Interna = chkInterna.Checked;
            oEmpresa.CodTango = txtTango.Text;
            if (_idEmpresa == 0)
            {
                oEmpresa.IdEmpresa = oEmpresaBus.EmpresaAdd(oEmpresa);
            }
            else
            {
                oEmpresa.IdEmpresa = _idEmpresa;
                oEmpresaBus.EmpresaUpdate(oEmpresa);
            }

            if (oEmpresa.IdEmpresa > 0)
            {
                Mensaje.successMsj("Empresa guardada con éxito", this.Page, "Guardado", null);                
                LimpiarControles();
                CargarEmpresas(txtBusqueda.Text);
            }
        }

        private void LimpiarControles()
        {
            txtDescripcion.Text = string.Empty;
            txtRazonSocial.Text = string.Empty;
            txtDomicilio.Text = string.Empty;
            ddlCondicionIva.SelectedIndex = 0;
            txtCuit.Text = string.Empty;
            chkInterna.Checked = false;
            txtTango.Text = string.Empty;
            _idEmpresa = 0;
        }

        private void CargarEmpresas(string nombre)
        {
            EmpresaBus oEmpresaBus = new EmpresaBus();
            gvEmpresa.DataSource = oEmpresaBus.EmpresaGetByFilter(nombre);
            gvEmpresa.DataBind();

            cambiarHidden("0");
        }

        #endregion

    }
}
