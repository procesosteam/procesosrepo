﻿<%@ Page Title="" Language="C#" MasterPageFile="~/frmPrincipal.Master" AutoEventWireup="true" CodeBehind="frmOrdenRegresoGestion.aspx.cs" Inherits="ProcesoPP.UI.Regreso.frmOrdenRegresoGestion" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div id="contenido" runat="server">
<div id="DivFiltro">
    <div id="apertura">Gestión de OTI Regreso</div>  

    <div class="solicitud100">
        <div class="sol_tit"> <label>Fecha Desde</label></div>
        <div class="sol_campo"><asp:TextBox runat="server" id="txtFecha" /></div>
        <div class="sol_fecha"><img id="btnFecha" alt="calendario" src="../img/calendario.gif" style="width:20px; height:20px; cursor:pointer;" onmouseover="CambiarConfiguracionCalendario('txtFecha',this);"/></div>
     </div>

    <div class="solicitud100">
        <div class="sol_tit"> <label>Fecha Hasta</label></div>
        <div class="sol_campo"><asp:TextBox runat="server" id="txtFechaHasta" /></div>
        <div class="sol_fecha"><img id="btnFechaHasta" alt="calendario" src="../img/calendario.gif" style="width:20px; height:20px; cursor:pointer;" onmouseover="CambiarConfiguracionCalendario('txtFechaHasta',this);"/></div>
    </div>

     <div class="solicitud100">
        <div class="sol_tit"> <label>Nº Solicitud</label></div>
        <div class="sol_campo"><asp:TextBox runat="server" id="txtNroSolicitud" onkeypress="return ValidarNumeroEntero(event,this);"/></div><div class="sol_fecha"></div>
     </div>
     <div class="solicitud100">
        <div class="sol_tit"> <label>Empresa</label></div>
        <div class="sol_campo"><asp:DropDownList runat="server" id="ddlEmpresa" /></div><div class="sol_fecha"></div></div>

    <div class="solicitud100">
        <div class="sol_btn"><asp:Button runat="server" id="btnBuscar" cssclass="btn_form"
                  Text="Buscar" onclick="btnBuscar_Click" /></div>
    </div>
</div>
<div>
     <asp:GridView ID="gvOTI" runat="server" AutoGenerateColumns="False" 
         DataMember="idCondicion" CssClass="tabla3" HeaderStyle-CssClass="celda1" 
            RowStyle-CssClass="celda2" onrowcommand="gvOTI_RowCommand"
            onrowdatabound="gvOTI_RowDataBound">
         <Columns>
            <asp:TemplateField HeaderText="N° Orden" HeaderStyle-CssClass="columna1" ItemStyle-CssClass="columna1">
                 <ItemTemplate>
                     <asp:Label ID="lblidOrden" runat="server" Text='<%# Bind("idOrden") %>'></asp:Label>
                 </ItemTemplate>
             </asp:TemplateField>
             <asp:TemplateField HeaderText="Nº Solic." HeaderStyle-CssClass="columna1" ItemStyle-CssClass="columna1">               
                 <ItemTemplate>
                     <asp:Label ID="lblNroSolicitud" runat="server" Text='<%# Bind("NroSolicitud") %>'></asp:Label>
                 </ItemTemplate>
             </asp:TemplateField>
             <asp:TemplateField HeaderText="Fecha" HeaderStyle-CssClass="columna2" ItemStyle-CssClass="columna2">
                 <ItemTemplate>
                     <asp:Label ID="lblFecha" runat="server" Text='<%# Bind("Fecha") %>'></asp:Label>
                 </ItemTemplate>
             </asp:TemplateField>
               <asp:TemplateField HeaderText="Solicitante" HeaderStyle-CssClass="columna1" ItemStyle-CssClass="columna1">
                 <ItemTemplate>
                     <asp:Label ID="lblSolicitante" runat="server" Text='<%# Bind("Solicitante") %>'></asp:Label>
                 </ItemTemplate>
             </asp:TemplateField>
             <asp:TemplateField  HeaderText="Empresa" HeaderStyle-CssClass="columna1" ItemStyle-CssClass="columna1">
                <ItemTemplate>
                     <asp:Label ID="lblEmpresa" runat="server" Text='<%# Bind("Empresa") %>'></asp:Label>
                 </ItemTemplate>
             </asp:TemplateField>
             <asp:TemplateField  ItemStyle-CssClass="regreso">
                <ItemTemplate>
                     <asp:HiddenField ID="hfIdOT" runat="server" value='<%# Bind("idOrden") %>'/>
                     <asp:HiddenField ID="hfidParte" runat="server" value='<%# Bind("idParteEntrega") %>'/>
                     <asp:LinkButton ID="btnVer" runat="server" Text="Verif." CssClass="regreso" CommandArgument='<%# Bind("idOrden") %>' CommandName="Verificar"></asp:LinkButton>
                 </ItemTemplate>
             </asp:TemplateField>
             <asp:TemplateField  ItemStyle-CssClass="regreso">
                <ItemTemplate>
                    <asp:HiddenField ID="hfIdSC" runat="server" value='<%# Eval("idSolicitud") %>'/>
                     <asp:LinkButton ID="btnEditar" runat="server" Text="Editar" CssClass="regreso" CommandArgument='<%# Bind("idOrden") %>' CommandName="Modificar"></asp:LinkButton>
                 </ItemTemplate>
             </asp:TemplateField>
             <asp:TemplateField  ItemStyle-CssClass="regreso">
                <ItemTemplate>
                     <asp:LinkButton ID="btnEliminar" runat="server" Text="Eliminar" CssClass="regreso" CommandArgument='<%# Bind("idOrden") %>' CommandName="Eliminar" OnClientClick="return Eliminar('la Orden de Trabajo');"></asp:LinkButton>
                 </ItemTemplate>
             </asp:TemplateField>
         </Columns>
     </asp:GridView>
</div>
</div>
</asp:Content>
