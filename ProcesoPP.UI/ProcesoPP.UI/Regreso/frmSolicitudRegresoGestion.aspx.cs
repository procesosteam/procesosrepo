﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ProcesoPP.Business;
using ProcesoPP.Model;

namespace ProcesoPP.UI.Regreso
{
    public partial class frmSolicitudRegresoGestion : System.Web.UI.Page
    {
        #region EVENTOS
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    CargarEmpresa();
                    CargarSolicitud();
                }
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                CargarSolicitud();
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        protected void gvSolicitud_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "OTI":
                    HiddenField hfIdOTI = ((DataControlFieldCell)((Control)(e.CommandSource)).Parent).Controls[1] as HiddenField;
                    Response.Redirect("frmOTIRegresoCarga.aspx?idSolicitud=" + e.CommandArgument + "&idOTI=" + hfIdOTI.Value);
                    break;
                case "Modificar":
                    Response.Redirect("frmSolicitudRegresoCarga.aspx?idSolicitudRegreso=" + e.CommandArgument);
                    break;
                case "Eliminar":
                    try
                    {
                        Eliminar(e.CommandArgument.ToString());
                    }
                    catch (Exception ex)
                    {
                        Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
                    }
                    break;
            }
        }

        #endregion

        #region METODOS

        private void Eliminar(string idSolicitud)
        {
            SolicitudCliente oSolicitudCliente = new SolicitudCliente();
            SolicitudClienteBus oSolicitudClienteBus = new SolicitudClienteBus();
            oSolicitudCliente = oSolicitudClienteBus.SolicitudClienteGetById(int.Parse(idSolicitud));
            oSolicitudCliente.Baja = true;
            oSolicitudClienteBus.SolicitudClienteUpdate(oSolicitudCliente);

            CargarSolicitud();
        }
        
        private void CargarEmpresa()
        {
            EmpresaBus oEmpresaBus = new EmpresaBus();
            ddlEmpresa.DataSource = oEmpresaBus.EmpresaGetAll();
            ddlEmpresa.DataTextField = "Descripcion";
            ddlEmpresa.DataValueField = "idEmpresa";
            ddlEmpresa.DataBind();
            ddlEmpresa.Items.Insert(0, new ListItem("Seleccione...", "0"));
        }

        private void CargarSolicitud()
        {
            SolicitudClienteBus oSolicitudClienteBus = new SolicitudClienteBus();
            gvSolicitud.DataSource = oSolicitudClienteBus.SolicitudClienteRegresoGetByFilter(txtNroSolicitud.Text == "" ? "0" : txtNroSolicitud.Text,
                                                                                      ddlEmpresa.SelectedValue,
                                                                                      txtFecha.Text,
                                                                                      txtFechaHasta.Text);
            gvSolicitud.DataBind();
        }

        #endregion

    }
}
