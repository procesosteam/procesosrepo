﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ProcesoPP.Business;
using ProcesoPP.Model;
using ProcesoPP.Services;

namespace ProcesoPP.UI.Regreso
{
    public partial class frmOrdenRegresoGestion : System.Web.UI.Page
    {
        #region PROPIEDADES
        public int _idUsuario
        {
            get { return (int)ViewState["idUsuario"]; }
            set { ViewState["idUsuario"] = value; }
        }

        public Permisos _Permiso
        {
            get { return (Permisos)ViewState["permiso"]; }
            set { ViewState["permiso"] = value; }
        }
        #endregion

        #region EVENTOS
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                _idUsuario = Common.EvaluarSession(Request, this.Page);
                if (_idUsuario != 0)
                {
                    _Permiso = Common.getModulo(Request, this.Page);
                    if (_Permiso != null && _Permiso.Lectura)
                    {
                        if (!IsPostBack)
                        {
                            CargarEmpresa();
                            CargarOTI();
                        }
                    }
                    else
                    {
                        Mensaje.errorMsj("NO TIENE PERMISOS A ESTE FORMULARIO", this.Page, "SIN ACCESO", "../frmPrincipal.aspx");
                    }
                }
                else
                {
                    Mensaje.errorMsj("DEBE LOGUEARSE", this.Page, "LOGIN", "../frmLogin.aspx");
                }
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                CargarOTI();
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        protected void gvOTI_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "Verificar":
                    HiddenField hfIdParte = ((DataControlFieldCell)((Control)(e.CommandSource)).Parent).FindControl("hfidParte") as HiddenField;
                    Response.Redirect("frmVerificacionRegreso.aspx?idOTI=" + e.CommandArgument + "&idParte=" + hfIdParte.Value + "&idModulo=" + _Permiso.oModulo.idModulo);
                    break;
                case "Modificar":
                    HiddenField hfIdSC = ((DataControlFieldCell)((Control)(e.CommandSource)).Parent).FindControl("hfIdSC") as HiddenField;
                    Response.Redirect("frmOTIRegresoCarga.aspx?idOTI=" + e.CommandArgument + "&idSolicitud=" + hfIdSC.Value + "&idModulo=" + _Permiso.oModulo.idModulo);
                    break;
                case "Eliminar":
                    try
                    {
                        Eliminar(e.CommandArgument.ToString());
                    }
                    catch (Exception ex)
                    {
                        Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
                    }
                    break;
            }
        }

        protected void gvOTI_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Common.CargarRow(e,_Permiso);
            }
        }
        #endregion

        #region METODOS

        private void Eliminar(string idOrden)
        {
            Model.OrdenTrabajo oOrdenTrabajo = new Model.OrdenTrabajo();
            OrdenTrabajoBus oOrdenTrabajoBus = new OrdenTrabajoBus();
            oOrdenTrabajo = oOrdenTrabajoBus.OrdenTrabajoGetById(int.Parse(idOrden));
            oOrdenTrabajo.Baja = true;
            oOrdenTrabajoBus.OrdenTrabajoUpdate(oOrdenTrabajo);

            CargarOTI();
        }

        private void CargarEmpresa()
        {
            EmpresaBus oEmpresaBus = new EmpresaBus();
            ddlEmpresa.DataSource = oEmpresaBus.EmpresaGetAll();
            ddlEmpresa.DataTextField = "Descripcion";
            ddlEmpresa.DataValueField = "idEmpresa";
            ddlEmpresa.DataBind();
            ddlEmpresa.Items.Insert(0, new ListItem("Seleccione...", "0"));
        }

        private void CargarOTI()
        {
            OrdenTrabajoBus oOrdenTrabajoBus = new OrdenTrabajoBus();
            gvOTI.DataSource = oOrdenTrabajoBus.OrdenTrabajoRegresoGetByFilter(txtNroSolicitud.Text == "" ? "0" : txtNroSolicitud.Text,
                                                                          ddlEmpresa.SelectedValue,
                                                                          txtFecha.Text,
                                                                          txtFechaHasta.Text);
            gvOTI.DataBind();
        }

        #endregion
    }
}
