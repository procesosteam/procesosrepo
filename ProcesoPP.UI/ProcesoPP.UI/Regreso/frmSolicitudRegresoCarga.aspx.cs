﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ProcesoPP.Business;
using ProcesoPP.Model;
using ProcesoPP.Services;

namespace ProcesoPP.UI.Regreso
{
    public partial class frmSolicitudRegresoCarga : System.Web.UI.Page
    {
        #region PROPIEDADES

        public int _idUsuario
        {
            get { return (int)ViewState["idUsuario"]; }
            set { ViewState["idUsuario"] = value; }
        }
        public int _idEmpresa
        {
            get { return (int)ViewState["idEmpresa"]; }
            set { ViewState["idEmpresa"] = value; }
        }
        public int _idSolicitud
        {
            get { return (int)ViewState["idSolicitud"]; }
            set { ViewState["idSolicitud"] = value; }
        }
        public int _idSolicitudRegreso
        {
            get { return (int)ViewState["idSolicitudRegreso"]; }
            set { ViewState["idSolicitudRegreso"] = value; }
        }

        public Permisos _Permiso
        {
            get { return (Permisos)ViewState["permiso"]; }
            set { ViewState["permiso"] = value; }
        }
        #endregion

        #region EVENTOS

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                _idUsuario = Common.EvaluarSession(Request, this.Page);
                if (_idUsuario != 0)
                {
                    _Permiso = Common.getModulo(Request, this.Page);
                    if (_Permiso != null && _Permiso.Lectura)
                    {
                        if (!IsPostBack)
                        {
                            InitScreen();
                        }
                    }
                    else
                    {
                        Mensaje.errorMsj("NO TIENE PERMISOS A ESTE FORMULARIO", this.Page, "SIN ACCESO", "../frmPrincipal.aspx");
                    }
                }
                else
                {
                    Mensaje.errorMsj("DEBE LOGUEARSE", this.Page, "LOGUIN", "../frmLogin.aspx");
                }
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                GuardarSolicitud();
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        protected void ddlDesde_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddlDesde.SelectedIndex > 0 && ddlHasta.SelectedIndex > 0)
                    TraerKilometraje();
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        protected void ddlSector_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (_idEmpresa > 0)
                    CargarSolicitantes(ddlSector.SelectedValue, _idEmpresa);
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        #endregion

        #region METODOS

        private void InitScreen()
        {
            CargarLugares(ddlDesde);
            CargarLugares(ddlHasta);
            
            if (Request["idSolicitudRegreso"] != null)
            {
                _idSolicitudRegreso = int.Parse(Request["idSolicitudRegreso"]);
                CargarSolicitud();
            }
            else
            {
                _idSolicitudRegreso = 0;
            }

            if (Request["idSolicitud"] != null)
            {
                _idSolicitud = int.Parse(Request["idSolicitud"]);
                CargarSolicitud();
            }
            else
            {
                _idSolicitud = 0;                
            }
            if (!_Permiso.Escritura)
            {
                btnGuardar.Visible = false;
            }
            SelectedBPP(ddlHasta);
            TraerKilometraje();
        }

        private void CargarSolicitantes(string idSector, int idEmpresa)
        {
            SolicitanteBus oSolicitanteBus = new SolicitanteBus();
            ddlSolicitante.DataSource = oSolicitanteBus.SolicitanteGetBySectorEmpresa(int.Parse(idSector), idEmpresa);
            ddlSolicitante.DataTextField = "Nombre";
            ddlSolicitante.DataValueField = "idSolicitante";
            ddlSolicitante.DataBind();
            ddlSolicitante.Items.Insert(0, new ListItem("Seleccione...", "0"));
        }

        private void CargarSector(int idEmpresa)
        {
            SectorBus oSectorBus = new SectorBus();
            ddlSector.DataSource = oSectorBus.SectorGetAllByIdEmpresa(idEmpresa);
            ddlSector.DataTextField = "Descripcion";
            ddlSector.DataValueField = "idSector";
            ddlSector.DataBind();
            ddlSector.Items.Insert(0, new ListItem("N/A", "0"));
        }

        private void SelectedBPP(DropDownList ddl)
        {
            LugarBus oLugarBus = new LugarBus();
            oLugarBus.SelectedBPP(ref ddl);
        }

        private void TraerKilometraje()
        {
            if (ddlDesde.SelectedIndex > 0 && ddlHasta.SelectedIndex > 0)
            {
                KilometrosBus oKilometrosBus = new KilometrosBus();
                Kilometros oKilometros = new Kilometros();
                oKilometros.oLugarDesde = new Lugar();
                oKilometros.oLugarHasta = new Lugar();
                oKilometros.oLugarDesde.IdLugar = int.Parse(ddlDesde.SelectedItem.Value);
                oKilometros.oLugarHasta.IdLugar = int.Parse(ddlHasta.SelectedItem.Value);

                oKilometros = oKilometrosBus.KilometrosGetBy(oKilometros);
                if (oKilometros != null)
                    txtKms.Text = oKilometros.Kms.ToString("N0");
                else
                    txtKms.Text = "0";
            }
        }

        private void CargarEspecificacion(int idTipoServicio)
        {
            EspecificacionTecnicaBus oEspecificacionTecnicaBus = new EspecificacionTecnicaBus();
            ddlEspecificacion.DataSource = oEspecificacionTecnicaBus.EspecificacionTecnicaGetByIdTipoServicio(idTipoServicio);
            ddlEspecificacion.DataTextField = "Descripcion";
            ddlEspecificacion.DataValueField = "IdEspecificaciones";
            ddlEspecificacion.DataBind();
        }
        
        private void CargarLugares(DropDownList ddl)
        {
            LugarBus oLugarBus = new LugarBus();
            oLugarBus.CargarLugar(ref ddl);
            ddl.Items.Insert(0, new ListItem("N/A", "0"));
        }

        private void CargarSolicitud()
        {
            SolicitudCliente oSolicitudRegreso = new SolicitudCliente();
            SolicitudClienteBus oSolicitudClienteBus = new SolicitudClienteBus();
            if (_idSolicitudRegreso == 0)
            {
                oSolicitudRegreso = oSolicitudClienteBus.SolicitudClienteGetById(_idSolicitud);
                txtPozoDestino.Text = string.Empty; //oSolicitudRegreso.oMovimiento.PozoOrigen;
                txtPozoOrigen.Text = oSolicitudRegreso.oMovimiento.PozoDestino;
                ddlDesde.SelectedValue = oSolicitudRegreso.oMovimiento.oLugarDestino.IdLugar.ToString();
                ddlHasta.SelectedValue = oSolicitudRegreso.oMovimiento.oLugarOrigen.IdLugar.ToString();
            }
            else
            {
                oSolicitudRegreso = oSolicitudClienteBus.SolicitudClienteGetById(_idSolicitudRegreso);
                ddlDesde.SelectedValue = oSolicitudRegreso.oMovimiento.oLugarOrigen.IdLugar.ToString();
                ddlHasta.SelectedValue = oSolicitudRegreso.oMovimiento.oLugarDestino.IdLugar.ToString();
                txtPozoDestino.Text = oSolicitudRegreso.oMovimiento.PozoDestino;                
                txtPozoOrigen.Text = oSolicitudRegreso.oMovimiento.PozoOrigen;
                txtFecha.Text = oSolicitudRegreso.Fecha.ToString();
            }

            ddlEmpresa.Text = oSolicitudRegreso.oSector.oEmpresa.Descripcion;
            _idEmpresa = oSolicitudRegreso.oSector.oEmpresa.IdEmpresa;
            CargarSector(_idEmpresa);
            ddlSector.SelectedValue = oSolicitudRegreso.IdSector.ToString();
            CargarSolicitantes(ddlSector.SelectedValue, _idEmpresa);
            ddlSolicitante.SelectedValue = oSolicitudRegreso.IdSolicitante.ToString();


            txtKms.Text = oSolicitudRegreso.oMovimiento.KmsEstimados.ToString();
            txtNroSS.Text = oSolicitudRegreso.NroSolicitud.ToString();            
            ddlTipoServicio.Text = oSolicitudRegreso.oTipoServicio.Descripcion;
            
            ddlCondicion.Text = oSolicitudRegreso.oCondicion.Descripcion;
            txtNroAviso.Text = oSolicitudRegreso.NroPedidoCliente;
            txtEspecif.Text = oSolicitudRegreso.ServicioAux;
            
            if (oSolicitudRegreso.oTipoServicio.oEspecificacion.Count > 0)
            {
                ddlEspecificacion.Visible = true;
                txtEspecif.Visible = false;
                CargarEspecificacion(oSolicitudRegreso.IdTipoServicio);
                SelectedEspecificaciones(oSolicitudRegreso.oListEspecificaciones);
            }
            else
            {
                ddlEspecificacion.Visible = false;
                txtEspecif.Visible = true;
            }
        }

        private void SelectedEspecificaciones(List<EspecificacionTecnica> oListaEsp)
        {
            foreach (ListItem chk in ddlEspecificacion.Items)
            {
                if (oListaEsp.Exists(e => e.IdEspecificaciones == int.Parse(chk.Value)))
                    chk.Selected = true;
                else
                    chk.Selected = false;
            }
        }

        private void GuardarSolicitud()
        {
            if ((int.Parse(txtKms.Text) != 0) //&& (ddlDesde.SelectedValue != ddlHasta.SelectedValue))
               || ((int.Parse(txtKms.Text) == 0) && (ddlDesde.SelectedValue == ddlHasta.SelectedValue)))
            {
                SolicitudClienteBus oSolicitudClienteBus = new SolicitudClienteBus();
                SolicitudCliente oSolicitudCliente = new SolicitudCliente();
                if (_idSolicitudRegreso == 0)
                {
                    if (_idSolicitud != 0)
                        oSolicitudCliente = oSolicitudClienteBus.SolicitudClienteGetById(_idSolicitud);
                    else
                    {
                        oSolicitudCliente.oMovimiento = new Movimiento();
                        oSolicitudCliente.oMovimiento.oLugarDestino = new Lugar();
                        oSolicitudCliente.oMovimiento.oLugarOrigen = new Lugar();
                    }
                }
                else
                    oSolicitudCliente = oSolicitudClienteBus.SolicitudClienteGetById(_idSolicitudRegreso);

                oSolicitudCliente.Entrega = false;

                oSolicitudCliente.oMovimiento.oLugarOrigen.IdLugar = int.Parse(ddlDesde.SelectedItem.Value);
                oSolicitudCliente.oMovimiento.oLugarDestino.IdLugar = int.Parse(ddlHasta.SelectedValue.ToString());
                oSolicitudCliente.oMovimiento.PozoDestino = txtPozoDestino.Text;
                oSolicitudCliente.oMovimiento.KmsEstimados = int.Parse(txtKms.Text);
                oSolicitudCliente.oMovimiento.PozoOrigen = txtPozoOrigen.Text;

                oSolicitudCliente.NroSolicitud = int.Parse(txtNroSS.Text);
                oSolicitudCliente.Fecha = DateTime.Parse(txtFecha.Text);
                oSolicitudCliente.Hora = DateTime.Parse(txtFecha.Text);
                oSolicitudCliente.FechaAlta = DateTime.Now;
                oSolicitudCliente.oListEspecificaciones = getId(ddlEspecificacion);
                oSolicitudCliente.NroPedidoCliente = txtNroAviso.Text;
                oSolicitudCliente.IdSector = int.Parse(ddlSector.SelectedValue);
                oSolicitudCliente.IdSolicitante = int.Parse(ddlSolicitante.SelectedValue);

                oSolicitudCliente.IdMovimiento = oSolicitudCliente.oMovimiento.IdMovimiento;
                oSolicitudCliente.IdUsuario = _idUsuario;

                if (_idSolicitudRegreso == 0)
                {
                    _idSolicitudRegreso = oSolicitudCliente.IdSolicitud = oSolicitudClienteBus.SolicitudClienteAdd(oSolicitudCliente);
                    //OrdenTrabajoBus OOrdenTrabajoBus = new OrdenTrabajoBus();
                    //OOrdenTrabajoBus.OrdenTrabajoAddRegreso(_idSolicitud, _idSolicitudRegreso);
                    Mensaje.successMsj("Solicitud agregada con éxito.", this.Page, "Exito", "frmSolicitudRegreso.aspx?idModulo=" + _Permiso.oModulo.idModulo.ToString());

                }
                else
                {
                    oSolicitudCliente.IdSolicitud = _idSolicitudRegreso;
                    oSolicitudClienteBus.SolicitudClienteUpdate(oSolicitudCliente);
                    Mensaje.successMsj("Solicitud agregada con éxito.", this.Page, "Exito", "frmSolicitudRegreso.aspx?idModulo=" + _Permiso.oModulo.idModulo.ToString());
                }
            }
            else
            {
                Mensaje.errorMsj("Debe ingresar un kilometraje mayor a 0(cero).", this.Page, "ERROR", null);
            }
            
        }

        private List<EspecificacionTecnica> getId(CheckBoxList chkList)
        {
            List<EspecificacionTecnica> oList = new List<EspecificacionTecnica>();
            foreach (ListItem chk in chkList.Items)
            {
                if (chk.Selected)
                {
                    EspecificacionTecnica tsEsp = new EspecificacionTecnica();
                    tsEsp.IdEspecificaciones = int.Parse(chk.Value);
                    tsEsp.Descripcion = chk.Text;
                    oList.Add(tsEsp);
                }
            }

            return oList;
        }

        #endregion

    }
}
