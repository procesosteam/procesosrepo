﻿<%@ Page Title="" Language="C#" MasterPageFile="~/frmPrincipal.Master" AutoEventWireup="true" CodeBehind="frmSolicitudRegresoGestion.aspx.cs" Inherits="ProcesoPP.UI.Regreso.frmSolicitudRegresoGestion" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
 <div id="contenido" runat="server" >
<div id="apertura">Gestión de Solicitud de Servicio. REGRESO</div>
    

<div class="solicitud100">
<div class="sol_tit50"> <label>Fecha Desde</label></div>

<div class="sol_campo30"><asp:TextBox runat="server"  CssClass="sol_campo25" id="txtFecha" /></div>

<div class="sol_fecha"><img id="btnFecha" alt="calendario" src="../img/calendario.gif" style="width:20px; height:20px; cursor:pointer;" onmouseover="CambiarConfiguracionCalendario('txtFecha',this);"/></div>

<div class="sol_tit2"></div>

<div class="sol_tit50"><label>Fecha Hasta</label></div>
<div class="sol_campo30"><asp:TextBox runat="server" CssClass="sol_campo25" id="txtFechaHasta" /></div>
<div class="sol_fecha"><img id="btnFechaHasta" alt="calendario" src="../img/calendario.gif" style="width:20px; height:20px; cursor:pointer;" onmouseover="CambiarConfiguracionCalendario('txtFechaHasta',this);"/></div>
<div class="sol_tit2"></div></div>


<div class="solicitud100">
<div class="sol_tit50"> <label>Nº Solicitud</label></div>
 <div class="sol_campo30"><asp:TextBox CssClass="sol_campo25" runat="server" id="txtNroSolicitud" onkeypress="return ValidarNumeroEntero(event,this);"/></div><div class="sol_tit2"></div><div class="sol_fecha"></div>
     
     
<div class="sol_tit50"><label>Empresa</label></div>
 <div class="sol_campo30"><asp:DropDownList runat="server" id="ddlEmpresa" /></div>
        
<div class="sol_fecha"></div></div>

    <div class="solicitud100">
        <div class="sol_btn"><asp:Button runat="server" id="btnBuscar" cssclass="btn_form"
                  Text="Buscar" onclick="btnBuscar_Click" /></div>
    </div>
</div>
    <div>

<asp:GridView ID="gvSolicitud" runat="server" AutoGenerateColumns="False" 
DataMember="idCondicion" CssClass="tabla3" HeaderStyle-CssClass="celda1" 
onrowcommand="gvSolicitud_RowCommand" 
onrowdatabound="gvSolicitud_RowDataBound">
         <Columns>
         
             <asp:TemplateField HeaderText="Nº Solicitud" HeaderStyle-CssClass="columna1" ItemStyle-CssClass="columna1">               
                 <ItemTemplate>
                     <asp:Label ID="lblNroSolicitud" runat="server" Text='<%# Bind("NroSolicitud") %>'></asp:Label>
                 </ItemTemplate>
             </asp:TemplateField>
             <asp:TemplateField HeaderText="Fecha Hora" HeaderStyle-CssClass="columna2" ItemStyle-CssClass="columna2">
                 <ItemTemplate>
                     <asp:Label ID="lblFecha" runat="server" Text='<%# Bind("Fecha") %>'></asp:Label>
                 </ItemTemplate>
             </asp:TemplateField>
               <asp:TemplateField HeaderText="Solicitante" HeaderStyle-CssClass="columna1" ItemStyle-CssClass="columna1">
                 <ItemTemplate>
                     <asp:Label ID="lblSolicitante" runat="server" Text='<%# Bind("Solicitante") %>'></asp:Label>
                 </ItemTemplate>
             </asp:TemplateField>
             <asp:TemplateField  HeaderText="Empresa" HeaderStyle-CssClass="columna1" ItemStyle-CssClass="columna1">
                <ItemTemplate>
                     <asp:Label ID="lblEmpresa" runat="server" Text='<%# Bind("Empresa") %>'></asp:Label>
                 </ItemTemplate>
             </asp:TemplateField>
              <asp:TemplateField  ItemStyle-CssClass="regreso">
                <ItemTemplate>
                     <asp:HiddenField ID="hfIdOT" runat="server" value='<%# Bind("idOrden") %>'/>
                     <asp:LinkButton ID="btnOT" runat="server" Text="OtiR" CssClass="regreso" CommandArgument='<%# Bind("idSolicitud") %>' CommandName="OTI"></asp:LinkButton>
                 </ItemTemplate>
             </asp:TemplateField>
             <asp:TemplateField  ItemStyle-CssClass="regreso">
                <ItemTemplate>
                     <asp:LinkButton ID="btnModificar" runat="server" Text="Editar" CssClass="regreso" CommandArgument='<%# Bind("idSolicitud") %>' CommandName="Modificar"></asp:LinkButton>
                 </ItemTemplate>
             </asp:TemplateField>
             <asp:TemplateField  ItemStyle-CssClass="regreso">
                <ItemTemplate>
                     <asp:LinkButton ID="btnEliminar" runat="server" Text="Eliminar" CssClass="regreso" CommandArgument='<%# Bind("idSolicitud") %>' CommandName="Eliminar" OnClientClick="return Eliminar('la Solicitud');"></asp:LinkButton>
                 </ItemTemplate>
             </asp:TemplateField>         
         </Columns>
     </asp:GridView>
</div>

</asp:Content>
