﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ProcesoPP.Business;
using ProcesoPP.Model;
using ProcesoPP.Services;
using System.Configuration;

namespace ProcesoPP.UI.Regreso
{
    public partial class frmSolicitudRegreso : System.Web.UI.Page
    {
        #region PROPIEDADES

        public Permisos _Permiso
        {
            get { return (Permisos)ViewState["permiso"]; }
            set { ViewState["permiso"] = value; }
        }

        public int _idUsuario
        {
            get { return (int)ViewState["idUsuario"]; }
            set { ViewState["idUsuario"] = value; }
        }

        public int _ADM
        {
            get { return int.Parse(ConfigurationManager.AppSettings["ADM"].ToString()); }
        }

        #endregion

        #region EVENTOS

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                _idUsuario = Common.EvaluarSession(Request, this.Page);
                if (_idUsuario != 0)
                {
                    _Permiso = Common.getModulo(Request, this.Page);
                    if (_Permiso != null && _Permiso.Lectura)
                    {
                        if (!IsPostBack)
                        {
                            CargarEmpresa();
                            CargarEquipo();                            
                        }
                    }
                    else
                    {
                        Mensaje.errorMsj("NO TIENE PERMISOS A ESTE FORMULARIO", this.Page, "SIN ACCESO", "../frmPrincipal.aspx");
                    }
                }
                else
                {
                    Mensaje.errorMsj("DEBE LOGUEARSE", this.Page, "LOGIN", "../frmLogin.aspx");
                }
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                CargarSolicitud();
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        protected void gvSolicitud_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            string strUrl = "";
            switch (e.CommandName)
            {
                case "OTI":
                    HiddenField hfIdOTI = ((DataControlFieldCell)((Control)(e.CommandSource)).Parent).FindControl("hfIdOT") as HiddenField;
                    HiddenField hfIdTrailer = ((DataControlFieldCell)((Control)(e.CommandSource)).Parent).FindControl("hfIdTrailer") as HiddenField;
                    //Response.Redirect("../OrdenTrabajo/frmOrdenTrabajoCarga.aspx?idSolicitud=" + e.CommandArgument + "&idOTI=" + hfIdOTI.Value + "&idModulo=" + _Permiso.oModulo.idModulo);
                    strUrl = "idSolicitud=" + Common.encriptarURL(e.CommandArgument.ToString()) +
                             "&idOTI=" + Common.encriptarURL(hfIdOTI.Value) +
                             "&idModulo=" + _Permiso.oModulo.idModulo.ToString() +
                             "&idTrailer=" + Common.encriptarURL(hfIdTrailer.Value);
                    Response.Redirect("../SSe3/frmSSOti.aspx?" + strUrl);
                    break;
                case "Modificar":
                    //Response.Redirect("frmSolicitudCarga.aspx?idSolicitud=" + e.CommandArgument + "&idModulo=" + _Permiso.oModulo.idModulo);
                    Response.Redirect("../SSe3/frmSSNueva.aspx?idSolicitud=" + e.CommandArgument + "&idModulo=" + _Permiso.oModulo.idModulo + "&Accion=" + e.CommandName);
                    break;
                case "Eliminar":
                    try
                    {
                        Eliminar(e.CommandArgument.ToString());
                    }
                    catch (Exception ex)
                    {
                        Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
                    }
                    break;
            }
        }
        
        protected void gvSolicitud_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvSolicitud.PageIndex = e.NewPageIndex;
            CargarSolicitud();
        }

        protected void gvSolicitud_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Common.CargarRow(e, _Permiso);
                LinkButton btnEliminar = e.Row.FindControl("btnEliminar") as LinkButton;

                if (_Permiso.oTipoUsuario.idTipoUsuario == _ADM)
                    btnEliminar.Visible = true;
                else
                    btnEliminar.Visible = false;
            }
        }

        #endregion

        #region METODOS

        private void Eliminar(string idSolicitud)
        {
            SolicitudCliente oSolicitudCliente = new SolicitudCliente();
            SolicitudClienteBus oSolicitudClienteBus = new SolicitudClienteBus();
            oSolicitudCliente = oSolicitudClienteBus.SolicitudClienteGetById(int.Parse(idSolicitud));
            oSolicitudCliente.Baja = true;
            oSolicitudClienteBus.SolicitudClienteUpdate(oSolicitudCliente);

            CargarSolicitud();
        }
        
        private void CargarEmpresa()
        {
            EmpresaBus oEmpresaBus = new EmpresaBus();
            ddlEmpresa.DataSource = oEmpresaBus.EmpresaGetAll();
            ddlEmpresa.DataTextField = "Descripcion";
            ddlEmpresa.DataValueField = "idEmpresa";
            ddlEmpresa.DataBind();
            ddlEmpresa.Items.Insert(0, new ListItem("Seleccione...", "0"));
        }

        private void CargarSolicitud()
        {
            SolicitudClienteBus oSolicitudClienteBus = new SolicitudClienteBus();
            gvSolicitud.DataSource = oSolicitudClienteBus.SolicitudRegresoGetByFilter(txtNroSolicitud.Text == "" ? "0" : txtNroSolicitud.Text,
                                                                                      ddlEmpresa.SelectedValue,
                                                                                      txtFecha.Text,
                                                                                      txtFechaHasta.Text,ddlTrailer.SelectedValue);
            gvSolicitud.DataBind();
        }

        private void CargarEquipo()
        {
            VehiculoBus oTrailerBus = new VehiculoBus();
            oTrailerBus.CargarComboEquipo(ref ddlTrailer, "Seleccione un equipo...");
        }
        
        #endregion

        

    }
}
