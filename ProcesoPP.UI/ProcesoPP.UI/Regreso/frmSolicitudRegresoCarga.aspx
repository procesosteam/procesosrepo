﻿<%@ Page Title="" Language="C#" MasterPageFile="~/frmPrincipal.Master" AutoEventWireup="true" CodeBehind="frmSolicitudRegresoCarga.aspx.cs" Inherits="ProcesoPP.UI.Regreso.frmSolicitudRegresoCarga" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="tabla3" id="divMensaje" runat="server"><asp:Label ID="lblError" CssClass="error2" runat="server" Text="Error" Visible="false"></asp:Label></div>
 <div id="contenido" runat="server">
    <div id="apertura">Solicitud de Servicio Vuelta.</div>
    <div class="solicitud100">
        <div class="sol_tit2"><label>SS N°</label></div>
        <div class="sol_campo30">
            <asp:Label runat="server" CssClass="sol_campo25" Text="0" ID="txtNroSS" onkeypress="return ValidarNumeroEntero(event,this);" />
        </div>
        <div class="sol_tit2">
            <label>Condición</label></div>
        <div class="sol_campo30">
            <asp:Label ID="ddlCondicion" runat="server" />
        </div>
        <div class="sol_tit2">
            <label>Fecha</label></div>
        <div class="sol_campo30">
            <asp:TextBox CssClass="sol_campo25" runat="server" ID="txtFecha" /></div>
        <div class="sol_fecha">
            <img id="btnFecha" alt="calendario" src="../img/calendario.gif" style="width: 20px;height: 20px; cursor: pointer;" onmouseover="CambiarConfiguracionCalendario('txtFecha',this);" /></div>
    </div>
    <div id="titulos">Datos del Solicitante.</div>
    <div class="solicitud100">
        <div class="sol_tit2">
            <label>Empresa</label></div>
        <div class="sol_campo30">
            <asp:Label ID="ddlEmpresa" runat="server"/>
        </div>
        <div class="sol_tit2">
            <label>Sector</label></div>
        <div class="sol_campo30">
            <asp:DropDownList ID="ddlSector" runat="server" AutoPostBack="true" 
                    onselectedindexchanged="ddlSector_SelectedIndexChanged"/>
        </div>
        <div class="sol_tit2">
            <label>Solicitante</label></div>
        <div class="sol_campo30">
            <asp:DropDownList ID="ddlSolicitante" runat="server" /></div>
        </div>
        
        <div class="sol_fecha"></div>
    </div>
    <div style="visibility:hidden">  
        <div class="sol_tit2"><label>Nº Aviso</label></div>
        <div class="sol_campo30">
            <asp:Label ID="txtNroAviso" CssClass="sol_campo25" runat="server" /></div>
        <div class="sol_tit2"><label>Teléfono</label></div>
        <div class="sol_campo30">
            <asp:Label ID="txtTelefono" CssClass="sol_campo25" runat="server" /></div>           
    </div>
    <div id="titulos">Tipo de Servicio</div>
    <div class="sol_regresa" id="divTipoServicio">
        <asp:Label runat="server" ID="ddlTipoServicio" AutoPostBack="true" OnSelectedIndexChanged="ddlTipoServicio_SelectedIndexChanged" />
    </div>
   <div class="sol_box" id="divEspecificaciones">
        <asp:CheckBoxList runat="server" Enabled="false" CssClass="sol_check" ID="ddlEspecificacion" RepeatColumns="4" Visible="false"></asp:CheckBoxList>
        <asp:Label runat="server" ID="txtEspecif" Visible="false"></asp:Label>
    </div>
    <div id="titulos">Movimiento</div>
    <div class="solicitud100">
        <div class="sol_tit2"><label>Origen</label></div>
        <div class="sol_campo30">
            <asp:DropDownList ID="ddlDesde" runat="server" Enabled="false" onselectedindexchanged="ddlDesde_SelectedIndexChanged" AutoPostBack="true" /></div>
        <div class="sol_tit2"><label>Pozo / Eq.</label></div>
        <div class="sol_campo30"> <asp:Label ID="txtPozoOrigen" CssClass="sol_campo25" runat="server" Style="text-transform: uppercase"/></div>
        <div class="sol_tit2"></div>
        <div class="sol_campo30"></div>
        <div class="sol_fecha"></div>
    </div>
    <div class="solicitud100">
        <div class="sol_tit2"><label>Destino</label></div>
        <div class="sol_campo30">
            <asp:DropDownList ID="ddlHasta" runat="server" Enabled="false" onselectedindexchanged="ddlDesde_SelectedIndexChanged" AutoPostBack="true"/></div>
        <div class="sol_tit2"><label>Pozo / Eq.</label></div>
        <div class="sol_campo30">
            <asp:Label ID="txtPozoDestino" CssClass="sol_campo25" runat="server" Style="text-transform: uppercase"/></div>
        <div class="sol_tit2">
            <label>Kms Est.</label></div>
        <div class="sol_campo30">
            <asp:Label ID="txtKms" CssClass="sol_campo25" runat="server" ReadOnly="true" onkeypress="return ValidarNumeroDecimal(event,this,2);" /></div>
        <div class="sol_fecha"></div>
    </div>
    <div class="solicitud100">
        <div class="sol_btn">
            <asp:Button runat="server" ID="btnGuardar" Text="GUARDAR" CssClass="btn_form" OnClientClick="return ValidarObj('txtNroSS|Ingrese le Nro. de Solicitud de Servicio.~ddlCondicion|Seleccione una condición.~txtFecha|Debe ingresar una fecha.~ddlEmpresa|Seleccione una empresa.~txtNombre|Debe ingresar nombre y apellido del Solicitante.~ddlSector|Debe seleccionar el sector.~ddlTipoServicio|Debe seleccionar un tipo de servicio.~ddlEspecificacion|Debe seleccionar un especificació.~txtOrigen|Debe ingresar el origen.~txtDestino|Debe ingresar el destino.~txtPozo| Debe ingresar el nombre del pozo.~ txtKms|Debe ingresar los Kms estimados.')"
                OnClick="btnGuardar_Click"></asp:Button>
        </div>
        <div class="sol_btn">
            <input type="button" id="btnVolver" value="VOLVER" class="btn_form" onclick="IrA('../SolicitudServicio/frmSolicitudGestion.aspx?idModulo=2');" />
        </div>
    </div>
</div>

</asp:Content>
