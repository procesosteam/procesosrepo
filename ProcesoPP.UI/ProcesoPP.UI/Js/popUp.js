﻿
var scrollCachePosition = 0;
var contenedor = "ctl00_ContentPlaceHolder1_";

function abrirPopUp() {
    $(window).resize();
    $("#capaPopUp").show("slow");
    $("#popUpDiv").show("slow");         
}

function cerrarPopUp() {
    $('#popUpDiv').hide("slow");
    $('#capaPopUp').hide("slow");

}

function idGestion() {
    location.href = "frmSolicitudGestion.aspx?idModulo=2";
}


function idOT() {
    grid = document.getElementById(contenedor + "gvTransporte");
    idSolicitud = document.getElementById(contenedor + "hfIdSol").value;
    ids = "";
    cont = 0;
    if (grid != null) {
        for (i = 1; i < grid.rows.length; i++) {
            if (grid.rows[i].cells[0].children[0].checked) {
                ids = grid.rows[i].cells[0].children[1].value;
                cont++;
            }
        }
    }
    if (cont == 1)
        location.href = "..\\OrdenTrabajo\\frmOrdenTrabajoCarga.aspx?idVehiculo=" + ids + "&idSolicitud=" + idSolicitud + "&idModulo=4";
    else
        new Messi('Debe seleccionar _solo un trailer', { title: 'Validación', modal: true });
}
