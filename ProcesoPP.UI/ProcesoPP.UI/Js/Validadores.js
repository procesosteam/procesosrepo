﻿function Volver() {
    history.back();
}
function IrA(URL) {
    if (URL != "")
        window.location = URL;
    else
        window.history.back();  return false;
}
///CONTENEDOR MASTER PAGE
var contenedor = "ctl00_ContentPlaceHolder1_";

///VALIDADOR 
// LOS OBJETOS A VALIDAR DEBEN ENVIARSE ATRAVES DE UN STRING
// SEPARANDO CADA OBJETO CON SU MENSAJE A TRAVES DE UN '~' PARA LEGO DISTINGUIR LOS OBJETOS A VALIDAR
// Y A SU VEZ SEPARAR EL OBJETOS DEL MENSAJE POR UN '|'
// EJ: ValidarObj('ddlEspecificacion|Seleccione una Especificacion.~txtDescripcion|Ingrese una descripción.')
//Valida Vacio
function ValidarObj(oTxt) {
    aMSJ = "";
    oTxt = oTxt.split('~');
    for (i = 0; i < oTxt.length; i++) {
        objeto = oTxt[i].split('|')[0];
        MSJ = oTxt[i].split('|')[1];
        aMSJ += validar(objeto, MSJ);
    }
    if (aMSJ.length > 0) {
        new Messi(aMSJ, { title: 'FALTAN DATOS', modal: true, titleClass: 'error' });
        return false;
    }
    else {
        return true;
    }
}

function validar(oTxt, msj) {
    if (document.getElementById(contenedor + oTxt) != null)
        oTxt = document.getElementById(contenedor + oTxt);
    else
        oTxt = document.getElementById(oTxt);
    retMensje = "";
    if (oTxt != null) {
        switch (oTxt.type) {
            case "text":
                if (oTxt.value.length == 0) {
                    retMensje = "- " + msj + "<br>";
                }
                break;
            case "select-one":
                if (oTxt.selectedIndex == 0) {
                    retMensje = "- " + msj + "<br>";
                }
                break;
            case "password":
                if (oTxt.value.length == 0) {
                    retMensje = "- " + msj + "<br>";
                }
                break;
            default:
                retMensje = "";
                break;

        }
    }
    return retMensje;
}


function Eliminar(descripcion) {
    return confirm("Desea eliminar " + descripcion + "?");

//    new Messi('Desea eliminar ' + descripcion + '?',
//            { title: 'Pregunta',
//                buttons: [{ id: 0, label: 'Si', val: 'true', btnClass: 'btn-success' },
//                          { id: 1, label: 'No', val: 'false', btnClass: 'btn-danger'}],
//                          callback: function(val) {
//                              return val;
//                          }
//            });

}

function ValidarClave(objClave, objRepite) {
    objClave = document.getElementById(contenedor + objClave);
    objRepite = document.getElementById(contenedor + objRepite);

    if (objClave.value != objRepite.value) {
        new Messi('Las claves ingresadas deben ser identicas', { title: 'Validar Claves', modal: true });
        return false;
    }
    else { return true; }
}

function proponerNombre(nombre, apellido, oTxt) {
    nombre = document.getElementById(contenedor + nombre);
    apellido = document.getElementById(contenedor + apellido);
    oTxt = document.getElementById(contenedor + oTxt);

    oTxt.value = (nombre.value.substring(0, 1) + apellido.value).toLowerCase();

}

//Define que campos se van a poder ingresar por teclado en un textbox.
function ValidarNumeroDecimal(e, Ctrl, Ctd) {
    tecla = (document.all) ? e.keyCode : e.which;
    if (tecla == 8) return true; //Tecla de retroceso (para poder borrar) 
    if (tecla == 45) return true; //Tecla - (signo menos)
    if ((tecla == 46) || (tecla == 44)) //Tecla . (punto) //Tecla , (coma)  
    {
        if (NoExisteComa(Ctrl.value)) // Si hay una coma devuelve false
        {
            Ctrl.value = Ctrl.value + ",";
        }
    }

    if ((!NoExisteComa(Ctrl.value)) && (CtdDecimales(Ctrl.value) > (Ctd - 1))) {
        return false;
    }
    patron = /\d/; //Solo acepta números 
    te = String.fromCharCode(tecla);
    return patron.test(te);
}

function ValidarNumeroEntero(e, Ctrl) {
    tecla = (document.all) ? e.keyCode : e.which;
    if (tecla == 8) return true; //Tecla de retroceso (para poder borrar) 
    patron = /\d/; //Solo acepta números 
    te = String.fromCharCode(tecla);
    return patron.test(te);
}

function NoExisteComa(cadena) {
    for (var i = 0; i < cadena.length; i++) {
        var caracter = cadena[i];
        if (caracter == ",") {
            return false;
        }
    }
    return true;
}

function CtdDecimales(cadena) {
    var numero = cadena.split(",")
    if (numero[1] != null) {
        return numero[1].length;
    }
    else {
        return 0
    }
}