<%@ Page Title="" Language="C#" MasterPageFile="~/frmPrincipal.Master" AutoEventWireup="true" EnableEventValidation="false"
    CodeBehind="frmVehiculoCarga.aspx.cs" Inherits="ProcesoPP.UI.Vehiculo.frmCargaVehiculo"%>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../Css/personal.css" rel="stylesheet" type="text/css" />
    <script src="../Js/jquery.maskedinput.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () { SetMenu("liVehiculos"); });
        function verificarForm() {
            msj = 'txtAnioVehiculo|Ingrese Año del Vehiculo.' +
                '~txtColor|Debe ingresar color del Vehiculo' +
                '~ddlEmpresa|Debe seleccionar Empresa.' +
                '~ddlMarca|Debe seleccionar Marca.' +
                '~ddlSector|Debe seleccionar Sector.' +
                '~txtModelo|Debe ingresar el modelo del Vehiculo.' +
                '~txtTitular|Debe ingresar titular.' +
                '~ddlCodificacion|Debe seleccionar codificacion.' +
                '~txtNCodificacion|Debe ingresar un numero de codificacion.';
            val = ValidarObj(msj);
            hfEsValido = document.getElementById('<%=hfEsValido.ClientID%>');
            if (val) {
                hfEsValido.value = true;
                return true;
            }
            else
                return val;
        }

        jQuery(function ($) {
            $("#<%=txtEmision.ClientID%>").mask("99/99/9999", { placeholder: "dd/mm/yyyy" });
        });
        jQuery(function ($) {
            $("#<%=txtVEmision.ClientID%>").mask("99/99/9999", { placeholder: "dd/mm/yyyy" });
        });
        jQuery(function ($) {
            $("#<%=txtVCedulaVerde.ClientID%>").mask("99/99/9999", { placeholder: "dd/mm/yyyy" });
        });
        jQuery(function ($) {
            $("#<%=txtVencimientoLeasing.ClientID%>").mask("99/99/9999", { placeholder: "dd/mm/yyyy" });
        });
        jQuery(function ($) {
            $("#<%=txtVencimientoPatente.ClientID%>").mask("99/99/9999", { placeholder: "dd/mm/yyyy" });
        });
        jQuery(function ($) {
            $("#<%=txtVVTV.ClientID%>").mask("99/99/9999", { placeholder: "dd/mm/yyyy" });
        });
        jQuery(function ($) {
            $("#<%=txtPolizaHasta.ClientID%>").mask("99/99/9999", { placeholder: "dd/mm/yyyy" });
        });
        jQuery(function ($) {
            $("#<%=txtPolizaDesde.ClientID%>").mask("99/99/9999", { placeholder: "dd/mm/yyyy" });
        });
        jQuery(function ($) {
            $("#<%=txtFechaAlta.ClientID%>").mask("99/99/9999", { placeholder: "dd/mm/yyyy" });
        });
        
        function AbrirVentana(url) {
            window.open("../Administracion/VerAdjunto.aspx?idArchivo=" + url + "&esPersonal=false", '');
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:HiddenField ID="hfEsValido" Value="false" runat="server" />
    <asp:HiddenField ID="hfFechasValidas" Value="true" runat="server" />
    <div id="ape_personal">
        <asp:label ID="lbltitulo1" runat="server"></asp:label>
    </div>
    <div id="tit_personal">
        Datos del Vehiculo
    </div>
    <div id="celda_p50">
        <div id="campo_per30">
            <label>Marca:</label>
        </div>
        <div id="campo_per50">
            <asp:DropDownList CssClass="sol_campo50c" ID="ddlMarca" runat="server" />
        </div>
    </div>
    <div id="celda_p50">
        <div id="campo_per30">
            <label>Modelo:</label></div>
        <div id="campo_per50bis">
            <asp:TextBox CssClass="campos" ID="txtModelo" runat="server"> </asp:TextBox>
        </div>
    </div>
    <div id="celda_p50">
        <div id="campo_per30">
            <label>Año:</label>
        </div>
        <div id="campo_per50bis">
            <asp:TextBox CssClass="campos" ID="txtAnioVehiculo" onkeypress="return ValidarNumeroEntero(event,this);"
                runat="server"> </asp:TextBox>
        </div>
    </div>
    <div id="celda_p50">
        <div id="campo_per30">
            <label>Patente:</label></div>
        <div id="campo_per50bis">
            <asp:TextBox CssClass="campos" ID="txtPatente" runat="server"> </asp:TextBox>
        </div>
    </div>
    <div id="celda_p50">
        <div id="campo_per30">
            <label>Empresa:</label>
        </div>
        <div id="campo_per50bis">
            <asp:DropDownList CssClass="sol_campo50c" ID="ddlEmpresa" runat="server" />
            <cc1:cascadingdropdown id="cddEmpresa" targetcontrolid="ddlEmpresa" prompttext="N/A"
                promptvalue="0" servicemethod="GetEmpresa" runat="server" category="idEmpresa"
                loadingtext="Cargando..." />
        </div>
    </div>
    <cc1:toolkitscriptmanager id="ToolkitScriptManager1" runat="server">
        </cc1:toolkitscriptmanager>
    <div id="celda_p50">
        <div id="campo_per30">
            <label>Sector:</label></div>
        <div id="campo_per50bis">
            <asp:DropDownList CssClass="sol_campo50c" ID="ddlSector" runat="server" />
            <cc1:cascadingdropdown id="cddSector" targetcontrolid="ddlSector" prompttext="N/A"
                promptvalue="0" servicemethod="GetSector" runat="server" category="idSector"
                parentcontrolid="ddlEmpresa" loadingtext="Cargando..." />
        </div>
    </div>
    <div id="celda_p50">
        <div id="campo_per30">
            <label>Alta:</label>
        </div>
        <div id="campo_per50bis">
            <div id="check">
                Si</div>
            <asp:RadioButton CssClass="check2" GroupName="radioAlta" ID="raSi" runat="server" />
            <div id="check">
                No</div>
            <asp:RadioButton CssClass="check2" GroupName="radioAlta" ID="raNo" runat="server" />
        </div>
    </div>
    <div id="celda_p50">
        <div id="campo_per30">
            <label>Fecha:</label></div>
        <div id="campo_per50bis">
            <asp:TextBox CssClass="campos" ID="txtFechaAlta" runat="server"> </asp:TextBox>
        </div>
        <div id="campo_per_icono">
            <img id="Img1" alt="calendario" src="../img/calendario.gif" style="width: 15px; height: 15px;
                cursor: pointer;" onmouseover="CambiarConfiguracionCalendario('txtFechaAlta',this);" /></div>
    </div>
    <%--<div id="celda_p50">
        <div id="campo_per30">
            <label>
                Baja:</label>
        </div>
        <div id="campo_per50bis">
            <div id="check">
                Si</div>
            <asp:RadioButton CssClass="check2" GroupName="radioBaja" ID="rbSi" runat="server" />
            <div id="check">
                No</div>
            <asp:RadioButton CssClass="check2" GroupName="radioBaja" ID="rbNo" runat="server" />
        </div>
    </div>
    <div id="celda_p50">
        <div id="campo_per30">
            <label>
                Fecha:</label></div>
        <div id="campo_per50bis">
            <asp:TextBox CssClass="campos" ID="txtFechaBaja" runat="server"> </asp:TextBox>
        </div>
        <div id="campo_per_icono">
            <img id="Img4" alt="calendario" src="../img/calendario.gif" style="width: 15px; height: 15px;
                cursor: pointer;" onmouseover="CambiarConfiguracionCalendario('txtFechaBaja',this);" /></div>
    </div>--%>
    <div id="celda_p50">
        <div id="campo_per30">
            <label>
                Titular:</label>
        </div>
        <div id="campo_per50bis">
            <asp:TextBox CssClass="campos" ID="txtTitular" runat="server"> </asp:TextBox>
        </div>
    </div>
    <div id="celda_p50">
        <div id="campo_per30">
            <label id="lblMotor" runat="server">
                Potencia de Motor:</label>
        </div>
        <div id="campo_per50bis">
            <asp:TextBox CssClass="campos" ID="txtMotor" runat="server"> </asp:TextBox>
        </div>
    </div>
    <div id="celda_p50">
        <div id="campo_per30">
            <label>
                Color:</label></div>
        <div id="campo_per50bis">
            <asp:TextBox CssClass="campos" ID="txtColor" runat="server"> </asp:TextBox>
        </div>
    </div>
    <div id="celda_p50">
        <div id="campo_per30">
            <label id="lblNMotor" runat="server">
                No Motor:</label>
        </div>
        <div id="campo_per50bis">
            <asp:TextBox CssClass="campos" ID="txtNMotor" runat="server"> </asp:TextBox>
        </div>
    </div>
    <div id="celda_p50">
        <div id="campo_per30">
            <label id="lblNChasis" runat="server">
                No de Chasis:</label>
        </div>
        <div id="campo_per50bis">
            <asp:TextBox CssClass="campos" ID="txtNChasis" runat="server"> </asp:TextBox>
        </div>
    </div>
    <div id="tit_personal">
        Información de Identificación
    </div>
    <div id="celda_p50">
        <div id="campo_per30">
            <label>
                Codificación:</label></div>
        <div id="campo_per50">
            <asp:DropDownList CssClass="sol_campo50c" ID="ddlCodificacion" runat="server" />
        </div>
    </div>
    <div id="celda_p50">
        <div id="campo_per30">
            <label>
                No:</label></div>
        <div id="campo_per50bis">
            <asp:TextBox CssClass="campos" ID="txtNCodificacion" runat="server"> </asp:TextBox>
        </div>
    </div>
    <div id="celda_p50">
        <div id="campo_per30">
            <label>
                Tacografo N°:</label></div>
        <div id="campo_per50bis">
            <asp:TextBox CssClass="campos" onkeypress="return ValidarNumeroEntero(event,this);"
                ID="txtNTacografo" runat="server"> </asp:TextBox>
        </div>
    </div>
    <div id="celda_p50">
        <div id="campo_per30">
            <label>
                Alta:</label></div>
        <div id="campo_per50">
            <asp:RadioButton CssClass="check2" GroupName="radioTacografo" ID="rtSi" runat="server" />
            <div id="check">
                Baja:</div>
            <asp:RadioButton CssClass="check2" GroupName="radioTacografo" ID="rtNo" runat="server" />
        </div>
    </div>
    <div id="tit_personal">
        Datos del Vencimiento</div>
    <div id="celda_p50">
        <div id="campo_per30">
            <label id="lblCedulaVerde" runat="server">
                Cedula Verde:</label>
        </div>
        <div id="checkCedulaVerde" class="campo_per50bis" runat="server">
            <div id="check">
                Si</div>
            <asp:RadioButton CssClass="check2" GroupName="radioCVerde" ID="rcdSi" runat="server" />
            <div id="check">
                No</div>
            <asp:RadioButton CssClass="check2" GroupName="radioCVerde" ID="rcvNo" runat="server" />
        </div>
    </div>
    <div id="celda_p50">
        <div id="campo_per30">
            <label id="lblVCedulaVerde" runat="server">
                Vencimiento:</label></div>
        <div id="campo_per50bis">
            <asp:TextBox CssClass="campos" ID="txtVCedulaVerde" runat="server"> </asp:TextBox>
        </div>
        <div id="campo_per_icono">
            <asp:FileUpload ID="fuVCedulaVerde" runat="server" CssClass="subir" Width="18" Height="1"
                border="0"></asp:FileUpload></div>
    </div>
    <div id="celda_p50">
        <div id="campo_per30">
            <label>
                Patente:</label>
        </div>
        <div id="campo_per50bis">
            <div id="check">
                Si</div>
            <asp:RadioButton CssClass="check2" GroupName="radioPatente" ID="rpSi" runat="server" />
            <div id="check">
                No</div>
            <asp:RadioButton CssClass="check2" GroupName="radioPatente" ID="rpNo" runat="server" />
        </div>
    </div>
    <div id="celda_p50">
        <div id="campo_per30">
            <label>
                Vencimiento:</label></div>
        <div id="campo_per50bis">
            <asp:TextBox CssClass="campos" ID="txtVencimientoPatente" runat="server"> </asp:TextBox>
        </div>
        <div id="campo_per_icono">
            <asp:FileUpload ID="fuVencimientoPatente" runat="server" CssClass="subir" Width="18"
                Height="1" border="0"></asp:FileUpload></div>
    </div>
    <div id="celda_p50">
        <div id="campo_per30">
            <label>
                Leasing:</label>
        </div>
        <div id="campo_per50bis">
            <div id="check">
                Si</div>
            <asp:RadioButton CssClass="check2" GroupName="radioLeasing" ID="rlSi" runat="server" />
            <div id="check">
                No</div>
            <asp:RadioButton CssClass="check2" GroupName="radioLeasing" ID="rlNo" runat="server" />
        </div>
    </div>
    <div id="celda_p50">
        <div id="campo_per30">
            <label>
                Vencimiento:</label></div>
        <div id="campo_per50bis">
            <asp:TextBox CssClass="campos" ID="txtVencimientoLeasing" runat="server"> </asp:TextBox>
        </div>
        <div id="campo_per_icono">
            <asp:FileUpload ID="fuVencimientoLeasing" runat="server" CssClass="subir" Width="18"
                Height="1" border="0"></asp:FileUpload></div>
    </div>
    <div id="celda_p50">
        <div id="campo_per30">
            <label>
                VTV No:</label>
        </div>
        <div id="campo_per50bis">
            <asp:TextBox CssClass="campos" ID="txtVTV" runat="server"> </asp:TextBox>
        </div>
    </div>
    <div id="celda_p50">
        <div id="campo_per30">
            <label>
                Vencimiento:</label></div>
        <div id="campo_per50bis">
            <asp:TextBox CssClass="campos" ID="txtVVTV" runat="server"> </asp:TextBox>
        </div>
        <div id="campo_per_icono">
            <asp:FileUpload ID="fuVVTV" runat="server" CssClass="subir" Width="18" Height="1"
                border="0"></asp:FileUpload></div>
    </div>
    <div id="celda_p50">
        <div id="campo_per30">
            <label>
                R.U.T.A:</label>
        </div>
        <div id="campo_per50bis">
            <div id="check">
                Si</div>
            <asp:RadioButton CssClass="check2" GroupName="radioRUTA" ID="rrSi" runat="server" />
            <div id="check">
                No</div>
            <asp:RadioButton CssClass="check2" GroupName="radioRUTA" ID="rrNo" runat="server" />
        </div>
    </div>
    <div id="celda_p50">
        <div id="campo_per30">
            <label>
                Expe. No:</label></div>
        <div id="campo_per50bis">
            <asp:TextBox CssClass="campos" ID="txtExp" runat="server"> </asp:TextBox>
        </div>
        <div id="campo_per_icono">
            <asp:FileUpload ID="fuExp" runat="server" CssClass="subir" Width="18" Height="1"
                border="0"></asp:FileUpload></div>
    </div>
    <div id="celda_p50">
        <div id="campo_per30">
            <label>
                Emisión:</label>
        </div>
        <div id="campo_per50bis">
            <asp:TextBox CssClass="campos" ID="txtEmision" runat="server"> </asp:TextBox>
        </div>
        <div id="campo_per_icono">
            <img id="Img13" alt="calendario" src="../img/calendario.gif" style="width: 15px;
                height: 15px; cursor: pointer;" onmouseover="CambiarConfiguracionCalendario('txtEmision',this);" /></div>
    </div>
    <div id="celda_p50">
        <div id="campo_per30">
            <label>
                Vencimiento:</label></div>
        <div id="campo_per50bis">
            <asp:TextBox CssClass="campos" ID="txtVEmision" runat="server"> </asp:TextBox>
        </div>
        <div id="campo_per_icono">
            <img id="Img7" alt="calendario" src="../img/calendario.gif" style="width: 15px; height: 15px;
                cursor: pointer;" onmouseover="CambiarConfiguracionCalendario('txtVEmision',this);" /></div>
    </div>
    <div id="tit_personal">
        Datos del Seguro</div>
    <div id="celda_p50">
        <div id="campo_per30">
            <label>
                Compañia Aseg:</label>
        </div>
        <div id="campo_per50bis">
            <div id="check">
                Si</div>
            <asp:RadioButton CssClass="check2" GroupName="radioAseguradora" ID="rasSi" runat="server" />
            <div id="check">
                No</div>
            <asp:RadioButton CssClass="check2" GroupName="radioAseguradora" ID="rasNo" runat="server" />
        </div>
    </div>
    <div id="celda_p50">
        <div id="campo_per30">
            <label>
                N° Poliza:</label></div>
        <div id="campo_per50bis">
            <asp:TextBox CssClass="campos" ID="txtNPoliza" runat="server"> </asp:TextBox>
        </div>
        <div id="campo_per_icono">
            <asp:FileUpload ID="fuNPoliza" runat="server" CssClass="subir" Width="18" Height="1"
                border="0"></asp:FileUpload></div>
    </div>
    <div id="celda_p50">
        <div id="campo_per30">
            <label>
                Vigencia desde:</label>
        </div>
        <div id="campo_per50bis">
            <asp:TextBox CssClass="campos" ID="txtPolizaDesde" runat="server"> </asp:TextBox>
        </div>
    </div>
    <div id="celda_p50">
        <div id="campo_per30">
            <label>
                Hasta:</label></div>
        <div id="campo_per50bis">
            <asp:TextBox CssClass="campos" ID="txtPolizaHasta" runat="server"> </asp:TextBox>
        </div>
        <div id="campo_per_icono">
            <asp:FileUpload ID="fuPolizaHasta" runat="server" CssClass="subir" Width="18" Height="1"
                border="0"></asp:FileUpload></div>
    </div>
    <div id="celda_p100">
        <div id="campo_per50">
            <asp:Button ID="btn_archivos" runat="server" BackColor="LightGray" Text="Archivos"
                Enabled="false" Visible="false" OnClick="btn_cargarArchivos_click" />
        </div>
    </div>
    
    <div id="celda_pbtns">
        <asp:Button ID="btn_guardar" CssClass="btn_personal" OnClientClick="return verificarForm()"
            OnClick="btn_guardar_Click" Text="GUARDAR" runat="server" />
        <asp:Button ID="btn_cancelar" CssClass="btn_personal" Text="CANCELAR" runat="server"
            OnClick="btn_cancelar_Click" />
    </div>
    <%-- Espacio de PopUp de Archivos --%>
    <div id="capaPopUp">
    </div>
    <div id="popUpDiv">
        <div id="capaContent">
            <div style="width: 90%; padding-top: 5%; padding-left: 10%; padding-bottom: 5%; height: 75%;
                overflow: auto;">
                <asp:GridView ID="gvVechiculosFiles" runat="server" AutoGenerateColumns="False" DataMember="idVehiculo"
                    CssClass="columna_per" HeaderStyle-CssClass="celda1" OnRowCommand="gvVehiculosFiles_RowCommand"
                    OnRowDataBound="gvVehiculosFiles_RowDataBound" AllowPaging="true" PageSize="20"
                    OnPageIndexChanging="gvVehiculosFiles_PageIndexChanging" PagerStyle-CssClass="tabla_font"
                    EmptyDataText="No existen Archivos" EmptyDataRowStyle-CssClass="celda50" Width="95%"
                    EmptyDataRowStyle-Width="80%">
                    <Columns>
                        <asp:TemplateField HeaderText="Nombre del Archivo" HeaderStyle-CssClass="columna_per"
                            ItemStyle-CssClass="columna_per">
                            <ItemTemplate>
                                <asp:Label ID="lblNombreArchivo" runat="server" Text='<%# Bind("Nombre") %>' ItemStyle-CssClass="columna5"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-CssClass="btn_iconos">
                            <ItemTemplate>
                                <asp:LinkButton ID="btnVer" runat="server" title="Ver Archivo" CssClass="ver" OnClientClick='<%# string.Format("javascript:return AbrirVentana(\"{0}\")",Eval("idVehiculoFiles")) %>'></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-CssClass="btn_iconos">
                            <ItemTemplate>
                                <asp:LinkButton ID="btnEliminar" runat="server" title="Eliminar Archivo" CssClass="eliminar"
                                    CommandArgument='<%# Bind("idVehiculoFiles") %>' CommandName="Eliminar" OnClientClick="return Eliminar('Archivo');"></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>
            <div id="botones" style="width: 95%; height: 10%;">
                <div class="regreso_3">
                    <input type="button" onclick="cerrarPopUp();" title="Cerrar" id="btnCerrar" class="btn_form"
                        value="CERRAR" />
                </div>
            </div>
        </div>
    </div>
</asp:Content>
