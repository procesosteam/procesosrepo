﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using ProcesoPP.Business;
using ProcesoPP.Model;
using ProcesoPP.Services;

namespace ProcesoPP.UI.Vehiculo
{
    public partial class frmVehiculoHistorial : System.Web.UI.Page
    {
        #region PROPIEDADES

        public Permisos _Permiso
        {
            get { return (Permisos)ViewState["permiso"]; }
            set { ViewState["permiso"] = value; }
        }

        public int _idUsuario
        {
            get { return (int)ViewState["idUsuario"]; }
            set { ViewState["idUsuario"] = value; }
        }

        public int _ADM
        {
            get { return int.Parse(ConfigurationManager.AppSettings["ADM"].ToString()); }
        }

        #endregion

        #region EVENTOS

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                _idUsuario = Common.EvaluarSession(Request, this.Page);
                if (_idUsuario != 0)
                {
                    _Permiso = Common.getModulo(Request, this.Page);
                    if (_Permiso != null && (_Permiso.Lectura && _Permiso.Escritura))
                    {
                        if (!IsPostBack)
                        {
                            CargarCodificacion();
                            CargarEmpresa();


                        }
                    }
                    else
                    {
                        Mensaje.errorMsj("NO TIENE PERMISOS A ESTE FORMULARIO", this.Page, "SIN ACCESO", "../frmPrincipal.aspx");
                    }
                }
                else
                {
                    Mensaje.errorMsj("DEBE LOGUEARSE", this.Page, "LOGIN", "../frmLogin.aspx");
                }
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        private void CargarCodificacion()
        {
            CodificacionBus oCodificacionBus = new CodificacionBus();
            ddlCodificacion.DataSource = oCodificacionBus.CodificacionGetAll();
            ddlCodificacion.DataTextField = "Descripcion";
            ddlCodificacion.DataValueField = "IdCodificacion";
            ddlCodificacion.DataBind();
            ddlCodificacion.Items.Insert(0, new ListItem("Seleccione Codificacion", "0"));
        }




        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                CargarVehiculo();
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }



        protected void gvVehiculo_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            switch (e.CommandName)
            {

                case "Ver":
                    Response.Redirect("frmVehiculoCarga.aspx?idVehiculo=" + e.CommandArgument + "&idModulo=" + _Permiso.oModulo.idModulo + "&Modificando=false");
                    break;

                //case "Eliminar":
                //    try
                //    {
                //        Eliminar(e.CommandArgument.ToString());
                //    }
                //    catch (Exception ex)
                //    {
                //        Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
                //    }
                //    break;
            }

        }

        protected void gvVehiculo_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
                CargarRow(e);
        }

        protected void gvVehiculo_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvVehiculo.PageIndex = e.NewPageIndex;
            CargarVehiculo();
        }

        #endregion

        #region METODOS


        private void CargarRow(GridViewRowEventArgs e)
        {
            //LinkButton btnEliminar = e.Row.FindControl("btnEliminar") as LinkButton;
            Common.CargarRow(e, _Permiso);

            //ColorearLabel("lblVencimientoCedula", e);
            //ColorearLabel("lblVencimientoPatente", e);
            //ColorearLabel("lblVencimientoLeasing", e);
            //ColorearLabel("lblVencimientoVTV", e);
            //ColorearLabel("lblVencimiento", e);
            //ColorearLabel("lblSeguro", e);


        }

        private void ColorearLabel(string nombre, GridViewRowEventArgs e)
        {
            TimeSpan comparacion;
            Label lbl;
            lbl = e.Row.FindControl(nombre) as Label;
            if (lbl.Text != "")
            {
                DateTime fechaVenc = DateTime.Parse(lbl.Text);
                if (fechaVenc <= DateTime.Now)
                {
                    ((System.Web.UI.WebControls.DataControlFieldCell)(lbl.Parent)).BackColor = System.Drawing.Color.Red;
                    //e.Row.BackColor = System.Drawing.Color.Red;
                }
                else
                {
                    comparacion = (fechaVenc).Subtract(DateTime.Now);
                    if (comparacion.TotalDays <= 30)
                    {
                        ((System.Web.UI.WebControls.DataControlFieldCell)(lbl.Parent)).BackColor = System.Drawing.Color.Yellow;
                        //e.Row.BackColor = System.Drawing.Color.Yellow;
                    }
                }
            }
        }



        //Baja fisica de la base
        //private void Eliminar(string idVehiculo)
        //{
        //    VehiculoBus oVehiculoBus = new VehiculoBus();
        //    bool eliminado = oVehiculoBus.VehiculoDelete(int.Parse(idVehiculo));
        //    CargarVehiculo();
        //}

        private void CargarVehiculo()
        {
            VehiculoBus oVehiculoBus = new VehiculoBus();
            gvVehiculo.DataSource = oVehiculoBus.VehiculoHistorialGetByFilter(txtPatente.Text, txtRuta.Text, txtVCedula.Text, txtVtoPatente.Text, txtVTV.Text, (ddlCodificacion.SelectedValue), (ddlEmpresa.SelectedValue));
            gvVehiculo.DataBind();
        }

        private void CargarEmpresa()
        {
            //Cargamos las empresasInternas
            EmpresaBus oEmpresaBus = new EmpresaBus();
            oEmpresaBus.CargarEmpresaInterna(ref ddlEmpresa, "Seleccione Empresa");

        }

        #endregion

    }
}