﻿<%@ Page Title="" Language="C#" MasterPageFile="~/frmPrincipal.Master" AutoEventWireup="true"
    CodeBehind="frmVehiculoGestion.aspx.cs" Inherits="ProcesoPP.UI.Vehiculo.frmVehiculoGestion" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../Js/jquery.maskedinput.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () { SetMenu("liVehiculos"); }); 
        
   
        jQuery(function ($) {
            $("#<%=txtVtoPatente.ClientID%>").mask("99/99/9999", { placeholder: "dd/mm/yyyy" });
        });
        jQuery(function ($) {
            $("#<%=txtVCedula.ClientID%>").mask("99/99/9999", { placeholder: "dd/mm/yyyy" });
        });
        jQuery(function ($) {
            $("#<%=txtPatente.ClientID%>").mask("aaa-999");
        });
    </script>
</asp:Content>
<asp:Content ID="cphContenedor" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="contenido" runat="server">
        <div id="apertura">
            <asp:label ID="lblTitulo" runat="server"></asp:label></div>
        <div class="solicitud100">
            <div class="sol_tit50">
                <label>
                    Patente</label></div>
            <div class="sol_campo50">
                <asp:TextBox runat="server" ID="txtPatente" /></div>
            <div class="sol_fecha">
            </div>
            <div class="sol_fecha">
            </div>
            <div class="sol_tit50">
                <label>
                    VTV</label></div>
            <div class="sol_campo50">
                <asp:TextBox runat="server" ID="txtVTV" /></div>
        </div>
        <div class="solicitud100">
            <div class="sol_tit50">
                <label>
                    Ruta</label></div>
            <div class="sol_campo50">
                <asp:TextBox runat="server" CssClass="sol_campo50c" ID="txtRuta" />
            </div>
            <div class="sol_fecha">
            </div>
            <div class="sol_fecha">
            </div>
            <div class="sol_tit50">
                <label>
                    Vto Patente</label></div>
            <div class="sol_campo50">
                <asp:TextBox runat="server" CssClass="sol_campo50c" ID="txtVtoPatente" />
            </div>
        </div>
        <div class="solicitud100">
            <div class="sol_tit50">
                <label>Vto Cedula Verde</label></div>
            <div class="sol_campo50">
                <asp:TextBox runat="server" CssClass="sol_campo25" ID="txtVCedula" /></div>
            <div class="sol_fecha">
            </div>
            <div class="sol_fecha">
            </div>
            <div class="sol_tit50">
                <label>Codificacion</label></div>
            <div class="sol_campo50">
                <asp:DropDownList runat="server" CssClass="sol_campo50c" ID="ddlCodificacion" />
            </div>
        </div>
        <div class="solicitud100">
            <div class="sol_tit50">
                <label>Empresa</label></div>
            <div class="sol_campo50">
                <asp:DropDownList runat="server" CssClass="sol_campo50c" ID="ddlEmpresa" />
            </div>
        </div>
    </div>
    <div class="solicitud100sin">
        <div class="sol_btn">
            <asp:Button runat="server" ID="btnBuscar" CssClass="btn_form" Text="Buscar" OnClick="btnBuscar_Click" />
            <asp:Button runat="server" ID="btnExportar" CssClass="btn_form" Text="Exportar" OnClick="btExportar_Click" />
        </div>
    </div>
    <asp:GridView ID="gvVehiculo" runat="server" AutoGenerateColumns="False" DataMember="idVehiculo"
        CssClass="tabla3" HeaderStyle-CssClass="celda1" OnRowCommand="gvVehiculo_RowCommand"
        OnRowDataBound="gvVehiculo_RowDataBound" AllowPaging="true" PageSize="20" OnPageIndexChanging="gvVehiculo_PageIndexChanging"
        PagerStyle-CssClass="tabla_font">
        <Columns>
            
            <asp:TemplateField HeaderText="Codificacion" HeaderStyle-CssClass="columna6" ItemStyle-CssClass="columna6">
                <ItemTemplate>
                    <asp:Label ID="lblCodificacion" runat="server" Text='<%# Bind("Codificacion") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Patente" HeaderStyle-CssClass="columna7" ItemStyle-CssClass="columna7">
                <ItemTemplate>
                    <asp:Label ID="lblPatente" runat="server" Text='<%# Bind("Patente") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Empresa" HeaderStyle-CssClass="columna6"
                ItemStyle-CssClass="columna6">
                <ItemTemplate>
                    <asp:Label ID="lblEmpresa" runat="server" Text='<%# Bind("Empresa") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
           
            
            <asp:TemplateField HeaderText="Venc. Cedula" HeaderStyle-CssClass="columna7"
                ItemStyle-CssClass="columna7">
                <ItemTemplate>
                    <asp:Label ID="lblVencimientoCedula" runat="server" Text='<%# Bind("VencimientoCedula") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>            
            <asp:TemplateField HeaderText="Venc. Patente" HeaderStyle-CssClass="columna7"
                ItemStyle-CssClass="columna7">
                <ItemTemplate>
                    <asp:Label ID="lblVencimientoPatente" runat="server" Text='<%# Bind("VencimientoPatente") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Venc. Leasing" HeaderStyle-CssClass="columna7"
                ItemStyle-CssClass="columna7">
                <ItemTemplate>
                    <asp:Label ID="lblVencimientoLeasing" runat="server" Text='<%# Bind("VencimientoLeasing") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Venc. VTV" HeaderStyle-CssClass="columna7" ItemStyle-CssClass="columna7">
                <ItemTemplate>
                    <asp:Label ID="lblVencimientoVTV" runat="server" Text='<%# Bind("VencimientoVTV") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Venc. R.U.T.A" HeaderStyle-CssClass="columna7" ItemStyle-CssClass="columna7">
                <ItemTemplate>
                    <asp:Label ID="lblVencimiento" runat="server" Text='<%# Bind("Vencimiento") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Venc Seguro" HeaderStyle-CssClass="columna7"
                ItemStyle-CssClass="columna7">
                <ItemTemplate>
                    <asp:Label ID="lblSeguro" runat="server" Text='<%# Bind("VencimientoSeguro") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField ItemStyle-CssClass="btn_iconos" HeaderStyle-CssClass="btn_iconos">
                <ItemTemplate>
                    <asp:LinkButton ID="btnVer" runat="server" title="Ver" CssClass="ver" CommandArgument='<%# Bind("idVehiculo") %>'
                        CommandName="Ver"></asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField ItemStyle-CssClass="btn_iconos" HeaderStyle-CssClass="btn_iconos">
                <ItemTemplate>
                    <asp:LinkButton ID="btnModificar" runat="server" title="Editar Equipo" CssClass="editar"
                        CommandArgument='<%# Bind("idVehiculo") %>' CommandName="Modificar"></asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField ItemStyle-CssClass="btn_iconos" HeaderStyle-CssClass="btn_iconos">
                <ItemTemplate>
                    <asp:LinkButton ID="btnEliminar" runat="server" title="Eliminar Equipo" CssClass="eliminar"
                        CommandArgument='<%# Bind("idVehiculo") %>' CommandName="Eliminar" OnClientClick="return Eliminar('el Vehiculo');"></asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
</asp:Content>
