﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ProcesoPP.Business;
using ProcesoPP.Model;
using ProcesoPP.Services;
using System.Data;
using System.Configuration;
using System.IO;
using System.Diagnostics;
using System.Web.Services;
using AjaxControlToolkit;

namespace ProcesoPP.UI.Vehiculo
{
    public partial class frmCargaVehiculo : System.Web.UI.Page
    {
        #region <PROPIEDADES>

        public int _idUsuario
        {
            get { return (int)ViewState["idUsuario"]; }
            set { ViewState["idUsuario"] = value; }
        }

        public int _ADM
        {
            get { return int.Parse(ConfigurationManager.AppSettings["ADM"].ToString()); }
        }

        public int _idVehiculo
        {
            get { return (int)ViewState["idVehiculo"]; }
            set { ViewState["idVehiculo"] = value; }
        }

        public int _IdIdentificacion
        {
            get { return (int)ViewState["IdIdentificacion"]; }
            set { ViewState["IdIdentificacion"] = value; }
        }

        public Permisos _Permiso
        {
            get { return (Permisos)ViewState["permiso"]; }
            set { ViewState["permiso"] = value; }
        }


        public bool _Modificando
        {
            get { return (bool)ViewState["Modificando"]; }
            set { ViewState["Modificando"] = value; }
        }

        public bool _EsEquipo
        {
            get { return (bool)ViewState["EsEquipo"]; }
            set { ViewState["EsEquipo"] = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                _idUsuario = Common.EvaluarSession(Request, this.Page);
                if (_idUsuario != 0)
                {
                    _Permiso = Common.getModulo(Request, this.Page);
                    if (_Permiso != null && (_Permiso.Lectura && _Permiso.Escritura))
                    {
                        if (!IsPostBack)
                        {

                            VerificarSiEsEquipo();
                            CargarTitulo();
                            InitScreen();
                        }
                    }
                    else
                    {
                        Mensaje.errorMsj("NO TIENE PERMISOS A ESTE FORMULARIO", this.Page, "SIN ACCESO", "../frmPrincipal.aspx");
                    }
                }
                else
                {
                    Mensaje.errorMsj("DEBE LOGUEARSE", this.Page, "LOGIN", "../frmLogin.aspx");
                }
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        
        protected void btn_guardar_Click(object sender, EventArgs e)
        {
            try
            {
                comprobarFechas();
                if (hfFechasValidas.Value == "false")
                {
                    Mensaje.errorMsj("Falta(n) fecha(s). Por favor revise la(s) fecha(s)", this.Page, "Error", null);
                }
                if (!_EsEquipo)
                {
                    if ((txtMotor.Text == "") || (txtNMotor.Text == ""))
                    {
                        Mensaje.errorMsj("Falta(n) dato(s). Por favor revise datos de motor", this.Page, "Error", null);
                        
                    }
                    if(txtPatente.Text == "")
                    {
                        Mensaje.errorMsj("Falta cargar datos de la Patente. Por favor revise", this.Page, "Error", null);
                    }
                    if(txtNChasis.Text == "")
                        Mensaje.errorMsj("Falta cargar el Numero de Chasis. Por favor revise", this.Page, "Error", null);
                }
                if (bool.Parse(hfEsValido.Value))
                {
                    GuardarVehiculo();
                    GuardarArchivos();
                    Mensaje.successMsj("Datos guardados correctamente", this.Page, "Guardado", "frmVehiculoGestion.aspx?idUsuario=" + this._idUsuario + "&idModulo=" + this._Permiso.oModulo.idModulo + "&EsEquipo="+_EsEquipo);
                }
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }



        public void btn_cancelar_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("frmVehiculoGestion.aspx?idUsuario=" + this._idUsuario + "&idModulo=" + this._Permiso.oModulo.idModulo+"&EsEquipo="+_EsEquipo);
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }


        protected void btn_cargarArchivos_click(object sender, EventArgs e)
        {
            try
            {
                gvVechiculosFiles.Visible = true;
                CargarVehiculosFiles();
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }



        protected void gvVehiculosFiles_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            switch (e.CommandName)
            {

                //case "Ver":
                //    string ruta = e.CommandArgument.ToString();
                    
                //    if (ruta.EndsWith("pdf"))
                //    {
                //        Response.ContentType = "Application/pdf";
                //        Response.TransmitFile(ruta);
                //    }
                //    else if (ruta.EndsWith("jpg"))
                //    {
                //        Response.ContentType = "image/jpeg";
                //        Response.TransmitFile(ruta);
                //    }
                //    else if (ruta.EndsWith("jpeg"))
                //    {
                //        Response.ContentType = "image/pjpeg";
                //        Response.TransmitFile(ruta);
                //    }
                //    else if (ruta.EndsWith("png"))
                //    {
                //        Response.ContentType = "image/png";
                //        Response.TransmitFile(ruta);
                //    }
                //    else if (ruta.EndsWith("bmp"))
                //    {
                //        Response.ContentType = "image/bmp";
                //        Response.TransmitFile(ruta);
                //    }
                //    else
                //    {
                //        Response.TransmitFile(ruta);
                //    }
                //    break;
                    
                case "Eliminar":
                    try
                    {
                        Eliminar(int.Parse(e.CommandArgument.ToString()));
                    }
                    catch (Exception ex)
                    {
                        Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
                    }
                    break;
            }
        }

        protected void gvVehiculosFiles_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
                CargarRow(e);
        }

        protected void gvVehiculosFiles_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvVechiculosFiles.PageIndex = e.NewPageIndex;
            CargarVehiculosFiles();
        }


        private void CargarRow(GridViewRowEventArgs e)
        {
            LinkButton btnEliminar = e.Row.FindControl("btnEliminar") as LinkButton;
            Common.CargarRow(e, _Permiso);


            if (_Permiso.oTipoUsuario.idTipoUsuario == _ADM)
                btnEliminar.Visible = true;
            else
                btnEliminar.Visible = false;

        }

        #endregion

        #region METODOS

        private void InitScreen()
        {
            CargarMarcas();
            CargarCodificacion();
            if (Request["idVehiculo"] != null)
            {

                Model.Vehiculo oVehiculo;
                VehiculoBus oVehiculoBus = new VehiculoBus();
                if (Request["Modificando"] != null)
                    _Modificando = bool.Parse(Request["Modificando"]);
                else
                    _Modificando = true;
                int id = int.Parse(Request["idVehiculo"]);
                _idVehiculo = id;
                oVehiculo = oVehiculoBus.VehiculoGetById(id);
                CargarVehiculoExistente(oVehiculo);
                if (_Modificando == false)
                {
                    Common.AnularControles(this.Form);
                    this.btn_guardar.Visible = false;
                    this.btn_archivos.Visible = true;
                    this.btn_archivos.Enabled = true;
                    this.btn_cancelar.Enabled = true;
                }

            }
            else
            {
                _Modificando = false;

            }

            if (_EsEquipo)
            {
                lblCedulaVerde.Visible = false;
                lblVCedulaVerde.Visible = false;
                lblNChasis.Visible = false;
                lblNMotor.Visible = false;
                lblMotor.Visible = false;
                txtNChasis.Visible =false;
                txtNMotor.Visible = false;
                txtMotor.Visible = false;
                rcdSi.Visible = false;
                rcvNo.Visible = false;
                txtVCedulaVerde.Visible = false;
                fuVCedulaVerde.Visible = false;
                checkCedulaVerde.Visible = false;
                
            }

            if (!_Permiso.Escritura)
            {
                btn_guardar.Visible = false;
            }
        }

        private void VerificarSiEsEquipo()
        {
            if (Request["EsEquipo"] != null)
                _EsEquipo = bool.Parse(Request["EsEquipo"]);
            else
                _EsEquipo = false;
        }

        private void CargarTitulo()
        {
            if (_EsEquipo)
            {
                lbltitulo1.Text = "Equipo. Alta de Equipo";
            }
            else
            {
                lbltitulo1.Text = "Vehiculo. Alta de Vehiculo";
            }
        }

        private void CargarCodificacion()
        {
            CodificacionBus oCodif = new CodificacionBus();
            if(_EsEquipo)
                ddlCodificacion.DataSource = oCodif.CodificacionGetEquipo();
            else
                ddlCodificacion.DataSource = oCodif.CodificacionGetVehiculo();
            ddlCodificacion.DataTextField = "Codigo";
            ddlCodificacion.DataValueField = "IdCodificacion";
            ddlCodificacion.DataBind();
            ddlCodificacion.Items.Insert(0, new ListItem("Seleccione Codificacion", "0"));
        }

        private bool esFecha(TextBox t)
        {
            string cadena = t.Text;
            bool esValida = true;
            if ((cadena.IndexOf('/') < 0) && (!esNumerico(t)))
            {
                esValida = false;
            }
            return esValida;
        }

        private bool esNumerico(TextBox t)
        {
            string cadena = t.Text;
            bool esValida = true;
            int pos = 0;
            while ((esValida) && (cadena.Length - 1 < pos))
            {
                if (!char.IsNumber(cadena, pos))
                {
                    esValida = false;
                }
            }
            return esValida;
        }



        private void CargarVehiculoExistente(Model.Vehiculo oVehiculo)
        {
            VehiculoIdentificacion oVehiculoI;
            VehiculoIdentificacionBus oVehiculoIBus = new VehiculoIdentificacionBus();
            VehiculoSeguro oVehiculoS;
            VehiculoSeguroBus oVehiculoSeguroBus = new VehiculoSeguroBus();
            VehiculoVencimiento oVehiculoVencimiento;
            VehiculoVencimientoBus oVehiculoVencimientoBus = new VehiculoVencimientoBus();
            ddlMarca.SelectedValue = oVehiculo.IdMarca.ToString();
            CargarEmpresaSector(oVehiculo.IdSector);
            //ddlSector.SelectedValue = oVehiculo.IdSector.ToString();
            txtAnioVehiculo.Text = oVehiculo.Anio.ToString();
            txtColor.Text = oVehiculo.Color;
            txtModelo.Text = oVehiculo.Modelo;
            txtNMotor.Text = oVehiculo.NroMotor;
            txtMotor.Text = oVehiculo.PotenciaMotor;
            txtNChasis.Text = oVehiculo.NroChasis;
            txtPatente.Text = oVehiculo.Patente;
            if (oVehiculo.FechaAltaEmpresa.Year > 1910)
            {
                raSi.Checked = true;
                txtFechaAlta.Text = oVehiculo.FechaAltaEmpresa.ToString("dd/MM/yyyy");
            }
            else
                raNo.Checked = true;
            //if (oVehiculo.FechaBajaEmpresa.Year > 1910)
            //{
            //    rbSi.Checked = true;
            //    txtFechaBaja.Text = oVehiculo.FechaBajaEmpresa.ToString("dd/MM/yyyy");
            //}
            //else
            //    rbNo.Checked = true;
            txtTitular.Text = oVehiculo.Titular;


            oVehiculoI = oVehiculoIBus.VehiculoIdentificacionGetByIdVehiculo(oVehiculo.IdVehiculo);
            if (oVehiculoI != null)
            {
                ddlCodificacion.SelectedValue = oVehiculoI.IdCodificacion.ToString();
                txtNTacografo.Text = oVehiculoI.NroTacografo;

                if (oVehiculoI.Alta)
                    rtSi.Checked = true;
                else
                    rtNo.Checked = true;
                txtNCodificacion.Text = oVehiculoI.NroCodificacion;

            }
            oVehiculoS = oVehiculoSeguroBus.VehiculoSeguroGetByIdVehiculo(oVehiculo.IdVehiculo);
            if (oVehiculoS != null)
            {
                if (oVehiculoS.CompaniaAseg)
                    rasSi.Checked = true;
                else
                    rasNo.Checked = true;
                txtNPoliza.Text = oVehiculoS.NroPoliza;
                if (oVehiculoS.VigenciaDesde.Year > 1910)
                    txtPolizaDesde.Text = oVehiculoS.VigenciaDesde.ToString("dd/MM/yyyy");
                if (oVehiculoS.VigenciaHasta.Year > 1910)
                    txtPolizaHasta.Text = oVehiculoS.VigenciaHasta.ToString("dd/MM/yyyy");

            }
            oVehiculoVencimiento = oVehiculoVencimientoBus.VehiculoVencimientoGetByIdVehiculo(oVehiculo.IdVehiculo);
            if (oVehiculoVencimiento != null)
            {
                if (oVehiculoVencimiento.VencimientoCedula.Year > 1910)
                {
                    rcdSi.Checked = true;
                    txtVCedulaVerde.Text = oVehiculoVencimiento.VencimientoCedula.ToString("dd/MM/yyyy");
                }
                else
                {
                    rcvNo.Checked = true;
                }
                if (oVehiculoVencimiento.VencimientoLeasing.Year > 1910)
                {
                    rlSi.Checked = true;
                    txtVencimientoLeasing.Text = oVehiculoVencimiento.VencimientoLeasing.ToString("dd/MM/yyyy");
                }
                else
                {
                    rlNo.Checked = true;
                }
                if (oVehiculoVencimiento.VencimientoPatente.Year > 1910)
                {
                    rpSi.Checked = true;
                    txtVencimientoPatente.Text = oVehiculoVencimiento.VencimientoPatente.ToString("dd/MM/yyyy");
                }
                else
                {
                    rpNo.Checked = true;
                }
                if (oVehiculoVencimiento.VencimientoVTV.Year > 1910)
                    txtVVTV.Text = oVehiculoVencimiento.VencimientoVTV.ToString("dd/MM/yyyy");
                txtVTV.Text = oVehiculoVencimiento.NroVTV;
                if (oVehiculoVencimiento.RUTA)
                    rrSi.Checked = true;
                else
                    rrNo.Checked = true;
                txtExp.Text = oVehiculoVencimiento.NroExpediente;
                if (oVehiculoVencimiento.Emision.Year > 1910)
                    txtEmision.Text = oVehiculoVencimiento.Emision.ToString("dd/MM/yyyy");
                if (oVehiculoVencimiento.Vencimiento.Year > 1910)
                    txtVEmision.Text = oVehiculoVencimiento.Vencimiento.ToString("dd/MM/yyyy");


            }


        }

        private void CargarEmpresa()
        {
            //Cargamos las empresasInternas
            EmpresaBus oEmpresaBus = new EmpresaBus();
            oEmpresaBus.CargarEmpresaInterna(ref ddlEmpresa, "Seleccione Empresa");

        }

        //private void CargarSector(int idEmpresa)
        //{
        //    SectorBus oSector = new SectorBus();
        //    ddlSector.DataSource = oSector.SectorGetAllByIdEmpresa(idEmpresa);
        //    ddlSector.DataTextField = "Descripcion";
        //    ddlSector.DataValueField = "IdSector";
        //    ddlSector.DataBind();
        //}


        private void CargarEmpresaSector(int idSector)
        {
            SectorBus oSectorBus = new SectorBus();
            Sector oSector = oSectorBus.SectorGetById(idSector);
            cddEmpresa.SelectedValue = oSector.IdEmpresa.ToString();
            GetSector("idEmpresa:" + cddEmpresa.SelectedValue + ";", "idSector");
            cddSector.SelectedValue = idSector.ToString();
            
        }

        private void GuardarVehiculo()
        {
            bool tieneTacografo = false;
            Model.Vehiculo oVehiculo;
            VehiculoBus oVehiculoBus = new VehiculoBus();
            VehiculoIdentificacion oVehiculoI;
            VehiculoIdentificacionBus oVehiculoIBus = new VehiculoIdentificacionBus();
            VehiculoSeguro oVehiculoS;
            VehiculoSeguroBus oVehiculoSeguroBus = new VehiculoSeguroBus();
            VehiculoVencimiento oVehiculoVencimiento;
            VehiculoVencimientoBus oVehiculoVencimientoBus = new VehiculoVencimientoBus();
            if (_Modificando)
            {
                int id = int.Parse(Request["idVehiculo"]);
                oVehiculo = oVehiculoBus.VehiculoGetById(id);
                oVehiculoI = oVehiculoIBus.VehiculoIdentificacionGetByIdVehiculo(id);
                if (oVehiculoI.IdIdentificacion == 0)
                    oVehiculoI = new VehiculoIdentificacion();
                oVehiculoS = oVehiculoSeguroBus.VehiculoSeguroGetByIdVehiculo(id);
                if (oVehiculoS.IdSeguro == 0)
                    oVehiculoS = new VehiculoSeguro();
                oVehiculoVencimiento = oVehiculoVencimientoBus.VehiculoVencimientoGetByIdVehiculo(id);
                if (oVehiculoVencimiento.IdVencimiento == 0)
                    oVehiculoVencimiento = new VehiculoVencimiento();
            }
            else
            {
                oVehiculo = new Model.Vehiculo();
                oVehiculoI = new VehiculoIdentificacion();
                oVehiculoS = new VehiculoSeguro();
                oVehiculoVencimiento = new VehiculoVencimiento();
                oVehiculo.FechaAlta = DateTime.Now;
            }
            //Guardamos los campos
            oVehiculo.EsEquipo = _EsEquipo;
            oVehiculo.Anio = int.Parse(txtAnioVehiculo.Text);
            oVehiculo.Color = txtColor.Text;
            if (txtFechaAlta.Text != "")
                oVehiculo.FechaAltaEmpresa = DateTime.Parse(txtFechaAlta.Text);
            else
                oVehiculo.FechaAltaEmpresa = DateTime.MinValue;
            //if (txtFechaBaja.Text != "")
            //    oVehiculo.FechaBajaEmpresa = DateTime.Parse(txtFechaBaja.Text);
            oVehiculo.IdEmpresa = int.Parse(ddlEmpresa.SelectedValue);
            oVehiculo.IdMarca = int.Parse(ddlMarca.SelectedValue);
            oVehiculo.IdSector = int.Parse(ddlSector.SelectedValue);
            oVehiculo.Modelo = txtModelo.Text;
            oVehiculo.NroChasis = txtNChasis.Text;
            oVehiculo.NroMotor = txtNMotor.Text;
            oVehiculo.Patente = txtPatente.Text;
            oVehiculo.PotenciaMotor = txtMotor.Text;
            oVehiculo.Titular = txtTitular.Text;
            oVehiculo.IdUsuario = _idUsuario;
            oVehiculo.Alta = raSi.Checked;
            //oVehiculo.Baja = rbSi.Checked;

            //Guardamos la informacion de Vehiculo Identificacion
            oVehiculoI.Alta = rtSi.Checked;
            oVehiculoI.Baja = rtNo.Checked;
            oVehiculoI.IdCodificacion = ddlCodificacion.SelectedValue.Equals("") ? 0 : int.Parse(ddlCodificacion.SelectedValue);
            oVehiculoI.NroTacografo = txtNTacografo.Text;
            oVehiculoI.NroCodificacion = txtNCodificacion.Text;

            //Guardamos la informacion de Vehiculo Seguro
            oVehiculoS.CompaniaAseg = rasSi.Checked;
            oVehiculoS.NroPoliza = txtNPoliza.Text;
            if (txtPolizaDesde.Text != "")
                oVehiculoS.VigenciaDesde = DateTime.Parse(txtPolizaDesde.Text);
            else
                oVehiculoS.VigenciaDesde = DateTime.MinValue;
            if (txtPolizaHasta.Text != "")
                oVehiculoS.VigenciaHasta = DateTime.Parse(txtPolizaHasta.Text);
            else
                oVehiculoS.VigenciaHasta = DateTime.MinValue;

            //Guardamos la informacion de Vencimiento
            if (txtEmision.Text != "")
                oVehiculoVencimiento.Emision = DateTime.Parse(txtEmision.Text);
            else
                oVehiculoVencimiento.Emision = DateTime.MinValue;
            oVehiculoVencimiento.NroExpediente = txtExp.Text;
            oVehiculoVencimiento.NroVTV = txtVTV.Text;
            oVehiculoVencimiento.RUTA = rrSi.Checked;
            if (txtVEmision.Text != "")
                oVehiculoVencimiento.Vencimiento = DateTime.Parse(txtVEmision.Text);
            else
                oVehiculoVencimiento.Vencimiento = DateTime.MinValue;
            if (txtVCedulaVerde.Text != "")
                oVehiculoVencimiento.VencimientoCedula = DateTime.Parse(txtVCedulaVerde.Text);
            else
                oVehiculoVencimiento.VencimientoCedula = DateTime.MinValue;
            if (txtVencimientoLeasing.Text != "")
                oVehiculoVencimiento.VencimientoLeasing = DateTime.Parse(txtVencimientoLeasing.Text);
            else
                oVehiculoVencimiento.VencimientoLeasing = DateTime.MinValue;
            if (txtVencimientoPatente.Text != "")
                oVehiculoVencimiento.VencimientoPatente = DateTime.Parse(txtVencimientoPatente.Text);
            else
                oVehiculoVencimiento.VencimientoPatente = DateTime.MinValue;
            if (txtVVTV.Text != "")
                oVehiculoVencimiento.VencimientoVTV = DateTime.Parse(txtVVTV.Text);
            else
                oVehiculoVencimiento.VencimientoVTV = DateTime.MinValue;

            //Guardamos en las tablas correspondientes
            if (_Modificando)
            {
                //Verificamos que alguno de ellos puede no existir
                if (oVehiculoI.IdIdentificacion == 0)
                {
                    oVehiculoI.IdVehiculo = oVehiculo.IdVehiculo;
                    oVehiculoIBus.VehiculoIdentificacionAdd(oVehiculoI);

                }
                //Si existe lo actualizamos
                else
                {
                    oVehiculoIBus.VehiculoIdentificacionUpdate(oVehiculoI);
                }
                if (oVehiculoS.IdSeguro == 0)
                {
                    oVehiculoS.IdVehiculo = oVehiculo.IdVehiculo;
                    oVehiculoSeguroBus.VehiculoSeguroAdd(oVehiculoS);

                }
                else
                {
                    oVehiculoSeguroBus.VehiculoSeguroUpdate(oVehiculoS);
                }
                if (oVehiculoVencimiento.IdVencimiento == 0)
                {
                    oVehiculoVencimiento.IdVehiculo = oVehiculo.IdVehiculo;
                    oVehiculoVencimientoBus.VehiculoVencimientoAdd(oVehiculoVencimiento);
                }
                else
                {
                    oVehiculoVencimientoBus.VehiculoVencimientoUpdate(oVehiculoVencimiento);
                }
                oVehiculoBus.VehiculoUpdate(oVehiculo);
            }
            //El Vehiculo antes no existia
            else
            {
                _idVehiculo = oVehiculoBus.VehiculoAdd(oVehiculo);
                oVehiculoS.IdVehiculo = _idVehiculo;
                oVehiculoSeguroBus.VehiculoSeguroAdd(oVehiculoS);
                oVehiculoVencimiento.IdVehiculo = _idVehiculo;
                oVehiculoVencimientoBus.VehiculoVencimientoAdd(oVehiculoVencimiento);

            }
            //Si tiene Tacografo, debemos guardar todos los datos del mismo o actualizarlo
            if (txtNTacografo.Text != "")
            {
                tieneTacografo = true;
            }
            //if (tieneTacografo)
            //{
            if (_Modificando)
                oVehiculoIBus.VehiculoIdentificacionUpdate(oVehiculoI);
            else
            {
                oVehiculoI.IdVehiculo = _idVehiculo;
                oVehiculoIBus.VehiculoIdentificacionAdd(oVehiculoI);
            }
            //}

        }
        private void guardaIndividual(string nombreArchivo, ref List<VehiculoFiles> archivos, string ruta, FileUpload control)
        {
            VehiculoFiles nuevo = new VehiculoFiles();
            string extension = control.FileName.ToString();
            extension = extension.Substring(extension.IndexOf('.'));
            string fecha = DateTime.Today.ToString("dd/MM/yyyy");
            fecha = fecha.Replace('/', '-');
            nuevo.IdVehiculo = _idVehiculo;
            nuevo.Nombre = nombreArchivo + "_" + fecha + extension;
            //nuevo.Ruta = Server.MapPath("~/Adjuntos/Vehiculo/" + "Vehiculo_" + _idVehiculo + "/" + nuevo.Nombre);
            nuevo.Ruta = ruta + (nuevo.Nombre);
            archivos.Add(nuevo);
            control.SaveAs(ruta + nuevo.Nombre);
        }
        private void GuardarArchivos()
        {
            List<VehiculoFiles> archivos = new List<VehiculoFiles>();
            string ruta = Server.MapPath(@"~/Adjuntos/Vehiculo/" + "Vehiculo_" + _idVehiculo.ToString() + "/");
            if (!Directory.Exists(ruta))
            {
                DirectoryInfo di = Directory.CreateDirectory(ruta);
            }

            if (fuExp.HasFile)
            {
                guardaIndividual("Expediente", ref archivos, ruta, fuExp);
            }
            if (fuNPoliza.HasFile)
            {
                guardaIndividual("NumeroPoliza", ref archivos, ruta, fuNPoliza);
            }
            if (fuPolizaHasta.HasFile)
            {
                guardaIndividual("PolizaHasta", ref archivos, ruta, fuPolizaHasta);
            }
            if (fuVCedulaVerde.HasFile)
            {
                guardaIndividual("VencimientoCedulaVerde", ref archivos, ruta, fuVCedulaVerde);
            }
            if (fuVencimientoLeasing.HasFile)
            {
                guardaIndividual("VencimientoLeasing", ref archivos, ruta, fuVencimientoLeasing);
            }
            if (fuVencimientoPatente.HasFile)
            {
                guardaIndividual("VencimientoPatente", ref archivos, ruta, fuVencimientoPatente);
            }
            if (fuVVTV.HasFile)
            {
                guardaIndividual("VencimientoVTV", ref archivos, ruta, fuVVTV);
            }

            foreach (VehiculoFiles vf in archivos)
            {
                VehiculoFilesBus oVFBus = new VehiculoFilesBus();
                oVFBus.VehiculoFilesAdd(vf);
            }
        }


        private void CargarVehiculosFiles()
        {
            VehiculoFilesBus oVehiculosFilesBus = new VehiculoFilesBus();
            gvVechiculosFiles.DataSource = oVehiculosFilesBus.VehiculoFilesGetByIdVehiculo(_idVehiculo);
            gvVechiculosFiles.DataBind();
            Mensaje.mostrarPopUp(this);
        }


        private void CargarMarcas()
        {
            MarcaBus oMarcaBus = new MarcaBus();
            ddlMarca.DataSource = oMarcaBus.MarcaGetAll();
            ddlMarca.DataTextField = "Descripcion";
            ddlMarca.DataValueField = "IdMarca";
            ddlMarca.DataBind();
            ddlMarca.Items.Insert(0, new ListItem("Seleccione Marca", "0"));
        }


        private void Eliminar(int idVehiculoFiles)
        {
            VehiculoFilesBus oVehiculoFilesBus = new VehiculoFilesBus();
            VehiculoFiles oVehiculoFiles = oVehiculoFilesBus.VehiculoFilesGetById(idVehiculoFiles);
            //System.IO.File.Delete(oPersonalFiles.Ruta); //Activar esta linea para permitir el borrado fisico de los archivos
            oVehiculoFilesBus.VehiculoFilesDelete(idVehiculoFiles);

        }

        private void comprobarFechas()
        {
            //Corroboramos que si esta marcado algun radio tenga el vencimiento correspondiente

            //Por defecto es valido
            hfFechasValidas.Value = "true";
            //Consultamos uno por uno si ambos datos fueron cargados
            if ((raSi.Checked) && (txtFechaAlta.Text == ""))
                hfFechasValidas.Value = "false";
            //if ((rbSi.Checked) && (txtFechaBaja.Text == ""))
            //    hfFechasValidas.Value = "false";
            if ((rtSi.Checked) && (txtNTacografo.Text == ""))
                hfFechasValidas.Value = "false";
            if ((rcdSi.Checked) && (txtVCedulaVerde.Text == ""))
                hfFechasValidas.Value = "false";
            if ((rpSi.Checked) && (txtPatente.Text == ""))
                hfFechasValidas.Value = "false";
            if ((rlSi.Checked) && (txtVencimientoLeasing.Text == ""))
                hfFechasValidas.Value = "false";
            if ((txtVTV.Text != "") && (txtVVTV.Text == ""))
                hfFechasValidas.Value = "false";
            if ((rrSi.Checked) && (txtExp.Text == "") && (txtEmision.Text == "") && (txtVEmision.Text == ""))
                hfFechasValidas.Value = "false";
            if ((rasSi.Checked) && (txtNPoliza.Text == "") && (txtPolizaDesde.Text == "") && (txtPolizaHasta.Text == ""))
                hfFechasValidas.Value = "false";
            
         

        }


        [WebMethod]
        public static CascadingDropDownNameValue[] GetEmpresa(string knownCategoryValues, string category)
        {
            EmpresaBus oEmpresaBus = new EmpresaBus();
            List<CascadingDropDownNameValue> empresa = oEmpresaBus.GetDataEmpresaInteras();
            return empresa.ToArray();
        }

        [WebMethod]
        public static CascadingDropDownNameValue[] GetSector(string knownCategoryValues, string category)
        {
            string idEmpresa = CascadingDropDown.ParseKnownCategoryValuesString(knownCategoryValues)["idEmpresa"];
            SectorBus oSectorBus = new SectorBus();
            List<CascadingDropDownNameValue> sectores = oSectorBus.GetDataSector(int.Parse(idEmpresa));
            return sectores.ToArray();
        }

        #endregion



    }
}