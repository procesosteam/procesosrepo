﻿<%@ Page Title="" Language="C#" MasterPageFile="~/frmPrincipal.Master" AutoEventWireup="true" CodeBehind="frmVehiculoHistorial.aspx.cs" Inherits="ProcesoPP.UI.Vehiculo.frmVehiculoHistorial" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<script src="../Js/jquery.maskedinput.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () { SetMenu("liVehiculos"); });


        jQuery(function ($) {
            $("#<%=txtVtoPatente.ClientID%>").mask("99/99/9999", { placeholder: "dd/mm/yyyy" });
        });
        jQuery(function ($) {
            $("#<%=txtVCedula.ClientID%>").mask("99/99/9999", { placeholder: "dd/mm/yyyy" });
        });
        jQuery(function ($) {
            $("#<%=txtPatente.ClientID%>").mask("aaa-999");
        });
    </script>
</asp:Content>
<asp:Content ID="cphContenedor" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div id="contenido" runat="server">
        <div id="apertura">
            Historial de Vehiculos.</div>
        <div class="solicitud100">
            <div class="sol_tit50">
                <label>
                    Patente</label></div>
            <div class="sol_campo30">
                <asp:TextBox runat="server" ID="txtPatente" /></div>
            <div class="sol_fecha">
            </div>
            <div class="sol_fecha">
            </div>
            <div class="sol_tit50">
                <label>
                    VTV</label></div>
            <div class="sol_campo50">
                <asp:TextBox runat="server" ID="txtVTV" /></div>
        </div>
        <div class="solicitud100">
            <div class="sol_tit50">
                <label>
                    Ruta</label></div>
            <div class="sol_campo30">
                <asp:TextBox runat="server" CssClass="sol_campo50c" ID="txtRuta" />
            </div>
            <div class="sol_fecha">
            </div>
            <div class="sol_fecha">
            </div>
            <div class="sol_tit50">
                <label>
                    Vto Patente</label></div>
            <div class="sol_campo50">
                <asp:TextBox runat="server" CssClass="sol_campo50c" ID="txtVtoPatente" />
            </div>
        </div>
        <div class="solicitud100">
            <div class="sol_tit50">
                <label>
                    Vto Cedula Verde</label></div>
            <div class="sol_campo30">
                <asp:TextBox runat="server" CssClass="sol_campo25" ID="txtVCedula" /></div>
            <div class="sol_fecha">
            </div>
            <div class="sol_fecha">
            </div>
            <div class="sol_tit50">
                <label>
                    Codificacion</label></div>
            <div class="sol_campo30">
                <asp:DropDownList runat="server" CssClass="sol_campo50c" ID="ddlCodificacion" />
            </div>
        </div>
        <div class="solicitud100">
            <div class="sol_tit50">
                <label>
                    Empresa</label></div>
            <div class="sol_campo30">
                <asp:DropDownList runat="server" CssClass="sol_campo50c" ID="ddlEmpresa" />
            </div>
        </div>
    </div>
    <div class="solicitud100sin">
        <div class="sol_btn">
            <asp:Button runat="server" ID="btnBuscar" CssClass="btn_form" Text="Buscar" OnClick="btnBuscar_Click" />
        </div>
    </div>
    <asp:GridView ID="gvVehiculo" runat="server" AutoGenerateColumns="False" DataMember="idVehiculo"
        CssClass="tabla3" HeaderStyle-CssClass="celda1" OnRowCommand="gvVehiculo_RowCommand"
        OnRowDataBound="gvVehiculo_RowDataBound" AllowPaging="true" PageSize="20" OnPageIndexChanging="gvVehiculo_PageIndexChanging"
        PagerStyle-CssClass="tabla_font">
        <Columns>
            <asp:TemplateField HeaderText="Patente" HeaderStyle-CssClass="columna6" ItemStyle-CssClass="columna6">
                <ItemTemplate>
                    <asp:Label ID="lblPatente" runat="server" Text='<%# Bind("Patente") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Empresa" HeaderStyle-CssClass="columna9"
                ItemStyle-CssClass="columna9">
                <ItemTemplate>
                    <asp:Label ID="lblEmpresa" runat="server" Text='<%# Bind("Empresa") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Sector" HeaderStyle-CssClass="columna9"
                ItemStyle-CssClass="columna9">
                <ItemTemplate>
                    <asp:Label ID="lblSector" runat="server" Text='<%# Bind("Sector") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Marca" HeaderStyle-CssClass="columna5" ItemStyle-CssClass="columna5">
                <ItemTemplate>
                    <asp:Label ID="lblMarca" runat="server" Text='<%# Bind("Marca") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Fecha Baja" HeaderStyle-CssClass="columna5"
                ItemStyle-CssClass="columna5">
                <ItemTemplate>
                    <asp:Label ID="lblFechaBaja" runat="server" Text='<%# Bind("FechaBaja") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField ItemStyle-CssClass="btn_iconos" HeaderStyle-CssClass="btn_iconos">
                <ItemTemplate>
                    <asp:LinkButton ID="btnVer" runat="server" title="Ver" CssClass="ver" CommandArgument='<%# Bind("idVehiculo") %>'
                        CommandName="Ver"></asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
            <%--<asp:TemplateField ItemStyle-CssClass="btn_iconos" HeaderStyle-CssClass="btn_iconos">
                <ItemTemplate>
                    <asp:LinkButton ID="btnModificar" runat="server" title="Editar Personal" CssClass="editar"
                        CommandArgument='<%# Bind("idVehiculo") %>' CommandName="Modificar"></asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField ItemStyle-CssClass="btn_iconos" HeaderStyle-CssClass="btn_iconos">
                <ItemTemplate>
                    <asp:LinkButton ID="btnEliminar" runat="server" title="Eliminar Personal" CssClass="eliminar"
                        CommandArgument='<%# Bind("idVehiculo") %>' CommandName="Eliminar" OnClientClick="return Eliminar('el Vehiculo');"></asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>--%>
        </Columns>
    </asp:GridView>
</asp:Content>
