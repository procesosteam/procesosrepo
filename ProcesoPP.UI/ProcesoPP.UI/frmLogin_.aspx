﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="frmLogin.aspx.cs" Inherits="ProcesoPP.UI.frmLogin" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>LOGIN</title>    
    <link href="Css/estilos.css" rel="stylesheet" type="text/css"/>
    <link href="Css/messi.min.css" rel="stylesheet" type="text/css" />
    <link href="Css/messi.css" rel="stylesheet" type="text/css" />
    
    <script src="Js/jquery-1.10.1.min.js" type="text/javascript"></script>
    <script src="Js/messi.min.js" type="text/javascript"></script>
    <script src="Js/messi.js" type="text/javascript"></script>
    <script src="Js/Validadores.js" type="text/javascript"></script>
    <script src="Js/Funciones.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">    
     <div id="contenido">
        <div id="apertura">INICIO DE SESION</div>     
        <div class="label_relleno20"></div>        
        <div class="label_admint">
            <label>Usuario:</label>
        </div>
        <div class="label_adminc">
            <asp:TextBox runat="server" CssClass="sol_campoadm" ID="txtNombre"></asp:TextBox></div>
        <div class="label_relleno20"></div>
        <div class="label_relleno20"></div>        
        <div class="label_admint">
            <label>Contraseña:</label>
        </div>
        <div class="label_adminc">
            <asp:TextBox runat="server" CssClass="sol_campoadm" ID="txtClave" TextMode="Password"></asp:TextBox></div>
        <div class="label_relleno20"></div> 
        <div class="label_relleno20"></div>
        <div class="label_adminc">
            <asp:LinkButton runat="server" CssClass="sol_campoadm" ID="lnkContra" Text="¿Olvido la contraseña?" PostBackUrl="~/Administracion/frmCambioContrasenia.aspx"/></div>        
        <div class="label_adminc">
             <asp:Button runat="server" ID="btnIniciar" CssClass="btn_form" Text="Iniciar Sesión" OnClientClick="return ValidarObj('txtNombre|Debe ingresar el Nombre de Usuario.~txtClave|Debe ingresar la Clave.');"
                OnClick="btnGuardar_Click" /></div>
    </div>
    </form>
</body>
</html>
