﻿<%@ Page Title="PP - Inicio" Language="C#" MasterPageFile="~/frmPrincipal.Master" AutoEventWireup="true"
    CodeBehind="frmPrincipal.aspx.cs" Inherits="ProcesoPP.UI.frmPrincipal1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function show(id) {
            
            if (id!="")
                $("#" + id).show();
            else
                $("#" + id).hide();
        }

        function sethide(obj) {
            $("#"+obj.ID).hide();
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="contenido" runat="server" style="height: 1000px; width: 100%;" >
        <div class="solicitud100tit" onclick="return show('divSinOTI');">
            <div class="princ50">
                <label>
                    Cant. SS sin OTI:
                </label>
            </div>
            <div class="princ20">
                <asp:Label runat="server" ID="lblCant"></asp:Label><div class="princrelle">
                </div>
            </div>
        </div>
        <div style="height: 35%; width: 100%;" id="divSinOTI" onload="sethide(this);">
            <div class="tablaprinc">
                <table class="celda1">
                    <tr>
                        <td class="columna1">
                            Nº SS
                        </td>
                        <td class="columna7">
                            Fecha
                        </td>
                        <td class="columna5">
                            Empresa
                        </td>
                        <td class="columna6">
                            Sector
                        </td>
                        <td class="columna6">
                            Servicio
                        </td>
                        <td class="columna2">
                            Origen
                        </td>
                        <td class="columna1">
                            Pozo
                        </td>
                        <td class="columna2">
                            Destino
                        </td>
                        <td class="columna1">
                            Pozo
                        </td>
                        <td class="columna1">
                            Usuario
                        </td>
                    </tr>
                </table>
            </div>
            <div style="height: 80%; width: 100%; overflow-y: scroll; float: left;">
                <asp:GridView ID="gvSolicitud" runat="server" AutoGenerateColumns="False" ShowHeader="false"
                    DataMember="idSolicitud" CssClass="tablaprinc" HeaderStyle-CssClass="celda1"
                    OnRowCommand="gvSolicitud_RowCommand">
                    <Columns>
                        <asp:TemplateField HeaderText="Nº SS" HeaderStyle-CssClass="columna1" ItemStyle-CssClass="columna1">
                            <ItemTemplate>
                                <asp:Label ID="lblNroSolicitud" runat="server" Text='<%# Bind("NroSolicitud") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Fecha" HeaderStyle-CssClass="columna7" ItemStyle-CssClass="columna7">
                            <ItemTemplate>
                                <asp:Label ID="lblFecha" runat="server" Text='<%# Bind("Fecha") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Empresa" HeaderStyle-CssClass="columna5" ItemStyle-CssClass="columna5">
                            <ItemTemplate>
                                <asp:Label ID="lblSolicitante" runat="server" Text='<%# Bind("Empresa") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Sector" HeaderStyle-CssClass="columna6" ItemStyle-CssClass="columna6">
                            <ItemTemplate>
                                <asp:Label ID="lblEmpresa" runat="server" Text='<%# Bind("Sector") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Servicio" HeaderStyle-CssClass="columna6" ItemStyle-CssClass="columna6">
                            <ItemTemplate>
                                <asp:Label ID="lblTipoServ" runat="server" Text='<%# Bind("TipoServicio") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Origen" HeaderStyle-CssClass="columna2" ItemStyle-CssClass="columna2">
                            <ItemTemplate>
                                <asp:Label ID="lblOrigen" runat="server" Text='<%# Bind("Origen") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Pozo" HeaderStyle-CssClass="columna1" ItemStyle-CssClass="columna1">
                            <ItemTemplate>
                                <asp:Label ID="lblPozoOrigen" runat="server" Text='<%# Bind("PozoOrigen") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Destino" HeaderStyle-CssClass="columna2" ItemStyle-CssClass="columna2">
                            <ItemTemplate>
                                <asp:Label ID="lblDestino" runat="server" Text='<%# Bind("Destino") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Pozo" HeaderStyle-CssClass="columna1" ItemStyle-CssClass="columna1">
                            <ItemTemplate>
                                <asp:Label ID="lblPozoDestino" runat="server" Text='<%# Bind("PozoDestino") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Usuario" HeaderStyle-CssClass="columna1" ItemStyle-CssClass="columna1">
                            <ItemTemplate>
                                <asp:Label ID="lblUsuario" runat="server" Text='<%# Bind("Usuario") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-CssClass="btn_iconos">
                            <ItemTemplate>
                                <asp:LinkButton ID="btnOT" runat="server" title="OTI" CssClass="oti" CommandArgument='<%# Bind("idSolicitud") %>'
                                    CommandName="OTI"></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>
        </div>
        <div style="height: 40%; width: 100%;">
            <div class="solicitud100tit">
                ALERTA - Equipo en campo mas de 15 días</div>
            <div style="height: 90%; width: 100%; overflow-y: scroll; float: left;">
                <asp:GridView ID="gvAlerta" runat="server" AutoGenerateColumns="False" DataMember="idSolicitud"
                    CssClass="tablaprinc" HeaderStyle-CssClass="celda1" OnRowCommand="gvSolicitud_RowCommand">
                    <Columns>
                        <asp:TemplateField HeaderText="Nº SS" HeaderStyle-CssClass="columna1" ItemStyle-CssClass="columna1">
                            <ItemTemplate>
                                <asp:Label ID="lblNroSolicitud" runat="server" Text='<%# Bind("NSolicitud") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Fecha" HeaderStyle-CssClass="columna7" ItemStyle-CssClass="columna7">
                            <ItemTemplate>
                                <asp:Label ID="lblFecha" runat="server" Text='<%# Bind("Fecha") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Empresa" HeaderStyle-CssClass="columna5" ItemStyle-CssClass="columna5">
                            <ItemTemplate>
                                <asp:Label ID="lblSolicitante" runat="server" Text='<%# Bind("Empresa") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Sector" HeaderStyle-CssClass="columna6" ItemStyle-CssClass="columna6">
                            <ItemTemplate>
                                <asp:Label ID="lblEmpresa" runat="server" Text='<%# Bind("Sector") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Servicio" HeaderStyle-CssClass="columna6" ItemStyle-CssClass="columna6">
                            <ItemTemplate>
                                <asp:Label ID="lblTipoServ" runat="server" Text='<%# Bind("TipoServicio") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="N° Equipo" HeaderStyle-CssClass="columna1" ItemStyle-CssClass="columna1">
                            <ItemTemplate>
                                <asp:Label ID="lblNEquipo" runat="server" Text='<%# Bind("NroEquipo") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Cant. Días" HeaderStyle-CssClass="columna1" ItemStyle-CssClass="columna1">
                            <ItemTemplate>
                                <asp:Label ID="lblCantDias" runat="server" Text='<%# Bind("CantDias") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Origen" HeaderStyle-CssClass="columna2" ItemStyle-CssClass="columna2">
                            <ItemTemplate>
                                <asp:Label ID="lblOrigen" runat="server" Text='<%# Bind("Origen") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Destino" HeaderStyle-CssClass="columna2" ItemStyle-CssClass="columna2">
                            <ItemTemplate>
                                <asp:Label ID="lblDestino" runat="server" Text='<%# Bind("Destino") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-CssClass="btn_iconos">
                            <ItemTemplate>
                                <asp:LinkButton ID="btnAlerta" runat="server" title="ALERTA" CssClass="alerta" CommandArgument='<%# Bind("idSolicitud") %>'
                                    CommandName="Alerta"></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>
        </div>
        <div style="height: 35%; width: 100%;" runat="server" id="divAlertaAcuerdo">
            <div class="solicitud100tit">
                <label>
                    Alerta Vencimiento Contratos:
                </label>
            </div>
            <div style="height: 90%; width: 100%; overflow-y: scroll; float: left;">
                <asp:GridView ID="gvAcuerdos" runat="server" AutoGenerateColumns="False" DataMember="idAcuerdo"
                    CssClass="tablaprinc" HeaderStyle-CssClass="celda1" OnRowCommand="gvSolicitud_RowCommand">
                    <Columns>
                        <asp:TemplateField HeaderText="Nº Acuerdo" HeaderStyle-CssClass="columna2" ItemStyle-CssClass="columna2">
                            <ItemTemplate>
                                <asp:Label ID="lblNroAcuerdo" runat="server" Text='<%# Bind("NroAcuerdo") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="N° Rev." HeaderStyle-CssClass="columna2" ItemStyle-CssClass="columna2">
                            <ItemTemplate>
                                <asp:Label ID="lblNroRevision" runat="server" Text='<%# Bind("NroRevision") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Cliente" HeaderStyle-CssClass="columna5" ItemStyle-CssClass="columna5">
                            <ItemTemplate>
                                <asp:Label ID="lblCliente" runat="server" Text='<%# Bind("Cliente") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Fecha Inicio" HeaderStyle-CssClass="columna6" ItemStyle-CssClass="columna6">
                            <ItemTemplate>
                                <asp:Label ID="lblFecha" runat="server" Text='<%# Bind("FechaInicio") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Fecha Fin" HeaderStyle-CssClass="columna6" ItemStyle-CssClass="columna6">
                            <ItemTemplate>
                                <asp:Label ID="lblFechaFin" runat="server" Text='<%# Bind("FechaFin") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Fecha Emisión" HeaderStyle-CssClass="columna6" ItemStyle-CssClass="columna6">
                            <ItemTemplate>
                                <asp:Label ID="lblFechaEm" runat="server" Text='<%# Bind("fechaEmision") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-CssClass="btn_iconos">
                            <ItemTemplate>
                                <asp:LinkButton ID="btnRenovar" runat="server" title="Renovar Contrato" CssClass="regresar"
                                    CommandArgument='<%# Bind("idAcuerdo") %>' CommandName="Renovar"></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>
        </div>
    </div>
</asp:Content>
