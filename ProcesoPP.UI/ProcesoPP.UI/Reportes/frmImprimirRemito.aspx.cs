﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ProcesoPP.Business;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;
using ProcesoPP.Model;
using System.Drawing.Printing;
using System.Data;
using ProcesoPP.Services;

namespace ProcesoPP.UI.Reportes
{
    public partial class frmImprimirRemito : System.Web.UI.Page
    {
        #region PROPIEDADES
        public int _idRemito
        {
            get { return (int)ViewState["idRemito"]; }
            set { ViewState["idRemito"] = value; }
        }

        private DataTable _dt
        {
            get { return (DataTable)ViewState["dt"]; }
            set { ViewState["dt"] = value; }
        }

        #endregion

        #region EVENTOS
        
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                InitScreen();
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error:  " + ex.Message, this.Page, "ERROR", null);
            }
        }

        public override void VerifyRenderingInServerForm(Control control)
        {
        }


        protected void gvDetalles_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                TextBox txtDetalle = e.Row.FindControl("txtDetalle") as TextBox;
                txtDetalle.Rows = txtDetalle.Text.Split('\n').Count();
            }
        }

        #endregion
        
        #region METODOS

        private void InitScreen()
        {
            _idRemito = 0;
            if (Request["idRemito"] != null)
                _idRemito = int.Parse(Request["idRemito"]);

            updateRemito();
          //  CrearPDFImprimir();
            CargarRemitoById();
            updateRemito();
        }

        private void CargarRemitoById()
        {
            CrearDT();

            Model.Remitos oRemito = new ProcesoPP.Model.Remitos();
            RemitosBus oRemitosBus = new RemitosBus();
            oRemito = oRemitosBus.RemitosGetById(_idRemito);
            lblFecha.Text = oRemito.Fecha.ToString("dd/MM/yyyy");
            txtObservacion.Text = oRemito.Observacion;
            lblNroRemito.Text = oRemito.NroRemito.ToString();
            foreach (RemitoDetalle oDetalle in oRemito.oListDetalles)
            {
                DataRow dr = _dt.NewRow();
                dr["idRemitoDetalle"] = oDetalle.IdRemitoDetalle;
                dr["Cantidad"] = oDetalle.Cantidad;
                dr["detalle"] = oDetalle.Detalle;
                dr["PU"] = oDetalle.PrecioUnit;
                dr["Total"] = oDetalle.PrecioTotal;
                _dt.Rows.Add(dr);
            }

            gvDetalles.DataSource = _dt;
            gvDetalles.DataBind();
        }

        private void CrearDT()
        {
            _dt = new DataTable();
            _dt.Columns.Add("idRemitoDetalle");
            _dt.Columns.Add("Cantidad");
            _dt.Columns.Add("Detalle");
            _dt.Columns.Add("PU");
            _dt.Columns.Add("Total");
        }


        private void CrearPDFImprimir()
        {
            string sPathApp = System.AppDomain.CurrentDomain.BaseDirectory.ToString();
            string fileName = System.IO.Path.Combine(Server.MapPath("~"), "Reportes\\remito.pdf");
            string copia = "Remito_" + DateTime.Now.ToString("yyyyMMdd_HHmmss") + ".pdf";

            MemoryStream memoryStream = new MemoryStream();

            Merge(copia, fileName);
        }

        public void Merge(string strFileTarget, string arrStrFilesSource)
        {
            try
            {
                MemoryStream stmFile = new MemoryStream();

                Document objDocument = null;
                PdfWriter objWriter = null;
                
                PdfReader objReader = new PdfReader(arrStrFilesSource);
                int intNumberOfPages = objReader.NumberOfPages;

                objDocument = new Document(objReader.GetPageSizeWithRotation(1));
                objWriter = PdfWriter.GetInstance(objDocument,
                            new FileStream(Request.PhysicalApplicationPath + "~1.pdf", FileMode.Create));//PdfWriter.GetInstance(objDocument, stmFile);
                objDocument.Open();

                PdfAction jAction = PdfAction.JavaScript("this.print(true);\r", objWriter);
                objWriter.AddJavaScript(jAction);

                for (int intPage = 0; intPage < intNumberOfPages; intPage++)
                {
                    int intRotation = objReader.GetPageRotation(intPage + 1);
                    PdfImportedPage objPage = objWriter.GetImportedPage(objReader, intPage + 1);

                    // Asigna el tamaño de la página
                    objDocument.SetPageSize(objReader.GetPageSizeWithRotation(intPage + 1));
                    // Crea una nueva página
                    objDocument.NewPage();
                    // Añade la página leída
                    if (intRotation == 90 || intRotation == 270)
                        objWriter.DirectContent.AddTemplate(objPage, 0, -1f, 1f, 0, 0,
                                                            objReader.GetPageSizeWithRotation(intPage + 1).Height);
                    else
                        objWriter.DirectContent.AddTemplate(objPage, 1f, 0, 0, 1f, 0, 0);
                    agregarCampos(objWriter);
                }
                if (objDocument != null)
                    objDocument.Close();

                //frame1.Attributes.Add("src", Request.PhysicalApplicationPath + "~1.pdf");
               // frame1.Attributes["src"] = Request.PhysicalApplicationPath + "~1.pdf";

                //Response.ContentType = "application/pdf";
                //Response.AddHeader("content-disposition", "attachment;filename=" + strFileTarget);
                //Response.Buffer = true;
                //Response.Clear();
                //Response.OutputStream.Write(stmFile.GetBuffer(), 0, stmFile.GetBuffer().Length);
                //Response.OutputStream.Flush();
                //Response.End();

            }
            catch (Exception objException)
            {
                System.Diagnostics.Debug.WriteLine(objException.Message);

            }
        }


        private void agregarCampos(PdfWriter objWriter)
        {
            RemitosBus oRemitosBus = new RemitosBus();
            Model.Remitos oRemito = new ProcesoPP.Model.Remitos();
            oRemito = oRemitosBus.RemitosGetById(_idRemito);

            iTextSharp.text.pdf.BaseFont fuente = FontFactory.GetFont(FontFactory.HELVETICA, iTextSharp.text.Font.DEFAULTSIZE, iTextSharp.text.Font.NORMAL).BaseFont;

            PdfContentByte cb = objWriter.DirectContent;
            cb.BeginText();
            cb.SetFontAndSize(fuente, 12);
            //FECHA
            DateTime fechaOTI = oRemito.Fecha;
            cb.SetTextMatrix(420, 710);
            cb.ShowText(fechaOTI.Day.ToString());
            cb.SetTextMatrix(470, 710);
            cb.ShowText(fechaOTI.Month.ToString());
            cb.SetTextMatrix(500, 710);
            cb.ShowText(fechaOTI.Year.ToString());

            int y = 680;
            //CLIENTE
            cb.SetTextMatrix(140, y);
            cb.ShowText(oRemito.oParte.oOrdenTrabajo.oSolicitudCliente.oSector.oEmpresa.Descripcion);            
            //DOMICILIO
            y = y - 20;
            cb.SetTextMatrix(140, y);
            cb.ShowText(oRemito.oParte.oOrdenTrabajo.oSolicitudCliente.oSector.oEmpresa.Domicilio);

            //IVA
            y = y - 25;
            string imageFilePath = System.IO.Path.Combine(Server.MapPath("~"), "img");
            //imageFilePath = System.IO.Path.Combine(imageFilePath, "check.png");            
            //iTextSharp.text.Image jpg = iTextSharp.text.Image.GetInstance(imageFilePath);
            //jpg.ScaleToFit(15, 15);
            //jpg.Alignment = iTextSharp.text.Image.UNDERLYING;
            
            string condIva = oRemito.oParte.oOrdenTrabajo.oSolicitudCliente.oSector.oEmpresa.CondIVA;
            //if (condIva != string.Empty)
            //{
            //    if (condIva.Equals(Empresa.IdCondicionIVA.RI.ToString()))
            //    {
            //        jpg.SetAbsolutePosition(163, y);
            //        cb.AddImage(jpg);
            //    }
            //    else if (condIva.Equals(Empresa.IdCondicionIVA.Exento.ToString()))
            //    {
            //        jpg.SetAbsolutePosition(210, y);
            //        cb.AddImage(jpg);
            //    }
            //    else if (condIva.Equals(Empresa.IdCondicionIVA.NRI.ToString()))
            //    {
            //        jpg.SetAbsolutePosition(265, y);
            //        cb.AddImage(jpg);
            //    }
            //    else if (condIva.Equals(Empresa.IdCondicionIVA.CF.ToString()))
            //    {
            //        jpg.SetAbsolutePosition(187, y - 15);
            //        cb.AddImage(jpg);
            //    }
            //    else if (condIva.Equals(Empresa.IdCondicionIVA.Monotributista.ToString()))
            //    {
            //        jpg.SetAbsolutePosition(265, y - 20);
            //        cb.AddImage(jpg);
            //    }
            //}
            //CUIT
            y = y + 5;
            cb.SetTextMatrix(340, y);
            cb.ShowText(oRemito.oParte.oOrdenTrabajo.oSolicitudCliente.oSector.oEmpresa.CUIT);
             //FACTURA N°
            y = y - 20;
            cb.SetTextMatrix(300, y);
            cb.ShowText("");
           
            //DETALLES
            y = y - 35;
            foreach (RemitoDetalle item in oRemito.oListDetalles)
            {
                y = y - 20;
                cb.SetTextMatrix(150, y);
                cb.ShowText(item.Cantidad.ToString());

                cb.SetTextMatrix(400, y);
                cb.ShowText(item.PrecioUnit.ToString());

                cb.SetTextMatrix(500, y);
                cb.ShowText(item.PrecioTotal.ToString());    

                foreach (string det in item.Detalle.Split('\n'))
                {
                    cb.SetTextMatrix(200, y);
                    cb.ShowText(det);
                    y = y - 20;
                }            
            }
            //cb.SetTextMatrix(150, y);
            //cb.ShowText(lblTipoServicio);
            //y -= 20;
            ////EQUIPOS
            //cb.SetTextMatrix(145, y);
            //cb.ShowText(lblTrailer != string.Empty ? lblTrailer : ddlTrailer);
            ////ESP TECNICAS
            //y -= 20;
            //string items = lblEspecificacion;
            //cb.SetTextMatrix(190, y);
            //cb.ShowText(items);

            //y = y - 57;
            ////ORIGEN         
            //cb.SetTextMatrix(140, y);
            //cb.ShowText(lblLugarOrigen);
            ////POZO
            //cb.SetTextMatrix(418, y);
            //cb.ShowText(lblPozoOrigen);

            //y -= 20;
            ////DESTINO
            //cb.SetTextMatrix(140, y);
            //cb.ShowText(lblLugarDestino);
            ////POZO
            //cb.SetTextMatrix(418, y);
            //cb.ShowText(lblPozoDestino);

            //y -= 20;
            ////KMS RECORRIDOS
            //cb.SetTextMatrix(150, y);
            //cb.ShowText(lblKmsEstimados);

            //y -= 57;
            ////Vehículo
            //cb.SetTextMatrix(140, y);
            //cb.ShowText(ddlVehiculo);
            //y -= 20;
            ////Chofer I
            //cb.SetTextMatrix(140, y);
            //cb.ShowText(ddlChofer);
            //y -= 20;
            ////Chofer II
            //cb.SetTextMatrix(140, y);
            //cb.ShowText(ddlAcompanante);

            cb.EndText();

        }

        private void updateRemito()
        {
            RemitosBus oRemitosBus = new RemitosBus();
            Model.Remitos oRemito = new ProcesoPP.Model.Remitos();
            oRemito = oRemitosBus.RemitosGetById(_idRemito);
            oRemito.Impreso = true;
            oRemitosBus.RemitosUpdate(oRemito);
        }

        #endregion

    }
}
