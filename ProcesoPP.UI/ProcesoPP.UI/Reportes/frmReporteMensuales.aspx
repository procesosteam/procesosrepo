﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="frmReporteMensuales.aspx.cs" Title="PP- Reporte Alquileres Mensuales"
    MaintainScrollPositionOnPostback="true" MasterPageFile="~/frmPrincipal.Master"
    Inherits="ProcesoPP.UI.Reportes.frmReporteMensuales" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        $(document).ready(function () { SetMenu("liReportes"); }); 
</script>
</asp:Content>
<asp:Content ID="cphContenedor" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="contenido" runat="server">
        <div id="DivFiltro">
            <div id="apertura">Reporte Alquileres Mensuales</div>
            <div class="solicitud100">
                <div class="sol_tit50">
                    <label>N° Solicitud</label></div>
                <div class="sol_campo50">
                    <asp:TextBox CssClass="sol_campo25" runat="server" ID="txtNroSolicitud" onkeypress="return ValidarNumeroEntero(event,this);"
                        Text="0" /></div>
            </div>
            <div class="solicitud100">
                <div class="sol_tit50">
                    <label>Fecha Desde</label></div>
                <div class="sol_campo30">
                    <asp:TextBox CssClass="sol_campo25" runat="server" ID="txtFecha" /></div>
                <div class="sol_fecha">
                    <img id="btnFecha" alt="calendario" src="../img/calendario.gif" style="width: 20px;
                        height: 20px; cursor: pointer;" onmouseover="CambiarConfiguracionCalendario('txtFecha',this);" /></div>
                <div class="sol_fecha">
                </div>
                <div class="sol_tit50">
                    <label>Fecha Hasta</label></div>
                <div class="sol_campo30">
                    <asp:TextBox CssClass="sol_campo25" runat="server" ID="txtFechaHasta" /></div>
                <div class="sol_fecha">
                    <img id="btnFechaHasta" alt="calendario" src="../img/calendario.gif" style="width: 20px;
                        height: 20px; cursor: pointer;" onmouseover="CambiarConfiguracionCalendario('txtFechaHasta',this);" /></div>
            </div>
            <div class="solicitud100">
                <div class="sol_tit50">
                    <label>Empresa</label></div>
                <div class="sol_campo50">
                    <asp:DropDownList runat="server" ID="ddlEmpresa" AutoPostBack="true" OnSelectedIndexChanged="ddlEmpresa_SelectedIndexChanged" />
                </div>
                <div class="sol_fecha">
                </div>
                <div class="sol_tit50">
                    <label>Sector</label></div>
                <div class="sol_campo50">
                    <asp:DropDownList runat="server" ID="ddlSector" />
                </div>
            </div>
            <div class="solicitud100">
                <div class="sol_tit50">
                    <label>Tipo Servicio</label></div>
                <div class="sol_campo50">
                    <asp:DropDownList runat="server" ID="ddlTipoServicio" />
                </div>
                <div class="sol_fecha">
                </div>
                <div class="sol_tit50">
                    <label>N° Equipo</label></div>
                <div class="sol_campo50">
                    <asp:DropDownList runat="server" ID="ddlTrailer" />
                </div>
            </div>
            <div class="solicitud100">                
                <div class="sol_tit50">
                    <label>Remito</label></div>
                <div class="sol_campo50">
                    <asp:RadioButtonList ID="chkRemito" runat="server" RepeatColumns="3">
                        <asp:ListItem Value="1" Text="Si"></asp:ListItem>
                        <asp:ListItem Value="0" Text="No"></asp:ListItem>
                        <asp:ListItem Value=" " Text="Todos" Selected="True"></asp:ListItem>
                    </asp:RadioButtonList>
                </div>
            </div>
            <div class="solicitud100sin">
                <div class="sol_btn">
                    <asp:Button runat="server" ID="btnBuscar" CssClass="btn_form" Text="Buscar" OnClick="btnBuscar_Click" />
                </div>
                <div class="sol_btn">
                    <asp:Button runat="server" ID="btnExportar" CssClass="btn_form" Text="Exportar" OnClick="btnExportar_Click" />
                </div>
            </div>
            
        </div>
        <asp:GridView ID="gvMovimientos" runat="server" AutoGenerateColumns="true" DataMember="idCondicion"
            HeaderStyle-CssClass="tabla_font" RowStyle-CssClass="tabla_font2" CssClass="tabla3"
            AllowPaging="true" PageSize="20" OnPageIndexChanging="gvMovimientos_PageIndexChanging"
            PagerStyle-CssClass="tabla_font">
        </asp:GridView>
    </div>
</asp:Content>
