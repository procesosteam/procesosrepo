﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ProcesoPP.Business;
using ProcesoPP.Model;
using ProcesoPP.Services;

namespace ProcesoPP.UI.Reportes
{
    public partial class frmHistorial : System.Web.UI.Page
    {
        #region PROPIEDADES

        public Permisos _Permiso
        {
            get { return (Permisos)ViewState["permiso"]; }
            set { ViewState["permiso"] = value; }
        }

        public int _idUsuario
        {
            get { return (int)ViewState["idUsuario"]; }
            set { ViewState["idUsuario"] = value; }
        }

        #endregion

        #region < EVENTOS >

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                _idUsuario = Common.EvaluarSession(Request, this.Page);
                if (_idUsuario != 0)
                {
                    _Permiso = Common.getModulo(Request, this.Page);
                    if (_Permiso != null && _Permiso.Lectura)
                    {
                        if (!IsPostBack)
                        {
                            CargarEmpresa();
                            CargarEquipo();
                        }
                    }
                    else
                    {
                        Mensaje.errorMsj("NO TIENE PERMISOS A ESTE FORMULARIO", this.Page, "SIN ACCESO", "../frmPrincipal.aspx");
                    }
                }
                else
                {
                    Mensaje.errorMsj("DEBE LOGUEARSE", this.Page, "LOGIN", "../frmLogin.aspx");
                }
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                BuscarSS();
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        protected void gvOTI_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "OTI":
                    HiddenField hfIdOTI = ((DataControlFieldCell)((Control)(e.CommandSource)).Parent).Controls[1] as HiddenField;
                    string strUrl = "idSolicitud=" + Common.encriptarURL(e.CommandArgument.ToString()) +
                                     "&idOTI=" + Common.encriptarURL(hfIdOTI.Value) +
                                     "&idModulo=" + _Permiso.oModulo.idModulo.ToString() ;
                    Response.Redirect("../SSe3/frmSSOti.aspx?" + strUrl);
                    break;
                case "Ver":
                    Label lblIdOTI = ((GridViewRow)(((LinkButton)e.CommandSource).NamingContainer)).FindControl("lblidOrden") as Label;
                    Response.Redirect("../SSe3/frmSSOTI.aspx?idSolicitud=" + Common.encriptarURL(e.CommandArgument.ToString()) + "&idOti=" + Common.encriptarURL(lblIdOTI.Text) + "&idModulo=" + _Permiso.oModulo.idModulo + "&volver=frmHistorial.aspx&accion=" + e.CommandName);
                    break;
                case "Modificar":
                    Response.Redirect("../SSe3/frmSSNueva.aspx?idSolicitud=" + e.CommandArgument + "&idModulo=" + _Permiso.oModulo.idModulo);
                    break;
                case "Eliminar":
                    try
                    {
                        Eliminar(e.CommandArgument.ToString());
                    }
                    catch (Exception ex)
                    {
                        Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
                    }
                    break;
            }
        }

        protected void gvOTI_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Common.CargarRow(e, _Permiso);
                LinkButton btnOT = e.Row.FindControl("btnOT") as LinkButton;
                
                if (!_Permiso.Escritura)
                {
                    btnOT.Visible = false;
                }

            }
        }

        protected void gvOTI_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvOTI.PageIndex = e.NewPageIndex;
            BuscarSS();
        }

        #endregion

        #region < METODOS >
        
        private void Eliminar(string idSolicitud)
        {
            SolicitudCliente oSolicitudCliente = new SolicitudCliente();
            SolicitudClienteBus oSolicitudClienteBus = new SolicitudClienteBus();
            oSolicitudCliente = oSolicitudClienteBus.SolicitudClienteGetById(int.Parse(idSolicitud));
            oSolicitudCliente.Baja = true;
            oSolicitudClienteBus.SolicitudClienteUpdate(oSolicitudCliente);

            BuscarSS();
        }
        
        private void BuscarSS()
        {
            SolicitudClienteBus oSolicitudClienteBus = new SolicitudClienteBus();
           gvOTI.DataSource = oSolicitudClienteBus.SolicitudHistorialGetByFilter(txtNroSolicitud.Text == "" ? "0" : txtNroSolicitud.Text,
                                                                                  ddlEmpresa.SelectedValue,
                                                                                  txtFecha.Text,
                                                                                  txtFechaHasta.Text, 
                                                                                  ddlTrailer.SelectedValue,
                                                                                  txtNroParte.Text == "" ? "0" : txtNroParte.Text);

           gvOTI.DataBind();

        }
        
        private void CargarEquipo()
        {
            VehiculoBus oVehiculoBus = new VehiculoBus();
            oVehiculoBus.CargarComboEquipo(ref ddlTrailer, "Seleccione un equipo...");
        }

        private void CargarEmpresa()
        {
            EmpresaBus oEmpresaBus = new EmpresaBus();
            ddlEmpresa.DataSource = oEmpresaBus.EmpresaGetAll();
            ddlEmpresa.DataTextField = "Descripcion";
            ddlEmpresa.DataValueField = "idEmpresa";
            ddlEmpresa.DataBind();
            ddlEmpresa.Items.Insert(0, new ListItem("Seleccione...", "0"));
        }
        #endregion

    }
}
