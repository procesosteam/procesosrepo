﻿<%@ Page Language="C#" AutoEventWireup="true" MaintainScrollPositionOnPostback="true" MasterPageFile="~/frmPrincipal.Master"  CodeBehind="frmReporteMovimientos.aspx.cs" Inherits="ProcesoPP.UI.Reportes.frmReporteMovimientos" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server"></asp:Content>
<asp:Content ID="cphContenedor" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="contenido" runat="server">
<div id="DivFiltro">

<div id="apertura">Reporte Movimientos</div>  

<div class="solicitud100">
    <div class="sol_tit50"><label>Fecha Desde</label></div>
    <div class="sol_campo30"><asp:TextBox CssClass="sol_campo25" runat="server" id="txtFecha" /></div>
    <div class="sol_fecha"><img id="btnFecha" alt="calendario" src="../img/calendario.gif" style="width:20px; height:20px; cursor:pointer;" onmouseover="CambiarConfiguracionCalendario('txtFecha',this);"/></div>
    <div class="sol_tit2"></div>    
    <div class="sol_tit50"> <label>Fecha Hasta</label></div>
    <div class="sol_campo30"><asp:TextBox CssClass="sol_campo25" runat="server" id="txtFechaHasta" /></div>
    <div class="sol_fecha"><img id="btnFechaHasta" alt="calendario" src="../img/calendario.gif" style="width:20px; height:20px; cursor:pointer;" onmouseover="CambiarConfiguracionCalendario('txtFechaHasta',this);"/></div>
    <div class="sol_tit2"></div>
</div>
<div class="solicitud100">
    <div class="sol_tit50"> <label>Empresa</label></div>
    <div class="sol_campo50"><asp:DropDownList runat="server" id="ddlEmpresa" AutoPostBack="true"
            onselectedindexchanged="ddlEmpresa_SelectedIndexChanged" /></div>  
    <div class="sol_tit2"></div><div class="sol_fecha"></div>          
     <div class="sol_tit50"> <label>Sector</label></div>
    <div class="sol_campo50"><asp:DropDownList runat="server" id="ddlSector" /></div>    
</div>
<div class="solicitud100">
    <div class="sol_tit50"> <label>Condición</label></div>
    <div class="sol_campo50"><asp:DropDownList runat="server" id="ddlCondicion" /></div>  
    <div class="sol_tit2"></div><div class="sol_fecha"></div>          
     <div class="sol_tit50"> <label>N° Equipo</label></div>
    <div class="sol_campo50"><asp:DropDownList runat="server" id="ddlTrailer" /></div>    
</div>
<div class="solicitud100">
    <div class="sol_tit50"><label>Origen</label></div>
    <div class="sol_campo50"><asp:DropDownList runat="server" id="ddlOrigen"/></div>
    <div class="sol_tit2"></div><div class="sol_fecha"></div>
    <div class="sol_tit50">  <label>Destino</label></div>
    <div class="sol_campo50"><asp:DropDownList runat="server" id="ddlDestino"/></div>            
    <div class="sol_fecha"></div>
</div>
<div class="solicitud100">
    <div class="sol_tit50"><label>Chofer</label></div>
    <div class="sol_campo50"><asp:DropDownList runat="server" id="ddlChofer"/></div>
    <div class="sol_tit2"></div><div class="sol_fecha"></div>
    <div class="sol_tit50">  <label>Vehiculo</label></div>
    <div class="sol_campo50"><asp:DropDownList runat="server" id="ddlVehiculo"/></div>            
    <div class="sol_fecha"></div>
</div>

<div class="solicitud100">
    
    <div class="sol_btn">
        <asp:Button runat="server" id="btnBuscar" cssclass="btn_form"
              Text="Buscar" onclick="btnBuscar_Click" />
    </div>
    <div class="sol_btn">
        <asp:Button runat="server" id="btnExportar" cssclass="btn_form"
              Text="Exportar" onclick="btnExportar_Click" />
    </div>
</div>
</div>

<asp:GridView ID="gvMovimientos" runat="server" AutoGenerateColumns="true" 
    DataMember="idCondicion" CssClass="tabla3" >
</asp:GridView>
</div>
</asp:Content>