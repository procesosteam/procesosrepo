﻿<%@ Page Title="PP - Reporte de Servicios" Language="C#" AutoEventWireup="true" MaintainScrollPositionOnPostback="true" MasterPageFile="~/frmPrincipal.Master"  CodeBehind="frmReporteMovimientos.aspx.cs" Inherits="ProcesoPP.UI.Reportes.frmReporteMovimientos" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<script type="text/javascript" >
    $(document).ready(function() { SetMenu("liReportes"); }); 
</script>
</asp:Content>
<asp:Content ID="cphContenedor" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div id="contenido" runat="server">
<div id="DivFiltro">

<div id="apertura">Reporte de Servicios</div>  
<div class="solicitud100">
    <div class="sol_tit50"><label>N° Solicitud</label></div>
    <div class="sol_campo50"><asp:TextBox CssClass="sol_campo25" runat="server" id="txtNroSolicitud" onkeypress="return ValidarNumeroEntero(event,this);" Text="0"/></div>
 
</div>

<div class="solicitud100">
    <div class="sol_tit50"><label>Fecha Desde</label></div>
    <div class="sol_campo30"><asp:TextBox CssClass="sol_campo25" runat="server" id="txtFecha" /></div>
    <div class="sol_fecha"><img id="btnFecha" alt="calendario" src="../img/calendario.gif" style="width:20px; height:20px; cursor:pointer;" onmouseover="CambiarConfiguracionCalendario('txtFecha',this);"/></div>
    
   <div class="sol_fecha"></div>
   
    <div class="sol_tit50"> <label>Fecha Hasta</label></div>
    <div class="sol_campo30"><asp:TextBox CssClass="sol_campo25" runat="server" id="txtFechaHasta" /></div>
    <div class="sol_fecha"><img id="btnFechaHasta" alt="calendario" src="../img/calendario.gif" style="width:20px; height:20px; cursor:pointer;" onmouseover="CambiarConfiguracionCalendario('txtFechaHasta',this);"/></div>
</div>

<div class="solicitud100">
    <div class="sol_tit50"> <label>Empresa</label></div>
    <div class="sol_campo50"><asp:DropDownList runat="server" id="ddlEmpresa" AutoPostBack="true"
            onselectedindexchanged="ddlEmpresa_SelectedIndexChanged" /></div>  
  <div class="sol_fecha"></div>          
     <div class="sol_tit50"> <label>Sector</label></div>
    <div class="sol_campo50"><asp:DropDownList runat="server" id="ddlSector" /></div>    
</div>
<div class="solicitud100">
    <div class="sol_tit50"> <label>Condición</label></div>
    <div class="sol_campo50"><asp:DropDownList runat="server" id="ddlCondicion" /></div>  
   <div class="sol_fecha"></div>          
     <div class="sol_tit50"> <label>N° Equipo</label></div>
    <div class="sol_campo50"><asp:DropDownList runat="server" id="ddlTrailer" /></div>    
</div>
<div class="solicitud100">
    <div class="sol_tit50"><label>Tipo Servicio</label></div>
    <div class="sol_campo50"><asp:DropDownList runat="server" id="ddlTipoServicio"/></div>
<div class="sol_fecha"></div>
    <div class="sol_tit50"><label>Remito</label></div>         
    <div class="sol_campo50">
        <asp:RadioButtonList ID="chkRemito" runat="server" RepeatColumns="3">
            <asp:ListItem Value="1" Text="Si"></asp:ListItem>
            <asp:ListItem Value="0" Text="No"></asp:ListItem>
            <asp:ListItem Value=" " Text="Todos" Selected="True"></asp:ListItem>
        </asp:RadioButtonList></div>
</div>
<div class="solicitud100sin">    
    <div class="sol_btn">
        <asp:Button runat="server" id="btnBuscar" cssclass="btn_form"
              Text="Buscar" onclick="btnBuscar_Click" />
    </div>
    <div class="sol_btn">
        <asp:Button runat="server" id="btnExportar" cssclass="btn_form"
              Text="Exportar" onclick="btnExportar_Click" />
    </div>
</div>

 <div class="solicitud100tit"> Calculo de Cantidades:</div>

<div class="solicitud100es">    
    <div class="sol_tit50"><label>Conexiones: </label></div>
    <div class="sol_repo"><asp:Label runat="server" ID="lblConexiones" CssClass="boton40"></asp:Label></div>
    <div class="sol_tit50">
      <label>Desconexiones: </label></div>
    <div class="sol_repo"><asp:Label runat="server" ID="lblDesconexiones"  CssClass="boton40"></asp:Label></div>
  
    <div class="sol_tit50">
      <label>Días de Alquiler: </label></div>
    <div class="sol_repo"><asp:Label runat="server" ID="lblAlquiler" CssClass="boton40"></asp:Label></div>
    <div class="sol_tit50"><label>Kms.: </label></div>
    <div class="sol_repo"><asp:Label runat="server" ID="lblKms" CssClass="boton40"></asp:Label></div>
</div>
</div>

<asp:GridView ID="gvMovimientos" runat="server" AutoGenerateColumns="true" 
    DataMember="idCondicion" HeaderStyle-CssClass="tabla_font" 
        RowStyle-CssClass= "tabla_font2" CssClass="tabla3" 
        AllowPaging="true" PageSize="20" 
        onpageindexchanging="gvMovimientos_PageIndexChanging" PagerStyle-CssClass="tabla_font">
</asp:GridView>
</div>
</asp:Content>