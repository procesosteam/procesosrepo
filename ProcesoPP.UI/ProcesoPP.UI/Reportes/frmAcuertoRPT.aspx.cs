﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ProcesoPP.UI.Reportes
{
    public partial class frmAcuertoRPT : System.Web.UI.Page
    {

        public int _idAcuerdo
        {
            get { return (int)ViewState["idAcuerdo"]; }
            set { ViewState["idAcuerdo"] = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

            _idAcuerdo = 0;
            if (Request["idAcuerdo"] != null)
            {
                _idAcuerdo = int.Parse(Request["idAcuerdo"]);
                PrintAcuerdo();
            }
                       
        }

        protected void btnPrint_Click(object sender, EventArgs e)
        {
            if (_idAcuerdo > 0)
                PrintAcuerdo();
        }

        private void CargarImpresoras()
        {
            ddlPrinter.DataSource = System.Drawing.Printing.PrinterSettings.InstalledPrinters;
            ddlPrinter.DataBind();
        }

        private void PrintAcuerdo()
        {
            AcuerdoDS ds = new AcuerdoDS();
            
            AcuerdoDS.AcuerdoGetByIdDataTable table = ds.AcuerdoGetById;
            AcuerdoDSTableAdapters.AcuerdoGetByIdTableAdapter adapter = new ProcesoPP.UI.Reportes.AcuerdoDSTableAdapters.AcuerdoGetByIdTableAdapter();
            adapter.Fill(table, _idAcuerdo);

            AcuerdoDS.AcuerdoTituloGetByIdAcuerdoDataTable tableAt = ds.AcuerdoTituloGetByIdAcuerdo;
            AcuerdoDSTableAdapters.AcuerdoTituloGetByIdAcuerdoTableAdapter adapterAt = new ProcesoPP.UI.Reportes.AcuerdoDSTableAdapters.AcuerdoTituloGetByIdAcuerdoTableAdapter();
            adapterAt.Fill(tableAt, _idAcuerdo);

            AcuerdoDS.AcuerdoServicioGetByIdAcuerdoDataTable tableAS = ds.AcuerdoServicioGetByIdAcuerdo;
            AcuerdoDSTableAdapters.AcuerdoServicioGetByIdAcuerdoTableAdapter adapterAS = new ProcesoPP.UI.Reportes.AcuerdoDSTableAdapters.AcuerdoServicioGetByIdAcuerdoTableAdapter();
            adapterAS.Fill(tableAS, _idAcuerdo);

            AcuerdoDS.AcuerdoConceptoCostoGetByIdAcuerdoDataTable tableACC = ds.AcuerdoConceptoCostoGetByIdAcuerdo;
            AcuerdoDSTableAdapters.AcuerdoConceptoCostoGetByIdAcuerdoTableAdapter adapterACC = new ProcesoPP.UI.Reportes.AcuerdoDSTableAdapters.AcuerdoConceptoCostoGetByIdAcuerdoTableAdapter();
            adapterACC.Fill(tableACC, _idAcuerdo);
            
            Acuerdo oacuerdoRpt = new Acuerdo();
            
            oacuerdoRpt.Load(Server.MapPath("~/Reportes/Acuerdo.rpt"));
            oacuerdoRpt.SetDataSource(ds);            
            CrystalReportViewer1.ReportSource = oacuerdoRpt;

            Response.ContentType = "application/pdf";
            //Response.AddHeader("content-disposition", "attachment;filename=" + table.Rows[0]["Titulo"] + "_" + DateTime.Now);
            Response.Buffer = true;
            Response.Clear();

            oacuerdoRpt.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, true, "Carta Acuerdo");

            Response.End();

            //oacuerdoRpt.PrintOptions.PrinterName = ddlPrinter.SelectedItem.Text; //Asigno la impresora            
            //oacuerdoRpt.PrintToPrinter(1, false, 0, 0); //Imprimo 3 copias
            //ClientScript.RegisterStartupScript(typeof(Page), "closePage", "window.close();", true);


        }

    }
}
