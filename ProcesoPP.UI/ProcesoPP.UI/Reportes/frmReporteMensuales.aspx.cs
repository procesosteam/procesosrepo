﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ProcesoPP.Model;
using ProcesoPP.Services;
using ProcesoPP.Business;
using ParteDiario.Services;
using System.Data;

namespace ProcesoPP.UI.Reportes
{
    public partial class frmReporteMensuales : System.Web.UI.Page
    {
        #region PROPIEDADES

        public Permisos _Permiso
        {
            get { return (Permisos)ViewState["permiso"]; }
            set { ViewState["permiso"] = value; }
        }

        public int _idUsuario
        {
            get { return (int)ViewState["idUsuario"]; }
            set { ViewState["idUsuario"] = value; }
        }

        #endregion

        #region EVENTOS

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                _idUsuario = Common.EvaluarSession(Request, this.Page);
                if (_idUsuario != 0)
                {
                    _Permiso = Common.getModulo(Request, this.Page);
                    if (_Permiso != null && _Permiso.Lectura)
                    {
                        if (!IsPostBack)
                            InitScreen();
                    }
                    else
                    {
                        Mensaje.errorMsj("NO TIENE PERMISOS A ESTE FORMULARIO", this.Page, "SIN ACCESO", "../frmPrincipal.aspx");
                    }
                }
                else
                {
                    Mensaje.errorMsj("DEBE LOGUEARSE", this.Page, "LOGIN", "../frmLogin.aspx");
                }
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        protected void ddlEmpresa_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddlEmpresa.SelectedIndex > 0)
                {
                    CargarSector(ddlEmpresa.SelectedValue);
                }
                else
                {
                    ddlSector.Items.Clear();
                    ddlSector.Items.Insert(0, new ListItem("Seleccione...", "0"));
                }
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                CargarGrilla();
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        protected void btnExportar_Click(object sender, EventArgs e)
        {
            try
            {
                Exportar();
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        protected void gvMovimientos_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvMovimientos.PageIndex = e.NewPageIndex;
            CargarGrilla();
        }

        public override void VerifyRenderingInServerForm(Control control)
        {

        }

        #endregion

        #region METODOS

        private void Exportar()
        {

            SolicitudClienteBus oSolicitudClienteBus = new SolicitudClienteBus();
            DataTable dt = oSolicitudClienteBus.ReporteAlquileresMensuales(txtFecha.Text,
                                                                            txtFechaHasta.Text,
                                                                            ddlEmpresa.SelectedValue,
                                                                            ddlSector.SelectedValue,
                                                                            ddlTrailer.SelectedValue,
                                                                            txtNroSolicitud.Text == string.Empty ? "0" : txtNroSolicitud.Text,
                                                                            ddlTipoServicio.SelectedValue,
                                                                            chkRemito.SelectedValue);

            string path = System.IO.Path.Combine(Server.MapPath("~"), "img");
            ExportarExcel oExport = new ExportarExcel("ReporteMensuales_" + DateTime.Now.ToString("yyyyMMddHHmmss"), path, "REPORTE DE ALQUILERES MENSUALES");
            oExport.ExportarExcel2003(dt);
        }

        private void InitScreen()
        {
            
            CargarEmpresa();
            CargarTrailer();
            CargarTipoServicio();
            ddlSector.Items.Insert(0, new ListItem("Seleccione...", "0"));
        }

        private void CargarGrilla()
        {
            SolicitudClienteBus oSolicitudClienteBus = new SolicitudClienteBus();
            DataTable dt = oSolicitudClienteBus.ReporteAlquileresMensuales(txtFecha.Text,
                                                                            txtFechaHasta.Text,
                                                                            ddlEmpresa.SelectedValue,
                                                                            ddlSector.SelectedValue,
                                                                            ddlTrailer.SelectedValue,
                                                                            txtNroSolicitud.Text == string.Empty ? "0" : txtNroSolicitud.Text,
                                                                            ddlTipoServicio.SelectedValue,
                                                                            chkRemito.SelectedValue);
            gvMovimientos.DataSource = dt;
            gvMovimientos.DataBind();

            
        }

        private void CargarTipoServicio()
        {
            TipoServicioBus oTipoServicioBus = new TipoServicioBus();
            oTipoServicioBus.CargarCombo(ref ddlTipoServicio, "Seleccione...");
        }

        private void CargarTrailer()
        {
            VehiculoBus oVehiculoBus = new VehiculoBus();
            oVehiculoBus.CargarComboEquipo(ref ddlTrailer, "Seleccione...");
        }
                
        private void CargarSector(string idEmpresa)
        {
            ddlSector.Items.Clear();
            SectorBus oSectorBus = new SectorBus();
            oSectorBus.CargarSectorPorEmpresa(ref ddlSector, int.Parse(idEmpresa));
            ddlSector.Items.Insert(0, new ListItem("Seleccione...", "0"));
        }

        private void CargarEmpresa()
        {
            EmpresaBus oEmpresaBus = new EmpresaBus();
            oEmpresaBus.CargarEmpresa(ref ddlEmpresa, "Seleccione...");
        }

        
        #endregion

    }
}