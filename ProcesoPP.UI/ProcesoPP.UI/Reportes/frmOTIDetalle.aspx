﻿<%@ Page Title="" Language="C#" MasterPageFile="~/frmPrincipal.Master" AutoEventWireup="true" CodeBehind="frmOTIDetalle.aspx.cs" Inherits="ProcesoPP.UI.Reportes.frmOTIDetalle" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
 <div id="contenido" runat="server">
    <div id="apertura">OTI</div>  
    <div class="regreso_3">
       <label class="numeracion" >SS N°: </label>
       <asp:Label runat="server" id="lblNroSS" />
    </div>
     <div class="regreso_3">
        <label class="numeracion">Condición: </label>
        <asp:Label ID="lblCondicion" runat="server" />
    </div>  
    <div class="regreso_3">
       <label class="numeracion">Fecha: </label>
        <asp:Label  runat="server" id="lblFecha" />
     </div>
     <div class="regreso_2" style="visibility:hidden;">
        <label class="numeracion">N° Orden de Trabajo: </label>
        <asp:Label ID="lblNroOT" runat="server" />
    </div>
    <div id="titulos">Datos del Solicitante.</div>      
    <div class="regreso_3">
        <label class="numeracion">Empresa: </label>
        <asp:Label ID="lblEmpresa" runat="server"/>
    </div>
    <div class="regreso_3">
         <label class="numeracion">Sector: </label>
         <asp:Label ID="lblSector" runat="server"/>
    </div>  
    <div class="regreso_3">
        <label class="numeracion">Solicitante: </label>
        <asp:Label ID="txtNombre" runat="server" />
    </div>        
    <div style="visibility:hidden">
       <label class="numeracion">Nº Aviso: </label>
       <asp:Label ID="lblNroAviso" runat="server" />
    </div>        
    <div style="visibility:hidden">
        <label class="numeracion">Teléfono: </label>
        <asp:Label ID="lblTelefono" runat="server" />
    </div>

    <div id="titulos">Tipo de Servicio</div>
    
    <div class="regreso_3"> <label>Tipo de Servicio</label></div>
    <div class="regreso_2">
        <asp:Label runat="server" ID="lblTipoServicio"></asp:Label>
    </div>
    
    <div class="regreso_3"> <label>Equipos</label></div>
    <div class="regreso_2">
        <asp:Label runat="server" ID="lblTrailer"></asp:Label>
    </div>
     
    <div class="regreso_3"><label>Especificaciones Técnicas</label> </div>
    <div class="regreso_2"><asp:ListBox runat="server" ID="lblEspecificacion" Enabled="false"></asp:ListBox></div>
    
     <div id="titulos">Movimientos</div>
     <div class="regreso_3">
        <label class="numeracion">Origen</label>
        <asp:Label ID="lblLugarOrigen" runat="server" />
    </div>
    <div class="regreso_3">
        <label class="numeracion">Pozo Origen</label>
        <asp:Label ID="lblPozoOrigen" runat="server" />
    </div>
    
        <div class="regreso_3">
       <label class="numeracion">Km. Est:</label>
       <asp:Label id="lblKmsEstimados" runat="server"/>
    </div>
    <div class="regreso_3">
        <label class="numeracion">Destino</label>
        <asp:Label ID="lblLugarDestino" runat="server" />
    </div>
    <div class="regreso_3">
        <label class="numeracion">Pozo Destino</label>
        <asp:Label ID="lblPozoDestino" runat="server" />
    </div> <div class="regreso_3"></div>
    
    
    <div id="titulos">Equipo Asignado</div>
    <div class="regreso_3"><label>Vehículo:</label></div>
    <div class="regreso_2"><asp:Label runat="server" ID="lblVehiculo"> </asp:Label></div>
    <div class="regreso_3"><label>Chofer:</label></div>
    <div class="regreso_2"><asp:Label runat="server" ID="lblChofer"> </asp:Label></div>
    <div class="regreso_3"><label>Acompañante:</label></div>
    <div class="regreso_2"><asp:Label runat="server" ID="lblAcompanante"> </asp:Label></div>
    <div id="botones"> 
        <div class="regreso_3">
            <asp:Button runat="server" ID="btnImprimir" CssClass="btn_form" Text="IMPRIMIR" 
                onclick="btnImprimir_Click"/></div>
        <div class="regreso_3">
            <asp:Button ID="btnVolver" runat="server" Text="VOLVER" class="btn_form" onclick="btnVolver_Click"/>
        </div>       
    </div>
    
</div>   
</asp:Content>
