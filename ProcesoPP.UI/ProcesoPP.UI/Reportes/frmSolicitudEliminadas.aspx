﻿<%@ Page Title="" Language="C#" MasterPageFile="~/frmPrincipal.Master" AutoEventWireup="true" CodeBehind="frmSolicitudEliminadas.aspx.cs" Inherits="ProcesoPP.UI.Reportes.frmSolicitudEliminadas" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<script type="text/javascript" >
    $(document).ready(function() { SetMenu("liReportes"); }); 
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="contenido" runat="server">
<div id="DivFiltro">

<div id="apertura">Solicitudes Eliminadas</div>  

<div class="solicitud100">
    <div class="sol_tit50"><label>Fecha Desde</label></div>
    <div class="sol_campo30"><asp:TextBox CssClass="sol_campo25" runat="server" id="txtFecha" /></div>
    <div class="sol_fecha"><img id="btnFecha" alt="calendario" src="../img/calendario.gif" style="width:20px; height:20px; cursor:pointer;" onmouseover="CambiarConfiguracionCalendario('txtFecha',this);"/></div>

 
    <div class="sol_tit50"> <label>Fecha Hasta</label></div>
    <div class="sol_campo30"><asp:TextBox CssClass="sol_campo25" runat="server" id="txtFechaHasta" /></div>
    <div class="sol_fecha"><img id="btnFechaHasta" alt="calendario" src="../img/calendario.gif" style="width:20px; height:20px; cursor:pointer;" onmouseover="CambiarConfiguracionCalendario('txtFechaHasta',this);"/></div>
    <div class="sol_tit2"></div>
</div>

<div class="solicitud100">
    <div class="sol_tit50"><label>Nº Solicitud</label></div>
    <div class="sol_campo30"><asp:TextBox CssClass="sol_campo25" runat="server" id="txtNroSolicitud" onkeypress="return ValidarNumeroEntero(event,this);"/></div>
   <div class="sol_fecha"></div>
    <div class="sol_tit50">  <label>N° Parte</label></div>
    <div class="sol_campo30"><asp:TextBox runat="server" CssClass="sol_campo25" id="txtNroParte" onkeypress="return ValidarNumeroEntero(event,this);" Text="0"/></div>            
    <div class="sol_fecha"></div>
</div>
<div class="solicitud100">
    <div class="sol_tit50"> <label>Empresa</label></div>
    <div class="sol_campo30"><asp:DropDownList runat="server" id="ddlEmpresa" /></div>    
    <div class="sol_fecha"></div>        
    <div class="sol_tit50"> <label>N° Equipo</label></div>
    <div class="sol_campo30"><asp:DropDownList runat="server" id="ddlTrailer"  /></div> 
</div>
<div class="solicitud100sin">
    <div class="sol_btn"><asp:Button runat="server" id="btnBuscar" cssclass="btn_form"
              Text="Buscar" onclick="btnBuscar_Click" /></div>
</div>
</div>


<asp:GridView ID="gvOTI" runat="server" AutoGenerateColumns="False" 
         DataMember="idCondicion" CssClass="tabla3" HeaderStyle-CssClass="celda1" 
         RowStyle-CssClass="celda2" onrowcommand="gvOTI_RowCommand"
         onrowdatabound="gvOTI_RowDataBound"
         AllowPaging="true" PageSize="20" 
        onpageindexchanging="gvOTI_PageIndexChanging" PagerStyle-CssClass="tabla_font">
         <Columns>
            <asp:TemplateField Visible="false">
                 <ItemTemplate>
                     <asp:Label ID="lblidOrden" runat="server" Text='<%# Bind("idOrden") %>'></asp:Label>
                     <asp:Label ID="lblidSolicitud" runat="server" Text='<%# Bind("idSolicitud") %>'></asp:Label>
                     <asp:HiddenField ID="hfidParte" runat="server" value='<%# Bind("idParteEntrega") %>'/>                     
                 </ItemTemplate>
             </asp:TemplateField>
             <asp:TemplateField HeaderText="Nº SS" HeaderStyle-CssClass="columna1" ItemStyle-CssClass="columna1" HeaderStyle-Width="10%" ItemStyle-Width="10%">               
                 <ItemTemplate>
                     <asp:Label ID="lblNroSolicitud" runat="server" Text='<%# Bind("NroSolicitud") %>'></asp:Label>
                 </ItemTemplate>
             </asp:TemplateField>
             <asp:TemplateField HeaderText="Fecha" HeaderStyle-CssClass="columna2" ItemStyle-CssClass="columna2" HeaderStyle-Width="15%" ItemStyle-Width="15%">
                 <ItemTemplate>
                     <asp:Label ID="lblFecha" runat="server" Text='<%# Bind("Fecha") %>'></asp:Label>
                 </ItemTemplate>
             </asp:TemplateField>
             <asp:TemplateField  HeaderText="Empresa" HeaderStyle-CssClass="columna5" ItemStyle-CssClass="columna5">
                <ItemTemplate>
                     <asp:Label ID="lblEmpresa" runat="server" Text='<%# Bind("Empresa") %>'></asp:Label>
                 </ItemTemplate>
             </asp:TemplateField>
              <asp:TemplateField HeaderText="Sector" HeaderStyle-CssClass="columna5" ItemStyle-CssClass="columna5">
                 <ItemTemplate>
                     <asp:Label ID="lblSolicitante" runat="server" Text='<%# Bind("Sector") %>'></asp:Label>
                 </ItemTemplate>
             </asp:TemplateField>
             <asp:TemplateField  HeaderText="Servicio" HeaderStyle-CssClass="columna6" ItemStyle-CssClass="columna6">
                <ItemTemplate>
                     <asp:Label ID="lblTipoServ" runat="server" Text='<%# Bind("TipoServicio") %>'></asp:Label>
                 </ItemTemplate>
             </asp:TemplateField>             
             <asp:TemplateField  HeaderText="Nº Equipo" HeaderStyle-CssClass="columna7" ItemStyle-CssClass="columna7">
                <ItemTemplate>
                     <asp:Label ID="lblTrailer" runat="server" Text='<%# Bind("Trailer") %>'></asp:Label>
                 </ItemTemplate>
             </asp:TemplateField>
             <asp:TemplateField  HeaderText="IDA" HeaderStyle-CssClass="columna1" ItemStyle-CssClass="columna1">
                <ItemTemplate>
                     <asp:CheckBox ID="chkEntrega" Enabled="false" runat="server" Checked='<%# Bind("Entrega") %>'></asp:CheckBox>
                 </ItemTemplate>
             </asp:TemplateField>
             <asp:TemplateField  HeaderText="PaP" HeaderStyle-CssClass="columna1" ItemStyle-CssClass="columna1">
                <ItemTemplate>
                     <asp:CheckBox ID="chkNuevoMov" Enabled="false" runat="server" Checked='<%# Bind("NuevoMovimiento") %>'></asp:CheckBox>
                 </ItemTemplate>
             </asp:TemplateField>
             <asp:TemplateField  ItemStyle-CssClass="btn_iconos">
                <ItemTemplate>
                     <asp:LinkButton ID="btnVer" runat="server" ToolTip="Ver OTI" CssClass="ver" CommandArgument='<%# Bind("idSolicitud") %>' CommandName="Ver"></asp:LinkButton>
                 </ItemTemplate>
             </asp:TemplateField>
         </Columns>
     </asp:GridView>

</div>
</asp:Content>
