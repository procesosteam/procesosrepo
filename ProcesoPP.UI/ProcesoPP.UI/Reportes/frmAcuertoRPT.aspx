<%@ Page Title="PP - Impresión de Acuerdo" Language="C#" AutoEventWireup="true" CodeBehind="frmAcuertoRPT.aspx.cs" Inherits="ProcesoPP.UI.Reportes.frmAcuertoRPT" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=13.0.2000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Imprimir Acuerdo</title>
    <link rel="icon" href="../iconos/favicon.ico" type="image/x-icon" />    
    <link href="../Css/estilos.css" rel="stylesheet" type="text/css"/>
    <link href="../Css/acuerdo.css" rel="stylesheet" type="text/css"/>
    <script type="text/javascript">
        function close() {
            window.close();
            return false;
        }
    </script>
</head>
<body >
    <form id="form1" runat="server">
        <div class="sol_100">
            <div class="sol_tit"><label>Elije una impresora</label></div>
            <div class="sol_campo50">
                <asp:DropDownList ID="ddlPrinter" runat="server"></asp:DropDownList>
            </div></div>
        <div class="sol_100">
            <asp:Button runat="server" ID="btnPrint" Text="Imprimir" CssClass="btn_form" OnClientClick="close();" 
                onclick="btnPrint_Click"/>
        </div>
        <div class="sol_100">
            <CR:CrystalReportViewer ID="CrystalReportViewer1" runat="server" CssFilename="~/Css/acuerdo.css" 
            AutoDataBind="true" />
        </div>
    </form>
</body>
</html>

