﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ProcesoPP.Business;
using System.Drawing.Printing;
using System.Data;
using System.Configuration;
using ProcesoPP.Services;

namespace ProcesoPP.UI.Reportes
{
    public partial class frmRemitoRPT : System.Web.UI.Page
    {
        public int _idRemito
        {
            get { return (int)ViewState["idRemito"]; }
            set { ViewState["idRemito"] = value; }
        }
        public int _NroCopias
        {
            get { return int.Parse(ConfigurationManager.AppSettings["copias"].ToString()); }
        }
        public string _impresora
        {
            get { return (string)(ConfigurationManager.AppSettings["impresora"].ToString()); }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                InitScreen();
            }
            catch (Exception ex)
            {
                string path = Server.MapPath("/ErrorLog.txt");
                Common.LogError(ex, path);
                Mensaje.errorMsj("Ha ocurrido un error:  " + ex.Message, this.Page, "ERROR", null);
            }
        }

        private void InitScreen()
        {
            _idRemito = 0;
            if (Request["idRemito"] != null)
                _idRemito = int.Parse(Request["idRemito"]);

            updateRemito();
            ImprimirRemitoRPT();
        }

        private void ImprimirRemitoRPT()
        {

            //string NombreImpresora = @"\\CYN-PC\HP Deskjet F4200 series";//Donde guardare el nombre de la impresora por defecto
            
            DataTable Dt = new DataTable();
            RemitosBus oRemitosBus = new RemitosBus();
            Dt = oRemitosBus.RemitoGetRPT(_idRemito);
            
            RemitoDS oRemitoDs = new RemitoDS();
            if (Dt.Rows.Count > 0)
            {
                DataRow dr = Dt.Rows[0];
                DataRow drNew = oRemitoDs.Remitos.NewRow();
                drNew["NroRemito"] = dr["nroRemito"];
                drNew["fecha"] = dr["fecha"];
                DateTime fecha = DateTime.Parse(dr["fecha"].ToString());
                drNew["dia"] = fecha.Day.ToString();
                drNew["mes"] = fecha.Month.ToString();
                drNew["anio"] = fecha.Year.ToString();
                drNew["observacion"] = dr["observacion"];
                drNew["cliente"] = dr["RazonSocial"];
                drNew["domicilio"] = dr["domicilio"];
                drNew["cuit"] = dr["cuit"];
                drNew["condIva"] = dr["condicionIva"];
                drNew["Sector"] = dr["sector"];
                drNew["Solicitante"] = dr["solicitante"];
                drNew["NroSS"] = dr["NroSolicitud"];
                drNew["NroPE"] = dr["NroParte"];
                drNew["CCosto"] = dr["CCosto"];
                drNew["NroOC"] = dr["NroOC"];
                oRemitoDs.Remitos.Rows.Add(drNew);

                foreach (DataRow drDetalle in Dt.Rows)
                {
                    DataRow drDetDs = oRemitoDs.RemitoDetalle.NewRow();
                    drDetDs["Cantidad"] = drDetalle["Cantidad"].ToString() == "0" ? "" : drDetalle["Cantidad"];
                    drDetDs["Detalle"] = drDetalle["detalle"];
                    drDetDs["PrecioUnit"] = drDetalle["PrecioUnit"];
                    drDetDs["PrecioTotal"] = drDetalle["PrecioTotal"];
                    drDetDs["nroRemito"] = dr["nroRemito"];
                    oRemitoDs.RemitoDetalle.Rows.Add(drDetDs);
                }
            }
                        
            remito RP = new remito();            

            RP.Load(Server.MapPath("~/Reportes/remito.rpt"));
            RP.SetDataSource(oRemitoDs);
            this.CrystalReportViewer1.ReportSource = RP;
            
            RP.PrintOptions.PrinterName = _impresora; //Asigno la impresora
            RP.PrintToPrinter(_NroCopias, false, 0, 0); //Imprimo 3 copias
            
        }
                
        private void updateRemito()
        {
            RemitosBus oRemitosBus = new RemitosBus();
            Model.Remitos oRemito = new ProcesoPP.Model.Remitos();
            oRemito = oRemitosBus.RemitosGetById(_idRemito);
            oRemito.Impreso = true;
            oRemitosBus.RemitosUpdate(oRemito);
        }

    }
}
