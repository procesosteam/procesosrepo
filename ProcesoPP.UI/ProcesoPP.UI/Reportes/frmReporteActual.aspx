﻿<%@ Page Language="C#"  MasterPageFile="~/frmPrincipal.Master" AutoEventWireup="true" CodeBehind="frmReporteActual.aspx.cs" Inherits="ProcesoPP.UI.Reportes.frmReporteActual" Title="PP - Posicion Actual " %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<script type="text/javascript" >
    $(document).ready(function() { SetMenu("liReportes"); }); 
</script>
</asp:Content>
<asp:Content ID="cphContenedor" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="contenido" runat="server">
    <div id="apertura">Reporte Actual</div> 
    <div class="solicitud100">
        <div class="sol_tit50"> <label>Empresa</label></div>
      <div class="sol_campo30"><asp:DropDownList runat="server" CssClass="sol_campo50c" id="ddlEmpresa" AutoPostBack="true"
                onselectedindexchanged="ddlEmpresa_SelectedIndexChanged" /></div>  
<div class="sol_fecha"></div><div class="sol_fecha"></div>          
         <div class="sol_tit50"> <label>Sector</label></div>
       <div class="sol_campo30"><asp:DropDownList runat="server" CssClass="sol_campo50c" id="ddlSector" /></div>    
    </div>
    <div class="solicitud100">
        <div class="sol_tit50"> <label>Condición</label></div>
       <div class="sol_campo30"><asp:DropDownList runat="server" CssClass="sol_campo50c" id="ddlCondicion" /></div>  
<div class="sol_fecha"></div><div class="sol_fecha"></div>          
         <div class="sol_tit50"> <label>N° Equipo</label></div>
       <div class="sol_campo30"><asp:DropDownList runat="server" CssClass="sol_campo50c" id="ddlTrailer" /></div>    
    </div>
     <div class="solicitud100">
        <div class="sol_tit50"> <label>Destino</label></div>
        <div class="sol_campo30"><asp:DropDownList runat="server" CssClass="sol_campo50c" id="ddlDestinos" /></div>  
<div class="sol_fecha"></div> <div class="sol_fecha"></div>          
         <div class="sol_tit50"><label>Tipo Servicio</label></div>
<div class="sol_campo30"><asp:DropDownList runat="server" CssClass="sol_campo50c" id="ddlTipoServicio"/></div>
    </div>
    <div class="solicitud100sin">    
        <div class="sol_btn">
            <asp:Button runat="server" id="btnBuscar" cssclass="btn_form"
                  Text="Buscar" onclick="btnBuscar_Click" />
        </div>
        <div class="sol_btn">
            <asp:Button runat="server" id="btnExportar" cssclass="btn_form"
                  Text="Exportar" onclick="btnExportar_Click" />
        </div>
    </div>
    
    <asp:GridView ID="gvMovimientos" runat="server" AutoGenerateColumns="true" 
        DataMember="idCondicion" HeaderStyle-CssClass="tabla_font" 
        RowStyle-CssClass= "tabla_font2" CssClass="tabla3" >
    </asp:GridView>
 </div>
</asp:Content>