﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="frmImprimirRemito.aspx.cs" Inherits="ProcesoPP.UI.Reportes.frmImprimirRemito" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>REMITO</title>
    <link href="../Css/estilos.css" rel="Stylesheet" />
    <style type="text/css">
    @media print {
    .header, .hide { visibility: hidden }
    }
    </style>
    <script type="text/javascript">
    function load() {
        //var x = document.getElementById("frame1");
        window.print();
        window.print();
        window.print();
        window.close();   
    }
</script>
</head>
<body onload="load();">
    <form id="form1" runat="server">
    
    <%--<iframe id="frame1" runat="server" src="" frameborder="0" style="height: 0px; width: 0px;" />--%>
    <div>
       <div class="solicitud100">
        <div class="sol_tit"><label>Fecha: </label></div>
        <div class="sol_ver50g">
            <asp:Label runat="server" ID="lblFecha"></asp:Label> 
        </div>
        <div class="sol_tit">
            <label>Remito Nº: </label></div>  
            <div class="sol_ver50g"><asp:Label runat="server" ID="lblNroRemito"></asp:Label> </div>
        </div>
        <div class="solicitud100">
<div class="sol_tit">
    <label>Empresa: </label></div>
    <div class="sol_ver50g"><asp:Label runat="server" ID="lblEmpresa"></asp:Label>  </div>
<div class="sol_tit"><label>Domicilio: </label></div>  
<div class="sol_ver50g"> <asp:Label runat="server" ID="lblDomicilio"></asp:Label>  </div>
</div>


<div class="solicitud100">
<div class="sol_tit"> <label>Condicion IVA: </label></div>
<div class="sol_ver50g"><asp:Label runat="server" ID="lblIVA"></asp:Label>  </div>


<div class="sol_tit2"><label>CUIT:</label></div>
<div class="sol_ver30g"><asp:Label runat="server" ID="lblCUIT"></asp:Label>  </div>
</div>

<div class="solicitud100">
    <asp:GridView runat="server" ID="gvDetalles" AutoGenerateColumns="false"
        CssClass="tabla3"  onrowdatabound="gvDetalles_RowDataBound" >
        <Columns>
            <asp:TemplateField HeaderText="Cantidad" HeaderStyle-CssClass="r_tit15">
                <ItemTemplate>
                    <asp:TextBox runat="server" ID="txtCantidad" Text='<%# Eval("Cantidad") %>' CssClass="cajaTextoGrilla" onkeypress="return ValidarNumeroEntero(event,this);"></asp:TextBox>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Detalle" HeaderStyle-CssClass="r_tit65">
                <ItemTemplate>
                    <asp:TextBox runat="server" ID="txtDetalle" CssClass="cajaTextoGrilla" TextMode="MultiLine" Rows="4" Text='<%# Eval("detalle") %>' style="overflow:hidden;"></asp:TextBox>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Precio Unit." HeaderStyle-CssClass="r_tit15">
                <ItemTemplate>
                    <asp:TextBox runat="server" ID="txtPU" CssClass="cajaTextoGrilla" onkeypress="return ValidarNumeroDecimal(event,this,2);"  Text='<%# Eval("PU") %>'></asp:TextBox>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Total" HeaderStyle-CssClass="r_tit15" ItemStyle-HorizontalAlign="Right">
                <ItemTemplate>
                    <asp:TextBox runat="server" ID="txtTotal" CssClass="cajaTextoGrilla" onkeypress="return ValidarNumeroDecimal(event,this,2);"  Text='<%# Eval("Total") %>'></asp:TextBox>
                </ItemTemplate>
            </asp:TemplateField>
            
        </Columns>
    </asp:GridView>
</div>

<div class="solicitud100b" style="height:80px;">
<div class="sol_observacion"  style="width:15%;"><label>Observación:</label></div>
<div class="sol_observacion" style="width:80%;"><asp:TextBox runat="server" ID="txtObservacion" Width="100%" TextMode="MultiLine" Rows="3"></asp:TextBox>  </div>
</div>

<div class="solicitud100c">
    <div class="r_dato15">Sector</div>
    <div class="r_dato35"><asp:Label runat="server" ID="lblSector"></asp:Label> </div>

    <div class="r_dato15">Solicitante</div>
    <div class="r_dato35"><asp:Label runat="server" ID="lblSolicitante"></asp:Label> </div>
</div>


<div class="solicitud100c">
    <div class="r_dato15">C. Costos</div>
    <div class="r_dato35">MANUAL</div>

    <div class="r_dato15">OC Nº</div>
    <div class="r_dato35">MANUAL</div>
</div>


<div class="solicitud100c">
    <div class="r_dato15">SS Nº</div>
    <div class="r_dato35"><asp:Label runat="server" ID="lblNroSS"></asp:Label> </div>

    <div class="r_dato15">PO Nº</div>
    <div class="r_dato35"> <asp:Label runat="server" ID="lblNroParte"></asp:Label> </div>
</div>
    </div>
    </form>
</body>
</html>
