﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ProcesoPP.Business;
using ProcesoPP.Model;
using iTextSharp.text.pdf;
using iTextSharp.text;
using System.IO;
using ProcesoPP.Services;

namespace ProcesoPP.UI.Reportes
{
    public partial class frmOTIDetalle : System.Web.UI.Page
    {
        #region <PROPIEDADES>

        public int _idUsuario
        {
            get { return (int)ViewState["idUsuario"]; }
            set { ViewState["idUsuario"] = value; }
        }

        private int _idSolicitud
        {
            get { return (int)ViewState["idSolicitud"]; }
            set { ViewState["idSolicitud"] = value; }
        }

        private int _idVehiculo
        {
            get { return (int)ViewState["idVehiculo"]; }
            set { ViewState["idVehiculo"] = value; }
        }

        private int _idOTI
        {
            get { return (int)ViewState["idOTI"]; }
            set { ViewState["idOTI"] = value; }
        }

        private string _volver
        {
            get { return (string)ViewState["volver"]; }
            set { ViewState["volver"] = value; }
        }
        
        public Permisos _Permiso
        {
            get { return (Permisos)ViewState["permiso"]; }
            set { ViewState["permiso"] = value; }
        }

        #endregion

        #region EVENTOS

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                _idUsuario = Common.EvaluarSession(Request, this.Page);
                if (_idUsuario != 0)
                {
                    _Permiso = Common.getModulo(Request, this.Page);
                    if (_Permiso != null && _Permiso.Lectura)
                    {
                        if (!IsPostBack)
                        {
                            InitScreen();
                        }
                    }
                    else
                    {
                        Mensaje.errorMsj("NO TIENE PERMISOS A ESTE FORMULARIO", this.Page, "SIN ACCESO", "../frmPrincipal.aspx");
                    }
                }
                else
                {
                    Mensaje.errorMsj("DEBE LOGUEARSE", this.Page, "LOGIN", "../frmLogin.aspx");
                }
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        protected void btnImprimir_Click(object sender, EventArgs e)
        {
            try
            {
                Imprimir();
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        protected void btnVolver_Click(object sender, EventArgs e)
        {
            if (_volver != string.Empty)
                Response.Redirect(_volver);
        }
        #endregion

        #region <METODOS>

        #region IMPRESION

        public override void VerifyRenderingInServerForm(Control control)
        {
        }

        private void Imprimir()
        {
            string sPathApp = System.AppDomain.CurrentDomain.BaseDirectory.ToString();
            string fileName = sPathApp + @"Reportes\oti.pdf";
            string copia = "OTI_" + DateTime.Now.ToString("yyyyMMdd_HHmmss") + ".pdf";

            MemoryStream memoryStream = new MemoryStream();

            Merge(copia, fileName);

        }

        public void Merge(string strFileTarget, string arrStrFilesSource)
        {
            try
            {
                System.IO.MemoryStream stmFile = new MemoryStream();

                Document objDocument = null;
                PdfWriter objWriter = null;

                PdfReader objReader = new PdfReader(arrStrFilesSource);
                int intNumberOfPages = objReader.NumberOfPages;

                objDocument = new Document(objReader.GetPageSizeWithRotation(1));
                objWriter = PdfWriter.GetInstance(objDocument, stmFile);
                objDocument.Open();
                for (int intPage = 0; intPage < intNumberOfPages; intPage++)
                {
                    int intRotation = objReader.GetPageRotation(intPage + 1);
                    PdfImportedPage objPage = objWriter.GetImportedPage(objReader, intPage + 1);

                    // Asigna el tamaño de la página
                    objDocument.SetPageSize(objReader.GetPageSizeWithRotation(intPage + 1));
                    // Crea una nueva página
                    objDocument.NewPage();
                    // Añade la página leída
                    if (intRotation == 90 || intRotation == 270)
                        objWriter.DirectContent.AddTemplate(objPage, 0, -1f, 1f, 0, 0,
                                                            objReader.GetPageSizeWithRotation(intPage + 1).Height);
                    else
                        objWriter.DirectContent.AddTemplate(objPage, 1f, 0, 0, 1f, 0, 0);
                    agregarCampos(objWriter);
                }
                if (objDocument != null)
                    objDocument.Close();
                Response.ContentType = "application/pdf";

                Response.AddHeader("content-disposition", "attachment;filename=" + strFileTarget);
                Response.Buffer = true;
                Response.Clear();
                Response.OutputStream.Write(stmFile.GetBuffer(), 0, stmFile.GetBuffer().Length);
                Response.OutputStream.Flush();
                Response.End();

            }
            catch (Exception objException)
            {
                System.Diagnostics.Debug.WriteLine(objException.Message);

            }
        }

        private void agregarCampos(PdfWriter objWriter)
        {
            iTextSharp.text.pdf.BaseFont fuente = FontFactory.GetFont(FontFactory.HELVETICA, iTextSharp.text.Font.DEFAULTSIZE, iTextSharp.text.Font.NORMAL).BaseFont;

            PdfContentByte cb = objWriter.DirectContent;
            cb.BeginText();
            cb.SetFontAndSize(fuente, 12);
            //FECHA
            cb.SetTextMatrix(420, 710);
            cb.ShowText(DateTime.Now.Day.ToString());
            cb.SetTextMatrix(470, 710);
            cb.ShowText(DateTime.Now.Month.ToString());
            cb.SetTextMatrix(500, 710);
            cb.ShowText(DateTime.Now.Year.ToString());

            int y = 650;
            //EMPRESA
            cb.SetTextMatrix(140, y);
            cb.ShowText(lblEmpresa.Text);
            //NRO SS
            cb.SetTextMatrix(410, y);
            cb.ShowText(lblNroSS.Text);
            //SECTOR
            y = y - 20;
            cb.SetTextMatrix(140, y);
            cb.ShowText(lblSector.Text);
            //CONDICION
            cb.SetTextMatrix(418, y);
            cb.ShowText(lblCondicion.Text);
            //SOLICITANTE
            y = y - 20;
            cb.SetTextMatrix(145, y);
            cb.ShowText(txtNombre.Text);

            y = y - 57;
            //TIPO SERVICIO
            cb.SetTextMatrix(150, y);
            cb.ShowText(lblTipoServicio.Text);
            y -= 20;
            //EQUIPOS
            cb.SetTextMatrix(145, y);
            cb.ShowText(lblTrailer.Text);
            //ESP TECNICAS
            y -= 20;
            string items = "", coma = "";
            foreach (System.Web.UI.WebControls.ListItem item in lblEspecificacion.Items)
            {
                items += coma + item.Text;
                coma = " - ";
            }
            cb.SetTextMatrix(190, y);
            cb.ShowText(items);

            y = y - 57;
            //ORIGEN         
            cb.SetTextMatrix(140, y);
            cb.ShowText(lblLugarOrigen.Text);
            //POZO
            cb.SetTextMatrix(418, y);
            cb.ShowText(lblPozoOrigen.Text);

            y -= 20;
            //DESTINO
            cb.SetTextMatrix(140, y);
            cb.ShowText(lblLugarDestino.Text);
            //POZO
            cb.SetTextMatrix(418, y);
            cb.ShowText(lblPozoDestino.Text);

            y -= 20;
            //KMS RECORRIDOS
            cb.SetTextMatrix(150, y);
            cb.ShowText(lblKmsEstimados.Text);

            y -= 57;
            //Vehículo
            cb.SetTextMatrix(140, y);
            cb.ShowText(lblVehiculo.Text);
            y -= 20;
            //Chofer I
            cb.SetTextMatrix(140, y);
            cb.ShowText(lblChofer.Text);
            y -= 20;
            //Chofer II
            cb.SetTextMatrix(140, y);
            cb.ShowText(lblAcompanante.Text);

            cb.EndText();

        }

        #endregion

        private void InitScreen()
        {
            _idVehiculo = 0;
            _idSolicitud = 0;
            _idOTI = 0;

            if (Request["idSolicitud"] != null)
            {
                _idSolicitud = int.Parse(Request["idSolicitud"]);
                CargarSolicitud();
            }

            if (Request["idOTI"] != null && Request["idOTI"] != string.Empty)
            {
                _idOTI = int.Parse(Request["idOTI"]);
                CargarOTI();
            }
            if (Request["volver"] != null)
            {
                _volver = Request["volver"] + "?idModulo=" + _Permiso.oModulo.idModulo;
                if (!_volver.Contains("Historial"))
                    btnImprimir.Visible = false;
            }
            else
                _volver = string.Empty;
        }

        private void CargarOTI()
        {
            Model.OrdenTrabajo oOrdenTrabajo = new Model.OrdenTrabajo();
            OrdenTrabajoBus oOrdenTrabajoBus = new OrdenTrabajoBus();
            oOrdenTrabajo = oOrdenTrabajoBus.OrdenTrabajoGetById(_idOTI);
            lblVehiculo.Text = (new TransporteBus()).TransporteGetById(oOrdenTrabajo.IdTransporte).ItemVerificacion;
            Chofer oChofer = (new ChoferBus()).ChoferGetById(oOrdenTrabajo.IdChofer);
            lblChofer.Text = oChofer.Apellido + ", " + oChofer.Nombre;
            Chofer oAcompanante = (new ChoferBus()).ChoferGetById(oOrdenTrabajo.IdAcompanante);
            lblAcompanante.Text = oAcompanante.Apellido + ", " + oAcompanante.Nombre;            
            _idVehiculo = oOrdenTrabajo.IdVehiculo;
            lblNroOT.Text = oOrdenTrabajo.IdOrden.ToString();

            if (_idVehiculo != 0)
            {
                lblTrailer.Visible = true;
                Model.Vehiculo oVehiculo = (new VehiculoBus()).VehiculoGetById(oOrdenTrabajo.IdVehiculo);
                lblTrailer.Text = oVehiculo.oVehiculoIdentificacion.oCodificacion.Codigo + ' ' + oVehiculo.oVehiculoIdentificacion.NroCodificacion;
                //lblEspecificacion.Items.Add(oVehiculo.oEspecificacion.Descripcion);
            }
        }

        private void CargarSolicitud()
        {
            SolicitudClienteBus oSolicitudClienteBus = new SolicitudClienteBus();
            SolicitudCliente oSolicitudCliente = new SolicitudCliente();
            oSolicitudCliente = oSolicitudClienteBus.SolicitudClienteGetById(_idSolicitud);

            lblNroSS.Text = oSolicitudCliente.NroSolicitud.ToString();
            lblCondicion.Text = oSolicitudCliente.oCondicion.Descripcion;
            lblFecha.Text = oSolicitudCliente.Fecha.ToString("dd/MM/yyyy");

            lblEmpresa.Text = oSolicitudCliente.oSector.oEmpresa.Descripcion;
            txtNombre.Text = oSolicitudCliente.oSolicitante.Nombre;
            lblSector.Text = oSolicitudCliente.oSector.Descripcion;
            lblNroAviso.Text = oSolicitudCliente.NroPedidoCliente;
            lblTelefono.Text = oSolicitudCliente.oSolicitante.Telefono;

            lblLugarOrigen.Text = oSolicitudCliente.oMovimiento.oLugarOrigen.Descripcion;
            lblPozoOrigen.Text = oSolicitudCliente.oMovimiento.PozoOrigen;
            lblLugarDestino.Text = oSolicitudCliente.oMovimiento.oLugarDestino.Descripcion;
            lblPozoDestino.Text = oSolicitudCliente.oMovimiento.PozoDestino;
            lblKmsEstimados.Text = oSolicitudCliente.oMovimiento.KmsEstimados.ToString();
            lblTipoServicio.Text = oSolicitudCliente.oTipoServicio.Descripcion;
            if (oSolicitudCliente.ServicioAux != null || oSolicitudCliente.ServicioAux != string.Empty)
                lblEspecificacion.Items.Add(oSolicitudCliente.ServicioAux);
        }
        
        #endregion
    }
}
