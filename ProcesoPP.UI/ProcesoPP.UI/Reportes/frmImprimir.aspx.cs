﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using iTextSharp;
using iTextSharp.text.pdf;
using iTextSharp.text;
using iTextSharp.text.pdf.parser;
using System.Collections;
using ProcesoPP.Services;

namespace ProcesoPP.UI.Reportes
{
    public partial class frmImprimir : System.Web.UI.Page
    {
        
        public string LblFecha{  get ;  set ; }
        public string lblEmpresa { get; set; }
        public string lblNroSS { get; set; }
        public string lblSector { get; set; }
        public string lblCondicion { get; set; }
        public string txtNombre { get; set; }
        public string lblTipoServicio { get; set; }
        public string lblTrailer { get; set; }
        public string ddlTrailer { get; set; }
        public string lblEspecificacion { get; set; }
        public string lblPozoOrigen { get; set; }
        public string lblPozoDestino { get; set; }
        public string lblLugarOrigen { get; set; }
        public string lblLugarDestino { get; set; }
        public string lblKmsEstimados { get; set; }
        public string ddlVehiculo { get; set; }
        public string ddlChofer { get; set; }
        public string ddlAcompanante { get; set; }

        public string _idOTI { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            //initVars();
            //Imprimir();
            _idOTI = "";
            if (Request["idOTI"] != null)
            {
                _idOTI = Request["idOTI"];
                PrintOTI();
            }
        }

        private void initVars()
        {
            LblFecha = string.Empty;
            lblEmpresa = string.Empty;
            lblNroSS = string.Empty;
            lblSector = string.Empty;
            lblCondicion = string.Empty;
            txtNombre = string.Empty;
            lblTipoServicio = string.Empty;
            lblTrailer = string.Empty;
            ddlTrailer = string.Empty;
            lblEspecificacion = string.Empty;
            lblPozoOrigen = string.Empty;
            lblPozoDestino = string.Empty;
            lblLugarOrigen = string.Empty;
            lblLugarDestino = string.Empty;
            lblKmsEstimados = string.Empty;
            ddlVehiculo = string.Empty;
            ddlChofer = string.Empty;
            ddlAcompanante = string.Empty;

            if (Request["LblFecha"] != null)
                LblFecha = Request["LblFecha"];
            if (Request["lblEmpresa"] != null)
                lblEmpresa = Request["lblEmpresa"];
            if (Request["lblNroSS"] != null)
                lblNroSS = Request["lblNroSS"];
            if (Request["lblSector"] != null)
                lblSector = Request["lblSector"];
            if (Request["lblCondicion"] != null)
                lblCondicion = Request["lblCondicion"];
            if (Request["txtNombre"] != null)
                txtNombre = Request["txtNombre"];
            if (Request["lblTipoServicio"] != null)
                lblTipoServicio = Request["lblTipoServicio"];
            if (Request["lblTrailer"] != null)
                lblTrailer = Request["lblTrailer"];
            if (Request["ddlTrailer"] != null)
                ddlTrailer = Request["ddlTrailer"];
            if (Request["lblEspecificacion"] != null)
                lblEspecificacion = Request["lblEspecificacion"];
            if (Request["lblPozoOrigen"] != null)
                lblPozoOrigen = Request["lblPozoOrigen"];
            if (Request["lblPozoDestino"] != null)
                lblPozoDestino = Request["lblPozoDestino"];
            if (Request["lblLugarOrigen"] != null)
                lblLugarOrigen = Request["lblLugarOrigen"];
            if (Request["lblLugarDestino"] != null)
                lblLugarDestino = Request["lblLugarDestino"];
            if (Request["lblKmsEstimados"] != null)
                lblKmsEstimados = Request["lblKmsEstimados"];
            if (Request["ddlVehiculo"] != null)
                ddlVehiculo = Request["ddlVehiculo"];
            if (Request["ddlChofer"] != null)
                ddlChofer = Request["ddlChofer"];
            if (Request["ddlAcompanante"] != null)
                ddlAcompanante = Request["ddlAcompanante"];

        }


        #region IMPRESION

        public override void VerifyRenderingInServerForm(Control control)
        {
        }

        private void Imprimir()
        {
            string sPathApp = System.AppDomain.CurrentDomain.BaseDirectory.ToString();
            string fileName = sPathApp + @"Reportes\oti.pdf";
            string copia = "OTI_" + DateTime.Now.ToString("yyyyMMdd_HHmmss") + ".pdf";

            MemoryStream memoryStream = new MemoryStream();

            Merge(copia, fileName);
            
        }

        public void Merge(string strFileTarget, string arrStrFilesSource)
        {
            try
            {
                MemoryStream stmFile = new MemoryStream();

                Document objDocument = null;
                PdfWriter objWriter = null;

                PdfReader objReader = new PdfReader(arrStrFilesSource);
                int intNumberOfPages = objReader.NumberOfPages;

                objDocument = new Document(objReader.GetPageSizeWithRotation(1));
                objWriter = PdfWriter.GetInstance(objDocument, stmFile);
                objDocument.Open();
                for (int intPage = 0; intPage < intNumberOfPages; intPage++)
                {
                    int intRotation = objReader.GetPageRotation(intPage + 1);
                    PdfImportedPage objPage = objWriter.GetImportedPage(objReader, intPage + 1);

                    // Asigna el tamaño de la página
                    objDocument.SetPageSize(objReader.GetPageSizeWithRotation(intPage + 1));
                    // Crea una nueva página
                    objDocument.NewPage();
                    // Añade la página leída
                    if (intRotation == 90 || intRotation == 270)
                        objWriter.DirectContent.AddTemplate(objPage, 0, -1f, 1f, 0, 0,
                                                            objReader.GetPageSizeWithRotation(intPage + 1).Height);
                    else
                        objWriter.DirectContent.AddTemplate(objPage, 1f, 0, 0, 1f, 0, 0);
                    agregarCampos(objWriter);
                }
                if (objDocument != null)
                    objDocument.Close();
                Response.ContentType = "application/pdf";

                Response.AddHeader("content-disposition", "attachment;filename=" + strFileTarget);
                Response.Buffer = true;
                Response.Clear();
                Response.OutputStream.Write(stmFile.GetBuffer(), 0, stmFile.GetBuffer().Length);
                Response.OutputStream.Flush();
                Response.End();

            }
            catch (Exception objException)
            {
                System.Diagnostics.Debug.WriteLine(objException.Message);

            }
        }

        private void agregarCampos(PdfWriter objWriter)
        {
            iTextSharp.text.pdf.BaseFont fuente = FontFactory.GetFont(FontFactory.HELVETICA, iTextSharp.text.Font.DEFAULTSIZE, iTextSharp.text.Font.NORMAL).BaseFont;

            PdfContentByte cb = objWriter.DirectContent;
            cb.BeginText();
            cb.SetFontAndSize(fuente, 12);
            //FECHA
            DateTime fechaOTI = DateTime.Parse(LblFecha);
            cb.SetTextMatrix(420, 710);
            cb.ShowText(fechaOTI.Day.ToString());
            cb.SetTextMatrix(470, 710);
            cb.ShowText(fechaOTI.Month.ToString());
            cb.SetTextMatrix(500, 710);
            cb.ShowText(fechaOTI.Year.ToString());

            int y = 650;
            //EMPRESA
            cb.SetTextMatrix(140, y);
            cb.ShowText(lblEmpresa);
            //NRO SS
            cb.SetTextMatrix(410, y);
            cb.ShowText(lblNroSS);
            //SECTOR
            y = y - 20;
            cb.SetTextMatrix(140, y);
            cb.ShowText(lblSector);
            //CONDICION
            cb.SetTextMatrix(418, y);
            cb.ShowText(lblCondicion);
            //SOLICITANTE
            y = y - 20;
            cb.SetTextMatrix(145, y);
            cb.ShowText(txtNombre);

            y = y - 57;
            //TIPO SERVICIO
            cb.SetTextMatrix(150, y);
            cb.ShowText(lblTipoServicio);
            y -= 20;
            //EQUIPOS
            cb.SetTextMatrix(145, y);
            cb.ShowText(lblTrailer != string.Empty ? lblTrailer : ddlTrailer);
            //ESP TECNICAS
            y -= 20;
            string items = lblEspecificacion;           
            cb.SetTextMatrix(190, y);
            cb.ShowText(items);

            y = y - 57;
            //ORIGEN         
            cb.SetTextMatrix(140, y);
            cb.ShowText(lblLugarOrigen);
            //POZO
            cb.SetTextMatrix(418, y);
            cb.ShowText(lblPozoOrigen);

            y -= 20;
            //DESTINO
            cb.SetTextMatrix(140, y);
            cb.ShowText(lblLugarDestino);
            //POZO
            cb.SetTextMatrix(418, y);
            cb.ShowText(lblPozoDestino);

            y -= 20;
            //KMS RECORRIDOS
            cb.SetTextMatrix(150, y);
            cb.ShowText(lblKmsEstimados);

            y -= 57;
            //Vehículo
            cb.SetTextMatrix(140, y);
            cb.ShowText(ddlVehiculo);
            y -= 20;
            //Chofer I
            cb.SetTextMatrix(140, y);
            cb.ShowText(ddlChofer);
            y -= 20;
            //Chofer II
            cb.SetTextMatrix(140, y);
            cb.ShowText(ddlAcompanante);

            cb.EndText();

        }

        private void PrintOTI()
        {
            rptOtiDS ds = new rptOtiDS();

            rptOtiDS.OrdenTrabajoDataTable table = ds.OrdenTrabajo;
            rptOtiDSTableAdapters.OrdenTrabajoTableAdapter adapter = new rptOtiDSTableAdapters.OrdenTrabajoTableAdapter();
            adapter.Fill(table, _idOTI);
                        
            rptOTI oOTIRpt = new rptOTI();

            oOTIRpt.Load(Server.MapPath("~/Reportes/rptOTI.rpt"));
            oOTIRpt.SetDataSource(ds);
            CrystalReportViewer1.ReportSource = oOTIRpt;

            Response.ContentType = "application/pdf";
            Response.Buffer = true;
            Response.Clear();

            oOTIRpt.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, true, "Orden de Trabajo");

            Response.End();

        }

        #endregion

    }
}
