﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ProcesoPP.Model;
using ProcesoPP.Business;
using ProcesoPP.Services;
using System.Web.UI.HtmlControls;
using ParteDiario.Services;
using System.Data;

namespace ProcesoPP.UI.Reportes
{
    public partial class frmReporteActual : System.Web.UI.Page
    {
        #region PROPIEDADES

        public Permisos _Permiso
        {
            get { return (Permisos)ViewState["permiso"]; }
            set { ViewState["permiso"] = value; }
        }

        public int _idUsuario
        {
            get { return (int)ViewState["idUsuario"]; }
            set { ViewState["idUsuario"] = value; }
        }

        #endregion

        #region EVENTOS

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                _idUsuario = Common.EvaluarSession(Request, this.Page);
                if (_idUsuario != 0)
                {
                    _Permiso = Common.getModulo(Request, this.Page);
                    if (_Permiso != null && _Permiso.Lectura)
                    {
                        if (!IsPostBack)
                            InitScreen();
                    }
                    else
                    {
                        Mensaje.errorMsj("NO TIENE PERMISOS A ESTE FORMULARIO", this.Page, "SIN ACCESO", "../frmPrincipal.aspx");
                    }
                }
                else
                {
                    Mensaje.errorMsj("DEBE LOGUEARSE", this.Page, "LOGIN", "../frmLogin.aspx");
                }
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        protected void ddlEmpresa_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddlEmpresa.SelectedIndex > 0)
                {
                    CargarSector(ddlEmpresa.SelectedValue);
                }
                else
                {
                    ddlSector.Items.Clear();
                    ddlSector.Items.Insert(0, new ListItem("Seleccione...", "0"));
                }
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                CargarGrilla();
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        protected void btnExportar_Click(object sender, EventArgs e)
        {
            try
            {
                Exportar();
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        public override void VerifyRenderingInServerForm(Control control)
        {

        }

        #endregion

        #region METODOS

        private void Exportar()
        {
            SolicitudClienteBus oSolicitudClienteBus = new SolicitudClienteBus();
            DataTable dt = oSolicitudClienteBus.ReportePosicionActual(ddlEmpresa.SelectedValue,
                                                                        ddlSector.SelectedValue,
                                                                        ddlCondicion.SelectedValue,
                                                                        ddlTrailer.SelectedValue,
                                                                        ddlDestinos.SelectedValue,
                                                                        ddlTipoServicio.SelectedValue);
         
            string path = System.IO.Path.Combine(Server.MapPath("~"), "img");
            ExportarExcel oExport = new ExportarExcel("ReporteActual_" + DateTime.Now.ToString("yyyyMMddHHmmss"), path, "REPORTE ACTUAL");
            oExport.ExportarExcel2003(dt);
        }

        private void InitScreen()
        {
            CargarCondicion();
            CargarEmpresa();
            CargarLugares();
            CargarTrailer();
            CargarTipoServicio();
            ddlSector.Items.Insert(0, new ListItem("Seleccione...", "0"));
        }

        private void CargarGrilla()
        {
            SolicitudClienteBus oSolicitudClienteBus = new SolicitudClienteBus();
            gvMovimientos.DataSource = oSolicitudClienteBus.ReportePosicionActual(ddlEmpresa.SelectedValue,
                                                                                ddlSector.SelectedValue,
                                                                                ddlCondicion.SelectedValue,
                                                                                ddlTrailer.SelectedValue,
                                                                                ddlDestinos.SelectedValue, 
                                                                                ddlTipoServicio.SelectedValue);
                                                                            
            gvMovimientos.DataBind();

        }

        private void CargarTipoServicio()
        {
            TipoServicioBus oTipoServicioBus = new TipoServicioBus();
            oTipoServicioBus.CargarCombo(ref ddlTipoServicio, "Seleccione...");
        }

        private void CargarTrailer()
        {
            VehiculoBus oVehiculoBus = new VehiculoBus();
            oVehiculoBus.CargarComboEquipo(ref ddlTrailer, "Seleccione...");
        }

        private void CargarLugares()
        {
            LugarBus oLugarBus = new LugarBus();
            oLugarBus.CargarLugar(ref ddlDestinos);
            ddlDestinos.Items.Insert(0, new ListItem("Seleccione...", "0"));
        }

        private void CargarSector(string idEmpresa)
        {
            ddlSector.Items.Clear();
            SectorBus oSectorBus = new SectorBus();
            oSectorBus.CargarSectorPorEmpresa(ref ddlSector, int.Parse(idEmpresa));
            ddlSector.Items.Insert(0, new ListItem("Seleccione...", "0"));
        }

        private void CargarCondicion()
        {
            CondicionBus oCondicionBus = new CondicionBus();
            oCondicionBus.CargarCombo(ref ddlCondicion, "Seleccione...");
        }

        private void CargarEmpresa()
        {
            EmpresaBus oEmpresaBus = new EmpresaBus();
            oEmpresaBus.CargarEmpresa(ref ddlEmpresa, "Seleccione...");
            
        }


        #endregion
    }
}
