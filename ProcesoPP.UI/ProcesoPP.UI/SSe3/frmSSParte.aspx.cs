﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using ProcesoPP.Services;
using ProcesoPP.Model;
using ProcesoPP.Business;
using System.Data;

namespace ProcesoPP.UI.SSe3
{
    public partial class frmSSParte : System.Web.UI.Page
    {
        #region <PROPIEDADES>

        public Permisos _Permiso
        {
            get { return (Permisos)ViewState["permiso"]; }
            set { ViewState["permiso"] = value; }
        }

        public int _idUsuario
        {
            get { return (int)ViewState["idUsuario"]; }
            set { ViewState["idUsuario"] = value; }
        }

        private int _idSolicitud
        {
            get { return (int)ViewState["idSolicitud"]; }
            set { ViewState["idSolicitud"] = value; }
        }

        private int _idOTI
        {
            get { return (int)ViewState["idOTI"]; }
            set { ViewState["idOTI"] = value; }
        }

        private int _idParte
        {
            get { return (int)ViewState["idParte"]; }
            set { ViewState["idParte"] = value; }
        }

        private string _accion
        {
            get { return (string)ViewState["accion"]; }
            set { ViewState["accion"] = value; }
        }

        public int _ADM
        {
            get { return int.Parse(ConfigurationManager.AppSettings["ADM"].ToString()); }
        }

        public int _idConexion
        {
            get { return int.Parse(ConfigurationManager.AppSettings["CON"].ToString()); }
        }

        #endregion
        #region <EVENTOS>

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                _idUsuario = Common.EvaluarSession(Request, this.Page);
                if (_idUsuario != 0)
                {
                    _Permiso = Common.getModulo(Request, this.Page);
                    if (_Permiso != null && _Permiso.Lectura)
                    {
                        if (!IsPostBack)
                        {
                            InitScreen();
                        }
                    }
                    else
                    {
                        Mensaje.errorMsj("NO TIENE PERMISOS A ESTE FORMULARIO", this.Page, "SIN ACCESO", "../frmPrincipal.aspx");
                    }
                }
                else
                {
                    Mensaje.errorMsj("DEBE LOGUEARSE", this.Page, "LOGIN", "../frmLogin.aspx");
                }
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                btnGuardar.Visible = false;
                GuardarParte();
                
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        #endregion

        #region <METODOS>

        private void GuardarParte()
        {
            ParteEntrega oParteEntrega = new ParteEntrega();
            ParteEntregaBus oParteEntregaBus = new ParteEntregaBus();
            if (_idParte != 0)
            {
                oParteEntrega = oParteEntregaBus.ParteEntregaGetById(_idParte);
            }

            oParteEntrega.Fecha = DateTime.Now;
            oParteEntrega.HoraLlegada = DateTime.MinValue;
            oParteEntrega.HoraSalida = DateTime.MinValue;
            oParteEntrega.IdOrdenTrabajo = _idOTI;
            oParteEntrega.NroParteEntrega = txtNroParte.Text;
            oParteEntrega.Entrega = true;
            oParteEntrega.Observacion = txtObs.Text;

            List<VerificaParte> oListCant = getVerifica();
            if (_idParte == 0)
                oParteEntregaBus.ParteEntregaAdd(oParteEntrega, oListCant);
            else
            {
                oParteEntrega.IdParteEntrega = _idParte;
                oParteEntregaBus.ParteEntregaUpdate(oParteEntrega, oListCant);
            }
            Mensaje.successMsj("Parte entrega guardada con éxito.", this.Page, "Exito", "frmSSParteGestion.aspx?idModulo=" + _Permiso.oModulo.idModulo.ToString());

        }

        private List<VerificaParte> getVerifica()
        {
            List<VerificaParte> oListVerificaParte = new List<VerificaParte>();
            VerificaParte oVerificaParte = new VerificaParte();
            List<Verifica> oListVerifica = (new VerificaBus()).VerificaGetAll();
            if (divConexion.Visible)
            {
                oVerificaParte.IdVerifica = oListVerifica.Find(v => v.Descripcion.Contains("Conexi")).IdVerifica;
                oVerificaParte.Cantidad = decimal.Parse(txtConexion.Text);
                oListVerificaParte.Add(oVerificaParte);

            }
            if (divDesconexion.Visible)
            {
                oVerificaParte = new VerificaParte();
                oVerificaParte.IdVerifica = oListVerifica.Find(v => v.Descripcion.Contains("Desconexi")).IdVerifica;
                oVerificaParte.Cantidad = decimal.Parse(txtDesconexion.Text);
                oListVerificaParte.Add(oVerificaParte);
            }
            if (divCantPersonal.Visible)
            {
                oVerificaParte = new VerificaParte();
                oVerificaParte.IdVerifica = oListVerifica.Find(v => v.Descripcion.Contains("Personal")).IdVerifica;
                oVerificaParte.Cantidad = decimal.Parse(txtCantPersonal.Text);
                oListVerificaParte.Add(oVerificaParte);

                oVerificaParte = new VerificaParte();
                oVerificaParte.IdVerifica = oListVerifica.Find(v => v.Descripcion.Contains("HHT")).IdVerifica;
                oVerificaParte.Cantidad = decimal.Parse(txtHs.Text);
                oListVerificaParte.Add(oVerificaParte);
                
                oVerificaParte = new VerificaParte();
                oVerificaParte.IdVerifica = oListVerifica.Find(v => v.Descripcion.Contains("HHE")).IdVerifica;
                oVerificaParte.Cantidad = decimal.Parse(txtHsEspera.Text);
                oListVerificaParte.Add(oVerificaParte);
            }
            if (divCuadrilla.Visible)
            {
                oVerificaParte = new VerificaParte();
                oVerificaParte.IdVerifica = oListVerifica.Find(v => v.Descripcion.Contains("HHT")).IdVerifica;
                oVerificaParte.Cantidad = decimal.Parse(txtHsCuadrilla.Text);
                oListVerificaParte.Add(oVerificaParte);

                oVerificaParte = new VerificaParte();
                oVerificaParte.IdVerifica = oListVerifica.Find(v => v.Descripcion.Contains("HHE")).IdVerifica;
                oVerificaParte.Cantidad = decimal.Parse(txtHsEsperaCuadrilla.Text);
                oListVerificaParte.Add(oVerificaParte);
            }
            if (divMateriales.Visible)
            {
                oVerificaParte = new VerificaParte();
                oVerificaParte.IdVerifica = oListVerifica.Find(v => v.Descripcion.Contains("MAT")).IdVerifica;
                oVerificaParte.Cantidad = decimal.Parse(txtCantidadMateriales.Text);
                oListVerificaParte.Add(oVerificaParte);
            }

            return oListVerificaParte;
        }

        private void InitScreen()
        {

            _idSolicitud = 0;
            _idOTI = 0;
            _idParte = 0;

            if (Request["idSolicitud"] != null)
            {
                _idSolicitud = int.Parse(Request["idSolicitud"]);
                CargarSolicitud();
            }

            if (Request["idOTI"] != null && Request["idOTI"] != string.Empty)
            {
                _idOTI = int.Parse(Request["idOTI"]);
                CargarOTI();
            }
            if (Request["idParte"] != null
                && Request["idParte"] != string.Empty)
            {
                _idParte = int.Parse(Request["idParte"]);
                if (_idParte != 0)
                    CargarParte();
            }

            if (Request["Accion"] != null)
            {
                _accion = Request["Accion"];
                if (_accion.Equals("Ver"))
                {
                    Common.AnularControles(this.Form);
                    btnGuardar.Visible = false;
                }
            }
            
        }

        private void CargarParte()
        {
            ParteEntrega oParteEntrega = new ParteEntrega();
            ParteEntregaBus oParteEntregaBus = new ParteEntregaBus();
            oParteEntrega = oParteEntregaBus.ParteEntregaGetById(_idParte);
            txtNroParte.Text = oParteEntrega.NroParteEntrega;
            txtObs.Text = oParteEntrega.Observacion;            
            _idOTI = oParteEntrega.IdOrdenTrabajo;
            _idSolicitud = oParteEntrega.oOrdenTrabajo.IdSolicitudCliente;
            CargarSolicitud();
            CargarOTI();
            CargarVerificacion();    

        }

        private void CargarVerificacion()
        {
            VerificaBus oVerificaBus = new VerificaBus();
            DataTable dt = oVerificaBus.VerificaParteGetByIdParte(_idParte);
            foreach (DataRow item in dt.Rows)
            {
                if (divConexion.Visible)
                {
                    if (item["Descripcion"].ToString().Contains("Conexi"))
                    {
                        txtConexion.Text = item["Cantidad"].ToString();
                    }
                    if (item["Descripcion"].ToString().Contains("Desconexi"))
                    {
                        txtDesconexion.Text = item["Cantidad"].ToString();
                    }
                }

                if (divCantPersonal.Visible)
                {
                    if (item["Descripcion"].ToString().Contains("Personal"))
                    {
                        txtCantPersonal.Text = item["Cantidad"].ToString();
                    }
                    if (item["Descripcion"].ToString().Contains("HHE"))
                    {
                        txtHsEspera.Text = item["Cantidad"].ToString();
                    }
                    if (item["Descripcion"].ToString().Contains("HHT"))
                    {
                        txtHs.Text = item["Cantidad"].ToString();
                    }
                }

                if (divCuadrilla.Visible)
                {
                    if (item["Descripcion"].ToString().Contains("HHE"))
                    {
                        txtHsEsperaCuadrilla.Text = item["Cantidad"].ToString();
                    }
                    if (item["Descripcion"].ToString().Contains("HHT"))
                    {
                        txtHsCuadrilla.Text = item["Cantidad"].ToString();
                    }
                }
                if (divMateriales.Visible)
                {
                    if (item["Descripcion"].ToString().Contains("MAT"))
                    {
                        txtCantidadMateriales.Text = item["Cantidad"].ToString();
                    }                    
                }
            }

            if (txtCantPersonal.Text != string.Empty && txtHs.Text != string.Empty)
                lblTotalHs.Text = (decimal.Parse(txtCantPersonal.Text) * decimal.Parse(txtHs.Text)).ToString("N00");
            if (txtCantPersonal.Text != string.Empty && txtHsEspera.Text != string.Empty)
                lblTotalHsEspera.Text = (decimal.Parse(txtCantPersonal.Text) * decimal.Parse(txtHsEspera.Text)).ToString("N00");

        }


        private void CargarOTI()
        {
            Model.OrdenTrabajo oOrdenTrabajo = new Model.OrdenTrabajo();
            OrdenTrabajoBus oOrdenTrabajoBus = new OrdenTrabajoBus();
            oOrdenTrabajo = oOrdenTrabajoBus.OrdenTrabajoGetById(_idOTI);
            if (oOrdenTrabajo.oTransporte != null)
                lblVehiculo.Text = oOrdenTrabajo.oTransporte.oVehiculoIdentificacion.oCodificacion.Codigo + ' ' + oOrdenTrabajo.oTransporte.oVehiculoIdentificacion.NroCodificacion;
            if (oOrdenTrabajo.oChofer != null)
                lblChofer.Text = oOrdenTrabajo.oChofer.Apellido + ", " + oOrdenTrabajo.oChofer.Nombres;
            if (oOrdenTrabajo.oAcompanante != null)
                lblAcompanante.Text = oOrdenTrabajo.oAcompanante.Apellido + ", " + oOrdenTrabajo.oAcompanante.Nombres;
            if (oOrdenTrabajo.oVehiculo != null)
                lblEquipo.Text = oOrdenTrabajo.oVehiculo.Descripcion;
            if (oOrdenTrabajo.oCuadrilla != null)
                lblCuadrilla.Text = oOrdenTrabajo.oCuadrilla.Nombre;
            if (oOrdenTrabajo.oListOTIConcepto != null && oOrdenTrabajo.oListOTIConcepto.Count>0)
            {
                txtDetalle.Text = oOrdenTrabajo.oListOTIConcepto.First(o => o.IdTipoServicio == oOrdenTrabajo.oSolicitudCliente.IdTipoServicio).Valor;
                if (oOrdenTrabajo.oSolicitudCliente.oTipoServicio.RequiereEquipo && 
                    oOrdenTrabajo.oSolicitudCliente.oTipoServicio.oEspecificacion.Exists(e => !e.EquipoPropio))
                {
                    string id = oOrdenTrabajo.oListOTIConcepto.First(o => o.IdTipoServicio == oOrdenTrabajo.oSolicitudCliente.IdTipoServicio && oOrdenTrabajo.oSolicitudCliente.oTipoServicio.RequiereEquipo).Valor;
                    Equipos oEQ = new Equipos();
                    oEQ = (new EquiposBus()).EquiposGetById(int.Parse(id));
                    lblEquipo.Text = oEQ.Descripcion;
                }
                if (divConexion.Visible)
                {
                    txtConexion.Text = txtDetalle.Text;
                }
                if (divDesconexion.Visible)
                {
                    txtDesconexion.Text = txtDetalle.Text;
                }
            }
            if (oOrdenTrabajo.oListOTIDetalle.Count > 0)
            {
                PersonalBus oPersonalBus = new PersonalBus();
                gvPersonal.DataSource = oPersonalBus.PersonalGetByIdOrden(oOrdenTrabajo.IdOrden);
                gvPersonal.DataBind();

                txtCantPersonal.Text = oOrdenTrabajo.oListOTIDetalle.Count(i=> i.IdPersonal != null && i.IdPersonal != 0).ToString();
            }
        }

        #region CARGASOLICITUD

        private void CargarSolicitud()
        {
            SolicitudClienteBus oSolicitudClienteBus = new SolicitudClienteBus();
            SolicitudCliente oSS = new SolicitudCliente();
            oSS = oSolicitudClienteBus.SolicitudClienteGetById(_idSolicitud);
            lblNroSS.Text = oSS.NroPedidoCliente == string.Empty ? oSS.NroSolicitud.ToString() + " - " + oSS.letra : oSS.NroPedidoCliente;            
            lblEmpresa.Text = oSS.oSector.oEmpresa.Descripcion;
            lblSector.Text = oSS.oSector.Descripcion;
            lblSolicitante.Text = oSS.oSolicitante != null ? oSS.oSolicitante.Apellido : "";
            //MOVIMIENTO
            if (oSS.oTipoServicio.RequiereTraslado &&
                oSS.oMovimiento != null)
            {
                CargarMovimientos(oSS);
            }
            //EQUIPO
            if (oSS.oTipoServicio.RequiereEquipo)
            {
                CargarConEspecificacion(oSS);                
            }
            //CUADRILLA
            if (oSS.oTipoServicio.RequiereCuadrilla)
            {
                CargarCuadrilla(oSS);
            }
            //REQUIERE PERSONA
            if (oSS.oTipoServicio.RequierePersonal)
            {
                CargarConCantPersona(oSS);
            }
           
            //MATERIALES
            if (oSS.oTipoServicio.RequiereMateriales)
            {
                CargarMateriales(oSS);  
            }

            //CONEXION/DESCONEXION
            if (!oSS.oTipoServicio.RequiereMateriales &&
                !oSS.oTipoServicio.RequiereTraslado &&
                !oSS.oTipoServicio.MasEquipo &&
                !oSS.oTipoServicio.RequierePersonal &&
                !oSS.oTipoServicio.RequiereCuadrilla &&
                !oSS.oTipoServicio.RequiereEquipo)       
            {
                CargarConnDesc(oSS);   
            }

        }

        private void CargarMateriales(SolicitudCliente oSS)
        {
            divMateriales.Visible = true;
            lblTipoServMateriales.Text = oSS.oTipoServicio.Descripcion;
            hfidTipoServMateriales.Value = oSS.oTipoServicio.IdTipoServicio.ToString();
            
            List<EspecificacionTecnica> oListET = oSS.oListEspecificaciones.FindAll(s => s.RequiereMaterial);
            lbMateriales.DataSource = oListET;
            lbMateriales.DataBind();
        }

        private void CargarCuadrilla(SolicitudCliente oSS)
        {
            divCuadrilla.Visible = true;
            lblTipoServCuadrilla.Text = oSS.oTipoServicio.Descripcion;
            hfidTipoServCuadrilla.Value = oSS.oTipoServicio.IdTipoServicio.ToString(); 
        }

        private void CargarConnDesc(SolicitudCliente oSS)
        {
            if (oSS.oTipoServicio.Descripcion.ToLower().Contains("descone"))
            {
                divDesconexion.Visible = true;
                lblTipoServDescon.Text = oSS.oTipoServicio.Descripcion;
                hfidTipoServDescon.Value = oSS.oTipoServicio.IdTipoServicio.ToString();                
            }
            else
            {
                divConexion.Visible = true;
                lblTipoServConn.Text = oSS.oTipoServicio.Descripcion;
                hfidTipoServConn.Value = oSS.oTipoServicio.IdTipoServicio.ToString();
            }
        }

        private void CargarConCantPersona(SolicitudCliente oSS)
        {
            divCantPersonal.Visible = true;
            lblTipoServCantPersonal.Text = oSS.oTipoServicio.Descripcion;
            hfidTipoServCantPersonal.Value = oSS.oTipoServicio.IdTipoServicio.ToString();
        }

        private void CargarConEspecificacion(SolicitudCliente oSS)
        {
            divConEsp.Visible = true;
            lblTipoServConEsp.Text = oSS.oTipoServicio.Descripcion;
            hfidTipoServConEsp.Value = oSS.oTipoServicio.IdTipoServicio.ToString();

        }

        private void CargarMovimientos(SolicitudCliente oSS)
        {
            divMov.Visible = true;
            lblTipoServMov.Text = oSS.oTipoServicio.Descripcion;
            hfidTipoServMov.Value = oSS.oTipoServicio.IdTipoServicio.ToString();

            lblOrigen.Text = oSS.oMovimiento.oLugarOrigen.Descripcion;
            lblPozoOrigen.Text = oSS.oMovimiento.PozoOrigen;
            lblDestino.Text = oSS.oMovimiento.oLugarDestino.Descripcion;
            lblPozoDestino.Text = oSS.oMovimiento.PozoDestino;
            lblKms.Text = oSS.oMovimiento.KmsEstimados.ToString();
        }

        #endregion

       
        #endregion
    }


}