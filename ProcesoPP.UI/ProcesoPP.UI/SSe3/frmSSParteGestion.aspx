﻿<%@ Page Title="PP - Gestion de partes" Language="C#" MasterPageFile="~/frmPrincipal.Master" AutoEventWireup="true"  CodeBehind="frmSSParteGestion.aspx.cs" EnableEventValidation="false" Inherits="ProcesoPP.UI.SSe3.frmSSParteGestion" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
 <script type="text/javascript">
     $(document).ready(function () { SetMenu("liOperaciones"); });


     function selectTodos() {
         gv = document.getElementById('<%= gvOTI.ClientID %>');
         chkHeader = gv.rows[0].cells[8].children[0].children[0];
         for (var i = 1; i < gv.rows.length; i++) {
             row = gv.rows[i];
             chkUnir = row.cells[8].children[1].children[0];
             if (chkHeader.checked) {
                 chkUnir.checked = true;
             }
             else {
                 chkUnir.checked = false;
             }
         }

         return false;
     }
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="DivFiltro">
        <div id="apertura">
            Gestión de Parte Entrega</div>
        <div class="solicitud100">
            <div class="sol_tit50">
                <label>Fecha Desde</label></div>
            <div class="sol_campo30">
                <asp:TextBox CssClass="sol_campo25" runat="server" ID="txtFecha" /></div>
            <div class="sol_fecha">
                <img id="btnFecha" alt="calendario" src="../img/calendario.gif" style="width: 20px;
                    height: 20px; cursor: pointer;" onmouseover="CambiarConfiguracionCalendario('txtFecha',this);" /></div>

            <div class="sol_fecha"></div>  
            <div class="sol_tit50">
                <label>Fecha Hasta</label></div>
            <div class="sol_campo30">
                <asp:TextBox CssClass="sol_campo25" runat="server" ID="txtFechaHasta" /></div>
            <div class="sol_fecha">
                <img id="btnFechaHasta" alt="calendario" src="../img/calendario.gif" style="width: 20px;
                    height: 20px; cursor: pointer;" onmouseover="CambiarConfiguracionCalendario('txtFechaHasta',this);" /></div>
            
        </div>
        <div class="solicitud100">
            <div class="sol_tit50"><label>Nº Solicitud</label></div>

            <div class="sol_campo30">
                <asp:TextBox CssClass="sol_campo25" runat="server" ID="txtNroSolicitud" /></div>

            <div class="sol_fecha"></div><div class="sol_fecha"></div>

            <div class="sol_tit50">
                <label>N° Parte</label></div>
            <div class="sol_campo30">
                <asp:TextBox runat="server" CssClass="sol_campo25" ID="txtNroParte" onkeypress="return ValidarNumeroEntero(event,this);" /></div>            
        </div>
        <cc1:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
        </cc1:ToolkitScriptManager>
       <div class="solicitud100">        
            <div class="sol_tit50"><label>Empresa</label></div>
        
            <div class="sol_campo30"><asp:DropDownList  runat="server" id="ddlEmpresa" CssClass="sol_campo50c" />
                    <cc1:CascadingDropDown ID="cddEmpresa" TargetControlID="ddlEmpresa" PromptText="Seleccione una empresa" PromptValue="0" 
                    ServiceMethod="GetEmpresa" runat="server" Category="idEmpresa" 
                    LoadingText="Cargando..."/>
            </div> 
            <div class="sol_fecha"></div>
            <div class="sol_fecha"></div>
            <div class="sol_tit50"><label>Sector</label></div>
            <div class="sol_campo30"><asp:DropDownList  runat="server" id="ddlSector" CssClass="sol_campo50c" />
                    <cc1:CascadingDropDown ID="cddSector" TargetControlID="ddlSector" PromptText="Seleccione un sector" PromptValue="0" 
                    ServiceMethod="GetSector" runat="server" Category="idSector" 
                    ParentControlID="ddlEmpresa" LoadingText="Cargando..." />
            </div> 
            <div class="sol_fecha"></div>
        </div>  
        
        
        <div class="solicitud100">
            <div class="sol_tit50">
                <label>Cuadrilla</label></div>

            <div class="sol_campo30">
                <asp:DropDownList runat="server" CssClass="sol_campo50c" ID="ddlCuadrilla" ></asp:DropDownList></div>

            <div class="sol_fecha"></div><div class="sol_fecha"></div>
            
            <div class="sol_tit50">
                <label>N° Equipo</label></div>
            <div class="sol_campo30">
                <asp:DropDownList runat="server" CssClass="sol_campo50c" ID="ddlTrailer" />
            </div>
        </div>

      <div class="solicitud100">          
        <div class="sol_tit50"> <label>Tipo Servicio</label></div>
        <div class="sol_campo30"><asp:DropDownList runat="server" CssClass="sol_campo50c"  id="ddlTipoServicio" /></div> 
        <div class="sol_fecha"></div>
        <div class="sol_fecha"></div>
        <div class="sol_tit50"> <label>Condición</label></div>
        <div class="sol_campo30"><asp:DropDownList runat="server" CssClass="sol_campo50c" id="ddlCondicion" /></div> 
     </div> 

        <div class="solicitud100sin"> 
            <div class="sol_btn">
                <asp:Button runat="server" ID="btnBuscar" CssClass="btn_form" Text="Buscar" OnClick="btnBuscar_Click" /></div>
        </div>
    </div>
    <asp:GridView ID="gvOTI" runat="server" AutoGenerateColumns="False" CssClass="tabla3"
        HeaderStyle-CssClass="celda1" RowStyle-CssClass="celda2" OnRowCommand="gvOTI_RowCommand"
        OnRowDataBound="gvOTI_RowDataBound" PagerStyle-CssClass="celda1" AllowPaging="true"
        PageSize="15" OnPageIndexChanging="gvOTI_PageIndexChanging">
        <Columns>
            <asp:TemplateField Visible="false">
                <ItemTemplate>
                    <asp:Label ID="lblidOrden" runat="server" Text='<%# Bind("idOrden") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Nº SS" HeaderStyle-CssClass="columna2" ItemStyle-CssClass="columna2" >
                <ItemTemplate>
                    <asp:Label ID="lblNroSolicitud" runat="server" Text='<%# Bind("NroSolicitud") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Nº Parte" HeaderStyle-CssClass="columna2" ItemStyle-CssClass="columna2" >
                <ItemTemplate>
                    <asp:Label ID="lblNroParte" runat="server" Text='<%# Bind("NroParte") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Fecha" HeaderStyle-CssClass="columna2" ItemStyle-CssClass="columna2">
                <ItemTemplate>
                    <asp:Label ID="lblFecha" runat="server" Text='<%# Bind("Fecha") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Empresa" HeaderStyle-CssClass="columna5" ItemStyle-CssClass="columna5">
                <ItemTemplate>
                    <asp:Label ID="lblEmpresa" runat="server" Text='<%# Bind("Empresa") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Sector" HeaderStyle-CssClass="columna5" ItemStyle-CssClass="columna5">
                <ItemTemplate>
                    <asp:Label ID="lblSolicitante" runat="server" Text='<%# Bind("Solicitante") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Servicio" HeaderStyle-CssClass="columna6" ItemStyle-CssClass="columna6">
                <ItemTemplate>
                    <asp:Label ID="lblTipoServ" runat="server" Text='<%# Bind("TipoServicio") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Nº Equipo" HeaderStyle-CssClass="columna7" ItemStyle-CssClass="columna7">
                <ItemTemplate>
                    <asp:Label ID="lblTrailer" runat="server" Text='<%# Bind("Trailer") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            
            
            <asp:TemplateField HeaderText="Cuadrilla" HeaderStyle-CssClass="columna7" ItemStyle-CssClass="columna7">
                <ItemTemplate>
                    <asp:Label ID="lblcuadrilla" runat="server" Text='<%# Bind("cuadrilla") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            
            
            <asp:TemplateField HeaderText="IDA" HeaderStyle-CssClass="columna1" ItemStyle-CssClass="columna1" Visible="false">
                <ItemTemplate>
                    <asp:CheckBox ID="chkEntrega" Enabled="false" runat="server" Checked='<%# Bind("Entrega") %>'>
                    </asp:CheckBox>
                </ItemTemplate>
            </asp:TemplateField>

            <asp:TemplateField  HeaderText="Unir" HeaderStyle-CssClass="columna1" ItemStyle-CssClass="columna1" Visible="false">
               <HeaderTemplate>
                    Unir
                    <asp:CheckBox ID="chkUnirH" runat="server" ToolTip="Para seleccionar todos los partes" onchange="return selectTodos();" ></asp:CheckBox>
               </HeaderTemplate>
                <ItemTemplate>
                    <asp:HiddenField runat="server" ID="hfIdOrden" Value='<%# Eval("idOrden") %>' />
                    <asp:CheckBox ID="chkUnir" runat="server" ToolTip="Click para unir o seleccionar partes" ></asp:CheckBox>
                 </ItemTemplate>
             </asp:TemplateField>                
            <asp:TemplateField ItemStyle-CssClass="btn_iconos" HeaderStyle-CssClass="btn_iconos">
                <ItemTemplate>
                    <asp:HiddenField ID="hfIdSCParte" runat="server" Value='<%# Eval("idSolicitud") %>' />
                    <asp:HiddenField ID="hfIdOT" runat="server" Value='<%# Bind("idOrden") %>' />
                    <asp:HiddenField ID="hfidParte" runat="server" Value='<%# Bind("idParteEntrega") %>' />
                    <asp:LinkButton ID="btnModificar" runat="server" ToolTip="Cargar" CssClass="editar"
                        CommandArgument='<%# Bind("idOrden") %>' CommandName="Verificar"></asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField ItemStyle-CssClass="btn_iconos" HeaderStyle-CssClass="btn_iconos">
                <ItemTemplate>
                    <asp:HiddenField ID="hfIdSC" runat="server" Value='<%# Eval("idSolicitud") %>' />
                    <asp:HiddenField ID="hfidParteVer" runat="server" Value='<%# Bind("idParteEntrega") %>' />
                    <asp:LinkButton ID="btnVer" runat="server" ToolTip="Ver" CssClass="ver" CommandArgument='<%# Bind("idOrden") %>'
                        CommandName="Ver"></asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField ItemStyle-CssClass="btn_iconos" HeaderStyle-CssClass="btn_iconos">
                <ItemTemplate>            
                    <asp:HiddenField ID="hfMensual" runat="server" Value='<%# Eval("mensual") %>' />        
                    <asp:LinkButton ID="btnMensual" runat="server" ToolTip="Habilitar Remito Mensual" CssClass="mensual" CommandArgument='<%# Bind("idParteEntrega") %>'
                        CommandName="Mensual"></asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
    
</asp:Content>
