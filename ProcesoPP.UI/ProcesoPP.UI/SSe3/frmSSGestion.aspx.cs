﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using ProcesoPP.Business;
using ProcesoPP.Model;
using ProcesoPP.Services;

namespace ProcesoPP.UI.SSe3
{
    public partial class frmSSGestion : System.Web.UI.Page
    {
        #region PROPIEDADES

        public Permisos _Permiso
        {
            get { return (Permisos)ViewState["permiso"]; }
            set { ViewState["permiso"] = value; }
        }

        public int _idUsuario
        {
            get { return (int)ViewState["idUsuario"]; }
            set { ViewState["idUsuario"] = value; }
        }
        
        public int _ADM
        {
            get { return int.Parse(ConfigurationManager.AppSettings["ADM"].ToString()); }
        }

        #endregion
        
        #region EVENTOS

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                 _idUsuario = Common.EvaluarSession(Request, this.Page);
                 if (_idUsuario != 0)
                 {
                     _Permiso = Common.getModulo(Request, this.Page);
                     if (_Permiso != null && _Permiso.Lectura)
                     {
                         if (!IsPostBack)
                         {
                             CargarEmpresa();
                             CargarEquipo();
                             CargarCuadrillas();
                         }
                     }
                     else
                     {
                         Mensaje.errorMsj("NO TIENE PERMISOS A ESTE FORMULARIO", this.Page, "SIN ACCESO", "../frmPrincipal.aspx");
                     }
                 }
                 else
                 {
                     Mensaje.errorMsj("DEBE LOGUEARSE", this.Page, "LOGIN", "../frmLogin.aspx");
                 }
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message + "\n StackTrace: " + ex.StackTrace, this.Page, "Error", null);
            }
        }
        
        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                CargarSolicitud();
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        protected void gvSolicitud_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            string strUrl = "";
            switch (e.CommandName)
            {
                case "OTI":
                    HiddenField hfIdOTI = ((DataControlFieldCell)((Control)(e.CommandSource)).Parent).FindControl("hfIdOT") as HiddenField;
                    HiddenField hfidVehiculo = ((DataControlFieldCell)((Control)(e.CommandSource)).Parent).FindControl("hfidVehiculo") as HiddenField;
                    //Response.Redirect("../OrdenTrabajo/frmOrdenTrabajoCarga.aspx?idSolicitud=" + e.CommandArgument + "&idOTI=" + hfIdOTI.Value + "&idModulo=" + _Permiso.oModulo.idModulo);
                    strUrl = "idSolicitud=" + Common.encriptarURL(getIdsSS(e.CommandArgument.ToString())) +
                             "&idOTI=" + Common.encriptarURL(hfIdOTI.Value) + 
                             "&idModulo=" + _Permiso.oModulo.idModulo.ToString() + 
                             "&idVehiculo="+Common.encriptarURL(hfidVehiculo.Value);
                    Response.Redirect("frmSSOti.aspx?"+strUrl);
                    break;
                case "Modificar":
                    //Response.Redirect("frmSolicitudCarga.aspx?idSolicitud=" + e.CommandArgument + "&idModulo=" + _Permiso.oModulo.idModulo);
                    Response.Redirect("frmSSNueva.aspx?idSolicitud=" + e.CommandArgument + "&idModulo=" + _Permiso.oModulo.idModulo + "&Accion=" + e.CommandName);
                    break;
                case "Regreso":
                    //Response.Redirect("../Regreso/frmSolicitudRegresoCarga.aspx?idSolicitud=" + e.CommandArgument + "&idModulo=" + _Permiso.oModulo.idModulo);
                    Response.Redirect("frmSSNueva.aspx?idSolicitud=" + e.CommandArgument + "&idModulo=" + _Permiso.oModulo.idModulo + "&Accion=" + e.CommandName);
                    break;
                case "NuevoMovimiento":                    
                    Response.Redirect("frmSSNueva.aspx?idSolicitud=" + e.CommandArgument + "&NuevoMovimiento=true" + "&idModulo=" + _Permiso.oModulo.idModulo + "&Accion=" + e.CommandName );
                    break;
                case"Eliminar":
                    try
                    {
                        hfIdOTI = ((DataControlFieldCell)((Control)(e.CommandSource)).Parent).FindControl("hfIdOT") as HiddenField;
                        Eliminar(e.CommandArgument.ToString(), hfIdOTI.Value);
                    }
                    catch (Exception ex)
                    {
                        Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
                    }
                    break;
            }
        }
        private string getIdsSS(string idSS)
        {
            string ids = "";
            string coma = "";
            bool a = false;
            foreach (GridViewRow row in gvSolicitud.Rows)
            {
                CheckBox chkUnir = row.FindControl("chkUnir") as CheckBox;
                if (chkUnir.Checked)
                {
                    HiddenField hfidSolicitud = row.FindControl("hfidSolicitud") as HiddenField;
                    ids += coma + hfidSolicitud.Value;
                    coma = ",";
                    if (idSS.Equals(hfidSolicitud.Value))
                    {
                        a = true;
                    }                    
                }
            }
            if (!a)
            {
                ids += idSS;
            }

            return ids;
        }

        protected void gvSolicitud_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
                CargarRow(e);
        }

        protected void gvSolicitud_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvSolicitud.PageIndex = e.NewPageIndex;
            CargarSolicitud();
        }

        #endregion

        #region METODOS


        private void CargarRow(GridViewRowEventArgs e)
        {           
            HiddenField hfEntrega = e.Row.FindControl("hfEntrega") as HiddenField;
            HiddenField hfIdOT = e.Row.FindControl("hfIdOT") as HiddenField;
            LinkButton btnRegreso = e.Row.FindControl("btnRegreso") as LinkButton;
            LinkButton btnOTI = e.Row.FindControl("btnOT") as LinkButton;
            LinkButton btnNuevoMov = e.Row.FindControl("btnNuevoMov") as LinkButton;
            LinkButton btnEliminar = e.Row.FindControl("btnEliminar") as LinkButton;
            Label lblNroPed = e.Row.FindControl("lblNroPed") as Label;
            Label lblNroSolicitud = e.Row.FindControl("lblNroSolicitud") as Label;

            if (lblNroPed.Text == string.Empty)
            {
                lblNroPed.Visible = false;
                lblNroSolicitud.Visible = true;
            }
            else
            {
                lblNroPed.Visible = true;
                lblNroSolicitud.Visible = false;
            }

            if (!bool.Parse(hfEntrega.Value))
            {
                btnRegreso.Visible = false;
                btnNuevoMov.Visible = false;
                //btnOTI.Visible = false;
                ((DataControlFieldCell)(btnRegreso.Parent)).Text = "R";
                e.Row.ToolTip = "Regreso";
                
            }

            if (int.Parse(hfIdOT.Value) == 0)
            {
                btnRegreso.Visible = false;
                btnNuevoMov.Visible = false;
            }
            if (!_Permiso.Escritura)
                btnOTI.Visible = false;

            Common.CargarRow(e, _Permiso);

            if (_Permiso.oTipoUsuario.idTipoUsuario == _ADM)
                btnEliminar.Visible = true;
            else
                btnEliminar.Visible = false;

        }        
    

        private void Eliminar(string idSolicitud, string idOti)
        {
            SolicitudCliente oSolicitudCliente = new SolicitudCliente();
            SolicitudClienteBus oSolicitudClienteBus = new SolicitudClienteBus();
            oSolicitudCliente = oSolicitudClienteBus.SolicitudClienteGetById(int.Parse(idSolicitud));
            oSolicitudCliente.Baja = true;
            oSolicitudClienteBus.SolicitudClienteUpdate(oSolicitudCliente);

            if (idOti.Length > 0 && !idOti.Equals(string.Empty) && int.Parse(idOti)>0)
            {
                OrdenTrabajoBus oOrdenTrabajoBus = new OrdenTrabajoBus();                
                oOrdenTrabajoBus.OrdenTrabajoDelete(int.Parse(idOti));
            }

            CargarSolicitud();
        }
        
        private void CargarEmpresa()
        {
            EmpresaBus oEmpresaBus = new EmpresaBus();
            ddlEmpresa.DataSource = oEmpresaBus.EmpresaGetAll();
            ddlEmpresa.DataTextField = "Descripcion";
            ddlEmpresa.DataValueField = "idEmpresa";
            ddlEmpresa.DataBind();
            ddlEmpresa.Items.Insert(0, new ListItem("Seleccione...","0"));
        }

        private void CargarSolicitud()
        {
            SolicitudClienteBus oSolicitudClienteBus = new SolicitudClienteBus();
            gvSolicitud.DataSource = oSolicitudClienteBus.SolicitudClienteGetByFilter(txtNroSolicitud.Text==""?"0":txtNroSolicitud.Text,
                                                                                      ddlEmpresa.SelectedValue,
                                                                                      txtFecha.Text,
                                                                                      txtFechaHasta.Text,
                                                                                      ddlTrailer.SelectedValue,
                                                                                      ddlCuadrilla.SelectedValue);
            gvSolicitud.DataBind();
        }

        private void CargarEquipo()
        {
            VehiculoBus oVehiculoBus = new VehiculoBus();
            oVehiculoBus.CargarComboEquipo(ref ddlTrailer, "Seleccione un equipo...");
        }

        private void CargarCuadrillas()
        {
            CuadrillaBus oCuadrillaBus = new CuadrillaBus();
            ddlCuadrilla.DataSource = oCuadrillaBus.CuadrillaGetAllBaja();
            ddlCuadrilla.DataTextField = "Nombre";
            ddlCuadrilla.DataValueField = "idCuadrilla";
            ddlCuadrilla.DataBind();
            
            ddlCuadrilla.Items.Insert(0, new System.Web.UI.WebControls.ListItem("Seleccione una Cuadrilla", "0"));
        }
        #endregion

    }
}
