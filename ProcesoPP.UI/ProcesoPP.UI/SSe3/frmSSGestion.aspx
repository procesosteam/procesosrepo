﻿<%@ Page Language="C#" MasterPageFile="~/frmPrincipal.Master" AutoEventWireup="true" CodeBehind="frmSSGestion.aspx.cs" Inherits="ProcesoPP.UI.SSe3.frmSSGestion" Title="Gestión de solicitudes" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<script type="text/javascript" >
    $(document).ready(function() { SetMenu("liOperaciones"); }); 
</script>
 <script type="text/javascript">
     function validarCheck() {
         cont = 0;
         nro = "";

         gv = document.getElementById('<%= gvSolicitud.ClientID %>');
         for (var i = 1; i < gv.rows.length; i++) {
             row = gv.rows[i];
             chkUnir = row.cells[8].children[1].children[0];
             if (chkUnir.checked) {
                 if (cont == 0) {
                     nro = row.cells[0].children[0].innerText.split("-")[0];
                 }
                 else {
                     if (nro != row.cells[0].children[0].innerText.split("-")[0]) {
                         nro = "";
                         break;
                     }
                 }
                 cont++;

             }
         }
         
         if (cont != 0 && nro == "") {
             new Messi("Debe seleccionar Solicitudes con el mismo Nro.",
                        { title: 'Error', modal: true, titleClass: 'error', buttons: [{ id: 0, label: 'Aceptar', val: 'Y'}] });
             return false;
         }
         
     }

     function selectTodos() {
         gv = document.getElementById('<%= gvSolicitud.ClientID %>');
         chkHeader = gv.rows[0].cells[9].children[0].children[0];
         for (var i = 1; i < gv.rows.length; i++) {
             row = gv.rows[i];
             chkUnir = row.cells[9].children[1].children[0];
             if (chkHeader.checked) {
                 chkUnir.checked = true;
             }
             else {
                 chkUnir.checked = false;
             }
         }

         return false;
     }
     </script>

</asp:Content>
<asp:Content ID="cphContenedor" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div id="contenido" runat="server">

    <div id="apertura">Gestión de Solicitudes Activas</div> 
        <div class="solicitud100">
            <div class="sol_tit50"> <label>Fecha Desde</label></div>
            <div class="sol_campo30"><asp:TextBox runat="server" CssClass="sol_campo25" id="txtFecha" /></div>
            <div class="sol_fecha"><img id="btnFecha" alt="calendario" src="../img/calendario.gif" style="width:20px; height:20px; cursor:pointer;" onmouseover="CambiarConfiguracionCalendario('txtFecha',this);"/></div>
<div class="sol_fecha"></div>  

 <div class="sol_tit50"> <label>Fecha Hasta</label></div>
            <div class="sol_campo30"><asp:TextBox runat="server" CssClass="sol_campo25" id="txtFechaHasta" /></div>
            <div class="sol_fecha"><img id="btnFechaHasta" alt="calendario" src="../img/calendario.gif" style="width:20px; height:20px; cursor:pointer;" onmouseover="CambiarConfiguracionCalendario('txtFechaHasta',this);"/></div>
        </div>

     <div class="solicitud100">
        <div class="sol_tit50"> <label>Nº Solicitud</label></div>
        <div class="sol_campo30"><asp:TextBox CssClass="sol_campo25" runat="server" id="txtNroSolicitud" onkeypress="return ValidarNumeroEntero(event,this);"/></div>
        <div class="sol_fecha"></div> 
        <div class="sol_fecha"></div>   
        <div class="sol_tit50"> <label>Empresa</label></div>
        <div class="sol_campo30"><asp:DropDownList  runat="server" CssClass="sol_campo50c" id="ddlEmpresa" /></div>        
    </div>
    <div class="solicitud100">
        <div class="sol_tit50"> <label>N° Equipo</label></div>
        <div class="sol_campo30"><asp:DropDownList runat="server" CssClass="sol_campo50c" id="ddlTrailer" /></div> 
        
        
        <div class="sol_fecha"></div> 
        <div class="sol_fecha"></div>   
        <div class="sol_tit50"> <label>Cuadrilla</label></div>
        <div class="sol_campo30"><asp:DropDownList  runat="server" CssClass="sol_campo50c" ID="ddlCuadrilla"></asp:DropDownList></div>        

  </div>

<div class="solicitud100sin">
<div class="sol_btn">
            <asp:Button runat="server" id="btnBuscar" cssclass="btn_form" Text="Buscar" onclick="btnBuscar_Click" />
        </div>
    </div>
</div>

    <asp:GridView ID="gvSolicitud" runat="server" AutoGenerateColumns="False" 
         DataMember="idCondicion" CssClass="tabla3" HeaderStyle-CssClass="celda1" 
            onrowcommand="gvSolicitud_RowCommand" onrowdatabound="gvSolicitud_RowDataBound"
            AllowPaging="true" PageSize="20" onpageindexchanging="gvSolicitud_PageIndexChanging"
        PagerStyle-CssClass="tabla_font">
         <Columns>
             <asp:TemplateField HeaderText="Nº SS" HeaderStyle-CssClass="columna1" ItemStyle-CssClass="columna1">
                 <ItemTemplate>
                     <asp:Label ID="lblNroSolicitud" runat="server" Text='<%# Bind("NroSolicitud") %>'></asp:Label>
                     <asp:Label ID="lblNroPed" runat="server" Text='<%# Bind("NroPedidoCliente") %>'></asp:Label>
                     
                     <asp:HiddenField ID="hfEntrega" runat="server" value='<%# Bind("Entrega") %>'/>
                 </ItemTemplate>
             </asp:TemplateField>
             <asp:TemplateField HeaderText="Fecha" HeaderStyle-CssClass="columna7" ItemStyle-CssClass="columna7">
                 <ItemTemplate>
                     <asp:Label ID="lblFecha" runat="server" Text='<%# Bind("Fecha") %>'></asp:Label>
                 </ItemTemplate>
             </asp:TemplateField>
               <asp:TemplateField HeaderText="Empresa" HeaderStyle-CssClass="columna6" ItemStyle-CssClass="columna6">
                 <ItemTemplate>
                     <asp:Label ID="lblSolicitante" runat="server" Text='<%# Bind("Empresa") %>'></asp:Label>
                 </ItemTemplate>
             </asp:TemplateField>
             <asp:TemplateField  HeaderText="Sector" HeaderStyle-CssClass="columna6" ItemStyle-CssClass="columna6">
                <ItemTemplate>
                     <asp:Label ID="lblEmpresa" runat="server" Text='<%# Bind("Sector") %>'></asp:Label>
                 </ItemTemplate>
             </asp:TemplateField>
             <asp:TemplateField  HeaderText="Servicio" HeaderStyle-CssClass="columna7" ItemStyle-CssClass="columna7">
                <ItemTemplate>
                     <asp:Label ID="lblTipoServ" runat="server" Text='<%# Bind("TipoServicio") %>'></asp:Label>
                 </ItemTemplate>
             </asp:TemplateField>             
             <asp:TemplateField  HeaderText="Nº Equipo" HeaderStyle-CssClass="columna7" ItemStyle-CssClass="columna7">
                <ItemTemplate>
                     <asp:Label ID="lblTrailer" runat="server" Text='<%# Bind("Trailer") %>'></asp:Label>
                 </ItemTemplate>
             </asp:TemplateField>
             
             
             <asp:TemplateField  HeaderText="Cuadrilla" HeaderStyle-CssClass="columna7" ItemStyle-CssClass="columna7">
                <ItemTemplate>
                     <asp:Label ID="lblcuadrilla" runat="server" Text='<%# Bind("Cuadrilla") %>'></asp:Label>
                 </ItemTemplate>
             </asp:TemplateField>
             
             
             
             <asp:TemplateField  HeaderText="Destino" HeaderStyle-CssClass="columna7" ItemStyle-CssClass="columna7">
                <ItemTemplate>
                     <asp:Label ID="lblDestino" runat="server" Text='<%# Bind("Destino") %>'></asp:Label>
                 </ItemTemplate>
             </asp:TemplateField>
             <asp:TemplateField  HeaderText="Pozo" HeaderStyle-CssClass="columna1" ItemStyle-CssClass="columna1">
                <ItemTemplate>
                     <asp:Label ID="lblPozoDestino" runat="server" Text='<%# Bind("PozoDestino") %>'></asp:Label>
                 </ItemTemplate>
             </asp:TemplateField>
             <asp:TemplateField  HeaderText="Unir SS" HeaderStyle-CssClass="columna1" ItemStyle-CssClass="columna1">
               <HeaderTemplate>
                    Unir
                    <asp:CheckBox ID="chkUnirH" runat="server" ToolTip="Para seleccionar todas las Solicitudes" onchange="return selectTodos();" ></asp:CheckBox>
               </HeaderTemplate>
                <ItemTemplate>
                    <asp:HiddenField runat="server" ID="hfidSolicitud" Value='<%# Eval("idSolicitud") %>' />
                     <asp:CheckBox ID="chkUnir" runat="server" ToolTip="Click para unir o seleccionar OTI" ></asp:CheckBox>
                 </ItemTemplate>
             </asp:TemplateField> 
             <asp:TemplateField  ItemStyle-CssClass="btn_iconos" HeaderStyle-CssClass="btn_iconos">
                <ItemTemplate>
                     <asp:HiddenField ID="hfIdOT" runat="server" value='<%# Bind("idOrden") %>'/>
                     <asp:HiddenField ID="hfidVehiculo" runat="server" value='<%# Bind("idVehiculo") %>'/>
                     <asp:LinkButton ID="btnOT" runat="server" title="OTI" CssClass="oti" CommandArgument='<%# Bind("idSolicitud") %>' CommandName="OTI" OnClientClick="return validarCheck();" ></asp:LinkButton>
                 </ItemTemplate>
             </asp:TemplateField>
             <asp:TemplateField ItemStyle-CssClass="btn_iconos" HeaderStyle-CssClass="btn_iconos">
                <ItemTemplate>
                     <asp:LinkButton ID="btnModificar" runat="server" title="Editar SS" CssClass="editar" CommandArgument='<%# Bind("idSolicitud") %>' CommandName="Modificar"></asp:LinkButton>
                 </ItemTemplate>
             </asp:TemplateField>               
             <asp:TemplateField  ItemStyle-CssClass="btn_iconos" HeaderStyle-CssClass="btn_iconos">
                <ItemTemplate>
                     <asp:LinkButton ID="btnRegreso" runat="server" title="Cargar Regreso" CssClass="regresar" CommandArgument='<%# Bind("idSolicitud") %>' CommandName="Regreso"></asp:LinkButton>
                 </ItemTemplate>
             </asp:TemplateField>
             <asp:TemplateField  ItemStyle-CssClass="btn_iconos" HeaderStyle-CssClass="btn_iconos">
                <ItemTemplate>                    
                    <asp:LinkButton ID="btnNuevoMov" runat="server" title="Pozo a Pozo" CssClass="pozo" CommandArgument='<%# Bind("idSolicitud") %>' CommandName="NuevoMovimiento"></asp:LinkButton>  
                 </ItemTemplate>
             </asp:TemplateField>
             <asp:TemplateField  ItemStyle-CssClass="btn_iconos" HeaderStyle-CssClass="btn_iconos">
                <ItemTemplate>
                     <asp:LinkButton ID="btnEliminar" runat="server" title="Eliminar SS" CssClass="eliminar" CommandArgument='<%# Bind("idSolicitud") %>' CommandName="Eliminar" OnClientClick="return Eliminar('la Solicitud');"></asp:LinkButton>
                 </ItemTemplate>
             </asp:TemplateField>
         </Columns>
     </asp:GridView>

</asp:Content>
