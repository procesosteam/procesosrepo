﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using ProcesoPP.Model;
using System.Configuration;
using ProcesoPP.Services;
using ProcesoPP.Business;

namespace ProcesoPP.UI.SSe3
{
    public partial class frmSSRemitoCarga : System.Web.UI.Page
    {
        #region Propiedades

        public int _idRemito
        {
            get { return (int)ViewState["idRemito"]; }
            set { ViewState["idRemito"] = value; }
        }
        public List<string> _idParte
        {
            get { return (List<string>)ViewState["idParte"]; }
            set { ViewState["idParte"] = value; }
        }

        public string _movimiento
        {
            get { return (string)ViewState["detalle"]; }
            set { ViewState["detalle"] = value; }
        }

        public string _cantMovimiento
        {
            get { return (string)ViewState["cantMovimiento"]; }
            set { ViewState["cantMovimiento"] = value; }
        }

        public string _alquiler
        {
            get { return (string)ViewState["alquiler"]; }
            set { ViewState["alquiler"] = value; }
        }
        public string _cantAlquiler
        {
            get { return (string)ViewState["cantAlquiler"]; }
            set { ViewState["cantAlquiler"] = value; }
        }

        public string _conn
        {
            get { return (string)ViewState["conn"]; }
            set { ViewState["conn"] = value; }
        }
        public string _accion
        {
            get { return (string)ViewState["accion"]; }
            set { ViewState["accion"] = value; }
        }
        public string _des
        {
            get { return (string)ViewState["des"]; }
            set { ViewState["des"] = value; }
        }

        private DataTable _dt
        {
            get { return (DataTable)ViewState["dt"]; }
            set { ViewState["dt"] = value; }
        }

        public Permisos _Permiso
        {
            get { return (Permisos)ViewState["permiso"]; }
            set { ViewState["permiso"] = value; }
        }

        public int _idUsuario
        {
            get { return (int)ViewState["idUsuario"]; }
            set { ViewState["idUsuario"] = value; }
        }
        public int _ADM
        {
            get { return int.Parse(ConfigurationManager.AppSettings["ADM"].ToString()); }
        }
        
        public int _IDMenos
        {
            get { return int.Parse(ConfigurationManager.AppSettings["MENOS"].ToString()); }
        }

        public int _IDMas
        {
            get { return int.Parse(ConfigurationManager.AppSettings["MAS"].ToString()); }
        }

        #endregion

        #region Eventos

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                _idUsuario = Common.EvaluarSession(Request, this.Page);
                if (_idUsuario != 0)
                {
                    _Permiso = Common.getModulo(Request, this.Page);
                    if (!IsPostBack)
                    {
                        InitScreen();
                    }
                }
                else
                {
                    Mensaje.errorMsj("DEBE LOGUEARSE", this.Page, "LOGIN", "../frmLogin.aspx");
                }
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        protected void gvDetalles_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "addRow":
                    addRow(e);
                    break;
                case "deleteRow":
                    deleteRow(e);
                    break;
            }
        }

        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                GuardarRemito();
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message + "\nSource: " + ex.Source + "\nMétodo: " + ex.TargetSite, this.Page, "Error", null);
            }
        }

        protected void gvDetalles_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                TextBox txtCantidad = e.Row.FindControl("txtCantidad") as TextBox;
                TextBox txtDetalle = e.Row.FindControl("txtDetalle") as TextBox;
                DropDownList ddlCodTango = e.Row.FindControl("ddlCodTangoI") as DropDownList;
                HiddenField hfCodTango = e.Row.FindControl("hfCodTango") as HiddenField;
                if (txtDetalle.Text.Split('\n').Count() > 3)
                    txtDetalle.Rows = txtDetalle.Text.Split('\n').Count();
                txtCantidad.Rows = txtDetalle.Rows;                
            }
            if (e.Row.RowType == DataControlRowType.Footer)
            {
                DropDownList ddlCodTango = e.Row.FindControl("ddlCodTangoI") as DropDownList;
                CargarCodTango(ddlCodTango, "");
            }
        }

        #endregion

        #region Metodos

        private void CargarCodTango(DropDownList ddlCodTango, string detalle)
        {
            TipoServicioBus oTsBus = new TipoServicioBus();
            if (detalle.Length > 0)
            {
                if (detalle.IndexOf("servicio") > 1)
                    detalle = detalle.Substring(detalle.IndexOf(":") + 2, 4);
                else
                    detalle = detalle.Substring(0, 4);
            }
            ddlCodTango.DataSource = oTsBus.CodTangoGetByFilter(detalle);
            ddlCodTango.DataTextField = "Descripcio";
            ddlCodTango.DataValueField = "cod_articu";
            ddlCodTango.DataBind();
            ddlCodTango.Items.Insert(0, new ListItem("Seleccione...", "0"));
        }

        private void InitScreen()
        {
            _idRemito = 0;
            _idParte = new List<string>();
            _des = string.Empty;
            _conn = string.Empty;
            _movimiento = string.Empty;
            _alquiler = string.Empty;
            _accion = string.Empty;

            CrearDT();
            if (Request["idParte"] != null)
            {
                _idParte = Request["idParte"].Split('-').ToList();
                CargarRemitoPorParte();
                //gvDetalles.Columns[0].Visible = true;
            }
            if (Request["accion"] != null)
            {
                _accion = Request["accion"];
            }
            if (_accion == "Ver")
            {
                btnGuardar.Visible = false;
            }
            if (Request["idRemito"] != null && int.Parse(Request["idRemito"]) != 0)
            {
                _idRemito = int.Parse(Request["idRemito"]);
                CargarRemitoById();
                gvDetalles.Columns[0].Visible = false;
            }
            else
            {
                SugerirNroRemito();
                CargarGrillaDetalle();
            }
            if (_accion.Equals("Bonificado"))
                gvDetalles.Columns[0].Visible = false;

        }

        private void CargarRemitoById()
        {
            Model.Remitos oRemito = new ProcesoPP.Model.Remitos();
            RemitosBus oRemitosBus = new RemitosBus();
            oRemito = oRemitosBus.RemitosGetById(_idRemito);
            txtFecha.Text = oRemito.Fecha.ToString("dd/MM/yyyy");
            txtObservacion.Text = oRemito.Observacion;
            lblNroRemito.Text = oRemito.NroRemito.ToString();
            txtCCosto.Text = oRemito.CCosto;
            txtNroOC.Text = oRemito.NroOC;

            foreach (RemitoDetalle oDetalle in oRemito.oListDetalles)
            {
                ParteEntrega oParte = (new ParteEntregaBus()).ParteEntregaGetById(oDetalle.IdParte);
                DataRow dr = _dt.NewRow();
                dr["idRemitoDetalle"] = oDetalle.IdRemitoDetalle;
                dr["SS"] = oParte.oOrdenTrabajo != null ? "SS: " + oParte.oOrdenTrabajo.oSolicitudCliente.NroPedidoCliente + "\r\n" + "PO: " + oParte.NroParteEntrega : "";
                dr["Cantidad"] = oDetalle.Cantidad;
                dr["detalle"] = oDetalle.Detalle;
                dr["PU"] = oDetalle.PrecioUnit;
                dr["Total"] = oDetalle.PrecioTotal;
                dr["idParte"] = oDetalle.IdParte;
                _dt.Rows.Add(dr);
            }
            if (oRemito.Impreso && _Permiso.oTipoUsuario.idTipoUsuario == _ADM && _accion != "Ver")
            {
                btnGuardar.Visible = true;
                btnGuardar.Text = "Re-Imprimir";
            }
            BindGrid();
        }

        private void GuardarRemito()
        {
            Model.Remitos oRemito = new ProcesoPP.Model.Remitos();
            RemitoDetalle oDetalle = new RemitoDetalle();
            RemitosBus oRemitosBus = new RemitosBus();
            ParteEntregaBus oParteEntregaBus = new ParteEntregaBus();
            DateTime ultimaFecha = oRemitosBus.RemitosGetUltimaFecha();
            List<RemitoDetalleTango> oListDetTango = new List<RemitoDetalleTango>();
                        
            oRemito.oParte = oParteEntregaBus.ParteEntregaGetById(oRemito.oParte.IdParteEntrega);
            oRemito.IdUsuario = _idUsuario;
            oRemito.Impreso = false;
            oRemito.Anulado = false;
            oRemito.NroRemito = int.Parse(lblNroRemito.Text);
            oRemito.Observacion = txtObservacion.Text.ToUpper();
            oRemito.Cantidad = 1;
            oRemito.Fecha = DateTime.Parse(txtFecha.Text);
            oRemito.NroOC = txtNroOC.Text;
            oRemito.CCosto = txtCCosto.Text;

            int nRenglon = 1;

            foreach (GridViewRow row in gvDetalles.Rows)
            {

                HiddenField hfIdRemitoDetalle = row.FindControl("hfIdRemitoDetalle") as HiddenField;
                Label lblSS = row.FindControl("lblSS") as Label;
                TextBox txtCantidad = row.FindControl("txtCantidad") as TextBox;
                TextBox txtDetalle = row.FindControl("txtDetalle") as TextBox;
                TextBox txtPU = row.FindControl("txtPU") as TextBox;
                TextBox txtTotal = row.FindControl("txtTotal") as TextBox;
                Label lblidParte = row.FindControl("lblidParte") as Label;

                oDetalle = new RemitoDetalle();
                oDetalle.Detalle = txtDetalle.Text;
                oDetalle.IdRemitoDetalle = int.Parse(hfIdRemitoDetalle.Value);
                oDetalle.Cantidad = int.Parse(txtCantidad.Text);
                oDetalle.PrecioUnit = decimal.Parse(txtPU.Text);
                oDetalle.PrecioTotal = decimal.Parse(txtTotal.Text);
                oDetalle.IdParte = lblidParte.Text.Equals(string.Empty)? oRemito.oParte.IdParteEntrega : int.Parse(lblidParte.Text);

                oRemito.oListDetalles.Add(oDetalle);
                if (!_accion.Equals("Bonificado"))
                    oListDetTango.AddRange(CargarDetalleTango(row, ref nRenglon));

            }

            if (_idRemito == 0)
            {
                if (ultimaFecha <= oRemito.Fecha)
                {
                    _idRemito = oRemitosBus.RemitosAdd(oRemito, oListDetTango);
                }
                else
                    Mensaje.errorMsj("Debe ingresar una fecha mayor o igual al último remito", this.Page, "Fecha Invalida", null);
            }
            else
            {
                oRemito.IdRemito = _idRemito;
                oRemitosBus.RemitosUpdate(oRemito);
            }
            if (_idRemito > 0 && (!_accion.Equals("Bonificado")))
            {
                Mensaje.preguntaMsj("Remito Guardado! \n Desea Imprimir?", this.Page, "Exito", "../Reportes/frmRemitoRPT.aspx?idRemito=" + _idRemito.ToString());
                //Response.Redirect("frmRemitosGestion.aspx?idModulo=8");
            }
            if (_accion.Equals("Bonificado"))
            {
                Mensaje.successMsj("Remito Bonificado con éxito!", this, "Exito", "frmSSRemitoGestion.aspx?idModulo=8");
            }

        }

        private List<RemitoDetalleTango> CargarDetalleTango(GridViewRow row, ref int nRenglon)
        {
            List<RemitoDetalleTango> oListDetalle = new List<RemitoDetalleTango>();
            RemitoDetalleTango oDetalle = new RemitoDetalleTango();
            TextBox txtCantidad = row.FindControl("txtCantidad") as TextBox;
            TextBox txtDetalle = row.FindControl("txtDetalle") as TextBox;
            TextBox txtPU = row.FindControl("txtPU") as TextBox;
            TextBox txtTotal = row.FindControl("txtTotal") as TextBox;
            //DropDownList ddlCodTango = row.FindControl("ddlCodTangoI") as DropDownList;
            Label hfCodTango = row.FindControl("hfCodTango") as Label;

            string[] descrip = txtDetalle.Text.Split('\n');

            for (int i = 0; i < descrip.Length; i++)
            {
                if (descrip[i] != string.Empty)
                {
                    string renglon = descrip[i];

                    for (int pos = 0; pos < renglon.Length; pos = pos + 30)
                    {
                        oDetalle = new RemitoDetalleTango();
                        string linea = renglon.Substring(pos).Length > 30 ? renglon.Substring(pos, 30) : renglon.Substring(pos);
                        oDetalle.Descripcion = linea;

                        if (renglon.Length > 30)
                        {
                            oDetalle.Descripcion = renglon.Substring(0, 30);
                            oDetalle.DescripAdic = renglon.Substring(30);
                        }
                        else
                        {
                            oDetalle.Descripcion = renglon;
                        }

                        oDetalle.nRenglon = nRenglon;
                        oDetalle.nroRemito = lblNroRemito.Text;
                        if (i == 0 && pos == 0)
                        {
                            oDetalle.cantidad = int.Parse(txtCantidad.Text);
                            oDetalle.PrecioUnit = decimal.Parse(txtPU.Text);
                            oDetalle.PrecioTotal = decimal.Parse(txtTotal.Text);
                            oDetalle.codTango = hfCodTango.Text;
                        }
                        oListDetalle.Add(oDetalle);
                        nRenglon++;
                    }
                }

            }

            return oListDetalle;
        }

        private void imprimir()
        {

        }

        private void deleteRow(GridViewCommandEventArgs e)
        {
            int indexRow = int.Parse(e.CommandArgument.ToString());
            _dt.Rows.RemoveAt(indexRow);
            BindGrid();
        }

        private void addRow(GridViewCommandEventArgs e)
        {
            revisarGrilla();

            GridViewRow row = (GridViewRow)((ImageButton)e.CommandSource).NamingContainer;
            TextBox txtCant = row.FindControl("txtCantidad") as TextBox;
            Label lblSS = row.FindControl("lblSS") as Label;
            TextBox txtDetalle = row.FindControl("txtDetalle") as TextBox;
            TextBox txtPU = row.FindControl("txtPU") as TextBox;
            TextBox txtTotal = row.FindControl("txtTotal") as TextBox;
            DropDownList ddlCodTango = row.FindControl("ddlCodTangoI") as DropDownList;

            DataRow dr = _dt.NewRow();
            dr["idRemitoDetalle"] = 0;
            dr["Cantidad"] = txtCant.Text.Length == 0 ? "0" : txtCant.Text;
            dr["SS"] = lblSS.Text;
            dr["detalle"] = txtDetalle.Text;
            dr["PU"] = txtPU.Text;
            dr["Total"] = txtTotal.Text;
            dr["codTango"] = ddlCodTango.SelectedValue;

            if (txtDetalle.Text.Length > 0)
            {
                _dt.Rows.Add(dr);
                BindGrid();
            }
            else
                Mensaje.errorMsj("Debe ingresar detalle para agregar la fila", this.Page, "Error", null);

        }

        private void revisarGrilla()
        {
            _dt.Rows.Clear();
            foreach (GridViewRow row in gvDetalles.Rows)
            {
                TextBox txtCant = row.FindControl("txtCantidad") as TextBox;
                Label lblSS = row.FindControl("lblSS") as Label;
                TextBox txtDetalle = row.FindControl("txtDetalle") as TextBox;
                TextBox txtPU = row.FindControl("txtPU") as TextBox;
                TextBox txtTotal = row.FindControl("txtTotal") as TextBox;
                DropDownList ddlCodTango = row.FindControl("ddlCodTangoI") as DropDownList;
                Label hfCodTango = row.FindControl("hfCodTango") as Label;
                Label lblidParte = row.FindControl("lblidParte") as Label;

                DataRow dr = _dt.NewRow();
                dr["idRemitoDetalle"] = 0;
                dr["idParte"] = lblidParte.Text.Equals(string.Empty) ? "0" : lblidParte.Text;
                dr["Cantidad"] = txtCant.Text.Length == 0 ? "0" : txtCant.Text;
                dr["SS"] = lblSS.Text;
                dr["detalle"] = txtDetalle.Text;
                dr["PU"] = txtPU.Text;
                dr["Total"] = txtTotal.Text;
                dr["codTango"] = hfCodTango.Text;

                if (hfCodTango != null && ddlCodTango != null)
                {                   
                    hfCodTango.Text = ddlCodTango.SelectedValue;
                }
                if (txtDetalle.Text.Length > 0)
                {
                    _dt.Rows.Add(dr);
                }
            }
        }

        private void CrearDT()
        {
            _dt = new DataTable();
            _dt.Columns.Add("idRemitoDetalle");
            _dt.Columns.Add("SS");
            _dt.Columns.Add("Cantidad");
            _dt.Columns.Add("Detalle");
            _dt.Columns.Add("PU");
            _dt.Columns.Add("Total");
            _dt.Columns.Add("codTango");
            _dt.Columns.Add("idParte");
        }

        private void CargarDetalle(int cant, string nroSS, string detalle, string pu, string total, string codTango, int idParte)
        {

            DataRow dr = _dt.NewRow();
            dr["idRemitoDetalle"] = 0;
            dr["SS"] = nroSS;
            dr["Cantidad"] = cant;
            dr["detalle"] = detalle.ToUpper();
            dr["PU"] = pu;
            dr["Total"] = total;
            dr["codTango"] = codTango;
            dr["idParte"] = idParte;
            _dt.Rows.Add(dr);
            BindGrid();
        }

        private void BindGrid()
        {
            gvDetalles.DataSource = _dt;
            gvDetalles.DataBind();

        }

        private void SugerirNroRemito()
        {
            if (_accion.Equals("Bonificado"))
            {
                lblNroRemito.Text = "0";
            }
            else
            {
                RemitosBus oRemitosBus = new RemitosBus();
                lblNroRemito.Text = oRemitosBus.RemitoGetNroRemito().ToString();
            }
        }

        private void CargarRemitoPorParte()
        {
            
            ParteEntregaBus oParteEntregaBus = new ParteEntregaBus();
            ParteEntrega oParte = new ParteEntrega();

            oParte = oParteEntregaBus.ParteEntregaGetById(int.Parse(_idParte.First()));
            txtFecha.Text = DateTime.Today.ToString("dd/MM/yyyy");
            //DATOS DEL PRIMER PARTE
            lblCUIT.Text = oParte.oOrdenTrabajo.oSolicitudCliente.oSector.oEmpresa.CUIT;
            lblDomicilio.Text = oParte.oOrdenTrabajo.oSolicitudCliente.oSector.oEmpresa.Domicilio;
            lblEmpresa.Text = oParte.oOrdenTrabajo.oSolicitudCliente.oSector.oEmpresa.RazonSocial;
            lblIVA.Text = oParte.oOrdenTrabajo.oSolicitudCliente.oSector.oEmpresa.CondIVA;
            lblSector.Text = oParte.oOrdenTrabajo.oSolicitudCliente.oSector.Descripcion;
            if (oParte.oOrdenTrabajo.oSolicitudCliente.oSolicitante != null)
            {
                lblSolicitante.Text = oParte.oOrdenTrabajo.oSolicitudCliente.oSolicitante.Apellido;
            }
          
        }

        private void CargarGrillaDetalle()
        {
            AcuerdoBus oAcuerdoBus = new AcuerdoBus();
            ParteEntregaBus oParteEntregaBus = new ParteEntregaBus();
            ParteEntrega oParte = new ParteEntrega();

            oParte = oParteEntregaBus.ParteEntregaGetById(int.Parse(_idParte.First()));
            DataTable dtAcuerdo = oAcuerdoBus.AcuerdoGetByCliente(oParte.oOrdenTrabajo.oSolicitudCliente);
            if (dtAcuerdo.Rows.Count > 0)
            {
                foreach (string idParte in _idParte)
                {
                    if (idParte != string.Empty)
                    {
                        oParte = oParteEntregaBus.ParteEntregaGetById(int.Parse(idParte));

                        string det = oParte.oOrdenTrabajo.oSolicitudCliente.oTipoServicio.Descripcion + " ";
                        int cant = 1;

                        List<DataRow> drDetalle = dtAcuerdo.Select("idTipoServicio=" + oParte.oOrdenTrabajo.oSolicitudCliente.IdTipoServicio.ToString()).ToList();
                        decimal pu = 0, total = 0;

                        if (oParte.oOrdenTrabajo.oSolicitudCliente.oTipoServicio.RequiereEquipo)
                        {
                            #region EQUIPO
                            cant = (new SolicitudClienteBus()).SolicitudClienteGetFechaRegreso(oParte.oOrdenTrabajo.oSolicitudCliente.NroSolicitud,
                                                                                           oParte.oOrdenTrabajo.oSolicitudCliente.IdSolicitud,
                                                                                           oParte.oOrdenTrabajo.oSolicitudCliente.Fecha);
                            cant = cant <= 0 ? 1 : cant;

                            //DETALLE ESPECIFICACION
                            oParte.oOrdenTrabajo.oSolicitudCliente.oListEspecificaciones.ForEach(e => det += "\n" + e.Descripcion + " ");
                           
                            //EQUIPO
                            if (oParte.oOrdenTrabajo.oVehiculo != null)
                                det += "\nEquipo: " + oParte.oOrdenTrabajo.oVehiculo.oVehiculoIdentificacion.oCodificacion.Codigo + " " + oParte.oOrdenTrabajo.oVehiculo.oVehiculoIdentificacion.NroCodificacion;
                            if (oParte.oOrdenTrabajo.oListOTIConcepto.Count>0)
                            {
                                int idEqCliente = 0;
                                if(int.TryParse(oParte.oOrdenTrabajo.oListOTIConcepto.First().Valor, out idEqCliente)){
                                    EquiposBus oEquipoBus = new EquiposBus();
                                    det += "\nEquipo: " + oEquipoBus.EquiposGetById(idEqCliente).Descripcion;
                                }
                            }
                            
                            //ACUERDO
                            if (oParte.oOrdenTrabajo.oSolicitudCliente.IdCondicion == (int)Condicion.id.diario)
                            {
                                DataRow drs = drDetalle.Find(d => d["Concepto"].ToString().Contains("Dia") && d["idEspecificacion"].ToString() == oParte.oOrdenTrabajo.oSolicitudCliente.oListEspecificaciones.First().IdEspecificaciones.ToString());
                                if (drs != null)
                                    pu = decimal.Parse(drs["Costo"].ToString());
                                if ((cant / 15) > 1)
                                {
                                    cant = 15 + (int)(Math.Truncate((double)(cant / 15)));
                                }
                            }
                            else
                            {
                                DataRow drs;
                                if (oParte.oOrdenTrabajo.oSolicitudCliente.oListEspecificaciones.Count > 0)
                                    drs = drDetalle.Find(d => d["Concepto"].ToString().Contains("Mens") && !d["Concepto"].ToString().Contains("Cant") && d["idEspecificacion"].ToString().Contains(oParte.oOrdenTrabajo.oSolicitudCliente.oListEspecificaciones[0].IdEspecificaciones.ToString()));
                                else                                
                                    drs = drDetalle.Find(d => d["Concepto"].ToString().Contains("Mens") && !d["Concepto"].ToString().Contains("Cant"));
                                
                                if (drs != null)
                                    pu = decimal.Parse(drs["Costo"].ToString());
                                total = pu;
                                cant = 1;
                            }

                            if (oParte.oOrdenTrabajo.oSolicitudCliente.IdCondicion == (int)Condicion.id.mensual)
                            {
                                det += "\n" + oParte.oOrdenTrabajo.oSolicitudCliente.oCondicion.Descripcion;
                            }
                            
                            #endregion
                        }
                        if (oParte.oOrdenTrabajo.oSolicitudCliente.oTipoServicio.RequiereTraslado)
                        {
                            #region TRASLADO

                            //DETALLE MOVIMIENTO
                            det += "\nDesde: " + oParte.oOrdenTrabajo.oSolicitudCliente.oMovimiento.oLugarOrigen.Descripcion + " " +
                                                        oParte.oOrdenTrabajo.oSolicitudCliente.oMovimiento.PozoOrigen;
                            det += "\nHasta: " + oParte.oOrdenTrabajo.oSolicitudCliente.oMovimiento.oLugarDestino.Descripcion + " " +
                                                        oParte.oOrdenTrabajo.oSolicitudCliente.oMovimiento.PozoDestino;
                            //KMS MOVIMIENTO
                            cant = oParte.oOrdenTrabajo.oSolicitudCliente.oMovimiento.KmsEstimados;

                            //ACUERDO
                            string codif = oParte.oOrdenTrabajo.oTransporte.oVehiculoIdentificacion.oCodificacion.Descripcion;
                            if (drDetalle.Exists(d => d["Concepto"].ToString().Contains(codif)))
                            {
                                /**COMPARO EL ID DE LA ESPECIFICACION QUE REQUIERE TRASLADO DE MENOS QUE 100 O MAS DE 100 KM**/
                                DataRow drs = null;
                                if (cant > 100)
                                {
                                    if (drDetalle.Exists(d => d["Concepto"].ToString().Contains(codif)
                                        && int.Parse(d["idEspecificacion"].ToString()) == _IDMas))
                                        drs = drDetalle.Find(d => d["Concepto"].ToString().Contains(codif) && int.Parse(d["idEspecificacion"].ToString()) == _IDMas);
                                }
                                else
                                {
                                    if (drDetalle.Exists(d => d["Concepto"].ToString().Contains(codif)
                                        && int.Parse(d["idEspecificacion"].ToString()) == _IDMenos))
                                    {
                                        drs = drDetalle.Find(d => d["Concepto"].ToString().Contains(codif) && int.Parse(d["idEspecificacion"].ToString()) == _IDMenos);
                                        det += "\nCant KMS: " + cant.ToString();
                                    }
                                    cant = 1;

                                }
                                if (drs != null)
                                    pu = decimal.Parse(drs["Costo"].ToString());
                                //VEHICULO
                                det += " \nVehiculo " + codif;
                                if (oParte.oOrdenTrabajo.oTransporte != null)
                                    det += "\nCodificación: " + oParte.oOrdenTrabajo.oTransporte.oVehiculoIdentificacion.oCodificacion.Codigo + " " + oParte.oOrdenTrabajo.oTransporte.oVehiculoIdentificacion.NroCodificacion;

                            }
                            #endregion
                        }
                        if (oParte.oOrdenTrabajo.oSolicitudCliente.oTipoServicio.RequierePersonal)
                        {
                            #region PERSONAL
                            //CANTIDAD
                            decimal pers = decimal.Parse(oParte.oVerificaParte.Select("descripcion like 'Personal'").First()["Cantidad"].ToString());
                            decimal cantHs = decimal.Parse(oParte.oVerificaParte.Select("descripcion like 'HHT'").First()["Cantidad"].ToString());
                            decimal cantHsE = decimal.Parse(oParte.oVerificaParte.Select("descripcion like 'HHE'").First()["Cantidad"].ToString());

                            //VALOR FIJO MENSUAL PARA EL PERSONAL
                            //SI EL CONTRATO TIENE UN VALOR FIJO MENSUAL Y LA SS ES MENSUAL, LE PONGO ESE VALOR FIJO
                            // SI LA SS ES DIARIA, LE PONGO EL VALOR HORAS HOMBRE TRABAJADAS....
                            if (drDetalle.Exists(d => d["Concepto"].ToString().Contains("VFM")))
                            {
                                DataRow drs = drDetalle.Find(d => d["Concepto"].ToString().Contains("VFM"));

                                pu = decimal.Parse(drs["Costo"].ToString());
                                cant = 1;
                            }
                            else if (drDetalle.Exists(d => d["Concepto"].ToString().Contains("HHT")) ||
                                     drDetalle.Exists(d => d["Concepto"].ToString().Contains("HHE")))
                            {
                                DataRow drs;
                                string det1 = det;
                                if (drDetalle.Exists(d => d["Concepto"].ToString().Contains("HHT")))
                                {
                                    drs = drDetalle.Find(d => d["Concepto"].ToString().Contains("HHT"));
                                    pu = decimal.Parse(drs["Costo"].ToString());
                                    cant = (int)(pers * cantHs);
                                    total = (pu * cant);

                                    det1 += "\nHORAS HOMBRE TRABAJADAS (HHT)";
                                    det1 += "\nRealizado " + oParte.oOrdenTrabajo.oSolicitudCliente.Fecha.ToString("dd/MM/yyyy");
                                    string ss1 = "SS: " + oParte.oOrdenTrabajo.oSolicitudCliente.NroPedidoCliente + "\r\n" + "PO: " + oParte.NroParteEntrega;
                                    CargarDetalle(cant, ss1, det1, pu.ToString(), total.ToString(), oParte.oOrdenTrabajo.oSolicitudCliente.oTipoServicio.CodTango, oParte.IdParteEntrega);
                                }
                                if (drDetalle.Exists(d => d["Concepto"].ToString().Contains("HHE")))
                                {
                                    drs = drDetalle.Find(d => d["Concepto"].ToString().Contains("HHE"));
                                    pu = decimal.Parse(drs["Costo"].ToString());
                                    cant = (int)(pers * cantHsE);
                                    det += "\nHORAS HOMBRE EN ESPERA (HHE)";
                                }
                            }
                            #endregion
                        }
                        if (oParte.oOrdenTrabajo.oSolicitudCliente.oTipoServicio.RequiereCuadrilla)
                        {
                            #region CUADRILLA
                            //CANTIDAD
                            decimal cantHs = decimal.Parse(oParte.oVerificaParte.Select("descripcion like 'HHT'").First()["Cantidad"].ToString());
                            decimal cantHsE = decimal.Parse(oParte.oVerificaParte.Select("descripcion like 'HHE'").First()["Cantidad"].ToString());

                            //VALOR FIJO MENSUAL PARA EL PERSONAL
                            //SI EL CONTRATO TIENE UN VALOR FIJO MENSUAL Y LA SS ES MENSUAL, LE PONGO ESE VALOR FIJO
                            // SI LA SS ES DIARIA, LE PONGO EL VALOR HORAS HOMBRE TRABAJADAS....
                            if (drDetalle.Exists(d => d["Concepto"].ToString().Contains("VFM")))
                            {
                                DataRow drs = drDetalle.Find(d => d["Concepto"].ToString().Contains("VFM"));
                                pu = decimal.Parse(drs["Costo"].ToString());
                                cant = 1;
                            }
                            else if (drDetalle.Exists(d => d["Concepto"].ToString().Contains("HHT")) ||
                                     drDetalle.Exists(d => d["Concepto"].ToString().Contains("HHE")))
                            {
                                DataRow drs;
                                string det1 = det;
                                if (drDetalle.Exists(d => d["Concepto"].ToString().Contains("HHT")))
                                {
                                    drs = drDetalle.Find(d => d["Concepto"].ToString().Contains("HHT"));
                                    pu = decimal.Parse(drs["Costo"].ToString());
                                    cant = (int)cantHs;
                                    total = (pu * cant);

                                    det1 += "\nHORAS HOMBRE TRABAJADAS (HHT)";
                                    det1 += "\nRealizado " + oParte.oOrdenTrabajo.oSolicitudCliente.Fecha.ToString("dd/MM/yyyy");
                                    string ss1 = "SS: " + oParte.oOrdenTrabajo.oSolicitudCliente.NroPedidoCliente + "\r\n" + "PO: " + oParte.NroParteEntrega;
                                    CargarDetalle(cant, ss1, det1, pu.ToString(), total.ToString(), "", oParte.IdParteEntrega);
                                }
                                if (drDetalle.Exists(d => d["Concepto"].ToString().Contains("HHE")))
                                {
                                    drs = drDetalle.Find(d => d["Concepto"].ToString().Contains("HHE"));
                                    pu = decimal.Parse(drs["Costo"].ToString());
                                    cant = (int)(cantHsE);
                                    det += "\nHORAS HOMBRE EN ESPERA (HHE)";
                                }
                            }
                            #endregion
                        }
                        if (oParte.oOrdenTrabajo.oSolicitudCliente.oTipoServicio.RequiereMateriales)
                        {                            
                            #region MATERIALES
                            foreach (EspecificacionTecnica oET in oParte.oOrdenTrabajo.oSolicitudCliente.oListEspecificaciones)
                            {
                                if (drDetalle.Exists(d => int.Parse(d["idEspecificacion"].ToString())== oET.IdEspecificaciones))
                                {
                                    string det1 = det;                                    
                                    string ss1 = "SS: " + oParte.oOrdenTrabajo.oSolicitudCliente.NroPedidoCliente + "\r\n" + "PO: " + oParte.NroParteEntrega;
                                    DataRow drs = drDetalle.Find(d => int.Parse(d["idEspecificacion"].ToString()) == oET.IdEspecificaciones);
                                    pu = decimal.Parse(drs["Costo"].ToString());
                                    cant = int.Parse(oParte.oVerificaParte.Select("descripcion like 'MAT'").First()["Cantidad"].ToString());
                                    total = (pu * cant);
                                    det1 += "\n" + oET.Descripcion;
                                    det1 += "\nRealizado " + oParte.oOrdenTrabajo.oSolicitudCliente.Fecha.ToString("dd/MM/yyyy");
                                    CargarDetalle(cant, ss1, det1, pu.ToString(), total.ToString(), oParte.oOrdenTrabajo.oSolicitudCliente.oTipoServicio.CodTango, oParte.IdParteEntrega);
                                }
                            }
                            //break;
                            #endregion
                        }
                        if (!oParte.oOrdenTrabajo.oSolicitudCliente.oTipoServicio.RequiereMateriales &&
                            !oParte.oOrdenTrabajo.oSolicitudCliente.oTipoServicio.RequiereCuadrilla &&
                            !oParte.oOrdenTrabajo.oSolicitudCliente.oTipoServicio.RequierePersonal &&
                            !oParte.oOrdenTrabajo.oSolicitudCliente.oTipoServicio.RequiereTraslado &&
                            !oParte.oOrdenTrabajo.oSolicitudCliente.oTipoServicio.RequiereEquipo)
                        {
                            #region CON/DESCON
                            
                            if(oParte.oVerificaParte.Select("descripcion like 'Desconexi%'").Count()>0){
                                cant = int.Parse(oParte.oVerificaParte.Select("descripcion like 'Desconexi%'").First()["Cantidad"].ToString());
                            }
                            else if (oParte.oVerificaParte.Select("descripcion like 'Conexi%'").Count() > 0)
                            {
                                cant = int.Parse(oParte.oVerificaParte.Select("descripcion like 'Conexi%'").First()["Cantidad"].ToString());
                            }

                            DataRow drs = drDetalle.Find(d => d["Concepto"].ToString().Contains("Costo"));
                            if (drs.ItemArray.Count() > 0)
                            {
                                pu = decimal.Parse(drs["Costo"].ToString());
                            }
                            
                            #endregion
                        }
                        total = (pu * cant);

                        if (oParte.oOrdenTrabajo.oSolicitudCliente.IdCondicion == (int)Condicion.id.diario)
                            det += "\nRealizado " + oParte.oOrdenTrabajo.oSolicitudCliente.Fecha.ToString("dd/MM/yyyy");

                        if (oParte.Observacion.Length > 0)
                        {
                            txtObservacion.Text += "Obs. PO N° " + oParte.NroParteEntrega + ": " + oParte.Observacion + "\n";
                        }
                        if (!(oParte.oOrdenTrabajo.oSolicitudCliente.oTipoServicio.RequierePersonal && _dt.Rows.Count == 1) && !(oParte.oOrdenTrabajo.oSolicitudCliente.oTipoServicio.RequiereMateriales))
                        {
                            string ss = "SS: " + oParte.oOrdenTrabajo.oSolicitudCliente.NroPedidoCliente + "\r\n" + "PO: " + oParte.NroParteEntrega;
                            CargarDetalle(cant, ss, det, pu.ToString(), total.ToString(), oParte.oOrdenTrabajo.oSolicitudCliente.oTipoServicio.CodTango, oParte.IdParteEntrega);
                        }
                    }
                }
            }
            else
            {
                Mensaje.errorMsj("No existe ningun acuerdo activo para este Cliente/ Sector", this, "Acuerdo", null);
            }
        }

        #endregion

    }
}