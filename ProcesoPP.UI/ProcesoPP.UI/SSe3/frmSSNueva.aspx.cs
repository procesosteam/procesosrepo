﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ProcesoPP.Model;
using ProcesoPP.Services;
using ProcesoPP.Business;
using System.Data;
using System.Configuration;
using Newtonsoft.Json;
using AjaxControlToolkit;
using System.Web.Services;
using System.Collections.Specialized;
using System.Web.Script.Services;
using System.Drawing;

namespace ProcesoPP.UI.SSe3
{
    public partial class frmSSNueva : System.Web.UI.Page
    {
        #region PROPIEDADES

        public int _idUsuario
        {
            get { return (int)ViewState["idUsuario"]; }
            set { ViewState["idUsuario"] = value; }
        }

        public int _idSolicitud
        {
            get { return (int)ViewState["idSolicitud"]; }
            set { ViewState["idSolicitud"] = value; }
        }

        public int _NroSolicitud
        {
            get { return (int)ViewState["nroSolicitud"]; }
            set { ViewState["nroSolicitud"] = value; }
        }

        public bool _nuevoMovimiento
        {
            get { return (bool)ViewState["nuevoMovimiento"]; }
            set { ViewState["nuevoMovimiento"] = value; }
        }

        public bool _regreso
        {
            get { return (bool)ViewState["regreso"]; }
            set { ViewState["regreso"] = value; }
        }

        public Permisos _Permiso
        {
            get { return (Permisos)ViewState["permiso"]; }
            set { ViewState["permiso"] = value; }
        }

        public bool _tieneOti
        {
            get { return (bool)ViewState["tieneoti"]; }
            set { ViewState["tieneoti"] = value; }
        }
        
        public string _accion
        {
            get { return (string)ViewState["accion"]; }
            set { ViewState["accion"] = value; }
        }

        public DataTable _oAcuerdo {
            get { return (DataTable)ViewState["oAcuerdo"]; }
            set { ViewState["oAcuerdo"] = value; }
        }
        #endregion

        #region EVENTOS
        
        protected void Page_Init(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                _idSolicitud = 0;
                _nuevoMovimiento = false;
                _regreso = false;
                _NroSolicitud = 0;
                _accion = string.Empty;

                if (Request["Accion"] != null)
                {
                    _accion = Request["Accion"];
                    if (_accion == "Regreso")
                    {
                        _regreso = true;
                    }
                }

                if (Request["idSolicitud"] != null)
                {
                    _idSolicitud = int.Parse(Request["idSolicitud"]);
                    if (Request["NuevoMovimiento"] != null)
                        _nuevoMovimiento = bool.Parse(Request["NuevoMovimiento"].ToString());
                    CargarSSById();
                }
               
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                _idUsuario = Common.EvaluarSession(Request, this.Page);
                if (_idUsuario != 0)
                {
                    _Permiso = Common.getModulo(Request, this.Page);
                    if (_Permiso != null && _Permiso.Lectura)
                    {
                        if (!IsPostBack)
                        {                         
                            InitScreen();
                        }
                    }
                    else
                    {
                        Mensaje.errorMsj("NO TIENE PERMISOS A ESTE FORMULARIO", this.Page, "SIN ACCESO", "../frmPrincipal.aspx");
                    }
                }
                else
                {
                    Mensaje.errorMsj("DEBE LOGUEARSE", this.Page, "LOGIN", "../frmLogin.aspx");
                }
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message + " | StackTrace: " + ex.StackTrace, this.Page, "Error", null);
            }
        }

        protected void Page_Unload(object sender, EventArgs e)
        {

        }

        protected void gvTransporte_Sorting(object sender, GridViewSortEventArgs e)
        {
        }
        protected void ddlSector_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {                
                CargarTipoServicios(ddlEmpresa.SelectedValue, ddlSector.SelectedValue);
            }
                catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message + " | StackTrace: " + ex.StackTrace, this.Page, "Error", null);
            }
        }

        protected void gvTipoServicio_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
                CargarRow(e.Row);
             
        }

        protected void gvEspecificaciones_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
                CrearHeader(e.Row);
            if (e.Row.RowType == DataControlRowType.DataRow)
                CrearMovimientoGrilla(e.Row);             
        }
        
        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                Guardar();
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message + " - Source: " + ex.Source, this.Page, "Error", null);
            }
        }

        protected void btnGuardarDisponibles_Click(object sender, EventArgs e)
        {
            try
            {
                if (((Button)sender).ID == btnGuardarDisponibles.ID )
                    GuardarTrailersDisponibles("frmSSGestion.aspx");

                if (((Button)sender).ID == btnIrOTI.ID)
                    GuardarTrailersDisponibles("frmSSOti.aspx");

            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

      
        #endregion

        #region METODOS

        #region COMBOSAJAX
                
        [WebMethod]
        public static CascadingDropDownNameValue[] GetEmpresa(string knownCategoryValues, string category)
        {
            EmpresaBus oEmpresaBus = new EmpresaBus();            
            List<CascadingDropDownNameValue> empresa = oEmpresaBus.GetDataEmpresa();
            return empresa.ToArray();
        }

        [WebMethod]
        public static CascadingDropDownNameValue[] GetSector(string knownCategoryValues, string category)
        {
            string idEmpresa = CascadingDropDown.ParseKnownCategoryValuesString(knownCategoryValues)["idEmpresa"];            
            SectorBus oSectorBus = new SectorBus();
            List<CascadingDropDownNameValue> sectores = oSectorBus.GetDataSector(int.Parse(idEmpresa));
            return sectores.ToArray();
        }

        [WebMethod]
        public static CascadingDropDownNameValue[] GetSolicitante(string knownCategoryValues, string category)
        {
            string idSector = CascadingDropDown.ParseKnownCategoryValuesString(knownCategoryValues)["idSector"];
            string idempresa = CascadingDropDown.ParseKnownCategoryValuesString(knownCategoryValues)["idEmpresa"];
            SolicitanteBus oSolicitanteBus = new SolicitanteBus();
            List<CascadingDropDownNameValue> Solicitantees = oSolicitanteBus.GetDataSolicitante(int.Parse(idSector), int.Parse(idempresa));
            return Solicitantees.ToArray();
        }

        #endregion

        /// <summary>
        /// Carga los tipos de servicios segun el cliente, viene con dos datatables relacionados, por un lado los tipo de 
        /// servicios y por el otro las especificaciones de esos servicios
        /// </summary>
        /// <param name="idEmpresa"></param>
        private void CargarTipoServicios(string idEmpresa, string idSector)
        {
            if (!idEmpresa.Equals("") && !idSector.Equals(""))
            {
                TipoServicioBus oTipoServicioBus = new TipoServicioBus();
                DataSet ds = oTipoServicioBus.TipoServicioGetByAcuerdoCliente(idEmpresa, idSector);
                gvTipoServicio.DataSource = ds;
                gvTipoServicio.DataBind();
                if (ds.Tables.Count > 0 && ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0)
                {                   
                    hfFechaInicioContrato.Value = ds.Tables[0].DefaultView.ToTable(true, "FechaInicio").Rows[0]["FechaInicio"].ToString();
                    hfFechaFinContrato.Value = ds.Tables[0].DefaultView.ToTable(true, "FechaFin").Rows[0]["FechaFin"].ToString();
                }
            }
        }

        private void SugerirNroSS()
        {
            SolicitudClienteBus oSolicitudClienteBus = new SolicitudClienteBus();
            txtNroSS.Text = oSolicitudClienteBus.SolicitudClienteSugerirNro().ToString();

            lblLetra.Text = Common.letra("", false);
        }

        private void InitScreen()
        {
            _idSolicitud = 0;
            _nuevoMovimiento = false;
            _NroSolicitud = 0;
            _regreso = false;
            _oAcuerdo = new DataTable();
            
            if (Request["Accion"] != null)
            {
                _accion = Request["Accion"];
                if (_accion == "Regreso")
                {
                    _regreso = true;
                }
            }

            if (Request["idSolicitud"] != null)
            {
                _idSolicitud = int.Parse(Request["idSolicitud"]);                
                if (Request["NuevoMovimiento"] != null)
                    _nuevoMovimiento = bool.Parse(Request["NuevoMovimiento"].ToString());
                                
            }
            else
            {
                SugerirNroSS();
            }
                        
        }

        private void CargarRow(GridViewRow row)
        {
            GridView gvEsp = (GridView)row.FindControl("gvEspecificaciones");
            DataView dvEsp = ((DataRowView)row.DataItem).CreateChildView("ParentChild");
            
            gvEsp.DataSource = dvEsp;
            gvEsp.DataBind();
                        
        }
     
        private void CrearHeader(GridViewRow row)
        {
 	       Label lblTipoServ = row.FindControl("lblTipoServ") as Label;
           lblTipoServ.Text = ((System.Web.UI.WebControls.Label)(row.Parent.Parent.Parent.Parent.FindControl("lblTipoServ"))).Text;

        }
        /// <summary>
        /// Crea la grilla de movivmiento, con los dropdown y textbox
        /// </summary>
        /// <param name="row">la fila donde se van a crear los controles</param>
        private void CrearMovimientoGrilla(GridViewRow row)
        {
            HiddenField hfRequiereTraslado = ((HiddenField)(row.Parent.Parent.Parent.Parent.FindControl("hfRequiereTraslado")));
            bool requiereTraslado = hfRequiereTraslado.Value.Equals(string.Empty) ? false : bool.Parse(hfRequiereTraslado.Value);
            if (requiereTraslado)
            {
                HiddenField hfIdTipoServicio = ((HiddenField)(row.Parent.Parent.Parent.Parent.FindControl("hfIdTipoServicio")));                
                HiddenField hfRequiereKms = ((HiddenField)(row.Parent.Parent.Parent.Parent.FindControl("hfRequiereKms")));
                HiddenField hfRequiereKmsTaco = ((HiddenField)(row.Parent.Parent.Parent.Parent.FindControl("hfRequiereKmsTaco")));
                HiddenField hfIdEspecificacion = ((HiddenField)(row.FindControl("hfIdEspecificacion")));
                HiddenField hfEspTraslado = ((HiddenField)(row.FindControl("hfEspTraslado")));

                CheckBox chkTipoServ = row.FindControl("chkEspecificacion") as CheckBox;

                bool espRequiereTraslado = hfEspTraslado.Value.Equals(string.Empty) ? false : bool.Parse(hfEspTraslado.Value);

                if ((hfIdTipoServicio.Value != string.Empty && requiereTraslado) && (hfIdEspecificacion.Value.Equals(string.Empty) || espRequiereTraslado))
                {
                    #region GvMovimiento
                    chkTipoServ.Visible = false;
                    row.Cells[1].Visible = false;

                    TableCell cell = new TableCell();
                    cell.Text = "MOVIMIENTO";
                    cell.CssClass = "nuevaSS_tdEsp";
                    row.Cells.Add(cell);

                    //FILA 1
                    cell = new TableCell();
                    Label lblOrigen = new Label();
                    lblOrigen.Text = "Origen";
                    cell.CssClass = "celda_ss1";
                    cell.Controls.Add(lblOrigen);
                    row.Cells.Add(cell);

                    cell = new TableCell();
                    DropDownList ddlOrigen = new DropDownList();
                    ddlOrigen.ID = "ddlOrigen";
                    cell.CssClass = "celda_ss2";
                    CargarLugares(ddlOrigen);
                    cell.Controls.Add(ddlOrigen);
                    row.Cells.Add(cell);

                    cell = new TableCell();
                    Label lblPzoO = new Label();
                    lblPzoO.Text = "Pozo/Eq";
                    cell.CssClass = "celda_ss1";
                    cell.Controls.Add(lblPzoO);
                    row.Cells.Add(cell);

                    TextBox txtPozoOrig = new TextBox();
                    txtPozoOrig.ID = "txtPozoOrig";
                    cell = new TableCell();
                    cell.CssClass = "celda_ss3";
                    cell.Controls.Add(txtPozoOrig);
                    row.Cells.Add(cell);

                    //FILA 2
                    cell = new TableCell();
                    Label lblDestino = new Label();
                    lblDestino.Text = "Destino";
                    cell.CssClass = "celda_ss1";
                    cell.Controls.Add(lblDestino);
                    row.Cells.Add(cell);

                    cell = new TableCell();
                    DropDownList ddlDestino = new DropDownList();
                    ddlDestino.ID = "ddlDestino";
                    cell.CssClass = "celda_ss2";
                    CargarLugares(ddlDestino);
                    cell.Controls.Add(ddlDestino);
                    row.Cells.Add(cell);

                    cell = new TableCell();
                    Label lblPzoDest = new Label();
                    lblPzoDest.Text = "Pozo/Eq";
                    cell.CssClass = "celda_ss1";
                    cell.Controls.Add(lblPzoDest);
                    row.Cells.Add(cell);

                    cell = new TableCell();
                    TextBox txtPozoDest = new TextBox();
                    txtPozoDest.ID = "txtPozoDest";
                    cell.CssClass = "celda_ss2";
                    cell.Controls.Add(txtPozoDest);
                    row.Cells.Add(cell);

                    cell = new TableCell();
                    Label lblkms = new Label();
                    lblkms.Text = "Kms Est.";
                    cell.CssClass = "celda_ss1";
                    cell.Controls.Add(lblkms);
                    row.Cells.Add(cell);

                    cell = new TableCell();
                    TextBox txtKms = new TextBox();
                    txtKms.ID = "txtKms";
                    txtKms.Width = 60;
                    cell.CssClass = "celda_ss1";
                    cell.Controls.Add(txtKms);

                    HiddenField hfKms = new HiddenField();
                    hfKms.ID = "hfKms_" + hfIdTipoServicio.Value;
                    cell.Controls.Add(hfKms);

                    row.Cells.Add(cell);

                    KilometrosGetAll(hfKms, hfIdTipoServicio.Value);
                    ddlOrigen.Attributes.Add("onchange", "getKms('" + ddlOrigen.ClientID + "','" + ddlDestino.ClientID + "','" + txtKms.ClientID + "','" + hfKms.ClientID + "');");
                    ddlDestino.Attributes.Add("onchange", "getKms('" + ddlOrigen.ClientID + "','" + ddlDestino.ClientID + "','" + txtKms.ClientID + "','" + hfKms.ClientID + "');");

                    if (hfRequiereKms != null && bool.Parse(hfRequiereKms.Value))
                    {
                        ddlOrigen.Enabled = false;
                    }
                    else if (hfRequiereKmsTaco != null && bool.Parse(hfRequiereKmsTaco.Value))
                    {
                        ddlOrigen.Enabled = true;
                    }
                    if (!_nuevoMovimiento)
                        txtPozoOrig.Enabled = false;
                    #endregion
                }
            }
        }

        private void CargarLugares(DropDownList ddl)
        {
            LugarBus oLugarBus = new LugarBus();
            oLugarBus.CargarLugar(ref ddl);
            ddl.Items.Insert(0, new ListItem("N/A", "0"));
        }

        protected void KilometrosGetAll(HiddenField hfKms, string idTipoServicio)
        {
            KilometrosBus oKilometrosBus = new KilometrosBus();
            hfKms.Value = JsonConvert.SerializeObject(oKilometrosBus.KilometrosGetAll(idTipoServicio));
        }
        
        #region GUARDAR

        private void Guardar()
        {
            DateTime fechaIni = DateTime.Parse(hfFechaInicioContrato.Value);
            DateTime fechaFin = DateTime.Parse(hfFechaFinContrato.Value);
            DateTime fecha = new DateTime();
            bool requiereCuadrilla = false;
            bool requiereEquipo = false;

            if (DateTime.TryParse(txtFecha.Text, out fecha))
            {
                if (fecha >= fechaIni && fechaFin >= fecha)
                {
                    SolicitudClienteBus oSSBus = new SolicitudClienteBus();
                    SolicitudCliente oSC = new SolicitudCliente();
                    SolicitudCliente oSCOrig = new SolicitudCliente();
                    List<EspecificacionTecnica> oListEsp = new List<EspecificacionTecnica>();

                    bool guarda = false;
                    if (_idSolicitud != 0)
                        oSCOrig = oSSBus.SolicitudClienteGetById(_idSolicitud);
                    if (oSCOrig.IdSolicitud == 0 || (oSCOrig.IdSolicitud != 0 && oSCOrig.Fecha < fecha))
                    {
                        int idMov = 0;
                        string ids = "";
                        string coma = "";
                        foreach (GridViewRow rowTS in gvTipoServicio.Rows)
                        {
                            GridView gvEspecifica = rowTS.FindControl("gvEspecificaciones") as GridView;
                            HiddenField hfIdTipoServ = rowTS.FindControl("hfIdTipoServicio") as HiddenField;
                            HiddenField hfAbreviacion = rowTS.FindControl("hfAbreviacion") as HiddenField;
                            int idTs = int.Parse(hfIdTipoServ.Value);
                            if (gvEspecifica.Rows.Count > 0)
                            {
                                CheckBox chkTipoServ = gvEspecifica.HeaderRow.FindControl("chkTipoServ") as CheckBox;
                                if (chkTipoServ.Checked)
                                {
                                    if (oSCOrig != null && oSCOrig.IdTipoServicio != 0 && oSCOrig.IdTipoServicio == idTs
                                        && !_nuevoMovimiento && !_regreso)
                                    {
                                        oSCOrig.Baja = true;
                                        oSSBus.SolicitudClienteDelete(oSCOrig.IdSolicitud);
                                    }
                                    oSC = new SolicitudCliente();
                                    oSC = CrearSS(oSC, idTs, hfAbreviacion.Value, oSCOrig.IdCondicion);
                                    if (oSC.oTipoServicio.RequiereCuadrilla)
                                        requiereCuadrilla = oSC.oTipoServicio.RequiereCuadrilla;
                                    if (oSC.oTipoServicio.RequiereEquipo && oSC.oTipoServicio.oEspecificacion.Exists(e => e.EquipoPropio))
                                        requiereEquipo = oSC.oTipoServicio.RequiereEquipo;
                                    oSC.oListEspecificaciones = CargarObjEspecificaciones(gvEspecifica);
                                    if (idMov != 0)
                                        oSC.IdMovimiento = idMov;

                                    oListEsp.AddRange(oSC.oListEspecificaciones);
                                    if (oSC.oTipoServicio.RequiereTraslado)
                                    {
                                        oSC.oMovimiento = CrearMovimiento(gvEspecifica);
                                    }
                                    oSC.IdSolicitud = oSSBus.SolicitudClienteAdd(oSC);
                                    if (oSC.IdSolicitud > 0)
                                    {
                                        ids += coma + oSC.IdSolicitud.ToString();
                                        coma = ",";
                                        guarda = true;
                                        idMov = oSC.IdMovimiento;
                                    }
                                    else
                                    {
                                        guarda = false;
                                        break;
                                    }
                                }
                            }
                        }
                        hfIds.Value = ids;
                        btnGuardar.Visible = false;

                        if (guarda)
                        {
                            ValidaPersonal(ids);
                            if ((requiereCuadrilla || requiereEquipo) && !_nuevoMovimiento && !_regreso)
                            {
                                CargarTrailer(oListEsp, ids);
                            }
                            else
                            {
                                Mensaje.successMsj("Solicitud Guardada con éxito", this, "Exito", "frmSSGestion.aspx?idModulo=" + _Permiso.oModulo.idModulo.ToString());
                            }
                        }
                    }
                    else
                    {
                        Mensaje.errorMsj("La fecha de la SS actual no debe ser anterior a la SS original.", this, "Fecha", null);
                    }
                }
                else
                {
                    Mensaje.errorMsj("No existe un contrato vigente en la fecha seleccionada", this, "Fecha", null);
                }
            }
        }

        private void ValidaPersonal(string idsSC)
        {
            foreach (string id in idsSC.Split(','))
            {
                SolicitudCliente oSC = (new SolicitudClienteBus()).SolicitudClienteGetById(int.Parse(id));

                //SI REQUIERE PERSONAL Y TIENE VALOR FIJO MENSUAL CONDICION MENSUAL, SI NO DIARIO
                if (oSC.oTipoServicio.RequierePersonal)
                {
                    _oAcuerdo = (new AcuerdoBus()).AcuerdoGetByCliente(oSC);
                    List<DataRow> drDetalle = _oAcuerdo.Select("idTipoServicio=" + oSC.IdTipoServicio.ToString()).ToList();
                    if (drDetalle.Exists(d => d["Concepto"].ToString().Contains("VFM")))
                    {
                        oSC.IdCondicion = (int)Condicion.id.mensual;
                    }
                    else
                        oSC.IdCondicion = (int)Condicion.id.diario;

                    (new SolicitudClienteBus()).SolicitudClienteUpdate(oSC);
                }
            }
        }

        private void CargarTrailer(List<EspecificacionTecnica> oListEsp, string idsSC)
        {
            DataTable dtAll = new DataTable();
            int contE = 0;
            foreach (string id in idsSC.Split(','))
            {
                SolicitudCliente oSC = (new SolicitudClienteBus()).SolicitudClienteGetById(int.Parse(id));
                
                //CONDICION MENSUAL
                //REVISAR SI LOS TRAILERS DEL ACUERDO ESTAN DISPONIBLES

                DataTable dt = validarAcuerdoTrailer(oListEsp, oSC);
                                
                if (dt == null && (oSC.oTipoServicio.RequiereCuadrilla || oSC.oTipoServicio.RequiereEquipo))
                {
                    if (oSC.oTipoServicio.RequiereEquipo)
                    {
                        contE++;
                        Mensaje.preguntaTrailerMsj("El cliente ha agotado sus equipos disponible. \n La condición de la Solicitud pasara a contar los días. Desea continuar?", this.Page, "Cambio de condición", "frmSSGestion.aspx?idModulo=" + _Permiso.oModulo.idModulo);
                        oSC.IdCondicion = (int)Condicion.id.diario;
                        (new SolicitudClienteBus()).SolicitudClienteUpdate(oSC);
                    }
                    TrailerBus oVehiculoBus = new TrailerBus();
                    if (!_nuevoMovimiento)
                        dt = oVehiculoBus.TrailerGetDisponibles(oListEsp, oSC.IdTipoServicio);
                    else
                        dt = oVehiculoBus.TrailerGetNoDisponibles(oListEsp, _NroSolicitud);
                }
                if (dt != null && (oSC.oTipoServicio.RequiereCuadrilla || oSC.oTipoServicio.RequiereEquipo))
                {
                    if (dtAll == null || dtAll.Rows.Count == 0)
                        dtAll = dt.Clone();
                    foreach (DataRow dr in dt.Rows)
                    {
                        dtAll.ImportRow(dr);
                    }
                }
            }
            gvTransporte.DataSource = dtAll;
            GridViewHelper helper = new GridViewHelper(this.gvTransporte);
            helper.RegisterGroup("TipoServicio", true, true);
            helper.GroupHeader += new GroupEvent(helper_GroupHeader);
            helper.ApplyGroupSort();
            gvTransporte.DataBind();

            if (_nuevoMovimiento && gvTransporte.HeaderRow != null && gvTransporte.HeaderRow.Cells[1] != null)
                gvTransporte.HeaderRow.Cells[1].Text = "EQUIPOS NO DISPONIBLES";

            if (dtAll != null && dtAll.Rows.Count > 0)
            {
                if (contE > 1)
                    btnIrOTI.Visible = false;
                Mensaje.mostrarPopUp(this);
            }
        }

        private DataTable validarAcuerdoTrailer(List<EspecificacionTecnica> oListEsp, SolicitudCliente OSC) 
        {
            AcuerdoBus oAcuerdoBus = new AcuerdoBus();
            //TRAIGO LA CANTIDAD ASIGNADA EN EL ACUERDO Y LA CANTIDAD DE EQUIPOS ESPECIFICOS.
            DataTable dtCant = oAcuerdoBus.AcuerdoCantTrailer(OSC.oSector.IdEmpresa.ToString(), OSC.IdSector.ToString(), oListEsp);
            if (dtCant != null && dtCant.Rows.Count>0)
            {
                int cantAsignados = 0;
                int cantAcuerdo = 0;
                int cantEnUso = 0;
                bool disponible = false;//DISPONIBLE --> SOLO PARA LOS QUE TIENE ASIGNADOS

                if (int.TryParse(dtCant.Rows[0]["cantAsignados"].ToString(), out cantAsignados) &&
                    int.TryParse(dtCant.Rows[0]["cantAcuerdo"].ToString(), out cantAcuerdo) &&
                    int.TryParse(dtCant.Rows[0]["cantTrailerUso"].ToString(), out cantEnUso))
                {
                    disponible = bool.Parse(dtCant.Rows[0]["disponible"].ToString());
                    if (cantAcuerdo == cantAsignados && disponible)
                    {
                        //OBTENGO LOS TRAILERS MENSUALIZADOS DISPONIBLES
                        AcuerdoConceptoTrailerBus oAcuerdoVehiculoBus = new AcuerdoConceptoTrailerBus();
                        DataTable dt = oAcuerdoVehiculoBus.AcuerdoConceptoTrailerGetByIdCliente(OSC.oSector.IdEmpresa.ToString(), OSC.IdSector.ToString(), oListEsp);
                        return dt;
                    }
                    else if(cantAsignados == 0 && cantEnUso < cantAcuerdo)
                    {
                        //SI NO TIENE TRAILERS ESPECIFICOS, OBTENGO TRAILER DISPONIBLE DE LAS ESP DEL ACUERDO
                        AcuerdoConceptoTrailerBus oAcuerdoVehiculoBus = new AcuerdoConceptoTrailerBus();
                        DataTable dt = oAcuerdoVehiculoBus.AcuerdoGetTrailerDisponibles(OSC.oSector.IdEmpresa.ToString(), OSC.IdSector.ToString(), oListEsp);
                        
                        return dt;
                    }
                }
            }
            
            return null;
            
        }

        private void helper_GroupHeader(string groupName, object[] values, GridViewRow row)
        {
            if (groupName == "TipoServicio")
            {
                row.CssClass = "nuevaSS_titleServ";
                row.Cells[0].Text = "&nbsp;&nbsp;" + row.Cells[0].Text;
            }            
        }

        private Movimiento CrearMovimiento(GridView gvEspecifica)
        {
            Movimiento oMov = new Movimiento();            
            oMov = JsonConvert.DeserializeObject<Movimiento>(hfMov.Value);
            
            return oMov;
        }

        private List<EspecificacionTecnica> CargarObjEspecificaciones(GridView gvEspecifica)
        {
            List<EspecificacionTecnica> oListET = new List<EspecificacionTecnica>();

            foreach (GridViewRow rowEsp in gvEspecifica.Rows)
            {
                HiddenField hfIdEsp = rowEsp.FindControl("hfIdEspecificacion") as HiddenField;
                CheckBox chkEsp = rowEsp.FindControl("chkEspecificacion") as CheckBox;
                if (chkEsp.Checked)
                {
                    EspecificacionTecnica oET = new EspecificacionTecnica();
                    oET.IdEspecificaciones = int.Parse(hfIdEsp.Value);
                    oListET.Add(oET);
                }
            }

            return oListET;
        }

        private SolicitudCliente CrearSS(SolicitudCliente oSC, int idTipoServicio, string Abrev, int idCondicion)
        {             
           
            oSC.Fecha = DateTime.Parse(txtFecha.Text);
            oSC.FechaAlta = DateTime.Now;
            int nro = 0;
            if (int.TryParse(txtNroSS.Text, out nro) || _accion == "Modificar")
            {
                _NroSolicitud = nro;               
            }
            else
            {               
                _NroSolicitud = int.Parse(txtNroSS.Text.Split('-')[0].Trim());
            }

            oSC.NroPedidoCliente = _NroSolicitud.ToString() + " - " + Abrev + " - " + lblLetra.Text; 
            oSC.NroSolicitud = _NroSolicitud;
            oSC.IdUsuario = _idUsuario;
            oSC.IdSolicitante = ddlSolicitante.SelectedValue == string.Empty ? 0 : int.Parse(ddlSolicitante.SelectedValue);
            oSC.IdSector = ddlSector.SelectedValue == string.Empty ? 0 : int.Parse(ddlSector.SelectedValue);
            oSC.oSector =  (new SectorBus()).SectorGetById(oSC.IdSector);
            oSC.Entrega = !_regreso;
            oSC.nuevoMovimiento = _nuevoMovimiento;            
            oSC.IdTipoServicio = idTipoServicio;
            oSC.oTipoServicio = (new TipoServicioBus()).TipoServicioGetById(idTipoServicio);
            if (idCondicion == 0)                
                oSC.IdCondicion = (int)Condicion.id.mensual;
            else
                oSC.IdCondicion = idCondicion;
            oSC.idSolicitudOrig = _idSolicitud;

            return oSC;

        }
        
        #endregion

        #region CARGASS
        
        private void CargarSSById()
        {
            SolicitudCliente oSC = new SolicitudCliente();
            SolicitudClienteBus oSolicitudClienteBus = new SolicitudClienteBus();

            oSC = oSolicitudClienteBus.SolicitudClienteGetById(_idSolicitud);
            hfIds.Value = _idSolicitud.ToString();
            
            txtFecha.Text = oSC.Fecha.ToString("dd/MM/yyyy");
            if (_nuevoMovimiento || oSC.NroPedidoCliente == string.Empty)
            {
                txtNroSS.Text = oSC.NroSolicitud.ToString();
                lblLetra.Text = Common.letra(oSC.NroPedidoCliente.Split('-')[2].Trim(), true);
            }
            else
            {
                if (_regreso)
                {
                    txtNroSS.Text = oSC.NroSolicitud.ToString() + " - Z";
                    lblLetra.Text = Common.letra("Y", true);
                }
                else
                {
                    if (_accion.Contains("Modif"))
                    {
                        txtNroSS.Text = oSC.NroSolicitud.ToString();
                        lblLetra.Text = oSC.letra;
                    }
                    else
                    {
                        txtNroSS.Text = oSC.NroPedidoCliente;
                        lblLetra.Text = Common.letra(oSC.NroPedidoCliente.Split('-')[2].Trim(), false);
                    }
                }
                
            }

            cddEmpresa.SelectedValue = oSC.oSector.IdEmpresa.ToString();
            cddSector.SelectedValue = oSC.IdSector.ToString();
            ccdSolicitante.SelectedValue = oSC.IdSolicitante.ToString();

            CargarTipoServicios(oSC.oSector.IdEmpresa.ToString(), oSC.IdSector.ToString());
            CargarEspecSS(oSC);
            
            //DE PRUEBA
            //CargarTrailer(oSC.oListEspecificaciones, oSC);
                       
        }
        /// <summary>
        /// Carga, segun el tipo de servicio chequeado, las especificaciones para chequear.
        /// </summary>
        /// <param name="idTipoServicio"></param>
        /// <param name="oListET"></param>
        /// <param name="oMov"></param>
        public void CargarEspecSS( SolicitudCliente oSC)
        {
            int idTipoServicio = oSC.IdTipoServicio;
            List<EspecificacionTecnica> oListET = oSC.oListEspecificaciones;

            foreach (GridViewRow rowTS in gvTipoServicio.Rows)
            {
                GridView gvEspecifica = rowTS.FindControl("gvEspecificaciones") as GridView;
                HiddenField hfIdTipoServ = rowTS.FindControl("hfIdTipoServicio") as HiddenField;
                int idHFTipoS = int.Parse(hfIdTipoServ.Value);
                if (gvEspecifica.Rows.Count > 0)
                {   
                    if (idTipoServicio == idHFTipoS)
                    {
                        CheckBox chkTipoServ = gvEspecifica.HeaderRow.FindControl("chkTipoServ") as CheckBox;
                        chkTipoServ.Checked = true;
                        foreach (GridViewRow rowEsp in gvEspecifica.Rows)
                        {
                            HiddenField hfIdEsp = rowEsp.FindControl("hfIdEspecificacion") as HiddenField;
                            CheckBox chkEsp = rowEsp.FindControl("chkEspecificacion") as CheckBox;
                            if (hfIdEsp.Value != string.Empty)
                            {
                                int idHFET = int.Parse(hfIdEsp.Value);

                                if (oListET.Exists(et => et.IdEspecificaciones == idHFET))
                                {
                                    chkEsp.Checked = true;
                                }
                                if (oSC.oTipoServicio.RequiereTraslado)
                                {
                                    CargarMovimientoGrilla(rowEsp, oSC.oMovimiento);
                                }
                            }
                        }
                    }
                }
                TipoServicio oHFTs = (new TipoServicioBus()).TipoServicioGetById(idHFTipoS);
                if (oHFTs.RequiereTraslado)
                {
                    if (oListET.Count > 0 && oSC.oMovimiento != null && oSC.oMovimiento.IdMovimiento > 0)
                    {
                        TipoServicio oTS = (new SolicitudClienteBus()).SolicitudClienteGetByIdMovimiento(oSC.IdMovimiento);
                        gvEspecifica = rowTS.FindControl("gvEspecificaciones") as GridView;
                        if (gvEspecifica.Rows.Count > 0)
                        {
                            CheckBox chkTipoServ = gvEspecifica.HeaderRow.FindControl("chkTipoServ") as CheckBox;
                            if (_accion != "Modificar" && idHFTipoS == oTS.IdTipoServicio)
                                chkTipoServ.Checked = true;
                            foreach (GridViewRow rowEsp in gvEspecifica.Rows)
                            {
                                CargarMovimientoGrilla(rowEsp, oSC.oMovimiento);
                            }
                        }
                    }
                }
                
            }

        }

        private void CargarMovimientoGrilla(GridViewRow rowEsp, Movimiento oMov)
        {
            DropDownList ddlOrigen = rowEsp.FindControl("ddlOrigen") as DropDownList;
            DropDownList ddlDestino = rowEsp.FindControl("ddlDestino") as DropDownList;
            TextBox txtPozoDest = rowEsp.FindControl("txtPozoDest") as TextBox;
            TextBox txtPozoOrig = rowEsp.FindControl("txtPozoOrig") as TextBox;
            TextBox txtKms = rowEsp.FindControl("txtKms") as TextBox;

            if (oMov != null && ddlOrigen != null && ddlDestino != null)
            {
                ddlOrigen.SelectedValue = oMov.oLugarOrigen.IdLugar.ToString();
                txtPozoOrig.Text = oMov.PozoOrigen;
                ddlDestino.SelectedValue = oMov.oLugarDestino.IdLugar.ToString();
                txtPozoDest.Text = oMov.PozoDestino;
                txtKms.Text = oMov.KmsEstimados.ToString();
            }
            RedefinirPantallaNuevoMov(oMov, txtPozoDest, ddlDestino, txtPozoOrig, ddlOrigen);
        }

        private void RedefinirPantallaNuevoMov(Movimiento oMovimiento, TextBox txtPozoDestino, DropDownList ddlHasta, TextBox txtPozoOrigen, DropDownList ddlDesde)
        {
            if (_nuevoMovimiento)
            {
                _idSolicitud = 0;
                nuevoMovEnable(ddlDesde);
                if (txtPozoDestino != null)
                    txtPozoDestino.Text = string.Empty;
                if (ddlHasta != null)
                    ddlHasta.SelectedValue = oMovimiento.oLugarOrigen.IdLugar.ToString();
                if (txtPozoOrigen != null)
                    txtPozoOrigen.Text = oMovimiento.PozoDestino;
                if (ddlDesde != null)
                    ddlDesde.SelectedValue = oMovimiento.oLugarDestino.IdLugar.ToString();
                if (ddlHasta != null)
                    ddlHasta.SelectedIndex = 0;
                txtFecha.Text = string.Empty;
                ddlSector.SelectedIndex = 0;
                ddlSolicitante.SelectedIndex = 0;
                
            }
            else
            {
                divSolServ.InnerText = "Solicitud de Servicio";
                //_nuevoMovimiento = oSolicitudCliente.nuevoMovimiento;
                if (_nuevoMovimiento)
                    nuevoMovEnable(ddlDesde);
            }
            if (_regreso)
            {
                LugarBus oLugarBus = new LugarBus();
                oLugarBus.SelectedBPP(ref ddlHasta);
                ddlDesde.SelectedValue = oMovimiento.oLugarDestino.IdLugar.ToString();
                txtPozoOrigen.Text = oMovimiento.PozoDestino;
                regresoEnable(ddlHasta, ddlDesde, txtPozoOrigen, txtPozoDestino);
            }

        }

        private void regresoEnable(DropDownList ddlHasta, DropDownList ddlDesde, TextBox txtPozoOrigen, TextBox txtPozoDesdetino)
        {
            _idSolicitud = 0;
            txtFecha.Text = string.Empty;
            divSolServ.InnerText = "Regreso";
            ddlEmpresa.Enabled = false;            
            txtNroSS.Enabled = false;
            ddlSector.SelectedIndex = 0;
            ddlSolicitante.SelectedIndex = 0;

            if (ddlHasta != null)
                ddlHasta.Enabled = false;
            if (ddlDesde != null)
                ddlDesde.Enabled = false;
            if (txtPozoOrigen != null)
                txtPozoOrigen.Enabled = false;
            if (txtPozoDesdetino != null)
            {
                txtPozoDesdetino.Text = string.Empty;
                txtPozoDesdetino.Enabled = false;
            }
            
        }

        private void nuevoMovEnable(DropDownList ddlDesde)
        {
            divSolServ.InnerText = "Nuevo Movimiento";
            ddlEmpresa.Enabled = false;
            //gvTipoServicio.Enabled = false;
            txtNroSS.Enabled = false;
            if (ddlDesde != null)
                ddlDesde.Enabled = false;
        } 

        #endregion

        #region GUARDARTRAILER

        private void GuardarTrailersDisponibles(string redirect)
        {
            SolicitudTrailerPosibleBus oSTPBus = new SolicitudTrailerPosibleBus();
            ListSolicitudTrailerPosible oListSTP = new ListSolicitudTrailerPosible();
            
            oListSTP = JsonConvert.DeserializeObject<ListSolicitudTrailerPosible>(hfTrailers.Value);
            if (hfTrailers.Value != string.Empty)
            {
                oListSTP.oList.ForEach(t => t.IdSolicitud = _NroSolicitud);
                _idSolicitud = oSTPBus.SolicitudTrailerPosibleAdd(oListSTP.oList);
            }
            string idV = "";
            if (oListSTP.oList.Count > 0)
                idV = Common.encriptarURL(oListSTP.oList.First().IdTrailer.ToString());

            Mensaje.successMsj("Solicitud Guardada con éxito", this, "Exito", redirect + "?idModulo=" + _Permiso.oModulo.idModulo.ToString() + "&idSolicitud=" + Common.encriptarURL(hfIds.Value) + "&idVehiculo=" + idV);
            
        }

        #endregion

        #endregion

    }
}