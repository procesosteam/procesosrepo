﻿<%@ Page Title="SS - Remito" Language="C#" MasterPageFile="~/frmPrincipal.Master" AutoEventWireup="true" CodeBehind="frmSSRemitoCarga.aspx.cs" Inherits="ProcesoPP.UI.SSe3.frmSSRemitoCarga" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        $(document).ready(function () {
            SetMenu("liRemito");

            sumaTotal();
        });
    </script>
    <script type="text/javascript">
        function preguntar(msj, redirect) {
            var r = confirm(msj);
            if (r == true) {
                window.open(redirect, "REMITO", "width:100; height:100;");
                window.location.href = 'frmSSRemitoGestion.aspx?idModulo=8';
            }
        }

        function sumaTotal() {
            gv = document.getElementById('<%= gvDetalles.ClientID %>');
            lblTotal = document.getElementById('<%= lblTotal.ClientID %>');
            total = 0;
            for (var i = 1; i < gv.rows.length; i++) {
                row = gv.rows[i];
                txtTotal = row.cells[row.cells.length - 2].children[0];
                if (txtTotal.value.length > 0) {
                    total = parseFloat(total) + parseFloat(txtTotal.value.replace(",", "."))
                }
            }
            lblTotal.innerText = total;
            lblTotal.innerText = "$ " + lblTotal.innerText.replace(".", ",");
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div id="divSolServ" class="apertura" runat="server">Carga de Remito</div>
<div class="solicitud100">
<div class="sol_tit"><label>Fecha: </label></div>
<div class="sol_ver50g">
    <asp:TextBox runat="server" ID="txtFecha" ></asp:TextBox> 
    <img id="btnFecha" alt="calendario" src="../img/calendario.gif" style="width:20px; height:20px; cursor:pointer;" onmouseover="CambiarConfiguracionCalendario('txtFecha',this);"/>
</div>
<div class="sol_tit">
    <label>Remito Nº: </label></div>  
    <div class="sol_ver50g"><asp:Label runat="server" ID="lblNroRemito"></asp:Label> </div>
</div>

<div class="solicitud100">
<div class="sol_tit">
    <label>Empresa: </label></div>
    <div class="sol_ver50g"><asp:Label runat="server" ID="lblEmpresa"></asp:Label>  </div>
<div class="sol_tit"><label>Domicilio: </label></div>  
<div class="sol_ver50g"> <asp:Label runat="server" ID="lblDomicilio"></asp:Label>  </div>
</div>


<div class="solicitud100">
<div class="sol_tit"> <label>Condicion IVA: </label></div>
<div class="sol_ver50g"><asp:Label runat="server" ID="lblIVA"></asp:Label>  </div>


<div class="sol_tit2"><label>CUIT:</label></div>
<div class="sol_ver30g"><asp:Label runat="server" ID="lblCUIT"></asp:Label>  </div>
</div>

<div class="solicitud100">
    <asp:GridView runat="server" ID="gvDetalles" AutoGenerateColumns="false" ShowFooter="true"
        CssClass="tabla3" onrowcommand="gvDetalles_RowCommand" 
        onrowdatabound="gvDetalles_RowDataBound" >
        <Columns>
            <asp:TemplateField HeaderText="Cod. Tango" HeaderStyle-CssClass="r_tit15" ItemStyle-Width="50px" HeaderStyle-Width="50px" >
                <ItemTemplate>                    
                    <asp:Label runat="server" ID="hfCodTango" CssClass="cajaTextoGrilla" Text='<%# Eval("codTango") %>' />
                </ItemTemplate>
                <FooterTemplate>
                    <asp:DropDownList runat="server" ID="ddlCodTangoI" ></asp:DropDownList>                    
                </FooterTemplate>
            </asp:TemplateField>
            
            <asp:TemplateField HeaderText="Cantidad" HeaderStyle-CssClass="r_tit15" ItemStyle-Width="50px" HeaderStyle-Width="50px">
                <ItemTemplate>
                    <asp:TextBox runat="server" ID="txtCantidad" CssClass="cajaTextoGrilla" TextMode="MultiLine" Rows="4" Text='<%# Eval("Cantidad") %>' onkeypress="return ValidarNumeroEntero(event,this);"></asp:TextBox>
                </ItemTemplate>
                <FooterTemplate>
                    <asp:TextBox runat="server" ID="txtCantidad" CssClass="cajaTextoGrilla" onkeypress="return ValidarNumeroEntero(event,this);"></asp:TextBox>
                </FooterTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="idParte" Visible ="false">
                <ItemTemplate>
                    <asp:Label runat="server" ID="lblidParte" Text='<%# Eval("idParte") %>' CssClass="cajaTextoGrilla"  ></asp:Label>                    
                </ItemTemplate>
                <FooterTemplate>
                    <asp:Label runat="server" ID="lblidParte" Text='<%# Eval("idParte") %>' CssClass="cajaTextoGrilla"  ></asp:Label>
                </FooterTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="SS / PO" HeaderStyle-CssClass="r_tit15" ItemStyle-Width="70px" HeaderStyle-Width="70px" >
                <ItemTemplate>
                    <asp:Label runat="server" ID="lblSS" Text='<%# Eval("SS") %>' CssClass="cajaTextoGrilla"  ></asp:Label>                    
                </ItemTemplate>
                <FooterTemplate>
                    <asp:Label runat="server" ID="lblSS" Text='<%# Eval("SS") %>' CssClass="cajaTextoGrilla"  ></asp:Label>
                </FooterTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Detalle" HeaderStyle-CssClass="r_tit65">
                <ItemTemplate>
                    <asp:TextBox runat="server" ID="txtDetalle" CssClass="cajaTextoGrilla" style="text-transform:uppercase;" TextMode="MultiLine" Rows="4" Text='<%# Eval("detalle") %>'></asp:TextBox>
                </ItemTemplate>
                 <FooterTemplate>
                    <asp:TextBox runat="server" ID="txtDetalle" CssClass="cajaTextoGrilla" style="text-transform:uppercase;" TextMode="MultiLine" Rows="4"></asp:TextBox>                    
                </FooterTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Precio Unit." HeaderStyle-CssClass="r_tit15">
                <ItemTemplate>
                    <asp:TextBox runat="server" ID="txtPU" CssClass="cajaTextoGrilla" onkeypress="return ValidarNumeroDecimal(event,this,2);"  Text='<%# Eval("PU") %>'></asp:TextBox>
                </ItemTemplate>
                <FooterTemplate>
                    <asp:TextBox runat="server" ID="txtPU" CssClass="cajaTextoGrilla" onkeypress="return ValidarNumeroDecimal(event,this,2);" Text="0"></asp:TextBox>
                </FooterTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Total" HeaderStyle-CssClass="r_tit15" ItemStyle-HorizontalAlign="Right">
                <ItemTemplate>
                    <asp:TextBox runat="server" ID="txtTotal" CssClass="cajaTextoGrilla" onkeypress="return ValidarNumeroDecimal(event,this,2);" onblur="sumaTotal();" Text='<%# Eval("Total") %>' ></asp:TextBox>
                </ItemTemplate>
                <FooterTemplate>
                    <asp:TextBox runat="server" ID="txtTotal" CssClass="cajaTextoGrilla" Text="0" onkeypress="return ValidarNumeroDecimal(event,this,2);"  onblur="sumaTotal();"></asp:TextBox>
                </FooterTemplate>
            </asp:TemplateField>
            <asp:TemplateField  HeaderStyle-CssClass="r_tit15" ItemStyle-Width="20px" HeaderStyle-Width="20px">
                <ItemTemplate>
                    <asp:HiddenField runat="server" ID="hfIdRemitoDetalle" Value='<%# Eval("idRemitoDetalle") %>'  />
                    <asp:ImageButton ImageUrl="~/iconos/btn_menos.png" runat="server" ID="btnEliminar" ToolTip="Eliminar Fila" Height="19" Width="19" CommandName="deleteRow" CommandArgument='<% # Container.DataItemIndex %>'></asp:ImageButton>
                </ItemTemplate>
                 <FooterTemplate>                    
                    <asp:HiddenField runat="server" ID="hfIdRemitoDetalle" Value="0" />
                    <%--<input type="image" id="btnGuardar" title="Agregar fila" src="../iconos/btn_mas.png" onclick="addRow('gvDetalles', this);" />--%>
                    <asp:ImageButton runat="server" ID="btnGuardar" ToolTip="Agregar fila" ImageUrl="~/iconos/btn_mas.png" Height="19" Width="19" CommandName="addRow" ></asp:ImageButton>
                </FooterTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
    <div>
        <table class="tablaprinc">
            <tr style="font-weight:bold">
                <td class="r_tit65" style="width:72%"></td>
                <td class="r_tit15">
                    Total:
                </td>
                <td class="r_tit15">
                    <asp:Label runat="server" ID="lblTotal" Text="0"></asp:Label>
                </td>
                <td class="r_tit15"></td>                        
            </tr>
        </table>
    </div>
</div>

<div class="solicitud100b" style="height:80px;">
<div class="sol_observacion"  style="width:15%;"><label>Observación:</label></div>
<div class="sol_observacion" style="width:80%; font-family:Arial, Helvetica, sans-serif; "><asp:TextBox runat="server" ID="txtObservacion" Width="100%" TextMode="MultiLine" Rows="3" Font-Names="Arial" style="text-transform:uppercase;"></asp:TextBox>  </div>
</div>

<div class="solicitud100c">
    <div class="r_dato15">Sector</div>
    <div class="r_dato35"><asp:Label runat="server" ID="lblSector"></asp:Label> </div>

    <div class="r_dato15">Solicitante</div>
    <div class="r_dato35"><asp:Label runat="server" ID="lblSolicitante"></asp:Label> </div>
</div>


<div class="solicitud100c">
    <div class="r_dato15">C. Costos</div>
    <div class="r_dato35"><asp:TextBox runat="server" ID="txtCCosto"></asp:TextBox></div>

    <div class="r_dato15">OC Nº</div>
    <div class="r_dato35"><asp:TextBox runat="server" ID="txtNroOC"></asp:TextBox></div>
</div>

<div id="botones">
    <asp:Button ID="btnGuardar" cssclass="btn_form" Text="GUARDAR" runat="server" 
        OnClientClick="return ValidarObj('txtFecha|El campo fecha no debe estar  vacio.')"
        onclick="btnGuardar_Click" />
    <input type="button" id="btnVolver" value="VOLVER" class="btn_form" onclick="IrA('frmSSRemitoGestion.aspx?idModulo=8');"/>
</div>
</asp:Content>
