﻿<%@ Page Language="C#" MasterPageFile="~/frmPrincipal.Master" AutoEventWireup="true" CodeBehind="frmSSRemitoGestion.aspx.cs" EnableEventValidation="false" Inherits="ProcesoPP.UI.SSe3.frmSSRemitoGestion" Title="PP - Gestión de Remitos" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" >
        $(document).ready(function () { SetMenu("liRemito"); }); 
    </script>
     <script type="text/javascript">
         function validarCheck() {
             cont = 0;
             cliente = "";

             gv = document.getElementById('<%= gvSolicitud.ClientID %>');
             for (var i = 1; i < gv.rows.length; i++) {
                 row = gv.rows[i];
                 chkUnir = row.cells[9].children[1].children[0];
                 if (chkUnir.checked) {
                     if (cont == 0) {
                         cliente = row.cells[5].children[0].innerText;
                     }
                     else {
                         if (cliente != row.cells[5].children[0].innerText) {
                             cliente = "";                     
                             break;
                         }
                     }
                     cont++;

                 }
             }

             if (cont == 0 ) {
                 new Messi("Debe seleccionar uno a más Remitos.",
                        { title: 'Error', modal: true, titleClass: 'error', buttons: [{ id: 0, label: 'Aceptar', val: 'Y'}] });
                 return false;
             }
             else {
                 if (cliente == "") {
                     new Messi("Debe seleccionar Remitos del mismo cliente.",
                        { title: 'Error', modal: true, titleClass: 'error', buttons: [{ id: 0, label: 'Aceptar', val: 'Y'}] });
                     return false;
                 }    
             }
         }

         function selectTodos() {
             gv = document.getElementById('<%= gvSolicitud.ClientID %>');
             chkHeader = gv.rows[0].cells[9].children[0].children[0];
             for (var i = 1; i < gv.rows.length; i++) {
                 row = gv.rows[i];
                 chkUnir = row.cells[9].children[1].children[0];
                 if (chkHeader.checked) {
                     chkUnir.checked = true;
                 }
                 else {
                     chkUnir.checked = false;
                 }
             }

             return false;
         }
     </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <cc1:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
        </cc1:ToolkitScriptManager>
    <div id="apertura">Gestión de Remitos.</div> 
    <div class="solicitud100">
        <div class="sol_tit50"><label>Fecha Desde</label></div>
        <div class="sol_campo30"><asp:TextBox runat="server" CssClass="sol_campo25" id="txtFecha" /></div>
        <div class="sol_fecha"><img id="btnFecha" alt="calendario" src="../img/calendario.gif" style="width:20px; height:20px; cursor:pointer;" onmouseover="CambiarConfiguracionCalendario('txtFecha',this);"/></div>
        
        <div class="sol_fecha"></div>
              
        <div class="sol_tit50"> <label>Fecha Hasta</label></div>
        <div class="sol_campo30"><asp:TextBox runat="server" CssClass="sol_campo25" id="txtFechaHasta" /></div>
        <div class="sol_fecha"><img id="btnFechaHasta" alt="calendario" src="../img/calendario.gif" style="width:20px; height:20px; cursor:pointer;" onmouseover="CambiarConfiguracionCalendario('txtFechaHasta',this);"/></div>
        
    </div>
    
     <div class="solicitud100">        
        <div class="sol_tit50"><label>Empresa</label></div>
        
        <div class="sol_campo30"><asp:DropDownList  runat="server" id="ddlEmpresa" CssClass="sol_campo50c" />
                <cc1:CascadingDropDown ID="cddEmpresa" TargetControlID="ddlEmpresa" PromptText="N/A" PromptValue="0" 
                ServiceMethod="GetEmpresa" runat="server" Category="idEmpresa" 
                LoadingText="Cargando..."/>
        </div> 
        <div class="sol_fecha"></div>
        <div class="sol_fecha"></div>
        <div class="sol_tit50"><label>Sector</label></div>
        <div class="sol_campo30"><asp:DropDownList  runat="server" id="ddlSector" CssClass="sol_campo50c" />
                <cc1:CascadingDropDown ID="cddSector" TargetControlID="ddlSector" PromptText="N/A" PromptValue="0" 
                ServiceMethod="GetSector" runat="server" Category="idSector" 
                ParentControlID="ddlEmpresa" LoadingText="Cargando..." />
        </div> 
        <div class="sol_fecha"></div>
     </div>   
        
     <div class="solicitud100">          
        <div class="sol_tit50"> <label>Tipo Servicio</label></div>
        <div class="sol_campo30"><asp:DropDownList runat="server" CssClass="sol_campo50c"  id="ddlTipoServicio" /></div> 
        <div class="sol_fecha"></div>
        <div class="sol_fecha"></div>
        <div class="sol_tit50"> <label>Nº Solicitud</label></div>
        <div class="sol_campo30"><asp:TextBox CssClass="sol_campo25" runat="server" id="txtNroSolicitud" onkeypress="return ValidarNumeroEntero(event,this);"/></div> 
     </div> 
        
     <div class="solicitud100">    
        <div class="sol_tit50"><label>Nº Parte</label></div>
        <div class="sol_campo30"><asp:TextBox CssClass="sol_campo25" runat="server" id="txtNroParte" onkeypress="return ValidarNumeroEntero(event,this);"/></div>          
        <div class="sol_fecha"></div>
        <div class="sol_fecha"></div>

        <div class="sol_tit50"><label>Nº Remito</label></div>
        <div class="sol_campo30"><asp:TextBox CssClass="sol_campo25" runat="server" id="txtNroRemito" onkeypress="return ValidarNumeroEntero(event,this);"/></div>          
        <div class="sol_fecha"></div>
        
     </div>
     <div class="solicitud100">  
        <div class="sol_tit50"> <label>Condición</label></div>
        <div class="sol_campo30"><asp:DropDownList runat="server" CssClass="sol_campo50c" id="ddlCondicion" /></div> 
        <div class="sol_fecha"></div>  
        <div class="sol_fecha"></div>  
        
        <div class="sol_tit50"> <label>N° Equipo</label></div>
        <div class="sol_campo30"><asp:DropDownList runat="server" id="ddlTrailer" CssClass="sol_campo50c" /></div> 
        <div class="sol_fecha"></div> 
       
     </div>
     <div class="solicitud100">  
        <div class="sol_tit50"><label>Remito</label></div>         
        <div class="sol_campo50">
            <asp:RadioButtonList ID="chkRemito" runat="server" RepeatColumns="3">
                <asp:ListItem Value="1" Text="Si"></asp:ListItem>
                <asp:ListItem Value="0" Text="No"></asp:ListItem>
                <asp:ListItem Value=" " Text="Todos" Selected="True"></asp:ListItem>
            </asp:RadioButtonList>
        </div>
    </div>
     <div class="solicitud100sin">  
        <div class="sol_btn">
            <asp:Button runat="server" id="btnBuscar" cssclass="btn_form" Text="Buscar" onclick="btnBuscar_Click" />
            <asp:Button runat="server" id="btnExportar" cssclass="btn_form" Text="Exportar" onclick="btExportar_Click" />
        </div>
     </div>
    <asp:GridView ID="gvSolicitud" runat="server" AutoGenerateColumns="False" 
        DataMember="idCondicion" CssClass="tabla3" HeaderStyle-CssClass="celda1"       
        AllowPaging="true" PageSize="20" onpageindexchanging="gvSolicitud_PageIndexChanging"
        PagerStyle-CssClass="tabla_font" onrowcommand="gvSolicitud_RowCommand" 
        onrowdatabound="gvSolicitud_RowDataBound">
     <Columns>
         <asp:TemplateField HeaderText="Nº SS" HeaderStyle-CssClass="columna1" ItemStyle-CssClass="columna1">               
             <ItemTemplate>
                 <asp:Label ID="lblNroSolicitud" runat="server" Text='<%# Bind("NroSolicitud")%>'></asp:Label>
             </ItemTemplate>
         </asp:TemplateField>
         <asp:TemplateField HeaderText="Fecha SS" HeaderStyle-CssClass="columna1" ItemStyle-CssClass="columna1">
             <ItemTemplate>
                 <asp:Label ID="lblFecha" runat="server" Text='<%# Bind("Fecha") %>'></asp:Label>
             </ItemTemplate>
         </asp:TemplateField>
         <asp:TemplateField HeaderText="Nº Parte" HeaderStyle-CssClass="columna7" ItemStyle-CssClass="columna7">               
             <ItemTemplate>
                 <asp:Label ID="lblNroParte" runat="server" Text='<%# Bind("NroParte") %>'></asp:Label>
             </ItemTemplate>
         </asp:TemplateField>
         <asp:TemplateField HeaderText="Nº Remito" HeaderStyle-CssClass="columna7" ItemStyle-CssClass="columna7">               
             <ItemTemplate>
                 <asp:Label ID="lblNroRemito" runat="server" Text='<%# Bind("NroRemito") %>'></asp:Label>
             </ItemTemplate>
         </asp:TemplateField>
         <asp:TemplateField HeaderText="Fecha Remito" HeaderStyle-CssClass="columna1" ItemStyle-CssClass="columna1">
             <ItemTemplate>
                 <asp:Label ID="lblFechaRemito" runat="server" Text='<%# Bind("[Fecha Emision]") %>'></asp:Label>
             </ItemTemplate>
         </asp:TemplateField>         
           <asp:TemplateField HeaderText="Empresa" HeaderStyle-CssClass="columna5" ItemStyle-CssClass="columna5">
             <ItemTemplate>
                 <asp:Label ID="lblSolicitante" runat="server" Text='<%# Bind("Empresa") %>'></asp:Label>
             </ItemTemplate>
         </asp:TemplateField>
         <asp:TemplateField  HeaderText="Sector" HeaderStyle-CssClass="columna6" ItemStyle-CssClass="columna6">
            <ItemTemplate>
                 <asp:Label ID="lblEmpresa" runat="server" Text='<%# Bind("Sector") %>'></asp:Label>
             </ItemTemplate>
         </asp:TemplateField>
         <asp:TemplateField  HeaderText="Servicio" HeaderStyle-CssClass="columna5" ItemStyle-CssClass="columna5">
            <ItemTemplate>
                 <asp:Label ID="lblTipoServ" runat="server" Text='<%# Bind("TipoServicio") %>'></asp:Label>
             </ItemTemplate>
         </asp:TemplateField>             
         <asp:TemplateField  HeaderText="Nº Equipo" HeaderStyle-CssClass="columna7" ItemStyle-CssClass="columna7">
            <ItemTemplate>
                 <asp:Label ID="lblTrailer" runat="server" Text='<%# Bind("Trailer") %>'></asp:Label>
             </ItemTemplate>
         </asp:TemplateField>       
         <asp:TemplateField  HeaderText="Unir REM" HeaderStyle-CssClass="columna1" ItemStyle-CssClass="columna1">
           <HeaderTemplate>
                Unir
                <asp:CheckBox ID="chkUnirH" runat="server" ToolTip="Para seleccionar todos los remitos" onchange="return selectTodos();" ></asp:CheckBox>
           </HeaderTemplate>
            <ItemTemplate>
                <asp:HiddenField runat="server" ID="hfIdParte" Value='<%# Eval("idParte") %>' />
                 <asp:CheckBox ID="chkUnir" runat="server" ToolTip="Click para unir o seleccionar remitos" ></asp:CheckBox>
             </ItemTemplate>
         </asp:TemplateField>                
         <asp:TemplateField ItemStyle-CssClass="btn_iconos" HeaderStyle-CssClass="btn_iconos">
            <ItemTemplate>
                 <asp:HiddenField runat="server" ID="hfIdRemito" Value='<%# Eval("idRemito") %>' />
                 <asp:LinkButton ID="btnModificar" runat="server" title="Cargar Remito" CssClass="editar" CommandArgument='<%# Bind("idParte") %>' CommandName="GenerarRemito" OnClientClick="return validarCheck();"></asp:LinkButton>
                 <asp:HiddenField runat="server" ID="hfImpreso" Value='<%# Eval("Impreso") %>' />
             </ItemTemplate>
         </asp:TemplateField>               
         <asp:TemplateField  ItemStyle-CssClass="btn_iconos"  HeaderStyle-CssClass="btn_iconos">
            <ItemTemplate>
                 <asp:LinkButton ID="btnVer" runat="server" title="Ver Remito" CssClass="ver" CommandArgument='<%# Bind("idParte") %>' CommandName="Ver" OnClientClick="return validarCheck();"></asp:LinkButton>
             </ItemTemplate>
         </asp:TemplateField>
         <asp:TemplateField  ItemStyle-CssClass="btn_iconos"  HeaderStyle-CssClass="btn_iconos">
            <ItemTemplate>
                 <asp:LinkButton ID="btnEliminar" runat="server" title="Anular Regreso" CssClass="eliminar" CommandArgument='<%# Bind("idRemito") %>' CommandName="Anular" Visible="false"  OnClientClick="return Eliminar('el Remito');"></asp:LinkButton>
             </ItemTemplate>
         </asp:TemplateField>
          <asp:TemplateField  ItemStyle-CssClass="btn_iconos"  HeaderStyle-CssClass="btn_iconos">
            <ItemTemplate>
                 <asp:LinkButton ID="btnBonificado" runat="server" title="Remito Bonificado" CssClass="bonif" CommandArgument='<%# Bind("idParte") %>' CommandName="Bonificado" OnClientClick="return validarCheck();"></asp:LinkButton>
             </ItemTemplate>
         </asp:TemplateField>
         <asp:TemplateField ItemStyle-CssClass="btn_iconos"  HeaderStyle-CssClass="btn_iconos">
            <ItemTemplate>
                 <asp:LinkButton ID="btnUnirTS" runat="server" title="Unir Remito por Tipo de Servicio" CssClass="unirTS" CommandArgument='<%# Bind("idParte") %>' CommandName="unirTS" OnClientClick="return validarCheck();"></asp:LinkButton>
             </ItemTemplate>
         </asp:TemplateField>
     </Columns>
 </asp:GridView>
</asp:Content>
