﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ProcesoPP.Business;
using ProcesoPP.Services;
using ProcesoPP.Model;
using System.Web.Services;
using AjaxControlToolkit;

namespace ProcesoPP.UI.SSe3
{
    public partial class frmSSParteGestion : System.Web.UI.Page
    {
        #region PROPIEDADES

        public int _idUsuario
        {
            get { return (int)ViewState["idUsuario"]; }
            set { ViewState["idUsuario"] = value; }
        }

        public Permisos _Permiso
        {
            get { return (Permisos)ViewState["permiso"]; }
            set { ViewState["permiso"] = value; }
        }

        #endregion

        #region EVENTOS

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                _idUsuario = Common.EvaluarSession(Request, this.Page);
                if (_idUsuario != 0)
                {
                    _Permiso = Common.getModulo(Request, this.Page);
                    if (_Permiso != null)
                    {
                        if (!IsPostBack)
                        {                            
                            CargarEquipo();
                            CargarCuadrilla();
                            CargarCondicion();
                            CargarTipoServicio();
                        }
                    }
                    else
                    {
                        Mensaje.errorMsj("NO TIENE PERMISOS A ESTE FORMULARIO", this.Page, "SIN ACCESO", "../frmPrincipal.aspx");
                    }
                }
                else
                {
                    Mensaje.errorMsj("DEBE LOGUEARSE", this.Page, "LOGIN", "../frmLogin.aspx");
                }
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                CargarOTI();
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        protected void gvOTI_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "Verificar":
                    HiddenField hfIdSCParte = ((DataControlFieldCell)((Control)(e.CommandSource)).Parent).FindControl("hfIdSCParte") as HiddenField;
                    HiddenField hfIdParte = ((DataControlFieldCell)((Control)(e.CommandSource)).Parent).FindControl("hfidParte") as HiddenField;
                    Response.Redirect("../SSe3/frmSSParte.aspx?idSolicitud=" + hfIdSCParte.Value + "&idOTI=" + e.CommandArgument + "&idParte=" + hfIdParte.Value + "&idModulo=" + _Permiso.oModulo.idModulo + "&accion=" + e.CommandName);
                    break;
                case "Ver":
                    HiddenField hfIdSC = ((DataControlFieldCell)((Control)(e.CommandSource)).Parent).FindControl("hfIdSC") as HiddenField;
                    HiddenField hfIdParteVer = ((DataControlFieldCell)((Control)(e.CommandSource)).Parent).FindControl("hfidParteVer") as HiddenField;
                    Response.Redirect("../SSe3/frmSSParte.aspx?idOTI=" + e.CommandArgument + "&idParte=" + hfIdParteVer.Value + "&idModulo=" + _Permiso.oModulo.idModulo+"&accion="+ e.CommandName);
                    break;
                case "Mensual":
                    int idParte = int.Parse(e.CommandArgument.ToString());
                    ModificarMensual(idParte);
                    break;
            }
        }

        
        protected void gvOTI_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                CargarRow(e);
            }
        }

        protected void gvOTI_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvOTI.PageIndex = e.NewPageIndex;
            CargarOTI();
        }

        #endregion

        #region METODOS

        #region COMBOSAJAX

        [WebMethod]
        public static CascadingDropDownNameValue[] GetEmpresa(string knownCategoryValues, string category)
        {
            EmpresaBus oEmpresaBus = new EmpresaBus();
            List<CascadingDropDownNameValue> empresa = oEmpresaBus.GetDataEmpresa();
            return empresa.ToArray();
        }

        [WebMethod]
        public static CascadingDropDownNameValue[] GetSector(string knownCategoryValues, string category)
        {
            string idEmpresa = CascadingDropDown.ParseKnownCategoryValuesString(knownCategoryValues)["idEmpresa"];
            SectorBus oSectorBus = new SectorBus();
            List<CascadingDropDownNameValue> sectores = oSectorBus.GetDataSector(int.Parse(idEmpresa));
            return sectores.ToArray();
        }

        #endregion

        private void CargarRow(GridViewRowEventArgs e)
        {
            Common.CargarRow(e, _Permiso);

            LinkButton btnVer = e.Row.FindControl("btnVer") as LinkButton;
            LinkButton btnModificar = e.Row.FindControl("btnModificar") as LinkButton;
            HiddenField hfidParte = e.Row.FindControl("hfidParte") as HiddenField;
            HiddenField hfMensual = e.Row.FindControl("hfMensual") as HiddenField;
            LinkButton btnMensual = e.Row.FindControl("btnMensual") as LinkButton;


            if (int.Parse(hfidParte.Value) == 0)
            {
                btnVer.Visible = false;
                btnMensual.Visible = false;
            }
            else
            {
                btnVer.Visible = true;
                if ( hfMensual.Value.Length > 0 && !hfMensual.Value.Equals("0"))
                {
                    btnMensual.Visible = false;
                }
                else
                {
                    if (_Permiso.Escritura) //REMITOS
                        btnMensual.Visible = true;
                }
            }            
        }

        private void Eliminar(string idOrden)
        {
            Model.OrdenTrabajo oOrdenTrabajo = new Model.OrdenTrabajo();
            OrdenTrabajoBus oOrdenTrabajoBus = new OrdenTrabajoBus();
            oOrdenTrabajo = oOrdenTrabajoBus.OrdenTrabajoGetById(int.Parse(idOrden));
            oOrdenTrabajo.Baja = true;
            oOrdenTrabajoBus.OrdenTrabajoUpdate(oOrdenTrabajo);

            CargarOTI();
        }

        private void ModificarMensual(int idParte)
        {
            ParteEntrega oPE = new ParteEntrega();
            ParteEntregaBus oPEBus = new ParteEntregaBus();

            oPE = oPEBus.ParteEntregaGetById(idParte);
            oPE.Mensual = true;

            oPEBus.ParteEntregaUpdate(oPE);            
            Mensaje.successMsj("A partir de ahora queda disponible el Remito mensual para cargar al cliente.", this, "Parte Mensual", null);
            CargarOTI();
        }


        private void CargarOTI()
        {
            OrdenTrabajoBus oOrdenTrabajoBus = new OrdenTrabajoBus();
            string idempresa = ddlEmpresa.SelectedValue == string.Empty ? "0" : ddlEmpresa.SelectedValue;
            string idsector = ddlSector.SelectedValue == string.Empty ? "0" : ddlSector.SelectedValue;
            gvOTI.DataSource = oOrdenTrabajoBus.OrdenTrabajoGetByFilter(txtNroSolicitud.Text == "" ? "0" : txtNroSolicitud.Text,
                                                                          idempresa,
                                                                          txtFecha.Text,
                                                                          txtFechaHasta.Text,
                                                                          txtNroParte.Text == "" ? "0" : txtNroParte.Text,
                                                                          ddlTrailer.SelectedValue,
                                                                          idsector,
                                                                          ddlCuadrilla.SelectedValue,
                                                                          ddlTipoServicio.SelectedValue,
                                                                          ddlCondicion.SelectedValue);
            gvOTI.DataBind();
        }

        private void CargarEquipo()
        {
            VehiculoBus oVehiculoBus = new VehiculoBus();
            oVehiculoBus.CargarComboEquipo(ref ddlTrailer, "Seleccione un equipo...");
        }

        private void CargarCuadrilla()
        {
            CuadrillaBus oCuadrillaBus = new CuadrillaBus();
            ddlCuadrilla.DataSource = oCuadrillaBus.CuadrillaGetAllBaja();
            ddlCuadrilla.DataTextField = "Nombre";
            ddlCuadrilla.DataValueField = "idCuadrilla";
            ddlCuadrilla.DataBind();

            ddlCuadrilla.Items.Insert(0, new System.Web.UI.WebControls.ListItem("Seleccione una Cuadrilla", "0"));
        }

        private void CargarCondicion()
        {
            CondicionBus oCondicionBus = new CondicionBus();
            oCondicionBus.CargarCombo(ref ddlCondicion, "Seleccione Condicion...");
        }

        private void CargarTipoServicio()
        {
            TipoServicioBus oTipoServicioBus = new TipoServicioBus();
            oTipoServicioBus.CargarCombo(ref ddlTipoServicio, "Selecione Tipo de Servicio...");
        }


        #endregion

        
    }
}