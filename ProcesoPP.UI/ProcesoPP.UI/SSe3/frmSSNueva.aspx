﻿<%@ Page Title="PP - Solicitud de Servicio" Language="C#" MasterPageFile="~/frmPrincipal.Master" AutoEventWireup="true" CodeBehind="frmSSNueva.aspx.cs" Inherits="ProcesoPP.UI.SSe3.frmSSNueva" EnableEventValidation="false" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<script type="text/javascript" >
    $(document).ready(function () { SetMenu("liOperaciones"); });

    $(document).ready(function () {
        $(".nuevaSS_titleServ").each(function () {
            oChk = this.children[0].children[0];
            mostrar(oChk);
        });

        if ($('select[name*="ddlOrigen"] option:selected')[0].index == 0) {            
            $('select[name*="ddlOrigen"]').find('option:contains("BPP"):first').prop("selected", true);            
        }

    });

    function mostrar(oChk) {
        if (!oChk.children[0].checked) {
            $("#" + oChk.parentElement.offsetParent.id + " > tbody > tr").not(":first").hide(500); //muestro mediante clase
        }
        else {
            if (oChk.parentElement.offsetParent.rows.length > 1
            && oChk.parentElement.offsetParent.rows[1].innerText.trim() != ""
            && oChk.parentElement.offsetParent.rows[1].innerHTML.trim() != ""
            ) {
                $("#" + oChk.parentElement.offsetParent.id + " > tbody > tr").not(":first").show(500);
            } else {
                $("#" + oChk.parentElement.offsetParent.id + " > tbody > tr").not(":first").hide();
            }
        }
    }
       
    function getKms(ddlOrigen, ddlDestino, txtKm, hfKms) {

        hfKmsAll = document.getElementById(hfKms);
        ddlOrigen = document.getElementById(ddlOrigen);
        ddlDestino = document.getElementById(ddlDestino);
        txtKm = document.getElementById(txtKm);
        if (ddlOrigen.value != 0 && ddlDestino.value != 0) {
            var kmsList = jQuery.parseJSON(hfKmsAll.value);

            $(kmsList).each(function () {
                if (this.oLugarDesde.IdLugar == ddlOrigen.value &&
                this.oLugarHasta.IdLugar == ddlDestino.value){
                    txtKm.value = this.Kms;
                    return false;
                    }
                else
                    txtKm.value = 0;
            });
        }
    }

    function guardarMov() {
        oMovimiento = new Object();
        oMovimiento.oLugarDestino = new Object();
        oMovimiento.oLugarOrigen = new Object();
        tds = $("td[class^='celda']");
        contMov = 0;
        if (tds.length > 0) {
           
            //VALIDO LA CANTIDAD DE MOVIMIENTOS SELECCIONADOS
            tablesMov = $("input[id*='hfRequiereTraslado']");
            $(tablesMov).each(function () {
                gvID = this.id.split("_");
                gvID = gvID[0] + "_" + gvID[1] + "_" + gvID[2] + "_" + gvID[3];
                hfMov = gvID + "_" + "hfRequiereTraslado";
                hfMov = $("input[id*=" + hfMov + "]");

                chkMov = gvID + "_" + "chkTipoServ";
                chkMov = $("[id^=" + gvID + "] [id$='chkTipoServ']");
                if (hfMov[0].value == "True" && chkMov[0].checked) {
                    contMov++;

                    if (contMov == 1) {
                        $(tds).each(function () {
                            oElement = this.children[0];
                            if (oElement.id.indexOf("Origen") >= 0 && oElement.id.indexOf(gvID) >= 0)
                                oMovimiento.oLugarOrigen.IdLugar = oElement.value;
                            if (oElement.id.indexOf("Destino") >= 0 && oElement.id.indexOf(gvID) >= 0)
                                oMovimiento.oLugarDestino.IdLugar = oElement.value;
                            if (oElement.id.indexOf("Kms") >= 0 && oElement.id.indexOf(gvID) >= 0)
                                oMovimiento.KmsEstimados = oElement.value;
                            if (oElement.id.indexOf("PozoOrig") >= 0 && oElement.id.indexOf(gvID) >= 0)
                                oMovimiento.PozoOrigen = oElement.value;
                            if (oElement.id.indexOf("PozoDest") >= 0 && oElement.id.indexOf(gvID) >= 0)
                                oMovimiento.PozoDestino = oElement.value;

                        });
                    }
                }
            });

            if (contMov > 1) {
                new Messi("No debe seleccionar mas de un Servicio con Movimiento",
                        { title: 'Error', modal: true, titleClass: 'error', buttons: [{ id: 0, label: 'Aceptar', val: 'Y'}] });
                return false;
            }
            else {

                hfMov = document.getElementById("ctl00_ContentPlaceHolder1_hfMov");
                var jsonString = JSON.stringify(oMovimiento);
                hfMov.value = jsonString;
                return true;
            }

        } 
    }

    function guardarTrailer() {
        
        oList = [];
        gvTrailer = document.getElementById('<%= gvTransporte.ClientID %>');
        hf = document.getElementById('<%= hfTrailers.ClientID %>');
        hf.value = "";
        for (var i = 2; i < gvTrailer.rows.length; i++) {
            oSTP = new Object();
            row = gvTrailer.rows[i];
            if (row.cells[0].childNodes.length > 1) {
                chk = row.cells[0].childNodes[1];
                if (chk.checked) {
                    hfTipoServ = row.cells[0].childNodes[5];
                    hfTrailer = row.cells[0].childNodes[7];

                    oSTP.IdPosible = 0;
                    oSTP.IdSolicitud = 0;
                    oSTP.idTrailer = hfTrailer.value;
                    oSTP.IdTipoServicio = hfTipoServ.value;

                    oList.push(oSTP);
                } 
            }
        }

        var myJSON = JSON.stringify({ ListSolicitudTrailerPosible: oList });
        hf.value = myJSON;
    }

    function guardar() {
        msj =   'txtNroSS|Ingrese le Nro. de Solicitud de Servicio.' +
                '~txtFecha|Debe ingresar una fecha.~ddlEmpresa|Seleccione una empresa.' +
                '~ddlSector|Debe seleccionar el sector.';
        val = ValidarObj(msj);

        if (val)
           return guardarMov();
        else
            return val;
    }
</script>
 <script type="text/javascript">
     function validarTrailers() {
         grid = document.getElementById("ctl00_ContentPlaceHolder1_gvTransporte");
         cont = 0;
         cant = 0;
         for (i = 1; i < grid.rows.length; i++) {
             row = grid.rows[i];
             if (row.cells.length == 1) {
                 cant = 0;
                 cont = 0;
             }
             else if (row.cells.length > 1) {
                 chk = row.cells[0].childNodes[1];
                 if (chk.checked) {
                     cant = row.cells[0].childNodes[3].value;
                     if (parseInt(cont) > parseInt(cant))
                         break;
                     cont++;
                 }
             }
         }

         if (parseInt(cont) > parseInt(cant)) {
             new Messi("No debe superar la cantidad de equipos cargada en el contrato",
                        { title: 'Error', modal: true, titleClass: 'error', buttons: [{ id: 0, label: 'Aceptar', val: 'Y'}] });
             return false;
         }
         else
             guardarTrailer();             

     }
    </script>
    <script type="text/javascript">
        function valirdarEsp(oChk) {
            cont = 0;
            gv = oChk.parentElement.parentElement.parentElement;

            for (var i = 1; i < gv.rows.length; i++) {
                row = gv.rows[i];
                chk = row.cells[0].children[2].children[0];
                requiereEq = (row.cells[0].children[3].value == "True" ? true : false);

                 if (chk.checked && requiereEq) {
                     cont++;

                     if (parseInt(cont) > 1) {
                         chk.checked = false;
                         break;
                     }
                 }
             }

             if (parseInt(cont) > 1) {
                 new Messi("No debe seleccionar mas de una especificacion técnica",
                        { title: 'Error', modal: true, titleClass: 'error', buttons: [{ id: 0, label: 'Aceptar', val: 'Y'}] });
                 return false;
             }
             else
                 return true;

        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<asp:HiddenField   runat="server" ID="hfFechaInicioContrato" />
<asp:HiddenField   runat="server" ID="hfFechaFinContrato" />
         <div id="divSolServ" class="apertura" runat="server">Solicitud de Servicio.</div>
         <asp:HiddenField   runat="server" id="hfIds"/>
         <div class="solicitud100">
            <div class="sol_tit50"><label>SS N°</label></div>
            <div class="sol_campo50">
                <asp:TextBox runat="server" Enabled="false" CssClass="sol_campo25" Text="0" ID="txtNroSS" onkeypress="return ValidarNumeroEntero(event,this);" />
            </div>
            <div class="sol_fecha">
                <asp:Label id="lblLetra" Text="" CssClass="sol_campo25" runat="server" /> </div>
            <div class="sol_tit50">
                <label>Fecha</label></div>
            <div class="sol_campo50">
                <asp:TextBox CssClass="sol_campo25" runat="server" ID="txtFecha" /></div>
            <div class="sol_fecha">
                <img id="btnFecha" alt="calendario" src="../img/calendario.gif" style="width: 20px;
                    height: 20px; cursor: pointer;" onmouseover="CambiarConfiguracionCalendario('txtFecha',this);" />
            </div>
        </div>

        <div id="titulos">Datos del Solicitante.</div>
        <cc1:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
        </cc1:ToolkitScriptManager>
        
        <div class="solicitud100">
            <div class="sol_tit30">
                <label>Empresa</label></div>
            <div class="sol_campo30">
                <asp:DropDownList ID="ddlEmpresa" runat="server" DataTextField="Descripcion" DataValueField="idEmpresa"/>
                <cc1:CascadingDropDown ID="cddEmpresa" TargetControlID="ddlEmpresa" PromptText="N/A" PromptValue="0" 
                ServiceMethod="GetEmpresa" runat="server" Category="idEmpresa" 
                LoadingText="Cargando..."/>
            </div>
            <div class="sol_tit30">
                <label>Sector</label></div>
            <div class="sol_campo30">                
                <asp:DropDownList ID="ddlSector" runat="server" DataTextField="Descripcion" DataValueField="idSector" AutoPostBack="true" OnSelectedIndexChanged="ddlSector_SelectedIndexChanged" />
               <cc1:CascadingDropDown ID="cddSector" TargetControlID="ddlSector" PromptText="N/A" PromptValue="0" 
                ServiceMethod="GetSector" runat="server" Category="idSector" 
                ParentControlID="ddlEmpresa" LoadingText="Cargando..." />
            </div>
        
            <div class="sol_tit30"><label>Solicitante</label></div>
            <div class="sol_campo30">
                <asp:DropDownList ID="ddlSolicitante" runat="server" DataTextField="Nombre" DataValueField="idSolicitante"/>
                 <cc1:CascadingDropDown ID="ccdSolicitante" TargetControlID="ddlSolicitante" PromptText="N/A" PromptValue="0" 
                ServiceMethod="GetSolicitante" runat="server" Category="idSolicitante" 
                ParentControlID="ddlSector" LoadingText="Cargando..." />
             </div>
            
        </div>
        <div id="titulos">Tipo de Servicio</div>
        <div class="solicitud100">
     
            <asp:GridView  AutoGenerateColumns="false" runat="server" ID="gvTipoServicio" ShowHeader="false" 
                        OnRowDataBound="gvTipoServicio_OnRowDataBound" Width="100%"  
                        BorderStyle="None" GridLines="None">
                <Columns>
                   <asp:TemplateField>
                        <ItemTemplate>
                            <asp:HiddenField runat="server" ID="hfIdTipoServicio" Value='<%# Eval("idTipoServicio") %>'/>
                            <asp:HiddenField runat="server" ID="hfAbreviacion" Value='<%# Eval("Abreviacion") %>' />
                            <asp:HiddenField runat="server" ID="hfRequiereTraslado" Value='<%# Eval("RequiereTraslado") %>' />
                            <asp:HiddenField runat="server" ID="hfRequiereKmsTaco" Value='<%# Eval("RequiereKmsTaco") %>' />
                            <asp:HiddenField runat="server" ID="hfRequiereKms" Value='<%# Eval("RequiereKms") %>' />
                            
                            <asp:Label runat="server" ID="lblTipoServ" Text='<%# Eval("TipoServicio") %>' Visible="false"/>
                            
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>                                                          
                        <ItemTemplate>                                                              
                            <asp:GridView  AutoGenerateColumns="false" runat="server" ID="gvEspecificaciones" 
                                            BorderStyle="None" GridLines="None"
                                            OnRowDataBound="gvEspecificaciones_OnRowDataBound" Width="100%"
                                            HeaderStyle-CssClass="nuevaSS_titleServ">
                                <Columns>        
                                    <asp:BoundField DataField="idEspecificacion" Visible="false" />                                   
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:CheckBox runat="server" ID="chkTipoServ" onchange="mostrar(this);"/>
                                            <asp:Label runat="server" ID="lblTipoServ" Text='<%# Eval("TipoServicio") %>' />                                            
                                        </HeaderTemplate>                                           
                                        <ItemTemplate>
                                            <asp:HiddenField runat="server" ID="hfIdEspecificacion" Value='<%# Eval("idEspecificacion") %>'/>
                                            <asp:HiddenField runat="server" ID="hfEspTraslado" Value='<%# Eval("RequiereTraslado") %>'/>                                            
                                            <asp:CheckBox runat="server" ID="chkEspecificacion" Text='<%# Eval("EspecificacionTecnica") %>' CssClass="nuevaSS_tdEsp" onchange="valirdarEsp(this);"/>
                                            <asp:HiddenField runat="server" ID="hfRequiereEq" Value='<%# Eval("RequiereEquipo") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </ItemTemplate>                               
                    </asp:TemplateField>                                                 
                </Columns>
            </asp:GridView>
        </div>

        <div class="solicitud100sin">
            <div class="sol_btn">
                <asp:Button runat="server" ID="btnGuardar" Text="GUARDAR" CssClass="btn_form" 
                    OnClientClick="return guardar(); " 
                    onclick="btnGuardar_Click"
                    ></asp:Button>
            </div>
            <div class="sol_btn">
                <input type="button" id="btnVolver" value="VOLVER" class="btn_form" onclick="IrA('');" />
            </div>
            
        </div>
    
    <asp:HiddenField runat="server" ID="hfKmsAll" />
    <asp:HiddenField runat="server" ID="hfMov" />
    <asp:HiddenField   runat="server" id="hfTrailers"/>
    <div id="capaPopUp"></div>
    <div id="popUpDiv">
        <div id="capaContent">
        
        <div style="width: 90%; padding-top: 5%; padding-left: 10%;	padding-bottom: 5%; height:75%; overflow:auto;">
                <asp:GridView ID="gvTransporte" CssClass="tabla3" runat="server" AutoGenerateColumns="False" DataMember="idVehiculo"
                    Width="95%" 
                    EmptyDataText="No existen registros" EmptyDataRowStyle-CssClass="celda50"
                    EmptyDataRowStyle-Width="80%" OnSorting="gvTransporte_Sorting">
                    <Columns>
                         <asp:BoundField DataField="idTipoServicio"  Visible="false"/>                         
                         <asp:BoundField DataField="TipoServicio" SortExpression="TipoServicio" Visible="false"/>
                         <asp:TemplateField HeaderStyle-CssClass="celda_boxt" ItemStyle-CssClass="celda_box">
                            <ItemTemplate>
                                <asp:CheckBox runat="server" ID="chkTrailer" />
                                <asp:HiddenField runat="server" ID="hfCant" Value='<%# Bind("cant") %>' />                                
                                <asp:HiddenField runat="server" ID="hfidTipoServicio" Value='<%# Bind("idTipoServicio") %>'/>
                                <asp:HiddenField runat="server" ID="hfidVehiculo" Value='<%# Bind("idVehiculo") %>'/>
                            </ItemTemplate>
                         </asp:TemplateField>                         
                         <asp:BoundField DataField="idEspecificacion"  Visible="false"/>
                         <asp:BoundField DataField="Especificacion" HeaderText="ESPECIFICACIONES" SortExpression="Especificacion"
                                        HeaderStyle-CssClass="celda_boxtit" ItemStyle-CssClass="celda_box1" ItemStyle-Width="42%" HeaderStyle-Width="42%"/>
                         <asp:BoundField DataField="Trailer" HeaderText="EQUIPOS DISPONIBLES" 
                                        HeaderStyle-CssClass="celda_boxtit" ItemStyle-CssClass="celda_box1" ItemStyle-Width="42%" HeaderStyle-Width="42%"/>
                    </Columns>
                </asp:GridView>
            </div>
            <div id="botones" style="width: 95%; height:10%;">
                <div class="regreso_3">
                    <input type="button" onclick="cerrarPopUp();" title="Cancelar" id="btnCerrar" class="btn_form"
                        value="CANCELAR" />
                </div>
                <div class="regreso_3">
                    <%--<input type="button" onclick="idOT();" title="Ir a la orden de trabajo" id="btnIrOT"
                        class="btn_form" value="OTI" />--%>
                    <asp:Button ToolTip="Ir a la orden de trabajo" id="btnIrOTI" runat="server" OnClientClick="return validarTrailers();"
                        CssClass="btn_form" Text="OTI" onclick="btnGuardarDisponibles_Click"></asp:Button>
                </div>
                <div class="regreso_3">
                    <%--<input type="button" onclick="idGestion();" title="Ir a gestión de solicitudes" id="btnIrGestion"
                        class="btn_form" value="GUARDAR SS" />--%>
                    <asp:Button ToolTip="Guardar Trailers Disponibles" id="btnGuardarDisponibles" runat="server" OnClientClick="return validarTrailers();"
                        CssClass="btn_form" Text="GUARDAR SS" onclick="btnGuardarDisponibles_Click"></asp:Button>

                </div>                
            </div>
        </div>
     </div>
</asp:Content>
