﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using ProcesoPP.Model;
using ProcesoPP.Services;
using ProcesoPP.Business;
using System.Data;

namespace ProcesoPP.UI.SSe3
{
    public partial class frmSSOti : System.Web.UI.Page
    {
        #region <PROPIEDADES>

        public Permisos _Permiso
        {
            get { return (Permisos)ViewState["permiso"]; }
            set { ViewState["permiso"] = value; }
        }

        public int _idUsuario
        {
            get { return (int)ViewState["idUsuario"]; }
            set { ViewState["idUsuario"] = value; }
        }

        private int[] _idsSolicitud
        {
            get { return (int[])ViewState["idSolicitud"]; }
            set { ViewState["idSolicitud"] = value; }
        }

        private int _idOTI
        {
            get { return (int)ViewState["idOTI"]; }
            set { ViewState["idOTI"] = value; }
        }

        private int _idVehiculo
        {
            get { return (int)ViewState["idVehiculo"]; }
            set { ViewState["idVehiculo"] = value; }
        }

        public int _ADM
        {
            get { return int.Parse(ConfigurationManager.AppSettings["ADM"].ToString()); }
        }
                
        public int _idCampamento
        {
            get { return int.Parse(ConfigurationManager.AppSettings["Camp"].ToString()); }
        }
                
        public int _idConexion
        {
            get { return int.Parse(ConfigurationManager.AppSettings["CON"].ToString()); }
        }

        public int _idDesconexion
        {
            get { return int.Parse(ConfigurationManager.AppSettings["DESCON"].ToString()); }
        }

        #endregion

        #region <EVENTOS>

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                _idUsuario = Common.EvaluarSession(Request, this.Page);
                if (_idUsuario != 0)
                {
                    _Permiso = Common.getModulo(Request, this.Page);
                    if (_Permiso != null && _Permiso.Lectura)
                    {
                        if (!IsPostBack)
                        {   
                            InitScreen();
                        }
                    }
                    else
                    {
                        Mensaje.errorMsj("NO TIENE PERMISOS A ESTE FORMULARIO", this.Page, "SIN ACCESO", "../frmPrincipal.aspx");
                    }
                }
                else
                {
                    Mensaje.errorMsj("DEBE LOGUEARSE", this.Page, "LOGIN", "../frmLogin.aspx");
                }
            }
            catch (Exception ex)
            {                
                Mensaje.errorMsj("Ha ocurrido un error. Mensaje: " + ex.Message + ". Detalle: " + ex.StackTrace, this.Page, "Error", null);
            }
        }

        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                btnGuardar.Visible = false;
                GuardarOT();
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        protected void btnCambiarTrailer_Click(object sender, EventArgs e)
        {
            try
            {
                SolicitudCliente oSS = (new SolicitudClienteBus()).SolicitudClienteGetById(int.Parse(hfSS_ConEsp.Value));
                getTrailersDisponibles(ref ddlEquipo,oSS);
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        #endregion

        #region <METODOS>

        private void GuardarOT()
        {
            string ids = "";
            OrdenTrabajoBus oOrdenTrabajoBus = new OrdenTrabajoBus();
            Model.OrdenTrabajo oOTI = new Model.OrdenTrabajo();
 
            foreach (int idSS in _idsSolicitud)
            {
                oOTI = new Model.OrdenTrabajo();
                oOTI = generarOTI(oOTI, idSS);
                
                if (_idOTI > 0)
                {
                    oOTI.IdOrden = _idOTI;
                }
                oOTI.IdOrden = oOrdenTrabajoBus.Guardar(oOTI);
                ids += oOTI.IdOrden + ",";
            }

            if (oOTI.IdOrden > 0)
            {
                //Mensaje.successMsjOpen("Orden trabajo guardada con éxito", this, "Exito", "../Reportes/frmImprimir.aspx?idOTI=" + oOTI.IdOrden.ToString());
                //Response.Redirect("frmSSGestion.aspx?idModulo=" + _Permiso.oModulo.idModulo.ToString(), false);
                Mensaje.successMsj("Se ha guardado la orden de trabajo con éxito!", this, "Exito", "frmSSGestion.aspx?idModulo=" + _Permiso.oModulo.idModulo.ToString()); 
                Mensaje.openAcuerdo(this.Page, "../Reportes/frmImprimir.aspx?idOTI=" + ids);

            }
        }

        private Model.OrdenTrabajo generarOTI(Model.OrdenTrabajo oOTI, int idss)
        {
            oOTI.IdSolicitudCliente = idss;
            oOTI.oSolicitudCliente = (new SolicitudClienteBus()).SolicitudClienteGetById(idss);
            //MOVIMIENTO
            if (divMov.Visible && idss == int.Parse(hfIdSS_mov.Value))
            {
                oOTI.IdChofer = (ddlChofer.SelectedValue.Equals(string.Empty) || ddlChofer.SelectedValue.Equals("0")) ? 0 : int.Parse(ddlChofer.SelectedValue);
                oOTI.IdTransporte = (ddlVehiculo.SelectedValue.Equals(string.Empty) || ddlVehiculo.SelectedValue.Equals("0")) ? 0 : int.Parse(ddlVehiculo.SelectedValue);
                oOTI.IdAcompanante = (ddlAcompanante.SelectedValue.Equals(string.Empty) || ddlAcompanante.SelectedValue.Equals("0")) ? 0 : int.Parse(ddlAcompanante.SelectedValue);
                oOTI.Entraga = false;
                oOTI.IdSolicitudCliente = int.Parse(hfIdSS_mov.Value);
            }
            //CON ESPECIFICACION
            if (divConEsp.Visible && idss == int.Parse(hfSS_ConEsp.Value))
            {
                oOTI.IdVehiculo = int.Parse(ddlEquipo.SelectedValue);
                oOTI.Entraga = true;
                oOTI.IdSolicitudCliente = int.Parse(hfSS_ConEsp.Value);
            }
            //REQUIERE EQUIPO - CLIENTE
            if (divEqCliente.Visible && idss == int.Parse(hfSS_EqCliente.Value))
            {
                AgregarConcepto(oOTI, lblTipoServEqCliente.Text, ddlEqCliente.SelectedValue, null, hfidTipoServEqCliente.Value);
                oOTI.Entraga = true;
                oOTI.IdSolicitudCliente = int.Parse(hfSS_EqCliente.Value);
            }
            //MATERIALES
            if (divMateriales.Visible && idss == int.Parse(hfSS_Materiales.Value))
            {
                AgregarConcepto(oOTI, lblTipoServMateriales.Text, txtDetalle.Text, null, hfidTipoServMateriales.Value);
                
                oOTI.Entraga = false;
                oOTI.IdSolicitudCliente = int.Parse(hfSS_Materiales.Value);
            }
            //REQUIERE ESPECIFICACION
            if (divCuadrilla.Visible && idss == int.Parse(hfSS_Cuadrilla.Value))
            {
                oOTI.IdEquipo = int.Parse(ddlCuadrilla.SelectedValue);
                oOTI.Entraga = true;
                oOTI.IdSolicitudCliente = int.Parse(hfSS_Cuadrilla.Value);
            }
            //REQUIERE PERSONAL
            if (divCantPersonal.Visible && idss == int.Parse(hfSS_CantPers.Value))
            {
                //PERSONAL
                AgregarConcepto(oOTI, "Personal",null, ddlCantPersonal, hfidTipoServCantPersonal.Value);
                AgregarDetalle(oOTI, ddlCantPersonal, hfidTipoServCantPersonal.Value, "P");
                                
                //EQUIPOS PROPIOS                
                AgregarConcepto(oOTI, "Equipos Propios", null, chkEquiposPropio, hfidTipoServCantPersonal.Value);
                AgregarDetalle(oOTI, chkEquiposPropio, hfidTipoServCantPersonal.Value, "EP");
                
                //EQUIPOS DEL CLIENTE
                AgregarConcepto(oOTI, "Equipos del Cliente", null, chkEquipoCliente, hfidTipoServCantPersonal.Value);
                AgregarDetalle(oOTI, chkEquipoCliente, hfidTipoServCantPersonal.Value, "EC");
                
                oOTI.Entraga = false;
                oOTI.IdSolicitudCliente = int.Parse(hfSS_CantPers.Value);
            }

            //CONEXION
            if (divConexion.Visible && idss == int.Parse(hfSS_Con.Value))
            {                
                AgregarConcepto(oOTI, "Conexión", txtConexion.Text, null, hfidTipoServConn.Value);                                
                
                oOTI.Entraga = false;
                oOTI.IdSolicitudCliente = int.Parse(hfSS_Con.Value);
            }
            //DESCONEXION
            if (divDesconexion.Visible && idss == int.Parse(hfSS_Descon.Value))
            {                
                AgregarConcepto(oOTI, "Desconexión", txtDesconexion.Text, null, hfidTipoServDescon.Value);

                oOTI.Entraga = false;
                oOTI.IdSolicitudCliente = int.Parse(hfSS_Descon.Value);
            }
            return oOTI;
        }

        private void AgregarDetalle(OrdenTrabajo oOTI, CheckBoxList chkList, string idTipoServicio, string id)
        {
            foreach (ListItem item in chkList.Items)
            {
                if (item.Selected)
                {
                    OrdenTrabajoDetalle oDet = new OrdenTrabajoDetalle();
                    oDet.IdOrden = oOTI.IdOrden;
                    oDet.IdTipoServicio = int.Parse(idTipoServicio);
                    if (id.Equals("P"))
                    {
                        oDet.IdPersonal = int.Parse(item.Value);
                    }
                    if (id.Equals("EC"))
                    {
                        oDet.IdEquipoCliente = int.Parse(item.Value);
                    }
                    if (id.Equals("EP"))
                    {
                        oDet.IdEquipoPropio = int.Parse(item.Value);
                    }
                    oOTI.oListOTIDetalle.Add(oDet);
                }
            }
            
        }

        private void AgregarConcepto(OrdenTrabajo oOrden, string concepto, string valor, CheckBoxList chkList, string idTipoServicio)
        {
            OrdenTrabajoConcepto oOTICon = new OrdenTrabajoConcepto();
            oOTICon.IdOti = oOrden.IdOrden;
            oOTICon.Concepto = concepto;
            if (chkList != null)
                oOTICon.Valor = getValorChk(chkList);
            if (valor != null)
                oOTICon.Valor = valor;
            oOTICon.IdTipoServicio = int.Parse(idTipoServicio);

            if (oOTICon.Valor != string.Empty)
                oOrden.oListOTIConcepto.Add(oOTICon);
        }

        private string getValorChk(CheckBoxList chkList)
        {
            string coma = "";
            string valor = "";
            foreach (ListItem item in chkList.Items)
            {
                if (item.Selected)
                {
                    valor += coma + item.Text;
                    coma = "; ";
                }
            }

            return valor;
        }

        private void InitScreen()
        {
                        
            _idOTI = 0;
            _idVehiculo = 0;
            if (_Permiso.oTipoUsuario.idTipoUsuario == _ADM)
            {
                btnCambiarTrailer.Visible = true;
            }

            if (Request["idVehiculo"] != null && Request["idVehiculo"].ToString() != string.Empty)
            {
                _idVehiculo = int.Parse(Common.desEncriptarURL(Request["idVehiculo"]));
            }

            if (Request["idOTI"] != null && Request["idOTI"] != string.Empty)
            {
                _idOTI = int.Parse(Common.desEncriptarURL(Request["idOTI"]));
            }

            if (Request["idSolicitud"] != null)
            {            
                string[] ids = Common.desEncriptarURL(Request["idSolicitud"]).Split(',');
                _idsSolicitud = Array.ConvertAll<string, int>(ids, int.Parse);
                CargarSolicitud();
            }
            CargarOTI();

            if (Request["accion"] != null && Request["accion"] == "Ver")
            {
                btnGuardar.Visible = false;
            }
        }

        private void CargarOTI()
        {
            if (_idOTI > 0)
            {
                Model.OrdenTrabajo oOrdenTrabajo = new Model.OrdenTrabajo();
                OrdenTrabajoBus oOrdenTrabajoBus = new OrdenTrabajoBus();
                oOrdenTrabajo = oOrdenTrabajoBus.OrdenTrabajoGetById(_idOTI);
                ddlVehiculo.SelectedValue = oOrdenTrabajo.IdTransporte.ToString();
                ddlChofer.SelectedValue = oOrdenTrabajo.IdChofer.ToString();
                ddlAcompanante.SelectedValue = oOrdenTrabajo.IdAcompanante.ToString();
                
                if (oOrdenTrabajo.IdVehiculo != 0 || oOrdenTrabajo.IdEquipo != 0)
                    seleccionarEquipo(oOrdenTrabajo);
                if (oOrdenTrabajo.oListOTIDetalle.Count > 0)
                {
                    seleccionarPersonal(oOrdenTrabajo.oListOTIDetalle);
                }

                if (oOrdenTrabajo.oListOTIConcepto.Exists(c => c.Concepto.Contains("Desconexi")))
                {
                    OrdenTrabajoConcepto item = oOrdenTrabajo.oListOTIConcepto.Find(c => c.Concepto.Contains("Desconexi"));
                    txtDesconexion.Text = item.Valor;
                }
                else if (oOrdenTrabajo.oListOTIConcepto.Exists(c => c.Concepto.Contains("Conexi")))
                {
                    OrdenTrabajoConcepto item = oOrdenTrabajo.oListOTIConcepto.Find(c => c.Concepto.Contains("Conexi"));
                    txtConexion.Text = item.Valor;
                }
                //MATERIALES
                if (oOrdenTrabajo.oListOTIConcepto.Exists(c => c.Concepto.Contains(oOrdenTrabajo.oSolicitudCliente.oTipoServicio.Descripcion) && c.oTipoServicio.RequiereMateriales))
                {
                    OrdenTrabajoConcepto item = oOrdenTrabajo.oListOTIConcepto.Find(c => c.Concepto.Contains(oOrdenTrabajo.oSolicitudCliente.oTipoServicio.Descripcion));
                    txtDetalle.Text = item.Valor;
                }
                if (oOrdenTrabajo.oListOTIConcepto != null &&
                    oOrdenTrabajo.oListOTIConcepto.Count > 0)
                {
                    if (oOrdenTrabajo.oListOTIConcepto.Exists(t => t.oTipoServicio.RequiereEquipo))
                    {
                        OrdenTrabajoConcepto item = oOrdenTrabajo.oListOTIConcepto.Find(c => c.oTipoServicio.RequiereEquipo);
                        ddlEqCliente.SelectedValue = item.Valor;                    
                    }
                }
            }   

        }

        private void seleccionarPersonal(List<OrdenTrabajoDetalle> list)
        {
            foreach (ListItem item in ddlCantPersonal.Items)
            {
                if (list.Exists(det => det.IdPersonal == int.Parse(item.Value)))
                    item.Selected = true;
            }
            foreach (ListItem item in chkEquipoCliente.Items)
            {
                if (list.Exists(det => det.IdEquipoCliente == int.Parse(item.Value)))
                    item.Selected = true;
            }
            foreach (ListItem item in chkEquiposPropio.Items)
            {
                if (list.Exists(det => det.IdEquipoPropio== int.Parse(item.Value)))
                    item.Selected = true;
            }
        }

        private void CargarTrailersPosibles(SolicitudCliente oSS, ref DropDownList ddl)
        {
            SolicitudTrailerPosibleBus oSolPosBus = new SolicitudTrailerPosibleBus();
            DataTable oListPos = oSolPosBus.SolicitudTrailerPosibleGetAllByNroSS(oSS);
         
            ddl.DataSource = null;
            if (oListPos != null && oListPos.Rows.Count > 0)
            {
                ddl.DataSource = oListPos;
                ddl.DataValueField = "idTrailer";
                if (ddl.ID == "ddlEquipo")
                {
                    ddl.DataTextField = "Trailer";
                }
                else if (ddl.ID == "ddlCuadrilla")
                {
                    ddl.DataTextField = "Cuadrilla";
                }
                ddl.DataBind();
                ddl.Items.Insert(0, new ListItem("Seleccione", "0"));
            }
            else
            {                
                getTrailersDisponibles(ref ddl, oSS);
            }
        }

        /// <summary>
        /// Selecciona equipo o cuadrilla, segun OTI
        /// </summary>
        /// <param name="oOTI"></param>
        private void seleccionarEquipo(Model.OrdenTrabajo oOTI)
        {
            if (ddlEquipo.Items.Count <= 1 || ddlCuadrilla.Items.Count <= 1)
            {                
                string descripcion = "";
                string id = "0";

                if (oOTI.oVehiculo != null)
                {
                    ddlEquipo.Items.Clear();
                    ddlEquipo.Enabled = false;
                    descripcion = oOTI.oVehiculo.oVehiculoIdentificacion.oCodificacion.Codigo + ' ' + oOTI.oVehiculo.oVehiculoIdentificacion.NroCodificacion;
                    id = oOTI.oVehiculo.IdVehiculo.ToString();
                    ddlEquipo.Items.Insert(0, new ListItem("Seleccione", "0"));
                    ddlEquipo.Items.Insert(1, new ListItem(descripcion, id));
                    ddlEquipo.SelectedValue = id;
                }
                else if (oOTI.oCuadrilla != null)
                {
                    ddlCuadrilla.Items.Clear();
                    ddlCuadrilla.Enabled = false;
                    descripcion = oOTI.oCuadrilla.Nombre;
                    id = oOTI.oCuadrilla.IdCuadrilla.ToString();
                    ddlCuadrilla.Items.Insert(0, new ListItem("Seleccione", "0"));
                    ddlCuadrilla.Items.Insert(1, new ListItem(descripcion, id));
                    ddlCuadrilla.SelectedValue = id;
                }                
            }
            else
            {
                if (oOTI.oVehiculo != null)
                {
                    ddlEquipo.SelectedValue = oOTI.oVehiculo.IdVehiculo.ToString();
                }
                else if (oOTI.oCuadrilla != null)
                {
                    ddlCuadrilla.SelectedValue = oOTI.oCuadrilla.IdCuadrilla.ToString();
                }
            }
        }

        #region CARGASOLICITUD
        
        private void CargarSolicitud()
        {
            SolicitudClienteBus oSolicitudClienteBus = new SolicitudClienteBus();
            
            foreach (int idSS in _idsSolicitud)
            {
                SolicitudCliente oSS = oSolicitudClienteBus.SolicitudClienteGetById(idSS);
                if (_idsSolicitud.Length > 1)
                {
                    lblNroSS.Text = oSS.NroSolicitud.ToString();
                }
                else
                {
                    lblNroSS.Text = oSS.NroPedidoCliente == string.Empty ? oSS.NroSolicitud.ToString() + " - " + oSS.letra : oSS.NroPedidoCliente;
                }
                lblFecha.Text = oSS.Fecha.ToString("dd/MM/yyyy");
                lblEmpresa.Text = oSS.oSector.oEmpresa.Descripcion;
                lblSector.Text = oSS.oSector.Descripcion;
                lblSolicitante.Text = oSS.oSolicitante != null ? oSS.oSolicitante.Apellido : "";
                lblCondicion.Text = oSS.oCondicion.Descripcion;

                //CUADRILLA
                if (oSS.oTipoServicio.RequiereCuadrilla)
                {
                    if (oSS.oListEspecificaciones.Count > 0)
                        MostrarCuadrilla(oSS);
                }
                //EQUIPO
                if (oSS.oTipoServicio.RequiereEquipo) 
                {
                    if (oSS.oListEspecificaciones.Count > 0)
                    {
                        if (oSS.oListEspecificaciones.Exists(s => s.RequiereEquipo && s.EquipoPropio))
                        {
                            CargarEquipo(oSS);
                        }
                        if (oSS.oListEspecificaciones.Exists(s => s.RequiereEquipo && !s.EquipoPropio))
                        {
                            CargarEquiposCliente(oSS);
                        }
                    }
                }
                //MOVIMIENTO
                if (oSS.oTipoServicio.RequiereTraslado)
                {
                    CargarMovimientos(oSS, true);
                }
                //REQUIERE PERSONA
                 if (oSS.oTipoServicio.RequierePersonal)
                {
                    CargarConPersona(oSS);
                }
                 if (oSS.oTipoServicio.MasEquipo)
                 {//TRAIGO TODOS LOS TRAILERS PORQUE EN TEORIA, SI REQUIERE PERSONAL Y TIENE ESPECIFICACIONES ES MANTENIMIENTO
                     CargarCheckEquiposPropios();
                     CargarCheckEquiposClientes(oSS.oSector.IdEmpresa);
                 }
                
                //CONEXION/DESCONEXION               
                 if (!oSS.oTipoServicio.RequiereMateriales &&
                     !oSS.oTipoServicio.RequiereTraslado &&
                     !oSS.oTipoServicio.MasEquipo &&
                     !oSS.oTipoServicio.RequierePersonal &&
                     !oSS.oTipoServicio.RequiereCuadrilla &&
                     !oSS.oTipoServicio.RequiereEquipo)
                {
                    CargarConnDesc(oSS);
                }
                //REQUIERE MATERIALES
                if(oSS.oTipoServicio.RequiereMateriales)
                {
                    CargarMateriales(oSS);
                }
            }             
        }

        private void CargarMateriales(SolicitudCliente oSS)
        {
            divMateriales.Visible = true;
            lblTipoServMateriales.Text = oSS.oTipoServicio.Descripcion;
            hfidTipoServMateriales.Value = oSS.oTipoServicio.IdTipoServicio.ToString();
            hfSS_Materiales.Value = oSS.IdSolicitud.ToString();

            List<EspecificacionTecnica> oListET = oSS.oListEspecificaciones.FindAll(s => s.RequiereMaterial);
            lbMateriales.DataSource = oListET;
            lbMateriales.DataBind();
        }

        private void CargarConnDesc(SolicitudCliente oSS)
        {
            if (oSS.oTipoServicio.Descripcion.ToLower().Contains("descone"))
            {
                divDesconexion.Visible = true;
                lblTipoServDescon.Text = oSS.oTipoServicio.Descripcion;
                hfidTipoServDescon.Value = oSS.oTipoServicio.IdTipoServicio.ToString();
                hfSS_Descon.Value = oSS.IdSolicitud.ToString();
            }
            else
            {
                divConexion.Visible = true;
                lblTipoServConn.Text = oSS.oTipoServicio.Descripcion;
                hfidTipoServConn.Value = oSS.oTipoServicio.IdTipoServicio.ToString();
                hfSS_Con.Value = oSS.IdSolicitud.ToString();
            }
        }

        private void CargarConPersona(SolicitudCliente oSS)
        {
            divCantPersonal.Visible = true;
            lblTipoServCantPersonal.Text = oSS.oTipoServicio.Descripcion;
            hfidTipoServCantPersonal.Value = oSS.oTipoServicio.IdTipoServicio.ToString();
            if (oSS.oTipoServicio != null)
                CargarPersona(ddlCantPersonal, oSS);
            hfSS_CantPers.Value = oSS.IdSolicitud.ToString();

        }

        private void CargarCheckEquiposClientes(int idCliente)
        {
            EquiposBus oEq = new EquiposBus();
            chkEquipoCliente.DataSource = oEq.EquiposGetByIdCliente(idCliente);
            chkEquipoCliente.DataBind();
        }

        private void CargarCheckEquiposPropios()
        {
            VehiculoBus oVehiculoBus = new VehiculoBus();
            chkEquiposPropio.DataSource = oVehiculoBus.VehiculoGetEquipos();
            chkEquiposPropio.DataBind();
        }

        private void CargarEquiposCliente(SolicitudCliente oSS)
        {
            //CARGAR LAS ESPECIFICACIONES QUE REQUIEREN EQUIPO Y SON DEL CLIENTE, Y MOSTRAR EL DIV DE LOS EQUIPOS DEL CLIENTE
            divEqCliente.Visible = true;
            lblTipoServEqCliente.Text = oSS.oTipoServicio.Descripcion;
            hfidTipoServEqCliente.Value = oSS.oTipoServicio.IdTipoServicio.ToString();
            hfSS_EqCliente.Value = oSS.IdSolicitud.ToString();

            List<EspecificacionTecnica> oListET = oSS.oListEspecificaciones.FindAll(s => s.RequiereEquipo && !s.EquipoPropio);
            lbEspEqCliente.DataSource = oListET;
            lbEspEqCliente.DataBind();

            //CARGAR LOS EQUIPOS DEL CLIENTE CON ESAS ESPECIFICACIONES
            EquiposBus oEquipoBus = new EquiposBus();
            DataTable dt = oEquipoBus.EquiposGetByClienteEspecificacion(oSS.oSector.IdEmpresa, oListET);
            ddlEqCliente.DataSource = dt;
            ddlEqCliente.DataBind();
            ddlEqCliente.Items.Insert(0, new System.Web.UI.WebControls.ListItem("Seleccione Equipo", "0"));

            if (oSS.oMovimiento != null && divMov.Visible == false)
            {
                CargarMovimientos(oSS, false);
                lblTipoServMov.Text = oSS.oTipoServicio.Descripcion;
            }
        }

        private void CargarEquipo(SolicitudCliente oSS)
        {
            divConEsp.Visible = true;
            lblTipoServConEsp.Text = oSS.oTipoServicio.Descripcion;
            hfidTipoServConEsp.Value = oSS.oTipoServicio.IdTipoServicio.ToString();
            hfSS_ConEsp.Value = oSS.IdSolicitud.ToString();

            ///TRAIGO LAS ESPECIFICACIONES QUE REQUIEREN EQUIPO Y QUE SON PROPIOS
            lbEspecificaciones.DataSource = oSS.oListEspecificaciones.FindAll(s => s.RequiereEquipo && s.EquipoPropio);
            lbEspecificaciones.DataBind();            
            
            VehiculoBus oVehiculoBus = new VehiculoBus();
            if (oSS.nuevoMovimiento || !oSS.Entrega || _idOTI != 0)
            {
                Model.OrdenTrabajo oO = (new OrdenTrabajoBus()).OrdenTrabajoGetByIdSolicitud(oSS.idSolicitudOrig);
                if (oO.oVehiculo != null || oO.oCuadrilla != null)
                    seleccionarEquipo(oO);
            }
            else
            {
                CargarTrailersPosibles(oSS, ref ddlEquipo);
            }

            if (oSS.oMovimiento != null && divMov.Visible == false)
            {
                CargarMovimientos(oSS, false);
                lblTipoServMov.Text = oSS.oTipoServicio.Descripcion;
            }

        }

        private void MostrarCuadrilla(SolicitudCliente oSS)
        {
            divCuadrilla.Visible = true;
            lblTipoServCuadri.Text = oSS.oTipoServicio.Descripcion;
            hfidTipoServCuadri.Value = oSS.oTipoServicio.IdTipoServicio.ToString();
            hfSS_Cuadrilla.Value = oSS.IdSolicitud.ToString();

            lbEspecifCuadri.DataSource = oSS.oListEspecificaciones.FindAll(s=> s.RequiereCuadrilla);
            lbEspecifCuadri.DataBind();
            CuadrillaBus oCuadrillaBus = new CuadrillaBus();
            if (oSS.nuevoMovimiento || !oSS.Entrega || _idOTI > 0)
            {
                Model.OrdenTrabajo oO = (new OrdenTrabajoBus()).OrdenTrabajoGetByIdSolicitud(oSS.idSolicitudOrig);
                if (oO.oVehiculo != null || oO.oCuadrilla != null)
                    seleccionarEquipo(oO);
            }
            else
            {
                CargarTrailersPosibles(oSS, ref ddlCuadrilla);
            }            

            if (oSS.oMovimiento != null && divMov.Visible == false)
            {
                CargarMovimientos(oSS, false);
                lblTipoServMov.Text = oSS.oTipoServicio.Descripcion;
            }
        }


        private void CargarMovimientos(SolicitudCliente oSS, bool div2MovVisible)
        {
            divMov.Visible = true;
            div1Mov.Visible = true;
            div2Mov.Visible = div2MovVisible;


            CargarVehiculo(oSS);
            if (oSS.oTipoServicio.RequiereChofer)
            {   //REQUIERE CHOFER SECTOR MANTENIMIENTO??
                divAcomp.Visible = true;
                divChofer.Visible = true;
                CargarPersona(ddlAcompanante, oSS);
                CargarPersona(ddlChofer,oSS);
            }

            lblTipoServMov.Text = oSS.oTipoServicio.Descripcion;
            hfidTipoServMov.Value = oSS.oTipoServicio.IdTipoServicio.ToString();
            hfIdSS_mov.Value = oSS.IdSolicitud.ToString();
            if (oSS.oMovimiento != null)
            {
                lblOrigen.Text = oSS.oMovimiento.oLugarOrigen.Descripcion;
                lblPozoOrigen.Text = oSS.oMovimiento.PozoOrigen;
                lblDestino.Text = oSS.oMovimiento.oLugarDestino.Descripcion;
                lblPozoDestino.Text = oSS.oMovimiento.PozoDestino;
                lblKms.Text = oSS.oMovimiento.KmsEstimados.ToString();
            }
        }
        
        #endregion

        private void CargarPersona(object obj, SolicitudCliente oSS)
        {
            PersonalBus oPersonalBus = new PersonalBus();
            if (obj is CheckBoxList)
            {
                CheckBoxList chk = (CheckBoxList)obj;

                chk.DataSource = oPersonalBus.PersonalGetByIdSectorEmpresa(oSS.oTipoServicio.idSector.ToString(),"0");
                chk.DataTextField = "ApellidoNombre";
                chk.DataValueField = "idPersonal";
                chk.DataBind();            
            }
            else
            {
               DropDownList ddl = (DropDownList)obj ;

               ddl.DataSource = oPersonalBus.PersonalGetLogistica();
               ddl.DataTextField = "Apellido";
               ddl.DataValueField = "idPersonal";
               ddl.DataBind();
               ddl.Items.Insert(0, new System.Web.UI.WebControls.ListItem("Seleccione Personal", "0"));
            }                        
        }

        private void CargarVehiculo(SolicitudCliente oSS)
        {
            VehiculoBus oVehiculoBus = new VehiculoBus();
            DataTable dt = oVehiculoBus.VehiculosGetByAcuerdoCliente(oSS.oSector.IdEmpresa, oSS.IdSector);
            ddlVehiculo.DataSource = dt;
            ddlVehiculo.DataTextField = "codificacion";
            ddlVehiculo.DataValueField = "idVehiculo";
            ddlVehiculo.DataBind();
            ddlVehiculo.Items.Insert(0, new System.Web.UI.WebControls.ListItem("Seleccione un vehículo", "0"));
            
        }

        private void getTrailersDisponibles(ref DropDownList ddl, SolicitudCliente oSS)
        {
            TrailerBus oVehiculoBus = new TrailerBus();
            SolicitudClienteBus oSolicitudClienteBus = new SolicitudClienteBus();

            DataTable dt = oVehiculoBus.TrailerGetDisponibles(oSS.oListEspecificaciones, oSS.IdTipoServicio);
            ddl.DataSource = dt;
            ddl.DataTextField = "Trailer";
            ddl.DataValueField = "idVehiculo";
            ddl.DataBind();
            ddl.Items.Insert(0, new System.Web.UI.WebControls.ListItem("Seleccione...", "0"));
            ddl.Enabled = true;
            if (oSS.oTipoServicio.RequiereEquipo)
            {
                lblAvisoEquipo.Visible = true;
                oSS.IdCondicion = (int)Condicion.id.diario;
                oSolicitudClienteBus.SolicitudClienteUpdate(oSS);
                lblCondicion.Text = Condicion.id.diario.ToString();
            }
            if(oSS.oTipoServicio.RequiereCuadrilla)
                lblAvisoCuadrilla.Visible = true;
        }
        
        #endregion

    }
}