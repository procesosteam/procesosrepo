﻿<%@ Page Title="" Language="C#" MasterPageFile="~/frmPrincipal.Master" AutoEventWireup="true"
    CodeBehind="frmSSParte.aspx.cs" Inherits="ProcesoPP.UI.SSe3.frmSSParte" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        $(document).ready(function () { SetMenu("liOperaciones"); });

        function validarAca() {

            msj = 'txtNroParte|Debe ingresar un Nro de parte.';
            return ValidarObj(msj)
        }

        function sumar(txtCant, txtHs, lbl) {
            oTxt = document.getElementById('ctl00_ContentPlaceHolder1_' + txtCant);
            oCantHs = document.getElementById("ctl00_ContentPlaceHolder1_" + txtHs);
            lbl = document.getElementById("ctl00_ContentPlaceHolder1_" + lbl);
            lbl.innerText = oTxt.value * oCantHs.value;
            
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="apertura">
        Parte Entrega</div>
    <div class="solicitud100">
        <div class="regreso_3">
            <label class="numeracion">
                Empresa</label>
            <asp:Label ID="lblEmpresa" runat="server" />
        </div>
        <div class="regreso_3">
            <label class="numeracion">
                Sector</label>
            <asp:Label ID="lblSector" runat="server" />
        </div>
        <div class="regreso_3">
            <label class="numeracion">
                Solicitante</label>
            <asp:Label ID="lblSolicitante" runat="server" />
        </div>
    </div>
    <div class="regreso_3">
        <label class="numeracion">
            SS N°:
        </label>
        <asp:Label runat="server" ID="lblNroSS" />
    </div>
    <div class="regreso_3">
        <label class="numeracion">
            N° Parte:
        </label>
        <asp:TextBox runat="server" ID="txtNroParte" />
    </div>
    <div id="titulos">
        Tipo de Servicio</div>
    <div id="divConEsp" runat="server" visible="false">
        <div class="solicitud100">
            <div class="nuevaSS_titleServ">
                <asp:Label runat="server" ID="lblTipoServConEsp" Text="Con Especificacion"></asp:Label>
                <asp:HiddenField runat="server" ID="hfidTipoServConEsp" />
            </div>
        </div>
        <div class="solicitud100">
            <div class="regreso_3">
                <label>Equipo</label>
            </div>
            <div class="regreso_3">
                <asp:Label runat="server" ID="lblEquipo" /></div>
        </div>
    </div>
    <div id="divCuadrilla" runat="server" visible="false">
        <div class="solicitud100">
            <div class="nuevaSS_titleServ">
                <asp:Label runat="server" ID="lblTipoServCuadrilla" Text="Cuadrilla"></asp:Label>
                <asp:HiddenField runat="server" ID="hfidTipoServCuadrilla" />
            </div>
        </div>
        <div class="solicitud100">
            <div class="regreso_3">
                <label>Cuadrilla</label>
            </div>
            <div class="regreso_3">
                <asp:Label runat="server" ID="lblCuadrilla" /></div>
        </div>
        <div class="solicitud100">
            <div class="sol_tit">
                <label>Cant. Hs. Trabajadas:</label></div>
            <div class="sol_tit30">
                <asp:TextBox runat="server" cssclass="sol_personal2" ID="txtHsCuadrilla"> </asp:TextBox></div>
            <div class="sol_titb2">
                <label>Cant. Hs. Espera:</label></div>
            <div class="sol_tit30">
                <asp:TextBox runat="server" ID="txtHsEsperaCuadrilla" cssclass="sol_personal2"> </asp:TextBox></div>
            
        </div>
    </div>
    <div id="divMov" runat="server" visible="false">
        <div class="sol_100">
            <div class="nuevaSS_titleServ">
                <asp:Label runat="server" ID="lblTipoServMov" Text="Movimiento"></asp:Label>
                <asp:HiddenField runat="server" ID="hfidTipoServMov" />
            </div>
        </div>
        <div class="solicitud100">
            <div class="sol_tit2">
                <label>Origen</label></div>
            <div class="sol_tit">
                <asp:Label Text="BPP" ID="lblOrigen" runat="server" />
            </div>
            <div class="sol_tit">
                <label>Pozo Origen</label></div>
            <div class="sol_tit">
                <asp:Label Text="Pozo" ID="lblPozoOrigen" runat="server" />
            </div>
        </div>
        <div class="solicitud100">
            <div class="sol_tit2">
                <label>Destino</label></div>
            <div class="sol_tit">
                <asp:Label Text="LLL" ID="lblDestino" runat="server" />
            </div>
            <div class="sol_tit">
                <label>Pozo Destino</label></div>
            <div class="sol_tit">
                <asp:Label Text="Pozo" ID="lblPozoDestino" runat="server" />
            </div>
            <div class="sol_tit2">
                <label>Kms</label></div>
            <div class="sol_tit">
                <asp:Label Text="150" ID="lblKms" runat="server" />
            </div>
        </div>
        <div class="solicitud100">
   
        <div class="sol_tit2">
            <label>Vehículo:</label></div>
        <div class="sol_tit">
            <asp:Label runat="server" ID="lblVehiculo"> </asp:Label></div>
        <div class="sol_tit2">
            <label>Chofer:</label></div>
        <div class="sol_tit">
            <asp:Label runat="server" ID="lblChofer"> </asp:Label></div>
        <div class="sol_tit2">
            <label>Acompañante:</label></div>
        <div class="sol_tit">
            <asp:Label runat="server" ID="lblAcompanante"> </asp:Label></div>
    </div>
    
   </div>     
    <div id="divCantPersonal" runat="server" visible="false">
        <div class="sol_100">
            <div class="nuevaSS_titleServ">
                <asp:Label runat="server" ID="lblTipoServCantPersonal" Text="Campamento"></asp:Label>
                <asp:HiddenField runat="server" ID="hfidTipoServCantPersonal" />
            </div>
        </div>
        <div class="solicitud100">
            <div class="sol_tit" style="height:auto;"><label>Personal Afectado/ Nombre y Apellido:</label></div> <div class="sol_tit30"></div>
            <div class="sol_per" style="height:auto;">
                <asp:GridView runat="server" AutoGenerateColumns="false" ID="gvPersonal" ShowHeader="false" GridLines="None">
                <Columns>
                    <asp:BoundField DataField="idPersonal" Visible="false"/>
                    <asp:BoundField DataField = "Apellido"/>
                </Columns>
                </asp:GridView>
            </div>
        </div>
        <div class="solicitud100">
            <div class="sol_tit">
                <label>Cant. Personal:</label></div>
            <div class="sol_tit30">
                <asp:TextBox runat="server" cssclass="sol_personal" ID="txtCantPersonal" onkeyup="return sumar('txtCantPersonal','txtHs','lblTotalHs');" onkeypress="return ValidarNumeroEntero(event, this);"> </asp:TextBox></div>
            <div class="sol_titb2">
                <label>Cant. Hs. Trabajadas por Persona:</label></div>
            <div class="sol_tit30">
                <asp:TextBox runat="server" ID="txtHs" cssclass="sol_personal2" onkeyup="return sumar('txtCantPersonal','txtHs','lblTotalHs');" onkeypress="return ValidarNumeroEntero(event, this);"> </asp:TextBox></div>
            <div class="sol_tit">
                <label>Total Hs. Trabajadas:</label></div>
            <div class="sol_tit30">
                <asp:Label runat="server" cssclass="sol_personal2" ID="lblTotalHs" Text="0"/></div>
        </div>
        <div class="solicitud100">
            <div class="sol_tit"></div>
            <div class="sol_tit30"></div>
            <div class="sol_titb2">
                <label>Cant. Hs. Espera por Persona:</label></div>
            <div class="sol_tit30">
                <asp:TextBox runat="server" ID="txtHsEspera" cssclass="sol_personal2" onkeyup="return sumar('txtCantPersonal','txtHsEspera','lblTotalHsEspera');" onkeypress="return ValidarNumeroEntero(event, this);"> </asp:TextBox></div>
            <div class="sol_tit">
                <label>Total. Hs. Espera:</label></div>
            <div class="sol_tit30">
                <asp:Label runat="server" cssclass="sol_personal2" ID="lblTotalHsEspera" Text="0"/></div>       
        </div>        
    </div>
    <div id="divSinEsp" runat="server" visible="false">
        <div class="sol_100">
            <div class="nuevaSS_titleServ">
                <asp:Label runat="server" ID="lblTipoServSinEsp" Text="Sin Esp"></asp:Label>
                <asp:HiddenField runat="server" ID="hfidTipoServSinEsp" />
            </div>
        </div>
        <div class="solicitud100">
            <div class="regreso_3">
                <label>Detalle:</label></div>
            <div class="regreso_2">
                <asp:TextBox runat="server" ID="txtDetalle" TextMode="MultiLine" Rows="4" Width="90%" Enabled="false"> </asp:TextBox></div>
        </div>
    </div>
    <div id="divConexion" runat="server" visible="false">
        <div class="sol_100">
            <div class="nuevaSS_titleServ">
                <asp:Label runat="server" ID="lblTipoServConn"></asp:Label>
                <asp:HiddenField runat="server" ID="hfidTipoServConn" />
            </div>
        </div>
        <div class="solicitud100">
            <div class="regreso_3">
                <label>Conexion:</label></div>
            <div class="regreso_2">
                <asp:TextBox runat="server" ID="txtConexion" onkeypress="return ValidarNumeroEntero(event,this);"> </asp:TextBox></div>           
        </div>
    </div>

    <div id="divDesconexion" runat="server" visible="false">
        <div class="sol_100">
            <div class="nuevaSS_titleServ">
                <asp:Label runat="server" ID="lblTipoServDescon"></asp:Label>
                <asp:HiddenField runat="server" ID="hfidTipoServDescon" />
            </div>
        </div>
        <div class="solicitud100">
             <div class="regreso_3">
                <label>Desconexión:</label></div>
            <div class="regreso_2">
                <asp:TextBox runat="server" ID="txtDesconexion" onkeypress="return ValidarNumeroEntero(event,this);"> </asp:TextBox></div>
        </div>
    </div>

    <div id="divMateriales" runat="server" visible="false">
        <div class="sol_100">
            <div class="nuevaSS_titleServ">
                <asp:Label runat="server" ID="lblTipoServMateriales"></asp:Label>
                <asp:HiddenField runat="server" ID="hfidTipoServMateriales" />
            </div>
        </div>
        <div class="solicitud100">
            <div class="regreso_3"><label>Detalle:</label></div>
            <div class="regreso_2">               
                 <asp:BulletedList ID="lbMateriales" DataTextField="Descripcion" DataValueField="idEspecificaciones" runat="server">
                </asp:BulletedList>
            </div>
   
        </div>
        <div class="solicitud100">
            <div class="regreso_3">
                <label>Cantidad:</label></div>
            <div class="regreso_2">
                <asp:TextBox runat="server" ID="txtCantidadMateriales" onkeypress="return ValidarNumeroEntero(event,this);"> </asp:TextBox></div>           
        </div>
    </div>
    <div id="divObs">
        <div class="solicitud100">
        <div class="regreso_3">
                <label>Observación:</label></div>
            <div class="regreso_2">
                <asp:TextBox runat="server" ID="txtObs" TextMode="MultiLine" Width="90%" Rows="4"> </asp:TextBox></div>
        </div>
    </div>
    <div id="solicitud100">
        <div class="sol_btn">
            <asp:Button ID="btnGuardar" runat="server" CssClass="btn_form" Text="GUARDAR" OnClientClick="return validarAca();"
                OnClick="btnGuardar_Click" /></div>
        <div class="sol_btn">
            <input type="button" id="btnVolver" value="VOLVER" class="btn_form" onclick="IrA('');" />
        </div>
    </div>
</asp:Content>
