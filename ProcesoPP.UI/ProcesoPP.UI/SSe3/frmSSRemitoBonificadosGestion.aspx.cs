﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ProcesoPP.Business;
using ProcesoPP.Services;
using ParteDiario.Services;
using System.Data;
using System.Configuration;
using ProcesoPP.Model;
using AjaxControlToolkit;
using System.Web.Services;

namespace ProcesoPP.UI.SSe3
{
    public partial class frmSSRemitoBonificadoGestion : System.Web.UI.Page
    {
        #region PROPIEDADES

        public Permisos _Permiso
        {
            get { return (Permisos)ViewState["permiso"]; }
            set { ViewState["permiso"] = value; }
        }

        public int _idUsuario
        {
            get { return (int)ViewState["idUsuario"]; }
            set { ViewState["idUsuario"] = value; }
        }
        public int _ADM
        {
            get { return int.Parse(ConfigurationManager.AppSettings["ADM"].ToString()); }
        }
        #endregion

        #region EVENTOS

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                _idUsuario = Common.EvaluarSession(Request, this.Page);
                if (_idUsuario != 0)
                {
                    _Permiso = Common.getModulo(Request, this.Page);
                    if (_Permiso != null && _Permiso.Lectura)
                    {
                        if (!IsPostBack)
                        {
                            CargarEquipo();
                            CargarTipoServicio();
                            CargarCondicion();
                            ddlSector.Items.Insert(0, new ListItem("Seleccione...", "0"));
                        }
                    }
                    else
                    {
                        Mensaje.errorMsj("NO TIENE PERMISOS A ESTE FORMULARIO", this.Page, "SIN ACCESO", "../frmPrincipal.aspx");
                    }
                }
                else
                {
                    Mensaje.errorMsj("DEBE LOGUEARSE", this.Page, "LOGIN", "../frmLogin.aspx");
                }
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                string idempresa = ddlEmpresa.SelectedValue == string.Empty ? "0" : ddlEmpresa.SelectedValue;
                string idsector = ddlSector.SelectedValue == string.Empty ? "0" : ddlSector.SelectedValue;
                RemitosGetByFilter(txtNroSolicitud.Text, idempresa, txtFecha.Text, txtFechaHasta.Text, txtNroParte.Text, ddlTrailer.SelectedValue, idsector, ddlTipoServicio.SelectedValue, ddlCondicion.SelectedValue, "0");
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        protected void gvSolicitud_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
                CargarRow(e);
        }

        protected void gvSolicitud_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvSolicitud.PageIndex = e.NewPageIndex;
            string idempresa = ddlEmpresa.SelectedValue == string.Empty ? "0" : ddlEmpresa.SelectedValue;
            string idsector = ddlSector.SelectedValue == string.Empty ? "0" : ddlSector.SelectedValue;
            RemitosGetByFilter(txtNroSolicitud.Text, idempresa, txtFecha.Text, txtFechaHasta.Text, txtNroParte.Text, ddlTrailer.SelectedValue, idsector, ddlTipoServicio.SelectedValue, ddlCondicion.SelectedValue, "0");
        }
              
        protected void gvSolicitud_RowCommand(object sender, GridViewCommandEventArgs e)
        {

            switch (e.CommandName.ToString())
            {
                
                case "Ver":
                   GridViewRow row = (GridViewRow)((LinkButton)e.CommandSource).NamingContainer;
                   HiddenField hfIdRemito = row.FindControl("hfIdRemito") as HiddenField;
                    Response.Redirect("frmSSRemitoCarga.aspx?idModulo=8&idParte=" + e.CommandArgument.ToString() + "&idRemito=" + hfIdRemito.Value + "&accion=" + e.CommandName);
                    break;
                case "Anular":
                    Anular(e.CommandArgument.ToString());
                    break;
            }
        }

        private string getIdsPartes()
        {
            string ids="";
            foreach (GridViewRow row in gvSolicitud.Rows)
            {
                CheckBox chkUnir = row.FindControl("chkUnir") as CheckBox;
                if (chkUnir.Checked)
                {
                    HiddenField hfIdParte = row.FindControl("hfIdParte") as HiddenField;
                    ids += hfIdParte.Value + "-";
                }
            }

            return ids;
        }

        protected void btExportar_Click(object sender, EventArgs e)
        {
            try
            {
                exportar(txtNroSolicitud.Text, ddlEmpresa.SelectedValue, txtFecha.Text, txtFechaHasta.Text, txtNroParte.Text, ddlEmpresa.SelectedValue, ddlSector.SelectedValue, ddlTipoServicio.SelectedValue, ddlCondicion.SelectedValue, "");
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        #endregion

        #region METODOS

        #region COMBOSAJAX

        [WebMethod]
        public static CascadingDropDownNameValue[] GetEmpresa(string knownCategoryValues, string category)
        {
            EmpresaBus oEmpresaBus = new EmpresaBus();
            List<CascadingDropDownNameValue> empresa = oEmpresaBus.GetDataEmpresa();
            return empresa.ToArray();
        }

        [WebMethod]
        public static CascadingDropDownNameValue[] GetSector(string knownCategoryValues, string category)
        {
            string idEmpresa = CascadingDropDown.ParseKnownCategoryValuesString(knownCategoryValues)["idEmpresa"];
            SectorBus oSectorBus = new SectorBus();
            List<CascadingDropDownNameValue> sectores = oSectorBus.GetDataSector(int.Parse(idEmpresa));
            return sectores.ToArray();
        }

        #endregion

        private void exportar(string nroSolicitud, string idEmpresa, string desde, string hasta, string nroParte, string idEquipo, string idSector, string idTipoServicio, string idCondicion, string chkRem)
        {
            RemitosBus oRemitosBus = new RemitosBus();
            DataTable dt = oRemitosBus.RemitosGetByFilter(nroSolicitud, idEmpresa, desde, hasta, nroParte, idEquipo, idSector, idTipoServicio, idCondicion, "0", chkRem);
            dt.Columns.Remove("idSolicitud");
            dt.Columns.Remove("idOrden");
            dt.Columns.Remove("idVehiculo");
            dt.Columns.Remove("idRemito");
            dt.Columns.Remove("idParte");
            dt.Columns.Remove("idParteEntrega");
            dt.Columns["NroSolicitud"].ColumnName = "N° SS";
            dt.Columns["NroRemito"].ColumnName = "N° Remito";
            dt.Columns["NroParte"].ColumnName = "N° Parte";
            dt.Columns["TipoServicio"].ColumnName = "Tipo Servicio";
            dt.Columns["Trailer"].ColumnName = "Equipo";

            string path = System.IO.Path.Combine(Server.MapPath("~"), "img");
            ExportarExcel oExport = new ExportarExcel("Remitos_" + DateTime.Now.ToString("yyyyMMddHHmmss"), path, "REMITOS");
            oExport.ExportarExcel2003(dt);

        }

        private void Anular(string idRemito)
        {
            RemitosBus oRemitosBus = new RemitosBus();
            Model.Remitos oRemito = new ProcesoPP.Model.Remitos();
            oRemito = oRemitosBus.RemitosGetById(int.Parse(idRemito));
            oRemito.Anulado = true;
            oRemito.FechaAnulado = DateTime.Now;

            oRemitosBus.RemitosUpdate(oRemito);
            string idempresa = ddlEmpresa.SelectedValue == string.Empty ? "0" : ddlEmpresa.SelectedValue;
            string idsector = ddlSector.SelectedValue == string.Empty ? "0" : ddlSector.SelectedValue;
            RemitosGetByFilter(txtNroSolicitud.Text, idempresa, txtFecha.Text, txtFechaHasta.Text, txtNroParte.Text, ddlTrailer.SelectedValue, idsector, ddlTipoServicio.SelectedValue, ddlCondicion.SelectedValue, "0");
        }

        private void CargarRow(GridViewRowEventArgs e)
        {
            
            Common.CargarRow(e, _Permiso);
            
        }

        private void RemitosGetByFilter(string nroSolicitud, string idEmpresa, string desde, string hasta, string nroParte, string idEquipo, string idSector, string idTipoServicio, string idCondicion, string nroRemito)
        {
            RemitosBus oRemitosBus = new RemitosBus();

            gvSolicitud.DataSource = oRemitosBus.RemitosBonificadoGetByFilter(nroSolicitud, idEmpresa, desde, hasta, nroParte, idEquipo, idSector, idTipoServicio, idCondicion,nroRemito);
            gvSolicitud.DataBind();
        }

        private void CargarCondicion()
        {
            CondicionBus oCondicionBus = new CondicionBus();
            oCondicionBus.CargarCombo(ref ddlCondicion, "Seleccione...");
        }

        private void CargarTipoServicio()
        {
            TipoServicioBus oTipoServicioBus = new TipoServicioBus();
            oTipoServicioBus.CargarCombo(ref ddlTipoServicio, "Selecione..");
        }

        private void CargarSector(string idEmpresa)
        {
            SectorBus oSectorBus = new SectorBus();
            oSectorBus.CargarSectorPorEmpresa(ref ddlSector, int.Parse(idEmpresa));
        }

        private void CargarEmpresa()
        {
            EmpresaBus oEmpresaBus = new EmpresaBus();
            oEmpresaBus.CargarEmpresa(ref ddlEmpresa, "Seleccione...");
        }

        private void CargarEquipo()
        {
            VehiculoBus oVehiculoBus = new VehiculoBus();
            oVehiculoBus.CargarComboEquipo(ref ddlTrailer, "Seleccione un equipo...");
        }
        #endregion

    }
}