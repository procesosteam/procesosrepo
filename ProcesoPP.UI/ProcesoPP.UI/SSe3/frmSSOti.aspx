﻿<%@ Page Title="PP - Orden Trabajo" Language="C#" MasterPageFile="~/frmPrincipal.Master" AutoEventWireup="true" CodeBehind="frmSSOti.aspx.cs" Inherits="ProcesoPP.UI.SSe3.frmSSOti" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        $(document).ready(function () { SetMenu("liOperaciones"); }); 

        function validarAca() {

            if (validarPersonal()) {
                msj = 'ddlEquipo|Sele<ccione un equipo.~' +
                  'ddlVehiculo|Seleccione un vehículo.' +
                  '~ddlChofer|Debe seleccionar el chofer.' +
                  '~ddlAcompanante|Debe seleccionar un Acompañante.';
                return ValidarObj(msj);
            }
            else {
                return false;
            }

            
        }

        function validarPersonal() {

            chkList = document.getElementById("<%=ddlCantPersonal.ClientID%>");
            cont = 0;

            for (var i = 0; i < chkList.rows.length; i++) {
                row = chkList.rows[i];
                for (var j = 0; j < row.cells.length; j++) {
                    chk = row.cells[j].childNodes[0];
                    if (chk != null) {
                        if (chk.checked) {
                            cont++;
                        } 
                    }
                }                
            }

            if (cont > 4) {
                alert("No debe exceder las 4 personas seleccionadas.");
                return false;
            }
            else {
                return true;
            }
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="apertura">OTI</div>  

    <div class="regreso_3">
       <label class="numeracion">SS N°: </label>
       <asp:Label runat="server" id="lblNroSS" />
    </div>
     
    <div class="regreso_3">
       <label class="numeracion">Fecha: </label>
        <asp:Label  runat="server" id="lblFecha" />
     </div>
     <div class="regreso_3" >
       <label class="numeracion">Condición: </label>
       <asp:Label  runat="server" id="lblCondicion" />
     </div>
     
    <div id="titulos">Datos del Solicitante.</div>
    <div class="solicitud100">
        <div class="regreso_3">
            <label class="numeracion">Empresa</label>
            <asp:Label ID="lblEmpresa" runat="server"  />
        </div>
        <div class="regreso_3">
            <label class="numeracion">Sector</label>
            <asp:Label ID="lblSector" runat="server" />
        </div>
        <div class="regreso_3">
            <label class="numeracion">Solicitante</label>
            <asp:Label ID="lblSolicitante" runat="server" />
        </div>
    </div>
  
    <div id="titulos">Tipo de Servicio</div>
      
    <div id="divMov" runat="server"  visible="false">
    
        <div id="div1Mov" runat="server">
            <div class="sol_100">
            <div class="nuevaSS_titleServ">
                <asp:Label runat="server" ID="lblTipoServMov" Text="Movimiento"></asp:Label>
                <asp:HiddenField runat="server" ID="hfidTipoServMov" />
                <asp:HiddenField runat="server" ID="hfIdSS_mov" />
            </div>
        </div>
        <div class="solicitud100">
            <div class="sol_tit"><label>Origen</label></div>
            <div class="sol_tit">
                <asp:Label Text="BPP" ID="lblOrigen" runat="server" /> </div>

            <div class="sol_tit"><label>Pozo Origen</label></div>
            <div class="sol_tit">
                <asp:Label Text="Pozo" ID="lblPozoOrigen" runat="server" /> </div>
        </div>
            <div class="solicitud100">
            <div class="sol_tit"><label>Destino</label></div>
            <div class="sol_tit">
                <asp:Label Text="LLL" ID="lblDestino" runat="server" /> </div>

            <div class="sol_tit"><label>Pozo Destino</label></div>
            <div class="sol_tit">
                <asp:Label Text="Pozo" ID="lblPozoDestino" runat="server" /> </div>

            <div class="sol_tit2"><label>Kms</label></div>
            <div class="sol_tit" style="width:5%;">
                <asp:Label Text="150" ID="lblKms" runat="server" /> </div>
        </div>
        </div>
        <div id="div2Mov" runat="server">
            <div class="solicitud100"></div>
            <div class="regreso_3"><label>Vehículo:</label></div>
            <div class="regreso_2"><asp:DropDownList runat="server" ID="ddlVehiculo"> </asp:DropDownList></div>
            <div id="divChofer" runat="server" visible="false">
                <div class="regreso_3"><label>Chofer:</label></div>
                <div class="regreso_2"><asp:DropDownList runat="server" ID="ddlChofer"> </asp:DropDownList></div>
            </div>
            <div id="divAcomp" runat="server" visible="false">
                <div class="regreso_3"><label>Acompañante:</label></div>
                <div class="regreso_2"><asp:DropDownList runat="server" ID="ddlAcompanante"> </asp:DropDownList></div>
            </div>
        </div>
    </div>
      
    <div id="divConEsp" runat="server" visible="false">
        <div class="solicitud100">
            <div class="nuevaSS_titleServ">
                <asp:Label runat="server" ID="lblTipoServConEsp" Text="Requiere Equipo"></asp:Label>
                <asp:HiddenField runat="server" ID="hfidTipoServConEsp" />
                <asp:HiddenField runat="server" ID="hfSS_ConEsp" />
            </div>
        </div>
        <div class="solicitud100">
            <asp:Label runat="server" ID="lblAvisoEquipo" Visible="false" CssClass="boton40" Text="Se muestran los equipos disponibles"></asp:Label>
        </div>
        <div class="solicitud100">
            <div class="regreso_3"><label>Equipo</label>   </div>
            <div class="regreso_3">
                    <asp:DropDownList runat="server" CssClass="sol_campo50c" id="ddlEquipo" DataTextField="Descripcion" DataValueField="idVehiculo" />   </div>
            <div class="regreso_3">
                    <asp:LinkButton runat="server" ID="btnCambiarTrailer" Text="Habilitar Equipo Diario" Visible ="false" CssClass="boton40"
                    onclick="btnCambiarTrailer_Click"></asp:LinkButton>
            </div>
        </div>
        <div class="solicitud100">
            <div class="regreso_3"><label>Especificaciones Tecnicas</label></div>
            <div class="regreso_3">
                <asp:ListBox runat="server" ID="lbEspecificaciones" DataTextField="Descripcion" DataValueField="idEspecificaciones">
                </asp:ListBox>
            </div>
        </div>
    </div>

    <div id="divEqCliente" runat="server" visible="false">
        <div class="solicitud100">
            <div class="nuevaSS_titleServ">
                <asp:Label runat="server" ID="lblTipoServEqCliente" Text="Requiere Equipo"></asp:Label>
                <asp:HiddenField runat="server" ID="hfidTipoServEqCliente" />
                <asp:HiddenField runat="server" ID="hfSS_EqCliente" />
            </div>
        </div>        
        <div class="solicitud100">
            <div class="regreso_3"><label>Equipo</label>   </div>
            <div class="regreso_3">
                    <asp:DropDownList runat="server" CssClass="sol_campo50c" id="ddlEqCliente" DataTextField="Descripcion" DataValueField="idEquipo" />   </div>
            <div class="regreso_3">
                    <asp:LinkButton runat="server" ID="btnCambiarEqCliente" Text="Habilitar Equipo Diario" Visible ="false" CssClass="boton40"
                    onclick="btnCambiarTrailer_Click"></asp:LinkButton>
            </div>
        </div>
        <div class="solicitud100">
            <div class="regreso_3"><label>Especificaciones Tecnicas</label></div>
            <div class="regreso_3">
                <asp:ListBox runat="server" ID="lbEspEqCliente" DataTextField="Descripcion" DataValueField="idEspecificaciones">
                </asp:ListBox>
            </div>
        </div>
    </div>

    <div id="divCuadrilla" runat="server" visible="false">
        <div class="solicitud100">
            <div class="nuevaSS_titleServ">
                <asp:Label runat="server" ID="lblTipoServCuadri" Text="Requiere Cuadrilla"></asp:Label>
                <asp:HiddenField runat="server" ID="hfidTipoServCuadri" />
                <asp:HiddenField runat="server" ID="hfSS_Cuadrilla" />
            </div>
        </div>
        <div class="solicitud100">
            <asp:Label runat="server" ID="lblAvisoCuadrilla" Visible="false" CssClass="boton40" Text="Se muestran los equipos disponibles"></asp:Label>
        </div>
        <div class="solicitud100">
            <div class="regreso_3"><label>Cuadrillas</label>   </div>
            <div class="regreso_3">
                    <asp:DropDownList runat="server" CssClass="sol_campo50c" id="ddlCuadrilla" DataTextField="Nombre" DataValueField="idCuadrilla" />   </div>
            <div class="regreso_3"></div>
        </div>
        <div class="solicitud100">
            <div class="regreso_3"><label>Especificaciones Tecnicas</label></div>
            <div class="regreso_3">
                <asp:ListBox runat="server" ID="lbEspecifCuadri" DataTextField="Descripcion" DataValueField="idEspecificaciones">
                </asp:ListBox>
            </div>
        </div>
    </div>
    
    <div id="divCantPersonal" runat="server" visible="false">
        <div class="sol_100">
            <div class="nuevaSS_titleServ">
                <asp:Label runat="server" ID="lblTipoServCantPersonal" Text="Personal"></asp:Label>
                <asp:HiddenField runat="server" ID="hfidTipoServCantPersonal" />
                <asp:HiddenField runat="server" ID="hfSS_CantPers" />
            </div>
            </div>
            <div class="solicitud100">
                <div class="regreso_3"><label>Personal:</label></div>
                <div class="regreso_2">
                    <asp:CheckBoxList runat="server" ID="ddlCantPersonal" RepeatColumns="3" RepeatDirection="Horizontal">  
                     </asp:CheckBoxList>
                 </div>
            </div>
            <div class="solicitud100">
                <div class="regreso_3"><label>Equipos Propios:</label></div>
                <div class="regreso_2">
                    <asp:CheckBoxList runat="server" ID="chkEquiposPropio" DataTextField="codificacion" RepeatDirection="Horizontal" DataValueField="idVehiculo" RepeatColumns="5">
                    </asp:CheckBoxList>
                </div>
            </div>
            <div class="solicitud100">
                <div class="regreso_3"><label>Equipos Cliente:</label></div>
                <div class="regreso_2">
                    <asp:CheckBoxList runat="server" ID="chkEquipoCliente" DataTextField="Descripcion" RepeatDirection="Horizontal" DataValueField="idEquipo" RepeatColumns="5">
                    </asp:CheckBoxList>
                </div>
            </div>
        </div>

    <div id="divMateriales" runat="server" visible="false">
        <div class="sol_100">
            <div class="nuevaSS_titleServ">
                <asp:Label runat="server" ID="lblTipoServMateriales"></asp:Label>
                <asp:HiddenField runat="server" ID="hfidTipoServMateriales" />
                <asp:HiddenField runat="server" ID="hfSS_Materiales" />
            </div>
        </div>
        <div class="solicitud100">
            <div class="regreso_3"><label>Detalle:</label></div>
            <div class="regreso_2">
                 <asp:BulletedList ID="lbMateriales" DataTextField="Descripcion" DataValueField="idEspecificaciones" runat="server">
                </asp:BulletedList>
            </div>
        </div>
        <div class="solicitud100">
            <div class="regreso_3"><label>Observación:</label></div>
            <div class="regreso_2"><asp:TextBox runat="server" ID="txtDetalle" TextMode="MultiLine" Rows="4" Width="90%"> </asp:TextBox></div>
        </div>
    </div>

    <div id="divConexion" runat="server" visible="false">
        <div class="sol_100">
            <div class="nuevaSS_titleServ">
                <asp:Label runat="server" ID="lblTipoServConn"></asp:Label>
                <asp:HiddenField runat="server" ID="hfidTipoServConn" />
                <asp:HiddenField runat="server" ID="hfSS_Con" />                
            </div>
        </div>
        <div class="solicitud100">
            <div class="sol_tit"><label>Conexion:</label></div>
            <div class="sol_tit"><asp:TextBox runat="server" ID="txtConexion" onkeypress="return ValidarNumeroEntero(event,this);" > </asp:TextBox></div>            
        </div>
    </div>

    <div id="divDesconexion" runat="server" visible="false">
        <div class="sol_100">
            <div class="nuevaSS_titleServ">
                <asp:Label runat="server" ID="lblTipoServDescon"></asp:Label>
                <asp:HiddenField runat="server" ID="hfidTipoServDescon" />
                <asp:HiddenField runat="server" ID="hfSS_Descon" />                
            </div>
        </div>
        <div class="solicitud100">
            <div class="sol_tit"><label>Desconexión:</label></div>
            <div class="sol_tit"><asp:TextBox runat="server" ID="txtDesconexion" onkeypress="return ValidarNumeroEntero(event,this);" > </asp:TextBox></div>
        </div>
    </div>

    <div id="solicitud100"> 
        <div class="sol_btn">
            <asp:Button ID="btnGuardar" runat="server" CssClass="btn_form" Text="GUARDAR" 
                OnClientClick="return validarAca();"
                onclick="btnGuardar_Click" /></div>
        <div class="sol_btn">
            <input type="button" id="btnVolver" value="VOLVER" class="btn_form" onclick="IrA('');"/>
        </div>       
    </div>

</asp:Content>
