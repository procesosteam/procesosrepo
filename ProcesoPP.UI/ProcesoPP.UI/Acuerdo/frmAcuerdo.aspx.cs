﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ProcesoPP.Model;
using ProcesoPP.Services;
using ProcesoPP.Business;
using System.Data;
using System.Web.UI.HtmlControls;
using System.Collections;
using Newtonsoft.Json;
using System.Web.Script.Serialization;
using System.Configuration;
using System.Web.Services;
using AjaxControlToolkit;

namespace ProcesoPP.UI.Acuerdo
{
    public partial class frmAcuerdo : System.Web.UI.Page
    {
        #region PROPIEDADES
        public int _idUsuario
        {
            get { return (int)ViewState["idUsuario"]; }
            set { ViewState["idUsuario"] = value; }
        }

        public int _idAcuerdo
        {
            get { return (int)ViewState["idAcuerdo"]; }
            set { ViewState["idAcuerdo"] = value; }
        }

        public Permisos _Permiso
        {
            get { return (Permisos)ViewState["permiso"]; }
            set { ViewState["permiso"] = value; }
        }

        public List<DataTable> _listDt
        {
            get { return (List<DataTable>)ViewState["listDts"]; }
            set { ViewState["listDts"] = value; }
        }

        public DataTable _Dt
        {
            get { return (DataTable)ViewState["dtHs"]; }
            set { ViewState["dtHs"] = value; }
        }

        public DataTable _DtPrecios
        {
            get { return (DataTable)ViewState["dtPrecios"]; }
            set { ViewState["dtPrecios"] = value; }
        }

        public int _ADM
        {
            get { return int.Parse(ConfigurationManager.AppSettings["ADM"].ToString()); }
        }

        public bool _renova
        {
            get { return (bool)ViewState["renova"]; }
            set { ViewState["renova"] = value; }
        }
        public string _ver
        {
            get { return (string)ViewState["ver"]; }
            set { ViewState["ver"] = value; }
        }

        #endregion
        
        #region EVENTOS

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                _idUsuario = Common.EvaluarSession(Request, this.Page);
                if (_idUsuario != 0)
                {
                    _Permiso = Common.getModulo(Request, this.Page);
                    if (_Permiso != null && _Permiso.Lectura)
                    {
                        if (!IsPostBack)
                        {
                            InitScreen();
                            CargarClientes();                            
                        }
                        
                    }
                    else
                    {
                        Mensaje.errorMsj("NO TIENE PERMISOS A ESTE FORMULARIO", this.Page, "SIN ACCESO", "../frmPrincipal.aspx");
                    }
                }
                else
                {
                    Mensaje.errorMsj("DEBE LOGUEARSE", this.Page, "LOGIN", "../frmLogin.aspx");
                }
            }
            catch (Exception ex)
            {
                if (ex.Data.Count > 0)
                {
                    Mensaje.errorMsj("Ha ocurrido un error. \nDetalle: " + ex.Source, this.Page, "Error", ex.Data["return"].ToString());
                }
                else
                {
                    Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
                }
            }

        }

        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                GuardarAcuerdo(true);
            }
            catch (Exception ex)
            {
                hfError.Value = ex.StackTrace;
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message + " - Source: " + ex.Source, this.Page, "Error", null);
            }
        }

        protected void btnBorrador_Click(object sender, EventArgs e)
        {
            try
            {
                GuardarAcuerdo(false);
            }
            catch (Exception ex)
            {
                hfError.Value = ex.StackTrace;
                Mensaje.errorMsj("Ha ocurrido un error al guardar el borrador. Detalle: " + ex.Message + " - Source: " + ex.Source, this.Page, "Error", null);
            }
        }

        protected void btnGuardarEquipo_Onclick(object sender, EventArgs e)
        {
            try
            {
                GuardarTrailer();
            }
            catch (Exception ex)
            {
                hfError.Value = ex.StackTrace;
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message + " - Source: " + ex.Source, this.Page, "Error", null);
            }
        }

        #endregion
        
        #region METODOS

        [WebMethod]
        public static CascadingDropDownNameValue[] GetEmpresa(string knownCategoryValues, string category)
        {
            EmpresaBus oEmpresaBus = new EmpresaBus();
            List<CascadingDropDownNameValue> empresa = oEmpresaBus.GetDataEmpresa();
            return empresa.ToArray();
        }

        [WebMethod]
        public static CascadingDropDownNameValue[] GetSector(string knownCategoryValues, string category)
        {
            string idEmpresa = CascadingDropDown.ParseKnownCategoryValuesString(knownCategoryValues)["idEmpresa"];
            SectorBus oSectorBus = new SectorBus();
            List<CascadingDropDownNameValue> sectores = oSectorBus.GetDataSector(int.Parse(idEmpresa));
            return sectores.ToArray();
        }

        private void InitScreen()
        {
            _idAcuerdo = 0;
            _renova = false;
            _ver = string.Empty;

            if (Request["idAcuerdo"] != null)
                _idAcuerdo = int.Parse(Request["idAcuerdo"]);
            
            if (Request["renovar"] != null)
                _renova = bool.Parse(Request["renovar"]);

            if (Request["Accion"] != null && Request["Accion"].Equals("Ver"))
                _ver = Request["Accion"];
            
            if (_idAcuerdo > 0)
            {                
                CargarAcuerdo();
                if (_ver.Equals ("Ver"))
                {
                    Common.AnularControles(this.Form);
                    btnGuardar.Visible = false;
                    btnBorrador.Visible = false;
                }
            }
            else
            {
                CargarTipoServiciosHTML(null);
                txtFechaEmision.Text = DateTime.Today.ToString("dd/MM/yyyy");
                lblRevision.Text = "0";
            }
            
            if (_renova)
            {
                txtTitle.Enabled = false;
                txtNroAcuerdo.Enabled = false;
                ddlCliente.Enabled = false;
                txtFecha.Text = string.Empty;
                txtFechaFin.Text = string.Empty;
            }
            else
            {
                txtTitle.Enabled = true;
                txtNroAcuerdo.Enabled = true;
                ddlCliente.Enabled = true;
            }
            
        }
                       
        private void CargarClientes()
        {
            EmpresaBus oEmpresaBus = new EmpresaBus();
            oEmpresaBus.CargarEmpresa(ref ddlCliente, "Seleccione...");
        }

        #region CargarGrillas
        private void CargarTipoServiciosHTML(List<AcuerdoServicio> oListAcuerdoServ)
        {
            _listDt = new List<DataTable>();
            this.PlaceHolder1.Controls.Clear();

            GridView dgServ = new GridView();
            dgServ.Enabled = false;

            TipoServicioBus oTipoServicioBus = new TipoServicioBus();
            List<TipoServicio> oListServ = oTipoServicioBus.TipoServicioGetAll();

            foreach (TipoServicio servicio in oListServ)
            {                
                HtmlGenericControl liMenu_acu = new HtmlGenericControl();
                liMenu_acu.TagName = "li";
                liMenu_acu.Attributes.Add("class", "menu_acu");

                HtmlGenericControl ul = new HtmlGenericControl();
                ul.TagName = "ul";

                HtmlGenericControl liButton = new HtmlGenericControl();
                liButton.TagName = "li";
                liButton.Attributes.Add("class", "button");

                HtmlAnchor lk = new HtmlAnchor();
                lk.HRef = "#";

                HtmlGenericControl liDropdown = new HtmlGenericControl();
                liDropdown.TagName = "li";
                liDropdown.Attributes.Add("class", "dropdown");

                HtmlGenericControl tablas_acuerdo = new HtmlGenericControl();
                tablas_acuerdo.TagName = "div";
               
                lk.InnerHtml = servicio.Descripcion;
                liButton.Controls.Add(lk);
                
                CrearGrillasServicios(dgServ, servicio, tablas_acuerdo, oListAcuerdoServ);
                               
                liDropdown.Controls.Add(tablas_acuerdo);
                ul.Controls.Add(liButton);
                ul.Controls.Add(liDropdown);
                liMenu_acu.Controls.Add(ul);
                this.PlaceHolder1.Controls.Add(liMenu_acu);
                
            }
        }

        private GridView createNuevaGrilla(GridView dgServ, string descripcion)
        {
            dgServ = new GridView();

            dgServ.ID = "dg_" + descripcion.Replace(" ", "");
            dgServ.CssClass = "tabla_acuerdo";
            dgServ.AutoGenerateColumns = false;
            dgServ.EnableViewState = false;

            addCol(dgServ, "idTipoServicio", ListItemType.Item, "idTipoServicio", "td_espe", "td_espe1", false, false, typeof(int));
            addCol(dgServ, "idEspecificacion", ListItemType.Item, "idEspecificacion", "td_espe", "td_espe1", false, false, typeof(int));
            addCol(dgServ, "Detalle", ListItemType.Item, "descripcion", "td_espe", "td_espe1", true, false, typeof(string));

            return dgServ;
        }

        private void CrearGrillasServicios(GridView dgServ, TipoServicio servicio, HtmlGenericControl tablas_acuerdo, List<AcuerdoServicio> oListAcuerdoServ)
        {
            HiddenField hf = new HiddenField();
            DataTable dt = new DataTable();

            if (servicio.RequiereMateriales)
            {
                HtmlGenericControl divMat = AgregarDivRequiere("MATERIALES");
                
                dt = new DataTable();
                dgServ = createNuevaGrilla(dgServ, servicio.Descripcion + "_materiales");
                agregarColumnasConcepto(dgServ, "M");
                if (oListAcuerdoServ != null && (oListAcuerdoServ.Exists(AS => AS.IdTipoServicio == servicio.IdTipoServicio && (AS.requiere == null || AS.requiere.Equals("M")))))
                    dt = createDtConceptoConDatos(dt, oListAcuerdoServ.Find(AS => AS.IdTipoServicio == servicio.IdTipoServicio && (AS.requiere == null || AS.requiere.Equals("M"))), "M");
                else
                    dt = createDtConceptos(dt, servicio, dgServ, "M");
                
                dgServ.DataSource = dt;
                dgServ.DataBind();

                hf.ID = "hf"+dgServ.ID+"Requiere";
                hf.Value = "M";
                tablas_acuerdo.Controls.Add(hf);
                tablas_acuerdo.Controls.Add(divMat);
                tablas_acuerdo.Controls.Add(dgServ);
            }
            if (servicio.RequiereTraslado)
            {
                HtmlGenericControl divtR = AgregarDivRequiere("TRASLADOS");

                dt = new DataTable();
                dgServ = createNuevaGrilla(dgServ, servicio.Descripcion + "_traslado");
                agregarColumnasCodificacion(dgServ);

                if (oListAcuerdoServ != null && (oListAcuerdoServ.Exists(AS => AS.IdTipoServicio == servicio.IdTipoServicio && (AS.requiere == "T" || AS.requiere.Equals("T")))))
                    dt = createDtConceptoConDatos(dt, oListAcuerdoServ.Find(AS => AS.IdTipoServicio == servicio.IdTipoServicio && (AS.requiere == null || AS.requiere.Equals("T"))), "T");
                else
                    dt = createDtCodificacion(dt, servicio, dgServ);
                dgServ.DataSource = dt;
                dgServ.DataBind();

                hf = new HiddenField();
                hf.ID = "hf" + dgServ.ID + "Requiere";
                hf.Value = "T";
                tablas_acuerdo.Controls.Add(hf);
                tablas_acuerdo.Controls.Add(divtR);
                tablas_acuerdo.Controls.Add(dgServ);
            }
            if (servicio.RequierePersonal)
            {
                HtmlGenericControl divpER = AgregarDivRequiere("PERSONAL");

                dt = new DataTable();
                dgServ = createNuevaGrilla(dgServ, servicio.Descripcion + "_personal");
                agregarColumnasConcepto(dgServ,"P");
                if (oListAcuerdoServ != null && (oListAcuerdoServ.Exists(AS => AS.IdTipoServicio == servicio.IdTipoServicio && (AS.requiere == null || AS.requiere.Equals("P")))))
                   dt = createDtConceptoConDatos(dt, oListAcuerdoServ.Find(AS => AS.IdTipoServicio == servicio.IdTipoServicio && (AS.requiere == null ||AS.requiere.Equals("P"))), "P");
                else
                    dt = createDtConceptos(dt, servicio, dgServ, "P");
                dgServ.DataSource = dt;
                dgServ.DataBind();

                hf = new HiddenField();
                hf.ID = "hf" + dgServ.ID + "Requiere";
                hf.Value = "P";
                tablas_acuerdo.Controls.Add(hf);
                tablas_acuerdo.Controls.Add(divpER);
                tablas_acuerdo.Controls.Add(dgServ);
            }
            if (servicio.RequiereCuadrilla)
            {
                 HtmlGenericControl divpER = AgregarDivRequiere("CUADRILLA");

                dt = new DataTable();
                dgServ = createNuevaGrilla(dgServ, servicio.Descripcion + "_cuadrilla");
                agregarColumnasConcepto(dgServ,"P");
                if (oListAcuerdoServ != null && (oListAcuerdoServ.Exists(AS => AS.IdTipoServicio == servicio.IdTipoServicio && (AS.requiere == null || AS.requiere.Equals("C")))))
                   dt = createDtConceptoConDatos(dt, oListAcuerdoServ.Find(AS => AS.IdTipoServicio == servicio.IdTipoServicio && (AS.requiere == null ||AS.requiere.Equals("C"))), "C");
                else
                    dt = createDtConceptos(dt, servicio, dgServ, "C");
                dgServ.DataSource = dt;
                dgServ.DataBind();

                hf = new HiddenField();
                hf.ID = "hf" + dgServ.ID + "Requiere";
                hf.Value = "C";
                tablas_acuerdo.Controls.Add(hf);
                tablas_acuerdo.Controls.Add(divpER);
                tablas_acuerdo.Controls.Add(dgServ);
            }
            if (servicio.RequiereEquipo)
            {
                HtmlGenericControl divEQ = AgregarDivRequiere("EQUIPO");

                dt = new DataTable();
                dgServ = createNuevaGrilla(dgServ, servicio.Descripcion + "_equipo");
                agregarColumnasConcepto(dgServ, "E");
                if (oListAcuerdoServ != null && (oListAcuerdoServ.Exists(AS => AS.IdTipoServicio == servicio.IdTipoServicio && (AS.requiere == null || AS.requiere.Equals("E")))))
                   dt= createDtConceptoConDatos(dt, oListAcuerdoServ.Find(AS => AS.IdTipoServicio == servicio.IdTipoServicio && (AS.requiere == null ||AS.requiere.Equals("E"))), "E");
                else
                    dt = createDtConceptos(dt, servicio, dgServ, "E");
                dgServ.DataSource = dt;
                dgServ.DataBind();

                hf = new HiddenField();
                hf.ID = "hf" + dgServ.ID + "Requiere";
                hf.Value = "E";
                tablas_acuerdo.Controls.Add(hf);
                tablas_acuerdo.Controls.Add(divEQ);
                tablas_acuerdo.Controls.Add(dgServ);
            }

            if (!servicio.RequiereMateriales &&
                !servicio.RequiereTraslado &&
                !servicio.MasEquipo &&
                !servicio.RequierePersonal &&
                !servicio.RequiereCuadrilla &&
                !servicio.RequiereEquipo)
            {
                dt = new DataTable();
                dgServ = createNuevaGrilla(dgServ, servicio.Descripcion + "_NoRequiere");
                agregarColumnasConcepto(dgServ, "");
                if (oListAcuerdoServ != null && (oListAcuerdoServ.Exists(AS => AS.IdTipoServicio == servicio.IdTipoServicio && (AS.requiere == null || AS.requiere.Equals("")))))
                    dt = createDtConceptoConDatos(dt, oListAcuerdoServ.Find(AS => AS.IdTipoServicio == servicio.IdTipoServicio && (AS.requiere == null || AS.requiere.Equals(""))), "");
                else
                    dt = createDtConceptos(dt, servicio, dgServ, "");
                dgServ.DataSource = dt;
                dgServ.DataBind();

                hf = new HiddenField();
                hf.ID = "hf" + dgServ.ID + "NoRequiere";
                hf.Value = "E";
                tablas_acuerdo.Controls.Add(hf);
                tablas_acuerdo.Controls.Add(dgServ);
            }
            
        }

        private HtmlGenericControl AgregarDivRequiere(string innerText)
        {
            HtmlGenericControl divRequiere = new HtmlGenericControl();
            divRequiere.InnerHtml = innerText;
            divRequiere.TagName = "div";
            divRequiere.Attributes.Add("class", "solicitud100");
            divRequiere.Attributes.Add("style", "margin-top:15px;");

            return divRequiere;
        }

        private DataTable createDtConceptos(DataTable dt, TipoServicio oTipoServicio, GridView dgServ, string param)
        {            
            dt.TableName = oTipoServicio.Descripcion.Replace(" ", "");
            dt = createColumnDT(dt, oTipoServicio.IdTipoServicio, null, param);
            DataRow dr = null;            
            List<AcuerdoConcepto> OListConceptos = new List<AcuerdoConcepto>();
            OListConceptos = (new AcuerdoConceptoBus()).AcuerdoConceptoByConEsp(param.Equals("C")?"P":param);

            if (oTipoServicio.oEspecificacion.Count == 0)
            {
                dr = dt.NewRow();
                dr["descripcion"] = oTipoServicio.Descripcion;
                dr["idTipoServicio"] = oTipoServicio.IdTipoServicio;
                dr["idEspecificacion"] = "0";
                
                foreach (AcuerdoConcepto item in OListConceptos)
                {
                    dr[item.Concepto] = "";
                }
                dt.Rows.Add(dr);
            }
            else
            {
                bool requiereEquipo = false, requiereCuadrilla = false, requiereMat = false, requiereTra = false, requierePersonal = false ; 
                switch (param)
                {
                    case "C":
                        requiereCuadrilla = true;
                        break;
                    case "E":
                        requiereEquipo = true;
                        break;
                    case "M":
                        requiereMat = true;
                        break;
                    case "T":
                        requiereTra = true;
                        break;
                    case "P":
                        requierePersonal = true;
                        break;
                }
                foreach (EspecificacionTecnica oET in oTipoServicio.oEspecificacion.FindAll(e=> e.RequiereCuadrilla == requiereCuadrilla &&
                                                                                                e.RequierePersonal == requierePersonal &&
                                                                                                e.RequiereTraslado == requiereTra &&
                                                                                                e.RequiereEquipo == requiereEquipo &&
                                                                                                e.RequiereMaterial == requiereMat))
                {
                    dr = dt.NewRow();
                    dr["descripcion"] = oET.Descripcion;
                    dr["idTipoServicio"] = oTipoServicio.IdTipoServicio;
                    dr["idEspecificacion"] = oET.IdEspecificaciones;

                    foreach (AcuerdoConcepto item in OListConceptos)
                    {
                        dr[item.Concepto] = "";
                    }
                    dt.Rows.Add(dr);
                }
            }
            return dt;
        }

        private void agregarColumnasConcepto(GridView dgServ, string param)
        {
            List<AcuerdoConcepto> OListConceptos = new List<AcuerdoConcepto>();
            OListConceptos = (new AcuerdoConceptoBus()).AcuerdoConceptoByConEsp(param);
            foreach (AcuerdoConcepto item in OListConceptos)
            {
                addCol(dgServ, item.Concepto, ListItemType.EditItem, item.Concepto, "td_tit", "td_tit1", true, true, typeof(int));
            }
        }

        private void agregarColumnasCodificacion(GridView dgServ)
        {            
            CodificacionBus oCodBus = new CodificacionBus();
            List<string> oListCod = oCodBus.CodificacionGetVehiculo().Select(c => c.Descripcion).Distinct().ToList();

            foreach (string oCod in oListCod)
            {                
                addCol(dgServ, oCod, ListItemType.EditItem, oCod, "td_tit", "td_tit1", true, true, typeof(string));
            }
            
        }

        private DataTable createDtCodificacion(DataTable dt, TipoServicio oTipoServicio, GridView dgServ)
        {
            dt.TableName = oTipoServicio.Descripcion.Replace(" ", "");
            dt = createColumnDT(dt, oTipoServicio.IdTipoServicio, null, "");
            DataRow dr = null;
            CodificacionBus oCodBus = new CodificacionBus();
            List<Codificacion> oListCod = oCodBus.CodificacionGetVehiculo();
            
            foreach (Codificacion oCod in oListCod)
            {
                if (!dt.Columns.Contains(oCod.Descripcion))
                    dt.Columns.Add(new DataColumn(oCod.Descripcion, typeof(string)));
            }

            foreach (EspecificacionTecnica oET in oTipoServicio.oEspecificacion.FindAll(e => e.RequiereTraslado == true))
            {
                dr = dt.NewRow();
                dr["descripcion"] = oET.Descripcion;
                dr["idTipoServicio"] = oTipoServicio.IdTipoServicio;
                dr["idEspecificacion"] = oET.IdEspecificaciones;

                foreach (Codificacion item in oListCod)
                {
                    dr[item.Descripcion] = "";
                }
                dt.Rows.Add(dr);
            }
            
            return dt;
        }

        private void addCol(GridView dg, string headerText, ListItemType itemType, string itemBound, string headerStyle, string itemStyle, bool visible, bool validarDec, System.Type systemType)
        {
            TemplateField colDescr = new TemplateField();
            colDescr.HeaderTemplate = new DataGridTemplate(ListItemType.Header, headerText,false, typeof(string));
            colDescr.ItemTemplate = new DataGridTemplate(itemType, itemBound, validarDec, systemType);
            if (!visible)
            {
                colDescr.ItemStyle.CssClass = "hidden";
                colDescr.HeaderStyle.CssClass = "hidden";
            }
            else
            {
                colDescr.HeaderStyle.CssClass = headerStyle;
                colDescr.ItemStyle.CssClass = itemStyle;
            }
            if (!dg.Columns.Contains(colDescr))
                dg.Columns.Add(colDescr);
        }

        private DataTable createDtConceptoConDatos(DataTable dt, AcuerdoServicio oAcuerdoServicio, string param)
        {
            bool requiereEquipo = false, requiereCuadrilla = false, requiereMat = false, requiereTra = false, requierePersonal = false;
            switch (param)
            {
                case "C":
                    requiereCuadrilla = true;
                    break;
                case "E":
                    requiereEquipo = true;
                    break;
                case "M":
                    requiereMat = true;
                    break;
                case "T":
                    requiereTra = true;
                    break;
                case "P":
                    requierePersonal = true;
                    break;
            }

            dt.TableName = oAcuerdoServicio.oTipoServicio.Descripcion.Replace(" ", "");
            dt = createColumnDT(dt, oAcuerdoServicio.IdTipoServicio, null, param);

            DataRow dr = null;

            if (oAcuerdoServicio.oTipoServicio.oEspecificacion.Count == 0)
            {
                dr = dt.NewRow();
                dr["descripcion"] = oAcuerdoServicio.oTipoServicio.Descripcion;
                dr["idTipoServicio"] = oAcuerdoServicio.oTipoServicio.IdTipoServicio;
                dr["idEspecificacion"] = "0";

                foreach (AcuerdoConceptoCosto item in oAcuerdoServicio.oListConcepto)
                {
                    dr[item.Concepto] = item.Costo;
                }
                dt.Rows.Add(dr);
            }
            else
            {
                foreach (EspecificacionTecnica oET in oAcuerdoServicio.oTipoServicio.oEspecificacion.FindAll(e => e.RequiereCuadrilla == requiereCuadrilla &&
                                                                                                                 e.RequiereEquipo == requiereEquipo &&
                                                                                                                 e.RequiereMaterial == requiereMat &&
                                                                                                                 e.RequierePersonal == requierePersonal &&
                                                                                                                 e.RequiereTraslado == requiereTra))
                {

                    dr = dt.NewRow();
                    foreach (AcuerdoConceptoCosto oACC in oAcuerdoServicio.oListConcepto.FindAll(oAS => oAS.IdEspecificacion == oET.IdEspecificaciones &&
                                                                                                       (oAS.oEspecificacion.RequiereCuadrilla == requiereCuadrilla &&
                                                                                                        oAS.oEspecificacion.RequierePersonal == requierePersonal &&
                                                                                                        oAS.oEspecificacion.RequiereTraslado == requiereTra &&
                                                                                                        oAS.oEspecificacion.RequiereEquipo == requiereEquipo &&
                                                                                                        oAS.oEspecificacion.RequiereMaterial == requiereMat)))
                    {
                        dr["idTipoServicio"] = oAcuerdoServicio.IdTipoServicio;
                        dr["idEspecificacion"] = oACC.IdEspecificacion;
                        if (dt.Columns.IndexOf(oACC.Concepto) > 0)
                            dr[oACC.Concepto] = oACC.Costo;
                        dr["descripcion"] = oET.Descripcion;
                    }
                    if (!dr[0].ToString().Equals(string.Empty))
                        dt.Rows.Add(dr);
                    else
                    {
                        dr["idTipoServicio"] = oAcuerdoServicio.IdTipoServicio;
                        dr["idEspecificacion"] = oET.IdEspecificaciones;
                        dr["descripcion"] = oET.Descripcion;
                        List<AcuerdoConcepto> OListConceptos = (new AcuerdoConceptoBus()).AcuerdoConceptoByConEsp(param.Equals("C") ? "P" : param);
                        foreach (AcuerdoConcepto item in OListConceptos)
                        {
                            dr[item.Concepto] = "";
                        }
                        dt.Rows.Add(dr);
                    }

                }
            }
            return dt;
        }

        private DataTable createColumnDT(DataTable dt, int idTipoServicio, List<EspecificacionTecnica> oEsp, string conEsp)
        {

            dt.Columns.Add(new DataColumn("idTipoServicio", typeof(int)));
            dt.Columns.Add(new DataColumn("idEspecificacion", typeof(int)));
            dt.Columns.Add(new DataColumn("descripcion", typeof(string)));

            List<AcuerdoConcepto> OListConceptos = new List<AcuerdoConcepto>();
            OListConceptos = (new AcuerdoConceptoBus()).AcuerdoConceptoByConEsp(conEsp.Equals("C") ? "P" : conEsp);
            foreach (AcuerdoConcepto item in OListConceptos)
            {
                if (!dt.Columns.Contains(item.Concepto))
                    dt.Columns.Add(new DataColumn(item.Concepto, typeof(string)));
            }

            if (conEsp=="T" && OListConceptos.Count == 0)
            {
                CodificacionBus oCodBus = new CodificacionBus();
                List<Codificacion> oListCod = oCodBus.CodificacionGetVehiculo();

                foreach (Codificacion oCod in oListCod)
                {
                    if (!dt.Columns.Contains(oCod.Descripcion))
                        dt.Columns.Add(new DataColumn(oCod.Descripcion, typeof(decimal)));
                }

            }

            return dt;
        }
       
        private DataTable createDtEspConDatos(DataTable dt ,AcuerdoServicio oAcuerdoServicio, List<EspecificacionTecnica> oListEsp)
        {            
            dt.TableName = oAcuerdoServicio.oTipoServicio.Descripcion.Replace(" ", "");
            DataRow dr = null;
            foreach (EspecificacionTecnica oEsp in oListEsp)
            {
                List<AcuerdoConceptoCosto> oListACC = new List<AcuerdoConceptoCosto>();
                oListACC = oAcuerdoServicio.oListConcepto.FindAll(a => a.IdEspecificacion == oEsp.IdEspecificaciones);
                dr = dt.NewRow();
                dr["idTipoServicio"] = oAcuerdoServicio.IdTipoServicio;
                dr["idEspecificacion"] = oEsp.IdEspecificaciones;
                if (oListACC.Count > 0)
                {
                    foreach (AcuerdoConceptoCosto oACC in oListACC)
                    {
                        dr["descripcion"] = oACC.oEspecificacion.Descripcion;
                        dr[oACC.Concepto] = oACC.Costo;                    
                    }
                }
                else
                {
                    dr["descripcion"] = oEsp.Descripcion;                    
                }
                dt.Rows.Add(dr);
            }
            return dt;            
        }
        
        #endregion

        #region Guarda

        private void GuardarAcuerdo(bool activo)
        {
            AcuerdoBus oAcuerdoBus = new AcuerdoBus();
            //VerificarFechas controla que la fecha sea mayor al dia de hoy, para que no se carguen contratos viejos,
            //esta validación la sacamos hasta que se carguen los contratos, y queden al día
            bool ver = VerificarFechas(txtFecha.Text, txtFechaFin.Text);
            ver = true;
            if (ver)
            {
                Model.Acuerdo oAcuerdo = new ProcesoPP.Model.Acuerdo();
                if (_idAcuerdo > 0)
                {
                    oAcuerdo = oAcuerdoBus.AcuerdoGetById(_idAcuerdo);
                }
                oAcuerdo.Activo = activo;
                oAcuerdo.NroAcuerdo = txtNroAcuerdo.Text;
                oAcuerdo.FechaInicio = DateTime.Parse(txtFecha.Text);
                oAcuerdo.FechaFin = DateTime.Parse(txtFechaFin.Text);
                oAcuerdo.IdCliente = ddlCliente.SelectedValue == "" ? 0 : int.Parse(ddlCliente.SelectedValue);
                oAcuerdo.oListServicios = CargarJsonServicios();
                if (oAcuerdo.oListServicios != null && oAcuerdo.oListServicios.Count > 0)
                {
                    oAcuerdo.oListTitulos = CargarJsonTitulos();
                    oAcuerdo.FechaEmision = DateTime.Parse(txtFechaEmision.Text);
                    oAcuerdo.NroRevision = int.Parse(lblRevision.Text);
                    oAcuerdo.Titulo = txtTitle.Text;
                    oAcuerdo.TipoMoneda = ddlTipoMoneda.SelectedItem.Text;
                    oAcuerdo.idSector = ddlSector.SelectedValue == string.Empty ? 0 : int.Parse(ddlSector.SelectedValue);
                    _idAcuerdo = oAcuerdo.IdAcuerdo = oAcuerdoBus.AcuerdoAdd(oAcuerdo);

                    if (oAcuerdo.IdAcuerdo > 0)
                    {
                        Mensaje.successMsj("Se ha guardado el acuerdo con éxito!", this, "Exito", null); //"../Reportes/frmAcuertoRPT.aspx?idAcuerdo=" + oAcuerdo.IdAcuerdo.ToString());
                        _renova = false;
                        btnGuardar.Visible = false;
                        btnBorrador.Visible = false;
                        CargarAcuerdo();
                        CargarEspecificacionesAcuerdo();
                        //Mensaje.successMsj("Se ha guardado el acuerdo con éxito!", this, "Exito", "../Reportes/frmAcuertoRPT.aspx?idAcuerdo=" + oAcuerdo.IdAcuerdo.ToString());
                        Mensaje.openAcuerdo(this.Page, "../Reportes/frmAcuertoRPT.aspx?idAcuerdo=" + oAcuerdo.IdAcuerdo.ToString());

                    }
                }
            }
            else
            {
                //Mensaje.errorMsj("Existe un contrato activo para el cliente en estas fechas. Verifique.", this, "Error", null);
                Mensaje.errorMsj("La fecha de Inicio debe ser mayor o igual al día de hoy.", this, "Error", null);
            }
        }

        private bool VerificarFechas(string fechaDesde, string fechaHasta)
        {
            DateTime desde = DateTime.MinValue;
            DateTime hasta = DateTime.MinValue;
            if (DateTime.TryParse(fechaDesde, out desde) && DateTime.TryParse(fechaHasta, out hasta))
            {
                if (desde >= DateTime.Today)
                    return true;
                else
                    return false;
            }
            else
                return false;
        }

        private List<AcuerdoTitulo> CargarJsonTitulos()
        {
            AcuerdoTituloList oNewListTitulos = new AcuerdoTituloList();
            JavaScriptSerializer ser = new JavaScriptSerializer();
            oNewListTitulos = (AcuerdoTituloList)ser.Deserialize<AcuerdoTituloList>(hfTitulos.Value);

            AcuerdoTitulo oAcTitulo = new AcuerdoTitulo();
            oAcTitulo.Titulo = "header";
            oAcTitulo.Detalle = txtHeader.Text;
            oNewListTitulos.acuerdoTitulo.Add(oAcTitulo);

            return oNewListTitulos.acuerdoTitulo;
        }

        private List<AcuerdoServicio> CargarJsonServicios()
        {
            List<AcuerdoServicio> oListAcuerdoServ = new List<AcuerdoServicio>();
            AcuerdoConceptoCosto oACC = new AcuerdoConceptoCosto();
            AcuerdoServicio oAcServ = new AcuerdoServicio();
            JavaScriptSerializer ser = new JavaScriptSerializer();

            object oObj = (object)ser.DeserializeObject(hfServicios.Value);
            Dictionary<string, object> oDir = new Dictionary<string, object>();
            Dictionary<string, object> oDirServ = new Dictionary<string, object>();
            Dictionary<string, object> oDirEsp = new Dictionary<string, object>();
            
            oDir = (Dictionary<string, object>)(oObj);
            object[] oListServ;
            decimal costo;
            object[] oEsp;
            foreach (var item in oDir.Values)
            {
                oListServ = (object[])item;
                //RECORRO LOS TIPO DE SERVICIOS
                foreach (object oServ in oListServ)
                {
                    oDirServ = (Dictionary<string, object>)(oServ);
                    //C/TIPO DE SERV CON SUS ESP
                    
                    oAcServ = new AcuerdoServicio();
                    oAcServ.IdTipoServicio = int.Parse(oDirServ.Single(e => e.Key == "idTipoServicio").Value.ToString());
                    oAcServ.requiere = oDirServ.Single(e => e.Key == "requiere").Value.ToString();

                    oEsp = (object[])(oDirServ.Single(e => e.Key == "oEsp").Value);
                    //RECORRO ESPECIFICACIONES
                    foreach (object oE in oEsp)
                    {
                        oDirEsp = (Dictionary<string, object>)(oE);

                        //RECORRO LOS CONCEPTOS Y COSTOS
                        foreach (KeyValuePair<string, object> acc in oDirEsp)
                        {
                            oACC = new AcuerdoConceptoCosto();
                            oACC.IdEspecificacion = int.Parse(oDirEsp.Single(e => e.Key == "idEspecificacion").Value.ToString());
                            if (acc.Key != "idEspecificacion")
                            {                                
                                oACC.Concepto = acc.Key;
                                if (decimal.TryParse(acc.Value.ToString(), out costo))
                                {
                                    oACC.Costo = costo;
                                    oAcServ.oListConcepto.Add(oACC);
                                }
                            }
                        }                        
                    }
                    if (oAcServ.oListConcepto.Count > 0)
                        oListAcuerdoServ.Add(oAcServ);
                }
            }

            return oListAcuerdoServ;
        }

        #endregion

        #region CargarAcuerdo

        private void CargarAcuerdo()
        {
            AcuerdoBus oAcuerdoBus = new AcuerdoBus();
            Model.Acuerdo oAcuerdo = new ProcesoPP.Model.Acuerdo();
            oAcuerdo = oAcuerdoBus.AcuerdoGetById(_idAcuerdo);
            bool remitos = false;
            txtNroAcuerdo.Text = oAcuerdo.NroAcuerdo ;
            txtFecha.Text = oAcuerdo.FechaInicio.ToString("dd/MM/yyyy");
            txtFechaFin.Text = oAcuerdo.FechaFin.ToString("dd/MM/yyyy");
            //ddlCliente.SelectedValue = oAcuerdo.IdCliente.ToString();
            cddEmpresa.SelectedValue = oAcuerdo.IdCliente.ToString();
            cddSector.SelectedValue = oAcuerdo.idSector.ToString();
            ddlTipoMoneda.SelectedValue = oAcuerdo.TipoMoneda;
            txtFechaEmision.Text = oAcuerdo.FechaEmision == DateTime.MinValue ? "" : oAcuerdo.FechaEmision.ToString("dd/MM/yyyy");
            lblRevision.Text = oAcuerdo.NroRevision.ToString();
            if (_renova)
            {
                lblRevision.Text = (oAcuerdo.NroRevision + 1).ToString();
                remitos = validarRemitos(oAcuerdo);
            }
            txtTitle.Text = oAcuerdo.Titulo;
            txtHeader.Text = oAcuerdo.oHeader.Titulo;             
            CargarTipoServiciosHTML(oAcuerdo.oListServicios);
            CargarTitulos(oAcuerdo.oListTitulos);

        }

        private bool validarRemitos(Model.Acuerdo oAcuerdo)
        {
            ParteEntregaBus oParteEntregaBus = new ParteEntregaBus();
            DataTable dt = oParteEntregaBus.ValidarParteAcuerdo(oAcuerdo.IdAcuerdo);

            if (dt != null && dt.Rows.Count>0)
            {
                Exception ex = new Exception();
                ex.Data.Add("return", "frmAcuerdoGestion.aspx?idModulo="+ _Permiso.oModulo.idModulo);
                ex.Source = "Existen Partes pendientes para este acuerdo.";
                throw ex;
            }

            return true;

        }

        private void CargarTitulos(List<AcuerdoTitulo> list)
        {
            int i = 1;
            foreach (AcuerdoTitulo oTit in list)
            {

                HtmlGenericControl divCont = new HtmlGenericControl();
                divCont.Attributes.Add("id", "divCont" + i.ToString());
                divCont.TagName = "div";

                HtmlGenericControl divtit = new HtmlGenericControl();
                divtit.TagName = "div";
                divtit.Attributes.Add("class", "titac_servicio");

                HtmlGenericControl div70 = new HtmlGenericControl();
                div70.TagName = "div";
                div70.Attributes.Add("style", "float:left; width:70%;");
                TextBox txtTitulo = new TextBox();
                txtTitulo.Text = oTit.Titulo;
                txtTitulo.ID = "txtTitulo" + i;
                txtTitulo.CssClass = "pre_100";
                txtTitulo.Attributes.Add("name", "txtTitulo");
                div70.Controls.Add(txtTitulo);
                divtit.Controls.Add(div70);

                HtmlGenericControl div30 = new HtmlGenericControl();
                div30.TagName = "div";
                div30.Attributes.Add("style", "float:right; width:10%;");
                HtmlImage img = new HtmlImage();
                img.Src = "../iconos/btn_menos.png";
                img.ID = "btnRemove";
                img.Attributes.Add("onclick", "removeDiv(" + i + ")");
                img.Attributes.Add("style", "cursor:pointer;");
                div30.Controls.Add(img);
                divtit.Controls.Add(div30);

                HtmlGenericControl divdet = new HtmlGenericControl();
                divdet.TagName = "div";
                divdet.Attributes.Add("class", "pre_100");
                HtmlTextArea txtDetalle = new HtmlTextArea();
                txtDetalle.ID = "txtDetalle" + i;
                txtDetalle.Attributes.Add("Name", "txtDetalle");
                txtDetalle.Value = oTit.Detalle;
                txtDetalle.Attributes.Add("class", "pre_100v");
                txtDetalle.Attributes.Add("style", "font-family:Arial, Helvetica, Sans Serif");
                txtDetalle.Rows = 5;
                divdet.Controls.Add(txtDetalle);

                divCont.Controls.Add(divtit);
                divCont.Controls.Add(divdet);
                divsTitulos.Controls.Add(divCont);


                i++;

            }
        }

        #endregion

        private void CargarEspecificacionesAcuerdo()
        {
            AcuerdoBus oAcuerdoBus = new AcuerdoBus();
            DataTable dt = oAcuerdoBus.AcuerdoDetalleGetById(_idAcuerdo);
            if (dt.Rows.Count > 0)
            {
                gvTransporte.DataSource = dt;
                GridViewHelper helper = new GridViewHelper(this.gvTransporte);
                helper.RegisterGroup("TipoServicio", true, true);
                helper.GroupHeader += new GroupEvent(helper_GroupHeader);
                helper.ApplyGroupSort();

                gvTransporte.DataBind();

                Mensaje.mostrarPopUp(this);
            }
        }

        private void helper_GroupHeader(string groupName, object[] values, GridViewRow row)
        {
            if (groupName == "TipoServicio")
            {
                row.CssClass = "nuevaSS_titleServ2";
                row.Cells[0].Text = "&nbsp;&nbsp;" + row.Cells[0].Text;
            }
        }
        
        protected void gvTransporte_Sorting(object sender, GridViewSortEventArgs e)
        {
        }


        private void GuardarTrailer()
        {
            AcuerdoConceptoTrailer oACT = new AcuerdoConceptoTrailer();
            AcuerdoConceptoTrailerBus oACTBus = new AcuerdoConceptoTrailerBus();

            foreach (GridViewRow row in gvTransporte.Rows)
            {
                CheckBox chk = new CheckBox();
                chk = row.FindControl("chkTrailer") as CheckBox;
                HiddenField hfidAcuerodConcepto = row.FindControl("hfidAcuerodConcepto") as HiddenField;
                HiddenField hfIdVehiculo = row.FindControl("hfIdVehiculo") as HiddenField;
                if (chk.Checked &&
                    hfidAcuerodConcepto.Value != string.Empty &&
                    hfIdVehiculo.Value != string.Empty)
                {
                    oACT.IdAcuerdoConcepto = int.Parse(hfidAcuerodConcepto.Value);
                    oACT.IdVehiculo = int.Parse(hfIdVehiculo.Value);
                    oACTBus.AcuerdoConceptoTrailerAdd(oACT);
                }
            }

            Response.Redirect("frmAcuerdoGestion.aspx?idModulo="+ _Permiso.oModulo.idModulo);
        }
        
        #endregion

    }
}
