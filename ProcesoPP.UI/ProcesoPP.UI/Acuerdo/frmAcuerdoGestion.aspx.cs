﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ProcesoPP.Services;
using System.Configuration;
using ProcesoPP.Model;
using ProcesoPP.Business;
using ParteDiario.Services;
using System.Data;
using System.Web.Services;
using AjaxControlToolkit;

namespace ProcesoPP.UI.Acuerdo
{
    public partial class frmAcuerdoGestion : System.Web.UI.Page
    {
        #region PROPIEDADES

        public Permisos _Permiso
        {
            get { return (Permisos)ViewState["permiso"]; }
            set { ViewState["permiso"] = value; }
        }

        public int _idUsuario
        {
            get { return (int)ViewState["idUsuario"]; }
            set { ViewState["idUsuario"] = value; }
        }

        public int _ADM
        {
            get { return int.Parse(ConfigurationManager.AppSettings["ADM"].ToString()); }
        }

        #endregion

        #region EVENTOS

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                _idUsuario = Common.EvaluarSession(Request, this.Page);
                if (_idUsuario != 0)
                {
                    _Permiso = Common.getModulo(Request, this.Page);
                    if (_Permiso != null && _Permiso.Lectura)
                    {
                        if (!IsPostBack)
                        {
                         
                        }
                    }
                    else
                    {
                        Mensaje.errorMsj("NO TIENE PERMISOS A ESTE FORMULARIO", this.Page, "SIN ACCESO", "../frmPrincipal.aspx");
                    }
                }
                else
                {
                    Mensaje.errorMsj("DEBE LOGUEARSE", this.Page, "LOGIN", "../frmLogin.aspx");
                }
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                CargarAcuerdo();
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        protected void btnExportar_Click(object sender, EventArgs e)
        {
            try
            {
                Exportar();
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        protected void gvAcuerdo_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "Modificar":
                    Response.Redirect("frmAcuerdo.aspx?idAcuerdo=" + e.CommandArgument + "&idModulo=" + _Permiso.oModulo.idModulo + "&renovar=true");
                    break;
                case "Imprimir":
                    Mensaje.openAcuerdo(this.Page, "../Reportes/frmAcuertoRPT.aspx?idAcuerdo=" + e.CommandArgument);
                    break;
                case "Trailer":
                    Response.Redirect("frmAcuerdoTrailer.aspx?idAcuerdo=" + e.CommandArgument + "&idModulo=" + _Permiso.oModulo.idModulo + "&renovar=true");
                    break;
                case "Ver":
                    Response.Redirect("frmAcuerdo.aspx?idAcuerdo=" + e.CommandArgument + "&idModulo=" + _Permiso.oModulo.idModulo + "&Accion="+e.CommandName);
                    break;
                case "Eliminar":
                    try
                    {
                        Eliminar(e.CommandArgument.ToString());
                    }
                    catch (Exception ex)
                    {
                        Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
                    }
                    break;
            }
        }

        protected void gvAcuerdo_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
                CargarRow(e);
        }

        protected void gvAcuerdo_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvAcuerdo.PageIndex = e.NewPageIndex;
            CargarAcuerdo();
        }

        #endregion

        #region METODOS    

        #region COMBOSAJAX

        [WebMethod]
        public static CascadingDropDownNameValue[] GetEmpresa(string knownCategoryValues, string category)
        {
            EmpresaBus oEmpresaBus = new EmpresaBus();
            List<CascadingDropDownNameValue> empresa = oEmpresaBus.GetDataEmpresa();
            return empresa.ToArray();
        }

        [WebMethod]
        public static CascadingDropDownNameValue[] GetSector(string knownCategoryValues, string category)
        {
            string idEmpresa = CascadingDropDown.ParseKnownCategoryValuesString(knownCategoryValues)["idEmpresa"];
            SectorBus oSectorBus = new SectorBus();
            List<CascadingDropDownNameValue> sectores = oSectorBus.GetDataSector(int.Parse(idEmpresa));
            return sectores.ToArray();
        }


        #endregion

        private void Eliminar(string idAcuerdo)
        {
            AcuerdoBus oAcuerdoBus = new AcuerdoBus();
            Model.Acuerdo oAcuerdo = new ProcesoPP.Model.Acuerdo();
            oAcuerdo = oAcuerdoBus.AcuerdoGetById(int.Parse(idAcuerdo));
            oAcuerdo.Baja = true;
            oAcuerdoBus.AcuerdoUpdate(oAcuerdo);
            CargarAcuerdo();
        }

        private void CargarClientes()
        {
            EmpresaBus oEmpresaBus = new EmpresaBus();
            oEmpresaBus.CargarEmpresa(ref ddlCliente, "Seleccione...");
        }

        private void CargarRow(GridViewRowEventArgs e)
        {
            Common.CargarRow(e, _Permiso);
            LinkButton btnEliminar = e.Row.FindControl("btnEliminar") as LinkButton;
            if (_Permiso.oTipoUsuario.idTipoUsuario == _ADM)
                btnEliminar.Visible = true;
            else
                btnEliminar.Visible = false;
        }

        private void CargarAcuerdo()
        {
            AcuerdoBus oAcuerdoBus = new AcuerdoBus();
            gvAcuerdo.DataSource = oAcuerdoBus.AcuerdoGestionGetByFilter(txtNroAcuerdo.Text,
                                                                    txtFecha.Text,
                                                                    txtFechaHasta.Text,
                                                                    ddlCliente.SelectedValue.Equals(string.Empty) ? "0" : ddlCliente.SelectedValue,
                                                                    ddlSector.SelectedValue.Equals(string.Empty) ? "0" : ddlSector.SelectedValue);
            gvAcuerdo.DataBind();
        }

        private void Exportar()
        {
            AcuerdoBus oAcuerdoBus = new AcuerdoBus();
            DataTable dt = oAcuerdoBus.AcuerdoGestionGetByFilter(txtNroAcuerdo.Text,
                                                            txtFecha.Text,
                                                            txtFechaHasta.Text,
                                                            ddlCliente.SelectedValue.Equals(string.Empty) ? "0" : ddlCliente.SelectedValue,
                                                            ddlSector.SelectedValue.Equals(string.Empty) ? "0" : ddlSector.SelectedValue);
            dt.Columns.RemoveAt(0);
            string path = System.IO.Path.Combine(Server.MapPath("~"), "img");
            ExportarExcel oExport = new ExportarExcel("ReporteAcuerdo_" + DateTime.Now.ToString("yyyyMMddHHmmss"), path, "REPORTE ACUERDO");
            oExport.ExportarExcel2003(dt);
        }

        #endregion
    }

    
}
