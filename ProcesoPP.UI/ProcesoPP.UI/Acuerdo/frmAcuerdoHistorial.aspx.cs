﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ProcesoPP.Business;
using System.Data;
using ParteDiario.Services;
using ProcesoPP.Services;
using ProcesoPP.Model;
using System.Configuration;
using System.Web.Services;
using AjaxControlToolkit;

namespace ProcesoPP.UI.Acuerdo
{
    public partial class frmAcuerdoHistorial : System.Web.UI.Page
    {
        #region PROPIEDADES

        public Permisos _Permiso
        {
            get { return (Permisos)ViewState["permiso"]; }
            set { ViewState["permiso"] = value; }
        }

        public int _idUsuario
        {
            get { return (int)ViewState["idUsuario"]; }
            set { ViewState["idUsuario"] = value; }
        }

        public int _ADM
        {
            get { return int.Parse(ConfigurationManager.AppSettings["ADM"].ToString()); }
        }

        #endregion

        #region EVENTOS

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                _idUsuario = Common.EvaluarSession(Request, this.Page);
                if (_idUsuario != 0)
                {
                    _Permiso = Common.getModulo(Request, this.Page);
                    if (_Permiso != null && _Permiso.oTipoUsuario.idTipoUsuario == _ADM)
                    {
                        if (!IsPostBack)
                        {
                            
                        }
                    }
                    else
                    {
                        Mensaje.errorMsj("NO TIENE PERMISOS A ESTE FORMULARIO", this.Page, "SIN ACCESO", "../frmPrincipal.aspx");
                    }
                }
                else
                {
                    Mensaje.errorMsj("DEBE LOGUEARSE", this.Page, "LOGIN", "../frmLogin.aspx");
                }
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                CargarAcuerdo();
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        protected void btnExportar_Click(object sender, EventArgs e)
        {
            try
            {
                Exportar();
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        protected void gvAcuerdo_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "Modificar":
                    Response.Redirect("frmAcuerdo.aspx?idAcuerdo=" + e.CommandArgument + "&idModulo=" + _Permiso.oModulo.idModulo + "&renovar=true");
                    break;
                case "Imprimir":
                    Mensaje.openAcuerdo(this.Page, "../Reportes/frmAcuertoRPT.aspx?idAcuerdo=" + e.CommandArgument);
                    break;
                case "Ver":
                    Response.Redirect("frmAcuerdo.aspx?idAcuerdo=" + e.CommandArgument + "&idModulo=" + _Permiso.oModulo.idModulo + "&Accion=" + e.CommandName);
                    break;
                case "Eliminar":
                    try
                    {
                        Eliminar(e.CommandArgument.ToString());
                    }
                    catch (Exception ex)
                    {
                        Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
                    }
                    break;
            }
        }

        protected void gvAcuerdo_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
                CargarRow(e);
        }

        protected void gvAcuerdo_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvAcuerdo.PageIndex = e.NewPageIndex;
            CargarAcuerdo();
        }

        #endregion

        #region METODOS

        #region COMBOSAJAX

        [WebMethod]
        public static CascadingDropDownNameValue[] GetEmpresa(string knownCategoryValues, string category)
        {
            EmpresaBus oEmpresaBus = new EmpresaBus();
            List<CascadingDropDownNameValue> empresa = oEmpresaBus.GetDataEmpresa();
            return empresa.ToArray();
        }

        [WebMethod]
        public static CascadingDropDownNameValue[] GetSector(string knownCategoryValues, string category)
        {
            string idEmpresa = CascadingDropDown.ParseKnownCategoryValuesString(knownCategoryValues)["idEmpresa"];
            SectorBus oSectorBus = new SectorBus();
            List<CascadingDropDownNameValue> sectores = oSectorBus.GetDataSector(int.Parse(idEmpresa));
            return sectores.ToArray();
        }


        #endregion


        private void Eliminar(string idAcuerdo)
        {
            if (idAcuerdo != string.Empty)
            {
                AcuerdoBus oAcuerdoBus = new AcuerdoBus();
                oAcuerdoBus.AcuerdoDelete(int.Parse(idAcuerdo));
                CargarAcuerdo();
            }
        }

        private void CargarClientes()
        {
            EmpresaBus oEmpresaBus = new EmpresaBus();
            oEmpresaBus.CargarEmpresa(ref ddlCliente, "Seleccione...");
        }

        private void CargarRow(GridViewRowEventArgs e)
        {
            Common.CargarRow(e, _Permiso);
            LinkButton btnEliminar = e.Row.FindControl("btnEliminar") as LinkButton;
            HiddenField hfBaja = e.Row.FindControl("hfBaja") as HiddenField;
            HiddenField hfActivo = e.Row.FindControl("hfActivo") as HiddenField;

            if (_Permiso.oTipoUsuario.idTipoUsuario == _ADM)
                btnEliminar.Visible = true;
            else
                btnEliminar.Visible = false;

            if (hfBaja.Value != string.Empty && bool.Parse(hfBaja.Value))
            {
                e.Row.CssClass = "rowBaja";
                e.Row.ToolTip = "Acuerdo eliminado";
                btnEliminar.Visible = false;
            }
            else
            {
                if (hfActivo.Value != string.Empty && bool.Parse(hfActivo.Value))
                {
                    e.Row.CssClass = "rowActivo";
                    e.Row.ToolTip = "Acuerdo Activo";
                }
                else
                {
                    e.Row.ToolTip = "Acuerdo Borrador";
                }
            }

        }

        private void CargarAcuerdo()
        {
            AcuerdoBus oAcuerdoBus = new AcuerdoBus();
            gvAcuerdo.DataSource = oAcuerdoBus.AcuerdoGetByFilter(txtNroAcuerdo.Text,
                                                                    txtFecha.Text,
                                                                    txtFechaHasta.Text,
                                                                    ddlCliente.SelectedValue.Equals(string.Empty) ? "0" : ddlCliente.SelectedValue,
                                                                    ddlSector.SelectedValue.Equals(string.Empty) ? "0" : ddlSector.SelectedValue);
            gvAcuerdo.DataBind();
        }

        private void Exportar()
        {
            AcuerdoBus oAcuerdoBus = new AcuerdoBus();
            DataTable dt = oAcuerdoBus.AcuerdoGetByFilter(txtNroAcuerdo.Text,
                                                            txtFecha.Text,
                                                            txtFechaHasta.Text,
                                                            ddlCliente.SelectedValue.Equals(string.Empty) ? "0" : ddlCliente.SelectedValue,
                                                            ddlSector.SelectedValue.Equals(string.Empty) ? "0" : ddlSector.SelectedValue);
            dt.Columns.RemoveAt(0);
            string path = System.IO.Path.Combine(Server.MapPath("~"), "img");
            ExportarExcel oExport = new ExportarExcel("HistorialAcuerdo_" + DateTime.Now.ToString("yyyyMMddHHmmss"), path, "REPORTE ACUERDO");
            oExport.ExportarExcel2003(dt);
        }

        #endregion
    }
}
