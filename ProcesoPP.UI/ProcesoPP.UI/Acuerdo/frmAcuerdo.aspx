﻿<%@ Page Title="PP - Acuerdo" Language="C#" MasterPageFile="~/frmPrincipal.Master" AutoEventWireup="true" EnableEventValidation="false" CodeBehind="frmAcuerdo.aspx.cs" Inherits="ProcesoPP.UI.Acuerdo.frmAcuerdo"
    %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script type="text/javascript">
        $(document).ready(function() { SetMenu("liAcuerdo"); }); 
    </script>

    <script type="text/javascript" src="../Js/jquery.easing.1.3.js"></script>
    <script type="text/javascript" src="../Js/script.js"></script>
    <script type="text/javascript">
        function hide() {
            $("#divAcu").hide();
        }

        function show(chk, id) {
            if (chk.checked)
                $("#menu_acu_" + id).show();
            else
                $("#menu_acu_" + id).hide();
        }
        
        function clean(oObj) {
            if (oObj.value.indexOf("[Titulo") >= 0 ||
            oObj.value.indexOf("[Detalle") >= 0) {
                oObj.value = "";
                oObj.style.color = "#000";
            }
        }


        function HideMenu() {
            var Div = document.getElementById("DivContextMenu");
            Div.style.display = "none";
            $("#" + Div.id).fadeOut("slow");
            return false;
        }

        function ShowMenu(RowIndex) {
            var Div = document.getElementById("DivContextMenu");
            Div.style.display = "block";
            Div.style.position = "absolute";
            Div.style.left = "10%";
            Div.style.top = "10%";
            $("#" + Div.id).fadeIn("slow");
            return false;
        }

        function validarGuardar() {
            msj = ValidarObj('txtTitle|Ingrese el Titulo del contrato/acuerdo.~' +
                              'ddlTipoMoneda|Seleccione un tipo de moneda.~' +
                              'txtNroAcuerdo|Ingrese el Nro. de Acuerdo.~' +
                              'ddlCliente|Seleccione el cliente.~' +
                              'txtFechaEmision|Debe ingresar una fecha de emisión.~' +
                              'txtFecha|Debe ingresar una fecha de Inicio.~' +
                              'txtFechaFin|Debe ingresar una fecha de fin de vigencia.~' +
                              'ddlSector|Debe seleccionar el sector.');

            if (msj) {
                getTitulos();
                if (!getServicios())
                    return false;
            }

            return msj;
        }
    </script>

    <script type="text/javascript">
        function preguntar(msj) {
            var r = confirm(msj);
            return r;
        }
    </script>

    <script type="text/javascript">
        function getServicios() {
            gvServicios = document.getElementsByTagName("table");
            hf = document.getElementById("ctl00_ContentPlaceHolder1_hfServicios");
            listServ = [];
            contConn = 0;
            
            for (gv = 0; gv < gvServicios.length; gv++) {
                grid = gvServicios[gv];
                //i = 0 -> row Header
                if (grid.id != "") {
                    oServicio = new Object();
                    oServicio.oEsp = [];                    
                    
                    for (i = 1; i < grid.rows.length; i++) {
                        row = grid.rows[i];
                        hfReq = "hf" + grid.id + "Requiere";
                        hfReq = document.getElementById("ctl00_ContentPlaceHolder1_" + hfReq);
                        
                        oEspecificacion = new Object();
                        oServicio.idTipoServicio = row.cells[0].innerText;
                        if (hfReq != null) {
                            oServicio.requiere = hfReq.value;
                        }
                        else
                            oServicio.requiere = "";
                        
                        oEspecificacion.idEspecificacion = row.cells[1].innerText;
                        
                        for (j = 3; j < row.cells.length; j++) {
                            precioHEader = grid.rows[0].cells[j].innerText;
                            oEspecificacion[precioHEader] = row.cells[j].childNodes[0].value;
                            if (oServicio.idTipoServicio == 55 || oServicio.idTipoServicio == 56) {
                                if (oEspecificacion.idEspecificacion == 0  && oEspecificacion[precioHEader] != "")
                                    contConn++;
                            }
                        }
                        oServicio.oEsp.push(oEspecificacion);                                             
                    }
                    if (oServicio != null && oServicio.idTipoServicio != 0) {
                        listServ.push(oServicio);                        
                    }
                }
            }
           
            var myJSON = JSON.stringify({ Servicios: listServ });
            hf.value = myJSON;
            if (contConn < 2) {
                con = confirm("No exite precio cargado para la Conexión/Desconexión. Desea Continuar?");
                return con;
            }
            return true;
        }
    
    </script>

    <script type="text/javascript">
    $(document).ready(function() {
        var iCnt = 0;

        var idCont = "ctl00_ContentPlaceHolder1_";
        // CREATE A "DIV" ELEMENT AND DESIGN IT USING JQUERY ".css()" CLASS.
        var container = $(document.createElement('div'));
        var $eles = $("[id^='divCont']");
        if ($eles.length > 0)
            iCnt = $eles.length;
        $('#btAdd').click(function() {
            iCnt = iCnt + 1;
            // ADD TEXTBOX.
            $(container).append("<div id='divCont" + iCnt + "'><div class='titac_servicio'>" +
                                        "<div style='float:left; width:70%;'>" +
                                        "<input type='text' name='txtTitulo' id='txtTitulo" + iCnt + "' " +
                                        "class='pre_100' onfocus='clean(this);' style='color:#999999;' value='[Titulo]'/></div>" +
                                         "<div style='float:right; width:10%;'>" +
                                         "<img src='../iconos/btn_menos.png' id='btnRemove' title='Eliminar Titulo - Detalle' style='cursor:pointer;' onclick='removeDiv(" + iCnt + ");'/>" +
                                         "</div>" +
                                    "</div>" +
                                    "<div class='pre_100'>" +
                                        "<textarea name='txtDetalle' onfocus='clean(this);' ID='txtDetalle" + iCnt + "' cols='1' rows='5' class='pre_100v' " +
                                            "style='font-family:Arial, Helvetica, Sans Serif; color:#999999;'>[Detalle]</textarea> " +
                                    "</div></div>");

            $('#' + idCont + 'divsTitulos').after(container);   // ADD BOTH THE DIV ELEMENTS TO THE "main" CONTAINER.

        });
    });
    
    function removeDiv(icn) {   // REMOVE ELEMENTS ONE PER CLICK.
        if (icn != 0) {
            $('#divCont' + icn).remove();
        }
        if (icn == 0) {
            $(container).empty();
            $(container).remove();
            $('#btSubmit').remove();
            $('#btAdd').removeAttr('disabled');
            $('#btAdd').attr('class', 'bt')
        }
    }
     
    function getTitulos() {
        var idCont = "ctl00_ContentPlaceHolder1_";
        hf = document.getElementById(idCont + "hfTitulos");
        txtsTit = $("input[name*='txtTitulo']"); //document.getElementsByName("txtTitulo");
        txtsDet = $("textarea[name*='txtDetalle']");  //document.getElementsByName("txtDetalle");
        listTitulos = [];

        for (i = 0; i < txtsTit.length; i++) {

            oTitulo = new Object();
            oTitulo.titulo = txtsTit[i].value;
            oTitulo.detalle = txtsDet[i].value;
           
            listTitulos.push(oTitulo);

        }

        var myJSON = JSON.stringify({ "acuerdoTitulo": listTitulos });
        hf.value = myJSON;
        //hf.innerText = myJSON;
    }
</script>

    <script type="text/javascript">
        function validarcodificacions() {
            grid = document.getElementById("ctl00_ContentPlaceHolder1_gvTransporte");
            cont = 0;
            cant = 0;
            for (i = 1; i < grid.rows.length; i++) {
                row = grid.rows[i];
                if (row.cells.length == 1) {
                    cant = 0;
                    cont = 0;
                }
                else if (row.cells.length > 1) {
                    chk = row.cells[0].childNodes[1];
                    if (i == 1)
                        name = grid.rows[i].cells[1].innerText;
                    if (name != row.cells[1].innerText) {
                        cant = 0;
                        cont = 0;
                        name = row.cells[1].innerText;
                    }
                    if (chk.checked) {
                        cant = row.cells[0].childNodes[3].value;
                        cont++;
                        if (parseInt(cont) > parseInt(cant))
                            break;                        
                    }
                }
            }

            if (parseInt(cont) > parseInt(cant)) {
                new Messi("No debe superar la cantidad de equipos cargada en el contrato",
                        { title: 'Error', modal: true, titleClass: 'error', buttons: [{ id: 0, label: 'Aceptar', val: 'Y'}] });                 
                return false;
            }
            else
                return true;
            
        }
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <asp:HiddenField runat="server" ID="hfServicios" Value="" />
    <asp:HiddenField runat="server" ID="hfTitulos" Value="" />

    <div id="divSolServ" class="apertura">
        Carga de Acuerdo</div>
    <div class="solicitud100">    
       <div class="sol_tit50">Título </div>
       <div class="sol_campo30">
        <asp:TextBox runat="server" ID="txtTitle" CssClass="sol_campo25" style="text-transform:uppercase;"></asp:TextBox>
       </div>
    </div>
    <div class="solicitud100">
        <div class="sol_tit50">
            <label>N° Acuerdo</label></div>
        <div class="sol_campo30">
            <asp:TextBox runat="server" ID="txtNroAcuerdo" CssClass="sol_campo25"></asp:TextBox>
        </div>
        <div class="sol_fecha"></div>
        <div class="sol_tit50">
            <label>Revisión</label>
        </div>
        <div class="sol_campo30">
            <asp:label runat="server" ID="lblRevision"></asp:label>
        </div>        
    </div>
     <cc1:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
        </cc1:ToolkitScriptManager> 
    <div class="solicitud100">
        <div class="sol_tit50">
            <label>Cliente</label>
        </div>
        <div class="sol_campo30">
            <asp:DropDownList runat="server" ID="ddlCliente" CssClass="sol_campo25">
            </asp:DropDownList>
            <cc1:CascadingDropDown ID="cddEmpresa" TargetControlID="ddlCliente" PromptText="N/A" PromptValue="0" 
                ServiceMethod="GetEmpresa" runat="server" Category="idEmpresa" 
                LoadingText="Cargando..."/>
        </div>
        <div class="sol_fecha"></div>
         <div class="sol_tit50">
            <label>Sector</label>
        </div>
        <div class="sol_campo30">
            <asp:DropDownList runat="server" ID="ddlSector" CssClass="sol_campo25">
            </asp:DropDownList>
            <cc1:CascadingDropDown ID="cddSector" TargetControlID="ddlSector" PromptText="N/A" PromptValue="0" 
                ServiceMethod="GetSector" runat="server" Category="idSector" 
                ParentControlID="ddlCliente" LoadingText="Cargando..." />
        </div>
    </div>
    <div class="solicitud100">
        <div class="sol_tit50">
            <label>Fecha Inicio:</label>
        </div>
        <div class="sol_campo30">
            <asp:TextBox CssClass="sol_campo25" runat="server" ID="txtFecha" /></div>
        <div class="sol_fecha">
            <img id="btnFecha" alt="calendario" src="../img/calendario.gif" style="width: 20px;
                height: 20px; cursor: pointer;" onmouseover="CambiarConfiguracionCalendario('txtFecha',this);" />
        </div>
        <div class="sol_tit50">
            <label>Fecha Fin:</label>
        </div>
        <div class="sol_campo30">
            <asp:TextBox CssClass="sol_campo25" runat="server" ID="txtFechaFin" /></div>
        <div class="sol_fecha">
            <img id="btnFechaFin" alt="calendario" src="../img/calendario.gif" style="width: 20px;
                height: 20px; cursor: pointer;" onmouseover="CambiarConfiguracionCalendario('txtFechaFin',this);" />
        </div>
    </div>
    <div class="solicitud100">
        <div class="sol_tit50">
            <label>Tipo Moneda</label>
        </div>
        <div class="sol_campo30">
        <asp:DropDownList runat="server" ID="ddlTipoMoneda" CssClass="sol_campo25">
            <asp:ListItem>Seleccionar...</asp:ListItem>
            <asp:ListItem>Dolar</asp:ListItem>
            <asp:ListItem>Pesos Argentino</asp:ListItem>
        </asp:DropDownList>
        </div>
        <div class="sol_fecha"></div>
        <div class="sol_tit50">
            <label>Fecha Emisión:</label>
        </div>
        <div class="sol_campo30">
            <asp:TextBox CssClass="sol_campo25" runat="server" ID="txtFechaEmision" /></div>
        <div class="sol_fecha">
            <img id="btnFechaEmision" alt="calendario" src="../img/calendario.gif" style="width: 20px;
                height: 20px; cursor: pointer;" onmouseover="CambiarConfiguracionCalendario('txtFechaEmision',this);" />
        </div>
     </div>
      
    <div id="acuerdo">
        <div class="titac_servicio" >
            * Agregar párrafo de bienvenida
        </div>
        <div  class="pre_100" >
            <asp:TextBox runat="server" id="txtHeader" TextMode="MultiLine" Rows="4" CssClass="pre_100v" style="font-family:Arial, Helvetica, Sans Serif; color:#999999;" ></asp:TextBox>
        </div>
        <div class="titac_servicio">
            Tipo de Servicios</div>
        <div class="acu_100" id="divAcu" runat="server">
            <div id="main">
                <ul class="containera" runat="server" id="ulCont">
                    <%--<dbwc:DynamicControlsPlaceholder ID="DCP" runat="server" ControlsWithoutIDs="Persist"></dbwc:DynamicControlsPlaceholder>--%>
                    <asp:PlaceHolder ID="PlaceHolder1" runat="server"></asp:PlaceHolder>
                    <!--REPITE-->
                    <%--<li class="menu_acu" runat="server" id="liMenu_acu">                            
                        </li>--%>
                </ul>
            </div>
        </div>
        
        <div class="titac_servicio" >
            Agregar nuevo [Título] - [Detalle] <img src="../iconos/btn_mas.png" id="btAdd" title="Add" alt="" style="cursor:pointer;"/>
        </div>
        <div id="divsTitulos" runat="server">
                                
        </div>     
    </div>
    <div class="solicitud100">        
        
         <div class="sol_btn">
            <asp:Button runat="server" ID="btnGuardar" Text="GUARDAR" CssClass="btn_form" OnClientClick="return validarGuardar();"
                OnClick="btnGuardar_Click"></asp:Button>
      
            <input type="button" id="btnVolver" value="VOLVER" class="btn_form" onclick="IrA('');"/>      
        </div>
        <div class="sol_btn">
            <asp:Button runat="server" ID="btnBorrador" Text="GUARDAR BORRADOR" 
                CssClass="btn_form" OnClientClick="return validarGuardar();" onclick="btnBorrador_Click"
                ></asp:Button>
         </div>
    </div>
    <asp:HiddenField runat="server" ID="hfError" />

    <div id="capaPopUp"></div>
    <div id="popUpDiv">
        <div id="capaContent" style="width: 800px; min-width:95%; ">        
            <div style="width: 95%; padding-top: 2%; padding-left: 3%;	padding-bottom: 5%; height:70%; overflow:auto;">
                <asp:GridView ID="gvTransporte" CssClass="tabla3" runat="server" AutoGenerateColumns="False" DataMember="idVehiculo"
                    Width="95%"  
                    EmptyDataText="No existen registros" EmptyDataRowStyle-CssClass="celda50"
                    EmptyDataRowStyle-Width="80%" OnSorting="gvTransporte_Sorting">
                    <Columns>
                         <asp:BoundField DataField="idTipoServicio"  Visible="false"/>
                         <asp:BoundField DataField="cant"  Visible="false"/>
                         <asp:BoundField DataField="TipoServicio" SortExpression="TipoServicio" Visible="false"/>                         
                         <asp:TemplateField HeaderStyle-CssClass="celda_boxt" ItemStyle-CssClass="celda_box">
                            <ItemTemplate>
                                <asp:CheckBox runat="server" ID="chkTrailer" />
                                <asp:HiddenField runat="server" ID="hfCant" Value='<%# Bind("cant") %>' />
                                <asp:HiddenField runat="server" ID="hfidAcuerodConcepto" Value='<%# Bind("IdAcuerdoConcepto") %>' />
                                <asp:HiddenField runat="server" ID="hfidVehiculo" Value='<%# Bind("idVehiculo") %>' />
                            </ItemTemplate>
                         </asp:TemplateField>                         
                         <asp:BoundField DataField="idEspecificacion"  Visible="false"/>
                         <asp:BoundField DataField="Especificacion" HeaderText="ESPECIFICACIONES" SortExpression="Especificacion"
                                        HeaderStyle-CssClass="celda_boxtit" ItemStyle-CssClass="celda_box1" ItemStyle-Width="52%" HeaderStyle-Width="52%"/>
                         <asp:BoundField DataField="codificacion" HeaderText="EQUIPOS" ItemStyle-CssClass="celda_box1" ItemStyle-Width="25%" HeaderStyle-Width="25%"
                                        HeaderStyle-CssClass="celda_boxtit" />
                        <asp:BoundField DataField="cant" HeaderText="Cant" SortExpression="cant"
                                        HeaderStyle-CssClass="celda_boxtit" ItemStyle-CssClass="celda_box1" ItemStyle-Width="10%" HeaderStyle-Width="10%"/>
                         
                    </Columns>
                </asp:GridView>
            </div>
            <div id="botones" style="width: 95%; height:10%;">
                <div class="regreso_3">
                    <input type="button" onclick="cerrarPopUp();" title="Cancelar" id="btnCerrar" class="btn_form"
                        value="CANCELAR" />
                </div>
                <div class="regreso_3">
                    <asp:Button ToolTip="Guardar" id="btnGuardarEquipos" runat="server" OnClick="btnGuardarEquipo_Onclick" OnClientClick="return validarcodificacions();"
                        CssClass="btn_form" Text="GUARDAR" ></asp:Button>

                </div>                
            </div>

        </div>
    </div>

</asp:Content>
