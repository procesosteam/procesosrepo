﻿<%@ Page Title="PP - Acuerdos Borradores" Language="C#" MasterPageFile="~/frmPrincipal.Master" AutoEventWireup="true" CodeBehind="frmAcuerdoBorrador.aspx.cs" Inherits="ProcesoPP.UI.Acuerdo.frmAcuerdoBorrador" EnableEventValidation="false"%>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" >
    $(document).ready(function() { SetMenu("liAcuerdo"); });

    function openAcuerdo(redirect) {
        window.location.href = redirect;
    }
</script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

   <div id="contenido" runat="server">

    <div id="apertura">Acuerdos Borradores</div> 
        <div class="solicitud100">
        <div class="sol_tit50"> <label>Vigencia Desde</label></div>
        <div class="sol_campo30"><asp:TextBox runat="server" CssClass="sol_campo25" id="txtFecha" /></div>
        <div class="sol_fecha"><img id="btnFecha" alt="calendario" src="../img/calendario.gif" style="width:20px; height:20px; cursor:pointer;" onmouseover="CambiarConfiguracionCalendario('txtFecha',this);"/></div>

        <div class="sol_tit50"> <label>Vigencia Hasta</label></div>
        <div class="sol_campo30"><asp:TextBox runat="server" CssClass="sol_campo25" id="txtFechaHasta" /></div>
        <div class="sol_fecha"><img id="btnFechaHasta" alt="calendario" src="../img/calendario.gif" style="width:20px; height:20px; cursor:pointer;" onmouseover="CambiarConfiguracionCalendario('txtFechaHasta',this);"/></div>
    
    </div>
    <cc1:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
        </cc1:ToolkitScriptManager>
     <div class="solicitud100">   
        <div class="sol_tit50"> <label>Cliente</label></div>
        <div class="sol_campo30"><asp:DropDownList runat="server" id="ddlCliente" DataTextField="Descripcion" DataValueField="idEmpresa"/>
        <cc1:CascadingDropDown ID="cddCliente" TargetControlID="ddlCliente" PromptText="N/A" PromptValue="0" 
                ServiceMethod="GetEmpresa" runat="server" Category="idEmpresa" 
                LoadingText="Cargando..."/>
                </div>        
        <div class="sol_fecha"></div>
        
        <div class="sol_tit50"><label>Sector</label></div>
            <div class="sol_campo30">                
               <asp:DropDownList ID="ddlSector" runat="server" DataTextField="Descripcion" DataValueField="idSector" />
               <cc1:CascadingDropDown ID="cddSector" TargetControlID="ddlSector" PromptText="N/A" PromptValue="0" 
                ServiceMethod="GetSector" runat="server" Category="idSector" 
                ParentControlID="ddlCliente" LoadingText="Cargando..." />
            </div>
    </div>
    <div class="solicitud100">        
        <div class="sol_tit50"> <label>Nº Acuerdo</label></div>
        <div class="sol_campo30"><asp:TextBox CssClass="sol_campo25" runat="server" id="txtNroAcuerdo" /></div>
        <div class="sol_fecha"></div>
    </div>
    <div class="solicitud100">
        <div class="sol_btn">
            <asp:Button runat="server" id="btnBuscar" cssclass="btn_form" Text="Buscar" onclick="btnBuscar_Click" />
        
            <asp:Button runat="server" id="btnExportar" cssclass="btn_form" Text="Exportar" onclick="btnExportar_Click" />
        </div>
    </div>
</div>

    <asp:GridView ID="gvAcuerdo" runat="server" AutoGenerateColumns="False" 
            CssClass="tabla3" HeaderStyle-CssClass="celda1"             
            onrowcommand="gvAcuerdo_RowCommand" onrowdatabound="gvAcuerdo_RowDataBound"
            AllowPaging="true" PageSize="20" onpageindexchanging="gvAcuerdo_PageIndexChanging"
            PagerStyle-CssClass="celda1">
         <Columns>
             <asp:TemplateField HeaderText="Nº Acuerdo" HeaderStyle-CssClass="celda10" ItemStyle-CssClass="celda10">
                 <ItemTemplate>
                     <asp:Label ID="lblNroAcuerdo" runat="server" Text='<%# Bind("NroAcuerdo") %>'></asp:Label>                     
                 </ItemTemplate>
             </asp:TemplateField>
               <asp:TemplateField HeaderText="Nº Revisión" HeaderStyle-CssClass="celda10" ItemStyle-CssClass="celda10">
                 <ItemTemplate>
                     <asp:Label ID="lblNroRevision" runat="server" Text='<%# Bind("NroRevision") %>'></asp:Label>                     
                 </ItemTemplate>
             </asp:TemplateField>
             <asp:TemplateField HeaderText="Cliente" HeaderStyle-CssClass="celda30" ItemStyle-CssClass="celda30">
                 <ItemTemplate>
                     <asp:Label ID="lblCliente" runat="server" Text='<%# Bind("Cliente") %>'></asp:Label>
                 </ItemTemplate>
             </asp:TemplateField>
             <asp:TemplateField HeaderText="Sector" HeaderStyle-CssClass="celda20" ItemStyle-CssClass="celda20">
                 <ItemTemplate>
                     <asp:Label ID="lblSector" runat="server" Text='<%# Bind("Sector") %>'></asp:Label>
                 </ItemTemplate>
             </asp:TemplateField>
             <asp:TemplateField HeaderText="Fecha Inicio" HeaderStyle-CssClass="celda10" ItemStyle-CssClass="celda10">
                 <ItemTemplate>
                     <asp:Label ID="lblFecha" runat="server" Text='<%# Bind("FechaInicio") %>'></asp:Label>
                 </ItemTemplate>
             </asp:TemplateField>
              <asp:TemplateField HeaderText="Fecha Fin" HeaderStyle-CssClass="celda10" ItemStyle-CssClass="celda10">
                 <ItemTemplate>
                     <asp:Label ID="lblFechaFin" runat="server" Text='<%# Bind("FechaFin") %>'></asp:Label>
                 </ItemTemplate>
             </asp:TemplateField>             
             <asp:TemplateField ItemStyle-CssClass="celda5" HeaderStyle-CssClass="celda5">
                <ItemTemplate>
                     <asp:ImageButton ID="btnActivar" runat="server" title="Activar Acuerdo" ImageUrl="~/iconos/activar.png" Width="18" Height="18" CommandArgument='<%# Bind("idAcuerdo") %>' CommandName="Activar"></asp:ImageButton >
                 </ItemTemplate>
             </asp:TemplateField>
             <asp:TemplateField ItemStyle-CssClass="celda5" HeaderStyle-CssClass="celda5">
                <ItemTemplate>
                     <asp:LinkButton ID="btnVer" runat="server" title="Ver Acuerdo" CssClass="ver" CommandArgument='<%# Bind("idAcuerdo") %>' CommandName="Ver"></asp:LinkButton>
                 </ItemTemplate>
             </asp:TemplateField>
             <asp:TemplateField  ItemStyle-CssClass="celda5" HeaderStyle-CssClass="celda5">
                <ItemTemplate>
                    <asp:HiddenField runat="server" id="hfBaja" value='<%# Bind("Baja") %>'/>
                     <asp:LinkButton ID="btnEliminar" runat="server" title="Eliminar Acuerdo" CssClass="eliminar" CommandArgument='<%# Bind("idAcuerdo") %>' CommandName="Eliminar" OnClientClick="return Eliminar('el Acuerdo');"></asp:LinkButton>
                 </ItemTemplate>
             </asp:TemplateField>
         </Columns>
     </asp:GridView>

</asp:Content>
