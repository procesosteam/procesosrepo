﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ProcesoPP.Model;
using System.Configuration;
using ProcesoPP.Services;
using ProcesoPP.Business;

namespace ProcesoPP.UI.Acuerdo
{
    public partial class frmAcuerdoTrailer : System.Web.UI.Page
    {
        #region PROPIEDADES
        public int _idUsuario
        {
            get { return (int)ViewState["idUsuario"]; }
            set { ViewState["idUsuario"] = value; }
        }

        public int _idAcuerdo
        {
            get { return (int)ViewState["idAcuerdo"]; }
            set { ViewState["idAcuerdo"] = value; }
        }

        public Permisos _Permiso
        {
            get { return (Permisos)ViewState["permiso"]; }
            set { ViewState["permiso"] = value; }
        }

        public int _ADM
        {
            get { return int.Parse(ConfigurationManager.AppSettings["ADM"].ToString()); }
        }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                _idUsuario = Common.EvaluarSession(Request, this.Page);
                if (_idUsuario != 0)
                {
                    _Permiso = Common.getModulo(Request, this.Page);
                    if (_Permiso != null && _Permiso.Lectura)
                    {
                        if (!IsPostBack)
                        {
                            InitScreen();
                        }

                    }
                    else
                    {
                        Mensaje.errorMsj("NO TIENE PERMISOS A ESTE FORMULARIO", this.Page, "SIN ACCESO", "../frmPrincipal.aspx");
                    }
                }
                else
                {
                    Mensaje.errorMsj("DEBE LOGUEARSE", this.Page, "LOGIN", "../frmLogin.aspx");
                }
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }


        }

        private void InitScreen()
        {
            if (Request["idAcuerdo"] != null)
                _idAcuerdo = int.Parse(Request["idAcuerdo"]);
            else
                _idAcuerdo = 0;

            AcuerdoBus oAcuerdoBus = new AcuerdoBus();
            AcuerdoConceptoTrailerBus oAcuerdoTrailer = new AcuerdoConceptoTrailerBus();

            Model.Acuerdo oAcuerdo = new Model.Acuerdo();
            oAcuerdo = oAcuerdoBus.AcuerdoGetById(_idAcuerdo);
            txtTitle.Text = oAcuerdo.Titulo;
            txtNroAcuerdo.Text = oAcuerdo.NroAcuerdo;
            txtFecha.Text = oAcuerdo.FechaInicio.ToString("dd/MM/yyyy");
            txtFechaFin.Text = oAcuerdo.FechaFin.ToString("dd/MM/yyyy");
            lblCliente.Text = oAcuerdo.oCliente.RazonSocial;
            lblRevision.Text = oAcuerdo.NroRevision.ToString() ;

            gvTransporte.DataSource = oAcuerdoTrailer.AcuerdoConceptoTrailerGetByIdAcuerdo(_idAcuerdo);

            GridViewHelper helper = new GridViewHelper(this.gvTransporte);
            helper.RegisterGroup("TipoServicio", true, true);
            helper.GroupHeader += new GroupEvent(helper_GroupHeader);
            helper.ApplyGroupSort();

            gvTransporte.DataBind();
        }

        private void helper_GroupHeader(string groupName, object[] values, GridViewRow row)
        {
            if (groupName == "TipoServicio")
            {
                row.CssClass = "nuevaSS_titleServ";
                row.Cells[0].Text = "&nbsp;&nbsp;" + row.Cells[0].Text;
            }
        }

        protected void gvTransporte_Sorting(object sender, GridViewSortEventArgs e)
        {
        }

    }
}