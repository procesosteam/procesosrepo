﻿<%@ Page Title="" Language="C#" MasterPageFile="~/frmPrincipal.Master" AutoEventWireup="true" CodeBehind="frmAcuerdoTrailer.aspx.cs" Inherits="ProcesoPP.UI.Acuerdo.frmAcuerdoTrailer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script type="text/javascript">
        $(document).ready(function () { SetMenu("liAcuerdo"); }); 
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="divSolServ" class="apertura">
        <asp:Label runat="server" ID="txtTitle" CssClass="acuerdo_tit" style="text-transform:uppercase;"></asp:Label>
    </div>
    <div class="solicitud100">
<div class="sol_tit50">
            <label>Cliente</label>
        </div>
<div class="sol_campo50">
            <asp:Label id="lblCliente" runat="server" />
        </div></div>
        
    <div class="solicitud100">
        
<div class="sol_tit50">
            <label>N° Acuerdo</label></div>
        <div class="sol_campo30">
            <asp:Label runat="server" ID="txtNroAcuerdo" ></asp:Label>
        </div>        
        <div class="sol_tit30">
            <label>Revisión</label>
        </div>
        <div class="sol_campo20">
            <asp:label runat="server" ID="lblRevision"></asp:label>
        </div>        
    </div>
    <div class="solicitud100">
        
        
        <div class="sol_tit50">
            <label>Fecha Inicio:</label>
        </div>
        <div class="sol_campo30">
            <asp:Label runat="server" ID="txtFecha" /></div>
        <div class="sol_tit50">
            <label>Fecha Fin:</label>
        </div>
        <div class="sol_campo30">
            <asp:Label runat="server" ID="txtFechaFin" /></div>
    </div>
    
    
<div style="width: 100%; padding-top: 5%; padding-left: 5%;	padding-right: 5%;padding-bottom: 5%; height:90%; overflow:auto;">
    <asp:GridView ID="gvTransporte" CssClass="tabla_acu" runat="server" AutoGenerateColumns="False" DataMember="idVehiculo"
        Width="95%"  
        EmptyDataText="No existen registros" EmptyDataRowStyle-CssClass="acuerdo_tit"
        EmptyDataRowStyle-Width="80%" OnSorting="gvTransporte_Sorting">
        <Columns>
                <asp:BoundField DataField="idTipoServicio"  Visible="false"/>                
                <asp:BoundField DataField="TipoServicio" SortExpression="TipoServicio" Visible="false"/>                         
                <asp:TemplateField HeaderStyle-CssClass="celda_boxt" ItemStyle-CssClass="celda_box">
                <ItemTemplate>
                    <asp:HiddenField runat="server" ID="hfidAcuerodConcepto" Value='<%# Bind("IdAcuerdoConcepto") %>' />
                    <asp:HiddenField runat="server" ID="hfidVehiculo" Value='<%# Bind("IdVehiculo") %>' />
                </ItemTemplate>
                </asp:TemplateField>                         
                <asp:BoundField DataField="idEspecificacion"  Visible="false"/>
                <asp:BoundField DataField="Especificacion" HeaderText="ESPECIFICACIONES" SortExpression="Especificacion"
                            HeaderStyle-CssClass="celda_boxtit" ItemStyle-CssClass="celda_box1" ItemStyle-Width="45%" HeaderStyle-Width="45%"/>
                <asp:BoundField DataField="Trailer" HeaderText="EQUIPOS" ItemStyle-Width="45%" HeaderStyle-Width="45%"
                            HeaderStyle-CssClass="celda_boxtit" ItemStyle-CssClass="celda_box1"/>
        </Columns>
    </asp:GridView>
</div>
<div id="botones" style="width: 100%; height:10%;">

    <div class="solicitud100sin">

          <input type="button" id="btnVolver" value="VOLVER" class="btn_form" onclick="IrA('frmAcuerdoGestion.aspx?idModulo=6');"/>
    </div> 
</div>

</asp:Content>
