﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using ProcesoPP.Services;
using ProcesoPP.Model;
using ProcesoPP.Business;
using System.Collections.Generic;

namespace ProcesoPP.UI
{
    public partial class frmLogin : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (Request["Id"] != null)
                {
                    string StringDecrypted = PAB.Utils.Encryption.DeHex(Request["Id"].ToString());
                    string[] param = StringDecrypted.Split('&', '=');
                    Log(param[1].ToString(), param[3].ToString());
                }

                this.txtNombre.Focus();
            }
        }

        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                Log();
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Detalle del error: " + ex.Message, this.Page, "ERROR", null);
            }
        }

        protected void Log()
        {
            Utility oUtility = new Utility();
            Usuario oUsuario = new Usuario();
            UsuarioBus oUsuarioBus = new UsuarioBus();
            oUsuario = oUsuarioBus.UsuarioGetByUserName(txtNombre.Text);
            if (oUsuario != null)
            {
                if (oUsuario.Clave == oUtility.Encrypt(txtClave.Text))
                {
                    Session["idUsuario"] = oUsuario.IdUsuario.ToString();                    
                    CargarPermisos(oUsuario.oTipoUsuario.idTipoUsuario);
                    Response.Redirect("frmPrincipal.aspx");
                }
                else
                {
                    Mensaje.errorMsj("CLAVE INCORRECTA.", this.Page, "ERROR", "frmLogin.aspx");
                }
            }
            else
            {
                Mensaje.errorMsj("Nombre Usuario no existe.", this.Page, "ERROR", "frmLogin.aspx");
            }
        }

        protected void Log(string usuario, string password)
        {
            Utility oUtility = new Utility();
            Usuario oUsuario = new Usuario();
            UsuarioBus oUsuarioBus = new UsuarioBus();
            oUsuario = oUsuarioBus.UsuarioGetByUserName(usuario);
            if (oUsuario != null)
            {
                if (oUsuario.Clave == oUtility.Encrypt(password))
                {
                    Session["idUsuario"] = oUsuario.IdUsuario.ToString();
                    CargarPermisos(oUsuario.oTipoUsuario.idTipoUsuario);
                    Response.Redirect("frmPrincipal.aspx");
                }
                else
                {
                    Mensaje.errorMsj("CLAVE INCORRECTA.", this.Page, "ERROR", "frmLogin.aspx");
                }
            }
            else
            {
                Mensaje.errorMsj("Nombre Usuario no existe.", this.Page, "ERROR", "frmLogin.aspx");
            }
        }

        private void CargarPermisos(int idTipoUsuario)
        {
            PermisosBus oPermiso = new PermisosBus();
            List<Permisos> oListPermisos = oPermiso.PermisosGetIdTipoUsuario(idTipoUsuario);
            Session["Permisos"] = oListPermisos;

            ////REVISAR SI LA PERSONA TIENE PERMISOS PARA EL MODULO DE REMITOS
            //if(oListPermisos.Exists(p=>p.oModulo.Descripcion.ToLower().Contains("remi") && p.Escritura)){
            //    ParteEntregaBus oParteEntregaBus = new ParteEntregaBus();
            //    oParteEntregaBus.ParteInterno();
            //}
        }

    }
}
