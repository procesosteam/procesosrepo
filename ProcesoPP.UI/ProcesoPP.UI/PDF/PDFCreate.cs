﻿using System;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using System.Text;
using iTextSharp.text.pdf.parser;
namespace ProcesoPP.UI.PDF
{
    public class PDFCreate
    {

        public void exportarPDF(string docName, HtmlGenericControl div)
        {
            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.ContentType = "application/pdf";
            HttpContext.Current.Response.AddHeader("content-disposition", String.Format("attachment;filename={0}.pdf", docName.Replace(" ", "_")));
            HttpContext.Current.Response.Charset = "utf-8";

            StringWriter sw = new StringWriter();
            HtmlTextWriter hTextWriter = new HtmlTextWriter(sw);
            div.RenderControl(hTextWriter);
            string html = sw.ToString();
             
            Document document = new Document(PageSize.A4, 80, 50, 30, 65);
            TextReader reader = new StringReader(html);
            HTMLWorker worker = new HTMLWorker(document);

            PdfWriter.GetInstance(document, HttpContext.Current.Response.OutputStream);
            document.Open();
            worker.StartDocument();
            worker.Parse(reader);

            //StyleSheet styles = new iTextSharp.text.html.simpleparser.StyleSheet();            
            //List<IElement> listElement = HTMLWorker.ParseToList(new StringReader(html), styles);
            //listElement = listElement.Where(e => e.Chunks != null).ToList();
            //foreach (IElement E in listElement)                
            //    document.Add(E);
            worker.EndDocument();
            worker.Close();
            document.Close();
            HttpContext.Current.Response.End();
        }

        public void obtenerTextoFicheroPDF(string ficheroPDF)
        {
            StringBuilder textoPDFIndexado = new StringBuilder();
            try
            {
                if (File.Exists(ficheroPDF))
                {
                    PdfReader pdfReader = new PdfReader(ficheroPDF);
                    Document pdfDoc = new Document(PageSize.LEGAL.Rotate(), 10, 10, 40, 40);
                    PdfWriter.GetInstance(pdfDoc, HttpContext.Current.Response.OutputStream);

                    for (int page = 1; page <= pdfReader.NumberOfPages; page++)
                    {
                        ITextExtractionStrategy strategy = new SimpleTextExtractionStrategy();
                        string currentText = PdfTextExtractor.GetTextFromPage(pdfReader, page, strategy);

                        currentText = Encoding.UTF8.GetString(ASCIIEncoding.Convert(
                            Encoding.Default, Encoding.UTF8,
                            Encoding.Default.GetBytes(currentText)));
                        textoPDFIndexado.Append(currentText);
                        pdfReader.Close();
                    }


                    HttpContext.Current.Response.ContentType = "application/pdf";
                    HttpContext.Current.Response.AddHeader("content-disposition", "attachment;filename=ParteEntrega.pdf");
                    HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
                    HttpContext.Current.Response.Write(pdfDoc);
                    HttpContext.Current.Response.End();
                }
                //return textoPDFIndexado.ToString();
                
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
