﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using ProcesoPP.Business;
using ProcesoPP.Model;
using ProcesoPP.Services;

namespace ProcesoPP.UI.SolicitudServicio
{
    public partial class frmSolicitudGestion : System.Web.UI.Page
    {
        #region PROPIEDADES

        public Permisos _Permiso
        {
            get { return (Permisos)ViewState["permiso"]; }
            set { ViewState["permiso"] = value; }
        }

        public int _idUsuario
        {
            get { return (int)ViewState["idUsuario"]; }
            set { ViewState["idUsuario"] = value; }
        }
        
        public int _ADM
        {
            get { return int.Parse(ConfigurationManager.AppSettings["ADM"].ToString()); }
        }

        #endregion
        
        #region EVENTOS

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                 _idUsuario = Common.EvaluarSession(Request, this.Page);
                 if (_idUsuario != 0)
                 {
                     _Permiso = Common.getModulo(Request, this.Page);
                     if (_Permiso != null && _Permiso.Lectura)
                     {
                         if (!IsPostBack)
                         {
                             CargarEmpresa();
                             CargarEquipo();                             
                         }
                     }
                     else
                     {
                         Mensaje.errorMsj("NO TIENE PERMISOS A ESTE FORMULARIO", this.Page, "SIN ACCESO", "../frmPrincipal.aspx");
                     }
                 }
                 else
                 {
                     Mensaje.errorMsj("DEBE LOGUEARSE", this.Page, "LOGIN", "../frmLogin.aspx");
                 }
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                CargarSolicitud();
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        protected void gvSolicitud_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "OTI":
                    HiddenField hfIdOTI = ((DataControlFieldCell)((Control)(e.CommandSource)).Parent).Controls[1] as HiddenField;
                    Response.Redirect("../OrdenTrabajo/frmOrdenTrabajoCarga.aspx?idSolicitud=" + e.CommandArgument + "&idOTI=" + hfIdOTI.Value + "&idModulo=" + _Permiso.oModulo.idModulo);
                    break;
                case "Modificar":
                    Response.Redirect("frmSolicitudCarga.aspx?idSolicitud=" + e.CommandArgument + "&idModulo=" + _Permiso.oModulo.idModulo);
                    break;
                case "Regreso":
                    Response.Redirect("../Regreso/frmSolicitudRegresoCarga.aspx?idSolicitud=" + e.CommandArgument + "&idModulo=" + _Permiso.oModulo.idModulo);
                    break;
                case "NuevoMovimiento":
                    Response.Redirect("frmSolicitudCarga.aspx?idSolicitud=" + e.CommandArgument + "&NuevoMovimiento=true" + "&idModulo=" + _Permiso.oModulo.idModulo);
                    break;
                case"Eliminar":
                    try
                    {
                        Eliminar(e.CommandArgument.ToString());
                    }
                    catch (Exception ex)
                    {
                        Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
                    }
                    break;
            }
        }

        protected void gvSolicitud_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
                CargarRow(e);
        }

        protected void gvSolicitud_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvSolicitud.PageIndex = e.NewPageIndex;
            CargarSolicitud();
        }

        #endregion

        #region METODOS


        private void CargarRow(GridViewRowEventArgs e)
        {           
            HiddenField hfEntrega = e.Row.FindControl("hfEntrega") as HiddenField;
            HiddenField hfIdOT = e.Row.FindControl("hfIdOT") as HiddenField;
            LinkButton btnRegreso = e.Row.FindControl("btnRegreso") as LinkButton;
            LinkButton btnOTI = e.Row.FindControl("btnOT") as LinkButton;
            LinkButton btnNuevoMov = e.Row.FindControl("btnNuevoMov") as LinkButton;
            LinkButton btnEliminar = e.Row.FindControl("btnEliminar") as LinkButton;
            Label lblNroPed = e.Row.FindControl("lblNroPed") as Label;
            Label lblNroSolicitud = e.Row.FindControl("lblNroSolicitud") as Label;

            if (lblNroPed.Text == string.Empty)
            {
                lblNroPed.Visible = false;
                lblNroSolicitud.Visible = true;
            }
            else
            {
                lblNroPed.Visible = true;
                lblNroSolicitud.Visible = false;
            }

            if (!bool.Parse(hfEntrega.Value))
            {
                btnRegreso.Visible = false;
                btnOTI.Visible = false;
                ((DataControlFieldCell)(btnRegreso.Parent)).Text = "R";                
            }
            if (int.Parse(hfIdOT.Value) == 0)
            {
                btnRegreso.Visible = false;
                btnNuevoMov.Visible = false;
            }
            if (!_Permiso.Escritura)
                btnOTI.Visible = false;
            Common.CargarRow(e, _Permiso);

            if (_Permiso.oTipoUsuario.idTipoUsuario == _ADM)
                btnEliminar.Visible = true;
            else
                btnEliminar.Visible = false;

        }        
    

        private void Eliminar(string idSolicitud)
        {
            SolicitudCliente oSolicitudCliente = new SolicitudCliente();
            SolicitudClienteBus oSolicitudClienteBus = new SolicitudClienteBus();
            oSolicitudCliente = oSolicitudClienteBus.SolicitudClienteGetById(int.Parse(idSolicitud));
            oSolicitudCliente.Baja = true;
            oSolicitudClienteBus.SolicitudClienteUpdate(oSolicitudCliente);

            CargarSolicitud();
        }
        
        private void CargarEmpresa()
        {
            EmpresaBus oEmpresaBus = new EmpresaBus();
            ddlEmpresa.DataSource = oEmpresaBus.EmpresaGetAll();
            ddlEmpresa.DataTextField = "Descripcion";
            ddlEmpresa.DataValueField = "idEmpresa";
            ddlEmpresa.DataBind();
            ddlEmpresa.Items.Insert(0, new ListItem("Seleccione...","0"));
        }

        private void CargarSolicitud()
        {
            SolicitudClienteBus oSolicitudClienteBus = new SolicitudClienteBus();
            gvSolicitud.DataSource = oSolicitudClienteBus.SolicitudClienteGetByFilter(txtNroSolicitud.Text==""?"0":txtNroSolicitud.Text,
                                                                                      ddlEmpresa.SelectedValue,
                                                                                      txtFecha.Text,
                                                                                      txtFechaHasta.Text,
                                                                                      ddlTrailer.SelectedValue);
            gvSolicitud.DataBind();
        }

        private void CargarEquipo()
        {
            TrailerBus oTrailerBus = new TrailerBus();
            oTrailerBus.CargarCombo(ref ddlTrailer, "Seleccione un equipo...");
        }
        
        #endregion

    }
}
