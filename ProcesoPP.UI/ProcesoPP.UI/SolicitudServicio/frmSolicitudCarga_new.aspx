﻿<%@ Page Title="Carga Solicitud de Cliente" Language="C#" MasterPageFile="~/frmPrincipal.Master" AutoEventWireup="true" CodeBehind="frmSolicitudCarga.aspx.cs" Inherits="ProcesoPP.UI.SolicitudServicio.frmSolicitudCarga" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server"></asp:Content>
<asp:Content ID="cphContenedor" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="error"><asp:Label ID="lblError" CssClass="error2" runat="server" Text="Error" Visible="false"></asp:Label></div>
    <div id="contenido" runat="server">
    <div id="apertura">Solicitud de Servicio de Ida.</div>  
    
    
<div class="solicitud100">
<div class="sol_tit2"><label>SS N°</label></div>
<div class="sol_campob"><asp:TextBox  runat="server" Text="0" id="txtNroSS" onkeypress="return ValidarNumeroEntero(event,this);"/></div>
  
<div class="sol_tit2"><label>Condición</label></div>
<div class="sol_campob"><asp:DropDownList ID="ddlCondicion" runat="server" /></div></div>
    
      
<div class="solicitud100">
<div class="sol_tit"> <label>Fecha</label></div>
<div class="sol_campo2"><asp:TextBox  runat="server" id="txtFecha" /></div>
<div class="sol_fecha"><img id="btnFecha" alt="calendario" src="../img/calendario.gif" style="width:20px; height:20px; cursor:pointer;" onmouseover="CambiarConfiguracionCalendario('txtFecha',this);"/></div>
     </div>
      
    <div id="titulos">Datos del Solicitante.</div>      

<div class="solicitud100">
<div class="sol_tit"><label>Empresa</label></div>
<div class="sol_campo2"><asp:DropDownList ID="ddlEmpresa" runat="server"/></div><div class="sol_fecha"></div></div>

    
<div class="solicitud100">
<div class="sol_tit"><label>Nombre y Apellido</label></div>
<div class="sol_campo2"><asp:TextBox ID="txtNombre" runat="server" /></div><div class="sol_fecha"></div>
    </div>

<div class="solicitud100">
<div class="sol_tit"><label>Sector</label></div>
<div class="sol_campo2"><asp:DropDownList ID="ddlSector" runat="server"/></div><div class="sol_fecha"></div></div>  
            
<div class="solicitud100">    
<div class="sol_tit2"><label>Nº Aviso</label></div>
<div class="sol_campob"><asp:TextBox ID="txtNroAviso" runat="server" /></div>
       
<div class="sol_tit2"><label>Teléfono</label></div>
<div class="sol_campob"><asp:TextBox ID="txtTelefono" runat="server" /></div> </div>

    <div id="titulos">Tipo de Servicio</div>
      <div class="sol_100">
        <asp:DropDownList runat="server" ID="ddlTipoServicio"></asp:DropDownList></div>
      <div class="sol_100">
        <asp:DropDownList runat="server" ID="ddlEspecificacion"></asp:DropDownList></div>
    
    <div id="titulos">Movimiento</div>

<div class="solicitud100">
<div class="sol_tit2"><label>Origen</label></div>
<div class="sol_campob"><asp:TextBox ID="txtOrigen" runat="server" /></div>

<div class="sol_tit2"><label>Destino</label></div>
<div class="sol_campob"><asp:TextBox ID="txtDestino" runat="server"/></div></div>

<div class="solicitud100">
<div class="sol_tit2"><label>Pozo</label></div>
<div class="sol_campob"><asp:TextBox ID="txtPozo" runat="server"/></div>

<div class="sol_tit2">
  <label>Kms Est.</label></div>
<div class="sol_campob"><asp:TextBox ID="txtKms" runat="server" onkeypress="return ValidarNumeroDecimal(event,this,2);"/></div>
    </div>
<div class="solicitud100">
<div class="sol_btn">
      <asp:Button runat="server" id="btnGuardar" Text="GUARDAR" cssclass="btn_form"
            OnClientClick="return ValidarObj('txtNroSS|Ingrese le Nro. de Solicitud de Servicio.~ddlCondicion|Seleccione una condición.~ddlEmpresa|Seleccione una empresa.~txtNombre|Debe ingresar nombre y apellido del Solicitante.~ddlSector|Debe seleccionar el sector.~ddlTipoServicio|Debe seleccionar un tipo de servicio.~ddlEspecificacion|Debe seleccionar un especificació.')"
            onclick="btnGuardar_Click"></asp:Button>
    </div></div>

</div>
</asp:Content>
