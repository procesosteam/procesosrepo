﻿<%@ Page Title="Carga Solicitud de Cliente" Language="C#" MasterPageFile="~/frmPrincipal.Master" MaintainScrollPositionOnPostback="true"
    AutoEventWireup="true" CodeBehind="frmSolicitudCarga.aspx.cs" Inherits="ProcesoPP.UI.SolicitudServicio.frmSolicitudCarga" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="cphContenedor" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="tabla3" id="divMensaje" runat="server">
        <asp:Label ID="lblError" CssClass="error2" runat="server" Text="Error" Visible="false"></asp:Label></div>
    <div id="contenido" runat="server">
        <div id="apertura">Solicitud de Servicio.</div>
        <div class="solicitud100">
            <div class="sol_tit2">
                <label>SS N°</label></div>
            <div class="sol_campo30">
                <asp:TextBox runat="server" CssClass="sol_campo25" Text="0" ID="txtNroSS" onkeypress="return ValidarNumeroEntero(event,this);" />
            </div>
            <div class="sol_tit2">
                <label>Condición</label></div>
            <div class="sol_campo30">
                <asp:DropDownList ID="ddlCondicion" runat="server" />
            </div>
            <div class="sol_tit2">
                <label>Fecha</label></div>
            <div class="sol_campo30">
                <asp:TextBox CssClass="sol_campo25" runat="server" ID="txtFecha" /></div>
            <div class="sol_fecha">
                <img id="btnFecha" alt="calendario" src="../img/calendario.gif" style="width: 20px;
                    height: 20px; cursor: pointer;" onmouseover="CambiarConfiguracionCalendario('txtFecha',this);" /></div>
        </div>
        <div id="titulos">Datos del Solicitante.</div>
        <div class="solicitud100">
            <div class="sol_tit2">
                <label>Empresa</label></div>
            <div class="sol_campo30">
                <asp:DropDownList ID="ddlEmpresa" runat="server" AutoPostBack="true" 
                    onselectedindexchanged="ddlEmpresa_SelectedIndexChanged"/>
            </div>
            <div class="sol_tit2">
                <label>Sector</label></div>
            <div class="sol_campo30">
                <asp:DropDownList ID="ddlSector" runat="server" AutoPostBack="true" 
                    onselectedindexchanged="ddlSector_SelectedIndexChanged"/>
            </div>
            <div class="sol_tit2">
                <label>Solicitante</label></div>
            <div class="sol_campo30">
                <asp:DropDownList ID="ddlSolicitante" runat="server" /></div>
            
            <div class="sol_fecha"></div>
        </div>
        <div style="visibility:hidden">  
            <div class="sol_tit2"><label>Nº Aviso</label></div>
            <div class="sol_campo30">
                <asp:TextBox ID="txtNroAviso" CssClass="sol_campo25" runat="server" /></div>
            <div class="sol_tit2"><label>Teléfono</label></div>
            <div class="sol_campo30">
                <asp:TextBox ID="txtTelefono" CssClass="sol_campo25" runat="server" /></div>           
        </div>
        <div id="titulos">Tipo de Servicio</div>
        <div class="sol_100" id="divTipoServicio">
            <asp:DropDownList runat="server" ID="ddlTipoServicio" AutoPostBack="true" OnSelectedIndexChanged="ddlTipoServicio_SelectedIndexChanged">
            </asp:DropDownList>
        </div>
        <div class="sol_100" id="divEspecificaciones" runat="server" visible="false">
            <asp:CheckBoxList runat="server" ID="ddlEspecificacion" RepeatColumns="4"></asp:CheckBoxList>
        </div>
        <div id="titulos">Movimiento</div>
        <div class="solicitud100">
            <div class="sol_tit2"><label>Origen</label></div>
            <div class="sol_campo30">
                <asp:DropDownList ID="ddlDesde" CssClass="sol_campo25" runat="server" 
                    onselectedindexchanged="ddlDesde_SelectedIndexChanged" AutoPostBack="true" /></div>
            <div class="sol_tit2"><label>Pozo / Eq.</label></div>
            <div class="sol_campo30">
                <asp:TextBox ID="txtPozoOrigen" CssClass="sol_campo25" runat="server" /></div>
            <div class="sol_tit2"></div>
            <div class="sol_campo30"></div>
            <div class="sol_fecha"></div>
        </div>
        <div class="solicitud100">
            <div class="sol_tit2"><label>Destino</label></div>
            <div class="sol_campo30">
                <asp:DropDownList ID="ddlHasta" CssClass="sol_campo25" runat="server" 
                    onselectedindexchanged="ddlDesde_SelectedIndexChanged" AutoPostBack="true"/></div>
            <div class="sol_tit2"><label>Pozo / Eq.</label></div>
            <div class="sol_campo30">
                <asp:TextBox ID="txtPozoDestino" CssClass="sol_campo25" runat="server" /></div>
            <div class="sol_tit2">
                <label>Kms Est.</label></div>
            <div class="sol_campo30">
                <asp:TextBox ID="txtKms" CssClass="sol_campo25" runat="server" ReadOnly="true" onkeypress="return ValidarNumeroDecimal(event,this,2);" /></div>
            <div class="sol_fecha"></div>
        </div>
        <div class="solicitud100">
            <div class="sol_btn">
                <asp:Button runat="server" ID="btnGuardar" Text="GUARDAR" CssClass="btn_form" OnClientClick="return ValidarObj('txtNroSS|Ingrese le Nro. de Solicitud de Servicio.~ddlCondicion|Seleccione una condición.~txtFecha|Debe ingresar una fecha.~ddlEmpresa|Seleccione una empresa.~txtNombre|Debe ingresar nombre y apellido del Solicitante.~ddlSector|Debe seleccionar el sector.~ddlTipoServicio|Debe seleccionar un tipo de servicio.~ddlEspecificacion|Debe seleccionar un especificació.~txtOrigen|Debe ingresar el origen.~txtDestino|Debe ingresar el destino.~txtPozo| Debe ingresar el nombre del pozo.~ txtKms|Debe ingresar los Kms estimados.')"
                    OnClick="btnGuardar_Click"></asp:Button>
            </div>
            <div class="sol_btn">
                <input type="button" id="btnVolver" value="Volver" class="btn_form" onclick="Volver();" />
            </div>
        </div>
    </div>
    <div id="capaPopUp"></div>
    <div id="popUpDiv">
        <div id="capaContent">
            <div style="width: 450px;">
                <asp:GridView ID="gvTransporte" runat="server" AutoGenerateColumns="False" DataMember="idTipoServicio"
                    CssClass="tabla3" Width="95%" EmptyDataText="No existen registros" EmptyDataRowStyle-CssClass="celda50"
                    EmptyDataRowStyle-Width="80%">
                    <Columns>
                        <asp:TemplateField HeaderStyle-CssClass="celda20t" ItemStyle-CssClass="celda20">
                            <ItemTemplate>
                                <asp:CheckBox runat="server" ID="chkTrailer" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="idTrailer" HeaderText="id" HeaderStyle-CssClass="celda30t"
                            ItemStyle-CssClass="celda30" />
                        <asp:BoundField DataField="Descripcion" HeaderText="Descripcion" HeaderStyle-CssClass="celda40t"
                            ItemStyle-CssClass="celda40" />
                    </Columns>
                </asp:GridView>
            </div>
            <div id="botones" style="width: 95%;">
                <div class="regreso_3">
                    <input type="button" onclick="cerrarPopUp();" title="Cancelar" id="btnCerrar" class="btn_form"
                        value="Cancelar" />
                </div>
                <div class="regreso_3">
                    <input type="button" onclick="idOT();" title="Ir a la orden de trabajo" id="btnIrOT"
                        class="btn_form" value="Ir OT" />
                </div>
                <div class="regreso_3">
                    <input type="button" onclick="idGestion();" title="Ir a gestión de solicitudes" id="btnIrGestion"
                        class="btn_form" value="Ir Gestión" />
                </div>
            </div>
        </div>
    </div>
    <asp:HiddenField runat="server" ID="hfIdSol" Value="0" />
</asp:Content>
