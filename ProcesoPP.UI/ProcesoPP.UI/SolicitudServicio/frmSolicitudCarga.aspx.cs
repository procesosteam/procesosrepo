﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ProcesoPP.Business;
using ProcesoPP.Model;
using ProcesoPP.Services;
using System.Data;
using System.Configuration;

namespace ProcesoPP.UI.SolicitudServicio
{
    public partial class frmSolicitudCarga : System.Web.UI.Page
    {
        #region <PROPIEDADES>

        public int _idUsuario
        {
            get { return (int)ViewState["idUsuario"]; }
            set { ViewState["idUsuario"] = value; }
        }

        public int _idSolicitud
        {
            get { return (int)ViewState["idSolicitud"]; }
            set { ViewState["idSolicitud"] = value; }
        }

        public bool _nuevoMovimiento
        {
            get { return (bool)ViewState["nuevoMovimiento"]; }
            set { ViewState["nuevoMovimiento"] = value; }
        }

        public Permisos _Permiso
        {
            get { return (Permisos)ViewState["permiso"]; }
            set { ViewState["permiso"] = value; }
        }
        public bool _tieneOti
        {
            get { return (bool)ViewState["tieneoti"]; }
            set { ViewState["tieneoti"] = value; }
        }
        
        #endregion

        #region EVENTOS

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                _idUsuario = Common.EvaluarSession(Request, this.Page);
                if (_idUsuario != 0)
                {
                    _Permiso = Common.getModulo(Request, this.Page);
                    if (_Permiso != null && _Permiso.Lectura)
                    {
                        if (!IsPostBack)
                        {
                            CargarCondicion();
                            CargarEmpresa();
                            CargarTipoServicio();
                            InitScreen();
                        }
                    }
                    else
                    {
                        Mensaje.errorMsj("NO TIENE PERMISOS A ESTE FORMULARIO", this.Page, "SIN ACCESO", "../frmPrincipal.aspx");
                    }
                }
                else
                {
                    Mensaje.errorMsj("DEBE LOGUEARSE", this.Page, "LOGIN", "../frmLogin.aspx");
                }
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                GuardarSolicitud();
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }
        protected void btnEnviar_Click(object sender, EventArgs e)
        {
            try
            {
                enviarMail();
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj(ex.Message, this.Page, "Error", null);
            }
        }
        
        protected void ddlTipoServicio_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddlTipoServicio.SelectedIndex > 0)
                {
                    TipoServicioBus oTipoServicioBus = new TipoServicioBus();
                    if (oTipoServicioBus.TipoServicioGetById(int.Parse(ddlTipoServicio.SelectedValue)).oEspecificacion.Count > 0)
                    {
                        txtEspecif.Visible = false;
                        ddlEspecificacion.Visible = true;
                        CargarEspecificacion(int.Parse(ddlTipoServicio.SelectedValue));
                    }
                    else
                    {
                        ddlEspecificacion.Visible = false;
                        txtEspecif.Visible = true;
                    }
                }
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        protected void ddlEmpresa_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                CargarSector(ddlEmpresa.SelectedValue);
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        protected void ddlSector_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                CargarSolicitantes(ddlSector.SelectedValue, ddlEmpresa.SelectedValue);
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        protected void ddlDesde_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddlDesde.SelectedIndex > 0 && ddlHasta.SelectedIndex > 0)
                    TraerKilometraje();
            }
            catch (Exception ex)
            {
                Mensaje.errorMsj("Ha ocurrido un error. Detalle: " + ex.Message, this.Page, "Error", null);
            }
        }

        #endregion

        #region METODOS

        private void InitScreen()
        {
            CargarLugares(ddlDesde);
            SelectedBPP(ddlDesde);
            CargarLugares(ddlHasta);

            _nuevoMovimiento = false;
            _tieneOti = false;

            if (Request["idSolicitud"] != null)
            {
                _idSolicitud = int.Parse(Request["idSolicitud"]);
                if (Request["NuevoMovimiento"] != null)                
                    _nuevoMovimiento = bool.Parse(Request["NuevoMovimiento"].ToString());
                
                CargarSolicitud();                
            }
            else
            {
                ddlSector.Items.Insert(0, new ListItem("N/A", "0"));
                ddlSolicitante.Items.Insert(0, new ListItem("N/A", "0"));

                _idSolicitud = 0;
                SugerirNroSS();
            }
            TraerKilometraje();
            if (!_Permiso.Escritura)
            {
                btnGuardar.Visible = false;
            }
        }

        private void SelectedBPP(DropDownList ddl)
        {
            LugarBus oLugarBus = new LugarBus();
            oLugarBus.SelectedBPP(ref ddl);
        }

        private void TraerKilometraje()
        {
            KilometrosBus oKilometrosBus = new KilometrosBus();
            Kilometros oKilometros = new Kilometros();
            oKilometros.oLugarDesde = new Lugar();
            oKilometros.oLugarHasta = new Lugar();
            oKilometros.oLugarDesde.IdLugar = int.Parse(ddlDesde.SelectedItem.Value);
            oKilometros.oLugarHasta.IdLugar = int.Parse(ddlHasta.SelectedItem.Value);

            oKilometros = oKilometrosBus.KilometrosGetBy(oKilometros);
            if (oKilometros != null)
                txtKms.Text = oKilometros.Kms.ToString("F0");
            else
                txtKms.Text = "0";
        }

        private void CargarSolicitud()
        {
            SolicitudCliente oSolicitudCliente = new SolicitudCliente();
            SolicitudClienteBus oSolicitudClienteBus = new SolicitudClienteBus();

            oSolicitudCliente = oSolicitudClienteBus.SolicitudClienteGetById(_idSolicitud);
            CargarEmpresa();
            ddlEmpresa.SelectedValue = oSolicitudCliente.oSector.IdEmpresa.ToString();
            CargarSector(ddlEmpresa.SelectedItem.Value);
            ddlSector.SelectedValue = oSolicitudCliente.IdSector.ToString();
            CargarSolicitantes(ddlSector.SelectedItem.Value, ddlEmpresa.SelectedItem.Value);
            ddlSolicitante.SelectedValue = oSolicitudCliente.IdSolicitante.ToString();

            txtPozoOrigen.Text = oSolicitudCliente.oMovimiento.PozoOrigen;
            ddlDesde.SelectedValue = oSolicitudCliente.oMovimiento.oLugarOrigen.IdLugar.ToString();
            txtPozoDestino.Text = oSolicitudCliente.oMovimiento.PozoDestino;
            ddlHasta.SelectedValue = oSolicitudCliente.oMovimiento.oLugarDestino.IdLugar.ToString();
            txtKms.Text = oSolicitudCliente.oMovimiento.KmsEstimados.ToString();

            txtNroSS.Text = oSolicitudCliente.NroSolicitud.ToString();
            txtFecha.Text = oSolicitudCliente.Fecha.ToString("dd/MM/yyyy");
            ddlTipoServicio.SelectedValue = oSolicitudCliente.IdTipoServicio.ToString();
            ddlCondicion.SelectedValue = oSolicitudCliente.IdCondicion.ToString();
            txtNroAviso.Text = oSolicitudCliente.NroPedidoCliente;
            txtEspecif.Text = oSolicitudCliente.ServicioAux;
            //txtFechaEstimada.Text = oSolicitudCliente.FechaEstimada.ToString("dd/MM/yyyy") == "01/01/1900" ? "" : oSolicitudCliente.FechaEstimada.ToString("dd/MM/yyyy");
            
            if (oSolicitudCliente.oTipoServicio.oEspecificacion.Count > 0)
            {
                ddlEspecificacion.Visible = true;
                txtEspecif.Visible = false;
                CargarEspecificacion(oSolicitudCliente.IdTipoServicio);
                SelectedEspecificaciones(oSolicitudCliente.oListEspecificaciones);
            }
            else
            {
                ddlEspecificacion.Visible = false;
                txtEspecif.Visible = true;
            }
            RedefinirPantallaNuevoMov(oSolicitudCliente);
            
        }

        
        private bool VerificarOTI(int idSol)
        {
            OrdenTrabajoBus oOrdenTrabajoBus = new OrdenTrabajoBus();
            return oOrdenTrabajoBus.OrdenTrabajoGetAll().Exists(o => o.IdSolicitudCliente == idSol);
        }

        private void RedefinirPantallaNuevoMov(SolicitudCliente oSolicitudCliente)
        {
            if (_nuevoMovimiento)
            {
                //_tieneOti = VerificarOTI(_idSolicitud);
                //SugerirNroSS();
                _idSolicitud = 0;
                nuevoMovEnable();
                txtPozoDestino.Text = oSolicitudCliente.oMovimiento.PozoOrigen;
                ddlHasta.SelectedValue = oSolicitudCliente.oMovimiento.oLugarOrigen.IdLugar.ToString();
                txtPozoOrigen.Text = oSolicitudCliente.oMovimiento.PozoDestino;
                ddlDesde.SelectedValue = oSolicitudCliente.oMovimiento.oLugarDestino.IdLugar.ToString();
                txtFecha.Text = string.Empty;
                ddlSector.SelectedIndex = 0;
                ddlSolicitante.SelectedIndex = 0;
                ddlHasta.SelectedIndex = 0;
            }
            else
            {
                divSolServ.InnerText = "Solicitud de Servicio";
                _nuevoMovimiento = oSolicitudCliente.nuevoMovimiento;
                if (_nuevoMovimiento)
                    nuevoMovEnable();
            }

        }

        private void nuevoMovEnable()
        {
            divSolServ.InnerText = "Nuevo Movimiento";
            ddlEmpresa.Enabled = false;
            ddlTipoServicio.Enabled = false;
            ddlEspecificacion.Enabled = false;
            txtNroSS.Enabled = false;
            ddlCondicion.Enabled = false;
            txtEspecif.Enabled = false;
            ddlDesde.Enabled = false;
        } 

        private void SelectedEspecificaciones(List<EspecificacionTecnica> oListaEsp)
        {
            foreach (ListItem chk in ddlEspecificacion.Items)
            {
                if (oListaEsp.Exists(e => e.IdEspecificaciones == int.Parse(chk.Value)))
                    chk.Selected = true;
                else
                    chk.Selected = false;
            }
        }

        private void SugerirNroSS()
        {
            SolicitudClienteBus oSolicitudClienteBus = new SolicitudClienteBus();
            txtNroSS.Text = oSolicitudClienteBus.SolicitudClienteSugerirNro().ToString();
        }

        private void GuardarSolicitud()
        {            
            if ((int.Parse(txtKms.Text) != 0) //&& (ddlDesde.SelectedValue != ddlHasta.SelectedValue))
                || ((int.Parse(txtKms.Text) == 0) && (ddlDesde.SelectedValue == ddlHasta.SelectedValue)))
            {
                SolicitudClienteBus oSolicitudClienteBus = new SolicitudClienteBus();
                SolicitudCliente oSolicitudCliente = new SolicitudCliente();
                if (_idSolicitud != 0)
                    oSolicitudCliente = oSolicitudClienteBus.SolicitudClienteGetById(_idSolicitud);
                else
                {
                    oSolicitudCliente.oMovimiento = new Movimiento();
                    oSolicitudCliente.oMovimiento.oLugarDestino = new Lugar();
                    oSolicitudCliente.oMovimiento.oLugarOrigen = new Lugar();
                    oSolicitudCliente.Entrega = true;
                }

                oSolicitudCliente.IdSector = int.Parse(ddlSector.SelectedValue);
                oSolicitudCliente.IdSolicitante = int.Parse(ddlSolicitante.SelectedValue);

                oSolicitudCliente.oMovimiento.oLugarOrigen.IdLugar = int.Parse(ddlDesde.SelectedItem.Value);
                oSolicitudCliente.oMovimiento.PozoDestino = txtPozoDestino.Text;

                oSolicitudCliente.oMovimiento.oLugarDestino.IdLugar = int.Parse(ddlHasta.SelectedItem.Value);
                oSolicitudCliente.oMovimiento.PozoOrigen = txtPozoOrigen.Text;
                oSolicitudCliente.oMovimiento.KmsEstimados = int.Parse(txtKms.Text);

                oSolicitudCliente.NroSolicitud = int.Parse(txtNroSS.Text);
                oSolicitudCliente.Fecha = DateTime.Parse(txtFecha.Text);
                oSolicitudCliente.Hora = DateTime.Parse(txtFecha.Text);
                oSolicitudCliente.FechaAlta = DateTime.Now;
                oSolicitudCliente.IdTipoServicio = int.Parse(ddlTipoServicio.SelectedValue);
                oSolicitudCliente.oListEspecificaciones = getId(ddlEspecificacion);
                oSolicitudCliente.IdCondicion = int.Parse(ddlCondicion.SelectedValue);
                oSolicitudCliente.NroPedidoCliente = txtNroAviso.Text;
                oSolicitudCliente.ServicioAux = txtEspecif.Text;
                oSolicitudCliente.IdMovimiento = oSolicitudCliente.oMovimiento.IdMovimiento;
                oSolicitudCliente.IdUsuario = _idUsuario;
                oSolicitudCliente.FechaEstimada = DateTime.MinValue;
                oSolicitudCliente.nuevoMovimiento = _nuevoMovimiento;

                if (_idSolicitud == 0)
                {
                    _idSolicitud = oSolicitudCliente.IdSolicitud = oSolicitudClienteBus.SolicitudClienteAdd(oSolicitudCliente);
                    enviarMail();
                    hfIdSol.Value = oSolicitudCliente.IdSolicitud.ToString();
                    if (ddlEspecificacion.Visible)
                    {
                        TrailerBus oTrailerBus = new TrailerBus();
                        if (!_nuevoMovimiento)
                            gvTransporte.DataSource = oTrailerBus.TrailerGetDisponibles(getId(ddlEspecificacion));
                        else
                            gvTransporte.DataSource = oTrailerBus.TrailerGetNoDisponibles(getId(ddlEspecificacion), oSolicitudCliente.NroSolicitud);
                        gvTransporte.DataBind();
                        if (_nuevoMovimiento && gvTransporte.HeaderRow != null && gvTransporte.HeaderRow.Cells[1] != null)
                            gvTransporte.HeaderRow.Cells[1].Text = "EQUIPOS NO DISPONIBLES";
                        Mensaje.mostrarPopUp(this);
                    }
                    else
                    {
                        btnGuardar.Visible = false;
                        Mensaje.successMsj("Solicitud guardada con éxito.", this.Page, "Exito", "frmSolicitudGestion.aspx?idModulo=" + _Permiso.oModulo.idModulo.ToString());
                    }
                }
                else
                {
                    oSolicitudCliente.IdSolicitud = _idSolicitud;
                    oSolicitudClienteBus.SolicitudClienteUpdate(oSolicitudCliente);

                    Mensaje.successMsj("Solicitud guardada con éxito.", this.Page, "Exito", "frmSolicitudGestion.aspx?idModulo=" + _Permiso.oModulo.idModulo.ToString());
                }
            }
            else
            {
                Mensaje.errorMsj("Debe ingresar un kilometraje mayor a 0(cero).", this.Page, "ERROR", null);
            }
        }

        private void enviarMail()
        {
            try
            {
                SolicitudClienteBus oSolicitudCliente = new SolicitudClienteBus();
                DataTable dt = oSolicitudCliente.SolicitudClienteSinOTI();
                var message = new System.Text.StringBuilder();
                message.Append("<table border='1'>");
                message.Append("<tr>");
                message.Append("<td>N° SS</td>");
                message.Append("<td>Fecha</td>");
                message.Append("<td>Empresa</td>");
                message.Append("<td>Sector</td>");
                message.Append("<td>Servicio</td>");
                message.Append("<td>Origen</td>");
                message.Append("<td>Pozo</td>");
                message.Append("<td>Destino</td>");
                message.Append("<td>Pozo</td>");
                message.Append("</tr>");
                foreach (DataRow dr in dt.Rows)
                {
                    message.Append("<tr>");
                    message.Append("<td>" + dr["NroSolicitud"] + "</td>");
                    message.Append("<td>" + dr["Fecha"] + "</td>");
                    message.Append("<td>" + dr["Empresa"] + "</td>");
                    message.Append("<td>" + dr["Sector"] + "</td>");
                    message.Append("<td>" + dr["TipoServicio"] + "</td>");
                    message.Append("<td>" + dr["Origen"] + "</td>");
                    message.Append("<td>" + dr["PozoOrigen"] + "</td>");
                    message.Append("<td>" + dr["Destino"] + "</td>");
                    message.Append("<td>" + dr["PozoDestino"] + "</td>");
                    message.Append("</tr>");
                }
                message.Append("</table>");

                string para = ConfigurationManager.AppSettings["para"].ToString();
                string para2 = null;
                if (ConfigurationManager.AppSettings["para2"] != null)
                    para2 = ConfigurationManager.AppSettings["para2"].ToString();

                EnvioMail mail = new EnvioMail();
                mail.asunto = "OTI PENDIENTES A CONFIRMAR";
                mail.para = para;
                mail.para2 = para2;
                mail.cuerpo = message.ToString();
                mail.enviar();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private List<EspecificacionTecnica> getId(CheckBoxList chkList)
        {
            List<EspecificacionTecnica> oList = new List<EspecificacionTecnica>();
            foreach (ListItem chk in chkList.Items)
            {
                if (chk.Selected)
                {
                    EspecificacionTecnica tsEsp = new EspecificacionTecnica();
                    tsEsp.IdEspecificaciones = int.Parse(chk.Value);
                    tsEsp.Descripcion = chk.Text;
                    oList.Add(tsEsp);
                }
            }

            return oList;
        }

        private void CargarEspecificacion(int idTipoServicio)
        {            
            EspecificacionTecnicaBus oEspecificacionTecnicaBus = new EspecificacionTecnicaBus();
            ddlEspecificacion.DataSource = oEspecificacionTecnicaBus.EspecificacionTecnicaGetByIdTipoServicio(idTipoServicio);
            ddlEspecificacion.DataTextField = "Descripcion";
            ddlEspecificacion.DataValueField = "IdEspecificaciones";
            ddlEspecificacion.DataBind();
        }

        private void CargarTipoServicio()
        {
            TipoServicioBus oTipoServicioBus = new TipoServicioBus();
            ddlTipoServicio.DataSource = oTipoServicioBus.TipoServicioGetAll();
            ddlTipoServicio.DataTextField = "Descripcion";
            ddlTipoServicio.DataValueField = "idTipoServicio";            
            ddlTipoServicio.DataBind();
            ddlTipoServicio.Items.Insert(0, new ListItem("Seleccione Tipo Servicio", "0"));
        }

        private void CargarSector(string idEmpresa)
        {
            SectorBus oSectorBus = new SectorBus();
            ddlSector.DataSource = oSectorBus.SectorGetAllByIdEmpresa(int.Parse(idEmpresa));
            ddlSector.DataTextField = "Descripcion";
            ddlSector.DataValueField = "idSector";
            ddlSector.DataBind();
            ddlSector.Items.Insert(0, new ListItem("N/A", "0"));
        }

        private void CargarEmpresa()
        {
            EmpresaBus oEmpresaBus = new EmpresaBus();
            oEmpresaBus.CargarEmpresa(ref ddlEmpresa, "Seleccione...");
        }

        private void CargarCondicion()
        {
            CondicionBus oCondicionBus = new CondicionBus();
            ddlCondicion.DataSource = oCondicionBus.CondicionGetAll();
            ddlCondicion.DataTextField = "Descripcion";
            ddlCondicion.DataValueField = "idCondicion";
            ddlCondicion.DataBind();
            ddlCondicion.Items.Insert(0, new ListItem("N/A", "0"));
        }

        private void CargarSolicitantes(string idSector, string idEmpresa)
        {
            SolicitanteBus oSolicitanteBus = new SolicitanteBus();
            ddlSolicitante.DataSource = oSolicitanteBus.SolicitanteGetBySectorEmpresa(int.Parse(idSector), int.Parse(idEmpresa));
            ddlSolicitante.DataTextField = "Nombre";
            ddlSolicitante.DataValueField = "idSolicitante";
            ddlSolicitante.DataBind();
            ddlSolicitante.Items.Insert(0, new ListItem("N/A", "0"));
        }
        
        private void CargarLugares(DropDownList ddl)
        {
            LugarBus oLugarBus = new LugarBus();
            oLugarBus.CargarLugar(ref ddl);
            ddl.Items.Insert(0, new ListItem("N/A", "0"));

        }

        #endregion

    }
}
