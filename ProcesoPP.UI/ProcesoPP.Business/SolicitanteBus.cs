
using System;
using System.Data;
using ProcesoPP.SqlServerImpl;
using ProcesoPP.Model;
using System.Collections.Generic;
using AjaxControlToolkit;

namespace ProcesoPP.Business
{
	public class SolicitanteBus
	{
		#region Business

		public int SolicitanteAdd(Solicitante solicitante)
		{
			SolicitanteImpl oSolicitanteImpl = new SolicitanteImpl();
			return oSolicitanteImpl.SolicitanteAdd(solicitante);
		}

		public bool SolicitanteUpdate(Solicitante solicitante)
		{
			SolicitanteImpl oSolicitanteImpl = new SolicitanteImpl();
			return oSolicitanteImpl.SolicitanteUpdate(solicitante);
		}

		public bool SolicitanteDelete(int IdSolicitante)
		{
			SolicitanteImpl oSolicitanteImpl = new SolicitanteImpl();
			return oSolicitanteImpl.SolicitanteDelete(IdSolicitante);
		}

		public Solicitante SolicitanteGetById(int IdSolicitante)
		{
			SolicitanteImpl oSolicitanteImpl = new SolicitanteImpl();
			return oSolicitanteImpl.SolicitanteGetById(IdSolicitante);
		}

		public List<Solicitante> SolicitanteGetAll()
		{
			SolicitanteImpl oSolicitanteImpl = new SolicitanteImpl();
			return oSolicitanteImpl.SolicitanteGetAll();
		}

        public List<Solicitante> SolicitanteGetBySectorEmpresa(int idSector, int idEmpresa)
        {
            SolicitanteImpl oSolicitanteImpl = new SolicitanteImpl();
            List<Solicitante> oListSolicitantes = oSolicitanteImpl.SolicitanteGetByFilter(idSector, idEmpresa);
            return oListSolicitantes;
        }

        #endregion


        public List<CascadingDropDownNameValue> GetDataSolicitante(int idSector, int idEmpresa)
        {
            List<Solicitante> oListSolicitante = SolicitanteGetBySectorEmpresa(idSector, idEmpresa);
            List<CascadingDropDownNameValue> values = new List<CascadingDropDownNameValue>();
            foreach (Solicitante item in oListSolicitante)
            {
                CascadingDropDownNameValue oCdd = new CascadingDropDownNameValue();
                oCdd.name = item.Nombre;
                oCdd.value = item.IdSolicitante.ToString();
                values.Add(oCdd);
            }

            return values;
        }
    }
}
