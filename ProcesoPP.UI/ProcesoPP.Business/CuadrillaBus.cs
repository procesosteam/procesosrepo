
using System;
using System.Data;
using ProcesoPP.SqlServerImpl;
using ProcesoPP.Model;
using System.Collections.Generic;
using AjaxControlToolkit;

namespace ProcesoPP.Business
{
	public class CuadrillaBus
	{
		#region Business

		public int CuadrillaAdd(Cuadrilla cuadrilla)
		{
			CuadrillaImpl oCuadrillaImpl = new CuadrillaImpl();
			return oCuadrillaImpl.CuadrillaAdd(cuadrilla);
		}

		public bool CuadrillaUpdate(Cuadrilla cuadrilla)
		{
			CuadrillaImpl oCuadrillaImpl = new CuadrillaImpl();
			return oCuadrillaImpl.CuadrillaUpdate(cuadrilla);
		}

		public bool CuadrillaDelete(int IdCuadrilla)
		{
			CuadrillaImpl oCuadrillaImpl = new CuadrillaImpl();
			return oCuadrillaImpl.CuadrillaDelete(IdCuadrilla);
		}

		public Cuadrilla CuadrillaGetById(int IdCuadrilla)
		{
			CuadrillaImpl oCuadrillaImpl = new CuadrillaImpl();
			return oCuadrillaImpl.CuadrillaGetById(IdCuadrilla);
		}

		public List<Cuadrilla> CuadrillaGetAll()
		{
			CuadrillaImpl oCuadrillaImpl = new CuadrillaImpl();
			return oCuadrillaImpl.CuadrillaGetAll();
		}

		public List<Cuadrilla> CuadrillaGetAllBaja()
		{
			CuadrillaImpl oCuadrillaImpl = new CuadrillaImpl();
			return oCuadrillaImpl.CuadrillaGetAllBaja();
		}

		#endregion

        public DataTable CuadrillaGetByFilter(string nombre)
        {
            CuadrillaImpl oCuadrillaImpl = new CuadrillaImpl();
            return oCuadrillaImpl.CuadrillaGetByFilter(nombre);
        }

        public DataTable CuadrillaGestionGetByFilter(string desde, string hasta, string idPersonal, string idCuadrilla, string idSector, string idEmpresa)
        {
            CuadrillaImpl oCuadrillaImpl = new CuadrillaImpl();
            return oCuadrillaImpl.CuadrillaGestionGetByFilter(desde,hasta,idPersonal,idCuadrilla,idSector,idEmpresa);
        }

        public List<CascadingDropDownNameValue> GetDataCuadrilla()
        {
            CuadrillaImpl oCuadrillaImpl = new CuadrillaImpl();
            List<Cuadrilla> oListaCuadrilla = oCuadrillaImpl.CuadrillaGetAll();
            List<CascadingDropDownNameValue> values = new List<CascadingDropDownNameValue>();
            foreach (Cuadrilla item in oListaCuadrilla)
            {
                CascadingDropDownNameValue oCdd = new CascadingDropDownNameValue();
                oCdd.name = item.Nombre;
                oCdd.value = item.IdCuadrilla.ToString();
                values.Add(oCdd);
            }

            return values;

        }


        public DataTable SolicitudCuadrillaPosibleGet(int nroSolicitud, int idTipoServicio)
        {
            CuadrillaImpl oCuadrillaImpl = new CuadrillaImpl();
            return oCuadrillaImpl.SolicitudCuadrillaPosibleGet(nroSolicitud, idTipoServicio);
        }
    }
}
