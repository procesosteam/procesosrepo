
using System;
using System.Data;
using ProcesoPP.SqlServerImpl;
using ProcesoPP.Model;
using System.Collections.Generic;
using System.Web.UI.WebControls;

namespace ProcesoPP.Business
{
	public class TipoServicioBus
	{
		#region Business

		public int TipoServicioAdd(TipoServicio tiposervicio)
		{
			TipoServicioImpl oTipoServicioImpl = new TipoServicioImpl();
			return oTipoServicioImpl.TipoServicioAdd(tiposervicio);
		}

		public bool TipoServicioUpdate(TipoServicio tiposervicio)
		{
			TipoServicioImpl oTipoServicioImpl = new TipoServicioImpl();
			return oTipoServicioImpl.TipoServicioUpdate(tiposervicio);
		}

		public bool TipoServicioDelete(int IdTipoServicio)
		{
			TipoServicioImpl oTipoServicioImpl = new TipoServicioImpl();
			return oTipoServicioImpl.TipoServicioDelete(IdTipoServicio);
		}

		public TipoServicio TipoServicioGetById(int IdTipoServicio)
		{
			TipoServicioImpl oTipoServicioImpl = new TipoServicioImpl();
			return oTipoServicioImpl.TipoServicioGetById(IdTipoServicio);
		}

		public List<TipoServicio> TipoServicioGetAll()
		{
			TipoServicioImpl oTipoServicioImpl = new TipoServicioImpl();
			return oTipoServicioImpl.TipoServicioGetAll();
		}

		#endregion
        
        public void CargarCombo(ref System.Web.UI.WebControls.DropDownList ddlTipoServicio, string p)
        {
            ddlTipoServicio.DataSource = TipoServicioGetAll();
            ddlTipoServicio.DataTextField = "Descripcion";
            ddlTipoServicio.DataValueField = "idTipoServicio";
            ddlTipoServicio.DataBind();
            ddlTipoServicio.Items.Insert(0, new ListItem(p, "0"));
        }

        public List<TipoServicio> TipoServicioGetByFilter(string descripcion)
        {
            TipoServicioImpl oTipoServicioImpl = new TipoServicioImpl();
            return oTipoServicioImpl.TipoServicioGetByFilter(descripcion);
        }

        public DataTable CodTangoGetByFilter(string detalle)
        {
            TipoServicioImpl oTipoServicioImpl = new TipoServicioImpl();
            return oTipoServicioImpl.CodTangoGetByFilter(detalle);
        }

        public DataSet TipoServicioGetByAcuerdoCliente(string idEmpresa, string idSector)
        {
            TipoServicioImpl oTipoServicioImpl = new TipoServicioImpl();
            return oTipoServicioImpl.TipoServicioGetByAcuerdoCliente(idEmpresa, idSector);
        }
    }
}
