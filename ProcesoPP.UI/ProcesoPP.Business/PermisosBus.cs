﻿
using System;
using System.Data;
using ProcesoPP.SqlServerImpl;
using ProcesoPP.Model;
using System.Collections.Generic;
using System.Web.UI.WebControls;

namespace ProcesoPP.Business
{
    public class PermisosBus
    {
        public int PermisosAdd(Permisos permiso)
        {
            PermisosImpl oPermisosImpl = new PermisosImpl();
            return oPermisosImpl.PermisosAdd(permiso);
        }

        public void CargarModulos(ref System.Web.UI.WebControls.DropDownList ddlModulo, string p)
        {
            PermisosImpl oPermisosImpl = new PermisosImpl();
            ddlModulo.DataSource = oPermisosImpl.ModulosGetAll();
            ddlModulo.DataTextField = "Descripcion";
            ddlModulo.DataValueField = "idModulo";
            ddlModulo.DataBind();
            ddlModulo.Items.Insert(0, new ListItem(p, "0"));
        }

        public void CargarTipoUsuarios(ref System.Web.UI.WebControls.DropDownList ddlTipoUsuario, string p)
        {
            PermisosImpl oPermisosImpl = new PermisosImpl();
            ddlTipoUsuario.DataSource = oPermisosImpl.TipoUsuariosGetAll();
            ddlTipoUsuario.DataTextField = "Descripcion";
            ddlTipoUsuario.DataValueField = "idTipoUsuario";
            ddlTipoUsuario.DataBind();
            ddlTipoUsuario.Items.Insert(0, new ListItem(p, "0"));
        }

        public List<Permisos> PermisosGetAll()
        {
            PermisosImpl oPermisosImpl = new PermisosImpl();
            return oPermisosImpl.PermisosGetAll();
        }

        public bool PermisosDelete(int idPermisos)
        {
            PermisosImpl oPermisosImpl = new PermisosImpl();
            return oPermisosImpl.PermisosDelete(idPermisos);
        }

        public bool verificarPermiso(Permisos oPermisos)
        {
           return  PermisosGetAll().Exists(p => p.oModulo.idModulo == oPermisos.oModulo.idModulo &&
                                                p.oTipoUsuario.idTipoUsuario == oPermisos.oTipoUsuario.idTipoUsuario);
            
        }



        public List<Permisos> PermisosGetIdTipoUsuario(int idTipoUsuario)
        {
            PermisosImpl oPermisosImpl = new PermisosImpl();
            return oPermisosImpl.PermisosGetIdTipoUsuario(idTipoUsuario);
        }
    }
}