
using System;
using System.Data;
using ProcesoPP.SqlServerImpl;
using ProcesoPP.Model;
using System.Collections.Generic;

namespace ProcesoPP.Business
{
	public class PersonalConduccionBus
	{
		#region Business

		public int PersonalConduccionAdd(PersonalConduccion personalconduccion)
		{
			PersonalConduccionImpl oPersonalConduccionImpl = new PersonalConduccionImpl();
			return oPersonalConduccionImpl.PersonalConduccionAdd(personalconduccion);
		}

		public bool PersonalConduccionUpdate(PersonalConduccion personalconduccion)
		{
			PersonalConduccionImpl oPersonalConduccionImpl = new PersonalConduccionImpl();
			return oPersonalConduccionImpl.PersonalConduccionUpdate(personalconduccion);
		}

		public bool PersonalConduccionDelete(int IdPersonalConduccion)
		{
			PersonalConduccionImpl oPersonalConduccionImpl = new PersonalConduccionImpl();
			return oPersonalConduccionImpl.PersonalConduccionDelete(IdPersonalConduccion);
		}

		public PersonalConduccion PersonalConduccionGetById(int IdPersonalConduccion)
		{
			PersonalConduccionImpl oPersonalConduccionImpl = new PersonalConduccionImpl();
			return oPersonalConduccionImpl.PersonalConduccionGetById(IdPersonalConduccion);
		}

		public List<PersonalConduccion> PersonalConduccionGetAll()
		{
			PersonalConduccionImpl oPersonalConduccionImpl = new PersonalConduccionImpl();
			return oPersonalConduccionImpl.PersonalConduccionGetAll();
		}

		#endregion
	}
}
