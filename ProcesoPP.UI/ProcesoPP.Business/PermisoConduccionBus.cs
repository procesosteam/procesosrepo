using System;
using System.Data;
using ProcesoPP.SqlServerImpl;
using ProcesoPP.Model;
using ProcesoPP.Implement;
using System.Collections.Generic;

namespace ProcesoPP.Business
{
	public class PermisoConduccionBus
	{
		#region Business

		public int PermisoConduccionAdd(PermisoConduccion permisoconduccion)
		{
			PermisoConduccionImpl oPermisoConduccionImpl = new PermisoConduccionImpl();
			return oPermisoConduccionImpl.PermisoConduccionAdd(permisoconduccion);
		}

		public bool PermisoConduccionUpdate(PermisoConduccion permisoconduccion)
		{
			PermisoConduccionImpl oPermisoConduccionImpl = new PermisoConduccionImpl();
			return oPermisoConduccionImpl.PermisoConduccionUpdate(permisoconduccion);
		}

		public bool PermisoConduccionDelete(PermisoConduccion oPermisoConduccion)
		{
			PermisoConduccionImpl oPermisoConduccionImpl = new PermisoConduccionImpl();
            return oPermisoConduccionImpl.PermisoConduccionDelete(oPermisoConduccion);
		}

		public PermisoConduccion PermisoConduccionGetById(int id)
		{
			PermisoConduccionImpl oPermisoConduccionImpl = new PermisoConduccionImpl();
			return oPermisoConduccionImpl.PermisoConduccionGetById(id);
		}

		public List<PermisoConduccion> PermisoConduccionGetAll()
		{
			PermisoConduccionImpl oPermisoConduccionImpl = new PermisoConduccionImpl();
			return oPermisoConduccionImpl.PermisoConduccionGetAll();
		}

        public List<PermisoConduccion> PermisoConduccionGetByLegajo(int idLegajo)
        {
            PermisoConduccionImpl oPermisoConduccionImpl = new PermisoConduccionImpl();
            return oPermisoConduccionImpl.PermisoConduccionGetByLegajo(idLegajo);
        }

		#endregion
	}
}
