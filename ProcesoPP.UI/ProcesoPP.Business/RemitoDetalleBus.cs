
using System;
using System.Data;
using ProcesoPP.SqlServerImpl;
using ProcesoPP.Model;
using System.Collections.Generic;

namespace ProcesoPP.Business
{
	public class RemitoDetalleBus
	{
		#region Business

		public int RemitoDetalleAdd(RemitoDetalle remitodetalle)
		{
			RemitoDetalleImpl oRemitoDetalleImpl = new RemitoDetalleImpl();
			return oRemitoDetalleImpl.RemitoDetalleAdd(remitodetalle);
		}

		public bool RemitoDetalleUpdate(RemitoDetalle remitodetalle)
		{
			RemitoDetalleImpl oRemitoDetalleImpl = new RemitoDetalleImpl();
			return oRemitoDetalleImpl.RemitoDetalleUpdate(remitodetalle);
		}

		public bool RemitoDetalleDelete(int IdRemitoDetalle)
		{
			RemitoDetalleImpl oRemitoDetalleImpl = new RemitoDetalleImpl();
			return oRemitoDetalleImpl.RemitoDetalleDelete(IdRemitoDetalle);
		}

		public RemitoDetalle RemitoDetalleGetById(int IdRemitoDetalle)
		{
			RemitoDetalleImpl oRemitoDetalleImpl = new RemitoDetalleImpl();
			return oRemitoDetalleImpl.RemitoDetalleGetById(IdRemitoDetalle);
		}

		public List<RemitoDetalle> RemitoDetalleGetAll()
		{
			RemitoDetalleImpl oRemitoDetalleImpl = new RemitoDetalleImpl();
			return oRemitoDetalleImpl.RemitoDetalleGetAll();
		}

		#endregion
	}
}
