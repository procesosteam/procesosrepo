

using System;
using System.Data;
using ProcesoPP.SqlServerImpl;
using ProcesoPP.Model;
using System.Collections.Generic;

namespace ProcesoPP.Business
{
	public class VehiculoIdentificacionBus
	{
		#region Business

		public int VehiculoIdentificacionAdd(VehiculoIdentificacion vehiculoidentificacion)
		{
			VehiculoIdentificacionImpl oVehiculoIdentificacionImpl = new VehiculoIdentificacionImpl();
			return oVehiculoIdentificacionImpl.VehiculoIdentificacionAdd(vehiculoidentificacion);
		}

		public bool VehiculoIdentificacionUpdate(VehiculoIdentificacion vehiculoidentificacion)
		{
			VehiculoIdentificacionImpl oVehiculoIdentificacionImpl = new VehiculoIdentificacionImpl();
			return oVehiculoIdentificacionImpl.VehiculoIdentificacionUpdate(vehiculoidentificacion);
		}

		public bool VehiculoIdentificacionDelete(int IdIdentificacion)
		{
			VehiculoIdentificacionImpl oVehiculoIdentificacionImpl = new VehiculoIdentificacionImpl();
			return oVehiculoIdentificacionImpl.VehiculoIdentificacionDelete(IdIdentificacion);
		}

		public VehiculoIdentificacion VehiculoIdentificacionGetById(int IdIdentificacion)
		{
			VehiculoIdentificacionImpl oVehiculoIdentificacionImpl = new VehiculoIdentificacionImpl();
			return oVehiculoIdentificacionImpl.VehiculoIdentificacionGetById(IdIdentificacion);
		}

        public VehiculoIdentificacion VehiculoIdentificacionGetByIdVehiculo(int IdVehiculo)
        {
            VehiculoIdentificacionImpl oVehiculoIdentificacionImpl = new VehiculoIdentificacionImpl();
            return oVehiculoIdentificacionImpl.VehiculoIdentificacionGetByIdVehiculo(IdVehiculo);
        }

		public List<VehiculoIdentificacion> VehiculoIdentificacionGetAll()
		{
			VehiculoIdentificacionImpl oVehiculoIdentificacionImpl = new VehiculoIdentificacionImpl();
			return oVehiculoIdentificacionImpl.VehiculoIdentificacionGetAll();
		}

		public List<VehiculoIdentificacion> VehiculoIdentificacionGetAllBaja()
		{
			VehiculoIdentificacionImpl oVehiculoIdentificacionImpl = new VehiculoIdentificacionImpl();
			return oVehiculoIdentificacionImpl.VehiculoIdentificacionGetAllBaja();
		}

		#endregion
	}
}
