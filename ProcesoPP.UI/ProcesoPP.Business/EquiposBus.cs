
using System;
using System.Data;
using ProcesoPP.SqlServerImpl;
using ProcesoPP.Model;
using System.Collections.Generic;

namespace ProcesoPP.Business
{
	public class EquiposBus
	{
		#region Business

		public int EquiposAdd(Equipos equipos)
		{
			EquiposImpl oEquiposImpl = new EquiposImpl();
			return oEquiposImpl.EquiposAdd(equipos);
		}

		public bool EquiposUpdate(Equipos equipos)
		{
			EquiposImpl oEquiposImpl = new EquiposImpl();
			return oEquiposImpl.EquiposUpdate(equipos);
		}

		public bool EquiposDelete(int IdEquipo)
		{
			EquiposImpl oEquiposImpl = new EquiposImpl();
			return oEquiposImpl.EquiposDelete(IdEquipo);
		}

		public Equipos EquiposGetById(int IdEquipo)
		{
			EquiposImpl oEquiposImpl = new EquiposImpl();
			return oEquiposImpl.EquiposGetById(IdEquipo);
		}

		public List<Equipos> EquiposGetAll()
		{
			EquiposImpl oEquiposImpl = new EquiposImpl();
			return oEquiposImpl.EquiposGetAll();
		}

		#endregion

        public DataTable EquiposGetByIdCliente(int idCliente)
        {
            EquiposImpl oEquiposImpl = new EquiposImpl();
            return oEquiposImpl.EquiposGetByIdCliente(idCliente);
        }

        public DataTable EquiposGetByClienteEspecificacion(int idEmpresa, List<EspecificacionTecnica> oListET)
        {
            EquiposImpl oEquiposImpl = new EquiposImpl();
            return oEquiposImpl.EquiposGetByClienteEspecificacion(idEmpresa, oListET);
        }
    }
}
