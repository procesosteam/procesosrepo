﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ProcesoPP.Model;
using ProcesoPP.SqlServerImpl;

namespace ProcesoPP.Business
{
    public class KilometrosBus
    {
        public int KilometrosAdd(Kilometros oKilometros)
        {
            KilometrosImpl oKilometrosImpl = new KilometrosImpl();
            return oKilometrosImpl.KilometrosAdd(oKilometros);
        }

        public List<Kilometros> KilometrosGetAll()
        {
            KilometrosImpl oKilometrosImpl = new KilometrosImpl();
            return oKilometrosImpl.KilometrosGetAll();
        }

        public List<Kilometros> KilometrosGetAll(string idTipoServicio)
        {
            KilometrosImpl oKilometrosImpl = new KilometrosImpl();
            return oKilometrosImpl.KilometrosGetAll(idTipoServicio);
        }


        public int KilometrosDelete(int idKms)
        {
            KilometrosImpl oKilometrosImpl = new KilometrosImpl();
            return oKilometrosImpl.KilometrosDelete(idKms);
        }

        public Kilometros KilometrosGetBy(Kilometros oKms)
        {
            KilometrosImpl oKilometrosImpl = new KilometrosImpl();
            return oKilometrosImpl.KilometrosGetBy(oKms);
        }

        public List<Kilometros> KilometrosGetByFilter(string idDesde, string idHasta)
        {
            KilometrosImpl oKilometrosImpl = new KilometrosImpl();
            return oKilometrosImpl.KilometrosGetByFilter(idDesde, idHasta);
        }
    }
}
