
using System;
using System.Data;
using ProcesoPP.SqlServerImpl;
using ProcesoPP.Model;
using System.Collections.Generic;

namespace ProcesoPP.Business
{
	public class PersonalFilesBus
	{
		#region Business

		public int PersonalFilesAdd(PersonalFiles personalfiles)
		{
			PersonalFilesImpl oPersonalFilesImpl = new PersonalFilesImpl();
			return oPersonalFilesImpl.PersonalFilesAdd(personalfiles);
		}

		public bool PersonalFilesUpdate(PersonalFiles personalfiles)
		{
			PersonalFilesImpl oPersonalFilesImpl = new PersonalFilesImpl();
			return oPersonalFilesImpl.PersonalFilesUpdate(personalfiles);
		}

		public bool PersonalFilesDelete(int IdPersonalFiles)
		{
			PersonalFilesImpl oPersonalFilesImpl = new PersonalFilesImpl();
			return oPersonalFilesImpl.PersonalFilesDelete(IdPersonalFiles);
		}

		public PersonalFiles PersonalFilesGetById(int IdPersonalFiles)
		{
			PersonalFilesImpl oPersonalFilesImpl = new PersonalFilesImpl();
			return oPersonalFilesImpl.PersonalFilesGetById(IdPersonalFiles);
		}

		public List<PersonalFiles> PersonalFilesGetAll()
		{
			PersonalFilesImpl oPersonalFilesImpl = new PersonalFilesImpl();
			return oPersonalFilesImpl.PersonalFilesGetAll();
		}
        public List<PersonalFiles> getPersonalFilesByIdPersonal(int idPersonal)
        {
            PersonalFilesImpl oPersonalFilesImpl = new PersonalFilesImpl();
            return oPersonalFilesImpl.getPersonalFilesByIdPersonal(idPersonal);
        }
        public List<PersonalFiles> PersonalFilesGetByFilter(int idPersonal, string nombre, int longuitud)
        {
            PersonalFilesImpl oPersonalFilesImpl = new PersonalFilesImpl();
            return oPersonalFilesImpl.PersonalFilesGetByFilter(idPersonal, nombre, longuitud);
        }
		#endregion
	}
}
