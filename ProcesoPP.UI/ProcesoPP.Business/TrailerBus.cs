

using System;
using System.Data;
using ProcesoPP.SqlServerImpl;
using ProcesoPP.Model;
using System.Collections.Generic;
using System.Web.UI.WebControls;

namespace ProcesoPP.Business
{
	public class TrailerBus
	{
		#region Business

        public DataTable TrailerGetDisponibles(List<EspecificacionTecnica> oEspecificaciones, int idTipoServicio)
        {
            TrailerImpl oTrailerImpl = new TrailerImpl();
            return oTrailerImpl.TrailerGetDisponibles(oEspecificaciones, idTipoServicio);
        }

		#endregion

        public int TrailerGetSolicitudAnterior(int nroSolicitud)
        {
         TrailerImpl oTrailerImpl = new TrailerImpl();
         return oTrailerImpl.TrailerGetSolicitudAnterior(nroSolicitud);
        }

        public DataTable TrailerGetNoDisponibles(List<EspecificacionTecnica> oListEsp, int nroSolicitud)
        {

            TrailerImpl oTrailerImpl = new TrailerImpl();
            return oTrailerImpl.TrailerGetNoDisponibles(oListEsp, nroSolicitud);
        }

        public List<Vehiculo> SolicitudTrailerPosibleGet(int nroSolicitud, int idTipoServicio)
        {
            TrailerImpl oTrailerImpl = new TrailerImpl();
            return oTrailerImpl.SolicitudTrailerPosibleGet(nroSolicitud, idTipoServicio);

        }
    }
}
