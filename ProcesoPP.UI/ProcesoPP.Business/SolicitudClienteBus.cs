

using System;
using System.Data;
using ProcesoPP.SqlServerImpl;
using ProcesoPP.Model;
using System.Collections.Generic;

namespace ProcesoPP.Business
{
	public class SolicitudClienteBus
	{
		#region Business

		public int SolicitudClienteAdd(SolicitudCliente solicitudcliente)
		{
			SolicitudClienteImpl oSolicitudClienteImpl = new SolicitudClienteImpl();
			return oSolicitudClienteImpl.SolicitudClienteAdd(solicitudcliente);
		}

		public bool SolicitudClienteUpdate(SolicitudCliente solicitudcliente)
		{
			SolicitudClienteImpl oSolicitudClienteImpl = new SolicitudClienteImpl();
			return oSolicitudClienteImpl.SolicitudClienteUpdate(solicitudcliente);
		}

		public bool SolicitudClienteDelete(int IdSolicitud)
		{
			SolicitudClienteImpl oSolicitudClienteImpl = new SolicitudClienteImpl();
			return oSolicitudClienteImpl.SolicitudClienteDelete(IdSolicitud);
		}

		public SolicitudCliente SolicitudClienteGetById(int IdSolicitud)
		{
			SolicitudClienteImpl oSolicitudClienteImpl = new SolicitudClienteImpl();
			return oSolicitudClienteImpl.SolicitudClienteGetById(IdSolicitud);
		}

		public List<SolicitudCliente> SolicitudClienteGetAll()
		{
			SolicitudClienteImpl oSolicitudClienteImpl = new SolicitudClienteImpl();
			return oSolicitudClienteImpl.SolicitudClienteGetAll();
		}

		#endregion

        public int SolicitudClienteSugerirNro()
        {
            SolicitudClienteImpl oSolicitudClienteImpl = new SolicitudClienteImpl();
            return oSolicitudClienteImpl.SolicitudClienteSugerirNro();
        }

        public DataTable SolicitudClienteGetByFilter(string NroSolicitud, string idEmpresa, string fechaDesde, string feschaHasta, string idTrailer, string idCuadrilla)
        {
            SolicitudClienteImpl oSolicitudClienteImpl = new SolicitudClienteImpl();
            return oSolicitudClienteImpl.SolicitudClienteGetByFilter(NroSolicitud, idEmpresa,fechaDesde,feschaHasta, idTrailer, idCuadrilla);
        }

        public DataTable SolicitudRegresoGetByFilter(string NroSolicitud, string idEmpresa, string fechaDesde, string feschaHasta, string idTrailer)
        {
            SolicitudClienteImpl oSolicitudClienteImpl = new SolicitudClienteImpl();
            return oSolicitudClienteImpl.SolicitudRegresoGetByFilter(NroSolicitud, idEmpresa, fechaDesde, feschaHasta, idTrailer);
        }

        public DataTable SolicitudClienteRegresoGetByFilter(string NroSolicitud, string idEmpresa, string fechaDesde, string feschaHasta)
        {
            SolicitudClienteImpl oSolicitudClienteImpl = new SolicitudClienteImpl();
            return oSolicitudClienteImpl.SolicitudClienteRegresoGetByFilter(NroSolicitud, idEmpresa, fechaDesde, feschaHasta);
        }

        public DataTable ReporteMovimiento(string fechaDesde, string fechaHasta, string idEmpresa, string idSector, string idCondicion, string idTrailer, string idOrigen, string idDestino, string idChofer, string idVehiculo, string nroSol, string TipoServicio, string remito)
        {
            
            SolicitudClienteImpl oSolicitudClienteImpl = new SolicitudClienteImpl();
            return oSolicitudClienteImpl.ReporteMovimiento(fechaDesde, fechaHasta, idEmpresa, idSector, idCondicion, idTrailer, idOrigen, idDestino, idChofer, idVehiculo, nroSol, TipoServicio, remito);
        }

        public object SolicitudHistorialGetByFilter(string nroSolicitud, string idEmpresa, string fechaDesde, string fechaHasta, string idTrailer, string nroParte)
        {
            SolicitudClienteImpl oSolicitudClienteImpl = new SolicitudClienteImpl();
            return oSolicitudClienteImpl.SolicitudHistorialGetByFilter(nroSolicitud, idEmpresa, fechaDesde, fechaHasta, idTrailer, nroParte);
        }

        public DataTable ReportePosicionActual(string idEmpresa, string idSector, string idCondicion, string idEquipo, string idDestino, string idTipoServicio)
        {
            SolicitudClienteImpl oSolicitudClienteImpl = new SolicitudClienteImpl();
            return oSolicitudClienteImpl.ReportePosicionActual(idEmpresa, idSector, idCondicion, idEquipo, idDestino, idTipoServicio);
        }

        public DataTable SolicitudClienteSinOTI()
        {
            SolicitudClienteImpl oSolicitudClienteImpl = new SolicitudClienteImpl();
            return oSolicitudClienteImpl.SolicitudClienteSinOTI();
        }

        public DataTable SolicitudClienteEliminadas(string nroSolicitud, string idEmpresa, string fechaDesde, string fechaHasta, string idTrailer)
        {
         SolicitudClienteImpl oSolicitudClienteImpl = new SolicitudClienteImpl();
         return oSolicitudClienteImpl.SolicitudClienteEliminadas(nroSolicitud, idEmpresa, fechaDesde, fechaHasta, idTrailer);
        }

        public int SolicitudClienteGetFechaRegreso(int nroSolicitud, int idoSolicitud, DateTime fechaSC)
        {
            SolicitudClienteImpl oSolicitudClienteImpl = new SolicitudClienteImpl();
            return oSolicitudClienteImpl.SolicitudClienteGetFechaRegreso(nroSolicitud, idoSolicitud, fechaSC);
        }

        public DataTable ReporteAlertaMovimiento()
        {
            SolicitudClienteImpl oSolicitudClienteImpl = new SolicitudClienteImpl();
            return oSolicitudClienteImpl.ReporteAlertaMovimiento();
        }

        public List<SolicitudCliente> SolicitudClienteGetByNro(int nroSolicitud)
        {
            SolicitudClienteImpl oSolicitudClienteImpl = new SolicitudClienteImpl();
            return oSolicitudClienteImpl.SolicitudClienteGetByNro(nroSolicitud);
        }


        public TipoServicio SolicitudClienteGetByIdMovimiento(int idMovimiento)
        {
            SolicitudClienteImpl oSolicitudClienteImpl = new SolicitudClienteImpl();
            return oSolicitudClienteImpl.SolicitudClienteGetByIdMovimiento(idMovimiento);
        }

        public DataTable ReporteAlquileresMensuales(string fechaDesde, string fechaHasta, string idEmpresa, string idSector,string idTrailer,string nroSol, string TipoServicio, string remito)
        {
            SolicitudClienteImpl oSolicitudClienteImpl = new SolicitudClienteImpl();
            return oSolicitudClienteImpl.ReporteAlquileresMensuales(fechaDesde, fechaHasta, idEmpresa, idSector, idTrailer, nroSol, TipoServicio, remito);
        }
    }
}
