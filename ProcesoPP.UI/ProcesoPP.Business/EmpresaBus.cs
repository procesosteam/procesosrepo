
using System;
using System.Data;
using ProcesoPP.SqlServerImpl;
using ProcesoPP.Model;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using AjaxControlToolkit;

namespace ProcesoPP.Business
{
	public class EmpresaBus
	{
		#region Business

		public int EmpresaAdd(Empresa empresa)
		{
			EmpresaImpl oEmpresaImpl = new EmpresaImpl();
			return oEmpresaImpl.EmpresaAdd(empresa);
		}

		public bool EmpresaUpdate(Empresa empresa)
		{
			EmpresaImpl oEmpresaImpl = new EmpresaImpl();
			return oEmpresaImpl.EmpresaUpdate(empresa);
		}

		public bool EmpresaDelete(int IdEmpresa)
		{
			EmpresaImpl oEmpresaImpl = new EmpresaImpl();
			return oEmpresaImpl.EmpresaDelete(IdEmpresa);
		}

		public Empresa EmpresaGetById(int IdEmpresa)
		{
			EmpresaImpl oEmpresaImpl = new EmpresaImpl();
			return oEmpresaImpl.EmpresaGetById(IdEmpresa);
		}

		public List<Empresa> EmpresaGetAll()
		{
			EmpresaImpl oEmpresaImpl = new EmpresaImpl();
			return oEmpresaImpl.EmpresaGetAll();
		}

        public List<Empresa> EmpresaGetInternas()
        {
            EmpresaImpl oEmpresaImpl = new EmpresaImpl();
            return oEmpresaImpl.GetInternas();
        }

        public void CargarEmpresa(ref System.Web.UI.WebControls.DropDownList ddlEmpresa, string primerItem)
        {
            ddlEmpresa.DataSource = EmpresaGetAll();
            ddlEmpresa.DataTextField = "Descripcion";
            ddlEmpresa.DataValueField = "idEmpresa";
            ddlEmpresa.DataBind();
            ddlEmpresa.Items.Insert(0, new ListItem(primerItem, "0"));
        }

        public void CargarEmpresaInterna(ref System.Web.UI.WebControls.DropDownList ddlEmpresa, string primerItem)
        {
            ddlEmpresa.DataSource = EmpresaGetInternas();
            ddlEmpresa.DataTextField = "Descripcion";
            ddlEmpresa.DataValueField = "idEmpresa";
            ddlEmpresa.DataBind();
            ddlEmpresa.Items.Insert(0, new ListItem(primerItem, "0"));
        }

		#endregion

        public List<CascadingDropDownNameValue> GetDataEmpresa()
        {
            List<Empresa> oListEmpresa = EmpresaGetAll();
            List<CascadingDropDownNameValue> values = new List<CascadingDropDownNameValue>();
            foreach (Empresa item in oListEmpresa)
            {
                CascadingDropDownNameValue oCdd = new CascadingDropDownNameValue();
                oCdd.name = item.Descripcion;
                oCdd.value = item.IdEmpresa.ToString();
                values.Add(oCdd);
            }

            return values;
        }

        public List<CascadingDropDownNameValue> GetDataEmpresaInteras()
        {
            List<Empresa> oListEmpresa = EmpresaGetInternas();
            List<CascadingDropDownNameValue> values = new List<CascadingDropDownNameValue>();
            foreach (Empresa item in oListEmpresa)
            {
                CascadingDropDownNameValue oCdd = new CascadingDropDownNameValue();
                oCdd.name = item.Descripcion;
                oCdd.value = item.IdEmpresa.ToString();
                values.Add(oCdd);
            }

            return values;
        }

        public DataTable EmpresaGetByFilter(string nombre)
        {
            EmpresaImpl oEmpresaImpl = new EmpresaImpl();
            return oEmpresaImpl.EmpresaGetByFilter(nombre);
        }

    }
}
