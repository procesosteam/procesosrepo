
using System;
using System.Data;
using ProcesoPP.SqlServerImpl;
using ProcesoPP.Model;
using System.Collections.Generic;
using System.Web.UI.WebControls;

namespace ProcesoPP.Business
{
	public class UsuarioBus
	{
		#region Business

		public int UsuarioAdd(Usuario usuario)
		{
			UsuarioImpl oUsuarioImpl = new UsuarioImpl();
			return oUsuarioImpl.UsuarioAdd(usuario);
		}

		public bool UsuarioUpdate(Usuario usuario)
		{
			UsuarioImpl oUsuarioImpl = new UsuarioImpl();
			return oUsuarioImpl.UsuarioUpdate(usuario);
		}

		public bool UsuarioDelete(int IdUsuario)
		{
			UsuarioImpl oUsuarioImpl = new UsuarioImpl();
			return oUsuarioImpl.UsuarioDelete(IdUsuario);
		}

		public Usuario UsuarioGetById(int IdUsuario)
		{
			UsuarioImpl oUsuarioImpl = new UsuarioImpl();
			return oUsuarioImpl.UsuarioGetById(IdUsuario);
		}

		public List<Usuario> UsuarioGetAll()
		{
			UsuarioImpl oUsuarioImpl = new UsuarioImpl();
			return oUsuarioImpl.UsuarioGetAll();
		}

		#endregion

        public bool VerificarUserName(string userName)
        {
            return UsuarioGetAll().Exists(u => u.UserName == userName);
        }

        public Usuario UsuarioGetByUserName(string userName)
        {
            Usuario oUser = UsuarioGetAll().Find(u => u.UserName == userName);
            return oUser;
        }
    }
}
