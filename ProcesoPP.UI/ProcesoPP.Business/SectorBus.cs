
using System;
using System.Data;
using ProcesoPP.SqlServerImpl;
using ProcesoPP.Model;
using System.Collections.Generic;
using AjaxControlToolkit;

namespace ProcesoPP.Business
{
	public class SectorBus
	{
		#region Business

		public int SectorAdd(Sector sector)
		{
			SectorImpl oSectorImpl = new SectorImpl();
			return oSectorImpl.SectorAdd(sector);
		}

		public bool SectorUpdate(Sector sector)
		{
			SectorImpl oSectorImpl = new SectorImpl();
			return oSectorImpl.SectorUpdate(sector);
		}

		public bool SectorDelete(int IdSector)
		{
			SectorImpl oSectorImpl = new SectorImpl();
			return oSectorImpl.SectorDelete(IdSector);
		}

		public Sector SectorGetById(int IdSector)
		{
			SectorImpl oSectorImpl = new SectorImpl();
			return oSectorImpl.SectorGetById(IdSector);
		}

		public List<Sector> SectorGetAll()
		{
			SectorImpl oSectorImpl = new SectorImpl();
			return oSectorImpl.SectorGetAll();
		}

        public void CargarSector(ref System.Web.UI.WebControls.DropDownList ddlSector)
        {
            ddlSector.DataSource = SectorGetAll();
            ddlSector.DataTextField = "Descripcion";
            ddlSector.DataValueField = "idSector";
            ddlSector.DataBind();
            ddlSector.Items.Insert(0, new System.Web.UI.WebControls.ListItem("Seleccione Sector", "0"));
        }

        public void CargarSectorPorEmpresa(ref System.Web.UI.WebControls.DropDownList ddlSector, int idEmpresa)
        {
            ddlSector.DataSource = SectorGetAllByIdEmpresa(idEmpresa);
            ddlSector.DataTextField = "Descripcion";
            ddlSector.DataValueField = "idSector";
            ddlSector.DataBind();
            ddlSector.Items.Insert(0, new System.Web.UI.WebControls.ListItem("N/A", "0"));
        }

        public List<Sector> SectorGetAllByIdEmpresa(int idEmpresa)
        {
            SectorImpl oSectorImpl = new SectorImpl();
            List<Sector> dsSectores = oSectorImpl.SectorGetAllByIdEmpresa(idEmpresa);          
            return dsSectores;
        }

        #endregion


        public List<CascadingDropDownNameValue> GetDataSector(int idEmpresa)
        {
         
            List<Sector> oListSector = SectorGetAllByIdEmpresa(idEmpresa);
            List<CascadingDropDownNameValue> values = new List<CascadingDropDownNameValue>();
            if (idEmpresa > 0)
            {
                foreach (Sector item in oListSector)
                {
                    CascadingDropDownNameValue oCdd = new CascadingDropDownNameValue();
                    oCdd.name = item.Descripcion;
                    oCdd.value = item.IdSector.ToString();
                    values.Add(oCdd);
                }
            }
            return values;
        
        }

        public List<Sector> SectorGetAllByIdEmpresaInterna()
        {
            SectorImpl oSectorImpl = new SectorImpl();
            return oSectorImpl.SectorGetAllByIdEmpresaInterna();
        }
    }
}
