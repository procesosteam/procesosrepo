

using System;
using System.Data;
using ProcesoPP.SqlServerImpl;
using ProcesoPP.Model;
using System.Collections.Generic;

namespace ProcesoPP.Business
{
	public class VehiculoFilesBus
	{
		#region Business

		public int VehiculoFilesAdd(VehiculoFiles vehiculofiles)
		{
			VehiculoFilesImpl oVehiculoFilesImpl = new VehiculoFilesImpl();
			return oVehiculoFilesImpl.VehiculoFilesAdd(vehiculofiles);
		}

		public bool VehiculoFilesUpdate(VehiculoFiles vehiculofiles)
		{
			VehiculoFilesImpl oVehiculoFilesImpl = new VehiculoFilesImpl();
			return oVehiculoFilesImpl.VehiculoFilesUpdate(vehiculofiles);
		}

		public bool VehiculoFilesDelete(int IdVehiculoFiles)
		{
			VehiculoFilesImpl oVehiculoFilesImpl = new VehiculoFilesImpl();
			return oVehiculoFilesImpl.VehiculoFilesDelete(IdVehiculoFiles);
		}

		public VehiculoFiles VehiculoFilesGetById(int IdVehiculoFiles)
		{
			VehiculoFilesImpl oVehiculoFilesImpl = new VehiculoFilesImpl();
			return oVehiculoFilesImpl.VehiculoFilesGetById(IdVehiculoFiles);
		}

        public List<VehiculoFiles> VehiculoFilesGetByIdVehiculo(int IdVehiculo)
        {
            VehiculoFilesImpl oVehiculoFilesImpl = new VehiculoFilesImpl();
            return oVehiculoFilesImpl.VehiculoFilesGetByIdVehiculo(IdVehiculo);
        }

		public List<VehiculoFiles> VehiculoFilesGetAll()
		{
			VehiculoFilesImpl oVehiculoFilesImpl = new VehiculoFilesImpl();
			return oVehiculoFilesImpl.VehiculoFilesGetAll();
		}

		#endregion

        
    }
}
