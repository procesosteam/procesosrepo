

using System;
using System.Data;
using ProcesoPP.SqlServerImpl;
using ProcesoPP.Model;
using System.Collections.Generic;

namespace ProcesoPP.Business
{
	public class VerificaParteBus
	{
		#region Business

		public int VerificaParteAdd(VerificaParte verificaparte, bool trans)
		{
			VerificaParteImpl oVerificaParteImpl = new VerificaParteImpl();
			return oVerificaParteImpl.VerificaParteAdd(verificaparte, trans);
		}

        public bool VerificaParteUpdate(VerificaParte verificaparte, bool trans)
		{
			VerificaParteImpl oVerificaParteImpl = new VerificaParteImpl();
			return oVerificaParteImpl.VerificaParteUpdate(verificaparte, trans);
		}

		public bool VerificaParteDelete(int IdVerifica, int IdParte)
		{
			VerificaParteImpl oVerificaParteImpl = new VerificaParteImpl();
			return oVerificaParteImpl.VerificaParteDelete(IdVerifica, IdParte);
		}

		public VerificaParte VerificaParteGetById(int IdVerifica, int IdParte, bool trans)
		{
			VerificaParteImpl oVerificaParteImpl = new VerificaParteImpl();
			return oVerificaParteImpl.VerificaParteGetById(IdVerifica, IdParte, trans);
		}

		public List<VerificaParte> VerificaParteGetAll()
		{
			VerificaParteImpl oVerificaParteImpl = new VerificaParteImpl();
			return oVerificaParteImpl.VerificaParteGetAll();
		}

		#endregion
	}
}
