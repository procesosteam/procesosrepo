﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ProcesoPP.SqlServerImpl;
using ProcesoPP.SqlServerImpl;
using ProcesoPP.Model;

namespace ProcesoPP.Business
{
    public class LugarBus
    {
        #region Business

        public int LugarAdd(Lugar Lugar)
        {
            LugarImpl oLugarImpl = new LugarImpl();
            return oLugarImpl.LugarAdd(Lugar);
        }

        public bool LugarUpdate(Lugar Lugar)
        {
            LugarImpl oLugarImpl = new LugarImpl();
            return oLugarImpl.LugarUpdate(Lugar);
        }

        public bool LugarDelete(int IdLugar)
        {
            LugarImpl oLugarImpl = new LugarImpl();
            return oLugarImpl.LugarDelete(IdLugar);
        }

        public Lugar LugarGetById(int IdLugar)
        {
            LugarImpl oLugarImpl = new LugarImpl();
            return oLugarImpl.LugarGetById(IdLugar);
        }

        public List<Lugar> LugarGetAll()
        {
            LugarImpl oLugarImpl = new LugarImpl();
            return oLugarImpl.LugarGetAll();
        }

        public void CargarLugar(ref System.Web.UI.WebControls.DropDownList ddlLugar)
        {
            ddlLugar.DataSource = LugarGetAll();
            ddlLugar.DataTextField = "Descripcion";
            ddlLugar.DataValueField = "idLugar";
            ddlLugar.DataBind();
        }

        #endregion

        public void SelectedBPP(ref System.Web.UI.WebControls.DropDownList ddl)
        {
            Lugar oBpp = LugarGetAll().Find(bpp => bpp.Descripcion.ToUpper() == "BPP");
            if (oBpp != null && ddl != null)
            {
                ddl.SelectedValue = oBpp.IdLugar.ToString();
                ddl.Enabled = false;
            }
        }
    }
}
