

using System;
using System.Data;
using ProcesoPP.SqlServerImpl;
using ProcesoPP.Model;
using System.Collections.Generic;

namespace ProcesoPP.Business
{
	public class OrdenTrabajoBus
	{
		#region Business

		public int OrdenTrabajoAdd(OrdenTrabajo ordentrabajo)
		{
			OrdenTrabajoImpl oOrdenTrabajoImpl = new OrdenTrabajoImpl();
			return oOrdenTrabajoImpl.OrdenTrabajoAdd(ordentrabajo);
		}

		public bool OrdenTrabajoUpdate(OrdenTrabajo ordentrabajo)
		{
			OrdenTrabajoImpl oOrdenTrabajoImpl = new OrdenTrabajoImpl();
			return oOrdenTrabajoImpl.OrdenTrabajoUpdate(ordentrabajo);
		}

		public bool OrdenTrabajoDelete(int IdOrden)
		{
			OrdenTrabajoImpl oOrdenTrabajoImpl = new OrdenTrabajoImpl();
			return oOrdenTrabajoImpl.OrdenTrabajoDelete(IdOrden);
		}

		public OrdenTrabajo OrdenTrabajoGetById(int IdOrden)
		{
			OrdenTrabajoImpl oOrdenTrabajoImpl = new OrdenTrabajoImpl();
			return oOrdenTrabajoImpl.OrdenTrabajoGetById(IdOrden);
		}

		public List<OrdenTrabajo> OrdenTrabajoGetAll()
		{
			OrdenTrabajoImpl oOrdenTrabajoImpl = new OrdenTrabajoImpl();
			return oOrdenTrabajoImpl.OrdenTrabajoGetAll();
		}

		#endregion

        public DataTable OrdenTrabajoGetByFilter(string nroSolicitud, string idEmpresa, string fechaDesde, string fechaHasta, string nroParte, string idTrailer, string idSector, string idCuadrilla, string idTipoServicio, string idCondicion)
        {
            OrdenTrabajoImpl oOrdenTrabajoImpl = new OrdenTrabajoImpl();
            return oOrdenTrabajoImpl.OrdenTrabajoGetByFilter(nroSolicitud, idEmpresa, fechaDesde, fechaHasta, nroParte, idTrailer, idSector, idCuadrilla, idTipoServicio, idCondicion);
        }

        public object OrdenTrabajoRegresoGetByFilter(string nroSolicitud, string idEmpresa, string fechaDesde, string fechaHasta)
        {
            OrdenTrabajoImpl oOrdenTrabajoImpl = new OrdenTrabajoImpl();
            return oOrdenTrabajoImpl.OrdenTrabajoRegresoGetByFilter(nroSolicitud, idEmpresa, fechaDesde, fechaHasta);
        }

        public void OrdenTrabajoAddRegreso(int _idSolicitudOdiginal, int idSolicitudRegreso)
        {
            OrdenTrabajo oOrdenOriginal = new OrdenTrabajo();
            oOrdenOriginal = OrdenTrabajoGetAll().Find(o => o.IdSolicitudCliente == _idSolicitudOdiginal);
            if (oOrdenOriginal != null && oOrdenOriginal.IdOrden > 0)
            {
                oOrdenOriginal.IdSolicitudCliente = idSolicitudRegreso;
                OrdenTrabajoAdd(oOrdenOriginal);
            }
        }

        public int Guardar(OrdenTrabajo oOTI)
        {
            if (oOTI.IdOrden == 0)
            {
                oOTI.IdOrden  = OrdenTrabajoAdd(oOTI);
            }
            else
            {
                OrdenTrabajoUpdate(oOTI);
            }

            return oOTI.IdOrden;
        }

        public OrdenTrabajo OrdenTrabajoGetByIdSolicitud(int idSS)
        {
            OrdenTrabajoImpl oOrdenTrabajoImpl = new OrdenTrabajoImpl();
            return oOrdenTrabajoImpl.OrdenTrabajoGetByIdSolicitud(idSS);
        }

        public List<OrdenTrabajo> SolicitudesParteInterno(DateTime dateTime)
        {
            OrdenTrabajoImpl oOrdenTrabajoImpl = new OrdenTrabajoImpl();
            return oOrdenTrabajoImpl.SolicitudesParteInterno(dateTime);
        }
    }
}
