

using System;
using System.Data;
using ProcesoPP.SqlServerImpl;
using ProcesoPP.Model;
using System.Collections.Generic;

namespace ProcesoPP.Business
{
	public class ParteEntregaBus
	{
		#region Business

		public int ParteEntregaAdd(ParteEntrega parteentrega)
		{
			ParteEntregaImpl oParteEntregaImpl = new ParteEntregaImpl();
			return oParteEntregaImpl.ParteEntregaAdd(parteentrega);
		}

		public bool ParteEntregaUpdate(ParteEntrega parteentrega)
		{
			ParteEntregaImpl oParteEntregaImpl = new ParteEntregaImpl();
			return oParteEntregaImpl.ParteEntregaUpdate(parteentrega);
		}

		public bool ParteEntregaDelete(int IdParteEntrega)
		{
			ParteEntregaImpl oParteEntregaImpl = new ParteEntregaImpl();
			return oParteEntregaImpl.ParteEntregaDelete(IdParteEntrega);
		}

		public ParteEntrega ParteEntregaGetById(int IdParteEntrega)
		{
			ParteEntregaImpl oParteEntregaImpl = new ParteEntregaImpl();
			return oParteEntregaImpl.ParteEntregaGetById(IdParteEntrega);
		}

		public List<ParteEntrega> ParteEntregaGetAll()
		{
			ParteEntregaImpl oParteEntregaImpl = new ParteEntregaImpl();
			return oParteEntregaImpl.ParteEntregaGetAll();
		}

		#endregion


        public int ParteEntregaAdd(ParteEntrega oParteEntrega, List<VerificaParte> oListCant)
        {
            ParteEntregaImpl oParteEntregaImpl = new ParteEntregaImpl();
            int idParte = oParteEntregaImpl.ParteEntregaAdd(oParteEntrega, oListCant);
            oParteEntrega.IdParteEntrega = idParte;
            oParteEntrega.oOrdenTrabajo = (new OrdenTrabajoBus()).OrdenTrabajoGetById(oParteEntrega.IdOrdenTrabajo);
            if (oParteEntrega.oOrdenTrabajo.oSolicitudCliente.oSector.oEmpresa.Interna)
            {
                RemitosBus oRemitosBus = new RemitosBus();
                oRemitosBus.RemitoBonificado(oParteEntrega);
            }
            return idParte;
        }

        public bool ParteEntregaUpdate(ParteEntrega oParteEntrega, List<VerificaParte> oListCant)
        {
            ParteEntregaImpl oParteEntregaImpl = new ParteEntregaImpl();
            return oParteEntregaImpl.ParteEntregaUpdate(oParteEntrega, oListCant);
        }

        public int ParteInterno() 
        {
            int idParte = 0;
            if (DateTime.Today.Day == 22)//28)
            {                
                ParteEntregaImpl oParteEntregaImpl = new ParteEntregaImpl();
                OrdenTrabajoBus oOrdenTrabajoBus = new OrdenTrabajoBus();
                List<OrdenTrabajo> oListOTI = oOrdenTrabajoBus.SolicitudesParteInterno(DateTime.Today);

                foreach (OrdenTrabajo oOti in oListOTI)
                {
                    ParteEntrega oParte = new ParteEntrega();
                    oParte.Observacion = "PARTE INTERNO MENSUAL";
                    oParte.Entrega = oOti.Entraga;
                    oParte.NroParteEntrega = "PIM";
                    oParte.Fecha = DateTime.Now;
                    oParte.IdOrdenTrabajo = oOti.IdOrden;                    

                    idParte = oParteEntregaImpl.ParteEntregaAdd(oParte);
                }
            }
            return idParte;
        }


        public DataTable ValidarParteAcuerdo(int oAcuerdo)
        {
            ParteEntregaImpl oParteEntregaImpl = new ParteEntregaImpl();
            return oParteEntregaImpl.ValidarParteAcuerdo(oAcuerdo);
        }
    }
}
