
using System;
using System.Data;
using ProcesoPP.SqlServerImpl;
using ProcesoPP.Model;
using System.Collections.Generic;

namespace ProcesoPP.Business
{
	public class CodificacionBus
	{
		#region Business

		public int CodificacionAdd(Codificacion codificacion)
		{
			CodificacionImpl oCodificacionImpl = new CodificacionImpl();
			return oCodificacionImpl.CodificacionAdd(codificacion);
		}

		public bool CodificacionUpdate(Codificacion codificacion)
		{
			CodificacionImpl oCodificacionImpl = new CodificacionImpl();
			return oCodificacionImpl.CodificacionUpdate(codificacion);
		}

		public bool CodificacionDelete(int IdCodificacion)
		{
			CodificacionImpl oCodificacionImpl = new CodificacionImpl();
			return oCodificacionImpl.CodificacionDelete(IdCodificacion);
		}

		public Codificacion CodificacionGetById(int IdCodificacion)
		{
			CodificacionImpl oCodificacionImpl = new CodificacionImpl();
			return oCodificacionImpl.CodificacionGetById(IdCodificacion);
		}

		public List<Codificacion> CodificacionGetAll()
		{
			CodificacionImpl oCodificacionImpl = new CodificacionImpl();
			return oCodificacionImpl.CodificacionGetAll();
		}

        public List<Codificacion> CodificacionGetEquipo()
        {
            CodificacionImpl oCodificacionImpl = new CodificacionImpl();
            return oCodificacionImpl.CodificacionGetEquipo();
        }

        public List<Codificacion> CodificacionGetVehiculo()
        {
            CodificacionImpl oCodificacionImpl = new CodificacionImpl();
            return oCodificacionImpl.CodificacionGetVehiculo();
        }

		#endregion
	}
}
