

using System;
using System.Data;
using ProcesoPP.SqlServerImpl;
using ProcesoPP.Model;
using System.Collections.Generic;

namespace ProcesoPP.Business
{
	public class VerificaBus
	{
		#region Business

		public int VerificaAdd(Verifica verifica)
		{
			VerificaImpl oVerificaImpl = new VerificaImpl();
			return oVerificaImpl.VerificaAdd(verifica);
		}

		public bool VerificaUpdate(Verifica verifica)
		{
			VerificaImpl oVerificaImpl = new VerificaImpl();
			return oVerificaImpl.VerificaUpdate(verifica);
		}

		public bool VerificaDelete(int IdVerifica)
		{
			VerificaImpl oVerificaImpl = new VerificaImpl();
			return oVerificaImpl.VerificaDelete(IdVerifica);
		}

		public Verifica VerificaGetById(int IdVerifica)
		{
			VerificaImpl oVerificaImpl = new VerificaImpl();
			return oVerificaImpl.VerificaGetById(IdVerifica);
		}

		public List<Verifica> VerificaGetAll()
		{
			VerificaImpl oVerificaImpl = new VerificaImpl();
			return oVerificaImpl.VerificaGetAll();
		}

		#endregion

        public DataTable VerificaParteGetByIdParte(int _idParte)
        {
            VerificaParteImpl oVerificaParteImpl = new VerificaParteImpl();
            return oVerificaParteImpl.VerificaParteGetByIdParte(_idParte);
        }
    }
}
