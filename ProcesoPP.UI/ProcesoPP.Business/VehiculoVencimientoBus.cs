
using System;
using System.Data;
using ProcesoPP.SqlServerImpl;
using ProcesoPP.Model;
using System.Collections.Generic;

namespace ProcesoPP.Business
{
	public class VehiculoVencimientoBus
	{
		#region Business

		public int VehiculoVencimientoAdd(VehiculoVencimiento vehiculovencimiento)
		{
			VehiculoVencimientoImpl oVehiculoVencimientoImpl = new VehiculoVencimientoImpl();
			return oVehiculoVencimientoImpl.VehiculoVencimientoAdd(vehiculovencimiento);
		}

		public bool VehiculoVencimientoUpdate(VehiculoVencimiento vehiculovencimiento)
		{
			VehiculoVencimientoImpl oVehiculoVencimientoImpl = new VehiculoVencimientoImpl();
			return oVehiculoVencimientoImpl.VehiculoVencimientoUpdate(vehiculovencimiento);
		}

        public bool VehiculoVencimientoDelete(int IdVencimiento)
		{
			VehiculoVencimientoImpl oVehiculoVencimientoImpl = new VehiculoVencimientoImpl();
			return oVehiculoVencimientoImpl.VehiculoVencimientoDelete(IdVencimiento);
		}

        public VehiculoVencimiento VehiculoVencimientoGetById(int IdVencimiento)
		{
			VehiculoVencimientoImpl oVehiculoVencimientoImpl = new VehiculoVencimientoImpl();
			return oVehiculoVencimientoImpl.VehiculoVencimientoGetById(IdVencimiento);
		}

        public VehiculoVencimiento VehiculoVencimientoGetByIdVehiculo(int IdVehiculo)
        {
            VehiculoVencimientoImpl oVehiculoVencimientoImpl = new VehiculoVencimientoImpl();
            return oVehiculoVencimientoImpl.VehiculoVencimientoGetByIdVehiculo(IdVehiculo);
        }

		public List<VehiculoVencimiento> VehiculoVencimientoGetAll()
		{
			VehiculoVencimientoImpl oVehiculoVencimientoImpl = new VehiculoVencimientoImpl();
			return oVehiculoVencimientoImpl.VehiculoVencimientoGetAll();
		}

		#endregion
	}
}
