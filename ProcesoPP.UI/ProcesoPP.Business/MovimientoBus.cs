
using System;
using System.Data;
using ProcesoPP.SqlServerImpl;
using ProcesoPP.Model;
using System.Collections.Generic;

namespace ProcesoPP.Business
{
	public class MovimientoBus
	{
		#region Business

		public int MovimientoAdd(Movimiento movimiento)
		{
			MovimientoImpl oMovimientoImpl = new MovimientoImpl();
			return oMovimientoImpl.MovimientoAdd(movimiento);
		}

		public bool MovimientoUpdate(Movimiento movimiento)
		{
			MovimientoImpl oMovimientoImpl = new MovimientoImpl();
			return oMovimientoImpl.MovimientoUpdate(movimiento);
		}

		public bool MovimientoDelete(int IdMovimiento)
		{
			MovimientoImpl oMovimientoImpl = new MovimientoImpl();
			return oMovimientoImpl.MovimientoDelete(IdMovimiento);
		}

		public Movimiento MovimientoGetById(int IdMovimiento)
		{
			MovimientoImpl oMovimientoImpl = new MovimientoImpl();
			return oMovimientoImpl.MovimientoGetById(IdMovimiento);
		}

		public List<Movimiento> MovimientoGetAll()
		{
			MovimientoImpl oMovimientoImpl = new MovimientoImpl();
			return oMovimientoImpl.MovimientoGetAll();
		}

		#endregion
	}
}
