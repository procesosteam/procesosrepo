

using System;
using System.Data;
using ProcesoPP.SqlServerImpl;
using ProcesoPP.Model;
using System.Collections.Generic;

namespace ProcesoPP.Business
{
	public class CategoriaCarnetBus
	{
		#region Business

		public int CategoriaCarnetAdd(CategoriaCarnet categoriacarnet)
		{
			CategoriaCarnetImpl oCategoriaCarnetImpl = new CategoriaCarnetImpl();
			return oCategoriaCarnetImpl.CategoriaCarnetAdd(categoriacarnet);
		}

		public bool CategoriaCarnetUpdate(CategoriaCarnet categoriacarnet)
		{
			CategoriaCarnetImpl oCategoriaCarnetImpl = new CategoriaCarnetImpl();
			return oCategoriaCarnetImpl.CategoriaCarnetUpdate(categoriacarnet);
		}

		public bool CategoriaCarnetDelete(int IdCarnet)
		{
			CategoriaCarnetImpl oCategoriaCarnetImpl = new CategoriaCarnetImpl();
			return oCategoriaCarnetImpl.CategoriaCarnetDelete(IdCarnet);
		}

		public CategoriaCarnet CategoriaCarnetGetById(int IdCarnet)
		{
			CategoriaCarnetImpl oCategoriaCarnetImpl = new CategoriaCarnetImpl();
			return oCategoriaCarnetImpl.CategoriaCarnetGetById(IdCarnet);
		}

		public List<CategoriaCarnet> CategoriaCarnetGetAll()
		{
			CategoriaCarnetImpl oCategoriaCarnetImpl = new CategoriaCarnetImpl();
			return oCategoriaCarnetImpl.CategoriaCarnetGetAll();
		}

		#endregion
	}
}
