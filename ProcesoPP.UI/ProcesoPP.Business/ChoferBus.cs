

using System;
using System.Data;
using ProcesoPP.SqlServerImpl;
using ProcesoPP.Model;
using System.Collections.Generic;

namespace ProcesoPP.Business
{
	public class ChoferBus
	{
		#region Business

		public int ChoferAdd(Chofer chofer)
		{
			ChoferImpl oChoferImpl = new ChoferImpl();
			return oChoferImpl.ChoferAdd(chofer);
		}

		public bool ChoferUpdate(Chofer chofer)
		{
			ChoferImpl oChoferImpl = new ChoferImpl();
			return oChoferImpl.ChoferUpdate(chofer);
		}

		public bool ChoferDelete(int IdChofer)
		{
			ChoferImpl oChoferImpl = new ChoferImpl();
			return oChoferImpl.ChoferDelete(IdChofer);
		}

		public Chofer ChoferGetById(int IdChofer)
		{
			ChoferImpl oChoferImpl = new ChoferImpl();
			return oChoferImpl.ChoferGetById(IdChofer);
		}

		public List<Chofer> ChoferGetAll()
		{
			ChoferImpl oChoferImpl = new ChoferImpl();
			return oChoferImpl.ChoferGetAll();
		}

		#endregion

        public List<Chofer> ChoferGetAllCombo()
        {
            ChoferImpl oChoferImpl = new ChoferImpl();
            return oChoferImpl.ChoferGetAllCombo();
        }

        public void CargarCombo(ref System.Web.UI.WebControls.DropDownList ddlChofer, string p)
        {
            ddlChofer.DataSource = ChoferGetAllCombo();
            ddlChofer.DataValueField = "idChofer";
            ddlChofer.DataTextField = "Nombre";
            ddlChofer.DataBind();
            ddlChofer.Items.Insert(0, new System.Web.UI.WebControls.ListItem(p, "0"));
        }
    }
}
