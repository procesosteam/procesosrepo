﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ProcesoPP.Model;
using ProcesoPP.SqlServerImpl;
using System.Data;

namespace ProcesoPP.Business
{
    public class RemitosBus
    {
        public int RemitosAdd(Remitos remitos, List<RemitoDetalleTango> oListTango)
        {
            RemitosImpl oRemitosImpl = new RemitosImpl();
            return oRemitosImpl.RemitosAdd(remitos, oListTango);
        }

        public bool RemitosUpdate(Remitos remitos)
        {
            RemitosImpl oRemitosImpl = new RemitosImpl();
            return oRemitosImpl.RemitosUpdate(remitos);
        }

        public bool RemitosDelete(int IdRemito)
        {
            RemitosImpl oRemitosImpl = new RemitosImpl();
            return oRemitosImpl.RemitosDelete(IdRemito);
        }

        public Remitos RemitosGetById(int IdRemito)
        {
            RemitosImpl oRemitosImpl = new RemitosImpl();
            return oRemitosImpl.RemitosGetById(IdRemito);
        }

        public List<Remitos> RemitosGetAll()
        {
            RemitosImpl oRemitosImpl = new RemitosImpl();
            return oRemitosImpl.RemitosGetAll();
        }

        public DataTable RemitosGetByFilter(string nroSolicitud, string idEmpresa, string desde, string hasta, string nroParte, string idEquipo, string idSector, string idTipoServicio, string idCondicion, string nroRemito, string chkRem)
        {
            RemitosImpl oRemitosImpl = new RemitosImpl();
            return oRemitosImpl.RemitosGetByFilter(nroSolicitud, idEmpresa, desde, hasta, nroParte, idEquipo, idSector, idTipoServicio, idCondicion, nroRemito, chkRem);
        }

        public Remitos RemitoGetByIdParte(int idParte)
        {
             RemitosImpl oRemitosImpl = new RemitosImpl();
             return oRemitosImpl.RemitoGetByIdParte(idParte);
        }

        public int RemitoGetNroRemito()
        {
            RemitosImpl oRemitosImpl = new RemitosImpl();
            return oRemitosImpl.RemitoGetNroRemito();
        }

        public DateTime RemitosGetUltimaFecha()
        {
            RemitosImpl oRemitosImpl = new RemitosImpl();
            return oRemitosImpl.RemitosGetUltimaFecha();
        }

        public DataTable RemitosAnuladosGetByFilter(string nroSolicitud, string idEmpresa, string desde, string hasta, string nroParte, string idEquipo, string idSector, string idTipoServicio, string idCondicion)
        {
            RemitosImpl oRemitosImpl = new RemitosImpl();
            return oRemitosImpl.RemitosAnuladosGetByFilter(nroSolicitud, idEmpresa, desde, hasta, nroParte, idEquipo, idSector, idTipoServicio, idCondicion);
        }

        public DataTable RemitoGetRPT(int _idRemito)
        {
             RemitosImpl oRemitosImpl = new RemitosImpl();
             return oRemitosImpl.RemitoGetRPT(_idRemito);
        }

        public bool RemitoNumeroUpdate(int nroRemito)
        {
            RemitosImpl oRemitosImpl = new RemitosImpl();
            return oRemitosImpl.RemitoNumeroUpdate(nroRemito);
        }

        public bool RemitoBonificado(ParteEntrega oParteEntrega)
        {
            RemitosImpl oRemitosImpl = new RemitosImpl();
            return oRemitosImpl.RemitoBonificado(oParteEntrega);
        }

        public DataTable RemitosBonificadoGetByFilter(string nroSolicitud, string idEmpresa, string desde, string hasta, string nroParte, string idEquipo, string idSector, string idTipoServicio, string idCondicion, string nroRemito)
        {
            RemitosImpl oRemitosImpl = new RemitosImpl();
            return oRemitosImpl.RemitosBonificadosGetByFilter(nroSolicitud, idEmpresa, desde, hasta, nroParte, idEquipo, idSector, idTipoServicio, idCondicion, nroRemito);
        }

        public DataTable RemitosHistorialGetByFilter(string nroSolicitud, string idEmpresa, string desde, string hasta, string nroParte, string idEquipo, string idSector, string idTipoServicio, string idCondicion, string nroRemito, string chkRem)
        {
            RemitosImpl oRemitosImpl = new RemitosImpl();
            return oRemitosImpl.RemitosHistorialGetByFilter(nroSolicitud, idEmpresa, desde, hasta, nroParte, idEquipo, idSector, idTipoServicio, idCondicion, nroRemito, chkRem);
        }
    }
}
