
///////////////////////////////////////////////////////////////////////////
//
// Generated by MyGeneration Version # (1.3.1.1) 
// 
//
///////////////////////////////////////////////////////////////////////////


using System;
using System.Data;
using ProcesoPP.SqlServerImpl;
using ProcesoPP.Model;
using System.Collections.Generic;

namespace ProcesoPP.Business
{
	public class VehiculoSeguroBus
	{
		#region Business

		public int VehiculoSeguroAdd(VehiculoSeguro vehiculoseguro)
		{
			VehiculoSeguroImpl oVehiculoSeguroImpl = new VehiculoSeguroImpl();
			return oVehiculoSeguroImpl.VehiculoSeguroAdd(vehiculoseguro);
		}

		public bool VehiculoSeguroUpdate(VehiculoSeguro vehiculoseguro)
		{
			VehiculoSeguroImpl oVehiculoSeguroImpl = new VehiculoSeguroImpl();
			return oVehiculoSeguroImpl.VehiculoSeguroUpdate(vehiculoseguro);
		}

        public bool VehiculoSeguroDelete(int IdSeguro)
		{
			VehiculoSeguroImpl oVehiculoSeguroImpl = new VehiculoSeguroImpl();
			return oVehiculoSeguroImpl.VehiculoSeguroDelete(IdSeguro);
		}

        public VehiculoSeguro VehiculoSeguroGetById(int IdSeguro)
		{
			VehiculoSeguroImpl oVehiculoSeguroImpl = new VehiculoSeguroImpl();
			return oVehiculoSeguroImpl.VehiculoSeguroGetById(IdSeguro);
		}

        public VehiculoSeguro VehiculoSeguroGetByIdVehiculo(int IdVehiculo)
        {
            VehiculoSeguroImpl oVehiculoSeguroImpl = new VehiculoSeguroImpl();
            return oVehiculoSeguroImpl.VehiculoSeguroGetByIdVehiculo(IdVehiculo);
        }

		public List<VehiculoSeguro> VehiculoSeguroGetAll()
		{
			VehiculoSeguroImpl oVehiculoSeguroImpl = new VehiculoSeguroImpl();
			return oVehiculoSeguroImpl.VehiculoSeguroGetAll();
		}

		#endregion
	}
}
