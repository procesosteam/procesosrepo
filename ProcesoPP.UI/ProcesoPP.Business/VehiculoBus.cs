
using System;
using System.Data;
using ProcesoPP.SqlServerImpl;
using ProcesoPP.Model;
using System.Collections.Generic;

namespace ProcesoPP.Business
{
	public class VehiculoBus
	{
		#region Business

		public int VehiculoAdd(Vehiculo vehiculo)
		{
			VehiculoImpl oVehiculoImpl = new VehiculoImpl();
			return oVehiculoImpl.VehiculoAdd(vehiculo);
		}

		public bool VehiculoUpdate(Vehiculo vehiculo)
		{
			VehiculoImpl oVehiculoImpl = new VehiculoImpl();
			return oVehiculoImpl.VehiculoUpdate(vehiculo);
		}

        public bool VehiculoDelete(int IdVehiculo)
		{
			VehiculoImpl oVehiculoImpl = new VehiculoImpl();
			return oVehiculoImpl.VehiculoDelete(IdVehiculo);
		}

        public Vehiculo VehiculoGetById(int IdVehiculo)
		{
			VehiculoImpl oVehiculoImpl = new VehiculoImpl();
			return oVehiculoImpl.VehiculoGetById(IdVehiculo);
		}

        public DataTable VehiculoGetByFilter(string patente, string ruta, string vCedula, string vPatente, string vtv, string idCod, string idEMpresa)
        {
            VehiculoImpl oVehiculoImpl = new VehiculoImpl();
            return oVehiculoImpl.VehiculoGetByFilter(patente, ruta, vCedula, vPatente, vtv, idCod, idEMpresa);
        }

        public DataTable EquipoGetByFilter(string patente, string ruta, string vCedula, string vPatente, string vtv, string idCod, string idEMpresa)
        {
            VehiculoImpl oVehiculoImpl = new VehiculoImpl();
            return oVehiculoImpl.EquipoGetByFilter(patente, ruta, vCedula, vPatente, vtv, idCod, idEMpresa);
        }

        public DataTable VehiculoHistorialGetByFilter(string patente, string ruta, string vCedula, string vPatente, string vtv, string idCod, string idEMpresa)
        {
            VehiculoImpl oVehiculoImpl = new VehiculoImpl();
            return oVehiculoImpl.VehiculoHistorialGetByFilter(patente, ruta, vCedula, vPatente, vtv, idCod, idEMpresa);
        }

		public List<Vehiculo> VehiculoGetAll()
		{
			VehiculoImpl oVehiculoImpl = new VehiculoImpl();
			return oVehiculoImpl.VehiculoGetAll();
		}

        public DataTable VehiculoGetEquipos()
        {
            VehiculoImpl oVehiculoImpl = new VehiculoImpl();
            return oVehiculoImpl.VehiculoGetEquipos();
        }

        public DataTable VehiculoGetVehiculos()
        {
            VehiculoImpl oVehiculoImpl = new VehiculoImpl();
            return oVehiculoImpl.VehiculoGetVehiculos();
        }

		#endregion
        
        public DataTable VehiculoGetByIdSector(int idSector, int idEmpresa)
        {
            VehiculoImpl oVehiculoImpl = new VehiculoImpl();
            return oVehiculoImpl.VehiculoGetByIdSector(idSector,idEmpresa);
        }

        public void CargarComboEquipo(ref System.Web.UI.WebControls.DropDownList ddlTrailer, string p)
        {
            ddlTrailer.DataSource = VehiculoGetEquipos();
            ddlTrailer.DataTextField = "Codificacion";
            ddlTrailer.DataValueField = "idVehiculo";
            ddlTrailer.DataBind();
            ddlTrailer.Items.Insert(0, new System.Web.UI.WebControls.ListItem(p, "0"));
        }

        public void CargarComboVehiculos(ref System.Web.UI.WebControls.DropDownList ddlTrailer, string p)
        {
            ddlTrailer.DataSource = VehiculoGetVehiculos();
            ddlTrailer.DataTextField = "codificacion";
            ddlTrailer.DataValueField = "idVehiculo";
            ddlTrailer.DataBind();
            ddlTrailer.Items.Insert(0, new System.Web.UI.WebControls.ListItem(p, "0"));
        }


        public DataTable VehiculosGetByAcuerdoCliente(int idEmpresa, int idSector)
        {
            VehiculoImpl oVehiculoImpl = new VehiculoImpl();
            return oVehiculoImpl.VehiculosGetByAcuerdoCliente(idEmpresa, idSector);
        }
    }
}
