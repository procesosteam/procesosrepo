

using System;
using System.Data;
using ProcesoPP.SqlServerImpl;
using ProcesoPP.Model;
using System.Collections.Generic;
using System.Web.UI.WebControls;

namespace ProcesoPP.Business
{
	public class CondicionBus
	{
		#region Business

		public int CondicionAdd(Condicion condicion)
		{
			CondicionImpl oCondicionImpl = new CondicionImpl();
			return oCondicionImpl.CondicionAdd(condicion);
		}

		public bool CondicionUpdate(Condicion condicion)
		{
			CondicionImpl oCondicionImpl = new CondicionImpl();
			return oCondicionImpl.CondicionUpdate(condicion);
		}

		public bool CondicionDelete(int IdCondicion)
		{
			CondicionImpl oCondicionImpl = new CondicionImpl();
			return oCondicionImpl.CondicionDelete(IdCondicion);
		}

		public Condicion CondicionGetById(int IdCondicion)
		{
			CondicionImpl oCondicionImpl = new CondicionImpl();
			return oCondicionImpl.CondicionGetById(IdCondicion);
		}

		public List<Condicion> CondicionGetAll()
		{
			CondicionImpl oCondicionImpl = new CondicionImpl();
			return oCondicionImpl.CondicionGetAll();
		}

		#endregion
        
        public void CargarCombo(ref System.Web.UI.WebControls.DropDownList ddlCondicion, string primerItem)
        {
            ddlCondicion.DataSource = CondicionGetAll();
            ddlCondicion.DataTextField = "Descripcion";
            ddlCondicion.DataValueField = "idCondicion";
            ddlCondicion.DataBind();
            ddlCondicion.Items.Insert(0,new ListItem(primerItem,"0"));
        }
    }
}
