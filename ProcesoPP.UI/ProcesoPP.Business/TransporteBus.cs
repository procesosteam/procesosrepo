

using System;
using System.Data;
using ProcesoPP.SqlServerImpl;
using ProcesoPP.Model;
using System.Collections.Generic;
using System.Web.UI.WebControls;

namespace ProcesoPP.Business
{
	public class TransporteBus
	{
		#region Business

		public int TransporteAdd(Transporte transporte)
		{
			TransporteImpl oTransporteImpl = new TransporteImpl();
			return oTransporteImpl.TransporteAdd(transporte);
		}

		public bool TransporteUpdate(Transporte transporte)
		{
			TransporteImpl oTransporteImpl = new TransporteImpl();
			return oTransporteImpl.TransporteUpdate(transporte);
		}

		public bool TransporteDelete(int IdTransporte)
		{
			TransporteImpl oTransporteImpl = new TransporteImpl();
			return oTransporteImpl.TransporteDelete(IdTransporte);
		}

		public Transporte TransporteGetById(int IdTransporte)
		{
			TransporteImpl oTransporteImpl = new TransporteImpl();
			return oTransporteImpl.TransporteGetById(IdTransporte);
		}

		public List<Transporte> TransporteGetAll()
		{
			TransporteImpl oTransporteImpl = new TransporteImpl();
			return oTransporteImpl.TransporteGetAll();
		}

		#endregion

        public void CargarCombo(ref System.Web.UI.WebControls.DropDownList ddlVehiculo, string primerItem)
        {
            ddlVehiculo.DataSource = TransporteGetAll();
            ddlVehiculo.DataTextField = "Descripcion";
            ddlVehiculo.DataValueField = "idTransporte";
            ddlVehiculo.DataBind();
            ddlVehiculo.Items.Insert(0, new ListItem(primerItem, "0"));
        }
    }
}
