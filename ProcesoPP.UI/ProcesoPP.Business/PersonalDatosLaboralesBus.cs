
using System;
using System.Data;
using ProcesoPP.SqlServerImpl;
using ProcesoPP.Model;
using System.Collections.Generic;

namespace ProcesoPP.Business
{
	public class PersonalDatosLaboralesBus
	{
		#region Business

		public int PersonalDatosLaboralesAdd(PersonalDatosLaborales personaldatoslaborales)
		{
			PersonalDatosLaboralesImpl oPersonalDatosLaboralesImpl = new PersonalDatosLaboralesImpl();
			return oPersonalDatosLaboralesImpl.PersonalDatosLaboralesAdd(personaldatoslaborales);
		}

		public bool PersonalDatosLaboralesUpdate(PersonalDatosLaborales personaldatoslaborales)
		{
			PersonalDatosLaboralesImpl oPersonalDatosLaboralesImpl = new PersonalDatosLaboralesImpl();
			return oPersonalDatosLaboralesImpl.PersonalDatosLaboralesUpdate(personaldatoslaborales);
		}

		public bool PersonalDatosLaboralesDelete(int IdPersonalDatosLaborales)
		{
			PersonalDatosLaboralesImpl oPersonalDatosLaboralesImpl = new PersonalDatosLaboralesImpl();
			return oPersonalDatosLaboralesImpl.PersonalDatosLaboralesDelete(IdPersonalDatosLaborales);
		}

		public PersonalDatosLaborales PersonalDatosLaboralesGetById(int IdPersonalDatosLaborales)
		{
			PersonalDatosLaboralesImpl oPersonalDatosLaboralesImpl = new PersonalDatosLaboralesImpl();
			return oPersonalDatosLaboralesImpl.PersonalDatosLaboralesGetById(IdPersonalDatosLaborales);
		}

		public List<PersonalDatosLaborales> PersonalDatosLaboralesGetAll()
		{
			PersonalDatosLaboralesImpl oPersonalDatosLaboralesImpl = new PersonalDatosLaboralesImpl();
			return oPersonalDatosLaboralesImpl.PersonalDatosLaboralesGetAll();
		}

		#endregion
	}
}
